'use strict';

let dataSource = {
  "mindslab": {
    "name": "mindslab",
    "connector": "mysql",
    "host": "10.122.64.123",
    "port": 3306,
    "database": "chatbot",
    "user": "minds",
    "password": "msl1234~"
  },
  "queryjet": {
    "name": "queryjet",
    "connector": "mysql",
    "host": "10.122.64.123",
    "port": 3306,
    "database": "mobi_chat",
    "user": "minds",
    "password": "msl1234~"
  },
  "storage": {
    "name": "storage",
    "connector": "loopback-component-storage",
    "provider": "filesystem",
    "root": `${process.env.CHATBOT_ROOT}/storage`,
    "maxFileSize": "5242880"
  },
  "statistic": {
    "name": "mindslab",
    "connector": "mysql",
    "host": "10.122.64.123",
    "port": 3306,
    "database": "chatbot",
    "user": "minds",
    "password": "msl1234~"
  },
  "etridialog": {
    "host": "",
    "port": 3306,
    "url": "mysql://minds:msl1234~@10.122.64.123/etridialog",
    "database": "",
    "password": "",
    "name": "etridialog",
    "user": "",
    "connector": "mysql"
  },
}
module.exports = {
  queryjet: dataSource.queryjet,
  mindslab: dataSource.mindslab,
  storage: dataSource.storage,
  statistic: dataSource.statistic,
  etridialog: dataSource.etridialog
};
