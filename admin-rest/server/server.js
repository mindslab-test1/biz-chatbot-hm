'use strict';

/** will not using loopback-context
# leave it at first line of entry script
require('cls-hooked');

# middleware config
"loopback-context#per-request": {
  "params": {
    "enableHttpContext": true
  }
},
**/

const loopback = require('loopback');
const boot = require('loopback-boot');

const moment = require('moment');
const path = require('path');

const http = require('http');
const https = require('https');
const sslConfig = require('./ssl-config');

const app = module.exports = loopback();

const loggerName = path.basename(__filename, path.extname(__filename));

const isDev = () => process.env.NODE_ENV  !== 'production';
// const devlog = (...args) => { app.isDev() && console.log('debug',...args); };
const devlog = (...args) => {
  app.isDev()
  && console.log(
    '[%s] %s %s - %s'
    , 'DEBUG' // level
    , moment(new Date()).format('YYYY-MM-DD HH:mm:ss,SSS')  // time
    , ...args // logger(model) name, messages
  );
};

const getTz = () => process.env.TZ;

app.isDev = isDev;
app.devlog = devlog;

app.getTz = getTz;

app.devlog(loggerName, 'server launch...');
app.getTz() && app.devlog(loggerName, `tz: ${app.getTz()}`);

app.messages = require('./messages').messages;

// for loopback 2.0 cookie auth
// app.use(loopback.token({
//   model: app.models.accessToken
// }));

// for loopback 3.0 cookie auth
// 현재는 middleware.json 에 설정되어 있음.
// app.middleware('auth', loopback.token());

// request logger
if (isDev()) {

  // log 수정
  const monitorLog = req => {
    // console.log(req.method, req.url, req.body);
  
    const _getParams =_=> {
      return urls[1] ? decodeURI(urls[1]) : req.headers.filter;
    };
    
    const urls = req.url.split('?'),
    path = urls[0],
    params = _getParams() || '';
    devlog(req.method, path, 'params:', params, 'body:', req.body
      // ,'\n======\n'
      // ,'req:\n' , req
      // ,'\n======\n'
    );
  };


  app.use(require('body-parser').urlencoded({extended: true}), (req, res, next) => {
    monitorLog(req);
    next();
  });
}

app.start = function(httpOnly) {

  console.log("---------------------------------------------------------------------------------------------------------");
  console.log("환경", process.env.NODE_ENV);
  console.log("---------------------------------------------------------------------------------------------------------");

  if (httpOnly === undefined) {
    httpOnly = process.env.HTTP;
  }
  let server = null;
  if (!httpOnly || httpOnly !== 'Y') {
    const options = {
      key: sslConfig.privateKey,
      cert: sslConfig.certificate,
      passphrase: sslConfig.passphrase
    };
    server = https.createServer(options, app);
  } else {
    server = http.createServer(app);
  }
  server.listen(app.get('port'), function() {
    let baseUrl = ((!httpOnly || httpOnly !== 'Y') ? 'https://' : 'http://') + app.get('host') + ':' + app.get('port');
    app.emit('started', baseUrl);
    console.log('LoopBack server listening @ %s%s', baseUrl, '/');

    if (app.get('loopback-component-explorer')) {
      const explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }

    // request tester
    if (isDev()) {
      app.all('/ping', require('body-parser').urlencoded({extended: true}), (req, res) => {
        let html = `
        <html>
        <body>
        <h1>Header</h1>
        <pre>${JSON.stringify(req.headers, null, 4)}</pre>
        <h1>Body (urlencoded)</h1>
        <pre>${JSON.stringify(req.body, null, 4)}</pre>
        </body>
        </html>
        `;
        res.send(html);
        console.log('=====PING-HEADER=====\n', req.headers, '\n=====PING-BODY=====\n', req.body, '\n=====PING-END=====');
      });
      console.log('Test a request at %s/ping', baseUrl);
    }
  });
  return server;
};



// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) {
    app.start();
  }
});