/* eslint-disable camelcase */
'use strict';

const fs = require('fs');
const path = require('path');
const grpc = require('grpc');


let initOk = false;
var protoRoot,
  protos,
  hostMap;


/**
 * @namespace grpc-client-delegate
 */


function isObject(item) {
  return item && typeof item === 'object' && !Array.isArray(item);
}

function deepMerge(target, source) {
  if (isObject(target) && isObject(source)) {
    for (let key in source) {
      if (isObject(source[key])) {
        if (!target[key]) {
          Object.assign(target, {[key]: {}});
        }
        deepMerge(target[key], source[key]);
      } else {
        Object.assign(target, {[key]: source[key]});
      }
    }
  }
  return target;
}

/** dynamically load all proto files from given path **/
function _loadProtos(filePath, obj) {
  try {
    let stat = fs.statSync(filePath);
    if (stat.isFile() && filePath.endsWith('.proto')) {
      if (protoRoot) {
        filePath = {root: protoRoot, file: filePath.replace(protoRoot + '/', '')};
        // console.log(`file path = ${filePath.root}, ${filePath.file}`);
      }
      // merge loaded grpc object to obj
      deepMerge(obj, grpc.load(filePath));
    } else if (stat.isDirectory()) {
      fs.readdirSync(filePath).forEach(_filePath => {
        if (!(_filePath === 'compiler' && filePath.endsWith('google/protobuf'))) {
          _loadProtos(path.join(filePath, _filePath), obj);
        }
      });
    }
    return obj;
  } catch (e) {
    throw e;
  }
}

function loadProtos(_protoRoot) {
  if (fs.statSync(_protoRoot).isDirectory()) {
    let start = _protoRoot.substr(0, 2) === './' ? 2 : 0;
    let tailCut = _protoRoot.substr(_protoRoot.length - 1) === '/' ? -1 : 0;
    protoRoot = _protoRoot.substring(start, _protoRoot.length - tailCut);
  } else {
    protoRoot = null;
  }
  return _loadProtos(_protoRoot, {});
}


function initProtos(_protos, _namespace) {

  if (_protos === null || _protos === undefined) {
    _protos = protos;
  }

  for (let k in _protos) {
    let proto = _protos[k];
    let namespace = _namespace ? `${_namespace}.${k}` : k;

    // console.log(`proto[${namespace}] service: ${!!proto.service} type: ${typeof proto}`);

    if (proto.service) {
      let host = hostMap[namespace];
      if (host === false) continue;

      // find host by wildcarding namespace
      if (!host) {
        let pieces = namespace.split('.');
        while (!host && pieces.length > 0) {
          pieces.pop();
          host = hostMap[pieces.concat(['*']).join('.')];
        }
      }
      if (host === false) continue;

      // initiate service
      if (host) {
        _protos[k] = new proto(host, grpc.credentials.createInsecure());
        // console.log(`\x1b[32m[GRPC] ${namespace} configured: ${host}\x1b[0m`);
      } else {
        console.log(`\x1b[93m\x1b[41m[GRPC] ${namespace} not configured\x1b[0m`);
        return false;
      }
    } else if (typeof proto === 'object') {
      if (initProtos(proto, namespace) === false) return false;
    }
  }

  return true;
}


/**
 * Initialize gRPC object from a .proto file.path & host-map.
 * @memberof grpc
 * @alias grpc.init
 * @param {string=} protoPath The file or path to load
 * @param {{string}|{service: string, hostPort: string}} hosts The host to assign to all services
 *     or The host-list to assign to services of proto.
 */
function init(protoPath, hosts) {
  initOk = false;

  protos = loadProtos(protoPath);
  if (!protos) {
    return {result: false, error: 'Init failed: loading protos.'};
  }

  hostMap = typeof hosts === 'string' ? {'*': hosts} : hosts;
  if (!initProtos()) {
    return {result: false, error: 'Init failed: assigning hosts.'};
  }

  initOk = true;

  return {result: true};
}

function getRpc(selector) {
  return new Promise((resolve, reject) => {
    if (!initOk) {
      reject({code: 9000, detail: 'gRPC Client Delegate is not ready.'});
      return;
    }

    let pieces = selector.split('/');
    if (pieces.length != 2) {
      reject({code: 9001, detail: 'Selector is not properly assigned.'});
      return;
    }

    let func = pieces.pop();
    pieces = pieces[0].split('.');
    let channel = protos;
    while (pieces.length > 0) {
      channel = channel[pieces.shift()];
      if (typeof channel === 'undefined') {
        reject({code: 9002, detail: `Service[${selector}] is not found.`});
        return;
      }
    }

    if (typeof channel[func] === 'function') {
      resolve({channel: channel, func: func});
    } else {
      reject({code: 9003, detail: `Function is not found for ${selector}.`});
    }
  });
}

function call(selector, sync, param, inMetadata = null) {
  return getRpc(selector)
    .then(rpc => new Promise((resolve, reject) => {
      let _metadata = new grpc.Metadata();
      if (inMetadata) {
        inMetadata.forEach(meta => _metadata.add(meta.attr, meta.value));
      }

      let result, outMetadata;
      let call = sync ?
        rpc.channel[rpc.func](param, _metadata, (err, res) => {
          if (err) {
            console.log('err', err);
            if (err.statusCode) {
              result = {code: err.statusCode, detail: err.message};
            } else if (err.code && err.message) {
              result = {code: err.code, detail: `gRPC:${err.message}`};
            } else {
              result = {code: 9004, detail: 'gRPC sync call failed.'};
            }
          } else {
            result = res;
          }
        }) :
        rpc.channel[rpc.func](_metadata);

      call.on('metadata', metadata => {
        outMetadata = metadata.getMap();
        // console.log('metadata', outMetadata);
      });

      call.on('status', status => {
        // console.log('status', status);
        if (Object.keys(outMetadata).length > 0) {
          result.$metadata = [];
          Object.keys(outMetadata).forEach(key => {
            if (key !== 'accept-encoding') {
              result.$metadata.push({
                attr: key,
                value: new Buffer(outMetadata[key], 'base64').toString('utf-8')
              });
            }
          });
        }
        outMetadata = status.metadata.getMap();
        if (Object.keys(outMetadata).length > 0) {
          if (!result.$metadata) {
            result.$metadata = [];
          }
          Object.keys(outMetadata).forEach(key =>
            result.$metadata.push({attr: key, value: outMetadata[key]})
          );
        }

        if (status.code == 0) {
          resolve(result);
        } else {
          reject(result);
        }
      });

      if (!sync) {
        call.on('data', msg => {
          result = msg;
        });

        call.on('error', err => {
          console.log('>> error: ', err);
        });

        call.on('end', () => {
          // console.log('end : ');
        });

        call.write(param);
        call.end();
      }

    }));
}

function callFunction(selector, param, inMetadata = null) {
  return call(selector, true, param, inMetadata);
}


module.exports = {
  init,
  callFunction
};
