const roleList =
  [
    ['svd', 'RX', 'Monitor', 'Monitoring'], // Supervisord Roles
    ['dam', 'CRUDX', 'DAM', 'Dialog Agent Manager'], // DialogAgentManager Roles
    ['da', 'CRUDX', 'DA', 'Dialog Agent'], // DialogAgent Roles
    ['sc', 'CRUD', 'SC', 'Simple Classifier'], // Simple Classifier Roles
    ['ta', 'R', 'DNN', 'DNN Classifier'], // TA Classifier Roles
    ['itf', 'CRUD', 'ITF', 'Intent Finder'], // Intent Finder Roles
    ['aupl', 'R', 'AU-POL', 'Authentication Policy'], // Authentication Policy Roles
    ['stt', 'R', 'STT', 'STT Model'], // STT Model Roles
    ['chbt', 'CRUD', 'Chatbot', 'Chatbot'], // Chatbot Roles
    ['dai', 'CRUDX', 'DAI', 'Dialog Agent Instance'], // Dialog Agent Instance Roles
    ['fcd', 'R', 'Chat', 'Test Chat'], // Facade Roles
    ['egn', 'CRUD', 'Engine', 'Engine Configuration'], // Engine Configure Roles
    ['stfc', 'R', 'Satisfaction', 'Satisfaction'], // Satisfaction Roles
    ['accr', 'RUD', 'Accuracy', 'Accuracy'] // Accuracy Roles
  ];

const grpcAcls = [
  /*** DialogAgentManager APIs ***/
  ['getDialogAgentManagerAllList', 'dam^R'],
  ['getDialogAgentManagerInfo', 'dam^R'],
  ['getDialogAgentManagerList', 'dam^R'],
  ['insertDialogAgentManagerInfo', 'dam^C'],
  ['getDialogAgentWithDialogAgentInstanceList', 'dam^R'],
  ['updateDialogAgentManagerInfo', 'dam^U'],
  ['deleteDialogAgentManager', 'dam^D'],
  ['stopDialogAgentManager', 'dam^X'],
  ['startDialogAgentManager', 'dam^X'],
  ['restartDialogAgentManager', 'dam^X'],

  /*** DialogAgent APIs ***/
  ['getDialogAgentAllList', 'da^R'],
  ['getDialogAgentInfo', 'da^R'],
  ['getDialogAgentActivationInfoList', 'da^R'],
  ['insertDialogAgentInfo', 'da^C'],
  ['updateDialogAgentInfo', 'da^U'],
  ['deleteDialogAgent', 'da^D'],
  ['getRuntimeParameters', 'da^R'],
  ['getUserAttributes', 'da^R'],
  ['getExecutableDA', 'da^R'],
  ['stopDialogAgent', 'da^X'],
  ['startDialogAgent', 'da^X'],
  ['restartDialogAgent', 'da^X'],

  /*** Chatbot APIs ***/
  ['getChatbotAllList', 'chbt^R'],
  ['getChatbotInfo', 'chbt^R'],
  ['insertChatbotInfo', 'chbt^C'],
  ['updateChatbotInfo', 'chbt^U'],
  ['deleteChatbot', 'chbt^D'],

  /*** Simple Classifier APIs ***/
  ['getSimpleClassifierAllList', 'sc^R'],
  ['getSimpleClassifierInfo', 'sc^R'],
  ['insertSimpleClassifierInfo', 'sc^C'],
  ['updateSimpleClassifierInfo', 'sc^U'],
  ['deleteSimpleClassifier', 'sc^D'],

  /*** Dialog Agent Instance APIs ***/
  ['getDialogAgentInstanceAllList', 'dai^R'],
  ['getDialogAgentInstanceByChatbotName', 'dai^R'],
  ['getDialogAgentInstanceInfo', 'dai^R'],
  ['getDialogAgentInstanceResourceList', 'dai^R'],
  ['insertDialogAgentInstanceInfo', 'dai^C'],
  ['updateDialogAgentInstanceInfo', 'dai^U'],
  ['deleteDialogAgentInstance', 'dai^D'],
  ['stopDialogAgentInstance', 'dai^X'],
  ['startDialogAgentInstance', 'dai^X'],
  ['restartDialogAgentInstance', 'dai^X'],

  /*** Intent Finder APIs ***/
  ['getIntentFinderAllList', 'itf^R'],
  ['getIntentFinderInfo', 'itf^R'],
  ['insertIntentFinderInfo', 'itf^C'],
  ['updateIntentFinderInfo', 'itf^U'],
  ['deleteIntentFinder', 'itf^D'],

  /*** Authentication Policy APIs ***/
  ['getAuthticationPolicyAllList', 'aupl^R'],

  /*** Supervisord APIs ***/
  ['getSupervisordServerGroupList', 'svd^R'],
  ['startSupervisordProcess', 'svd^X'],
  ['stopSupervisordProcess', 'svd^X'],

  /*** Facade APIs ***/
  ['getFacadeChatbots', 'fcd^R'],
  ['openDialogService', 'fcd^R'],
  ['sendSimpleTextTalk', 'fcd^R'],
  ['sendTalkAnalyze', 'fcd^R'],
  ['closeDialogService', 'fcd^R'],

  /*** TA Classifier APIs ***/
  ['findClassifierResolverModel', 'ta^R'],
  ['getClassifierResolverModels', 'ta^R'],
  ['getClassifierResolverServers', 'ta^R'],
  ['getClassifierResolverModelCategories', 'ta^R'],

  /*** STT Model APIs ***/
  ['getSTTModels', 'stt^R'],
];


module.exports = {
  roleList,
  grpcAcls
};
