'use strict';
const path = require('path');

module.exports = function enableAuthentication(app) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  app.devlog(loggerName, 'authentication launch...');
  // enable authentication
  app.enableAuth();
};

/*
// renew 이전 소스
module.exports = function enableAuthentication(app, cb) {
  app.devlog('authentication launch...');
  // enable authentication
  // app.enableAuth();

  const restApiRoot = app.get('restApiRoot');
  const parseOriginPath = (allowContainer, currPath)=>{
    const temp = currPath.replace(`/${allowContainer}/`,'');
    const res = temp.indexOf('/') > 0 ? temp.substr(0, temp.indexOf('/')) : temp;
    return res;
  };

  class BaseResponse {
    constructor(){
      this.result = false;
      this.data = {};
      this.err = {};
    }
  }

  const validator = {
    container: {
      run: (currPath)=>{
        let res = new BaseResponse();

        const allowContainers = ['chatbot-img'];
        const allowContainersPaths = ['uploadex','upload','downloadex','download'];

        const allowContainer = allowContainers.find((v)=>(currPath.indexOf(v) > -1));
        const originPath = parseOriginPath(allowContainer, currPath);
        const allowContainerPath = allowContainersPaths.find((v)=>(originPath == v));

        if(!allowContainer || !allowContainerPath) {
          res.err = {
            message: app.messages.CONTAINERS.INVALID_CONTAINER,
            status: 500,
            name: "Error",
            code: "CONTAINERS.INVALID_CONTAINER"
          };
          return res;
        }
        if(allowContainerPath == originPath) {
          res.result = true;
          return res;
        }
      }
    }
  };

  app.remotes().phases.addBefore('auth', 'options-from-request').use((ctx, next)=>{
    const req = ctx.req;
    app.devlog('auth check');
    // app.devlog('auth check', req.baseUrl, req.url, req.accessToken);
    const currPath = req.url.split('?')[0];

    // const LOGIN = '/cm_users/login';
    // const LOGOUT = '/cm_users/logout';
    // if (`${req.baseUrl}${currPath}` == `${restApiRoot}${LOGIN}`
    //   || `${req.baseUrl}${currPath}` == `${restApiRoot}${LOGOUT}`
    // ) {
    //   return next();
    // }
    const allowExcludeUrls = [
      `${restApiRoot}/cm_users/login`,
      `${restApiRoot}/cm_users/logout`
    ];
    if(allowExcludeUrls.includes(`${req.baseUrl}${currPath}`)) {
      return next();
    }

    if(req.headers && req.headers.authorization) {

      // context ctx.req info
      // req.accessToken.id == req.headers.authorization
      // req.baseUrl == `${app.get('restApiRoot')}/${currentRequestModelName}`

    } else {
      app.devlog('auth check 2 no headers.authorization');

      const MODEL_NAME_CONTAINERS = 'containers';
      const allowBaseUrls = [
        `${restApiRoot}/${MODEL_NAME_CONTAINERS}`,
        // `/ping`,
      ];
      if(!allowBaseUrls.includes(req.baseUrl)) {
        console.log('before auth options-from-request :: base url check err', app.messages.AUTH.INVALID_ACCESS);
        return next({
          message: app.messages.AUTH.INVALID_ACCESS,
          status: 401,
          name: "Error",
          code: "AUTH.INVALID_ACCESS"
        });
      }
  
      let res = new BaseResponse();
      switch(req.baseUrl){
        case `${restApiRoot}/${MODEL_NAME_CONTAINERS}`:
          res = validator.container.run(currPath);
          break;
        default:
          console.log('before auth options-from-request :: base url check err repeat', app.messages.AUTH.INVALID_ACCESS);
          res.result = false;
          res.err = {
            message: app.messages.AUTH.INVALID_ACCESS,
            status: 401,
            name: "Error",
            code: "AUTH.INVALID_ACCESS"
          };
          break;
      }
      if(res.result) {
        return next();
      } else {
        return next(res.err);
      }
    }

    if (!req.accessToken) {
      return next({
        message: app.messages.AUTH.USER_NOT_LOGIN,
        status: 401,
        name: "Error",
        code: "AUTH.USER_NOT_LOGIN"
      });
    }
    const User = app.models.cm_user;
    User.findById(req.accessToken.userId, (err, user)=>{
      app.devlog('user found!', user);
      if (err) {
          return next(err);
      }
      if (!user) {
        return next({
            message: app.messages.AUTH.USER_CORRUPTED,
            status: 401,
            name: "Error",
            code: "AUTH.USER_CORRUPTED"
        });
      }
      if (user.deleted == true) {
        return next({
            message: app.messages.AUTH.USER_DELETED,
            status: 401,
            name: "Error",
            code: "AUTH.USER_DELETED"
        });
      }
      ctx.args.options = ctx.args.options ? ctx.args.options : {};
      ctx.args.options.currentUser = user;
      ctx.req.currentUser = {};
      ctx.req.currentUser = user;
      next();
    });
  });
  cb();
};
*/
