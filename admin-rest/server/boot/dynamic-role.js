'use strict';
const path = require('path');

/**
 ACL에서 사용할 dynmaic role을 정의합니다. static role은 사용하지 않습니다.

 - Built-in dynamic roles
 Role.OWNER             $owner              Owner of the object
 Role.AUTHENTICATED     $authenticated      authenticated user
 Role.UNAUTHENTICATED   $unauthenticated    Unauthenticated user
 Role.EVERYONE          $everyone           Everyone

 - 추가된 dynamic roles
 Role.ADMIN             ADMIN               시스템관리자
 Role.SERVICE_ADMIN     SERVICE_ADMIN       서비스관리자 (소속 Workspace 내의 권한 체크)
 Role.OPERATOR          OPERATOR            운영관리자 (소속 Workspace & Team 내의 권한 체크)
 Role.CS_AGENT          CS_AGENT            Chatbot 담당자 (소속 Workspace & Team 내의 권한 체크)
 **/

module.exports = function(app) {
    const utils = app.utils;
    const models = app.models;
    const Role = models.Role;

    const loggerName = path.basename(__filename, path.extname(__filename));

    app.devlog(loggerName, 'dynamic role launch...');

    Role.ADMIN = 'ADMIN';
    Role.registerResolver(Role.ADMIN, adminResolver);

    Role.userRoles = [
        Role.SERVICE_ADMIN = 'SERVICE_ADMIN',
        Role.OPERATOR = 'OPERATOR',
        Role.CS_AGENT = 'CS_AGENT',
    ];
    Role.userRoles.forEach(role => Role.registerResolver(role, roleResolver));

    function adminResolver(ROLE, ctx, next) {

        // do not allow anonymous users
        let userId = ctx.accessToken.userId;
        if (!userId) {
            return reject(next);
        }

        utils.getCurrentUser().then(user => {
            // no user found
            if (!user) {
                return reject(next);
            }

            let userData = (user.__data) ? user.__data : undefined;

            // check for the role
            let userRoleId = (userData && userData.role_id) ? userData.role_id : undefined;
            if (!userRoleId || userRoleId !== ROLE) {
                return reject(next);
            }

            next(null, true);
        });
    }

    function roleResolver(ROLE, ctx, next) {

        // do not allow anonymous users
        let userId = ctx.accessToken.userId;
        if (!userId) {
            return reject(next);
        }

        utils.getCurrentUser().then(user => {
            // no user found
            if (!user) {
                return reject(next);
            }

            let userData = (user.__data) ? user.__data : undefined;
            let userTeamData = (userData && userData.team.__data) ? userData.team.__data : undefined;
            let userWorkspaceRelData = (userTeamData && userTeamData.workspaceRel[0].__data) ? userTeamData.workspaceRel[0].__data : undefined;

            // check for the role
            let userRoleId = (userData && userData.role_id) ? userData.role_id : undefined;
            if (!userRoleId || userRoleId !== ROLE) {
                return reject(next);
            }

            if (ROLE === Role.OPERATOR || ROLE === Role.CS_AGENT) {
                // OPERATOR & CS_AGENT can only write self
                if (ctx.accessType !== 'READ' && ctx.modelName === 'cm_user'
                              && ctx.method !== 'logout' && ctx.method !== 'login'
                              && ctx.method !== 'initPassword') {
                    if (ctx.remotingContext.args.where.user_id !== userId) {
                        return reject(next);
                    }
                }
            }

            // ---- ids extraction from args -----
            let paramIdsArr = initIdsArr();
            extractIds(ctx.remotingContext.args, ctx.accessType, ctx.modelName, paramIdsArr);
            let paramIds = paramIdsArrToObj(paramIdsArr);
            setModelIdToParamIds(ctx.model, ctx.modelId, paramIds);

            if (paramIds.rc_id && !paramIds.conf_id && !paramIds.bot_id) {
                // chatbot-button case
                const filter = {
                    where: {
                        or: [
                            { greeting_msg_btn_id : paramIds.rc_id },
                            { unknown_msg_btn_id : paramIds.rc_id },
                            { notice1_img_id : paramIds.rc_id },
                            { notice2_img_id : paramIds.rc_id },
                            { notice3_img_id : paramIds.rc_id },
                            { notice4_img_id : paramIds.rc_id }
                        ]
                    }
                };

                models.chatbot_config.find(filter, (err, data) => {
                    if (data && data.length > 0){
                        if (data[0].conf_id) {
                            paramIds.conf_id = data[0].conf_id;
                            validation(paramIds, userWorkspaceRelData, ROLE, next);
                        } else {
                            return reject(next);
                        }
                    } else {
                        // no data
                        return reject(next);
                    }
                });

            } else if (paramIds.domain_id && ctx.model === models.cm_domain && !paramIds.team_id) {
                // Q&A domain case
                const filter = {
                    where: {
                        domain_id : paramIds.domain_id
                    }
                };

                models.cm_domain.find(filter, (err, data) => {
                    if (data && data.length > 0){
                        if (data[0].team_id) {
                            paramIds.team_id = data[0].team_id;
                            validation(paramIds, userWorkspaceRelData, ROLE, next);
                        } else {
                            return reject(next);
                        }
                    } else {
                        // no data
                        return reject(next);
                    }
                });
            } else if (paramIds.scf_id && !paramIds.bot_id) {
                // cf_simple case
                const filter = {
                    where: {
                        scf_id : paramIds.scf_id
                    }
                };

                models.cf_simple.find(filter, (err, data) => {
                    if (data && data.length > 0){
                        if (data[0].bot_id) {
                            paramIds.bot_id = data[0].bot_id;
                            validation(paramIds, userWorkspaceRelData, ROLE, next);
                        } else {
                            return reject(next);
                        }
                    } else {
                        // no data
                        return reject(next);
                    }
                });
            } else if (paramIds.preitf_id && !paramIds.bot_id) {
                // cf_simple case
                const filter = {
                    where: {
                        preitf_id : paramIds.preitf_id
                    }
                };

                models.itf_prebqa.find(filter, (err, data) => {
                    if (data && data.length > 0){
                        if (data[0].bot_id) {
                            paramIds.bot_id = data[0].bot_id;
                            validation(paramIds, userWorkspaceRelData, ROLE, next);
                        } else {
                            return reject(next);
                        }
                    } else {
                        // no data
                        return reject(next);
                    }
                });
            } else {
                validation(paramIds, userWorkspaceRelData, ROLE, next);
            }
        });
    }

    /* start of functions */
    function reject(next) {
        process.nextTick(function() {
            next(null, false);
        });
    }

    function initIdsArr() {
        let arr = [];
        const idTypes = ['workspace', 'team', 'bot', 'conf', 'rc', 'domain', 'scf', 'preitf'];
        idTypes.forEach(function(type) {
            let id = {};
            id['type'] = type;
            id['value'] = undefined;
            arr.push(id);
        });
        return arr;
    }

    function getIdFromParams(obj, type) {
        let id = undefined;
        let newId = obj[type+'_id'];

        if (newId !== undefined && newId != null && typeof newId !== 'object' && newId !== 0) {
            id = !isNaN(parseInt(newId)) ? parseInt(newId) : id;
        }
        return id;
    }

    function getIdsFromParams(obj, paramIdsArr) {
        for (let i=0; i < paramIdsArr.length; i++) {
            let id = paramIdsArr[i];
            let type = id.type;

            // 이미 id값이 있으면 대체불가
            if (id.value === undefined) {
                id.value = getIdFromParams(obj, type);
            }
        }
    }

    function getIdsFromAndParams(objs, paramIdsArr) {
        for (let i=0; i < objs.length; i++) {
            let obj = objs[i];
            getIdsFromParams(obj, paramIdsArr);
        }
    }

    function searchParamsInWhere(obj, paramIdsArr) {
        if (obj.where) {
            let where = obj.where;
            getIdsFromParams(where, paramIdsArr);

            if (where.and){
                let whereAnd = where.and;
                getIdsFromAndParams(whereAnd, paramIdsArr);
            }
        }
    }

    function getIdsFromObj(obj, paramIdsArr) {
        if(obj) {
            getIdsFromParams(obj, paramIdsArr);
            searchParamsInWhere(obj, paramIdsArr);
        }
    }

    function extractIds(args, accessType, modelName, paramIdsArr) {
        if (args) {
            getIdsFromObj(args, paramIdsArr);            // Query String
            getIdsFromObj(args.filter, paramIdsArr);     // filter
            getIdsFromObj(args.data, paramIdsArr);       // data

            // team 추가 또는 수정시
            if (accessType === "WRITE" && modelName === "cm_team") {
                if (args.data && args.data.workspaces) {
                    let workspaces = data.workspaces;
                    if (workspaces.length > 0) {
                        for (let i=0; i < paramIds.length; i++) {
                            if (paramIdsArr[i].type === 'workspace') {
                                paramIdsArr[i].value = getIdFromParams(workspaces[0], paramIdsArr[i].type);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    function paramIdsArrToObj(paramIdsArr) {
        let obj = {};
        paramIdsArr.forEach(function(id) {
            obj[id.type+'_id'] = id.value;
        });
        return obj;
    }

    function setModelIdToParamIds(model, modelId, paramIds) {
        // instance approach - findById
        if (modelId) {
            switch (model) {
                case models.cm_workspace:
                    paramIds.workspace_id = modelId;
                    break;
                case models.cm_team:
                    paramIds.team_id = modelId;
                    break;
                case models.chatbot_info:
                    paramIds.bot_id = modelId;
                    break;
                case models.chatbot_config:
                    paramIds.conf_id = modelId;
                    break;
                case models.cm_domain:
                    paramIds.domain_id = modelId;
                    break;
                case models.cf_simple:
                    paramIds.scf_id = modelId;
                    break;
                case models.itf_prebqa:
                    paramIds.preitf_id = modelId;
                    break;
            }
        }
    }

    function validation(paramIds, userWorkspaceRelData, ROLE, next) {
        models.cm_workspace_team.getRelationData(paramIds.workspace_id, paramIds.team_id, paramIds.bot_id, paramIds.conf_id, (err, data) => {
            let pWorkspaceId = paramIds.workspace_id;
            let pTeamId = paramIds.team_id;
            let pTeamIds = [];

            if (data) {
                if (!pWorkspaceId) {
                    let temp = data[0].workspace_id;
                    if (temp && temp != null && temp !== undefined && temp !== 0) {
                        pWorkspaceId = temp;
                    }
                }
                if (!pTeamId) {
                    for (let i=0; i<data.length; i++) {
                        let temp = data[i].team_id;
                        if (temp && temp != null && temp !== undefined && temp !== 0) {
                            pTeamIds.push(temp);
                        }
                    }
                }
            }

            // get user workspace & team ids
            let userWorkspaceId = undefined;
            let userTeamId = undefined;
            if (userWorkspaceRelData) {
                userWorkspaceId = userWorkspaceRelData.workspace_id;
                userTeamId = userWorkspaceRelData.team_id;
            }

            switch (ROLE) {
                case Role.SERVICE_ADMIN:
                    // check for workspace
                    if (!pWorkspaceId || !userWorkspaceId || userWorkspaceId !== pWorkspaceId) {
                        return reject(next);
                    }
                    break;

                case Role.CS_AGENT:
                case Role.OPERATOR:
                    // check for workspace
                    if (!pWorkspaceId || !userWorkspaceId || userWorkspaceId !== pWorkspaceId) {
                        return reject(next);
                    }
                    // check for team
                    if (!pTeamId || !userTeamId || userTeamId !== pTeamId) {
                        if (pTeamIds.length === 0 || !userTeamId || !pTeamIds.includes(userTeamId)) {
                            return reject(next);
                        }
                    }
                    break;

                default:
                    return reject(next);
            }
            next(null, true);
        });
    }
    /* end of functions */
};
