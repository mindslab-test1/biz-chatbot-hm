'use strict';
const path = require('path');

module.exports = function(app) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  app.devlog(loggerName, 'root launch...');
  // Install a `/` route that returns server status
  let router = app.loopback.Router();
  router.get('/', app.loopback.status());
  app.use(router);
};
