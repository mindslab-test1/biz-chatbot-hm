var path = require('path');
var loopback = require('loopback');
var boot = require('loopback-boot');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var app = module.exports = loopback();

app.autoMigration = true;

var bootOptions = {
  appRootDir: path.resolve(__dirname, '../')
};

function main() {
  var dataSource = app.dataSources.tutor;

  var modelNames = Object.keys(app.models);
  var models = [];

  modelNames.forEach(function(m) {
    var modelName = app.models[m].modelName;
    if (models.indexOf(modelName) === -1) {
      var model = app.models[m];
      if (model.dataSource === dataSource) {
        models.push(modelName);
      } else {
        console.log('Skipping model', modelName, 'which uses');
      }
    }
  });

  console.log('Models: ', models);

  var modelCount = models.length;
  console.log('Model Count', modelCount);

  models.forEach(function(m) {
    dataSource.automigrate(m, function(err) {
      if (err) {
        throw err;
      }
      console.log('Model created :', m);
      modelCount--;
      if (modelCount === 0) {
        console.log('Auto migration done');

        dataSource.disconnect();
        // eslint-disable-next-line no-process-exit
        // process.exit();
      }
    });
  });
}

boot(app, bootOptions, function(err) {
  if (err) {
    throw err;
  }
  if (require.main === module) {
    main();
  }
});

module.exports = app;
exports = module.exports; // eslint-disable-line
