module.exports = function(Model) {
  Reflect.defineProperty(Model, 'created_at', {type: Date, fixed: '$now'});
  Reflect.defineProperty(Model, 'modified_at', {type: Date, fixed: '$now'});
};
