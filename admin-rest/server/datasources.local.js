

'use strict';

let dataSource = {
  "mindslab": {
    "name": "mindslab",
    "connector": "mysql",
    "hostOrigin": "localhost",
    "host": "192.168.1.18",
    "port": 3306,
    "database": "chatbot",
    "user": "maum",
    "password": "ggoggoma"
  },
  "storage": {
    "name": "storage",
    "connector": "loopback-component-storage",
    "provider": "filesystem",
    "root": `${process.env.CHATBOT_ROOT}/storage`,
    "maxFileSize": "5242880"
  },
  "statistic": {
    "name": "mindslab",
    "connector": "mysql",
    "host": "192.168.1.18",
    "port": 3306,
    "database": "chatbot",
    "user": "maum",
    "password": "ggoggoma"
  },
  "etridialog": {
    "host": "",
    "port": 3306,
    "url": "mysql://maum:ggoggoma@192.168.1.18/etridialog",
    "database": "",
    "password": "",
    "name": "etridialog",
    "user": "",
    "connector": "mysql"
  },
}
module.exports = {
  mindslab: dataSource.mindslab,
  storage: dataSource.storage,
  statistic: dataSource.statistic,
  etridialog: dataSource.etridialog

};
