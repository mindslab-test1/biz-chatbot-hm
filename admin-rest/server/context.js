// const lctx = require('loopback-context');
const app = require('./server');

/*
 * set current user in context
 * set middleware.json
 */
module.exports = function authAfter() {
  return (req, res, next) => {
    // app.devlog('context req.accessToken ====\n', req.accessToken, '\n================');
    const user_id = req.accessToken && req.accessToken.userId || null;
    // app.devlog('context user_id: ', user_id);

    if (user_id === null) {
      return next();
    } else {
      app.models.cm_user.findById(user_id)
        .then(user => {
          // let cctx = lctx.getCurrentContext({bind: true});
          // if (!cctx) {
          //   console.error(new Error('Cannot get current context! (0)'));
          // } else {
          //   cctx.set('user', user);
          //   cctx.set('userId', user.id);
          // }
          /** trick **/
          // app.userId = user.user_id;
          app.user = user;
          next();
        })
        .catch(() => {
          // let cctx = lctx.getCurrentContext({bind: true});
          // if (!cctx) {
          //   console.error(new Error('Cannot get current context! (1)'));
          // } else {
          //   cctx.set('user', null);
          //   cctx.set('userId', null);
          // }
          /** trick **/
          // app.userId = null;
          app.user = null;
          next();
        });
    }
  };
};

// utilities
app.utils = {
  getCurrentUserId() {
    if (typeof this.dummyUser !== 'undefined') {
      return Promise.resolve(this.dummyUser && this.dummyUser.id || null);
    }

    /** trick **/
    return Promise.resolve(app.userId || null);

    // let cctx = lctx.getCurrentContext({bind: true});
    // if (!cctx) {
    //   console.error(new Error('Cannot get current context! (2)'));
    // }
    // let userId = cctx && cctx.get('userId') || null;
    // return Promise.resolve(userId);
  },
  getCurrentUser() { // Promise support
    if (typeof this.dummyUser !== 'undefined') {
      return Promise.resolve(this.dummyUser);
    }

    /** trick **/
    return Promise.resolve(app.user || null);

    // let cctx = lctx.getCurrentContext({bind: true});
    // if (!cctx) {
    //   console.error(new Error('Cannot get current context! (3)'));
    // }
    // let user = cctx && cctx.get('user') || null;
    // return Promise.resolve(user);
  },
  setDummyUser(user) {
    this.dummyUser = user;
  },
  unsetDummyUser() {
    delete this.dummyUser;
  },
};
