'use strict';
const app = require('../server');
const path = require('path');

module.exports = function (srch_morpheme) {
    const loggerName = path.basename(__filename, path.extname(__filename));

    srch_morpheme.getList = function ( skip, limit, order, text, next) {
        const connector = srch_morpheme.app.datasources.mindslab.connector;
        let sql = '';

        if (text && text != undefined && text != "") {

            sql =
                `SELECT 
                    MORPH_NO,
                    KEYWORD,
                    MORPH_TAG,
                    MEAN_CLASSIFICATION,
                    LAST_MORPH,
                    STATUS,
                    TYPE,
                    MORPH_INDEX_INFO
                FROM
                        srch_morpheme_tb
                WHERE
                    keyword like '%${text}%' 
                ORDER BY ${order}
                LIMIT ${limit} 
                OFFSET ${skip}`

        } else {

            sql =
                `SELECT 
                    MORPH_NO,
                    KEYWORD,
                    MORPH_TAG,
                    MEAN_CLASSIFICATION,
                    LAST_MORPH,
                    STATUS,
                    TYPE,
                    MORPH_INDEX_INFO
                FROM
                        srch_morpheme_tb
                ORDER BY ${order}
                LIMIT ${limit} 
                OFFSET ${skip}`

        }
        connector.query(sql, null, (err, resultObjects) => {
            if (err) next(err);

            // console.log(resultObjects);
            app.devlog(loggerName, resultObjects);
            next(null, resultObjects);
        });
    };

    srch_morpheme.remoteMethod(
        'getList', {
            isStatic: true,
             http: {path: '/getList', verb: 'get'},
            accepts: [
                { arg: 'skip', type: 'number', required: true },
                { arg: 'limit', type: 'number', required: true },
                { arg: 'order', type: 'string', required: true },
                { arg: 'text', type: 'string' },
            ],
            returns: { root: true },
        }
    );


    srch_morpheme.remoteMethod(
        'countList', {
            isStatic: true,
            http: {path: '/countList', verb: 'get'},
            accepts: [
                { arg: 'text', type: 'string' },
            ],
            returns: { root: true },
        }
    );



    srch_morpheme.countList = function ( text, next) {
        const connector = srch_morpheme.app.datasources.mindslab.connector;
        let sql = '';
        if (text && text != undefined && text != "") {
            sql =
                `SELECT count(*) as cnt
              FROM
                      srch_morpheme_tb
              WHERE
                  keyword like '%${text}%'`


        } else {
            sql =
                `SELECT count(*) as cnt
                FROM
                        srch_morpheme_tb`
        }
        connector.query(sql, null, (err, resultObjects) => {

            if (err) {
                next(err);
            }
            next(null, resultObjects);
        });
    };


    // delete By Ids
    srch_morpheme.deleteByIds = function(ids = [] , next) {
        return Promise.all(
            ids.map(id => srch_morpheme.destroyById(id).catch(() => ({count: 0})))
        )
            .then(result => {
                let deletedIds = ids.filter((id, index) => result[index].count !== 0);
                return deletedIds;
            });
    };

    srch_morpheme.remoteMethod(
        'deleteByIds', {
            accepts: [
                {arg: 'ids', type: ['string'], required: true},
            ],
            returns: {root: true},
            isStatic: true
        }
    );

    srch_morpheme.remoteMethod(
        'apply', {
            http: {
                path: '/morphemeApply',
                verb: 'get'
            },
            returns: { args: 'body', root: true}
        }
    );

    srch_morpheme.apply = function(next) {
        const XMLHttpRequest = require("xmlhttprequest-ssl").XMLHttpRequest;
        const xhr = new XMLHttpRequest();

        xhr.open('GET', 'http://10.10.156.74:9400/dic/make/morpheme', false);
        xhr.send();

        next(null, JSON.parse(xhr.responseText));
    }

};
