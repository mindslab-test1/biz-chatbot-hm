'use strict';
const app = require('../server');
const path = require('path');

module.exports = function (cm_synonym) {
    const loggerName = path.basename(__filename, path.extname(__filename));

    cm_synonym.getList = function (type, skip, limit, order, text, next) {
        const connector = cm_synonym.app.datasources.mindslab.connector;
        let sql = '';

        if (text && text != undefined && text != "") {

            sql =
                `SELECT *
                FROM
                    nlp_synonym_tb
                WHERE
                    type = ${type}
                    AND (word like '%${text}%' or synonym_list like '%${text}%')
                ORDER BY ${order}
                LIMIT ${limit} 
                OFFSET ${skip}`;

        } else {

            sql =
                `SELECT *
                FROM
                    nlp_synonym_tb
                WHERE
                    type = ${type}
                ORDER BY ${order}
                LIMIT ${limit} 
                OFFSET ${skip}`

        }
        connector.query(sql, null, (err, resultObjects) => {
            if (err) next(err);

            // console.log(resultObjects);
            app.devlog(loggerName, resultObjects);
            next(null, resultObjects);
        });
    };

    cm_synonym.remoteMethod(
        'getList', {
            isStatic: true,
             http: {path: '/getList', verb: 'get'},
            accepts: [
                { arg: 'type', type: 'number', required: true },
                { arg: 'skip', type: 'number', required: true },
                { arg: 'limit', type: 'number', required: true },
                { arg: 'order', type: 'string', required: true },
                { arg: 'text', type: 'string' },
            ],
            returns: { root: true },
        }
    );


    cm_synonym.remoteMethod(
        'countList', {
            isStatic: true,
            http: {path: '/countList', verb: 'get'},
            accepts: [
                { arg: 'type', type: 'number', required: true },
                { arg: 'text', type: 'string' },
            ],
            returns: { root: true },
        }
    );



    cm_synonym.countList = function (type, text, next) {
        const connector = cm_synonym.app.datasources.mindslab.connector;
        let sql = '';
        if (text && text != undefined && text != "") {
            sql =
                `SELECT count(*) as cnt
                FROM
                  nlp_synonym_tb
                WHERE
                  type = ${type}
                  AND (word like '%${text}%' or synonym_list like '%${text}%')`


        } else {
            sql =
                `SELECT count(*) as cnt
                FROM
                    nlp_synonym_tb
                WHERE
                    type = ${type}`
        }
        connector.query(sql, null, (err, resultObjects) => {

            if (err) {
                next(err);
            }
            next(null, resultObjects);
        });
    };


    // delete By Ids
    cm_synonym.deleteByIds = function(ids = [] , next) {
        return Promise.all(
            ids.map(id => cm_synonym.destroyById(id).catch(() => ({count: 0})))
        )
            .then(result => {
                let deletedIds = ids.filter((id, index) => result[index].count !== 0);
                return deletedIds;
            });
    };


    cm_synonym.remoteMethod(
        'deleteByIds', {
            accepts: [
                {arg: 'ids', type: ['string'], required: true},
                {arg: 'type', type: 'number', required: false},
            ],
            returns: {root: true},
            isStatic: true
        }
    );

    // cm_synonym.remoteMethod(
    //     'apply', {
    //         http: {
    //             path: '/synonymApply',
    //             verb: 'get'
    //         },
    //         returns: { args: 'body', root: true}
    //     }
    // );
    //
    // cm_synonym.apply = function(next) {
    //     const XMLHttpRequest = require("xmlhttprequest-ssl").XMLHttpRequest;
    //     const xhr = new XMLHttpRequest();
    //
    //     xhr.open('GET', 'http://10.10.156.74:9400/dic/make/synonyms', false);
    //     xhr.send();
    //
    //     next(null, JSON.parse(xhr.responseText));
    // }

};
