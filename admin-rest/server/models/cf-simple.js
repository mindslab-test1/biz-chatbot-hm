'use strict';
const app = require('../server');
const path = require('path');

module.exports = function(Cfsimple) {
    const loggerName = path.basename(__filename, path.extname(__filename));


    Cfsimple.deleteByBotId = function (bot_id, next) {

        const connector = Cfsimple.app.datasources.mindslab.connector;
        let sql =
          `DELETE 
            FROM
                cf_simple_tb
            WHERE
                bot_Id = '${bot_id}'`
    
        connector.query(sql, null, (err, resultObjects) => {
          if (err) next(err);
          next(null, resultObjects);
        });
      };
    
      Cfsimple.remoteMethod(
        'deleteByBotId', {
          isStatic: true,
          accepts: [
            {arg: 'bot_id', type: 'number', required: true}
          ],
          returns: {root: true},
        }
      );


      Cfsimple.deleteByDomainId = function (domain_id, bot_id, next) {

        const connector = Cfsimple.app.datasources.mindslab.connector;
        let sql =
          `DELETE 
            FROM
                cf_simple_tb
            WHERE
                domain_id = '${domain_id}' AND  bot_id = '${bot_id}'`
    
        connector.query(sql, null, (err, resultObjects) => {
          if (err) next(err);
          next(null, resultObjects);
        });
      };
    
      Cfsimple.remoteMethod(
        'deleteByDomainId', {
          isStatic: true,
          accepts: [
            {arg: 'domain_id', type: 'number', required: true},
            {arg: 'bot_id', type: 'number', required: true}
          ],
          returns: {root: true},
        }
      );
};
