'use strict';
const app = require('../server');
const path = require('path');

module.exports = function (itf_corpus_category) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  var Transaction = require('loopback-datasource-juggler').Transaction;


  itf_corpus_category.deleteByCorpus = function (data, next) {

    var count = -1;

    itf_corpus_category.beginTransaction({
      isolationLevel: Transaction.READ_COMMITTED,
      timeout: 30000 // 30000ms = 30s
    }, function (err, tx) {

      itf_corpus_category.count({ bot_id: data.bot_id, sds_domain: data.sds_domain, intent_name: data.intent_name }, { transaction: tx }, function (err, res) {
        if (err) {
          next(err);
          tx.rollback();
        } else {
          count = res;
          // console.log("itf_corpus_category count =", res);
          app.devlog(loggerName, "itf_corpus_category count =", res);
        }
      });

      // console.log("========================================================================================");
      // console.log("data =", data);
      // console.log("========================================================================================");
      app.devlog(loggerName, "========================================================================================");
      app.devlog(loggerName, "data =", data);
      app.devlog(loggerName, "========================================================================================");

      /* API insert */
      itf_corpus_category.deleteById(data.cor_id, { transaction: tx }, function (err, res) {
        if (err) {
          next(err);
          tx.rollback();
        } else {
          console.log(res);

          if (count == 1) {
            app.models.itf_intent_domain_rel.destroyAll({ bot_id: data.bot_id, sds_domain: data.sds_domain, intent_name: data.intent_name }, { transaction: tx }, function (err, res) {
              if (err) {
                next(err);
                tx.rollback();
              } else {
                next(null, res);
                tx.commit();
              }
            });
          } else {
            next(null, res);
            tx.commit();
          }
        }
      }); //findById

    }); // beginTransaction
  }; // deleteByCorpus

  itf_corpus_category.remoteMethod(
    'deleteByCorpus', {
      accepts: [
        { arg: 'data', type: 'object', http: { source : 'body' }, required: true }
        // {arg: 'ifApi', type: ['object'], required: true},
        // {arg: 'arr', type: ['array'] }
      ],
      returns: { root: true },
      isStatic: true
    });
  /*================================================================================================================================================== */

  itf_corpus_category.createCorpus = function (data, next) {

    var count = -1;
    itf_corpus_category.beginTransaction({
      isolationLevel: Transaction.READ_COMMITTED,
      timeout: 30000 // 30000ms = 30s
    }, function (err, tx) {
      // console.log("========================================================================================");
      // console.log("data =", data);
      // console.log("========================================================================================");
      app.devlog(loggerName, "========================================================================================");
      app.devlog(loggerName, "data =", data);
      app.devlog(loggerName, "========================================================================================");

      /* itf insert */
      itf_corpus_category.count({ bot_id: data.bot_id, sds_domain: data.sds_domain, intent_name: data.intent_name }, { transaction: tx }, function (err, res) {
        if (err) {
          next(err);
          tx.rollback();
        } else {
          count = res;
          // console.log("========================================================================================");
          // console.log("count =", res);
          // console.log("========================================================================================");
          app.devlog(loggerName, "========================================================================================");
          app.devlog(loggerName, "count =", res);
          app.devlog(loggerName, "========================================================================================");
        }
      });

      itf_corpus_category.upsert(data, { transaction: tx }, function (err, res) {
        if (err) {
          next(err);
          tx.rollback();

        } else {
          // console.log("========================================================================================");
          // console.log("itf_corpus_category =", res);
          // console.log("========================================================================================");
          app.devlog(loggerName, "========================================================================================");
          app.devlog(loggerName, "itf_corpus_category =", res);
          app.devlog(loggerName, "========================================================================================");


          if (count == 0) {
            app.models.itf_intent_domain_rel.create(res, { transaction: tx }, function (err, res) {
              if (err) {
                next(err);
                tx.rollback();
              } else {
                // console.log("========================================================================================");
                // console.log("itf_intent_domain_rel =", res);
                // console.log("========================================================================================");
                app.devlog(loggerName, "========================================================================================");
                app.devlog(loggerName, "itf_intent_domain_rel =", res);
                app.devlog(loggerName, "========================================================================================");

                app.models.itf_sds.count({ bot_id: data.bot_id, project_name: "INTENT_FINDER_" + data.bot_id }, { transaction: tx }, function (err, res) {
                  if(err){
                    next(err);
                    tx.rollback();
                  }else{
                    if(res && res > 0){
                      // console.log("========================================================================================");
                      // console.log("itf_sds count=", res);
                      // console.log("========================================================================================");
                      app.devlog(loggerName, "========================================================================================");
                      app.devlog(loggerName, "itf_sds count=", res);
                      app.devlog(loggerName, "========================================================================================");

                      next(null,res);
                      tx.commit();
                    }else{
                      var itf_sds = new Object();
                      itf_sds.bot_id = data.bot_id;
                      itf_sds.project_name = "INTENT_FINDER_" + data.bot_id;
                      itf_sds.itf_order = 1;
                      itf_sds.creator_id = data.creator_id;
                      itf_sds.updator_id = data.updator_id;
                      app.models.itf_sds.upsert(itf_sds, { transaction: tx }, function (err, res) {
                        if(err){
                          next(err);
                          tx.rollback();
                        }else{
                          // console.log("========================================================================================");
                          // console.log("itf_sds =", res);
                          // console.log("========================================================================================");
                          app.devlog(loggerName, "========================================================================================");
                          app.devlog(loggerName, "itf_sds =", res);
                          app.devlog(loggerName, "========================================================================================");
                          next(null,res);
                          tx.commit();
                        }
                      });
                    }
                  }
                });
               
              }
            }); //itf_intent_domain_rel
          } else {
            next(null, res);
            tx.commit();
          }
        }
      }); //findById
    }); // beginTransaction
  }; // deleteByCorpus


  itf_corpus_category.remoteMethod(
    'createCorpus', {
      accepts: [
        { arg: 'data', type: 'object', http: { source : 'body' }, required: true }
      ],
      returns: { root: true },
      isStatic: true
    });

  /*================================================================================================================================================== */

  itf_corpus_category.deleteByBotId = function (bot_id, next) {

    var count = -1;

    itf_corpus_category.beginTransaction({
      isolationLevel: Transaction.READ_COMMITTED,
      timeout: 30000 // 30000ms = 30s
    }, function (err, tx) {



      // console.log("========================================================================================");
      // console.log("bot_id =", bot_id);
      // console.log("========================================================================================");
      app.devlog(loggerName, "========================================================================================");
      app.devlog(loggerName, "bot_id =", bot_id);
      app.devlog(loggerName, "========================================================================================");

      /* API insert */
      itf_corpus_category.destroyAll({ bot_id: bot_id }, { transaction: tx }, function (err, res) {
        if (err) {
          next(err);
          tx.rollback();
        } else {
          // console.log(res);
          app.devlog(loggerName, res);

          if (count == 1) {
            app.models.itf_intent_domain_rel.destroyAll({ bot_id: bot_id }, { transaction: tx }, function (err, res) {
              if (err) {
                next(err);
                tx.rollback();
              } else {
                // app.models.itf_sds.destroyAll({ bot_id: bot_id }, { transaction: tx }, function (err, res) {
                //   if (err) {
                //     next(err);
                //     tx.rollback();
                //   } else {
                //     next(null, res);
                //     tx.commit();
                //   }
                // });
                next(null, res);
                    tx.commit();
              }
            });
          } else {
            next(null, res);
            tx.commit();
          }
        }
      }); //findById

    }); // beginTransaction
  }; // deleteByCorpus

  itf_corpus_category.remoteMethod(
    'deleteByBotId', {
      accepts: [
        { arg: 'bot_id', type: 'number', required: true }
      ],
      returns: { root: true },
      isStatic: true
    });


  /*================================================================================================================================================== */

  itf_corpus_category.deleteByDomainId = function (domain_id, bot_id, next) {


    itf_corpus_category.beginTransaction({
      isolationLevel: Transaction.READ_COMMITTED,
      timeout: 30000 // 30000ms = 30s
    }, function (err, tx) {
      // console.log("========================================================================================");
      // console.log("bot_id =", bot_id);
      // console.log("domain_id =", domain_id);
      // console.log("========================================================================================");
      app.devlog(loggerName, "========================================================================================");
      app.devlog(loggerName, "bot_id =", bot_id);
      app.devlog(loggerName, "domain_id =", domain_id);
      app.devlog(loggerName, "========================================================================================");
      var sds_domain = null;
      var filter = { where: { and: [{ bot_id: bot_id }, { domain_id: domain_id }] } };
      itf_corpus_category.findOne(filter, { transaction: tx }, function (err, res) {
        if (err) {
          next(err);
          tx.rollback();
        } else {
          if (res == null || res.sds_domain == null || res.sds_domain == "undefined" || res.sds_domain == '') {
            next("sds_domain is null");
            tx.rollback();
          } else {
            // console.log("========================================================================================");
            // console.log("itf_corpus_category find =", res);
            // console.log("itf_corpus_category find =", res.sds_domain);
            // console.log("========================================================================================");
            app.devlog(loggerName, "========================================================================================");
            app.devlog(loggerName, "itf_corpus_category find =", res);
            app.devlog(loggerName, "itf_corpus_category find =", res.sds_domain);
            app.devlog(loggerName, "========================================================================================");
            itf_corpus_category.destroyAll({ and: [{ bot_id: bot_id }, { domain_id: domain_id }] }, { transaction: tx }, function (err, res) {
              if (err) {
                next(err);
                tx.rollback();
              } else {
                sds_domain = res.sds_domain;
                // console.log(res);
                // console.log("========================================================================================");
                // console.log("sds_domain =", sds_domain);
                // console.log("========================================================================================");
                app.devlog(loggerName, res);
                app.devlog(loggerName, "========================================================================================");
                app.devlog(loggerName, "sds_domain =", sds_domain);
                app.devlog(loggerName, "========================================================================================");

                app.models.itf_intent_domain_rel.destroyAll({ and: [{ bot_id: bot_id }, { sds_domain: sds_domain }] }, { transaction: tx }, function (err, res) {
                  if (err) {
                    next(err);
                    tx.rollback();
                  } else {
                    next(null,res);
                    tx.commit();
                  }
                });
              }
            }); //findById
          }
        }
      });
    }); // beginTransaction
  }; // deleteByCorpus

  itf_corpus_category.remoteMethod(
    'deleteByDomainId', {
      accepts: [
        { arg: 'domain_id', type: 'number', required: true },
        { arg: 'bot_id', type: 'number', required: true }
      ],
      returns: { root: true },
      isStatic: true
    });


};
