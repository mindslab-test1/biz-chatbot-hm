'use strict';
const app = require('../server');
const path = require('path');

module.exports = function (srch_stopword) {
    const loggerName = path.basename(__filename, path.extname(__filename));

    srch_stopword.getList = function ( skip, limit, order, text, next) {
        const connector = srch_stopword.app.datasources.mindslab.connector;
        let sql = '';

        if (text && text != undefined && text != "") {

            sql =
                `SELECT 
                    STOP_NO,
                    STOPWORD
            FROM
                    srch_stopword_tb
            WHERE
                STOPWORD like '%${text}%'
            ORDER BY ${order}
            LIMIT ${limit} 
            OFFSET ${skip}`;

        } else {

            sql =
                `SELECT
                    STOP_NO,
                    STOPWORD
            FROM
                    srch_stopword_tb
            ORDER BY ${order}
            LIMIT ${limit} 
            OFFSET ${skip}`

        }
        connector.query(sql, null, (err, resultObjects) => {
            if (err) next(err);

            // console.log(resultObjects);
            app.devlog(loggerName, resultObjects);
            next(null, resultObjects);
        });
    };

    srch_stopword.remoteMethod(
        'getList', {
            isStatic: true,
            http: {path: '/getList', verb: 'get'},
            accepts: [
                { arg: 'skip', type: 'number', required: true },
                { arg: 'limit', type: 'number', required: true },
                { arg: 'order', type: 'string', required: true },
                { arg: 'text', type: 'string' },
            ],
            returns: { root: true },
        }
    );


    srch_stopword.remoteMethod(
        'countList', {
            isStatic: true,
            http: {path: '/countList', verb: 'get'},
            accepts: [
                { arg: 'text', type: 'string' }
            ],
            returns: { root: true },
        }
    );
    srch_stopword.countList = function ( text, next) {
        const connector = srch_stopword.app.datasources.mindslab.connector;
        let sql = '';
        if (text && text != undefined && text != "") {
            sql =
                `SELECT count(*) as cnt
              FROM
                      srch_stopword_tb
              WHERE
                  STOPWORD like '%${text}%'`


        } else {
            sql =
                `SELECT count(*) as cnt
                FROM
                        srch_stopword_tb`
        }
        connector.query(sql, null, (err, resultObjects) => {

            if (err) {
                next(err);
            }
            next(null, resultObjects);
        });
    };


    /* srch_stopword.deleteByIds = function(id, next ) {

          srch_stopword.destroyById(id, (error) =>{
            // console.log(error);
            app.devlog(loggerName, error);

            if(error == null){
                 next(null, {count :1});
            }else{
                next(null, {count :0});
            }
          });
      };

      srch_stopword.remoteMethod(
         'deleteByIds', {
           http: {path: '/:id', verb: 'post'},
           accepts: [
             {arg: 'id', type: 'number', required: true}
           ],
           returns: {root: true},
           isStatic: true
         }
       );*/

    // delete By Ids
    srch_stopword.deleteByIds = function(ids = [] , next) {
        return Promise.all(
            ids.map(stop_no => srch_stopword.destroyById(stop_no).catch(() => ({count: 0})))
        )
            .then(result => {
                let deletedIds = ids.filter((stop_no, index) => result[index].count !== 0);
                return deletedIds;
            });
    };

    srch_stopword.remoteMethod(
        'deleteByIds', {
            accepts: [
                {arg: 'ids', type: ['string'], required: true},
            ],
            returns: {root: true},
            isStatic: true
        }
    );

    srch_stopword.remoteMethod(
        'apply', {
            http: {
                path: '/stopwordApply',
                verb: 'get'
            },
            returns: { args: 'body', root: true}
        }
    );

    srch_stopword.apply = function(next) {
        const XMLHttpRequest = require("xmlhttprequest-ssl").XMLHttpRequest;
        const xhr = new XMLHttpRequest();

        xhr.open('GET', 'http://10.10.156.74:9400/dic/make/stopwords', false);
        xhr.send();

        next(null, JSON.parse(xhr.responseText));
    }
};
