'use strict';
const app = require('../server');
const path = require('path');

module.exports = function (srch_ranking) {
    const loggerName = path.basename(__filename, path.extname(__filename));

    srch_ranking.getList = function ( skip, limit, order, text, next) {
        const connector = srch_ranking.app.datasources.mindslab.connector;
        let sql = '';

        if (text && text != undefined && text != "") {

            sql =
                `SELECT 
                    ID,
                    FIELD_NAME,
                    BOOST
            FROM
                    srch_boost_tb
            WHERE
                FIELD_NAME like '%${text}%'
            ORDER BY ${order}
            LIMIT ${limit} 
            OFFSET ${skip}`;

        } else {

            sql =
                `SELECT
                   ID,
                    FIELD_NAME,
                    BOOST
            FROM
                    srch_boost_tb
            ORDER BY ${order}
            LIMIT ${limit} 
            OFFSET ${skip}`

        }
        connector.query(sql, null, (err, resultObjects) => {
            if (err) next(err);

            // console.log(resultObjects);
            app.devlog(loggerName, resultObjects);
            next(null, resultObjects);
        });
    };

    srch_ranking.remoteMethod(
        'getList', {
            isStatic: true,
             http: {path: '/getList', verb: 'get'},
            accepts: [
                { arg: 'skip', type: 'number', required: true },
                { arg: 'limit', type: 'number', required: true },
                { arg: 'order', type: 'string', required: true },
                { arg: 'text', type: 'string' },
            ],
            returns: { root: true },
        }
    );


    srch_ranking.remoteMethod(
        'countList', {
            isStatic: true,
            http: {path: '/countList', verb: 'get'},
            accepts: [
                { arg: 'text', type: 'string' }
            ],
            returns: { root: true },
        }
    );



    srch_ranking.countList = function ( text, next) {
        const connector = srch_ranking.app.datasources.mindslab.connector;
        let sql = '';
        if (text && text != undefined && text != "") {
            sql =
                `SELECT count(*) as cnt
              FROM
                      srch_boost_tb
              WHERE
                  FIELD_NAME like '%${text}%'`


        } else {
            sql =
                `SELECT count(*) as cnt
                FROM
                        srch_boost_tb`
        }
        connector.query(sql, null, (err, resultObjects) => {

            if (err) {
                next(err);
            }
            next(null, resultObjects);
        });
    };

    // delete By Ids
    srch_ranking.deleteByIds = function(ids = [] , next) {
        return Promise.all(
            ids.map(id => srch_ranking.destroyById(id).catch(() => ({count: 0})))
        )
            .then(result => {
                let deletedIds = ids.filter((id, index) => result[index].count !== 0);
                return deletedIds;
            });
    };

    srch_ranking.remoteMethod(
        'deleteByIds', {
            accepts: [
                {arg: 'ids', type: ['string'], required: true},
            ],
            returns: {root: true},
            isStatic: true
        }
    );


};
