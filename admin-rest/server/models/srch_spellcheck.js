'use strict';
const app = require('../server');
const path = require('path');

module.exports = function (srch_spellcheck) {
    const loggerName = path.basename(__filename, path.extname(__filename));

    srch_spellcheck.getList = function (skip, limit, order, text, next) {
        const connector = srch_spellcheck.app.datasources.mindslab.connector;
        let sql = '';

        if (text && text != undefined && text != "") {

            sql =
                `SELECT 
                ID,
                SPELL_KEYWORD,
                TYPE
            FROM
                    srch_spellcheck_tb
            WHERE
                SPELL_KEYWORD like '%${text}%'
            ORDER BY ${order}
            LIMIT ${limit} 
            OFFSET ${skip}`;

        } else {

            sql =
                `SELECT 
                ID,
                SPELL_KEYWORD,
                TYPE
            FROM
                    srch_spellcheck_tb
            ORDER BY ${order}
            LIMIT ${limit} 
            OFFSET ${skip}`

        }
        connector.query(sql, null, (err, resultObjects) => {
            if (err) next(err);

            // console.log(resultObjects);
            app.devlog(loggerName, resultObjects);
            next(null, resultObjects);
        });
    };
    srch_spellcheck.remoteMethod(
        'getList', {
            isStatic: true,
             http: {path: '/getList', verb: 'get'},
            accepts: [
                { arg: 'skip', type: 'number', required: true },
                { arg: 'limit', type: 'number', required: true },
                { arg: 'order', type: 'string', required: true },
                { arg: 'text', type: 'string' },
            ],
            returns: { root: true },
        }
    );


    srch_spellcheck.remoteMethod(
        'countList', {
            isStatic: true,
            http: {path: '/countList', verb: 'get'},
            accepts: [
                { arg: 'text', type: 'string' },
            ],
            returns: { root: true },
        }
    );



    srch_spellcheck.countList = function (text, next) {
        const connector = srch_spellcheck.app.datasources.mindslab.connector;
        let sql = '';
        if (text && text != undefined && text != "") {
            sql =
                `SELECT count(*) as cnt
              FROM
                      srch_spellcheck_tb
              WHERE
                  SPELL_KEYWORD like '%${text}%'`


        } else {
            sql =
                `SELECT count(*) as cnt
                FROM
                        srch_spellcheck_tb`
        }
        connector.query(sql, null, (err, resultObjects) => {

            if (err) {
                next(err);
            }
            next(null, resultObjects);
        });
    };


 /*   srch_spellcheck.deleteByIds = function(id, next ) {

        srch_spellcheck.destroyById(id, (error) =>{
           // console.log(error);
           app.devlog(loggerName, error);

           if(error == null){
                next(null, {count :1});
           }else{
               next(null, {count :0});
           }
         });
     };*/
   /* srch_spellcheck.remoteMethod(
        'deleteByIds', {
          http: {path: '/:id', verb: 'post'},
          accepts: [
            {arg: 'id', type: 'number', required: true},
          ],
          returns: {root: true},
          isStatic: true
        }
      );*/

    // delete By Ids
    srch_spellcheck.deleteByIds = function(ids = [] , next) {
        return Promise.all(
            ids.map(id => srch_spellcheck.destroyById(id).catch(() => ({count: 0})))
        )
            .then(result => {
                let deletedIds = ids.filter((id, index) => result[index].count !== 0);
                return deletedIds;
            });
    };

    srch_spellcheck.remoteMethod(
        'deleteByIds', {
            accepts: [
                {arg: 'ids', type: ['string'], required: true},
            ],
            returns: {root: true},
            isStatic: true
        }
    );
};


/*srch_autocomplete.deleteByIds = function(id, next ) {

       srch_autocomplete.destroyById(id, (error) =>{
         // console.log(error);
         app.devlog(loggerName, error);

         if(error == null){
              next(null, {count :1});
         }else{
             next(null, {count :0});
         }
       });
   };

   srch_autocomplete.remoteMethod(
      'deleteByIds', {
        http: {path: '/:id', verb: 'post'},
        accepts: [
          {arg: 'id', type: 'number', required: true},
        ],
        returns: {root: true},
        isStatic: true
      }
    );*/