'use strict';

const app = require('../server');
const formidable = require('formidable');
const fsp = require('fs-promise');
const moment = require('moment');
// const config = require('config');
const path = require('path');
const fs = require('fs');

module.exports = function(Container) {
  const loggerName = path.basename(__filename, path.extname(__filename));
  const delim = {
    path: '/'
  };
  /** internal **/
  // const MAUM_ROOT = process.env.MAUM_ROOT;
  // if (!MAUM_ROOT) throw new Error('\x1b[41mMAUM_ROOT is not set on environment!\x1b[0m');

  let getWsPath = (workspace_id)=>`ws${workspace_id}`;
  let getTeamPath = (team_id)=>`t${team_id}`;
  console.log("---------------------------------------------------------------------------------------------------------")
  console.log("환경", process.env.NODE_ENV);
  console.log("---------------------------------------------------------------------------------------------------------")
  const storagePath = app.datasources.storage.settings.root;

  app.devlog(loggerName, 'storagePath',storagePath);

  let getStoragePath = Container.getStoragePath = (container, workspace_id, team_id, filePath = '') => {
    return [storagePath, container, getWsPath(workspace_id), getTeamPath(team_id), filePath].join(delim.path);
  };
  const getStoragePathEx = Container.getStoragePathEx = (container, filePath) => {
    // return [storagePath, container, filePath].join(delim.path);
    return [storagePath, container].join(delim.path).concat(filePath);
  };
  const getStorageFileName = Container.getStorageFileName = (filePath) => {
    const ls = filePath.split(delim.path);
    if(ls && ls.length > 1) {
      return ls.pop();
    }
    if(!filePath) {
      return '';
    }
    return filePath;
  };

  const getExtension = (file)=>{
    if ( !file || !file.name || file.name.indexOf('.') < 0) { return ''; }
    const ext = file.name.substr(file.name.lastIndexOf('.') + 1);
    return ext;
  };

  const getBotImageName = (workspace_id, team_id, fileName, ext)=>{
    const type = fileName.split('_')[0];
    const bot_id = fileName.split('_')[1];

    return `${type}_${workspace_id}_${team_id}_${bot_id}.${ext}`;
  };
  const getDownloadPath = (container, file)=>{
    if( !container || container == ''
        || !file || !file.path || file.path == ''
    ) {
      // console.log('current file has not download path');
      app.devlog(loggerName, 'current file has not download path');
      return '';
    }
    return file.path.split(`/${container}`)[1];
  };

  /** public **/
  Container.uploadex = function(container, _workspace_id, _team_id, req, next) {
    let form = new formidable.IncomingForm();
    form.uploadDir = getStoragePath(container, _workspace_id, _team_id);
    form.maxFieldsSize = 10 * 1024 * 1024;
    form.hash = 'sha256';
    form.multiples = true;

    if(!fs.existsSync(form.uploadDir)) {
      fs.mkdirSync(form.uploadDir, { recursive: true });
    }

    app.devlog(loggerName, 'uploadex');
    const workspace_id = getWsPath(_workspace_id);
    const team_id = getTeamPath(_team_id);
    // app.devlog(loggerName, 'uploadex', form.uploadDir, form);
    form.parse(req, (err, fields, fileObjects) => {
      // app.devlog(loggerName, 'uploadex form parse');
      if (err) return next(err);

      let files = [];
      for (let k in fileObjects) {
        files.push(fileObjects[k]);
      }
      let ext;
      Promise.all(
        files.map(
          file => Promise.all([
            file.hash, file.name
          ])
          // rename original file name
          .then(result => {
            // app.devlog(loggerName, 'upload', result, file);

            //shield
            if(!file.type.startsWith('image/')){
              let err = new Error(`not image file`);
              err.code = 'NOT_IMAGE_FILE';
              throw err;
            }
            ext = getExtension(file);

            // let dupHashFile = result[0];
            // let dupNameFile = result[1];
            // // same file
            // if (dupHashFile && dupHashFile.hash == file.hash) {
            //   let err = new Error(`Same file is already uploaded as ${dupHashFile.name}`);
            //   err.code = 'FILE_DUPLICATED';
            //   throw err;
            // }
            // // same name
            // if (dupNameFile && dupNameFile.name == file.name) {
            //   /* renaming file like below
            //     xxx.ext
            //     xxx_2.ext
            //     xxx_3.ext
            //   */
            //   let oldName = file.name;
            //   let tokens = oldName.split('.');
            //   let extension = tokens.length > 1 ? `.${tokens.pop()}` : '';
            //   let remains = tokens.join('.');
            //   let checkNameRec = index => {
            //     let name = `${remains}_${index}${extension}`;
            //     return Container.count({container, workspace_id, team_id, name})
            //       .then(count => {
            //         if (count == 0) {
            //           return name;
            //         }
            //         return checkNameRec(index + 1);
            //       });
            //   };
            //   return checkNameRec(2);
            // }
            return file.name;
          })
          // rename real filename as hash value
          .then(newName => {
            // console.log('upload 2', newName, file);
            const resName = ext === 'thumb' ? file.name : getBotImageName(_workspace_id, _team_id, file.name, ext);
            const newPath = getStoragePath(container, _workspace_id, _team_id, resName);
            file.name = resName || newName;
            file.newPath = newPath;
            fsp.rename(file.path, file.newPath);
            file.path = newPath;
            file.path = getDownloadPath(container, file);
            return file;
          })
          // remove temp file
          .catch(err => {
            // console.log('upload catch 1');
            app.devlog(loggerName, 'upload catch 1');
            fsp.remove(file.path);
            next(err); // eslint-disable-line
            throw err;
          })
          // // read audio duration
          .then(() => {
            // app.devlog(loggerName, 'upload 3', file);
            // app.devlog(loggerName, 'result file', file);
            return new Promise((resolve, reject) => {
              resolve(file);
            });
          })
          // remove moved file when fails
          .catch(err => {
            // console.log('upload catch 2');
            app.devlog('upload catch 2');
            fsp.remove(file.newPath);
            next(err);
          })
        //end of map
        )
      //end of promise
      ).then(savedFiles => {
        // console.log('upload 4');
        app.devlog(loggerName, 'upload 4');
        // app.devlog(loggerName, 'savedFiles',savedFiles);
        next(null, savedFiles.length == 1 ? savedFiles[0] : savedFiles);
      });
    // end of handler
    });
  // end of upload
  };

  Container.remoteMethod(
    'uploadex',
    {
      description: 'Uploads a file',
      accepts: [
        {arg: 'container', type: 'string', http: {source: 'path'}},
        {arg: 'workspace_id', type: 'number', http: {source: 'path'}},
        {arg: 'team_id', type: 'number', http: {source: 'path'}},
        {arg: 'req', type: 'object', http: {source: 'req'}},
        // {arg: 'usage', type: 'string', required: true, http: {source: 'query'}},
      ],
      returns: {root: true},
      http: {path: '/:container/uploadex/:workspace_id/:team_id', verb: 'post'}
    }
  );
  Container.downloadex = (container, p, res, next) => {
    app.devlog(loggerName, 'downloadex', 'container=>', container, 'path=>', p);

    // parse whether name or id
    const filePath = getStoragePathEx(container, p);
    const fileName = getStorageFileName(p);
    res.set('Content-Type', 'application/octet-stream');
    res.set('Content-Disposition', `attachment;filename=${fileName}`);
    // res.set('Content-Transfer-Encoding', 'binary');
    res.removeHeader('X-Content-Type-Options');
    app.devlog(loggerName, 'downloadex result', 'name=>', fileName, ', path=>', filePath);
    res.sendFile(filePath, null, err => {
      if (err) {
        console.error(err);
        res.status(err.status).end();
        // return next(err);
      }
    });
  };
  Container.remoteMethod(
    'downloadex',
    {
      description: 'Download a file',
      accepts: [
        {arg: 'container', type: 'string', http: {source: 'path'}},
        {arg: 'p', type: 'string', required: true, http: {source: 'query'}},
        {arg: 'res', type: 'object', http: {source: 'res'}},
      ],
      returns: {root: true},
      http: {path: '/:container/downloadex', verb: 'get'}
    }
  );
  Container.downloadwt = (container, _workspace_id, _team_id, name, res, next) => {
    app.devlog(loggerName, 'downloadwt', 'container=>', container, ', workspace_id', _workspace_id, ', team_id', _team_id, 'name=>', name);

    // parse whether name or id
    const filePath = getStoragePath(container, _workspace_id, _team_id, name);
    res.set('Content-Type', 'application/octet-stream');
    res.set('Content-Disposition', `attachment;filename=${name}`);
    // res.set('Content-Transfer-Encoding', 'binary');
    app.devlog(loggerName, 'downloadwt result', 'name=>', name, ', path=>', filePath);
    res.sendFile(filePath, null, err => {
      if (err) {
        console.error(err);
        res.status(err.status).end();
        // return next(err);
      }
      app.devlog(loggerName, 'download 2');
    });
  };
  Container.remoteMethod(
    'downloadwt',
    {
      description: 'Download a file',
      accepts: [
        {arg: 'container', type: 'string', http: {source: 'path'}},
        {arg: 'workspace_id', type: 'number', http: {source: 'path'}},
        {arg: 'team_id', type: 'number', http: {source: 'path'}},
        {arg: 'name', type: 'string', http: {source: 'path'}},
        {arg: 'res', type: 'object', http: {source: 'res'}},
      ],
      returns: {root: true},
      http: {path: '/:container/downloadwt/:workspace_id/:team_id/:name', verb: 'get'}
    }
  );

  // Container.upload2 = (container, file) => {
  //   let _url = `${LoopBackConfig.getPath()}/${LoopBackConfig.getApiVersion()}/containers/:container/upload`;
  //   return this.http.post(`${_url}`, formData);
  // };
  // Container.remoteMethod(
  //   'upload2',
  //   {
  //     accepts: [
  //       {arg: 'container', type: 'string', required: true},
  //       {arg: 'file', type: 'file', required: true},
  //     ],
  //     returns: {root: true},
  //     isStatic: true
  //   }
  // );

  // Container.download = function(cb) {
  //   // getTheStreamBody() can be implemented by calling http.request() or fs.readFile() for example
  //   getTheStreamBody(function(err, stream) {
  //     if (err) return cb(err);
  //     // stream can be any of: string, buffer, ReadableStream (e.g. http.IncomingMessage)
  //     cb(null, stream, 'application/octet-stream');
  //   });
  // };

  // Container.remoteMethod('download', {
  //   returns: [
  //     {arg: 'body', type: 'file', root: true},
  //     {arg: 'Content-Type', type: 'string', http: { target: 'header' }}
  //   ]
  // });
};
