'use strict';
const app = require('../server');
const path = require('path');
const moment = require('moment');

module.exports = function (srch_static) {
    const loggerName = path.basename(__filename, path.extname(__filename));



    srch_static.getList = function (next) {
        const connector = srch_static.app.datasources.mindslab.connector;
        let sql = '';

            sql =
                `SELECT 
                USER_DIV_CODE,
                USER_DIV_NM,
                USER_DEPT_CD,
                USER_DEPT_NM
            FROM
                    mobis_user_master_tb
            GROUP BY 
                    USER_DIV_CODE`;

        connector.query(sql, null, (err, resultObjects) => {
            if (err) next(err);

            // console.log(resultObjects);
            app.devlog(loggerName, resultObjects);
            next(null, resultObjects);
        });
    };


    srch_static.remoteMethod(
        'getList', {
            isStatic: true,
            http: {path: '/getList', verb: 'get'},
            returns: {root: true},
        }
    );


}


