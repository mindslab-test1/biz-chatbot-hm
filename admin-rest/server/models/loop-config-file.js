'use strict';

const path = require('path');
const fs = require('fs-extra-promise');
const app = require('../server');

module.exports = function(LoopConfigFiles) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  /** config setting **/
  LoopConfigFiles.getConfig = function(firstDepth, next) {
    let config = app.get(firstDepth);
    if (next) return next(null, config);
    return config;
  };

  LoopConfigFiles.remoteMethod(
    'getConfig', {
      accepts: [
        {arg: 'firstDepth', type: 'string'}
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  /** datasource config setting **/
  LoopConfigFiles.getDataSourceConfig = function(next) {
    let config = app.dataSources.email_smtp.connector.transportsIndex.smtp.transporter.options;
    if (next) return next(null, config);
    return config;
  };

  LoopConfigFiles.remoteMethod(
    'getDataSourceConfig', {
      returns: {root: true},
      isStatic: true
    }
  );
};
