'use strict';
const app = require('../server');
const path = require('path');

module.exports = function(Notification) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  Notification.settings.automaticValidation = false;
  Notification.observe('before save', function beforeSave(ctx, next) {
    let instance = ctx.instance || ctx.data;
    if (ctx.isNewInstance) {
      instance.createdAt = new Date();
    }
    next();
  });
};
