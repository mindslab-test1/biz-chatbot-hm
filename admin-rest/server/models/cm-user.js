'use strict';
const app = require('../server');
const models = app.models;
const NodeCache = require('node-cache');
const configCache = require('../config').NodeCache;
const path = require('path');
const ipaddr = require('ipaddr.js');

var SALT_WORK_FACTOR = 10;
var MAX_PASSWORD_LENGTH = 72;
var bcrypt;
try {
    // Try the native module first
    bcrypt = require('bcryptjs');
    // Browserify returns an empty object
    if (bcrypt && typeof bcrypt.compare !== 'function') {
        bcrypt = require('bcryptjs');
    }
} catch (err) {
    // Fall back to pure JS impl
    bcrypt = require('bcryptjs');
}

/**
 * via base User
 */
// const g = require('../../lib/globalize');
// const utils = require('../../lib/utils');
const g = require('../../node_modules/loopback/lib/globalize');
const utils = require('../../node_modules/loopback/lib/utils');

module.exports = function (cm_user) {

  delete cm_user.validations.email;

  var config = configCache;

  if (configCache) {

  } else {
    config = {stdTTL: 60, checkperiod: 0};
  }

  const  sCache = new NodeCache(config);
  const loggerName = path.basename(__filename, path.extname(__filename));

  // delete By Ids
  cm_user.deleteByIds = function(ids = [] /*, next*/) {
    return Promise.all(
        ids.map(id => cm_user.destroyById(id).catch(() => ({count: 0})))
      )
      .then(result => {
        let deletedIds = ids.filter((id, index) => result[index].count !== 0);

        cm_user.deletePassWordHistory(deletedIds, function (error, res) {
            return deletedIds;
        });
      });
  };

  cm_user.remoteMethod(
    'deleteByIds', {
      accepts: [
        {arg: 'ids', type: ['string'], required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );
  cm_user.remoteMethod('destroyAll', {
    isStatic: true,
    description: 'Delete all matching records',
    accessType: 'WRITE',
    accepts: {arg: 'where', type: 'object', description: 'filter.where object'},
    http: {verb: 'del', path: '/'}
  });

  // cm_user.prototype.createAccessToken = function(ttl, options, cb) {
  //   app.devlog(loggerName, 'proto createAccessToken before callback : ttl, options ===>\n',ttl,options);
  //   if (cb === undefined && typeof options === 'function') {
  //     // createAccessToken(ttl, cb)
  //     cb = options;
  //     options = undefined;
  //   }

  //   cb = cb || utils.createPromiseCallback();

  //   let tokenData;
  //   if (typeof ttl !== 'object') {
  //     // createAccessToken(ttl[, options], cb)
  //     tokenData = {ttl};
  //   } else if (options) {
  //     // createAccessToken(data, options, cb)
  //     tokenData = ttl;
  //   } else {
  //     // createAccessToken(options, cb);
  //     tokenData = {};
  //   }

  //   var userSettings = this.constructor.settings;
  //   tokenData.ttl = Math.min(tokenData.ttl || userSettings.ttl, userSettings.maxTTL);

  //   this.accessTokens.create(tokenData, options, cb);
  //   app.devlog(loggerName, 'proto createAccessToken end typeof',typeof this.accessTokens);
  //   app.devlog(loggerName, 'proto createAccessToken end ',this.accessTokens);

  //   app.devlog(loggerName, 'proto createAccessToken end cb.promise', cb.promise);
  //   return cb.promise;
  // };

  /**
   * via base User.login
   * 디버깅 위해서 일단 유지 xxx
   * @param {*} credentials
   * @param {*} include
   * @param {*} fn
   */
  cm_user.login = function(credentials, include, fn) {
    app.devlog(loggerName, 'custom login=> ', '\n\ncredentials ====\n', credentials, '\n\ninclude ====\n', include);
    var self = this;
    if (typeof include === 'function') {
      fn = include;
      include = undefined;
    }

    fn = fn || utils.createPromiseCallback();

    include = (include || '');
    if (Array.isArray(include)) {
      include = include.map(function(val) {
        return val.toLowerCase();
      });
    } else {
      include = include.toLowerCase();
    }

    var realmDelimiter;
    // Check if realm is required
    var realmRequired = !!(self.settings.realmRequired ||
      self.settings.realmDelimiter);
    if (realmRequired) {
      realmDelimiter = self.settings.realmDelimiter;
    }

    cm_user.getLoginStatus(credentials.username, function (err, data) {

        var query = self.normalizeCredentials(credentials, realmRequired,  realmDelimiter);
        var incorrect_cnt = data.loginCnt;
        var gapTime = data.gapTime;
        var isHibernated = data.isHibernated;
        var initLocked = false;
        var isLoinProcess = true;


        if( incorrect_cnt >= 5 ){

            if(gapTime >=60){
                initLocked = true;
            }else{
                var defaultError = new Error(g.f('login failed'));
                defaultError.statusCode = 999;
                defaultError.code = 'EXCEED_ACCESS_LONIN_COUNT';
                defaultError.msg = 'EXCEED_ACCESS_LONIN_COUNT';
                fn(defaultError);
                isLoinProcess = false;
                return fn.promise;
            }
        }else if(isHibernated && isHibernated.toUpperCase()  === 'Y') {

            var defaultError = new Error(g.f('login failed'));
            defaultError.statusCode = 888;
            defaultError.code = 'ACCOUNT_IS_HIBERNATED';
            defaultError.msg = 'Account is hibernated';
            fn(defaultError);
            isLoinProcess = false;
            return fn.promise;

        }

        if(isLoinProcess) {

            if (realmRequired && !query.realm) {
                var err1 = new Error(g.f('{{realm}} is required'));
                err1.statusCode = 400;
                err1.code = 'REALM_REQUIRED';
                fn(err1);
                return fn.promise;
            }
            if (!query.email && !query.username) {
                var err2 = new Error(g.f('{{username}} or {{email}} is required'));
                err2.statusCode = 400;
                err2.code = 'USERNAME_EMAIL_REQUIRED';
                fn(err2);
                return fn.promise;
            }

            //app.devlog('query => ', query);

            self.findOne({where: query}, function (err, user) {
                var defaultError = new Error(g.f('login failed'));
                defaultError.statusCode = 401;
                defaultError.code = 'LOGIN_FAILED';

                function tokenHandler(err, token) {
                    //app.devlog(loggerName, 'tokenHandler start ', err, token);
                    //app.devlog(loggerName, 'tokenHandler fn', fn);
                    if (err) return fn(err);
                    //app.devlog(loggerName, 'tokenHandler include', include);
                    if (Array.isArray(include) ? include.indexOf('user') !== -1 : include === 'user') {
                        // NOTE(bajtos) We can't set token.user here:
                        //  1. token.user already exists, it's a function injected by
                        //     "AccessToken belongsTo User" relation
                        //  2. ModelBaseClass.toJSON() ignores own properties, thus
                        //     the value won't be included in the HTTP response
                        // See also loopback#161 and loopback#162
                        token.__data.user = user;
                    }
                    //app.devlog(loggerName, 'tokenHandler before fn ', err, token);
                    fn(err, token);
                    //app.devlog(loggerName, 'tokenHandler end ', err, token);
                    //app.devlog(loggerName, 'custome login=> ', credentials, include, fn,'\n====================================================================================');
                }

                if (err) {
                    app.devlog(loggerName, 'An error is reported from User.findOne: %j', err);
                    fn(defaultError);
                } else if (user) {
                    user.hasPassword(credentials.password, function (err, isMatch) {
                        if (err) {
                            app.devlog(loggerName, 'An error is reported from User.hasPassword: %j', err);
                            fn(defaultError);
                        } else if (isMatch) {
                            if (self.settings.emailVerificationRequired && !user.emailVerified) {
                                // Fail to log in if email verification is not done yet
                                app.devlog(loggerName, 'User email has not been verified');
                                err = new Error(g.f('login failed as the email has not been verified'));
                                err.code = 'LOGIN_FAILED_EMAIL_NOT_VERIFIED';
                                err.details = {
                                    userId: user.user_id,
                                };
                                fn(err);
                            } else {
                                //app.devlog('login before createAccessToken ', user, user.createAccessToken);
                                if (user.createAccessToken.length === 2) {
                                    user.createAccessToken(credentials.ttl, tokenHandler);
                                    //app.devlog('login after 1 createAccessToken ', credentials.ttl, tokenHandler);
                                } else {
                                    user.createAccessToken(credentials.ttl, credentials, tokenHandler);
                                    //app.devlog('login after 2 createAccessToken ', credentials.ttl, credentials, tokenHandler);
                                    //app.devlog('login after 3 ', fn);
                                }
                            }
                        } else {
                            incorrect_cnt++;
                            cm_user.updateIncorrectCnt(credentials.username, incorrect_cnt, (err, res) => {
                                console.log(res);
                                app.devlog(loggerName, 'The password is invalid for user %s', query.email || query.username);
                                fn(defaultError);
                            });


                        }
                    });
                } else {
                    app.devlog(loggerName, 'No matching record is found for user %s', query.email || query.username);
                    fn(defaultError);
                }
            });

            if(initLocked){
                cm_user.initIncorrectInfo(credentials.username, 'sloveLock', (err, res) =>{
                    app.devlog(loggerName, 'init locked time & count');
                    console.log('락 초기화');
                    console.log(res);
                    initLocked = false;
                });
            }

            // app.devlog(loggerName, 'custom login end fn.promise ', fn.promise);
            // app.devlog(loggerName, 'custom login end cm_user.settings', cm_user.settings);
        }

        return fn.promise;
    });

  };
  // cm_user.disableRemoteMethodByName('prototype.login');
  // cm_user.remoteMethod(
  //   'login',
  //   {
  //     description: 'Login a user with username/email and password.',
  //     accepts: [
  //       {arg: 'credentials', type: 'object', required: true, http: {source: 'body'}},
  //       {arg: 'include', type: ['string'], http: {source: 'query'},
  //         description: 'Related objects to include in the response. ' +
  //         'See the description of return value for more details.'},
  //     ],
  //     returns: {
  //       arg: 'accessToken', type: 'object', root: true,
  //       description:
  //         g.f('The response body contains properties of the {{AccessToken}} created on login.\n' +
  //         'Depending on the value of `include` parameter, the body may contain ' +
  //         'additional properties:\n\n' +
  //         '  - `user` - `U+007BUserU+007D` - Data of the currently logged in user. ' +
  //         '{{(`include=user`)}}\n\n'),
  //     },
  //     http: {verb: 'post'},
  //   }
  // );

  cm_user.afterRemote('login', function (ctx, auth, next) {

      app.devlog(loggerName, 'login after auth, next \nauth ====\n', auth,'\n\ncredentials ====\n', JSON.stringify(ctx.args.credentials), '\n\n====');

      //cm_user.accessToken.destroyAll({"id":{neq: auth.__data.id}},  (err, res) => {
          //if(err){
         //     next(err);
          //}else{
              models.cm_login_history.create(
               Object.assign({
                   user_id: auth.userId,
                   signed_yn: true
               }, getRemoteInformation(ctx)));
              next();
          //}
     // });
  });


  cm_user.afterRemoteError('login', function (ctx, next) {
    app.devlog(loggerName, 'login after err \ncredentials ====\n', JSON.stringify(ctx.args.credentials), '\n\n====');
    models.cm_login_history.create(
      Object.assign({
        user_id: ctx.args.credentials.username,
        signed_yn: false,
        msg: ctx.error.message
      }, getRemoteInformation(ctx)));
    next(ctx.error);
  });

  function getRemoteInformation(ctx) {
      let ipString = ctx.req.connection.remoteAddress || ctx.req.headers['x-forwarded-for'];

      if (ipString.substr(0, 7) == "::ffff:") {
          ipString = ipString.substr(7)
      }

    return {
      remote: ipString,
      agent: ctx.req.headers['user-agent']
    };
  }

  const prop = (t, p) => Object.assign(t, p);
  cm_user.createOptionsFromRemotingContext = function(ctx) {
    //app.devlog(loggerName, 'createOptionsFrom ctx ============\n\n', ctx, '\n\n=======================================');
    var base = this.base.createOptionsFromRemotingContext(ctx);
    return prop(base, {
      currentUserId: base.accessToken && base.accessToken.userId,
    });
  };


  //사용자 업데이트 후 절차...
  cm_user.afterRemote('upsertWithWhere', function (ctx, auth, next) {

      const userData = auth.__data;
      const is_passwordChange = userData.hasOwnProperty('password');

      if(is_passwordChange){
          //패스워드 변경시 히스토리에 저장한다.
            cm_user.insertPassWordHistory(userData.user_id, userData.password, function(err, res) {
              if(err){
                  next(err);
              }else{
                  next();
              }
          });
      }
  });


  //비번 변경 후 히스토리 입력...
  cm_user.afterRemote('setPasswordEx', function (ctx, auth, next) {

        const data = ctx.args;
        cm_user.findById(data.userId, function(err, user) {
            //패스워드 초기화 시 로그인할때 비번을 변경하도록 is_change_pass 를 N 으로 변경..
            const userInfo  = user.__data;
            cm_user.updateChangePass(userInfo.user_id, 'N', function(err, res) {
                if(err){
                    next(err);
                }else{
                    next();
                }
            });
        });
    });


   cm_user.afterRemote('initPassword', function (ctx, auth, next) {
       const data = ctx.args;
       cm_user.findById(data.userId, function (err, user) {
           //패스워드 변경시 히스토리에 저장한다.
           const userInfo = user.__data;
           cm_user.updateChangePass(userInfo.user_id, 'Y', (err, res) => {
               if (err) {
                   return next(err);
               } else {
                   cm_user.insertPassWordHistory(userInfo.user_id, userInfo.password, function (err, res) {
                       if (err) {
                           next(err);
                       } else {
                           next();
                       }
                   });
               }
           });
       });
   });




    cm_user.beforeRemote('saveOptions', function(ctx, unused, next) {

    //app.devlog(loggerName, 'saveOptions ctx, unused, next ============\n\n', ctx,'\n\n====\n\n', unused,'\n\n====\n\n', next, '\n\n=======================================');
    if (!ctx.args.options.accessToken) return next();
    cm_user.findById(ctx.args.options.accessToken.userId, function(err, user) {
      if (err) return next(err);
      ctx.args.options.currentUser = user;
      next();
    });
  })


  cm_user.getUserRoles = function (UserId, next) {
    return new Promise((resolve, reject) => {
      let query = {and: [{principalType: models.RoleMapping.USER}, {principalId: UserId}]};
      models.RoleMapping.find({where: query}, (err, mappings) => {
        if (err) {
          reject(err);
        } else {
          resolve(mappings.map(item => item.roleId));
        }
      })
    })
      .then(roleIds => new Promise((resolve, reject) => {
        if (roleIds.length < 1) {
          resolve({});
          return;
        }
        let ids = roleIds.map(id => {
          return {id: id};
        });
        let where = {where: {or: ids}};
        models.Role.find(where, (err, roles) => {
          if (err) {
            reject(err);
          }
          let _roles = {};
          roles.forEach(role => _roles[role.name] = true);
          resolve(_roles);
        })
      }));
  };

  cm_user.remoteMethod(
    'getUserRoles',
    {
      accepts: [
        {arg: 'UserId', type: 'number', required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  cm_user.getCurrentRoles = function (next) {
    return cm_user.getUserRoles(cm_user.app.userId, next);
  };

  cm_user.remoteMethod(
    'getCurrentRoles',
    {
      returns: {root: true},
      isStatic: true
    }
  );

  cm_user.getRoleList = function (next) {
    return new Promise((resolve) => resolve(require('../lib/acl-assets').roleList));
  };

  cm_user.remoteMethod(
    'getRoleList',
    {
      returns: {root: true},
      isStatic: true
    }
  );

  cm_user.createWithRoles = function (Args, next) {
    let Account = Args.Account;
    let Roles = Args.Roles;

    return new Promise((resolve, reject) => {
      cm_user.create(Account, (err, user) => {
        if (err) {
          reject(err);
          return;
        } else {
          resolve(user);
        }
      })
    })
      .then(user => new Promise((resolve, reject) => {
        if (Roles.length < 1) {
          resolve();
          return;
        }
        let where = {
          where: {
            or: Object.keys(Roles).filter(key => Roles[key] === true).map(key => {
              return {name: key};
            })
          }
        };
        models.Role.find(where, (err, roles) => {
          if (err) {
            reject(err);
            return;
          }
          resolve({userId: user.id, roles: roles});
        })
      }))
      .then(ws => {
        let roleMappings = [];
        ws.roles.forEach(role => {
          roleMappings.push(
            new Promise((resolve, reject) =>
              role.principals.create({
                principalType: models.RoleMapping.USER,
                principalId: ws.userId
              }, (err, principal) => {
                if (err) {
                  reject(err);
                  return;
                }
                resolve(principal);
              })
            )
          )
        });
        return Promise.all(roleMappings);
      })
      .then(() => Promise.resolve({result: true}));
  };

  cm_user.remoteMethod(
    'createWithRoles',
    {
      accepts: [
        {arg: 'Account', type: 'object', required: true, http: {source: 'body'}},
        {arg: 'Roles', type: 'object', required: false, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  cm_user.patchAttributesWithRoles = function (Args, next) {
    let Account = Args.Account;
    let Roles = Args.Roles;

    return new Promise((resolve, reject) => {
      cm_user.updateAll({id: Account.id}, Account, (err, count) => {
        if (err) {
          reject(err);
          return;
        } else {
          resolve();
        }
      })
    })
      .then(() => new Promise((resolve, reject) => {
        if (Roles.length < 1) {
          resolve();
          return;
        }
        let where = {
          where: {
            or: Object.keys(Roles).map(key => {
              return {name: key};
            })
          }
        };
        models.Role.find(where, (err, roles) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(roles);
        })
      }))
      .then(roles => new Promise((resolve, reject) => {
        if (!roles || roles.length < 1) {
          resolve();
          return;
        }
        let deleteRoles = roles.filter(role => Roles[role.name] === false);
        if (deleteRoles.length < 1) {
          resolve(roles);
          return;
        }
        // app.devlog('delete:' , deleteRoles.map(role => role.name));
        let where = {
          and: [
            {principalType: models.RoleMapping.USER},
            {principalId: Account.id},
            {
              or: deleteRoles.map(role => {
                return {roleId: role.id};
              })
            }
          ]
        };
        models.RoleMapping.destroyAll(where, (err) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(roles);
        });
      }))
      .then(roles => {
        if (!roles || roles.length < 1) {
          Promise.resolve();
          return;
        }
        let addRoles = roles.filter(role => Roles[role.name] === true);
        if (addRoles.length < 1) {
          Promise.resolve();
          return;
        }
        // app.devlog('add:' , addRoles.map(role => role.name));
        let roleMappings = [];
        addRoles.forEach(role => {
          roleMappings.push(
            new Promise((resolve, reject) =>
              role.principals.create({
                principalType: models.RoleMapping.USER,
                principalId: Account.id
              }, (err, principal) => {
                if (err) {
                  reject(err);
                  return;
                }
                resolve(principal);
              })
            )
          )
        });
        return Promise.all(roleMappings);
      })
      .then(() => Promise.resolve({result: true}));
  };

  cm_user.remoteMethod(
    'patchAttributesWithRoles',
    {
      accepts: [
        {arg: 'Account', type: 'object', required: true, http: {source: 'body'}},
        {arg: 'Roles', type: 'object', required: false, http: {source: 'body'}},
      ],
      returns: {root: true},
      isStatic: true
    }
  );
  cm_user.setPasswordEx = (userId, newPassword,workspace_id, team_id,  next)=>{
    app.devlog(loggerName, 'setPasswordEx');
    const obs = cm_user.setPassword(userId, newPassword, (err) => {
      if (err) {
          return next(err);
      }


      cm_user.initIncorrectInfo(userId, 'init', (err, res) =>{
          if (err) {
              return next(err);
          }else{
              return next();
          }
      });
    });

    return new Promise((resolve, reject) => obs);
  }


  cm_user.remoteMethod(
    'setPasswordEx',
    {
      accepts: [
        {arg: 'userId', type: 'string', required: true},
        {arg: 'newPassword', type: 'string', required: true},
        {arg: 'workspace_id', type: 'number', required: false},
        {arg: 'team_id', type: 'number', required: false},
      ],
      returns: {root: true},
      http: { path:'/setPasswordEx', verb: 'post' },
    }
  );


   cm_user.initPassword = (userId, newPassword, workspace_id, team_id,  next)=>{
    app.devlog(loggerName, 'initPassword');
    const obs = cm_user.setPassword(userId, newPassword, (err) => {
      if (err) {
          return next(err);
      }else{
           return next();
      }
    });

    return new Promise((resolve, reject) => obs);
  }


  cm_user.remoteMethod(
    'initPassword',
    {
      accepts: [
        {arg: 'userId', type: 'string', required: true},
        {arg: 'newPassword', type: 'string', required: true},
        {arg: 'workspace_id', type: 'number', required: false},
        {arg: 'team_id', type: 'number', required: false}
      ],
      returns: {root: true},
      http: {path: '/initPassword', verb: 'post'},
    }
  );


  cm_user.disableRemoteMethodByName('prototype.__count__accessTokens');
  cm_user.disableRemoteMethodByName('prototype.__create__accessTokens');
  // cm_user.disableRemoteMethodByName('prototype.__delete__accessTokens');
  // cm_user.disableRemoteMethodByName('prototype.__destroyById__accessTokens');
  cm_user.disableRemoteMethodByName('prototype.__findById__accessTokens');
  cm_user.disableRemoteMethodByName('prototype.__get__accessTokens');
  cm_user.disableRemoteMethodByName('prototype.__updateById__accessTokens');

  // this.accessTokens
  const this_accessTokens_OriginCode = function (condOrRefresh, options, cb) {
    if (arguments.length === 0) {
      if (typeof f.value === 'function') {
        return f.value(self);
      } else if (self.__cachedRelations) {
        return self.__cachedRelations[name];
      }
    } else {
      const condOrRefreshIsCallBack = typeof condOrRefresh === 'function' &&
        options === undefined &&
        cb === undefined;
      if (condOrRefreshIsCallBack) {
        // customer.orders(cb)
        cb = condOrRefresh;
        options = {};
        condOrRefresh = undefined;
      } else if (typeof options === 'function' && cb === undefined) {
        // customer.orders(condOrRefresh, cb);
        cb = options;
        options = {};
      }
      options = options || {};
      // Check if there is a through model
      // see https://github.com/strongloop/loopback/issues/1076
      if (f._scope.collect &&
        condOrRefresh !== null && typeof condOrRefresh === 'object') {
        f._scope.include = {
          relation: f._scope.collect,
          scope: condOrRefresh,
        };
        condOrRefresh = {};
      }
      return definition.related(self, f._scope, condOrRefresh, options, cb);
    }
  };
  // utils.createPromiseCallback
  const utils_createPromiseCallback = function createPromiseCallback() {
    var cb;
    var promise = new Promise(function(resolve, reject) {
      cb = function(err, data) {
        if (err) return reject(err);
        return resolve(data);
      };
    });
    cb.promise = promise;
    return cb;
  };

  cm_user.remoteMethod(
    'lastLoginInfo', {
      description: '최근 로그인 정보.',
      accepts: [
        {arg: 'user_id', type: 'string', required: true},
        {arg: 'workspace_id', type: 'number', required: false},
        {arg: 'team_id', type: 'number', required: false}
      ],
      returns: {root: true},
      isStatic: true,
       http: {path: '/lastLoginInfo', verb: 'get'},
    }
  );


   cm_user.lastLoginInfo = function(user_id, workspace_id, team_id,  next) {

      const time = new Date();
      const cacheTempName = user_id + '_' + time.getTime();

     sCache.get(cacheTempName, function(err, value) {

         if (!err) {
             if (value == undefined) {
                  let sql;

                  sql = `select user_id,
                               remote,
                               created_dtm,
                               signed_yn
                          from cm_login_history_tb
                          where user_id = '${user_id}'
                          order by created_dtm desc 
                          limit 1`;

                  console.log(sql);

                  const connector = cm_user.app.datasources.mindslab.connector;

                   connector.query(sql, null, (err, result) => {
                      if (err) {
                          next(err);
                      } else {
                          let userInfo = {};
                          if(result && result.length > 0) {
                              userInfo.userId = result[0].user_id;
                              userInfo.remoteIp =  result[0].remote;
                              userInfo.accessTime =  result[0].created_dtm;
                              userInfo.signedYn = result[0].signed_yn;
                          }

                          next(null, userInfo);
                      }
                  });
             }
         }
     });
  };


   cm_user.getLoginStatus = function(user_id, next) {

      const time = new Date();
      const cacheTempName = user_id + '_' + time.getTime();
      let userInfo = {};

      sCache.get(cacheTempName, function(err, value) {

         if (!err) {
             if (value == undefined) {
                  let sql;

                  sql = `select user_id,
                               incorrect_cnt,
                               TIMESTAMPDIFF(MINUTE, locked_dtm, now()) gapTime,
                               is_hibernated                               
                          from cm_user_tb
                          where user_id = '${user_id}'`;

                  console.log(sql);

                  const connector = cm_user.app.datasources.mindslab.connector;

                   connector.query(sql, null, (err, result) => {
                      if (err) {
                          next(err);
                      } else {

                          if(result && result.length > 0) {
                              userInfo.userId = result[0].user_id;
                              userInfo.loginCnt =  result[0].incorrect_cnt;
                              userInfo.gapTime =  result[0].gapTime;
                              userInfo.isHibernated = result[0].is_hibernated;
                          }
                          next(null, userInfo);
                      }
                  });
             }
         }
     });
  };


    cm_user.updateIncorrectCnt = function(user_id, incorrect_cnt, next) {

        const time = new Date();
        const cacheTempName = user_id + '_' + time.getTime();
        let cnt = 0;

        const locked_dtm_sql =`, locked_dtm =now() `;



        sCache.get(cacheTempName, function(err, value) {

            if (!err) {
                if (value == undefined) {
                    let sql;

                    sql = `update cm_user_tb
                           set incorrect_cnt = ${incorrect_cnt} `;
                    sql += locked_dtm_sql    + `   where user_id='${user_id}'`;
                    console.log(sql);

                    const connector = cm_user.app.datasources.mindslab.connector;

                    connector.query(sql, null, (err, result) => {
                        if (err) {
                            next(err);
                        } else {

                            console.log(result);

                            next(null, cnt);
                        }
                    });
                }
            }
        });
    };



    cm_user.initIncorrectInfo = function(user_id, type, next) {

        const time = new Date();
        const cacheTempName = user_id + '_' + time.getTime();
        let cnt = 0;

        sCache.get(cacheTempName, function(err, value) {

            if (!err) {
                if (value == undefined) {
                    let sql;

                    if(type =='init'){

                        sql = `update cm_user_tb
                           set incorrect_cnt = 0,
                               locked_dtm = null,
                               is_hibernated ='N'
                           where user_id = '${user_id}'`;

                    }else{

                        sql = `update cm_user_tb
                           set incorrect_cnt = 0,
                               locked_dtm = null
                           where user_id = '${user_id}'`;
                    }

                    console.log(sql);

                    const connector = cm_user.app.datasources.mindslab.connector;

                    connector.query(sql, null, (err, result) => {
                        if (err) {
                            next(err);
                        } else {
                            console.log(result);
                            next(null, cnt);
                        }
                    });
                }
            }
        });
    };



    cm_user.updateChangePass = function(user_id, flag,  next) {

        const time = new Date();
        const cacheTempName = user_id + '_' + time.getTime();
        let cnt = 0;

        sCache.get(cacheTempName, function(err, value) {

            if (!err) {
                if (value == undefined) {
                    let sql;

                    sql = `update cm_user_tb
                            set is_change_pass ='${flag}'
                           where user_id = '${user_id}'`;

                    console.log(sql);

                    const connector = cm_user.app.datasources.mindslab.connector;

                    connector.query(sql, null, (err, result) => {
                        if (err) {
                            next(err);
                        } else {
                            console.log(result);
                            next(null, cnt);
                        }
                    });
                }
            }
        });
    };


    cm_user.insertPassWordHistory = function(userId, password, next) {

        const time = new Date();
        const cacheTempName = userId + '_' + time.getTime();
        let cnt = 0;

        sCache.get(cacheTempName, function(err, value) {

            if (!err) {
                if (value == undefined) {
                    let sql;


                    sql = `insert into cm_password_history(user_id, password, created_dtm)
                          values('${userId}', '${password}', now())`;


                    console.log(sql);

                    const connector = cm_user.app.datasources.mindslab.connector;

                    connector.query(sql, null, (err, result) => {
                        if (err) {
                            next(err);
                        } else {
                            cnt = 1;
                            console.log(result);
                            next(null, cnt);
                        }
                    });
                }
            }
        });
    };



    cm_user.deletePassWordHistory = function(userIds, next) {

        const time = new Date();
        const userIdArray = userIds.join(',');
        const cacheTempName = userIds[0]  + '_' + time.getTime();
        let cnt = 0;



        sCache.get(cacheTempName, function(err, value) {

            if (!err) {
                if (value == undefined) {
                    let sql;


                    sql = `delete from cm_password_history where user_id in ('${userIdArray}')`;


                    console.log(sql);

                    const connector = cm_user.app.datasources.mindslab.connector;

                    connector.query(sql, null, (err, result) => {
                        if (err) {
                            next(err);
                        } else {
                            cnt = 1;
                            console.log(result);
                            next(null, cnt);
                        }
                    });
                }
            }
        });
    };



    cm_user.remoteMethod(
        'checkLastPasswords', {
            description: '최근 지정 패스워드 5개와 비교..',
            accepts: [
                {arg: 'user_id', type: 'string', required: true},
                {arg: 'password', type: 'string', required: true},
                {arg: 'workspace_id', type: 'number', required: false},
                {arg: 'team_id', type: 'number', required: false}
            ],
            returns: {root: true},
            isStatic: true,
            http: {path: '/checkLastPasswords', verb: 'get'},
        }
    );


    cm_user.checkLastPasswords = function(userId, password, workspace_id, team_id,  next) {

        const time = new Date();
        const cacheTempName = userId + '_' + time.getTime();
        let is_match = false;

        sCache.get(cacheTempName, function(err, value) {
            if (!err) {
                if (value == undefined) {
                    let sql;

                    sql = `select user_id, password
                           from cm_password_history
                           where user_id = '${userId}'
                           order by created_dtm desc
                           limit 5`;

                    app.devlog(loggerName, sql);
                    console.log(sql);

                    const connector = cm_user.app.datasources.mindslab.connector;

                    connector.query(sql, null, (err, result) => {
                        if (err) {
                            next(err);
                        } else {
                            const length = result.length ;
                            let isMatch = false;

                            for(let i = 0; i < length; i++){
                                isMatch = bcrypt.compareSync(password, result[i].password);
                                if(isMatch){
                                    is_match = isMatch;
                                    break;
                                }
                            }
                        }
                        next(null, is_match);
                    });
                }
            }
        });
    };


    cm_user.remoteMethod(
        'checkConnectedUser', {
            description: '현재 접속중인 사용자 체크',
            accepts: [
                {arg: 'user_id', type: 'string', required: true}
            ],
            returns: {root: true},
            isStatic: true,
            http: {path: '/checkConnectedUser', verb: 'get'},
        }
    );


    cm_user.checkConnectedUser = function(userId,  next) {

        const time = new Date();
        const cacheTempName = userId + '_' + time.getTime();

       // next = next || utils.createPromiseCallback();

        sCache.get(cacheTempName, function(err, value) {
            if (!err) {
                if (value == undefined) {

                    const filter = {"where": {"userId" : userId}};

                    cm_user.accessToken.find(filter, (err, data) => {

                        if(err){
                            next(err);
                        }else{
                            var isExistUser = false;

                            if(data && data.length > 0){
                                isExistUser = true;
                            }else{
                                isExistUser = false;
                            }
                            /*다중접속 허용을 위해 주석*/
                            //next(null, isExistUser);
                            next(null, false);
                        }
                    });
                }
            }
        });
    };
};
