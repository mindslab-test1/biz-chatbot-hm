'use strict';

const app = require('../server');
const _ = require('lodash');
const moment = require('moment');
const models = app.models;
const NodeCache = require('node-cache');

const configCache = require('../config').NodeCache;
const path = require('path');

module.exports = function(stats_dlg_history) {
  const loggerName = path.basename(__filename, path.extname(__filename));
  var config = configCache;
  if (configCache) {

  } else {
    config = {stdTTL: 60, checkperiod: 0};
  }
  const  sCache = new NodeCache(config);

  sCache.on('set', function(key, value) {
    // console.log('========================================================================================');
    // console.log('set', sCache.getStats());
    // console.log('========================================================================================');
    app.devlog(loggerName, '========================================================================================');
    app.devlog(loggerName, 'set', sCache.getStats());
    app.devlog(loggerName, '========================================================================================');
  });

  sCache.on('expired', function(key, value) {
    // console.log('========================================================================================');
    // console.log('expired', sCache.getStats());
    // console.log('========================================================================================');
    app.devlog(loggerName, '========================================================================================');
    app.devlog(loggerName, 'expired', sCache.getStats());
    app.devlog(loggerName, '========================================================================================');
  });

  stats_dlg_history.cachetest = function(bot_id, next) {
    sCache.get('stats', function(err, value) {
      if (!err) {
        if (value == undefined) {
          // console.log('undefined');
          app.devlog(loggerName, 'undefined');
          stats_dlg_history.count({bot_id: bot_id}, function(err, res) {
            if (err) {
              // console.log('err', err);
              app.devlog(loggerName, 'err', err);
            } else {
              // console.log('stats_dlg_history count =', res);
              app.devlog(loggerName, 'stats_dlg_history count =', res);

              sCache.set('stats', res, function(err, success) {
                if (!err && success) {
                  // console.log(success);
                  app.devlog(loggerName, success);
                }
              });
              next(null, res);
            }
          });
        } else {
          // console.log('cache value', value);
          app.devlog(loggerName, 'cache value', value);
          next(null, value);
        }
      }
    });
  };

  stats_dlg_history.remoteMethod(
    'cachetest', {
      http: {path: '/cachetest', verb: 'get'},
      accepts: [
        {arg: 'bot_id', type: 'number'},
      ],
      returns: {root: true},
    }
  );

  stats_dlg_history.getStats = function(start_date, end_date, boat_Ids, team_id, bot_id, next) {
    // console.log('start_date :', start_date, `${moment(start_date).format('YYYY-MM-DD')}`);
    // console.log('end_date :', end_date, `${moment(end_date).format('YYYY-MM-DD')}`);
    app.devlog(loggerName, 'start_date :', start_date, `${moment(start_date).format('YYYY-MM-DD')}`);
    app.devlog(loggerName, 'end_date :', end_date, `${moment(end_date).format('YYYY-MM-DD')}`);

    var arrayFlag = true;
    var arraySql = '';

    if (bot_id && bot_id != null && bot_id != undefined && bot_id != 0) {
      arrayFlag = false;
    } else {
      arrayFlag = true;
      if (team_id && team_id != null && team_id != undefined && team_id != 0) {
        arraySql = 'and bot_id in (select bot_id from chatbot_info_tb where team_id =' + team_id + ')';
      } else {
        team_id = 0;
        arraySql = 'and bot_id in (' + boat_Ids + ')';
      }
      bot_id = 0;
    }
    var cacheTempName = 'S_' + team_id + '_' + bot_id + `${moment(start_date).format('YYYY-MM-DD HH:mm:ss')}` + '_' + `${moment(end_date).format('YYYY-MM-DD HH:mm:ss')}`;


    sCache.get(cacheTempName, function(err, value) {
      if (!err) {
        if (value == undefined) {
          let sql =
            `select
                DATE_FORMAT( CONVERT_TZ('${moment(start_date).format('YYYY-MM-DD HH:mm:ss')}', @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as start_date,
                DATE_FORMAT( CONVERT_TZ('${moment(end_date).format('YYYY-MM-DD HH:mm:ss')}', @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as end_date,
                count(utter_id) as totalCount,
                count(DISTINCT(session_id)) as sessionCount,
                count(if(speaker='CHATBOT', speaker, null)) as chatbotCount,
                count(if(speaker='USER', speaker, null)) as dialogCount,
                count(if(dlg_step='initGreeting', dlg_step, null)) as greetCount,
                count(if(msg_type_cd='MT0002', msg_type_cd, null)) as unknownCount,
                TIMESTAMPDIFF(SECOND, '${moment(start_date).format('YYYY-MM-DD HH:mm:ss')}', '${moment(end_date).format('YYYY-MM-DD HH:mm:ss')}') as diff,
                IFNULL((count(if(speaker='CHATBOT', speaker, null)) / TIMESTAMPDIFF(SECOND, '${moment(start_date).format('YYYY-MM-DD HH:mm:ss')}', '${moment(end_date).format('YYYY-MM-DD HH:mm:ss')}')),0) as tpsAverage,
                IFNULL(
                    (
                      (
                        count(
                          utter_id
                        ) - (
                          count(
                            if(dlg_step='initGreeting', dlg_step, null)
                          ) + count(
                            if(msg_type_cd='MT0002', msg_type_cd, null)
                          )
                        )
                      ) / (
                        count(
                          utter_id
                        ) - count(
                           if(dlg_step='initGreeting', dlg_step, null)
                        )
                      )
                    ) * 100
                ,0)
                 as reliabilityAverage
                from 
                dlg_history_tb
                where timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                and timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`;

          if (arrayFlag) {
            sql += arraySql;
          } else {
            sql += `and bot_id = ${bot_id}`;
          }

          // console.log(sql);
          app.devlog(loggerName, sql);

          const connector = stats_dlg_history.app.datasources.statistic.connector;
          connector.query(sql, null, (err, result) => {
            if (err) {
              next(err);
            } else {
              // console.log('result =', result);
              app.devlog(loggerName, 'result =', result);

              sCache.set(cacheTempName, result, function(err, success) {
                if (!err && success) {
                  // console.log(success);
                  app.devlog(loggerName, success);
                }
              });
              next(null, result);
            }
          });
        } else {
          // console.log('cache value !!!', cacheTempName, value);
          app.devlog(loggerName, 'cache value !!!', cacheTempName, value);
          next(null, value);
        }
      }
    });
  };

  stats_dlg_history.remoteMethod(
    'getTopN', {
      http: {path: '/getTopN', verb: 'get'},
      accepts: [
          {arg: 'start_date', type: 'date', required: false},
          {arg: 'end_date', type: 'date', required: false},
          {arg: 'boat_Ids', type: 'string', required: false},
          {arg: 'type', type: 'string', required: false},
          {arg: 'step', type: 'string', required: false},
          {arg: 'team_id', type: 'number', required: false},
          {arg: 'bot_id', type: 'number'},
      ],
      returns: {root: true},
    }
  );

  stats_dlg_history.getTopN = function(start_date, end_date, boat_Ids,  type, step,  team_id, bot_id, next) {
    // console.log('start_date :', start_date, `${moment(start_date).format('YYMMDD')}`);
    // console.log('end_date :', end_date, `${moment(end_date).format('YYMMDDHH')}`);
    app.devlog(loggerName, 'start_date :', start_date, `${moment(start_date).format('YYMMDD')}`);
    app.devlog(loggerName, 'end_date :', end_date, `${moment(end_date).format('YYMMDDHH')}`);

    let arrayFlag = false;
    let arraySql = '';


    if (bot_id && bot_id != null && bot_id != undefined && bot_id != 0) {
      arrayFlag = false;
    } else {
      arrayFlag = true;

      if (team_id && team_id != null && team_id != undefined && team_id != 0) {
        arraySql = 'and bot_id in (select bot_id from chatbot_info_tb where team_id =' + team_id + ')';
      } else {
        team_id = 0;
         arraySql = 'and bot_id in (' + boat_Ids +')';
      }

      bot_id = 0;
    }

     const cacheTempName = `${step}` + '_' + type + '_' + team_id + '_' + bot_id + '_'
                    + `${moment(start_date).format('YYYY-MM-DD HH:mm:ss')}` + '_'
         + `${moment(end_date).format('YYYY-MM-DD HH:mm:ss')}`;

    sCache.get(cacheTempName, function(err, value) {
      if (!err) {
         if (value == undefined) {
           let sql;
           let conditon;

           conditon = ' group by domain_name\n' +
               '         )t\n' +
               '        order by cnt desc, domain_name asc\n' +
               '        limit 0, 10';

           sql =`select *
                 from (
                        select domain_name, count(domain_name) cnt
                        from dlg_history_tb
                        where timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                              and timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                              and domain_name <> ''  
                              and domain_name is not null
                              and speaker = 'CHATBOT'
                              and dlg_step = '${step}'
                           `;

           if (arrayFlag) {
              sql += arraySql;
            } else {
              sql += `and bot_id =${bot_id}`;
            }

            sql += conditon;

            // console.log(sql);
            app.devlog(loggerName, sql);
            const connector = stats_dlg_history.app.datasources.statistic.connector;

            connector.query(sql, null, (err, result) => {
            if (err) {
              next(err);
            } else {
              // console.log('result =', result);
              app.devlog(loggerName, 'result =', result);

              sCache.set(cacheTempName, result, function(err, success) {
                if (!err && success) {
                  // console.log(success);
                  app.devlog(loggerName, success);
                }
              });
              next(null, result);
            }
          });
         } else {
          // console.log('cache value !!!', cacheTempName, value);
          app.devlog(loggerName, 'cache value !!!', cacheTempName, value);
          next(null, value);
        }
      }
    });

  };

  stats_dlg_history.remoteMethod(
    'getStats', {
      http: {path: '/getStats', verb: 'get'},
      accepts: [
        {arg: 'start_date', type: 'date', required: false},
        {arg: 'end_date', type: 'date', required: false},
        {arg: 'boat_Ids', type: 'string', required: false},
        {arg: 'team_id', type: 'number', required: false},
        {arg: 'bot_id', type: 'number'},
      ],
      returns: {root: true},
    }
  );

  stats_dlg_history.getTpsChart = function(start_date, end_date, boat_Ids, type, team_id, bot_id, next) {
    // console.log('start_date :', start_date, `${moment(start_date).utc().format('YYMMDD')}`);
    // console.log('end_date :', end_date, `${moment(end_date).utc().format('YYMMDDHH')}`);
    app.devlog(loggerName, 'start_date :', start_date, `${moment(start_date).utc().format('YYMMDD')}`);
    app.devlog(loggerName, 'end_date :', end_date, `${moment(end_date).utc().format('YYMMDDHH')}`);



    var arrayFlag = false;
    var arraySql = '';

    if (bot_id && bot_id != null && bot_id != undefined && bot_id != 0) {
      arrayFlag = false;
    } else {
      arrayFlag = true;
      if (team_id && team_id != null && team_id != undefined && team_id != 0) {
        arraySql = 'and bot_id in (select bot_id from chatbot_info_tb where team_id =' + team_id + ')';
      } else {
        team_id = 0;
         console.log('boat_Ids : ' + boat_Ids);
        arraySql = 'and bot_id in (' + boat_Ids + ')';
      }
      bot_id = 0;
    }

    var cacheTempName = 'T_' + type + '_' + team_id + '_' + bot_id + '_' + `${moment(start_date).format('YYYY-MM-DD HH:mm:ss')}` + '_' + `${moment(end_date).format('YYYY-MM-DD HH:mm:ss')}`;

    sCache.get(cacheTempName, function(err, value) {
      if (!err) {
        if (value == undefined) {
          let sql;
          if (type && type != null && type != undefined && type != '' && type.toLowerCase() == 'time') {
            sql =
              `select
                DATE_FORMAT(DATE_FORMAT(timestamp, '%Y-%m-%d %H:00:00'),'%Y-%m-%dT%TZ') as tpsChartLabels,
                IFNULL((count(if(speaker='CHATBOT', speaker, null)) / 3600 ),0) as tpsChartData
              from 
                dlg_history_tb
              where timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                and timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`;
            if (arrayFlag) {
              sql += arraySql;
            } else {
              sql += `and bot_id =${bot_id}`;
            }
            sql += ' GROUP BY DATE_FORMAT(timestamp, \'%Y-%m-%d %H\')';
          } else {
            sql =
              `select
                DATE_FORMAT(CONVERT_TZ(timestamp, @@session.time_zone, '${app.getTz()}')  ,'%Y-%m-%dT00:00:00Z') as tpsChartLabels,
                IFNULL((count(if(speaker='CHATBOT', speaker, null)) / (3600 * 24)),0) as tpsChartData
              from 
                dlg_history_tb
              where 
                timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                and timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`;

            if (arrayFlag) {
              sql += arraySql;
            } else {
              sql += `and bot_id =${bot_id}`;
            }
            // sql += ' GROUP BY DATE_FORMAT(timestamp, \'%Y%m%d\')';
            sql += ' GROUP BY tpsChartLabels';
          }

          // console.log(sql);
          app.devlog(loggerName, sql);

          const connector = stats_dlg_history.app.datasources.statistic.connector;

          connector.query(sql, null, (err, result) => {
            if (err) {
              next(err);
            } else {
              var arrayData = [];

              if (result && result.length > 0) {
                for (var x = 0; x < result.length; x++) {
                  // console.log('result =', result[x]);
                  app.devlog(loggerName, 'result =', result[x]);
                  const temp = {period: result[x].tpsChartLabels, tps: result[x].tpsChartData};

                  arrayData.push(temp);
                }
              }
              // console.log('========================================================================================');
              // console.log('result =', arrayData);
              // console.log('========================================================================================');
              app.devlog(loggerName, '========================================================================================');
              app.devlog(loggerName, 'result =', arrayData);
              app.devlog(loggerName, '========================================================================================');

              sCache.set(cacheTempName, arrayData, function(err, success) {
                if (!err && success) {
                  // console.log(success);
                  app.devlog(loggerName, success);
                }
              });

              next(null, arrayData);
            }
          });
        } else {
          // console.log('cache value !!!', cacheTempName, value);
          app.devlog(loggerName, 'cache value !!!', cacheTempName, value);
          next(null, value);
        }
      }
    });
  };

  stats_dlg_history.remoteMethod(
    'getTpsChart', {
      http: {path: '/getTpsChart', verb: 'get'},
      accepts: [
        {arg: 'start_date', type: 'date', required: false},
        {arg: 'end_date', type: 'date', required: false},
        {arg: 'boat_Ids', type: 'string', required: false},
        {arg: 'type', type: 'string', required: false},
        {arg: 'team_id', type: 'number', required: false},
        {arg: 'bot_id', type: 'number'},
      ],
      returns: {root: true},
    }
  );

  stats_dlg_history.getDialogChart = function(start_date, end_date, boat_Ids,  type, team_id, bot_id, next) {
    // console.log('start_date :', start_date, `${moment(start_date).format('YYYY-MM-DD')}`);
    // console.log('end_date :', end_date, `${moment(end_date).format('YYYY-MM-DD')}`);
    app.devlog(loggerName, 'start_date :', start_date, `${moment(start_date).format('YYYY-MM-DD')}`);
    app.devlog(loggerName, 'end_date :', end_date, `${moment(end_date).format('YYYY-MM-DD')}`);

    var arrayFlag = false;
    var arraySql = '';

    if (bot_id && bot_id != null && bot_id != undefined && bot_id != 0) {
      arrayFlag = false;
    } else {
      arrayFlag = true;
      if (team_id && team_id != null && team_id != undefined && team_id != 0) {
        arraySql = 'and bot_id in (select bot_id from chatbot_info_tb where team_id =' + team_id + ')';
      } else {
        team_id = 0;
        arraySql = 'and bot_id in (' + boat_Ids + ')';
      }
      bot_id = 0;
    }

    var cacheTempName = 'D_' + type + '_' + team_id + '_' + bot_id + '_' + `${moment(start_date).format('YYYYMMDD')}` + '_' + `${moment(end_date).format('YYYYMMDD')}`;
    sCache.get(cacheTempName, function(err, value) {
      if (!err) {
        if (value == undefined) {
          let sql;
          if (type && type != null && type != undefined && type != '' && type.toLowerCase() == 'time') {
            sql =
              `select
                DATE_FORMAT(DATE_FORMAT(timestamp, '%Y-%m-%d %H:00:00'), '%Y-%m-%dT%TZ') as dialogChartLabels,
                count(if(speaker='CHATBOT', speaker, null)) as dialogChartData,
                count(if(msg_type_cd='MT0002', msg_type_cd, null)) as unknownCount
              from 
                dlg_history_tb
              where 
                timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                and timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`;
            if (arrayFlag) {
              sql += arraySql;
            } else {
              sql += `and bot_id =${bot_id}`;
            }
            sql += ' GROUP BY DATE_FORMAT(timestamp, \'%Y-%m-%d %H\')';
          } else {
            sql =
              `select
               -- DATE_FORMAT(CONVERT_TZ( DATE_FORMAT(timestamp, '%Y-%m-%d 00:00:00'), @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as dialogChartLabels,
                DATE_FORMAT(CONVERT_TZ(timestamp, @@session.time_zone, '${app.getTz()}')  ,'%Y-%m-%dT00:00:00Z') as dialogChartLabels,
                count(if(speaker='CHATBOT', speaker, null)) as dialogChartData,
                count(if(msg_type_cd='MT0002', msg_type_cd, null)) as unknownCount
               from 
                dlg_history_tb
              where 
                timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                and timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`;
            if (arrayFlag) {
              sql += arraySql;
            } else {
              sql += `and bot_id =${bot_id}`;
            }
            // sql += ' GROUP BY DATE_FORMAT(timestamp, \'%Y%m%d\')';
            sql += ' GROUP BY dialogChartLabels';
          }

          // console.log(sql);
          app.devlog(loggerName, sql);

          const connector = stats_dlg_history.app.datasources.statistic.connector;

          connector.query(sql, null, (err, result) => {
            if (err) {
              next(err);
            } else {
              var arrayPeriod = [];
              var arrayData = [];
              var arrayUnknown = [];

              var arrayData = [];

              if (result && result.length > 0) {
                for (var x = 0; x < result.length; x++) {
                  // console.log('result =', result[x]);
                  app.devlog(loggerName, 'result =', result[x]);
                  const temp = {period: result[x].dialogChartLabels, success: result[x].dialogChartData, fail: result[x].unknownCount, unknown: result[x].unknownCount};

                  arrayData.push(temp);
                }
              }
              // console.log('========================================================================================');
              // console.log('result =', arrayData);
              // console.log('========================================================================================');
              app.devlog(loggerName, '========================================================================================');
              app.devlog(loggerName, 'result =', arrayData);
              app.devlog(loggerName, '========================================================================================');

              sCache.set(cacheTempName, arrayData, function(err, success) {
                if (!err && success) {
                  // console.log(success);
                  app.devlog(loggerName, success);
                }
              });
              next(null, arrayData);
            }
          });
        } else {
          // console.log('cache value !!!', cacheTempName, value);
          app.devlog(loggerName, 'cache value !!!', cacheTempName, value);
          next(null, value);
        }
      }
    });
  };

  stats_dlg_history.remoteMethod(
    'getDialogChart', {
      http: {path: '/getDialogChart', verb: 'get'},
      accepts: [
        {arg: 'start_date', type: 'date', required: false},
        {arg: 'end_date', type: 'date', required: false},
        {arg: 'boat_Ids', type: 'string', required: false},
        {arg: 'type', type: 'string', required: false},
        {arg: 'team_id', type: 'number', required: false},
        {arg: 'bot_id', type: 'number'},
      ],
      returns: {root: true},
    }
  );

  stats_dlg_history.clearCache = function(next) {
    var data = myCache.getStats();
    // console.log('========================================================================================');
    // console.log('data =', data);
    // console.log('========================================================================================');
    app.devlog(loggerName, '========================================================================================');
    app.devlog(loggerName, 'data =', data);
    app.devlog(loggerName, '========================================================================================');
    sCache.flushAll();
    sCache.getStats();
    next(null, data);
  };
  stats_dlg_history.remoteMethod(
    'clearCache', {
      http: {path: '/clearCache', verb: 'get'},
      accepts: [
      ],
      returns: {root: true},
    }
  );
};
