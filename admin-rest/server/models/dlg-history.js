'use strict';

const app = require('../server');
const _ = require('lodash');
const moment = require('moment');
const path = require('path');

module.exports = function (Dlghistory) {
  const loggerName = path.basename(__filename, path.extname(__filename));
  const SUPERVISOR_TEAM_ID = 0;
  const isSupervisorTeam = (team_id) => team_id == SUPERVISOR_TEAM_ID;

  const sqlInner = `
    SELECT
        dh.session_id
      , dh.bot_id
      , (SELECT ci.bot_name FROM chatbot_info_tb ci WHERE ci.bot_id = dh.bot_id) as bot_name
      , COUNT(DISTINCT(
          SELECT dh_domain.domain_name
          FROM dlg_history_tb dh_domain
          WHERE dh_domain.utter_id = dh.utter_id
            AND dh_domain.session_id = dh.session_id
            AND dh_domain.dialog_seq = dh.dialog_seq
            AND dh_domain.domain_name != ''
        )) as domain_count
      , COUNT(IF(dh.speaker='CHATBOT', dh.speaker, null)) as dialog_count
      , COUNT(IF(dh.msg_type_cd='MT0002', dh.msg_type_cd, null)) as unknown_count
      , DATE_FORMAT( CONVERT_TZ(MIN(dh.\`timestamp\`), @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as open_dtm
      , DATE_FORMAT( CONVERT_TZ(MAX(dh.\`timestamp\`), @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as close_dtm
      , dh.user_id
      , dh.utter_id
      , dh.dialog_seq
      , dh.speaker
      , dh.sentence
      , DATE_FORMAT( CONVERT_TZ(dh.\`timestamp\`, @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as \`timestamp\`
    FROM
      dlg_history_tb dh
  `;
  Dlghistory.getSessionCount = function (start_date, end_date, searchOptions, team_id, next) {
    // const innerWhere = getInnerWhere(start_date, end_date, searchOptions, team_id);
    // const where = getWhere(start_date, end_date, searchOptions, team_id);
    // console.log("------------------------------------------------------------------------------------------");
    // console.log("searchOptions", JSON.parse(searchOptions));
    app.devlog(loggerName, "------------------------------------------------------------------------------------------");
    app.devlog(loggerName, "searchOptions", JSON.parse(searchOptions));
    const {
      session_id,
      bot_name,
      user_id,
      domain_count,
      dialog_count,
      unknown_count
    } = JSON.parse(searchOptions);
    // console.log("------------------------------------------------------------------------------------------");
    app.devlog(loggerName, "------------------------------------------------------------------------------------------");
    const connector = Dlghistory.app.datasources.mindslab.connector;

    var queryString =
        `SELECT COUNT(DISTINCT(session_id)) as count
            FROM dlg_history_tb
     WHERE 
          timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
          AND timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`;
    if (session_id) {
      queryString += ` AND session_id = '${session_id}' `;
    }
    if (user_id) {
      queryString += ` AND user_id = '${user_id}'`;
    }
    if (bot_name) {
      queryString += ` AND bot_id in (select bot_id from chatbot_info_tb where bot_name like '%${bot_name}%')`
    }
    if (team_id && team_id != null && team_id != undefined && team_id != 0) {
      queryString += ' AND bot_id in (select bot_id from chatbot_info_tb where team_id =' + team_id + ')';
    }


    if (domain_count) {
      queryString =
          `SELECT COUNT(*) as count FROM
      (SELECT
        COUNT(session_id) as count,
        COUNT(DISTINCT(if(domain_name != '', domain_name, null))) as domain_count
      FROM dlg_history_tb
      WHERE 
          timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
          AND timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}' group by session_id`;
      if (team_id && team_id != null && team_id != undefined && team_id != 0) {
        queryString += ' AND bot_id in (select bot_id from chatbot_info_tb where team_id =' + team_id + ')';
      }
      queryString += ' HAVING domain_count >=' + domain_count;
      queryString += ') A'
    }
    if (dialog_count) {
      queryString =
          `SELECT COUNT(*) as count FROM (
      SELECT
        COUNT(session_id) as count,
        COUNT(IF(speaker='CHATBOT', speaker, null)) as dialog_count
      FROM dlg_history_tb
      WHERE 
          timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
          AND timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}' group by session_id`;
      if (team_id && team_id != null && team_id != undefined && team_id != 0) {
        queryString += ' AND bot_id in (select bot_id from chatbot_info_tb where team_id =' + team_id + ')';
      }
      queryString += ' HAVING dialog_count >=' + dialog_count;
      queryString += ') A'
    }
    if (unknown_count) {
      queryString =
          `SELECT COUNT(*) as count FROM (
      SELECT
        COUNT(session_id) as count,
        COUNT(IF(msg_type_cd='MT0002', msg_type_cd, null)) as unknown_count
      FROM dlg_history_tb
      WHERE 
          timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
          AND timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}' group by session_id`;
      if (team_id && team_id != null && team_id != undefined && team_id != 0) {
        queryString += ' AND bot_id in (select bot_id from chatbot_info_tb where team_id =' + team_id + ')';
      }
      queryString += ' HAVING unknown_count >=' + unknown_count;
      queryString += ') A'
    }

    // console.log("------------------------------------------------------------------------------------------");
    // console.log(queryString);
    // console.log("------------------------------------------------------------------------------------------");
    app.devlog(loggerName, "------------------------------------------------------------------------------------------");
    app.devlog(loggerName, queryString);
    app.devlog(loggerName, "------------------------------------------------------------------------------------------");



    connector.query(queryString, null, (err, resultObjects) => {
      if (err) next(err);
      // console.log(resultObjects[0]);
      app.devlog(loggerName, resultObjects[0]);
      next(null, resultObjects[0]);
    });
  };
  Dlghistory.getSessionList = function (pg = 1, pg_size = 10, start_date, end_date, searchOptions, team_id, next) {
    // const innerWhere = getInnerWhere(start_date, end_date, searchOptions, team_id);
    // const where = getWhere(start_date, end_date, searchOptions, team_id);
    // const {
    //   orderColumn,
    //   direction
    // } = getOrders(searchOptions);
    // console.log("------------------------------------------------------------------------------------------");
    // console.log("searchOptions", JSON.parse(searchOptions));
    app.devlog(loggerName, "------------------------------------------------------------------------------------------");
    app.devlog(loggerName, "searchOptions", JSON.parse(searchOptions));
    const {
      session_id,
      bot_name,
      user_id,
      domain_count,
      dialog_count,
      unknown_count
    } = JSON.parse(searchOptions);
    // console.log("------------------------------------------------------------------------------------------");
    app.devlog(loggerName, "------------------------------------------------------------------------------------------");
    const startIdx = (pg_size * (pg - 1));
    const connector = Dlghistory.app.datasources.mindslab.connector;

    var queryString =
        ` 
    select * , chatbot_info_tb.bot_name as bot_name from 
    (select utter_id, session_id
          , dlg_history_tb.bot_id as bot_id
           , COUNT(DISTINCT(if(domain_name != '', domain_name, null))) as domain_count
            , COUNT(IF(speaker='CHATBOT', speaker, null)) as dialog_count
            , COUNT(IF(msg_type_cd='MT0002', msg_type_cd, null)) as unknown_count
            , DATE_FORMAT( CONVERT_TZ(MIN(timestamp), @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as open_dtm
          , DATE_FORMAT( CONVERT_TZ(MAX(timestamp), @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as close_dtm
          , user_id
          from
          dlg_history_tb
     WHERE
          timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
          AND timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`
    if (team_id && team_id != null && team_id != undefined && team_id != 0) {
      queryString += 'AND bot_id in (select bot_id from chatbot_info_tb where team_id =' + team_id + ')';
    }
    if (session_id) {
      queryString += ` AND session_id = '${session_id}' `;
    }
    if (user_id) {
      queryString += ` AND user_id = '${user_id}'`;
    }
    if (bot_name) {
      queryString += ` AND bot_id in (select bot_id from chatbot_info_tb where bot_name like '%${bot_name}%')`
    }

    queryString += ' group by session_id';

    if (domain_count) {
      queryString += ' HAVING domain_count >=' + domain_count;
    }
    if (dialog_count) {
      queryString += ' HAVING dialog_count >=' + dialog_count;
    }
    if (unknown_count) {
      queryString += ' HAVING unknown_count >=' + unknown_count;
    }
    queryString += ` ORDER BY utter_id desc
                LIMIT ${startIdx},${pg_size}
              ) A LEFT JOIN chatbot_info_tb ON A.bot_id = chatbot_info_tb.bot_id`;

    // console.log("------------------------------------------------------------------------------------------");
    // console.log(queryString);
    // console.log("------------------------------------------------------------------------------------------");
    app.devlog(loggerName, "------------------------------------------------------------------------------------------");
    app.devlog(loggerName, queryString);
    app.devlog(loggerName, "------------------------------------------------------------------------------------------");


    connector.query(queryString, null, (err, resultObjects) => {
      if (err) next(err);
      // console.log("resultObjects", resultObjects);
      next(null, resultObjects);

    });
  };




  const getInnerWhere = (start_date, end_date, searchOptions, team_id) => {
    const {
      session_id,
      user_id
    } = JSON.parse(searchOptions);
    const searchOptionList = [];
    // searchOptionList.push(`dh.session_id != 'system.0000000000'`);

    session_id && searchOptionList.push(`dh.session_id = '${session_id}'`);
    if (isSupervisorTeam(team_id) || team_id) {
      const bot_where = isSupervisorTeam(team_id) ? '' : ` WHERE team_id = '${team_id}'`;
      searchOptionList.push(`dh.bot_id IN (SELECT bot_id FROM chatbot_info_tb${bot_where})`);
    }
    start_date && searchOptionList.push(`dh.timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`);
    end_date && searchOptionList.push(`dh.timestamp <= '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`);
    user_id && searchOptionList.push(`dh.user_id = ${user_id}`);

    const where = searchOptionList.length > 0 ? `WHERE ${searchOptionList.join('\n    AND ')}` : '';
    return where;
  };
  const getWhere = (start_date, end_date, searchOptions, team_id) => {
    const {
      session_id,
      bot_name,
      user_id,
      domain_count,
      dialog_count,
      unknown_count
    } = JSON.parse(searchOptions);
    const searchOptionList = [];

    session_id && searchOptionList.push(`session_id = '${session_id}'`);
    if (isSupervisorTeam(team_id) || team_id) {
      const bot_where = isSupervisorTeam(team_id) ? '' : ` WHERE ci.team_id = '${team_id}'`;
      searchOptionList.push(`bot_id IN (SELECT ci.bot_id FROM chatbot_info_tb ci${bot_where})`);
    }
    start_date && searchOptionList.push(`open_dtm >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`);
    end_date && searchOptionList.push(`close_dtm <= '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`);
    bot_name && searchOptionList.push(`bot_name like '%${bot_name}%'`);
    user_id && searchOptionList.push(`user_id = ${user_id}`);
    domain_count && searchOptionList.push(`domain_count >= ${domain_count}`);
    dialog_count && searchOptionList.push(`dialog_count >= ${dialog_count}`);
    unknown_count && searchOptionList.push(`unknown_count >= ${unknown_count}`);

    const where = searchOptionList.length > 0 ? `WHERE ${searchOptionList.join('\n    AND ')}` : '';
    return where;
  };


  const getOrders = (searchOptions) => {
    const SET_SEARCH_OPTIONS = {
      session_id: {
        key: 'session_id',
        orderColumn: 'utter_id',
        direction: 'desc'
      },
      bot_name: {
        key: 'bot_name',
        orderColumn: 'bot_name',
        direction: 'asc'
      },
      user_id: {
        key: 'user_id',
        orderColumn: 'user_id',
        direction: 'asc'
      },
      domain_count: {
        key: 'domain_count',
        orderColumn: 'domain_count',
        direction: 'desc'
      },
      dialog_count: {
        key: 'dialog_count',
        orderColumn: 'dialog_count',
        direction: 'desc'
      },
      unknown_count: {
        key: 'unknown_count',
        orderColumn: 'unknown_count',
        direction: 'desc'
      }
      // ,sentence:      { key: 'sentence',      orderColumn: 'dh_g.sentence',   direction: 'desc' }
      // ,speaker:       { key: 'speaker',       orderColumn: 'dh_g.speaker',    direction: 'desc' }
    };

    const searchOptionsObj = JSON.parse(searchOptions);
    for (const optionKey in searchOptionsObj) {
      if (SET_SEARCH_OPTIONS[optionKey] &&
          SET_SEARCH_OPTIONS[optionKey].key == optionKey
      ) {
        const {
          orderColumn,
          direction
        } = SET_SEARCH_OPTIONS[optionKey];
        return {
          orderColumn,
          direction
        };
      }
    }
    const {
      orderColumn,
      direction
    } = SET_SEARCH_OPTIONS['session_id'];
    return {
      orderColumn,
      direction
    };
  };



  Dlghistory.getDlgErrorCount = function (start_date, end_date, searchOptions, team_id, next) {
    app.devlog(loggerName, "------------------------------------------------------------------------------------------");
    app.devlog(loggerName, "searchOptions", searchOptions);


    const {
      utter_id,
      bot_id,
      botName,
      sentence,
      user_id
    } = JSON.parse(searchOptions);


    app.devlog(loggerName, "------------------------------------------------------------------------------------------");

    const connector = Dlghistory.app.datasources.mindslab.connector;

    var queryString =
        `select count(bot_id) as count
         from(
                   select s.bot_id,
                           s.user_id,
                           (select bot_name from chatbot_info_tb b where b.bot_id = s.bot_id) as botName,
                           (select sentence from dlg_history_tb where  s.dialog_seq = dialog_seq and  s.session_id =session_id and s.utter_id = utter_id ) as sentence,
                           (select utter_id from dlg_history_tb where  s.dialog_seq = dialog_seq and  s.session_id =session_id and s.utter_id = utter_id  ) as utter_id,
                           (select timestamp from dlg_history_tb where  s.dialog_seq = dialog_seq and  s.session_id =session_id  and s.utter_id = utter_id  ) as timestamp
                   from (
                            select bot_id, user_id , session_id, (dialog_seq -1)  as dialog_seq, (utter_id-1) as utter_id, speaker, sentence, timestamp
                            from dlg_history_tb
                            where msg_type_cd = 'MT0002'
                                   and timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                                   and timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                                  and bot_id in (select bot_id  from chatbot_info_tb  where team_id = 4)
                         )s
               )e
          where 1 =1`;


    if (utter_id && utter_id != 0) {
      queryString += `  AND  e.utter_id = ${utter_id}`;
    }

    if(bot_id && bot_id != 0) {
      queryString += `  AND  e.bot_id =  ${bot_id}`;
    }

    if(botName && botName.length != 0) {
      queryString += `  AND  e.botName like '%${botName}%'`;
    }

    if (sentence && sentence.length != 0) {
      queryString += `  AND  e.sentence like '%${sentence}%'`;
    }

    if (user_id && user_id.length != 0) {
      queryString += `  AND  e.user_id = '${user_id}'`;
    }



    app.devlog(loggerName, "------------------------------------------------------------------------------------------");
    app.devlog(loggerName, queryString);
    app.devlog(loggerName, "------------------------------------------------------------------------------------------");


    connector.query(queryString, null, (err, resultObjects) => {
      if (err) next(err);
      // console.log(resultObjects[0]);
      app.devlog(loggerName, resultObjects[0]);
      next(null, resultObjects[0]);
    });
  };




  Dlghistory.getDlgErrorList = function (pg, pg_size, start_date, end_date, searchOptions, team_id, next) {
    app.devlog(loggerName, "------------------------------------------------------------------------------------------");
    app.devlog(loggerName, "searchOptions", searchOptions);


    const {
      utter_id,
      bot_id,
      botName,
      sentence,
      user_id
    } = JSON.parse(searchOptions);


    app.devlog(loggerName, "------------------------------------------------------------------------------------------");

    const startIdx = (pg_size * (pg - 1));

    const connector = Dlghistory.app.datasources.mindslab.connector;

    var queryString =
        `select e.utter_id,
                 e.botName,
                 e.sentence,
                 e.user_id,
                 DATE_FORMAT(TIMESTAMP ,'%Y-%m-%dT%TZ') as dlgTime
         from(
                   select  s.bot_id,
                          s.user_id,
                          (select bot_name from chatbot_info_tb b where b.bot_id = s.bot_id) as botName,
                          (select sentence from dlg_history_tb where  s.dialog_seq = dialog_seq and  s.session_id =session_id and s.utter_id = utter_id ) as sentence,
                          (select utter_id from dlg_history_tb where  s.dialog_seq = dialog_seq and  s.session_id =session_id and s.utter_id = utter_id  ) as utter_id,
                          (select timestamp from dlg_history_tb where  s.dialog_seq = dialog_seq and  s.session_id =session_id  and s.utter_id = utter_id  ) as timestamp
                   from (
                            select bot_id, user_id , session_id, (dialog_seq -1)  as dialog_seq, (utter_id-1) as utter_id, speaker, sentence, timestamp
                            from dlg_history_tb
                            where msg_type_cd = 'MT0002'
                                   and timestamp >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                                   and timestamp < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                                  and bot_id in (select bot_id  from chatbot_info_tb  where team_id = 4)
                         )s
               )e
             where 1 =1 `;


    if (utter_id && utter_id != 0) {
      queryString += `  AND  e.utter_id = ${utter_id}`;
    }

    if(bot_id && bot_id != 0) {
      queryString += `  AND  e.bot_id =  ${bot_id}`;
    }

    if(botName && botName.length != 0) {
      queryString += `  AND  e.botName like '%${botName}%'`;
    }

    if (sentence && sentence.length != 0) {
      queryString += `  AND  e.sentence like '%${sentence}%'`;
    }

    if (user_id && user_id.length != 0) {
      queryString += `  AND  e.user_id = '${user_id}'`;
    }


    queryString += ` ORDER BY e.timestamp desc
                        LIMIT ${startIdx},${pg_size}`;

    app.devlog(loggerName, "------------------------------------------------------------------------------------------");
    app.devlog(loggerName, queryString);
    app.devlog(loggerName, "------------------------------------------------------------------------------------------");


    connector.query(queryString, null, (err, resultObjects) => {
      if (err) next(err);
      // console.log(resultObjects[0]);
      app.devlog(loggerName, resultObjects);
      next(null, resultObjects);
    });
  };


  Dlghistory.remoteMethod(
      'getSessionCount', {
        http: {
          path: '/getSessionCount',
          verb: 'get'
        },
        accepts: [{
          arg: 'start_date',
          type: 'date',
          required: false
        },
          {
            arg: 'end_date',
            type: 'date',
            required: false
          },
          {
            arg: 'searchOptions',
            type: 'string',
            required: false
          },
          {
            arg: 'team_id',
            type: 'string',
            required: false
          },
        ],
        returns: {
          root: true
        },
      }
  );
  Dlghistory.remoteMethod(
      'getSessionList', {
        http: {
          path: '/getSessionList',
          verb: 'get'
        },
        accepts: [{
          arg: 'pg',
          type: 'number',
          required: true,
          default: 1
        },
          {
            arg: 'pg_size',
            type: 'number',
            required: true,
            default: 10
          },
          {
            arg: 'start_date',
            type: 'date',
            required: false
          },
          {
            arg: 'end_date',
            type: 'date',
            required: false
          },
          {
            arg: 'searchOptions',
            type: 'string',
            required: false
          },
          {
            arg: 'team_id',
            type: 'string',
            required: false
          }
        ],
        returns: {
          root: true
        },
      }
  );


  Dlghistory.remoteMethod(
      'getDlgErrorCount', {
        http: {
          path: '/getDlgErrorCount',
          verb: 'get'
        },
        accepts: [{
          arg: 'start_date',
          type: 'date',
          required: false
        },
          {
            arg: 'end_date',
            type: 'date',
            required: false
          },
          {
            arg: 'searchOptions',
            type: 'string',
            required: false
          },
          {
            arg: 'team_id',
            type: 'string',
            required: false
          },
        ],
        returns: {
          root: true
        },
      }
  );


  Dlghistory.remoteMethod(
      'getDlgErrorList', {
        http: {
          path: '/getDlgErrorList',
          verb: 'get'
        },
        accepts: [{
          arg: 'pg',
          type: 'number',
          required: true,
          default: 1
        },

          {
            arg: 'pg_size',
            type: 'number',
            required: true,
            default: 10
          },
          {
            arg: 'start_date',
            type: 'date',
            required: false
          },
          {
            arg: 'end_date',
            type: 'date',
            required: false
          },
          {
            arg: 'searchOptions',
            type: 'string',
            required: false
          },
          {
            arg: 'team_id',
            type: 'string',
            required: false
          },
        ],
        returns: {
          root: true
        },
      }
  );
};
