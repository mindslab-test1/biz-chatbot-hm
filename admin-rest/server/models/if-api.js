'use strict';
const app = require('../server');
const path = require('path');

module.exports = function (if_api) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  var Transaction = require('loopback-datasource-juggler').Transaction;

  if_api.addApi = function (ifApi, resList, reqList, jsonTemplate, workspace_id, next) {

    if_api.beginTransaction({
      isolationLevel: Transaction.READ_COMMITTED,
      timeout: 30000 // 30000ms = 30s
    }, function (err, tx) {
      var if_id;
      var userId;
      var useJsonTempYn = 'N';
      // console.log("========================================================================================");
      // console.log("ifApi =", ifApi);
      // console.log("resList =", resList);
      // console.log("reqList =", reqList);
      // console.log("jsonTemplate =", jsonTemplate);
      // console.log("========================================================================================");
      app.devlog(loggerName, "========================================================================================");
      app.devlog(loggerName, "ifApi =", ifApi);
      app.devlog(loggerName, "resList =", resList);
      app.devlog(loggerName, "reqList =", reqList);
      app.devlog(loggerName, "jsonTemplate =", jsonTemplate);
      app.devlog(loggerName, "========================================================================================");

      /* API insert */
      if_api.upsert(ifApi, { transaction: tx }, function (err, res) {
        // console.log("========================================================================================");
        // console.log("if_api result = ", res)
        // console.log("if_api err = ", err)
        // console.log("========================================================================================");
        app.devlog(loggerName, "========================================================================================");
        app.devlog(loggerName, "if_api result = ", res)
        app.devlog(loggerName, "if_api err = ", err)
        app.devlog(loggerName, "========================================================================================");
        if (err) {
          tx.rollback();
          next(err);
        } else {
          if_id = res.if_id;
          userId = res.creator_id;
          useJsonTempYn = res.use_json_temp_yn;
          if (resList) {

            for (var x = 0; x < resList.length; x++) {
              resList[x].if_id = if_id;
              resList[x].creator_id = userId;
              resList[x].updator_id = userId;
            }
            // console.log("========================================================================================");
            // console.log("resList = ", resList)
            // console.log("========================================================================================");
            app.devlog(loggerName, "========================================================================================");
            app.devlog(loggerName, "resList = ", resList)
            app.devlog(loggerName, "========================================================================================");
            /* response insert */
            app.models.if_api_result_mapping.create(resList, { transaction: tx }, function (err, res) {
              // console.log("========================================================================================");
              // console.log("if_api_result_mapping result = ", res)
              // console.log("if_api_result_mapping err = ", err)
              // console.log("========================================================================================");
              app.devlog(loggerName, "========================================================================================");
              app.devlog(loggerName, "if_api_result_mapping result = ", res)
              app.devlog(loggerName, "if_api_result_mapping err = ", err)
              app.devlog(loggerName, "========================================================================================");
              if (err) {
                tx.rollback();
                next(err);
              } else {

                if (jsonTemplate && useJsonTempYn == 'Y') {
                  jsonTemplate.if_id = if_id;
                  jsonTemplate.creator_id = userId;
                  jsonTemplate.updator_id = userId;
                  app.models.if_api_json_template.create(jsonTemplate, { transaction: tx }, function (err, res) {
                    if (err) {
                      next(err);
                      tx.rollback();
                    } else {
                      if (reqList) {

                        for (var x = 0; x < reqList.length; x++) {
                          reqList[x].if_id = if_id;
                          reqList[x].param_order = x + 1;
                          reqList[x].creator_id = userId;
                          reqList[x].updator_id = userId;
                        }

                        app.models.if_api_params.create(reqList, { transaction: tx }, function (err, res) {
                          if (err) {
                            next(err);
                            tx.rollback();
                          } else {
                            next(null, res);
                            tx.commit();

                          }
                        });
                      } else {
                        next(null, res);
                        tx.commit();
                      }
                    }
                  });
                } else {
                  if (reqList) {
                    for (var x = 0; x < reqList.length; x++) {
                      reqList[x].if_id = if_id;
                      reqList[x].param_order = x + 1;
                      reqList[x].creator_id = userId;
                      reqList[x].updator_id = userId;
                    }

                    app.models.if_api_params.create(reqList, { transaction: tx }, function (err, res) {
                      if (err) {
                        next(err);
                        tx.rollback();
                      } else {
                        next(null, res);
                        tx.commit();

                      }
                    });

                  } else {
                    next(null, res);
                    tx.commit();
                  }
                }
              }
            });
          }
        }
      });

      // tx.commit();
      tx.observe('timeout', function (context, next) {
        // console.log("timeout!!");
        app.devlog(loggerName, "timeout!!");
        next();
      });


      tx.observe('before commit', function (context, next) {
        // ...
        next(null, ifApi);
      });

      tx.observe('after commit', function (context, next) {
        next(null, ifApi);
      });

      tx.observe('before rollback', function (context, next) {
        // ...
        next();
      });

      tx.observe('after rollback', function (context, next) {
        // ...
        next();
      });
    });

  };


  if_api.updateApi = function (ifApi, resList, reqList, jsonTemplate, workspace_id, next) {

    if_api.beginTransaction({
      isolationLevel: Transaction.READ_COMMITTED,
      timeout: 30000 // 30000ms = 30s
    }, function (err, tx) {
      var updateIfApi = new Object();
      updateIfApi.if_id = ifApi.if_id;
      updateIfApi.api_name = ifApi.api_name
      updateIfApi.description = ifApi.description
      updateIfApi.endpoint = ifApi.endpoint
      updateIfApi.content_type = ifApi.content_type
      updateIfApi.use_json_temp_yn = ifApi.use_json_temp_yn
      updateIfApi.method = ifApi.method
      updateIfApi.updated_dtm = new Date();
      updateIfApi.updator_id = ifApi.updator_id

      var if_id = ifApi.if_id;
      var userId = ifApi.updator_id;
      var useJsonTempYn = ifApi.use_json_temp_yn;

      // console.log("========================================================================================");
      // console.log("ifApi =", ifApi);
      // console.log("resList =", resList);
      // console.log("reqList =", reqList);
      // console.log("jsonTemplate =", jsonTemplate);
      // console.log("========================================================================================");
      app.devlog(loggerName, "========================================================================================");
      app.devlog(loggerName, "ifApi =", ifApi);
      app.devlog(loggerName, "resList =", resList);
      app.devlog(loggerName, "reqList =", reqList);
      app.devlog(loggerName, "jsonTemplate =", jsonTemplate);
      app.devlog(loggerName, "========================================================================================");

      if_api.upsertWithWhere({ if_id: if_id }, updateIfApi, { transaction: tx }, function (err, res) {

        if (err) {
          next(err);
          tx.rollback();
        } else {
          // next(null, res);
          if (resList) {
            app.models.if_api_result_mapping.destroyAll({ if_id: if_id }, { transaction: tx }, function (err, res) {
              if (err) {
                next(err);
                tx.rollback();
              } else {
                for (var x = 0; x < resList.length; x++) {
                  resList[x].if_id = if_id;
                  resList[x].creator_id = userId;
                  resList[x].updator_id = userId;
                }
                /* response insert */
                app.models.if_api_result_mapping.create(resList, { transaction: tx }, function (err, res) {
                  if (err) {
                    next(err);
                    tx.rollback();
                  } else {
                    if (jsonTemplate && useJsonTempYn == 'Y') {

                      app.models.if_api_json_template.destroyAll({ if_id: if_id }, { transaction: tx }, function (err, res) {
                        if (err) {
                          next(err);
                          tx.rollback();
                        } else {

                          jsonTemplate.if_id = if_id;
                          jsonTemplate.creator_id = userId;
                          jsonTemplate.updator_id = userId;
                          app.models.if_api_json_template.create(jsonTemplate, { transaction: tx }, function (err, res) {
                            if (err) {
                              next(err);
                              tx.rollback();
                            } else {
                              if (reqList) {
                                app.models.if_api_params.destroyAll({ if_id: if_id }, { transaction: tx }, function (err, res) {
                                  if (err) {
                                    next(err);
                                    tx.rollback();
                                  } else {
                                    for (var x = 0; x < reqList.length; x++) {
                                      reqList[x].if_id = if_id;
                                      reqList[x].param_order = x + 1;
                                      reqList[x].creator_id = userId;
                                      reqList[x].updator_id = userId;
                                    }

                                    app.models.if_api_params.create(reqList, { transaction: tx }, function (err, res) {
                                      if (err) {
                                        next(err);
                                        tx.rollback();
                                      } else {
                                        next(null, res);
                                        tx.commit();

                                      }
                                    });
                                  }
                                });
                              } else {
                                next(null, res);
                                tx.commit();
                              }
                            }
                          });
                        }
                      });
                    } else {
                      if (reqList) {
                        app.models.if_api_params.destroyAll({ if_id: if_id }, { transaction: tx }, function (err, res) {
                          if (err) {
                            next(err);
                            tx.rollback();
                          } else {
                            for (var x = 0; x < reqList.length; x++) {
                              reqList[x].if_id = if_id;
                              reqList[x].param_order = x + 1;
                              reqList[x].creator_id = userId;
                              reqList[x].updator_id = userId;
                            }

                            app.models.if_api_params.create(reqList, { transaction: tx }, function (err, res) {
                              if (err) {
                                next(err);
                                tx.rollback();
                              } else {
                                next(null, res);
                                tx.commit();

                              }
                            });
                          }
                        });
                      } else {
                        next(null, res);
                        tx.commit();
                      }
                    }
                  }
                });
              }
            });
          } else {
            next(err);
            tx.rollback();
          }
        }
      });


      // tx.commit();
      tx.observe('timeout', function (context, next) {
        // console.log("timeout!!");
        app.devlog(loggerName, "timeout!!");
        next();
      });



      tx.observe('after commit', function (context, next) {
        // console.log("========================================================================================");
        // console.log("After Commit")
        // console.log("========================================================================================");
        app.devlog(loggerName, "========================================================================================");
        app.devlog(loggerName, "After Commit")
        app.devlog(loggerName, "========================================================================================");
        next(null, "Success");
      });



      tx.observe('after rollback', function (context, next) {
        // console.log("========================================================================================");
        // console.log("After rollback", context)
        // console.log("========================================================================================");
        app.devlog(loggerName, "========================================================================================");
        app.devlog(loggerName, "After rollback", context)
        app.devlog(loggerName, "========================================================================================");
        next();
      });
    });

  };

  if_api.remoteMethod(
    'addApi', {
      accepts: [
        { arg: 'ifApi', type: 'object', required: true },
        { arg: 'resList', type: 'array' },
        { arg: 'reqList', type: 'array' },
        { arg: 'jsonTemplate', type: 'object' },
        { arg: 'workspace_id', type: 'string' }

        // {arg: 'ifApi', type: ['object'], required: true},
        // {arg: 'arr', type: ['array'] }
      ],
      returns: { root: true },
      isStatic: true
    }
  );
  if_api.remoteMethod('updateApi', {
    accepts: [
      { arg: 'ifApi', type: 'object', required: true },
      { arg: 'resList', type: 'array' },
      { arg: 'reqList', type: 'array' },
      { arg: 'jsonTemplate', type: 'object' },
      { arg: 'workspace_id', type: 'string' }

    ],
    returns: { root: true },
    isStatic: true
  });



  if_api.remoteMethod('deleteApi', {
    accepts: [
      { arg: 'ifId', type: 'number', required: true },
      { arg: 'workspace_id', type: 'number', required: false }
    ],
    returns: { root: true },
    isStatic: true
  });

  if_api.deleteApi = function (ifId, workspace_id, next) {

    if_api.beginTransaction({
      isolationLevel: Transaction.READ_COMMITTED,
      timeout: 30000 // 30000ms = 30s
    }, function (err, tx) {

      // console.log("========================================================================================");
      // console.log("ifId =", ifId);
      // console.log("========================================================================================");
      app.devlog(loggerName, "========================================================================================");
      app.devlog(loggerName, "ifId =", ifId);
      app.devlog(loggerName, "========================================================================================");

      if_api.destroyAll({ if_id: ifId }, { transaction: tx }, function (err, res) {
        if (err) {
          next(err);
          tx.rollback();
        } else {
          // console.log("========================================================================================");
          // console.log("if_api res =", res);
          // console.log("========================================================================================");
          app.devlog(loggerName, "========================================================================================");
          app.devlog(loggerName, "if_api res =", res);
          app.devlog(loggerName, "========================================================================================");
          app.models.if_api_json_template.destroyAll({ if_id: ifId }, { transaction: tx }, function (err, res) {
            if (err) {
              next(err);
              tx.rollback();
            } else {
              // console.log("========================================================================================");
              // console.log("if_api_json_template res =", res);
              // console.log("========================================================================================");
              app.devlog(loggerName, "========================================================================================");
              app.devlog(loggerName, "if_api_json_template res =", res);
              app.devlog(loggerName, "========================================================================================");
              app.models.if_api_params.destroyAll({ if_id: ifId }, { transaction: tx }, function (err, res) {
                if (err) {
                  next(err);
                  tx.rollback();
                } else {
                  // console.log("========================================================================================");
                  // console.log("if_api_params res =", res);
                  // console.log("========================================================================================");
                  app.devlog(loggerName, "========================================================================================");
                  app.devlog(loggerName, "if_api_params res =", res);
                  app.devlog(loggerName, "========================================================================================");
                  next(null, res);
                  tx.commit();
                }
              });
            }
          });
        }
      });
    });
  };
};
