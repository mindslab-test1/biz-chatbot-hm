// /* eslint-disable camelcase */
// 'use strict';

// const path = require('path');
// const gcd = require('../lib/grpc-client-delegate');
// const app = require('../../server/server');
// const PROTO_ROOT = path.join(process.env.MAUM_ROOT, app.settings.grpc.protos.entry);
// const GRPC_HOSTS = app.settings.grpc.hosts;


// /** make entry points **/
// module.exports = function(Grpc) {
//   function prepareGrpcClientDelegate() {
//     let result = gcd.init(PROTO_ROOT, GRPC_HOSTS);

//     if (result.error) {
//       console.log('\x1b[93m\x1b[41mFailed to prepare gRPC client delegate.\x1b[0m', result.error);
//     } else {
//       console.log('\x1b[32m<< gRPC Client Delegate is READY >>\x1b[0m');
//     }
//   }

//   prepareGrpcClientDelegate();


//   function callConsoleFunction(service_func, param) {
//     return gcd.callFunction('maum.m2u.console.' + service_func, param);
//   }


//   /*** DialogAgentManager APIs ***/

//   Grpc.getDialogAgentManagerAllList = function(next) {
//     return callConsoleFunction('DialogAgentManagerAdmin/getDialogAgentManagerAllList');
//   };

//   Grpc.remoteMethod(
//     'getDialogAgentManagerAllList', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getDialogAgentManagerInfo = function(Key, next) {
//     return callConsoleFunction('DialogAgentManagerAdmin/getDialogAgentManagerInfo', {name: Key});
//   };

//   Grpc.remoteMethod(
//     'getDialogAgentManagerInfo', {
//       accepts: [
//         {arg: 'Key', type: 'string', required: true},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getDialogAgentManagerList = function(Key, next) {
//     return callConsoleFunction('DialogAgentManagerAdmin/getDialogAgentManagerList', {name: Key});
//   };

//   Grpc.remoteMethod(
//     'getDialogAgentManagerList', {
//       accepts: [
//         {arg: 'Key', type: 'string', required: true},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.insertDialogAgentManagerInfo = function(DialogAgentManager, next) {
//     return callConsoleFunction('DialogAgentManagerAdmin/insertDialogAgentManagerInfo', DialogAgentManager);
//   };

//   Grpc.remoteMethod(
//     'insertDialogAgentManagerInfo', {
//       accepts: [
//         {arg: 'DialogAgentManager', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getDialogAgentWithDialogAgentInstanceList = function(Key, next) {
//     return callConsoleFunction('DialogAgentManagerAdmin/getDialogAgentWithDialogAgentInstanceList', {name: Key});
//   };

//   Grpc.remoteMethod(
//     'getDialogAgentWithDialogAgentInstanceList', {
//       accepts: [
//         {arg: 'Key', type: 'string', required: true},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.updateDialogAgentManagerInfo = function(DialogAgentManager, next) {
//     return callConsoleFunction('DialogAgentManagerAdmin/updateDialogAgentManagerInfo', DialogAgentManager);
//   };

//   Grpc.remoteMethod(
//     'updateDialogAgentManagerInfo', {
//       accepts: [
//         {arg: 'DialogAgentManager', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.deleteDialogAgentManager = function(KeyList, next) {
//     return callConsoleFunction('DialogAgentManagerAdmin/deleteDialogAgentManager', KeyList);
//   };

//   Grpc.remoteMethod(
//     'deleteDialogAgentManager', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.stopDialogAgentManager = function(KeyList, next) {
//     return callConsoleFunction('DialogAgentManagerAdmin/stopDialogAgentManager', KeyList);
//   };

//   Grpc.remoteMethod(
//     'stopDialogAgentManager', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.startDialogAgentManager = function(KeyList, next) {
//     return callConsoleFunction('DialogAgentManagerAdmin/startDialogAgentManager', KeyList);
//   };

//   Grpc.remoteMethod(
//     'startDialogAgentManager', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.restartDialogAgentManager = function(KeyList, next) {
//     return callConsoleFunction('DialogAgentManagerAdmin/restartDialogAgentManager', KeyList);
//   };

//   Grpc.remoteMethod(
//     'restartDialogAgentManager', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );


//   /*** DialogAgent APIs ***/

//   Grpc.getDialogAgentAllList = function(next) {
//     return callConsoleFunction('DialogAgentAdmin/getDialogAgentAllList');
//   };

//   Grpc.remoteMethod(
//     'getDialogAgentAllList', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getDialogAgentInfo = function(Key, next) {
//     return callConsoleFunction('DialogAgentAdmin/getDialogAgentInfo', {name: Key});
//   };

//   Grpc.remoteMethod(
//     'getDialogAgentInfo', {
//       accepts: [
//         {arg: 'Key', type: 'string', required: true},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getDialogAgentActivationInfoList = function(Key, next) {
//     return callConsoleFunction('DialogAgentAdmin/getDialogAgentActivationInfoList', {name: Key});
//   };

//   Grpc.remoteMethod(
//     'getDialogAgentActivationInfoList', {
//       accepts: [
//         {arg: 'Key', type: 'string', required: true},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.insertDialogAgentInfo = function(DialogAgentInfo, next) {
//     return callConsoleFunction('DialogAgentAdmin/insertDialogAgentInfo', DialogAgentInfo);
//   };

//   Grpc.remoteMethod(
//     'insertDialogAgentInfo', {
//       accepts: [
//         {arg: 'DialogAgentInfo', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.updateDialogAgentInfo = function(DialogAgentInfo, next) {
//     return callConsoleFunction('DialogAgentAdmin/updateDialogAgentInfo', DialogAgentInfo);
//   };

//   Grpc.remoteMethod(
//     'updateDialogAgentInfo', {
//       accepts: [
//         {arg: 'DialogAgentInfo', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.deleteDialogAgent = function(KeyList, next) {
//     return callConsoleFunction('DialogAgentAdmin/deleteDialogAgent', KeyList);
//   };

//   Grpc.remoteMethod(
//     'deleteDialogAgent', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getRuntimeParameters = function(DialogAgentName, next) {
//     return callConsoleFunction('DialogAgentAdmin/getRuntimeParameters', DialogAgentName);
//   };

//   Grpc.remoteMethod(
//     'getRuntimeParameters', {
//       accepts: [
//         {arg: 'DialogAgentName', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getUserAttributes = function(DialogAgentName, next) {
//     return callConsoleFunction('DialogAgentAdmin/getUserAttributes', DialogAgentName);
//   };

//   Grpc.remoteMethod(
//     'getUserAttributes', {
//       accepts: [
//         {arg: 'DialogAgentName', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getExecutableDA = function(next) {
//     return callConsoleFunction('DialogAgentAdmin/getExecutableDa');
//   };

//   Grpc.remoteMethod(
//     'getExecutableDA', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.stopDialogAgent = function(KeyList, next) {
//     return callConsoleFunction('DialogAgentAdmin/stopDialogAgent', KeyList);
//   };

//   Grpc.remoteMethod(
//     'stopDialogAgent', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.startDialogAgent = function(KeyList, next) {
//     return callConsoleFunction('DialogAgentAdmin/startDialogAgent', KeyList);
//   };

//   Grpc.remoteMethod(
//     'startDialogAgent', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.restartDialogAgent = function(KeyList, next) {
//     return callConsoleFunction('DialogAgentAdmin/restartDialogAgent', KeyList);
//   };

//   Grpc.remoteMethod(
//     'restartDialogAgent', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );


//   /*** Chatbot APIs ***/

//   Grpc.getChatbotAllList = function(next) {
//     return callConsoleFunction('ChatbotAdmin/getChatbotAllList');
//   };

//   Grpc.remoteMethod(
//     'getChatbotAllList', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getChatbotInfo = function(Key, next) {
//     return callConsoleFunction('ChatbotAdmin/getChatbotInfo', {name: Key});
//   };

//   Grpc.remoteMethod(
//     'getChatbotInfo', {
//       accepts: [
//         {arg: 'Key', type: 'string', required: true},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.insertChatbotInfo = function(Chatbot, next) {
//     return callConsoleFunction('ChatbotAdmin/insertChatbotInfo', Chatbot);
//   };

//   Grpc.remoteMethod(
//     'insertChatbotInfo', {
//       accepts: [
//         {arg: 'Chatbot', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.updateChatbotInfo = function(Chatbot, next) {
//     return callConsoleFunction('ChatbotAdmin/updateChatbotInfo', Chatbot);
//   };

//   Grpc.remoteMethod(
//     'updateChatbotInfo', {
//       accepts: [
//         {arg: 'Chatbot', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.deleteChatbot = function(KeyList, next) {
//     return callConsoleFunction('ChatbotAdmin/deleteChatbot', KeyList);
//   };

//   Grpc.remoteMethod(
//     'deleteChatbot', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );


//   /*** Simple Classifier APIs ***/

//   Grpc.getSimpleClassifierAllList = function(next) {
//     return callConsoleFunction('SimpleClassifierAdmin/getSimpleClassifierAllList');
//   };

//   Grpc.remoteMethod(
//     'getSimpleClassifierAllList', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getSimpleClassifierInfo = function(Key, next) {
//     return callConsoleFunction('SimpleClassifierAdmin/getSimpleClassifierInfo', {name: Key});
//   };

//   Grpc.remoteMethod(
//     'getSimpleClassifierInfo', {
//       accepts: [
//         {arg: 'Key', type: 'string', required: true},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.insertSimpleClassifierInfo = function(SimpleClassifier, next) {
//     return callConsoleFunction('SimpleClassifierAdmin/insertSimpleClassifierInfo', SimpleClassifier);
//   };

//   Grpc.remoteMethod(
//     'insertSimpleClassifierInfo', {
//       accepts: [
//         {arg: 'SimpleClassifier', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.updateSimpleClassifierInfo = function(SimpleClassifier, next) {
//     return callConsoleFunction('SimpleClassifierAdmin/updateSimpleClassifierInfo', SimpleClassifier);
//   };

//   Grpc.remoteMethod(
//     'updateSimpleClassifierInfo', {
//       accepts: [
//         {arg: 'SimpleClassifier', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.deleteSimpleClassifier = function(KeyList, next) {
//     return callConsoleFunction('SimpleClassifierAdmin/deleteSimpleClassifier', KeyList);
//   };

//   Grpc.remoteMethod(
//     'deleteSimpleClassifier', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );


//   /*** Dialog Agent Instance APIs ***/

//   Grpc.getDialogAgentInstanceAllList = function(next) {
//     return callConsoleFunction('DialogAgentInstanceAdmin/getDialogAgentInstanceAllList');
//   };

//   Grpc.remoteMethod(
//     'getDialogAgentInstanceAllList', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getDialogAgentInstanceByChatbotName = function(Key, next) {
//     return callConsoleFunction('DialogAgentInstanceAdmin/getDialogAgentInstanceByChatbotName', {name: Key});
//   };

//   Grpc.remoteMethod(
//     'getDialogAgentInstanceByChatbotName', {
//       accepts: [
//         {arg: 'Key', type: 'string', required: true},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getDialogAgentInstanceInfo = function(Key, next) {
//     return callConsoleFunction('DialogAgentInstanceAdmin/getDialogAgentInstanceInfo', {name: Key});
//   };

//   Grpc.remoteMethod(
//     'getDialogAgentInstanceInfo', {
//       accepts: [
//         {arg: 'Key', type: 'string', required: true},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getDialogAgentInstanceResourceList = function(Key, next) {
//     return callConsoleFunction('DialogAgentInstanceAdmin/getDialogAgentInstanceResourceList', {name: Key});
//   };

//   Grpc.remoteMethod(
//     'getDialogAgentInstanceResourceList', {
//       accepts: [
//         {arg: 'Key', type: 'string', required: true},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.insertDialogAgentInstanceInfo = function(DialogAgentInstance, next) {
//     return callConsoleFunction('DialogAgentInstanceAdmin/insertDialogAgentInstanceInfo', DialogAgentInstance);
//   };

//   Grpc.remoteMethod(
//     'insertDialogAgentInstanceInfo', {
//       accepts: [
//         {arg: 'DialogAgentInstance', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.updateDialogAgentInstanceInfo = function(DialogAgentInstance, next) {
//     return callConsoleFunction('DialogAgentInstanceAdmin/updateDialogAgentInstanceInfo', DialogAgentInstance);
//   };

//   Grpc.remoteMethod(
//     'updateDialogAgentInstanceInfo', {
//       accepts: [
//         {arg: 'DialogAgentInstance', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.deleteDialogAgentInstance = function(KeyList, next) {
//     return callConsoleFunction('DialogAgentInstanceAdmin/deleteDialogAgentInstance', KeyList);
//   };

//   Grpc.remoteMethod(
//     'deleteDialogAgentInstance', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.stopDialogAgentInstance = function(KeyList, next) {
//     return callConsoleFunction('DialogAgentInstanceAdmin/stopDialogAgentInstance', KeyList);
//   };

//   Grpc.remoteMethod(
//     'stopDialogAgentInstance', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.startDialogAgentInstance = function(KeyList, next) {
//     return callConsoleFunction('DialogAgentInstanceAdmin/startDialogAgentInstance', KeyList);
//   };

//   Grpc.remoteMethod(
//     'startDialogAgentInstance', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.restartDialogAgentInstance = function(KeyList, next) {
//     return callConsoleFunction('DialogAgentInstanceAdmin/restartDialogAgentInstance', KeyList);
//   };

//   Grpc.remoteMethod(
//     'restartDialogAgentInstance', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );


//   /*** Intent Finder APIs ***/

//   Grpc.getIntentFinderAllList = function(next) {
//     return callConsoleFunction('IntentFinderAdmin/getIntentFinderAllList');
//   };

//   Grpc.remoteMethod(
//     'getIntentFinderAllList', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getIntentFinderInfo = function(Key, next) {
//     return callConsoleFunction('IntentFinderAdmin/getIntentFinderInfo', {name: Key});
//   };

//   Grpc.remoteMethod(
//     'getIntentFinderInfo', {
//       accepts: [
//         {arg: 'Key', type: 'string', required: true},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.insertIntentFinderInfo = function(IntentFinder, next) {
//     return callConsoleFunction('IntentFinderAdmin/insertIntentFinderInfo', IntentFinder);
//   };

//   Grpc.remoteMethod(
//     'insertIntentFinderInfo', {
//       accepts: [
//         {arg: 'IntentFinder', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.updateIntentFinderInfo = function(IntentFinder, next) {
//     return callConsoleFunction('IntentFinderAdmin/updateIntentFinderInfo', IntentFinder);
//   };

//   Grpc.remoteMethod(
//     'updateIntentFinderInfo', {
//       accepts: [
//         {arg: 'IntentFinder', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.deleteIntentFinder = function(KeyList, next) {
//     return callConsoleFunction('IntentFinderAdmin/deleteIntentFinder', KeyList);
//   };

//   Grpc.remoteMethod(
//     'deleteIntentFinder', {
//       accepts: [
//         {arg: 'KeyList', type: 'array', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );


//   /*** Authentication Policy APIs ***/

//   Grpc.getAuthticationPolicyAllList = function(next) {
//     return callConsoleFunction('AuthticationPolicyAdmin/getAuthticationPolicyAllList');
//   };

//   Grpc.remoteMethod(
//     'getAuthticationPolicyAllList', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );


//   /*** Supervisord APIs ***/
//   Grpc.getSupervisordServerGroupList = function(next) {
//     return gcd.callFunction('maum.Supervisord/getSupervisordServerGroupList');
//   };
//   Grpc.remoteMethod(
//     'getSupervisordServerGroupList', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.startSupervisordProcess = function(SupervisordProcessKey, next) {
//     return gcd.callFunction('maum.Supervisord/startSupervisordProcess', SupervisordProcessKey);
//   };
//   Grpc.remoteMethod(
//     'startSupervisordProcess', {
//       accepts: [
//         {arg: 'SupervisordProcessKey', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.stopSupervisordProcess = function(SupervisordProcessKey, next) {
//     return gcd.callFunction('maum.Supervisord/stopSupervisordProcess', SupervisordProcessKey);
//   };
//   Grpc.remoteMethod(
//     'stopSupervisordProcess', {
//       accepts: [
//         {arg: 'SupervisordProcessKey', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );


//   /*** Facade APIs ***/

//   Grpc.getFacadeChatbots = function(next) {
//     return gcd.callFunction('maum.m2u.facade.ChatbotFinder/getChatbots');
//   };

//   Grpc.remoteMethod(
//     'getFacadeChatbots', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.openDialogService = function(Caller, next) {
//     return gcd.callFunction('maum.m2u.facade.DialogService/open', Caller);
//   };
//   Grpc.remoteMethod(
//     'openDialogService', {
//       accepts: [
//         {arg: 'Caller', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.sendSimpleTextTalk = function(args, next) {
//     return gcd.callFunction('maum.m2u.facade.DialogService/simpleTextTalk', args.TextUtter, args.Metadata);
//   };
//   Grpc.remoteMethod(
//     'sendSimpleTextTalk', {
//       accepts: [
//         {arg: 'TextUtter', type: 'object', required: true, http: {source: 'body'}},
//         {arg: 'Metadata', type: 'object', required: false, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.sendTalkAnalyze = function(args, next) {
//     return gcd.callFunction('maum.m2u.facade.TalkService/analyze', args.TalkQuery, args.Metadata);
//   };
//   Grpc.remoteMethod(
//     'sendTalkAnalyze', {
//       accepts: [
//         {arg: 'TalkQuery', type: 'object', required: true, http: {source: 'body'}},
//         {arg: 'Metadata', type: 'object', required: false, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.closeDialogService = function(SessionKey, next) {
//     return gcd.callFunction('maum.m2u.facade.DialogService/close', SessionKey);
//   };
//   Grpc.remoteMethod(
//     'closeDialogService', {
//       accepts: [
//         {arg: 'SessionKey', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );


//   /*** TA Classifier APIs ***/

//   Grpc.findClassifierResolverModel = function(Model, next) {
//     return gcd.callFunction('maum.brain.cl.ClassifierResolver/find', Model);
//   };

//   Grpc.remoteMethod(
//     'findClassifierResolverModel', {
//       accepts: [
//         {arg: 'Model', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getClassifierResolverModels = function(next) {
//     return gcd.callFunction('maum.brain.cl.ClassifierResolver/getModels');
//   };

//   Grpc.remoteMethod(
//     'getClassifierResolverModels', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getClassifierResolverServers = function(next) {
//     return gcd.callFunction('maum.brain.cl.ClassifierResolver/getServers');
//   };

//   Grpc.remoteMethod(
//     'getClassifierResolverServers', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getClassifierResolverModelCategories = function(Model, next) {
//     return gcd.callFunction('maum.brain.cl.ClassifierResolver/getModelCategories', Model);
//   };

//   Grpc.remoteMethod(
//     'getClassifierResolverModelCategories', {
//       accepts: [
//         {arg: 'Model', type: 'object', required: true, http: {source: 'body'}},
//       ],
//       returns: {root: true},
//       isStatic: true
//     }
//   );

//   Grpc.getSTTModels = function(next) {
//     return gcd.callFunction('maum.brain.stt.SttModelResolver/getModels');
//   };


//   /*** STT Model APIs ***/

//   Grpc.remoteMethod(
//     'getSTTModels', {
//       returns: {root: true},
//       isStatic: true
//     }
//   );
// }
