'use strict';
const app = require('../server');
const path = require('path');
const moment = require('moment');

module.exports = function (srch_etlLogs) {
    const loggerName = path.basename(__filename, path.extname(__filename));

    srch_etlLogs.getList = function (skip, limit, order, text, start_date, end_date, next) {
        const connector = srch_etlLogs.app.datasources.queryjet.connector;
        let sql = '';

        if (text && text != undefined && text != "") {

            sql =
                `SELECT 
                R_OBJECT_ID,
                ERROR_MESSAGE,
                ERROR_TIME,
                SERVICE_NAME,
                INDEX_TYPE
            FROM
                    SEARCH_ERROR_LOG
            WHERE
                ERROR_TIME >= '${moment(start_date).utc().format('YYYY-MM-DD')}' AND ERROR_TIME < '${moment(end_date).utc().format('YYYY-MM-DD')}' AND
                ERROR_MESSAGE LIKE '%${text}%'
            ORDER BY ${order}
            LIMIT ${limit} 
            OFFSET ${skip}`;

        } else {

            sql =
                `SELECT 
                R_OBJECT_ID,
                ERROR_MESSAGE,
                ERROR_TIME,
                SERVICE_NAME,
                INDEX_TYPE
            FROM
                    SEARCH_ERROR_LOG
            ORDER BY ${order}
            LIMIT ${limit} 
            OFFSET ${skip}`;

        }
        connector.query(sql, null, (err, resultObjects) => {
            if (err) next(err);

            // console.log(resultObjects);
            app.devlog(loggerName, resultObjects);
            next(null, resultObjects);
        });
    };
    srch_etlLogs.remoteMethod(
        'getList', {
            isStatic: true,
            http: {path: '/getList', verb: 'get'},
            accepts: [
                {arg: 'skip', type: 'number', required: true},
                {arg: 'limit', type: 'number', required: true},
                {arg: 'order', type: 'string', required: true},
                {arg: 'text', type: 'string'},
                {arg: 'start_date', type: 'date', required: false},
                {arg: 'end_date', type: 'date', required: false}

            ],
            returns: {root: true},
        }
    );


    srch_etlLogs.remoteMethod(
        'countList', {
            isStatic: true,
            http: {path: '/countList', verb: 'get'},
            accepts: [
                {arg: 'text', type: 'string'},
                {arg: 'start_date', type: 'date', required: false},
                {arg: 'end_date', type: 'date', required: false},
            ],
            returns: {root: true},
        }
    );


    srch_etlLogs.countList = function (text, start_date, end_date, next) {
        app.devlog(loggerName, 'start_date :', start_date, `${moment(start_date).format('YYYY-MM-DD')}`);
        app.devlog(loggerName, 'end_date :', end_date, `${moment(end_date).format('YYYY-MM-DD')}`);

        const connector = srch_etlLogs.app.datasources.queryjet.connector;
        let sql = '';
        if (text && text != undefined && text != "") {
            sql =
                `SELECT count(*) as cnt
              FROM
                      SEARCH_ERROR_LOG
              WHERE
                  ERROR_TIME >= '${moment(start_date).utc().format('YYYY-MM-DD')}' AND ERROR_TIME < '${moment(end_date).utc().format('YYYY-MM-DD')}' 
              AND
                  ERROR_MESSAGE like '%${text}%'`


        } else {
            sql =
                `SELECT count(*) as cnt
                FROM
                    SEARCH_ERROR_LOG
                WHERE
                    ERROR_TIME >= '${moment(start_date).utc().format('YYYY-MM-DD')}' AND ERROR_TIME < '${moment(end_date).utc().format('YYYY-MM-DD')}' 
                        `
        }
        connector.query(sql, null, (err, resultObjects) => {
            if (err) next(err);
            next(null, resultObjects[0]);
        });
    };
};

