'use strict';
const app = require('../server');
const path = require('path');

module.exports = function(cm_cd_detail) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  // delete By Ids
  cm_cd_detail.deleteByIds = function(ids = [] /*, next*/) {
    return Promise.all(
        ids.map(id => cm_cd_detail.destroyById(id).catch(() => ({count: 0})))
      )
      .then(result => {
        let deletedIds = ids.filter((id, index) => result[index].count !== 0);
        return deletedIds;
      });
  };

  cm_cd_detail.remoteMethod(
    'deleteByIds', {
      accepts: [
        {arg: 'ids', type: ['string'], required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );
};
