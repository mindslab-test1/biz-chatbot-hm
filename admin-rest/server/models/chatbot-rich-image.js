'use strict';
const app = require('../server');
const path = require('path');

module.exports = function(ChatbotRichImage) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  ChatbotRichImage.remoteMethod('destroyAll', {
    isStatic: true,
    description: 'Delete all matching records',
    accessType: 'WRITE',
    accepts: {arg: 'where', type: 'object', description: 'filter.where object'},
    http: {verb: 'del', path: '/'}
  });
};
