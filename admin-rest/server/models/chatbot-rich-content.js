'use strict';
const app = require('../server');
const models = app.models;
const path = require('path');

module.exports = function(Chatbotrichcontent) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  Chatbotrichcontent.afterRemote('create', function (ctx, params, next) {
    app.devlog(loggerName, 'Chatbotrichcontent create after \nparams ====\n', params,'\n\nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    const id = params.rc_id;

    app.devlog(loggerName, 'Chatbotrichcontent create', id);
    // models.chatbot_config.find({where:{bot_id: params.bot_id}});
    const data = Object.assign({
      bot_id: params.bot_id,
      conf_id: params.conf_id,
      created_dtm: new Date(),
      creator_id: params.creator_id,
      updated_dtm: params.updated_dtm,
      updator_id: params.updator_id
    }, getRemoteInformation(ctx));
    let _data;
    switch(params.option){
      case 'greeting':
        _data = Object.assign(data,{
          greeting_msg_btn_id: id
        });
        break;
      case 'unknown':
        _data = Object.assign(data,{
          unknown_msg_btn_id: id
        });
        break;
      case 'notice1':
        _data = Object.assign(data,{
          notice1_img_id: id
        });
        break;
      case 'notice2':
        _data = Object.assign(data,{
          notice2_img_id: id
        });
        break;
      case 'notice3':
        _data = Object.assign(data,{
          notice3_img_id: id
        });
        break;
      case 'notice4':
        _data = Object.assign(data,{
          notice4_img_id: id
        });
        break;
      default:
        console.log('invalid btn_option');
        break;
    }
    models.chatbot_config.upsertWithWhere({conf_id:params.conf_id, bot_id:params.bot_id},
      _data);
    next();
  });

  Chatbotrichcontent.afterRemoteError('create', function (ctx, next) {
    app.devlog(loggerName, 'Chatbotrichcontent create err after \nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    next();
  });


  function getRemoteInformation(ctx) {
    app.devlog(loggerName, 'getRemoteInformation');
    return {
      remote: ctx.req.connection.remoteAddress || ctx.req.headers['x-forwarded-for'],
      agent: ctx.req.headers['user-agent']
    };
  }
};
