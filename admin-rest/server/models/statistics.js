'use strict';

const app = require('../server');
const path = require('path');

module.exports = function (Statistics) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  let connector, type;

  Statistics.assignConnector = function (Args, next) {
    return new Promise((resolve, reject) => {
      let targetKey = Object.keys(app.datasources)
        .find(key => app.datasources[key].settings.name === Statistics.config.targetDataSource);

      if (targetKey) {
        connector = app.datasources[targetKey].connector;
        type = connector.name;
      }
      resolve(type);
      resolve(Statistics);

      if (typeof type === 'string') {
        resolve(type);
      } else {
        reject(new Error('Connector is not Ready!'));
      }
    });
  };

  Statistics.remoteMethod(
    'assignConnector', {
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeQuery = function (Args, next) {
    return new Promise((resolve, reject) => {
      var source = app.datasources['mindslab'];
      connector = source.connector;
      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }
      let query = Args.Query;
      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }
      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeQuery', {
      accepts: [
        {arg: 'Query', type: 'object', required: true, http: {source: 'body'}}
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeEngineQuery = function (Args, next) {
    return new Promise((resolve, reject) => {

      var source = app.datasources['mindslab'];
      var database = source.settings.database;
      let query = '';
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      /*
      select
      day,
      coalesce(sum(case when keyword = "여권" then count end), 0) as '여권',
      coalesce(sum(case when keyword = "축제" then count end), 0) as '축제',
      coalesce(sum(case when keyword = "업무" then count end), 0) as '업무'
      from Statistics stat
      where engine = 'sds' and date(day) between '2017-01-01 00:00:00' and '2017-02-01 23:59:00'
      group by day
      union
      select
      null,
      coalesce(sum(case when keyword = "여권" then count end), 0) as '여권',
      coalesce(sum(case when keyword = "축제" then count end), 0) as '축제',
      coalesce(sum(case when keyword = "업무" then count end), 0) as '업무'
      from Statistics stat
      where engine = 'sds' and date(day) between '2017-01-01 00:00:00' and '2017-02-01 23:59:00';*/

      // Engine === all 인 경우 모든 엔진에 대한 category 값을 검색합니다.
      if (Args.queryParams.engine.toLowerCase() === 'all') {
        query = `select date(day) as 'day',`;
        Args.queryParams.categories.forEach(i =>
          query += `coalesce(sum(case when keyword = '${i.keyword}' then count end), 0)
            as '${i.displayName}',`);
        query = query.substring(0, query.length - 1);

        query += `from ${database}.Statistics stat
        where date(day) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        group by date(day)`;

        query += ' union select null,';
        Args.queryParams.categories.forEach(i =>
          query += `coalesce(sum(case when keyword = '${i.keyword}' then count end), 0)
            as '${i.displayName}',`);
        query = query.substring(0, query.length - 1);
        query += `from ${database}.Statistics stat
        where date(day) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}';`;
      } else {
        // Engine 별 category 값을 검색합니다.
        query = `select date(day) as 'day',`;
        Args.queryParams.categories.forEach(i =>
          query += `coalesce(sum(case when keyword = '${i.keyword}' then count end), 0)
            as '${i.displayName}',`);
        query = query.substring(0, query.length - 1);

        query += `from ${database}.Statistics stat
        where engine = '${Args.queryParams.engine}' and
        date(day) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        group by date(day)`;

        query += ' union select null,';
        Args.queryParams.categories.forEach(i =>
          query += `coalesce(sum(case when keyword = '${i.keyword}' then count end), 0)
            as '${i.displayName}',`);
        query = query.substring(0, query.length - 1);
        query += `from ${database}.Statistics stat
        where engine = '${Args.queryParams.engine}' and
        date(day) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}';`;
      }


      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeEngineQuery', {
      accepts: [
        {arg: 'queryParams', type: 'object', required: true, http: {source: 'body'}}
      ],
      returns: {root: true},
      isStatic: true
    }
  );


  Statistics.executeResponseQuery = function (Args, next) {
    return new Promise((resolve, reject) => {

      var source = app.datasources['mindslab'];
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }
      /*
        select day, sum(f) as fail, sum(s) as success from (
          select
            DATE_FORMAT(endAt, '%Y%m%d') as day,
            instr(outMeta, '"out.da.engine.name":"None"')>0 as f,
            instr(outMeta, '"out.da.engine.name":"None"')=0 as s
          from admin_console.Talk
          where date(endAt) between '2017-07-01' and '2017-12-01'
        ) A
        group by day;
      */

      let query = `select day, sum(f) as fail, sum(s) as success
        from (
          select
            DATE_FORMAT(endAt, '%Y-%m-%d') as day,
            instr(outMeta, '"out.da.engine.name":"None"')>0 as f,
            instr(outMeta, '"out.da.engine.name":"None"')=0 as s
          from ${database}.Talk
          where date(endAt) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        ) sub group by day
        union
        select null, sum(f) as fail, sum(s) as success
        from (
          select
            DATE_FORMAT(endAt, '%Y-%m-%d') as day,
            instr(outMeta, '"out.da.engine.name":"None"')>0 as f,
            instr(outMeta, '"out.da.engine.name":"None"')=0 as s
          from ${database}.Talk
          where date(endAt) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        ) bbb;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeResponseQuery', {
      accepts: [
        {arg: 'queryParams', type: 'object', required: true, http: {source: 'body'}}
      ],
      returns: {root: true},
      isStatic: true
    }
  );


  Statistics.executeResponseDownloadQuery = function (Args, next) {
    return new Promise((resolve, reject) => {

      var source = app.datasources['mindslab'];
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = `select
          DATE_FORMAT(endAt, '%Y-%m-%d %H:%i:%s') as 'day',
          sid as 'sessionID',
          seq as 'talkID',
          inText,
          outText,
          outMeta as 'metaInfo',
          case when instr(outMeta, '"out.da.engine.name":"None"')=0 then 'success'
            else 'fail' end as 'result',
          feedback as 'satisfaction',
          accuracy
        from ${database}.Talk
        where date(endAt) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        order by endAt, sessionID, talkID;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeResponseDownloadQuery', {
      accepts: [
        {arg: 'queryParams', type: 'object', required: true, http: {source: 'body'}}
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeSatisfactionQuery = function (Args, next) {
    return new Promise((resolve, reject) => {

      var source = app.datasources['mindslab'];
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = `select
          DATE_FORMAT(endAt, '%Y-%m-%d %H:%i:%s') as 'day',
          sid as 'sessionID',
          seq as 'talkID',
          inText,
          outText,
          outMeta as 'metaInfo',
          case when instr(outMeta, '"out.da.engine.name":"None"')=0 then 'success'
            else 'fail' end as 'result',
          feedback as 'satisfaction',
          accuracy
        from ${database}.Talk
        where date(endAt) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
        order by endAt, sessionID, talkID;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeSatisfactionQuery', {
      accepts: [
        {arg: 'queryParams', type: 'object', required: true, http: {source: 'body'}}
      ],
      returns: {root: true},
      isStatic: true
    }
  );
   Statistics.executeSatisfactionLineQuery = function (Args, next) {
    return new Promise((resolve, reject) => {

      var source = app.datasources['mindslab'];
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = `select 
        DATE_FORMAT(endAt, '%Y-%m-%d') as 'day',
        round(avg(feedback), 2) as 'satisfaction'
      from ${database}.Talk 
      where date(endAt) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
      group by day;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeSatisfactionLineQuery', {
      accepts: [
        {arg: 'queryParams', type: 'object', required: true, http: {source: 'body'}}
      ],
      returns: {root: true},
      isStatic: true
    }
  );


  Statistics.updateAccuracy = function (Args, next) {
    return new Promise((resolve, reject) => {

      var source = app.datasources['mindslab'];
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let whenQuery = '';
      let sidQuery = '';
      let seqQuery = '';

      for (let i = 0; i < Args.queryParams.length; i++) {
        whenQuery +=
          `when sid = ${Args.queryParams[i].sessionId} and seq = ${Args.queryParams[i].talkId}
            then ${Args.queryParams[i].accuracy} `;
        sidQuery += `${Args.queryParams[i].sessionId},`;
        seqQuery += `${Args.queryParams[i].talkId},`;
      }

      let commaSid = sidQuery.substr(0, sidQuery.length - 1);
      let commaSeq = seqQuery.substr(0, seqQuery.length - 1);

      let query = `update ${database}.Talk
       set accuracy = case ${whenQuery} else accuracy end
       where sid in (${commaSid}) and seq in (${commaSeq});`;

      // console.log('== Accuracy Update Query');
      // console.log(query);

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'updateAccuracy', {
      accepts: [
        {arg: 'queryParams', type: 'object', required: true, http: {source: 'body'}}
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeAccuracyLineQuery = function (Args, next) {
    return new Promise((resolve, reject) => {

      var source = app.datasources['mindslab'];
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = `select 
        DATE_FORMAT(endAt, '%Y-%m-%d') as 'day',
        round(avg(accuracy) , 2) as 'accuracy'
      from ${database}.Talk 
      where date(endAt) between '${Args.queryParams.startAt}' and '${Args.queryParams.endAt}'
      group by day;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeAccuracyLineQuery', {
      accepts: [
        {arg: 'queryParams', type: 'object', required: true, http: {source: 'body'}}
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeDailySessionQuery = function (next) {
    return new Promise((resolve, reject) => {

      var source = app.datasources['mindslab'];
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = `SELECT 
          DATE_FORMAT(startAt,'%Y-%m-%d') as date 
          , count(id) as totCnt, sec_to_time((sum(TIMEDIFF(endAt, startAt)))) as totTime
          , sum(talkCount) as totCall 
        FROM admin_console.Session
        group by date 
        order by date 
        desc limit 10;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeDailySessionQuery', {
      description: 'get daliy session info from Session Table',
      returns: {
        arg: 'data', type: 'object', root: true
      },
      http: {
        path: '/executeDailySessionQuery', verb: 'get'
      },
      isStatic: true
    });

  Statistics.executeDailySessionDownloadQuery = function (Args, next) {
    return new Promise((resolve, reject) => {

      var source = app.datasources['mindslab'];
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      let query = `select 
          date_format(startAt,'%Y-%m-%d') as date 
          , count(id) as '토탈 세션(수)'
          , sec_to_time((sum(timediff(endAt, startAt)))) as '총 접속시간'
          , sum(talkCount) as '총 호출(수)'
        from admin_console.Session
        where 
          date_format(startAt,'%Y-%m-%d') >= date_format(date_add('${Args.queryParams.startAt}',interval +1 day),'%Y-%m-%d') 
          and date_format(startAt,'%Y-%m-%d') <= date_format(date_add('${Args.queryParams.endAt}',interval +1 day),'%Y-%m-%d')
        group by date 
        order by date desc;`;

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeDailySessionDownloadQuery', {
      accepts: [
        {arg: 'queryParams', type: 'object', required: true, http: {source: 'body'}}
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  Statistics.executeTimeSessionQuery = function (next) {
    return new Promise((resolve, reject) => {

      var source = app.datasources['mindslab'];
      var database = source.settings.database;
      connector = source.connector;

      if (connector === undefined) {
        reject(new Error('Connector is not Ready!'));
        return;
      }

      var timeAry = new Array(120,110,100,90,80,70,60,50,40,30);

      let query = '';
      for (let index = 0; index < timeAry.length; index++) {
        query += `select
	        date_format(date_add(now(),interval -'${timeAry[index]}' minute),'%H:%i') as time
          , sum(
		      case
			    when
            endAt >= date_format(date_add(now(), interval -'${timeAry[index]+10}' minute), '%Y-%m-%d %H:%i')
            and startAt <= date_format(date_add(now(), interval -'${timeAry[index]}' minute), '%Y-%m-%d %H:%i')
          then 1
          else 0
		      end) as sessionCnt
        from admin_console.Session`
        if(index != (timeAry.length-1)){
          query += ` union `
        }
      }

      if (typeof query !== 'string') {
        reject(new Error('Query is not string!'));
        return;
      }
      if (query.length < 1) {
        reject(new Error('Query is Empty!'));
        return;
      }

      connector.execute(query, (err, resultSet) => {
        if (err) {
          reject(err);
        } else {
          resolve(resultSet);
        }
      });
    });
  };

  Statistics.remoteMethod(
    'executeTimeSessionQuery', {
      description: 'get Time session info from Session Table',
      returns: {
        arg: 'data', type: 'object', root: true
      },
      http: {
        path: '/executeTimeSessionQuery', verb: 'get'
      },
      isStatic: true
    });
};
