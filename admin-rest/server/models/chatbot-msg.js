'use strict';
const app = require('../server');
const path = require('path');

module.exports = function(Chatbotmsg) {
    const loggerName = path.basename(__filename, path.extname(__filename));

    Chatbotmsg.deleteByIds = function(id, bot_id , next ) {

         Chatbotmsg.destroyById(id, (error) =>{
           console.log(error);

           if(error == null){
                next(null, {count :1});
           }else{
               next(null, {count :0});
           }
         });
     };

     Chatbotmsg.remoteMethod(
        'deleteByIds', {
          http: {path: '/:id', verb: 'post'},
          accepts: [
            {arg: 'id', type: 'number', required: true},
            {arg: 'bot_id', type: 'number', required: false},
          ],
          returns: {root: true},
          isStatic: true
        }
      );

};
