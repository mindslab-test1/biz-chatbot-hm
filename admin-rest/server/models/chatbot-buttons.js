'use strict';
const app = require('../server');
const path = require('path');

module.exports = function(Chatbotbuttons) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  Chatbotbuttons.remoteMethod('destroyAll', {
    isStatic: true,
    description: 'Delete all matching records',
    accessType: 'WRITE',
    accepts: {arg: 'where', type: 'object', description: 'filter.where object'},
    http: {verb: 'del', path: '/'}
  });
};
