'use strict';
const app = require('../server');
const path = require('path');

module.exports = function(cm_workspace_team) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  // delete By Ids
  cm_workspace_team.deleteByIds = function(ids = []/* , next */) {
    return Promise.all(
        ids.map(id => cm_workspace_team.destroyById(id).catch(() => ({count: 0})))
      )
      .then(result => {
        let deletedIds = ids.filter((id, index) => result[index].count !== 0);
        return deletedIds;
      });
  };
  cm_workspace_team.afterRemoteError('destroyById', function (ctx, next) {
    app.devlog(loggerName, 'cm_workspace_team destroyById err after \nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    next();
  });
  cm_workspace_team.remoteMethod('destroyAll', {
    isStatic: true,
    description: 'Delete all matching records',
    accessType: 'WRITE',
    accepts: {arg: 'where', type: 'object', description: 'filter.where object'},
    http: {verb: 'del', path: '/'}
  });

    cm_workspace_team.getRelationData = function(workspace_id, team_id, bot_id, conf_id, next) {
      // app.devlog('workspace_id :', workspace_id);
      // app.devlog('team_id :', team_id);
      // app.devlog('bot_id :', bot_id);
      // app.devlog('conf_id :', conf_id);

      let sql =
          `SELECT
                WT.workspace_id
              , WT.team_id
              , CI.bot_id
              , CC.conf_id
          FROM cm_workspace_team_tb AS WT
          LEFT JOIN chatbot_info_tb AS CI ON CI.team_id = WT.team_id
          LEFT JOIN chatbot_config_tb AS CC ON CC.bot_id = CI.bot_id
          WHERE 1=1`;

      let whereSql = '';
      if (workspace_id && workspace_id != null && workspace_id !== undefined && workspace_id !== 0) {
        whereSql += ` AND WT.workspace_id = ${workspace_id}`;
      }
      if (team_id && team_id != null && team_id !== undefined && team_id !== 0) {
          whereSql += ` AND WT.team_id = ${team_id}`;
      }
      if (bot_id && bot_id != null && bot_id !== undefined && bot_id !== 0) {
          whereSql += ` AND CI.bot_id = ${bot_id}`;
      }
      if (conf_id && conf_id != null && conf_id !== undefined && conf_id !== 0) {
          whereSql += ` AND CC.conf_id = ${conf_id}`;
      }
      sql += whereSql + ` GROUP BY WT.team_id`;

      // app.devlog(loggerName, sql);

      const connector = cm_workspace_team.app.datasources.mindslab.connector;

      connector.query(sql, null, (err, result) => {
          if (err) {
              next(err);
          } else {
              let arrayData = [];

              if(result && result.length > 0) {
                for (let i = 0; i < result.length; i++) {
                  // console.log('result =', result[i]);
                  const temp = {
                      workspace_id: Number(result[i].workspace_id)
                      , team_id: Number(result[i].team_id)
                      , bot_id: Number(result[i].bot_id)
                      , conf_id: Number(result[i].conf_id)
                  };
                  arrayData.push(temp);
                }
              }
              next(null, arrayData);
          }
      });
    };
};
