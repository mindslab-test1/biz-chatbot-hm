'use strict';

const app = require('../server');
const moment = require('moment');
const models = app.models;
const NodeCache = require('node-cache');
const configCache = require('../config').NodeCache;
const path = require('path');

module.exports = function(front_user_comment) {
    const loggerName = path.basename(__filename, path.extname(__filename));
    var config = configCache;
    if (configCache) {

    } else {
        config = {stdTTL: 60, checkperiod: 0};
    }
    const sCache = new NodeCache(config);

    sCache.on('set', function (key, value) {
        // console.log('========================================================================================');
        // console.log('set', sCache.getStats());
        // console.log('========================================================================================');
        app.devlog(loggerName, '========================================================================================');
        app.devlog(loggerName, 'set', sCache.getStats());
        app.devlog(loggerName, '========================================================================================');
    });

    sCache.on('expired', function (key, value) {
        // console.log('========================================================================================');
        // console.log('expired', sCache.getStats());
        // console.log('========================================================================================');
        app.devlog(loggerName, '========================================================================================');
        app.devlog(loggerName, 'expired', sCache.getStats());
        app.devlog(loggerName, '========================================================================================');
    });

    front_user_comment.remoteMethod(
        'getCommentCount', {
            http: { path: '/getCommentCount',  verb: 'get'  },
            accepts: [
                {  arg: 'start_date',  type: 'date',  required: false  },
                {  arg: 'end_date', type: 'date',   required: false  },
                {  arg: 'searchOptions', type: 'string',  required: false  },
                {  arg: 'workspace_id', type: 'string',  required: false  },
            ],
            returns: {
                root: true
            },
        }
    );

    front_user_comment.getCommentCount = function (start_date, end_date, searchOptions, workspace_id, next) {
        app.devlog(loggerName, "------------------------------------------------------------------------------------------");
        app.devlog(loggerName, "searchOptions", searchOptions);


        const {
            botName,
            userName,
            teamName,
            title,
            content,
            chType
        } = JSON.parse(searchOptions);


        app.devlog(loggerName, "------------------------------------------------------------------------------------------");

        const connector = front_user_comment.app.datasources.mindslab.connector;

        var queryString =
            `   select count(comment_id) count
                from (
                         select f.comment_id,
                                (select cb.bot_name from chatbot_info_tb cb where cb.bot_id = f.bot_id)  as botName,
                                 m.USER_NM as userName,
                                 m.DEPT_NM as teamName,
                                f.channel as chType,
                                f.title,
                                f.content,
                                f.created_dtm as createTime
                         from front_user_comment_tb f
                              inner join mobis_user_master_tb m on f.user_id = m.user_id
                         where f.created_dtm >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                              and f.created_dtm < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                     )cm
                where 1 =1     `;


        if (botName && botName.length != 0) {
            queryString += `  AND  cm.botName like  '%${botName}%'`;
        }

        if(userName && userName.length != 0) {
            queryString += `  AND  cm.userName like '%${userName}%'`;
        }

        if(teamName && teamName.length != 0) {
            queryString += `  AND  cm.teamName ike '%${teamName}%'`;
        }

        if(title && title.length != 0) {
            queryString += `  AND  cm.title like '%${title}%'`;
        }

        if (content && content.length != 0) {
            queryString += `  AND  cm.content like '%${content}%'`;
        }

        if (chType && chType.length != 0) {
            queryString += `  AND  cm.chType = '${chType}'`;
        }



        app.devlog(loggerName, "------------------------------------------------------------------------------------------");
        app.devlog(loggerName, queryString);
        app.devlog(loggerName, "------------------------------------------------------------------------------------------");


        connector.query(queryString, null, (err, resultObjects) => {
            if (err) next(err);
            // console.log(resultObjects[0]);
            app.devlog(loggerName, resultObjects[0]);
            next(null, resultObjects[0]);
        });
    };



    front_user_comment.remoteMethod(
        'getCommentList', {
            http: { path: '/getCommentList',  verb: 'get'  },
            accepts: [
                { arg: 'pg', type: 'number', required: true, default: 1 },
                { arg: 'pg_size', type: 'number', required: true,default: 10},
                { arg: 'start_date',  type: 'date',  required: false  },
                { arg: 'end_date', type: 'date',   required: false  },
                { arg: 'searchOptions', type: 'string',  required: false  },
                { arg: 'workspace_id', type: 'string',  required: false  },
            ],
            returns: {
                root: true
            },
        }
    );

    front_user_comment.getCommentList = function (pg, pg_size, start_date, end_date, searchOptions, workspace_id, next) {
        app.devlog(loggerName, "------------------------------------------------------------------------------------------");
        app.devlog(loggerName, "searchOptions", searchOptions);


        const {
            botName,
            userName,
            teamName,
            title,
            content,
            chType
        } = JSON.parse(searchOptions);

        const startIdx = (pg_size * (pg - 1));

        app.devlog(loggerName, "------------------------------------------------------------------------------------------");

        const connector = front_user_comment.app.datasources.mindslab.connector;

        var queryString =
            `   select comment_id,
                        botName,
                        userName,
                        teamName,
                        title,
                        content,
                        chType,
                        created_dtm as  createTime
                from (
                         select f.comment_id,
                                (select cb.bot_name from chatbot_info_tb cb where cb.bot_id = f.bot_id)  as botName,
                                 m.USER_NM as userName,
                                 m.DEPT_NM as teamName,
                                f.channel as chType,
                                f.title,
                                f.content,  
                                f.created_dtm 
                         from front_user_comment_tb f
                              inner join mobis_user_master_tb m on f.user_id = m.user_id
                         where f.created_dtm >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                           and f.created_dtm < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                     )cm
                 where 1 =1 `;


        if (botName && botName.length != 0) {
            queryString += `  AND  cm.botName like '%${botName}%'`;
        }

        if(userName && userName.length != 0) {
            queryString += `  AND  cm.userName ike '%${userName}%'`;
        }

        if(teamName && teamName.length != 0) {
            queryString += `  AND  cm.teamName ike '%${teamName}%'`;
        }

        if(title && title.length != 0) {
            queryString += `  AND  cm.title like '%${title}%'`;
        }

        if (content && content.length != 0) {
            queryString += `  AND  cm.content like '%${content}%'`;
        }

        if (chType && chType.length != 0) {
            queryString += `  AND  cm.chType = '${chType}'`;
        }

        queryString += ` ORDER BY  created_dtm desc, botName asc, userName asc
                        LIMIT ${startIdx}, ${pg_size}`;


        app.devlog(loggerName, "------------------------------------------------------------------------------------------");
        app.devlog(loggerName, queryString);
        app.devlog(loggerName, "------------------------------------------------------------------------------------------");


        connector.query(queryString, null, (err, resultObjects) => {
            if (err) next(err);
            // console.log(resultObjects[0]);
            app.devlog(loggerName, resultObjects);
            next(null, resultObjects);
        });
    };

}
