'use strict';
const app = require('../server');
const path = require('path');

module.exports = function (srch_related) {
    const loggerName = path.basename(__filename, path.extname(__filename));

    srch_related.getList = function ( skip, limit, order, text, next) {
        const connector = srch_related.app.datasources.mindslab.connector;
        let sql = '';

        if (text && text != undefined && text != "") {

            sql =
                `SELECT 
                    ID,
                    KEYWORD
            FROM
                    srch_related_stopword_tb
            WHERE
                KEYWORD like '%${text}%'
            ORDER BY ${order}
            LIMIT ${limit} 
            OFFSET ${skip}`;

        } else {

            sql =
                `SELECT 
                    ID,
                    KEYWORD
            FROM
                    srch_related_stopword_tb
            ORDER BY ${order}
            LIMIT ${limit} 
            OFFSET ${skip}`

        }
        connector.query(sql, null, (err, resultObjects) => {
            if (err) next(err);

            // console.log(resultObjects);
            app.devlog(loggerName, resultObjects);
            next(null, resultObjects);
        });
    };

    srch_related.remoteMethod(
        'getList', {
            isStatic: true,
             http: {path: '/getList', verb: 'get'},
            accepts: [
                { arg: 'skip', type: 'number', required: true },
                { arg: 'limit', type: 'number', required: true },
                { arg: 'order', type: 'string', required: true },
                { arg: 'text', type: 'string' },
            ],
            returns: { root: true },
        }
    );


    srch_related.remoteMethod(
        'countList', {
            isStatic: true,
            http: {path: '/countList', verb: 'get'},
            accepts: [
                { arg: 'text', type: 'string' }
            ],
            returns: { root: true },
        }
    );



    srch_related.countList = function ( text, next) {
        const connector = srch_related.app.datasources.mindslab.connector;
        let sql = '';
        if (text && text != undefined && text != "") {
            sql =
                `SELECT count(*) as cnt
              FROM
                      srch_related_stopword_tb
              WHERE
                  KEYWORD like '%${text}%'`


        } else {
            sql =
                `SELECT count(*) as cnt
                FROM
                        srch_related_stopword_tb`
        }
        connector.query(sql, null, (err, resultObjects) => {

            if (err) {
                next(err);
            }
            next(null, resultObjects);
        });
    };

    // delete By Ids
    srch_related.deleteByIds = function(ids = [] , next) {
        return Promise.all(
            ids.map(id => srch_related.destroyById(id).catch(() => ({count: 0})))
        )
            .then(result => {
                let deletedIds = ids.filter((id, index) => result[index].count !== 0);
                return deletedIds;
            });
    };

    srch_related.remoteMethod(
        'deleteByIds', {
            accepts: [
                {arg: 'ids', type: ['string'], required: true},
            ],
            returns: {root: true},
            isStatic: true
        }
    );


};
