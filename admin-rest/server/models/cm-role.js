'use strict';
const models = require('../server').models;
const app = require('../server').app;
const path = require('path');

module.exports = function (cm_role) {
  const loggerName = path.basename(__filename, path.extname(__filename));
  const ROLE = {
      // ADMIN: 'ADMIN',
      // CS_ADMIN: 'CS_ADMIN',
      // CS_AGENT: 'CS_AGENT',
      // OPERATE_ADMIN: 'OPERATE_ADMIN',
      // OPERATE_TEMP: 'OPERATE_TEMP',
      // SERVICE_ADMIN: 'SERVICE_ADMIN',
      // SERVICE_TRAINER: 'SERVICE_TRAINER'
      ADMIN: 'ADMIN',
      CS_AGENT: 'CS_AGENT',
      OPERATOR: 'OPERATOR',
      SERVICE_ADMIN: 'SERVICE_ADMIN'
  };


  cm_role.isAdmin = function(role_id, user_id, next) {
    return new Promise((resolve, reject) => {
      if((role_id != ROLE.ADMIN
        && role_id != ROLE.SERVICE_ADMIN)) {
        resolve(false);
        return;
      }

      const where = {
        user_id: user_id
      };
      models.cm_user.find({where}, (err, user) => {
        if (err) {
          reject(err);
          return;
        }
        if (user.length != 1) {
          resolve(false);
          return;
        }

        const res = user[0].role_id == role_id;
        resolve(res);
      });
    })
    .then((res) => {
      return res;
    })
    .catch(err => {
      if (next) next(err);
      throw err;
    });
  };

  cm_role.remoteMethod(
    'isAdmin', {
      description: '해당 role_id 의 Admin 여부 확인.',
      accepts: [
        {arg: 'role_id', type: 'string', required: true},
        {arg: 'user_id', type: 'string', required: true}
      ],
      returns: {root: true},
      isStatic: true,
      http: {verb: 'get'}
    }
  );


  cm_role.isSupervisor = function(role_id/* , user_id */, next) {
    return new Promise((resolve, reject) => {
      if(role_id != ROLE.ADMIN) {
        resolve(false);
        return;
      }

      // const where = {
      //   user_id: user_id
      // };
      // models.cm_user.find({where}, (err, user) => {
      //   if (err) {
      //     reject(err);
      //     return;
      //   }
      //   if (user.length != 1) {
      //     resolve(false);
      //     return;
      //   }

      //   const res = user[0].role_id == role_id;
      //   resolve(res);
      // });
      resolve(true);
    })
    // and count and return
    .then((res) => {
      return res;
    })
    .catch(err => {
      if (next) next(err); // eslint-disable-line
      throw err;
    });
  };

  cm_role.remoteMethod(
    'isSupervisor', {
      description: '해당 role_id 의 supervisor 여부 확인.',
      accepts: [
        {arg: 'role_id', type: 'string', required: true}
        // ,{arg: 'user_id', type: 'string', required: true}
      ],
      returns: {root: true},
      isStatic: true,
      http: {verb: 'get'}
    }
  );

  cm_role.isServiceAdmin = function(role_id/* , user_id */, next) {
    return new Promise((resolve, reject) => {
      if(role_id != ROLE.SERVICE_ADMIN) {
        resolve(false);
        return;
      }

      // const where = {
      //   user_id: user_id
      // };
      // models.cm_user.find({where}, (err, user) => {
      //   if (err) {
      //     reject(err);
      //     return;
      //   }
      //   if (user.length != 1) {
      //     resolve(false);
      //     return;
      //   }

      //   const res = user[0].role_id == role_id;
      //   resolve(res);
      // });
      resolve(true);
    })
    .then((res) => {
      return res;
    })
    .catch(err => {
      if (next) next(err);
      throw err;
    });
  };

  cm_role.remoteMethod(
    'isServiceAdmin', {
      description: '해당 role_id 의 Service Admin 여부 확인.',
      accepts: [
        {arg: 'role_id', type: 'string', required: true}
        // ,{arg: 'user_id', type: 'string', required: true}
      ],
      returns: {root: true},
      isStatic: true,
      http: {verb: 'get'}
    }
  );
};
