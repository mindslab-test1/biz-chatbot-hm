'use strict';

const app = require('../server');
const moment = require('moment');
const models = app.models;
const NodeCache = require('node-cache');

const configCache = require('../config').NodeCache;
const path = require('path');

module.exports = function(monitor_server) {
    const loggerName = path.basename(__filename, path.extname(__filename));
    var config = configCache;
    if (configCache) {

    } else {
        config = {stdTTL: 60, checkperiod: 0};
    }
    const sCache = new NodeCache(config);

    sCache.on('set', function (key, value) {
        // console.log('========================================================================================');
        // console.log('set', sCache.getStats());
        // console.log('========================================================================================');
        app.devlog(loggerName, '========================================================================================');
        app.devlog(loggerName, 'set', sCache.getStats());
        app.devlog(loggerName, '========================================================================================');
    });

    sCache.on('expired', function (key, value) {
        // console.log('========================================================================================');
        // console.log('expired', sCache.getStats());
        // console.log('========================================================================================');
        app.devlog(loggerName, '========================================================================================');
        app.devlog(loggerName, 'expired', sCache.getStats());
        app.devlog(loggerName, '========================================================================================');
    });


    monitor_server.remoteMethod(
        'getServiceNames', {
            http: {path: '/getServiceNames', verb: 'get'},
            returns: {root: true},
        }
    );

    monitor_server.getServiceNames = function (next) {
        var date = new Date();
        var cacheTempName = 'D_' + date.getTime();

        sCache.get(cacheTempName, function(err, value) {

            app.devlog(loggerName, 'cache value !!!', cacheTempName, value);

            if (!err) {
                if (value == undefined) {
                    let sql;

                    sql =`select  server_ip as serverIp,
                                   server_name as serverName
                          from monitor_server_name_tb
                          order by server_name ASC`;

                    const connector = monitor_server.app.datasources.statistic.connector;

                    connector.query(sql, null, (err, result) => {
                        if (err) {
                            next(err);
                        } else {
                            var arrayData = [];

                            if (result && result.length > 0) {
                                for (var x = 0; x < result.length; x++) {
                                    arrayData.push(result[x].serverName);
                                }
                            }

                            app.devlog(loggerName, '========================================================================================');
                            app.devlog(loggerName, 'result =', arrayData);
                            app.devlog(loggerName, '========================================================================================');

                            sCache.set(cacheTempName, arrayData, function(err, success) {
                                if (!err && success) {
                                    // console.log(success);
                                    app.devlog(loggerName, success);
                                }
                            });
                            next(null, arrayData);
                        }

                    });
                } else {
                    next(null, value);
                }
            }
        });
    };


    monitor_server.remoteMethod(
        'getCpuUsed', {
            http: {path: '/getCpuUsed', verb: 'get'},
            accepts: [
                {arg: 'serviceName', type: 'string'},
                {arg: 'type', type: 'string', required: false},
                {arg: 'start_date', type: 'date', required: false},
                {arg: 'end_date', type: 'date', required: false},
            ],
            returns: {root: true},
        }
    );

    monitor_server.getCpuUsed = function (serviceName, type, start_date, end_date, next) {
        app.devlog(loggerName, 'start_date :', start_date, `${moment(start_date).format('YYYY-MM-DD')}`);
        app.devlog(loggerName, 'end_date :', end_date, `${moment(end_date).format('YYYY-MM-DD')}`);

        var cacheTempName = 'CPU_' + serviceName + '_' + `${moment(start_date).format('YYYYMMDD')}` + '_' + `${moment(end_date).format('YYYYMMDD')}`;

        // console.log('cache value !!!', cacheTempName, value);

        sCache.get(cacheTempName, function(err, value) {

            app.devlog(loggerName, 'cache value !!!', cacheTempName, value);

            if (!err) {
                if (value == undefined) {
                    let sql;


                    if (type && type != null && type != undefined && type != '' && type.toLowerCase() == 'time') {
                        sql =`select server_name as serverName,
                                     DATE_FORMAT( DATE_FORMAT(created_dtm, '%Y-%m-%d %H:00:00') ,'%Y-%m-%dT%TZ') as cpuChartLables,`;

                    }else{
                        sql =`select server_name as serverName,
                                 DATE_FORMAT(CONVERT_TZ( DATE_FORMAT(created_dtm, '%Y-%m-%d 00:00:00'), @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as cpuChartLables,`;
                    }

                    sql +=`   round(avg(cpu_used),2) as cpuUsed
                          from (
                                  select p.server_name,
                                          p.server_ip,
                                          s.cpu_used,
                                          s.created_dtm
                                   from monitor_server_tb s
                                        inner join monitor_server_name_tb p on s.server_ip = p.server_ip
                                )s
                          where server_name ='${serviceName}'
                              and created_dtm >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                              and created_dtm < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`;

                    if (type && type != null && type != undefined && type != '' && type.toLowerCase() == 'time') {

                        sql += `  group by   DATE_FORMAT(created_dtm, '%Y-%m-%d %H'), server_name
                                  order by DATE_FORMAT(created_dtm, '%Y-%m-%d %H') asc, server_name`;
                    }else{
                        sql += `  group by   DATE_FORMAT(created_dtm, '%Y-%m-%d'), server_name
                                  order by DATE_FORMAT(created_dtm, '%Y-%m-%d') asc, server_name`;
                    }

                    const connector = monitor_server.app.datasources.statistic.connector;

                    app.devlog(sql);

                    connector.query(sql, null, (err, result) => {
                        if (err) {
                            next(err);
                        } else {
                            const arrayData = [];
                            let max  =0;
                            let min = 9999999;
                            let avg = 0;
                            let count = 0;
                            let sumMemUsage = 0;

                            if (result && result.length > 0) {
                                for (var x = 0; x < result.length; x++) {
                                    const temp = {period: result[x].cpuChartLables, severName: result[x].serverName,  cpuUsed: result[x].cpuUsed};
                                    arrayData.push(temp)
                                    count++;

                                    if(max < result[x].cpuUsed) {
                                        max = result[x].cpuUsed;
                                    }

                                    if(min > result[x].cpuUsed) {
                                        min = result[x].cpuUsed;
                                    }

                                    sumMemUsage += result[x].cpuUsed;

                                }

                                avg = (sumMemUsage) / count;
                                avg = Math.floor(avg *100) / 100;
                            } else {
                                max = 0;
                                min = 0;
                            }

                            var resultData = {
                                data :arrayData,
                                maxUsage: max,
                                minUsage: min,
                                avgUsage: avg
                            };


                            app.devlog(loggerName, '========================================================================================');
                            app.devlog(loggerName, 'result =', resultData);
                            app.devlog(loggerName, '========================================================================================');

                            sCache.set(cacheTempName, resultData, function(err, success) {
                                if (!err && success) {
                                    // console.log(success);
                                    app.devlog(loggerName, success);
                                }
                            });

                            next(null, resultData);
                        }

                    });
                } else {
                    next(null, value);
                }
            }
        });
    };


  /*  monitor_server.getCpuUsed = function (serviceName, type, start_date, end_date, next) {
        app.devlog(loggerName, 'start_date :', start_date, `${moment(start_date).format('YYYY-MM-DD')}`);
        app.devlog(loggerName, 'end_date :', end_date, `${moment(end_date).format('YYYY-MM-DD')}`);

        var cacheTempName = 'D_' + serviceName + '_' + `${moment(start_date).format('YYYYMMDD')}` + '_' + `${moment(end_date).format('YYYYMMDD')}`;

        // console.log('cache value !!!', cacheTempName, value);

        sCache.get(cacheTempName, function(err, value) {

            app.devlog(loggerName, 'cache value !!!', cacheTempName, value);

            if (!err) {
                if (value == undefined) {
                    let sql;

                    sql =`select serverName,
                                  cpuChartLables,
                                  sum(cpuUsed) as cpuUsed
                            from(
                                  select  server_name as serverName,
                                          DATE_FORMAT(CONVERT_TZ( DATE_FORMAT(created_dtm, '%Y-%m-%d %H:00:00'), @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as cpuChartLables,
                                         round(avg(cpu_used),2) as cpuUsed
                                  from monitor_server_tb
                                   where 1=1
                                       and created_dtm >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                                       and created_dtm < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`;

                    if (type && type != null && type != undefined && type != '' && type.toLowerCase() == 'time') {

                        sql += `  group by DATE_FORMAT(created_dtm, '%Y-%m-%d %H'), server_name`;
                    }else{
                        sql += `  group by DATE_FORMAT(created_dtm, '%Y-%m-%d'), server_name`;
                    }

                    var serverNames = serviceName.split(',');

                    for(var name of serverNames){
                        sql += `    union all
                                  select distinct  '${name}' as serverName,
                                                   DATE_FORMAT(CONVERT_TZ( DATE_FORMAT(created_dtm, '%Y-%m-%d %H:00:00'), @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as cpuChartLables,
                                                   0 as cpuUSed
                                  from monitor_server_tb
                                   where 1=1
                                        and created_dtm >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                                        and created_dtm < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`;
                    }

                    sql+= `)t
                            group by serverName, cpuChartLables
                            order by cpuChartLables asc, serverName asc`;


                    app.devlog(sql);

                    const connector = monitor_server.app.datasources.statistic.connector;

                    connector.query(sql, null, (err, result) => {
                        if (err) {
                            next(err);
                        } else {
                            var arrayData = [];

                            if (result && result.length > 0) {
                                for (var x = 0; x < result.length; x++) {
                                    const temp = {period: result[x].cpuChartLables, serverName: result[x].serverName,  cpuUsed: result[x].cpuUsed};
                                    arrayData.push(temp)
                                }
                            }

                            app.devlog(loggerName, '========================================================================================');
                            app.devlog(loggerName, 'result =', arrayData);
                            app.devlog(loggerName, '========================================================================================');

                            sCache.set(cacheTempName, arrayData, function(err, success) {
                                if (!err && success) {
                                    // console.log(success);
                                    app.devlog(loggerName, success);
                                }
                            });

                            next(null, arrayData);

                        }

                    });
                } else {
                    next(null, value);
                }
            }
        });
    };*/


    monitor_server.remoteMethod(
        'getMemUsed', {
            http: {path: '/getMemUsed', verb: 'get'},
            accepts: [
                {arg: 'serviceName', type: 'string'},
                {arg: 'type', type: 'string', required: false},
                {arg: 'start_date', type: 'date', required: false},
                {arg: 'end_date', type: 'date', required: false},
            ],
            returns: {root: true},
        }
    );

    monitor_server.getMemUsed = function (serviceName, type, start_date, end_date, next) {
        app.devlog(loggerName, 'start_date :', start_date, `${moment(start_date).format('YYYY-MM-DD')}`);
        app.devlog(loggerName, 'end_date :', end_date, `${moment(end_date).format('YYYY-MM-DD')}`);

        var cacheTempName = 'MEM_' + serviceName + '_' + `${moment(start_date).format('YYYYMMDD')}` + '_' + `${moment(end_date).format('YYYYMMDD')}`;

        // console.log('cache value !!!', cacheTempName, value);

        sCache.get(cacheTempName, function(err, value) {

            app.devlog(loggerName, 'cache value !!!', cacheTempName, value);

            if (!err) {
                if (value == undefined) {
                    let sql;

                    if (type && type != null && type != undefined && type != '' && type.toLowerCase() == 'time') {
                        sql =`select server_name as serverName,
                                     DATE_FORMAT( DATE_FORMAT(created_dtm, '%Y-%m-%d %H:00:00') ,'%Y-%m-%dT%TZ') as cpuChartLables,`;

                    }else{
                        sql =`select server_name as serverName,
                                 DATE_FORMAT(CONVERT_TZ( DATE_FORMAT(created_dtm, '%Y-%m-%d 00:00:00'), @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as cpuChartLables,`;
                    }

                    sql +=`   avg(mem_total) as memTotal,
                                 round(avg(mem_usage),2) as memUsage,
                                 round(avg(mem_used),2) as memUsed
                           from (
                                  select p.server_name,
                                          p.server_ip,
                                          s.mem_total,
                                          s.mem_usage,
                                          s.mem_used,
                                          s.created_dtm
                                   from monitor_server_tb s
                                        inner join monitor_server_name_tb p on s.server_ip = p.server_ip
                                )s
                          where server_name ='${serviceName}'
                              and created_dtm >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                              and created_dtm < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`;

                    if (type && type != null && type != undefined && type != '' && type.toLowerCase() == 'time') {

                        sql += `  group by   DATE_FORMAT(created_dtm, '%Y-%m-%d %H'), server_name
                                  order by DATE_FORMAT(created_dtm, '%Y-%m-%d %H') asc, server_name`;
                    }else{
                        sql += `  group by   DATE_FORMAT(created_dtm, '%Y-%m-%d'), server_name
                                  order by DATE_FORMAT(created_dtm, '%Y-%m-%d') asc, server_name`;
                    }

                    app.devlog(sql);

                    const connector = monitor_server.app.datasources.statistic.connector;

                    connector.query(sql, null, (err, result) => {
                        if (err) {
                            next(err);
                        } else {
                            var arrayData = [];
                            let max  =0;
                            let min = 9999999;
                            let avg = 0;
                            let count = 0;
                            let sumdiskUsage = 0;

                            if (result && result.length > 0) {
                                for (var x = 0; x < result.length; x++) {
                                    const temp = {period: result[x].cpuChartLables,
                                                  severName: result[x].serverName,
                                                  memTotal: result[x].memTotal,
                                                  memUsage: result[x].memUsage,
                                                  memUsed: result[x].memUsed};

                                    count++;
                                    sumdiskUsage += result[x].memUsage;

                                    arrayData.push(temp);

                                    if (max < result[x].memUsage) {
                                        max = result[x].memUsage;
                                    }

                                    if (min > result[x].memUsage) {
                                        min = result[x].memUsage;
                                    }
                                }

                                avg = (sumdiskUsage) / count;
                                avg = Math.floor(avg *100) / 100;

                                var resultData = {
                                    data :arrayData,
                                    maxUsage: max,
                                    minUsage: min,
                                    avgUsage: avg
                                };

                            }

                            app.devlog(loggerName, '========================================================================================');
                            app.devlog(loggerName, 'result =', resultData);
                            app.devlog(loggerName, '========================================================================================');

                            sCache.set(cacheTempName, resultData, function(err, success) {
                                if (!err && success) {
                                    // console.log(success);
                                    app.devlog(loggerName, success);
                                }
                            });

                            next(null, resultData);

                        }

                    });
                } else {
                    next(null, value);
                }
            }
        });
    };


    monitor_server.remoteMethod(
        'getDiskUsed', {
            http: {path: '/getDiskUsed', verb: 'get'},
            accepts: [
                {arg: 'serviceName', type: 'string'},
                {arg: 'type', type: 'string', required: false},
                {arg: 'start_date', type: 'date', required: false},
                {arg: 'end_date', type: 'date', required: false},
            ],
            returns: {root: true},
        }
    );


    monitor_server.getDiskUsed = function (serviceName, type, start_date, end_date, next) {
        app.devlog(loggerName, 'start_date :', start_date, `${moment(start_date).format('YYYY-MM-DD')}`);
        app.devlog(loggerName, 'end_date :', end_date, `${moment(end_date).format('YYYY-MM-DD')}`);

        var cacheTempName = 'DISK_' + serviceName + '_' + `${moment(start_date).format('YYYYMMDD')}` + '_' + `${moment(end_date).format('YYYYMMDD')}`;

        // console.log('cache value !!!', cacheTempName, value);

        sCache.get(cacheTempName, function(err, value) {

            app.devlog(loggerName, 'cache value !!!', cacheTempName, value);

            if (!err) {
                if (value == undefined) {
                    let sql;

                    if (type && type != null && type != undefined && type != '' && type.toLowerCase() == 'time') {
                        sql =`select server_name as serverName,
                                     DATE_FORMAT( DATE_FORMAT(created_dtm, '%Y-%m-%d %H:00:00') ,'%Y-%m-%dT%TZ') as cpuChartLables,`;

                    }else{
                        sql =`select server_name as serverName,
                                 DATE_FORMAT(CONVERT_TZ( DATE_FORMAT(created_dtm, '%Y-%m-%d 00:00:00'), @@session.time_zone, '+00:00')  ,'%Y-%m-%dT%TZ') as cpuChartLables,`;
                    }

                    sql +=`   avg(disk_total) as diskTotal,
                                 round(avg(disk_usage),2) as diskUsage,
                                 round(avg(disk_used),2) as diskUsed    
                          from (
                                  select p.server_name,
                                          p.server_ip,
                                          s.disk_total,
                                          s.disk_usage,
                                          s.disk_used,
                                          s.created_dtm
                                   from monitor_server_tb s
                                        inner join monitor_server_name_tb p on s.server_ip = p.server_ip
                                )s
                          where server_name ='${serviceName}'
                              and created_dtm >= '${moment(start_date).utc().format('YYYY-MM-DD HH:mm:ss')}'
                              and created_dtm < '${moment(end_date).utc().format('YYYY-MM-DD HH:mm:ss')}'`;

                    if (type && type != null && type != undefined && type != '' && type.toLowerCase() == 'time') {

                        sql += `  group by   DATE_FORMAT(created_dtm, '%Y-%m-%d %H'), server_name
                                  order by DATE_FORMAT(created_dtm, '%Y-%m-%d %H') asc, server_name`;
                    }else{
                        sql += `  group by   DATE_FORMAT(created_dtm, '%Y-%m-%d'), server_name
                                  order by DATE_FORMAT(created_dtm, '%Y-%m-%d') asc, server_name`;
                    }

                    app.devlog(sql);

                    const connector = monitor_server.app.datasources.statistic.connector;

                    connector.query(sql, null, (err, result) => {
                        if (err) {
                            next(err);
                        } else {
                            var arrayData = [];
                            let max  =0;
                            let min = 0;
                            let avg = 0;
                            let count = 0;
                            let sumdiskUsage = 0;

                            if (result && result.length > 0) {
                                for (var x = 0; x < result.length; x++) {
                                    const temp = {
                                                  period: result[x].cpuChartLables,
                                                  serverName: result[x].serverName,
                                                  diskTotal: result[x].diskTotal,
                                                  diskUsage: result[x].diskUsage,
                                                  diskUsed: result[x].diskUsed
                                                };

                                    count++;
                                    sumdiskUsage += result[x].diskUsage;

                                    arrayData.push(temp);

                                    if (max < result[x].diskUsage) {
                                        max = result[x].diskUsage;
                                    }

                                    if (x === 0) {
                                        min = result[x].diskUsage;
                                    } else {
                                        if (min > result[x].diskUsage) {
                                            min = result[x].diskUsage;
                                        }
                                    }
                                }

                                avg = (sumdiskUsage) / count;
                                avg = Math.floor(avg *100) / 100;

                                var resultData = {
                                    data :arrayData,
                                    maxUsage: max,
                                    minUsage: min,
                                    avgUsage: avg
                                };
                            }

                            app.devlog(loggerName, '========================================================================================');
                            app.devlog(loggerName, 'result =', resultData);
                            app.devlog(loggerName, '========================================================================================');

                            sCache.set(cacheTempName, resultData, function(err, success) {
                                if (!err && success) {
                                    // console.log(success);
                                    app.devlog(loggerName, success);
                                }
                            });

                            next(null, resultData);

                        }

                    });
                } else {
                    next(null, value);
                }
            }
        });
    };


};
