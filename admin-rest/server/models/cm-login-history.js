'use strict';
const app = require('../server');
const path = require('path');

module.exports = function(cm_login_history) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  cm_login_history.disableRemoteMethodByName('create', true);
  cm_login_history.disableRemoteMethodByName('upsert', true);
  cm_login_history.disableRemoteMethodByName('deleteById', true);
  cm_login_history.disableRemoteMethodByName('updateAll', true);
  cm_login_history.disableRemoteMethodByName('updateAttributes', false);
  cm_login_history.disableRemoteMethodByName('createChangeStream', true);
};
