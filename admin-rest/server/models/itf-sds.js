'use strict';
const app = require('../server');
const path = require('path');

module.exports = function (itf_sds) {
    const loggerName = path.basename(__filename, path.extname(__filename));

    var Transaction = require('loopback-datasource-juggler').Transaction;

    itf_sds.remoteMethod(
        'deleteByBotId', {
            accepts: [
                { arg: 'botId', type: 'number', required: true }
            ],
            returns: { root: true },
            isStatic: true
        });
    itf_sds.deleteByBotId = function (botId, next) {

        
        itf_sds.destroyAll({ bot_id: botId }, function (err, res) {
            // console.log("========================================================================================");
            // console.log("itf_intent_domain_rel destroyAll =", res);
            // console.log("========================================================================================");
            app.devlog(loggerName, "========================================================================================");
            app.devlog(loggerName, "itf_intent_domain_rel destroyAll =", res);
            app.devlog(loggerName, "========================================================================================");
            next(null, res);

        });
    }


    itf_sds.remoteMethod(
        'updateListOrder', {
            accepts: [
                { arg: 'sdsList', type: 'array', required: true }
            ],
            returns: { root: true },
            isStatic: true
        });

    itf_sds.updateListOrder = function (sdsList, next) {

        itf_sds.beginTransaction({
            isolationLevel: Transaction.READ_COMMITTED,
            timeout: 30000 // 30000ms = 30s
        }, function (err, tx) {
            console.log(sdsList);
            next(null, sdsList);
            if (sdsList == null || sdsList.lengh == 0) {
                next("sdsList is null");
                tx.rollback();
            }

            itf_sds.destroyAll({ bot_id: sds[0].bot_id }, { transaction: tx }, function (err, res) {

                if (err) {
                    next(err)
                    tx.rollback();
                } else {
                    itf_sds.create(sdsList, { transaction: tx }, function (err, res) {
                        if (err) {

                        } else {
                            next(null, res);
                            tx.commit();
                        }
                    });
                }
            });
        });
    };
}