'use strict';
const app = require('../server');
const models = app.models;
const path = require('path');

module.exports = function (Chatbotinfo) {
  const loggerName = path.basename(__filename, path.extname(__filename));


  Chatbotinfo.afterRemote('create', function (ctx, params, next) {

    app.devlog(loggerName, 'Chatbotinfo create after \nparams ====\n', params, '\n\nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    const bot_id = params.bot_id;
    let scf_yn = 'N';
    let dnn_yn = 'N';
    if (ctx.args.data.chatbotConfigs && ctx.args.data.chatbotConfigs[0]) {
      scf_yn = ctx.args.data.chatbotConfigs[0].scf_yn;
      dnn_yn = ctx.args.data.chatbotConfigs[0].dnn_yn;
    }
    app.devlog(loggerName, 'Chatbotinfo create', bot_id);
    models.chatbot_config.create(

      Object.assign({
        bot_id,
        scf_yn,
        dnn_yn,
        created_dtm: params.created_dtm,
        creator_id: params.creator_id,
        updated_dtm: params.updated_dtm,
        updator_id: params.updator_id
      }, getRemoteInformation(ctx)));
    next();
  });



  Chatbotinfo.afterRemote('upsertWithWhere', function (ctx, params, next) {

    app.devlog(loggerName, 'Chatbotinfo upsertWithWhere after \nparams ====\n', params, '\n\nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    const bot_id = params.bot_id;

    app.devlog(loggerName, 'Chatbotinfo upsertWithWhere', bot_id);


    const data = ctx.args.data;
    const created_dtm = data.created_dtm;
    const updated_dtm = data.updated_dtm;
    const creator_id = data.creator_id;
    const updator_id = data.updator_id;

    if (data.chatbotConfigs && data.chatbotConfigs[0]) {
      const conf_id = data.chatbotConfigs[0].conf_id;
      const scf_yn = data.chatbotConfigs[0].scf_yn;
      const dnn_yn = data.chatbotConfigs[0].dnn_yn;
      const bqa_reconfirm_msg = data.chatbotConfigs[0].bqa_reconfirm_msg;
      const bqa_search_flow_type = data.chatbotConfigs[0].bqa_search_flow_type;
      const bqa_search_solr_or_match_range = data.chatbotConfigs[0].bqa_search_solr_or_match_range;
      models.chatbot_config.upsertWithWhere({
        bot_id,
        conf_id
      }, {
        bot_id,
        conf_id,
        scf_yn,
        dnn_yn,
        created_dtm,
        updated_dtm,
        creator_id,
        updator_id,
        bqa_reconfirm_msg,
        bqa_search_flow_type,
        bqa_search_solr_or_match_range
      }, getRemoteInformation(ctx));
      app.devlog(loggerName, 'Chatbotinfo chatbot_config upsert', bot_id, conf_id, scf_yn, dnn_yn);
    }

    const _updateMsgs = (messages) => {
      const _update = () => {
        messages.map((message) => {
          const msg_id = message.msg_id;
          const msg_type_cd = message.msg_type_cd;
          const msg = message.msg;
          models.chatbot_msg.upsertWithWhere({
            msg_id,
            bot_id
          }, {
            msg_id,
            bot_id,
            msg_type_cd,
            msg,
            created_dtm,
            updated_dtm,
            creator_id,
            updator_id
          }, getRemoteInformation(ctx));
          // models.cm_workspace_team.bulkUpdate();
        });
      };
      const msgIds = messages.reduce((res, msg) => {
        res.push(msg.msg_id);
        return res;
      }, []);
      models.chatbot_msg.destroyAll({
        bot_id,
        msg_id: {
          inq: msgIds
        }
      }, _update);
    };

    const _updateBtns = (richContent, callback) => {
      const rc_id = richContent.rc_id;
      const buttons = richContent.buttons;
      const created_dtm = richContent.created_dtm;
      const updated_dtm = richContent.updated_dtm;
      const creator_id = richContent.creator_id;
      const updator_id = richContent.updator_id;

      const _update = () => {
        buttons.map((button) => {
          const btn_order = button.btn_order;
          const title = button.title;
          const user_utter = button.user_utter;
          models.chatbot_buttons.upsertWithWhere({
            rc_id,
            btn_order
          }, {
            rc_id,
            btn_order,
            title,
            user_utter,
            created_dtm,
            updated_dtm,
            creator_id,
            updator_id
          }, getRemoteInformation(ctx));
        });
        callback();
      };
      models.chatbot_buttons.destroyAll({
        rc_id
      }, _update);
    };
    if (data.chatbotMsgs) {
      const messages = data.chatbotMsgs;
      _updateMsgs(messages);
    }
    const content_type_cd = 'RT0001';
    if (data.chatbotConfigs && data.chatbotConfigs[0] &&
        data.chatbotConfigs[0].richContentGreeting) {
      const richContent = data.chatbotConfigs[0].richContentGreeting;
      const rc_id = richContent.rc_id;
      _updateBtns(richContent, () => {
        // app.devlog('upsert greeting rich_content', rc_id, content_type_cd);
        models.chatbot_rich_content.upsertWithWhere({
          rc_id
        }, {
          rc_id,
          content_type_cd,
          created_dtm,
          updated_dtm,
          creator_id,
          updator_id
        }, getRemoteInformation(ctx));
      });
    }
    if (data.chatbotConfigs && data.chatbotConfigs[0] &&
        data.chatbotConfigs[0].richContentUnknown) {
      const richContent = data.chatbotConfigs[0].richContentUnknown;
      const rc_id = richContent.rc_id;
      _updateBtns(richContent, () => {
        // app.devlog('upsert unknown rich_content', rc_id, content_type_cd);
        models.chatbot_rich_content.upsertWithWhere({
          rc_id
        }, {
          rc_id,
          content_type_cd,
          created_dtm,
          updated_dtm,
          creator_id,
          updator_id
        }, getRemoteInformation(ctx));
      });
    }
    next();
  });

  Chatbotinfo.afterRemoteError('upsertWithWhere', function (ctx, next) {
    app.devlog(loggerName, 'Chatbotinfo upsertWithWhere err after \nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    next();
  });


  // TODO 삭제시 하위 데이터들도 삭제....
  // chatbot_config
  // chatbot_config_domain
  // chatbot_config_sds_domain

  function getRemoteInformation(ctx) {
    app.devlog(loggerName, 'Chatbotinfo getRemoteInformation');
    return {
      remote: ctx.req.connection.remoteAddress || ctx.req.headers['x-forwarded-for'],
      agent: ctx.req.headers['user-agent']
    };
  };

  Chatbotinfo.search = function (data, next) {
    app.devlog(loggerName, 'search');
    // console.log('search data',data);
    let filter = data;
    const obs = models.chatbot_info.find(filter, (err, items) => {
      if (err) {
        return next(err);
      }
      next(null, items);
    });
    return new Promise((resolve, reject) => obs);
  };

  Chatbotinfo.remoteMethod(
    'search', {
      accepts: [{
        arg: 'data',
        type: 'object',
        required: true,
        http: {
          source: 'body'
        }
      }],
      returns: {
        root: true
      }
    }
  );
  const parseButtons = (buttons)=>{
    return buttons.reduce((resLs, v)=>{
      const item = {
         rc_id: v['rc_id']
        ,btn_order: v['btn_order']
        ,title: v['title']
        ,user_utter: v['user_utter']
        ,sds_domain: v['sds_domain']
        ,created_dtm: v['created_dtm']
        ,updated_dtm: v['updated_dtm']
        ,creator_id: v['creator_id']
        ,updator_id: v['updator_id']
      };
      resLs.push(item);
      return resLs;
    },[]);
  };
  Chatbotinfo.updateChatbot = function (where, data, next) {
    app.devlog(loggerName, 'Chatbotinfo updateChatbot where' ,where);
    app.devlog(loggerName, 'Chatbotinfo updateChatbot data' ,data);

    var Transaction = require('loopback-datasource-juggler').Transaction;

    Chatbotinfo.beginTransaction({
      isolationLevel: Transaction.READ_COMMITTED,
      timeout: 30000
    }, function (err, tx) {

      const bot_id = data.bot_id;
      const updated_dtm = data.updated_dtm;
      const updator_id = data.updator_id;


      var promise1 = function () {
        return new Promise(function (resolve, reject) {
          Chatbotinfo.upsertWithWhere(where, data, {
            transaction: tx
          }, function (err, res) {
            if (err) {
              reject(err);
            } else {
              resolve();
            }
          });
        })
      };


      var promise2 = function () {
        return new Promise(function (resolve, reject) {

          if (data.chatbotConfigs && data.chatbotConfigs[0]) {
            const conf_id = data.chatbotConfigs[0].conf_id;
            const scf_yn = data.chatbotConfigs[0].scf_yn;
            const dnn_yn = data.chatbotConfigs[0].dnn_yn;
            const synonym_yn = data.chatbotConfigs[0].synonym_yn;
            const bqa_reconfirm_msg = data.chatbotConfigs[0].bqa_reconfirm_msg;
            const bqa_search_flow_type = data.chatbotConfigs[0].bqa_search_flow_type;
            const bqa_search_solr_or_match_range = data.chatbotConfigs[0].bqa_search_solr_or_match_range;
            models.chatbot_config.upsertWithWhere({
              bot_id,
              conf_id
            }, {
              bot_id,
              conf_id,
              scf_yn,
              dnn_yn,
              synonym_yn,
              updated_dtm,
              updator_id,
              bqa_reconfirm_msg,
              bqa_search_flow_type,
              bqa_search_solr_or_match_range
            }, {
              transaction: tx
            }, function (err, res) {
              if (err) {
                reject(err);
              } else {
                resolve();
              }
            });
          };
        });
      }


      var promise3 = function () {
        return new Promise(function (resolve, reject) {

          if (data.chatbotMsgs) {
            models.chatbot_msg.destroyAll({
              bot_id: bot_id
            }, {
              transaction: tx
            }, function (err, res) {
              if (err) {
                reject(err);
              } else {
                models.chatbot_msg.create(data.chatbotMsgs, {
                  transaction: tx
                }, function (err, res) {
                  if (err) {
                    reject(err);
                  } else {
                    resolve();
                  }
                });
              }
            });
          } else {
            resolve();
          }
        });
      };


      var promise4 = function () {
        return new Promise(function (resolve, reject) {
          const content_type_cd = 'RT0001';
          if (data.chatbotConfigs[0].richContentGreeting) {
            const richContent = data.chatbotConfigs[0].richContentGreeting;

            const rc_id = richContent.rc_id;
            var buttons = richContent.buttons;

            models.chatbot_rich_content.upsertWithWhere({
              rc_id
            }, {
              rc_id,
              content_type_cd,
              updated_dtm,
              updator_id
            }, {
              transaction: tx
            }, function (err, res) {
              if (err) {
                reject(err);
              } else {
                models.chatbot_buttons.destroyAll({
                  rc_id
                }, {
                  transaction: tx
                }, function (err, res) {
                  if (err) {
                    reject(err);
                  } else {

                    if(buttons && buttons != undefined){
                      buttons = parseButtons(buttons); // loopback bug
                      models.chatbot_buttons.create(buttons, {
                        transaction: tx
                      }, function (err, res) {
                        if (err) {
                          reject(err);
                        } else {
                          resolve();
                        }
                      })
                    }else{
                      resolve();
                    }
                  }
                });
              }
            });
          } else {
            resolve();
          }
        });
      };


      var promise5 = function () {
        return new Promise(function (resolve, reject) {
          const content_type_cd = 'RT0001';
          if (data.chatbotConfigs[0].richContentUnknown) {

            const richContent = data.chatbotConfigs[0].richContentUnknown;
            const rc_id = richContent.rc_id;

            var buttons = richContent.buttons;

            models.chatbot_rich_content.upsertWithWhere({
              rc_id
            }, {
              rc_id,
              content_type_cd,
              updated_dtm,
              updator_id
            }, {
              transaction: tx
            }, function (err, res) {
              if (err) {
                reject(err);
              } else {
                models.chatbot_buttons.destroyAll({
                  rc_id
                }, {
                  transaction: tx
                }, function (err, res) {
                  if (err) {
                    reject(err);
                  } else {

                    if(buttons && buttons != undefined){
                        buttons = parseButtons(buttons); // loopback bug
                        models.chatbot_buttons.create(buttons, {
                          transaction: tx
                        }, function (err, res) {
                          if (err) {
                            reject(err);
                          } else {
                            resolve();
                          }
                        })
                    }else{
                      resolve();
                    }
                  }
                });
              }
            });
          } else {
            resolve();
          }
        });
      };


      Promise.all([promise1(), promise2(), promise3(), promise4(), promise5()]).then(function () {
        // console.log("commit base");
        app.devlog(loggerName, "commit base");
        tx.commit();
        next(null, "Success");
      }).catch(function (err) {

        // console.log("rollback base", err);
        app.devlog(loggerName, "rollback base", err);

        tx.rollback();
        next(err);
      });
    });
  };

  

  Chatbotinfo.remoteMethod(
    'updateChatbot', {
      accepts: [{
          arg: 'where',
          type: 'object'
        },
        {
          arg: 'data',
          type: 'object',
          http: { source : 'body' }
        },
      ],
      returns: {
        root: true
      },
      isStatic: true
    }
  );

};
