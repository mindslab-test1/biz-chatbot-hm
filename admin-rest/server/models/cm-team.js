'use strict';
const app = require('../server');
const models = app.models;
const NodeCache = require('node-cache');
const configCache = require('../config').NodeCache;
const path = require('path');

module.exports = function(cm_team) {
  const loggerName = path.basename(__filename, path.extname(__filename));
  var config = configCache;

  if (configCache) {

  } else {
    config = {stdTTL: 60, checkperiod: 0};
  }

  const  sCache = new NodeCache(config);

  cm_team.afterRemoteError('deleteById', function (ctx, next) {
    app.devlog(loggerName, 'team deleteById err after \nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    next();
  });
  cm_team.beforeRemote('deleteById', function (ctx, params, next) {
    app.devlog(loggerName, 'team deleteById before \nparams ====\n', params,'\n\nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    const team_id = ctx.args.id;
    models.cm_workspace_team.destroyAll({team_id}, next);
  });


  cm_team.afterRemoteError('deleteByIds', function (ctx, next) {
    app.devlog(loggerName, 'team deleteByIds err after \nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    next();
  });
  cm_team.beforeRemote('deleteByIds', function (ctx, params, next) {
    app.devlog(loggerName, 'team deleteByIds before \nparams ====\n', params,'\n\nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    const teamIds = ctx.args.ids;
    if (teamIds.length < 1) {
      next(teamIds);
    }
    models.cm_workspace_team.destroyAll({team_id: {inq: teamIds}}, next);
  });
  // delete By Ids
  cm_team.deleteByIds = function(ids = []/* , next */) {
    app.devlog(loggerName, 'team deleteByIds \nids ====\n', JSON.stringify(ids));
    return Promise.all(
        ids.map(id => cm_team.destroyById(id).catch(() => ({count: 0})))
      )
      .then(result => {
        let deletedIds = ids.filter((id, index) => result[index].count !== 0);
        return deletedIds;
      });
  };
  cm_team.remoteMethod(
    'deleteByIds', {
      accepts: [
        {arg: 'ids', type: ['number'], required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );

  cm_team.afterRemote('create', function (ctx, params, next) {
    app.devlog(loggerName, 'team create after \nparams ====\n', params,'\n\nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    const team_id = params.team_id;
    const workspaces = ctx.args.data.workspaces;
    workspaces.map((workspace)=>{
      const workspace_id = workspace.workspace_id;
      app.devlog(loggerName, 'team create', team_id, workspace_id);

      models.cm_workspace_team.create(
        Object.assign({
          team_id,
          workspace_id
        }, getRemoteInformation(ctx)));
    });
    next();
  });

  cm_team.afterRemoteError('replaceById', function (ctx, next) {
    app.devlog(loggerName, 'team replaceById err after \nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    next();
  });
  // cm_team.afterRemote('replaceById', function (ctx, params, next) {
  //   app.devlog('team replaceById after \nparams ====\n', params,'\n\nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
  //   const team_id = params.team_id;
  //   const workspaces = ctx.args.data.workspaces;
  //   app.devlog('team destroy rels', team_id);
  //   const _update = ()=> {
  //     app.devlog('team update rels', team_id);
  //     workspaces.map((workspace)=>{
  //       const workspace_id = workspace.workspace_id;
  //       app.devlog('team update rel', team_id, workspace_id);
  //
  //       models.cm_workspace_team.upsertWithWhere({
  //         team_id,
  //         workspace_id
  //       }, {
  //         team_id,
  //         workspace_id
  //       }, getRemoteInformation(ctx));
  //       // models.cm_workspace_team.bulkUpdate();
  //     });
  //     next();
  //   };
  //   models.cm_workspace_team.destroyAll({team_id}, _update);
  // });


  function getRemoteInformation(ctx) {
    app.devlog(loggerName, 'team getRemoteInformation');
    return {
      remote: ctx.req.connection.remoteAddress || ctx.req.headers['x-forwarded-for'],
      agent: ctx.req.headers['user-agent']
    };
  };



  cm_team.remoteMethod(
    'selectTeamByWorkspaceCount', {
      http: {path: '/selectTeamByWorkspaceCount', verb: 'get'},
      accepts: [
        {arg: 'workspace_id', type: 'number', required: false},
        {arg: 'searchText', type: 'string', required: false},
      ],
      returns: {root: true},
    }
  );


  cm_team.remoteMethod(
    'selectTeamByWorkspace', {
      http: {path: '/selectTeamByWorkspace', verb: 'get'},
      accepts: [
        {arg: 'workspace_id', type: 'number', required: false},
        {arg: 'searchText', type: 'string', required: false},
        {arg: 'skip', type: 'number', required: false},
        {arg: 'limit', type: 'number', required: false},
      ],
      returns: {root: true},
    }
  );


  cm_team.selectTeamByWorkspaceCount = function(workspace_id, searchText, next) {

    const time = new Date();
    const cacheTempName = workspace_id + '_' + time.getTime();

    sCache.get(cacheTempName, function(err, value) {
      if (!err) {
        if (value == undefined) {
          let sql;
          sql = `select count(*) as cnt
            from cm_team_tb t
                inner join cm_workspace_team_tb t2 on t.team_id = t2.team_id
            where 1=1
                and t2.workspace_id = ${workspace_id}`;

          if (searchText && searchText != null && searchText !== undefined && searchText.length != 0) {
            sql += `  AND t.team_name like '%${searchText}%'`;
          }

          // console.log(sql);
          app.devlog(loggerName, sql);

          const connector = cm_team.app.datasources.mindslab.connector;

          connector.query(sql, null, (err, resultObjects) => {
            if (err) {
              next(err);
            }
            next(null, resultObjects);
          });
        }
      }
    });
  };

  cm_team.selectTeamByWorkspace = function(workspace_id, searchText, skip, limit, next) {

    const time = new Date();
    const cacheTempName = workspace_id + '_' + time.getTime();

    sCache.get(cacheTempName, function(err, value) {
      if (!err) {
        if (value == undefined) {
          let sql;
          sql = `select t.*
            from cm_team_tb t
                inner join cm_workspace_team_tb t2 on t.team_id = t2.team_id
            where 1=1
                and t2.workspace_id = ${workspace_id}`;

          if (searchText && searchText != null && searchText !== undefined && searchText.length != 0) {
            sql += `  AND t.team_name like '%${searchText}%'`;
          }

          if (skip != null && skip !== undefined ) {
            sql += `  order by t.team_id limit   ${skip}, ${limit}`;
          }else{
            sql += `  order by t.team_id `;
          }

          // console.log(sql);
          app.devlog(loggerName, sql);

          const connector = cm_team.app.datasources.mindslab.connector;
          connector.query(sql, null, (err, resultObjects) => {
            if (err) {
              next(err);
            }
            // console.log(resultObjects);
            app.devlog(loggerName, resultObjects);
            next(null, resultObjects);
          });
        }
      }
    });
  };

};
