'use strict';
const app = require('../server');
const path = require('path');

module.exports = function(chatbot_config_domain_rel) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  chatbot_config_domain_rel.remoteMethod(
    'updatesRel', {
      accepts: [
        {arg: 'where', type: 'object', required: true},
        {arg: 'datas', type: 'array', http:{source : 'body'}}
      ],
      returns: {root: true},
      isStatic: true
    }
  );
  chatbot_config_domain_rel.afterRemoteError('updatesRel', function (ctx, next) {
    app.devlog(loggerName, 'chatbot_config_domain_rel updatesRel err after \nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    next();
  });
  chatbot_config_domain_rel.afterRemote('updatesRel', function (ctx, params, next) {
    app.devlog(loggerName, 'chatbot_config_domain_rel updatesRel after \nparams ====\n', params,'\n\nargs ====\n', JSON.stringify(ctx.args), '\n\n====');

    const where = ctx.args.where;
    const conf_id = where.conf_id;
    const datas = ctx.args.datas;

    if(conf_id <= 0 || !conf_id){
      app.devlog(loggerName, 'invalid conf_id',conf_id);
      next();
      return;
    }

    const _update = ()=>{
      app.devlog(loggerName, 'update rels conf_id', conf_id);
      app.devlog(loggerName, 'update rels datas', datas);
      if(!datas || datas.length === 0){
        app.devlog(loggerName, 'no data');
        next();
        return;
      }
      datas.map((_data)=>{
        const domain_id = _data.domain_id;
        app.devlog(loggerName, 'update rel', conf_id, domain_id);

        chatbot_config_domain_rel.upsertWithWhere({
          conf_id,
          domain_id
        }, {
          conf_id,
          domain_id
        }, getRemoteInformation(ctx));
        // chatbot_config_domain_rel.bulkUpdate();
      });
      next();
    };
    chatbot_config_domain_rel.destroyAll({conf_id}, _update);
  });
  chatbot_config_domain_rel.updatesRel = function(where, datas = [], next) {
    app.devlog(loggerName, 'chatbot config domain updatesRel \nwhere ====\n', where, '\ndatas ====\n', JSON.stringify(datas));
    return Promise.all(
        datas.map(data => {})
      ).then(result => {});
  };

  function getRemoteInformation(ctx) {
    app.devlog(loggerName, 'chatbot config domain getRemoteInformation');
    return {
      remote: ctx.req.connection.remoteAddress || ctx.req.headers['x-forwarded-for'],
      agent: ctx.req.headers['user-agent']
    };
  };
};
