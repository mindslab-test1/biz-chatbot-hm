'use strict';
const app = require('../server');
const models = app.models;
const path = require('path');

module.exports = function(cm_workspace) {
  const loggerName = path.basename(__filename, path.extname(__filename));

  cm_workspace.afterRemoteError('deleteById', function (ctx, next) {
    app.devlog(loggerName, 'workspace deleteById err after \nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    next();
  });
  cm_workspace.beforeRemote('deleteById', function (ctx, params, next) {
    app.devlog(loggerName, 'workspace deleteById before \nparams ====\n', params,'\n\nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    const workspace_id = ctx.args.id;
    models.cm_workspace_team.destroyAll({workspace_id}, next);
  });


  cm_workspace.afterRemoteError('deleteByIds', function (ctx, next) {
    app.devlog(loggerName, 'workspace deleteByIds err after \nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    next();
  });
  cm_workspace.beforeRemote('deleteByIds', function (ctx, params, next) {
    app.devlog(loggerName, 'workspace deleteByIds before \nparams ====\n', params,'\n\nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    const workspaceIds = ctx.args.ids;
    if (workspaceIds.length < 1) {
      next(workspaceIds);
    }
    models.cm_workspace_team.destroyAll({workspace_id: {inq: workspaceIds}}, next);
  });
  // delete By Ids
  cm_workspace.deleteByIds = function(ids = [] /* , next */) {
    app.devlog(loggerName, 'workspace deleteByIds \nids ====\n', JSON.stringify(ids));
    return Promise.all(
        ids.map(id => cm_workspace.destroyById(id).catch(() => ({count: 0})))
      )
      .then(result => {
        let deletedIds = ids.filter((id, index) => result[index].count !== 0);
        return deletedIds;
      });
  };

  cm_workspace.remoteMethod(
    'deleteByIds', {
      accepts: [
        {arg: 'ids', type: ['number'], required: true},
      ],
      returns: {root: true},
      isStatic: true
    }
  );
  
  cm_workspace.afterRemote('create', function (ctx, params, next) {
    app.devlog(loggerName, 'workspace create after \nparams ====\n', params,'\n\nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    const workspace_id = params.workspace_id;
    const teams = ctx.args.data.teams;
    teams.map((team)=>{
      const team_id = team.team_id;
      app.devlog(loggerName, 'workspace create', workspace_id, team_id);

      models.cm_workspace_team.create(
        Object.assign({
          workspace_id,
          team_id
        }, getRemoteInformation(ctx)));
    });
    next();
  });
  
  cm_workspace.afterRemoteError('replaceById', function (ctx, next) {
    app.devlog(loggerName, 'workspace replaceById err after \nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    next();
  });
  cm_workspace.afterRemote('replaceById', function (ctx, params, next) {
    app.devlog(loggerName, 'workspace replaceById after \nparams ====\n', params,'\n\nargs ====\n', JSON.stringify(ctx.args), '\n\n====');
    const workspace_id = params.workspace_id;
    const teams = ctx.args.data.teams;
    const _update = ()=> {
      app.devlog(loggerName, 'workspace update rels', workspace_id);
      teams.map((team)=>{
        const team_id = team.team_id;
        app.devlog(loggerName, 'workspace update rel', workspace_id, team_id);
  
        models.cm_workspace_team.upsertWithWhere({
          workspace_id,
          team_id
        }, {
          workspace_id,
          team_id
        }, getRemoteInformation(ctx));
        // models.cm_workspace_team.bulkUpdate();
      });
      next();
    };
    // models.cm_workspace_team.destroyById(workspace_id);
    // _update();
    models.cm_workspace_team.destroyAll({workspace_id}, _update);
  });


  function getRemoteInformation(ctx) {
    app.devlog(loggerName, 'workspace getRemoteInformation');
    return {
      remote: ctx.req.connection.remoteAddress || ctx.req.headers['x-forwarded-for'],
      agent: ctx.req.headers['user-agent']
    };
  };
};
