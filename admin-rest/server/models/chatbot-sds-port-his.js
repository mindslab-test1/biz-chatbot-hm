'use strict';
const app = require('../server');
const _ = require('lodash');
const moment = require('moment');
const path = require('path');

module.exports = function(Chatbotsdsporthis) {
  const loggerName = path.basename(__filename, path.extname(__filename));
  Chatbotsdsporthis.getSessionCount = function(start_date, end_date, searchOptions, next) {
    const where = getWhere(start_date, end_date, searchOptions);

    const sql = 
      `SELECT
        COUNT(DISTINCT csph.session_id) as count
      FROM
        chatbot_sds_port_his_tb csph
      JOIN
        chatbot_info_tb ci
        ON ci.bot_id = csph.bot_id
      ${where}
      `;

    const connector = Chatbotsdsporthis.app.datasources.mindslab.connector;
    connector.query(sql, null, (err, resultObjects) => {
        if(err) next(err);
        next(null, resultObjects[0]);
    });
  };
  Chatbotsdsporthis.getSessionList = function(pg = 1, pg_size = 10, start_date, end_date, searchOptions, next) {
    const where = getWhere(start_date, end_date, searchOptions);
    const {orderColumn, direction} = getOrders(searchOptions);
    const startIdx = (pg_size * (pg - 1));

    const sql =
      `SELECT
          ci.bot_name
        , csph.session_id
        , csph.his_id
        , csph.bot_id
        , csph.open_dtm as open_dtm
        , csph.close_dtm as close_dtm
        , IFNULL((
          SELECT
            DISTINCT(dh.user_id)
          FROM dlg_history_tb dh
          WHERE dh.session_id = csph.session_id
          GROUP BY dh.session_id
        ), '') as user_id
        , IFNULL((
          SELECT
            IFNULL(COUNT(DISTINCT(dh.domain_name)),0)
          FROM dlg_history_tb dh
          WHERE dh.session_id = csph.session_id
            AND dh.domain_name != ''
          GROUP BY dh.session_id
        ), 0) as domain_count
        , IFNULL((
          SELECT
            IFNULL(COUNT(*),0)
          FROM dlg_history_tb dh
          WHERE dh.session_id = csph.session_id
          GROUP BY dh.session_id
        ), 0) as dialog_count
        , IFNULL((
          SELECT
            IFNULL(COUNT(*),0)
          FROM dlg_history_tb dh
          WHERE dh.session_id = csph.session_id
            AND dh.msg_type_cd != ''
          GROUP BY dh.session_id
        ), 0) as unknown_count
      FROM
        chatbot_sds_port_his_tb csph
      JOIN
        chatbot_info_tb ci
        ON ci.bot_id = csph.bot_id
      ${where}
      GROUP BY csph.session_id
      ORDER BY ${orderColumn} ${direction}
      LIMIT ${startIdx}, ${pg_size}
    `;
    // , ADDDATE(csph.open_dtm, INTERVAL -9 HOUR) as open_dtm
    // , ADDDATE(csph.close_dtm, INTERVAL -9 HOUR) as close_dtm

    // app.devlog(loggerName, `
    //   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    //   query: ${sql}
    //   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    // `);

    const connector = Chatbotsdsporthis.app.datasources.mindslab.connector;
    connector.query(sql, null, (err, resultObjects) => {
        if(err) next(err);
        next(null, resultObjects);
    });
  };
  const getWhere = (start_date, end_date, searchOptions)=>{
    const {session_id, bot_name, user_id, domain_count, dialog_count, unknown_count} = JSON.parse(searchOptions);
    const searchOptionList = [];

    searchOptionList.push(`csph.session_id != 'system.0000000000'`);
    
    session_id && searchOptionList.push(`csph.session_id = '${session_id}'`);
    start_date && searchOptionList.push(`csph.open_dtm >= '${moment(start_date).format('YYYY-MM-DD HH:mm:ss')}'`);
    end_date && searchOptionList.push(`csph.close_dtm <= '${moment(end_date).format('YYYY-MM-DD HH:mm:ss')}'`);
    // start_date && searchOptionList.push(`csph.open_dtm >= '${moment(start_date).format()}'`);
    // end_date && searchOptionList.push(`csph.close_dtm <= '${moment(end_date).format()}'`);
    // bot_name && searchOptionList.push(`ci.bot_name like '%${decodeURI(bot_name)}%'`);
    bot_name && searchOptionList.push(`ci.bot_name like '%${bot_name}%'`);
    user_id && searchOptionList.push(`
      (
        SELECT
          DISTINCT(dh.user_id) 
        FROM dlg_history_tb dh
        WHERE dh.session_id = csph.session_id
          AND dh.user_id = ${user_id}
        GROUP BY dh.session_id
      ) IS NOT NULL
    `);
    domain_count && searchOptionList.push(`
      (
        SELECT COUNT(DISTINCT(dh.domain_name))
        FROM dlg_history_tb dh
        WHERE dh.session_id = csph.session_id
        AND dh.domain_name != ''
        GROUP BY dh.session_id
      ) >= ${domain_count}
    `);
    dialog_count && searchOptionList.push(`
      (
        SELECT COUNT(*) 
        FROM dlg_history_tb dh
        WHERE dh.session_id = csph.session_id
        GROUP BY dh.session_id
      ) >= ${dialog_count}
    `);
    unknown_count && searchOptionList.push(`
      (
        SELECT COUNT(*)
        FROM dlg_history_tb dh
        WHERE dh.session_id = csph.session_id
        AND dh.msg_type_cd != ''
        GROUP BY dh.session_id
      ) >= ${unknown_count}
    `);

    const where = searchOptionList.length > 0 ? `WHERE ${searchOptionList.join('\n    AND ')}` : '';
    return where;
  };
  const getOrders = (searchOptions)=>{
    const SET_SEARCH_OPTIONS = {
       session_id   : { key: 'session_id',    orderColumn: 'csph.his_id',   direction: 'desc' }
      ,bot_name     : { key: 'bot_name',      orderColumn: 'ci.bot_name',   direction: 'asc'  }
      ,user_id      : { key: 'user_id',       orderColumn: 'user_id',       direction: 'asc'  }
      ,domain_count : { key: 'domain_count',  orderColumn: 'domain_count',  direction: 'desc' }
      ,dialog_count : { key: 'dialog_count',  orderColumn: 'dialog_count',  direction: 'desc' }
      ,unknown_count: { key: 'unknown_count', orderColumn: 'unknown_count', direction: 'desc' }
      // ,sentence:      { key: 'sentence',      orderColumn: 'csph.sentence',   direction: 'desc' }
      // ,speaker:       { key: 'speaker',       orderColumn: 'csph.speaker',    direction: 'desc' }
    };
    const searchOptionsObj = JSON.parse(searchOptions);
    for (const optionKey in searchOptionsObj) {
      if(SET_SEARCH_OPTIONS[optionKey]
          && SET_SEARCH_OPTIONS[optionKey].key == optionKey
      ) {
        const {orderColumn, direction} = SET_SEARCH_OPTIONS[optionKey];
        return {orderColumn, direction};
      }
    }
    const {orderColumn, direction} = SET_SEARCH_OPTIONS['session_id'];
    return {orderColumn, direction};
  };

  Chatbotsdsporthis.remoteMethod(
    'getSessionCount', {
      http: { path:'/getSessionCount', verb: 'get' },
      accepts: [
        {arg: 'start_date', type: 'date', required: false},
        {arg: 'end_date', type: 'date', required: false},
        {arg: 'searchOptions', type: 'string', required: false}
      ],
      returns: { root: true },
    }
  );
  Chatbotsdsporthis.remoteMethod(
    'getSessionList', {
      http: { path:'/getSessionList', verb: 'get' },
      accepts: [
        {arg: 'pg', type: 'number', required: true, default: 1 },
        {arg: 'pg_size', type: 'number', required: true, default: 10 },
        {arg: 'start_date', type: 'date', required: false},
        {arg: 'end_date', type: 'date', required: false},
        {arg: 'searchOptions', type: 'string', required: false}
      ],
      returns: { root: true },
    }
  );
};
