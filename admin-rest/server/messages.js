'use strict';

let messages = {
  AUTH: {
    INVALID_ACCESS: 'Invalid access.',
    USER_DELETED: 'User deleted.',
    USER_CORRUPTED: "User corrupted.",
    USER_NOT_LOGIN: 'User not login.',
  },
  CONTAINERS: {
    INVALID_CONTAINER: 'Invalid access to file Container.',
  }
}
module.exports = {
  messages
};