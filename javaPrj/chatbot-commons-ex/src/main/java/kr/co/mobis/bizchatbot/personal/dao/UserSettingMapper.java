package kr.co.mobis.bizchatbot.personal.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import kr.co.mobis.bizchatbot.personal.dao.data.UserSettingItem;

@Mapper
public interface UserSettingMapper {

	public List<UserSettingItem> getUserSetting(String userId);
	public long insertUserSetting(String userId);
	public long updateUserSetting(HashMap<String,String> params);
}
