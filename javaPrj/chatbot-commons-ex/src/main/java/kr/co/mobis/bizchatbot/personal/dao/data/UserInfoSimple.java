package kr.co.mobis.bizchatbot.personal.dao.data;

import java.io.Serializable;

public class UserInfoSimple implements Serializable{

	protected String userId;
	protected String userNm;
	protected String userEmail;
	protected String userMobile;
	protected String userTelephone;
	protected String deptCd;
	protected String deptNm;
	protected String userDivCode;
	protected String userDivNm;
	protected String confirmYn;

	
	public UserInfoSimple() {
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserNm() {
		return userNm;
	}

	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getUserTelephone() {
		return userTelephone;
	}

	public void setUserTelephone(String userTelephone) {
		this.userTelephone = userTelephone;
	}

	public String getDeptCd() {
		return deptCd;
	}

	public void setDeptCd(String deptCd) {
		this.deptCd = deptCd;
	}

	public String getDeptNm() {
		return deptNm;
	}

	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}
	
	public String getDisplayInfo() {
		return this.deptNm + " " + this.userNm;
	}

	public String getUserDivCode() {
		return userDivCode;
	}

	public void setUserDivCode(String userDivCode) {
		this.userDivCode = userDivCode;
	}

	public String getUserDivNm() {
		return userDivNm;
	}

	public void setUserDivNm(String userDivNm) {
		this.userDivNm = userDivNm;
	}

	public void setConfirmYn(String confirmYn){ this.confirmYn = confirmYn;};

	public String getConfirmYn() { return this.confirmYn;};

	@Override
	public String toString() {
		return "UserInfoSimple [userId=" + userId + ", userNm=" + userNm + ", userEmail=" + userEmail
				+ ", userMobile=" + userMobile + ", userTelephone=" + userTelephone + ", deptCd=" + deptCd + ", deptNm="
				+ deptNm + ", userDivCode="+ userDivCode + ", userDivNm="+ userDivNm + " confirmYn = " + confirmYn +"]";
	}

}
