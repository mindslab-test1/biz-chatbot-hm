package kr.co.mobis.bizchatbot.personal.dao.data;

import java.io.Serializable;

public class UserSettingItem implements Serializable{

	protected String userId;
	protected String settingType;
	protected String settingValue;
	
	public UserSettingItem() {
		
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSettingType() {
		return settingType;
	}

	public void setSettingType(String settingType) {
		this.settingType = settingType;
	}

	public String getSettingValue() {
		return settingValue;
	}

	public void setSettingValue(String settingValue) {
		this.settingValue = settingValue;
	}

	@Override
	public String toString() {
		return "UserSettingItem [userId=" + userId + ", settingType=" + settingType + ", settingValue=" + settingValue
				+ "]";
	}
}
