package kr.co.mobis.bizchatbot.personal.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import kr.co.mobis.bizchatbot.personal.dao.data.UserComment;

@Mapper
public interface UserCommentMapper {
    public long insert(UserComment uc);
    public List<UserComment> list(HashMap map);
}
