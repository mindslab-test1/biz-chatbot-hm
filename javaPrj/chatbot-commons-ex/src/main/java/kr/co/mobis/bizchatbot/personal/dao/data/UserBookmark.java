package kr.co.mobis.bizchatbot.personal.dao.data;

import java.io.Serializable;

public class UserBookmark implements Serializable{

    protected String userId;
    protected String rObjectId;
    protected String rObjectName;
    protected int    botId;
    protected String createdDtm;
    protected String updatedDtm;
    
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getrObjectId() {
        return rObjectId;
    }
    public void setrObjectId(String rObjectId) {
        this.rObjectId = rObjectId;
    }
    public String getrObjectName() {
        return rObjectName;
    }
    public void setrObjectName(String rObjectName) {
        this.rObjectName = rObjectName;
    }
    public int getBotId() {
        return botId;
    }
    public void setBotId(int botId) {
        this.botId = botId;
    }
    public String getCreatedDtm() {
        return createdDtm;
    }
    public void setCreatedDtm(String createdDtm) {
        this.createdDtm = createdDtm;
    }
    public String getUpdatedDtm() {
        return updatedDtm;
    }
    public void setUpdatedDtm(String updatedDtm) {
        this.updatedDtm = updatedDtm;
    }
    @Override
    public String toString() {
        return "UserBookmark [userId=" + userId + ", rObjectId=" + rObjectId + ", rObjectName=" + rObjectName
                + ", botId=" + botId + ", createdDtm=" + createdDtm + ", updatedDtm=" + updatedDtm + "]";
    }
}
