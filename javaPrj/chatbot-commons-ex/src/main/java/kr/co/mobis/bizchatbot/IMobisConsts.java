package kr.co.mobis.bizchatbot;

public interface IMobisConsts {

	public static final String ALARAM_TYPE = "US";
	//공지사항
	public static final String ALARM_TYPE_NOTICE 	= "US0001";
	//메일
	public static final String ALARM_TYPE_MAIL 		= "US0002";
	//근태
	public static final String ALARM_TYPE_ATTENDANCE = "US0003";
	//결재
	public static final String ALARM_TYPE_APPROVAL 	= "US0004";


	//봇 순서
	public static final String BOT_TYPE = "BK";
//	public static final int BOT_COUNT = 5;
//	public static final String BOT_SEPERATOR = ";";
//	public static final String BOT_VALIDATION = "BK0[1-5]{1}";
	//마이봇
	public static final String BOT_TYPE_MAIBOT 		= "BK59";
	//경영지원
	public static final String BOT_TYPE_MAN_SUPP 	= "BK56";
	//IT HELP
	public static final String BOT_TYPE_IT_HELP 	= "BK58";
	//품질
	public static final String BOT_TYPE_QA 			= "BK57";
	//지능검색
	public static final String BOT_TYPE_SEARCH 		= "BK62";
	//핑퐁
	public static final String BOT_TYPE_PINGPONG 	= "BK99";

	//Cafeteria
	public static final String CAFE_LOC_HQ = "HQ"; //본사 
	public static final String CAFE_LOC_RC = "RC"; //연구소 
	public static final String CAFE_LOC_SS = "SS"; //서산연구소
	public static final String CAFE_LOC_OTHERS = "UNKNOWN";

	public enum CAFE_LOC{
		CAFE_LOC_HQ,
		CAFE_LOC_RC,
		CAFE_LOC_SS,
		CAFE_LOC_OTHERS
	}
}
