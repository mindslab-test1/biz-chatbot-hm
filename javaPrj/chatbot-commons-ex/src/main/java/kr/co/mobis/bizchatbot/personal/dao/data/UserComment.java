package kr.co.mobis.bizchatbot.personal.dao.data;

import java.io.Serializable;

public class UserComment implements Serializable{
    protected int    commentId;
    protected String userId;
    protected int    botId;
    protected String channel;
    protected String title;
    protected String content;
    protected String createdDtm;
    protected String updatedDtm;
    public int getCommentId() {
        return commentId;
    }
    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public int getBotId() {
        return botId;
    }
    public void setBotId(int botId) {
        this.botId = botId;
    }
    public String getChannel() {
        return channel;
    }
    public void setChannel(String channel) {
        this.channel = channel;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getCreatedDtm() {
        return createdDtm;
    }
    public void setCreatedDtm(String createdDtm) {
        this.createdDtm = createdDtm;
    }
    public String getUpdatedDtm() {
        return updatedDtm;
    }
    public void setUpdatedDtm(String updatedDtm) {
        this.updatedDtm = updatedDtm;
    }
    @Override
    public String toString() {
        return "UserComment [commentId=" + commentId + ", userId=" + userId + ", botId=" + botId + ", channel="
                + channel + ", title=" + title + ", content=" + content + ", createdDtm=" + createdDtm + ", updatedDtm="
                + updatedDtm + "]";
    }
    
    
}
