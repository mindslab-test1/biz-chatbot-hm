package kr.co.mobis.bizchatbot.svc.cafeteria.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CafeteriaMenuInfo implements Serializable{

	protected String location;
	//date YYYY-MM-DD
	protected String menuDate;
	protected String menuImage;
	
	public CafeteriaMenuInfo() {
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMenuDate() {
		return menuDate;
	}

	public void setMenuDate(String menuDate) {
		this.menuDate = menuDate;
	}

	public String getMenuImage() {
		return menuImage;
	}

	public void setMenuImage(String menuImage) {
		this.menuImage = menuImage;
	}

	@Override
	public String toString() {
		return "CafeteriaMenuInfo [location=" + location + ", menuDate=" + menuDate + ", menuImage=" + menuImage + "]";
	}
}
