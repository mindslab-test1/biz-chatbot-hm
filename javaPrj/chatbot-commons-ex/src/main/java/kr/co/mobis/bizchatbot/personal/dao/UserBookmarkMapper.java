package kr.co.mobis.bizchatbot.personal.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import kr.co.mobis.bizchatbot.personal.dao.data.UserBookmark;

@Mapper
public interface UserBookmarkMapper {
    public long insert(UserBookmark ub);
    public long delete(UserBookmark ub);
    public List<UserBookmark> list(HashMap map);
}
