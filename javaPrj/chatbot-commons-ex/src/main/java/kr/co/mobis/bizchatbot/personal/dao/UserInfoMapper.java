package kr.co.mobis.bizchatbot.personal.dao;

import org.apache.ibatis.annotations.Mapper;

import kr.co.mobis.bizchatbot.personal.dao.data.UserInfoSimple;

@Mapper
public interface UserInfoMapper {

	public UserInfoSimple getUserById(String userId);
}
