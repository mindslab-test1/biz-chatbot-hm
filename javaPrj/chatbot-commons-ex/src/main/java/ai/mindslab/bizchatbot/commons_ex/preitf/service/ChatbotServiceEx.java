package ai.mindslab.bizchatbot.commons_ex.preitf.service;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.GlobalItfMapper;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfData;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfSdsData;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfSimpleData;
import ai.mindslab.bizchatbot.commons_ex.preitf.dao.PreItfMapper;
import ai.mindslab.bizchatbot.commons_ex.preitf.dao.data.PreItfData;


@Service
public class ChatbotServiceEx implements IChatbotConst{

	private Logger logger = LoggerFactory.getLogger(ChatbotServiceEx.class);
	
	
	@Autowired
	private PreItfMapper preItf;
	
	@Autowired
	private GlobalItfMapper gItf;
	
	@Autowired
	private Environment env;
	
	private HashMap<String, Pattern> pattern = new HashMap<>();
	
	@Transactional
	public List<PreItfData> getPreItfList(int botId) throws Exception{
		logger.info("getPreItfList/{}/START", botId);
		
		List<PreItfData> list = null;
		
		try {
			list = preItf.getPreItf(botId);
			
		}catch(Exception e) {
			logger.warn("getPreItfList/{}",e.getMessage());
			throw e;
		}
		logger.info("getPreItfList/END");
		return list;
	}
	
	public Pattern getPattern(String regex) throws PatternSyntaxException{
		
		Pattern p = pattern.get(regex);
		if(p == null) {
			p = Pattern.compile(regex);
			pattern.put(regex, p);
		}
		return p;
	}
	
	public boolean doMatch(String regex, String utter) throws Exception{
//		logger.info("doMatch/{},{}",regex, utter);
		Matcher matcher = getPattern(regex).matcher(utter);
//		boolean bMatch = matcher.find();
		boolean bMatch = matcher.matches();
		logger.debug("doMatch/{},{}/result:{}", regex, utter, bMatch);
		return bMatch;
	}
	
	@Transactional
	public GlobalItfData getGlobalItfData(int botId) throws Exception{
		//QQQ cache.
		logger.info("getGlobalItfData/START/{}", botId);
		GlobalItfData gData = null;
		try {
			gData = gItf.getItfInfo(botId);
			if(gData != null) {
				List<GlobalItfSimpleData> simples = gItf.getItfSimpleList(botId);
				List<GlobalItfSdsData> sdsData = gItf.getItfSdsList(botId);
				gData.setSimpleList(simples);
				gData.setSdsList(sdsData);
			}
		}catch(Exception e) {
			logger.warn("getGlobalItfData/{}", e.getMessage());
			throw e;
		}
		return gData;
	}
	
}
