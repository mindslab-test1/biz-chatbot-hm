package ai.mindslab.bizchatbot.commons_ex.gitf.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfData;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfSdsData;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfSimpleData;

@Mapper
public interface GlobalItfMapper {

	public GlobalItfData getItfInfo(int botId);
	public List<GlobalItfSdsData> getItfSdsList(int botId);
	public List<GlobalItfSimpleData> getItfSimpleList(int botId);
}
