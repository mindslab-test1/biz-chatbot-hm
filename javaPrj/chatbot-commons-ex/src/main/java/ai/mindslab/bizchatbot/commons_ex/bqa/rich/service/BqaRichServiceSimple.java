package ai.mindslab.bizchatbot.commons_ex.bqa.rich.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotApiMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfoDisp;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiJsonTemplate;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiParam;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseListObject;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao.QaRichMapper;
import ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao.data.QaRichVo;

@Service
public class BqaRichServiceSimple {

	private Logger logger = LoggerFactory.getLogger(BqaRichServiceSimple.class);
	
	@Autowired
	private QaRichMapper richMapper;
	
	@Autowired
	protected ChatbotApiMapper chatbotMapper;
	
	public BqaRichServiceSimple() {
	}

	@Transactional(rollbackFor = { Exception.class })
	public QaRichVo getBqaRichInfo(String respCode) throws BizChatbotException{
		logger.info("getRespInfo/{}",respCode);

		QaRichVo qaRichVo = null;
		try {
			qaRichVo = richMapper.getQaRich(respCode);
		}catch(Exception e) {
			logger.warn("getRespInfo/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return qaRichVo;
	}
	
	@Transactional(rollbackFor = { Exception.class })
	public ChatbotApiInfo getBqaApiInfo(String respCode) throws BizChatbotException{
		logger.info("getBqaApiInfo/{},{}", respCode);
		
		ChatbotApiInfo apiInfo = null;
		QaRichVo qaRichVo = null;
		try {
			qaRichVo = richMapper.getQaRich(respCode);
			if(qaRichVo != null) {
				String type = qaRichVo.getContentTypeCd();
				if(IChatbotConst.RICH_CONTENT_TYPE_API.equals(type)) {
					apiInfo = chatbotMapper.getApiInfo(qaRichVo.getRcId());
					if(apiInfo != null) {
						List<ChatbotApiParam> pars = chatbotMapper.getParams(qaRichVo.getRcId());
						apiInfo.getParamList().addAll(pars);
						ChatbotApiJsonTemplate tpl = chatbotMapper.getJsonTemplate(qaRichVo.getRcId());
						apiInfo.setJsonTemplate(tpl);
					}
				}
			}
		}catch(Exception e) {
			logger.warn("getBqaApiInfo/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return apiInfo;
	}

	@Transactional(rollbackFor = { Exception.class })
	public BaseListObject<ChatbotApiInfoDisp> getBqaApiList(int workspaceId) throws BizChatbotException{
		logger.info("getBqaApiList/{}",workspaceId);
		
		BaseListObject<ChatbotApiInfoDisp> out = new BaseListObject<>();
		List<ChatbotApiInfoDisp> list = null;
		long tot = 0;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("workspaceId", workspaceId);
			//all data
			list = chatbotMapper.getApiList(params);
			tot = chatbotMapper.getApiListCount(params);
			out.setTotalCount(tot);
			out.setList(list);
		}catch(Exception e) {
			logger.warn("getBqaApiList/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return out;
	}
}
