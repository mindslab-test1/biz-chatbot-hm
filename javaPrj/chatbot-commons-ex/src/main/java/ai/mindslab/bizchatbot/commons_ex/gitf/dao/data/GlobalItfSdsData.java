package ai.mindslab.bizchatbot.commons_ex.gitf.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class GlobalItfSdsData implements Serializable{

	protected int gitfId;
	protected String intentName;
	protected String svcTypeCd;
	protected int ifId;
	
	public GlobalItfSdsData() {
	}

	public int getGitfId() {
		return gitfId;
	}

	public void setGitfId(int gitfId) {
		this.gitfId = gitfId;
	}

	public String getIntentName() {
		return intentName;
	}

	public void setIntentName(String intentName) {
		this.intentName = intentName;
	}

	public String getSvcTypeCd() {
		return svcTypeCd;
	}

	public void setSvcTypeCd(String svcTypeCd) {
		this.svcTypeCd = svcTypeCd;
	}

	public int getIfId() {
		return ifId;
	}

	public void setIfId(int ifId) {
		this.ifId = ifId;
	}

	@Override
	public String toString() {
		return "GlobalItfSdsData [gitfId=" + gitfId + ", intentName=" + intentName + ", svcTypeCd=" + svcTypeCd
				+ ", ifId=" + ifId + "]";
	}
}
