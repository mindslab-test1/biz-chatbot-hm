package ai.mindslab.bizchatbot.commons_ex.preitf.dao;

import java.util.List;

import ai.mindslab.bizchatbot.commons_ex.preitf.dao.data.PreItfData;

public interface PreItfMapper {

	List<PreItfData> getPreItf(int botId);
}
