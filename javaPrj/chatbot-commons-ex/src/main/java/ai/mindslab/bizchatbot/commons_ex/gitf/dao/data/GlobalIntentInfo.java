package ai.mindslab.bizchatbot.commons_ex.gitf.dao.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class GlobalIntentInfo implements Serializable{

	protected String intentName;
	protected String slotName;
	protected String slotValue;
	protected String svcTypeCd;
	protected int ifId;
	protected List<Slot> slots;
	
	public GlobalIntentInfo() {
	}

	public String getIntentName() {
		return intentName;
	}

	public void setIntentName(String intentName) {
		this.intentName = intentName;
	}

	public String getSlotName() {
		return slotName;
	}

	public void setSlotName(String slotName) {
		this.slotName = slotName;
	}

	public String getSlotValue() {
		return slotValue;
	}

	public void setSlotValue(String slotValue) {
		this.slotValue = slotValue;
	}

	public String getSvcTypeCd() {
		return svcTypeCd;
	}

	public void setSvcTypeCd(String svcTypeCd) {
		this.svcTypeCd = svcTypeCd;
	}

	public int getIfId() {
		return ifId;
	}

	public void setIfId(int ifId) {
		this.ifId = ifId;
	}

	public List<Slot> getSlots() {
		if(slots == null) {
			slots = new ArrayList<>();
		}
		return slots;
	}

	public void setSlots(List<Slot> slots) {
		this.slots = slots;
	}

	@Override
	public String toString() {
		return "GlobalIntentInfo [intentName=" + intentName + ", slotName=" + slotName + ", slotValue=" + slotValue
				+ ", svcTypeCd=" + svcTypeCd + ", ifId=" + ifId + "]";
	}
	
	public static class Slot implements Serializable{
		protected String slotName;
		protected String slotValue;
		public Slot() {}
		public Slot(String slotName, String slotValue) {
			this.slotName = slotName;
			this.slotValue = slotValue;
		}
		public String getSlotName() {
			return slotName;
		}
		public void setSlotName(String slotName) {
			this.slotName = slotName;
		}
		public String getSlotValue() {
			return slotValue;
		}
		public void setSlotValue(String slotValue) {
			this.slotValue = slotValue;
		}
		@Override
		public String toString() {
			return "Slot [slotName=" + slotName + ", slotValue=" + slotValue + "]";
		}
	}
}
