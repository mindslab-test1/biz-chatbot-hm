package ai.mindslab.bizchatbot.commons_ex.preitf.dao.data;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties({"createdDtm", "updatedDtm", "creatorId", "updatorId"})
public class PreItfData implements Serializable{

	protected int preitfId;
	protected int botId;
	protected String pattern;
	protected int itfOrder;
	
	protected Date createdDtm;
	protected Date updatedDtm;
	protected String creatorId;
	protected String updatorId;
	
	public PreItfData() {
		
	}

	public int getPreitfId() {
		return preitfId;
	}

	public void setPreitfId(int preitfId) {
		this.preitfId = preitfId;
	}

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public int getItfOrder() {
		return itfOrder;
	}

	public void setItfOrder(int itfOrder) {
		this.itfOrder = itfOrder;
	}

	public Date getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(Date createdDtm) {
		this.createdDtm = createdDtm;
	}

	public Date getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(Date updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	};
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
