package ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao;


import ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao.data.QaRichVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;


@Mapper
public interface QaRichMapper {

	public QaRichVo getQaRich(String respCode);

	public int insertQaRich(QaRichVo data);

	public int deleteQaRich(QaRichVo data);

	public int updateQaRich(QaRichVo data);

	public int updateQaRichRespCode(Map<String, Object> paramMap);

}
