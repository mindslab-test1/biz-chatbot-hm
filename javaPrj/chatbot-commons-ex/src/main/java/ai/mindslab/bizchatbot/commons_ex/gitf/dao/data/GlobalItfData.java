package ai.mindslab.bizchatbot.commons_ex.gitf.dao.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class GlobalItfData implements Serializable{

	protected int botId;
	protected String projectName;
	protected List<GlobalItfSimpleData> simpleList;
	protected List<GlobalItfSdsData> sdsList;
	
	public GlobalItfData() {
	}

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public List<GlobalItfSimpleData> getSimpleList() {
		if(simpleList == null) {
			simpleList = new ArrayList<GlobalItfSimpleData>();
		}
		return simpleList;
	}

	public void setSimpleList(List<GlobalItfSimpleData> simpleList) {
		this.simpleList = simpleList;
	}

	public List<GlobalItfSdsData> getSdsList() {
		if(sdsList == null) {
			sdsList = new ArrayList<GlobalItfSdsData>();
		}
		return sdsList;
	}

	public void setSdsList(List<GlobalItfSdsData> sdsList) {
		this.sdsList = sdsList;
	}

	@Override
	public String toString() {
		return "GlobalItfData [botId=" + botId + ", projectName=" + projectName + "]";
	}

}
