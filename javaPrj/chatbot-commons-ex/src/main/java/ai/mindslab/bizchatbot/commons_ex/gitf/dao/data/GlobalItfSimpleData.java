package ai.mindslab.bizchatbot.commons_ex.gitf.dao.data;

import java.io.Serializable;

public class GlobalItfSimpleData implements Serializable{

	protected int gitfId;
	protected String pattern;
	protected String svcTypeCd;
	protected int itfOrder;
	protected int ifId;
	
	public GlobalItfSimpleData() {
	}

	public int getGitfId() {
		return gitfId;
	}

	public void setGitfId(int gitfId) {
		this.gitfId = gitfId;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getSvcTypeCd() {
		return svcTypeCd;
	}

	public void setSvcTypeCd(String svcTypeCd) {
		this.svcTypeCd = svcTypeCd;
	}

	public int getItfOrder() {
		return itfOrder;
	}

	public void setItfOrder(int itfOrder) {
		this.itfOrder = itfOrder;
	}

	public int getIfId() {
		return ifId;
	}

	public void setIfId(int ifId) {
		this.ifId = ifId;
	}

	@Override
	public String toString() {
		return "GlobalItfSimpleData [gitfId=" + gitfId + ", pattern=" + pattern + ", svcTypeCd=" + svcTypeCd
				+ ", itfOrder=" + itfOrder + ", ifId=" + ifId + "]";
	}
}
