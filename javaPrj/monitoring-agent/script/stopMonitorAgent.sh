#!/usr/bin/bash
export P_NM="admin-monitoring-$(hostname)"
echo "profile ${CHATBOT_PROFILE}"

count=`ps -ef|grep ${P_NM} |grep -v grep| awk '{print $2}'|wc -l`

if [ $count -gt 0 ]; then
	pid=`ps -ef|grep ${P_NM}|grep -v grep|awk '{print $2}'`
	echo "shutting down ${P_NM} process...${pid}"
	kill -9 ${pid}
else
	echo "${P_NM} is not running...";
	exit 0;
fi
