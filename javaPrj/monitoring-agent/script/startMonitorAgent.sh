#!/usr/bin/bash

export P_NM="admin-monitoring-$(hostname)"

export SERVER_NAME="server_35"

echo "profile ${CHATBOT_PROFILE}"

count=`ps -ef|grep ${P_NM} |grep -v grep| awk '{print $2}'|wc -l`

if [ $count -gt 0 ]; then
	echo "${P_NM} Process is already running...";
else
 	nohup java -jar ${CHATBOT_ROOT}/lib/monitoring-agent.jar --spring.profiles.active=${CHATBOT_PROFILE} -Dp.name=${P_NM} --server.name=${SERVER_NAME} 1>/dev/null 2>&1 & 
fi
