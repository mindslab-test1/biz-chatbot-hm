package kr.co.mobis.chatbot.agent.scheduler.service;

import kr.co.mobis.chatbot.agent.config.AgentConfig;
import kr.co.mobis.chatbot.agent.scheduler.dao.MonitoringMapper;
import kr.co.mobis.chatbot.agent.scheduler.dao.data.MonitorSVO;
import kr.co.mobis.chatbot.agent.scheduler.dao.data.MonitorVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MonitorService {

    @Autowired
    MonitoringMapper mapper;


    @Transactional(transactionManager= AgentConfig.TRANSACTION_MANAGER_MAIN)
    public int insertServerName(MonitorSVO svo) throws Exception{
        int result= mapper.insertServerName(svo);
        return result;
    }


    @Transactional(transactionManager= AgentConfig.TRANSACTION_MANAGER_MAIN)
    public int insetMonitorStatus(MonitorVO svo) throws Exception{
        int result = mapper.insetMonitorStatus(svo);
        return result;
    }


}
