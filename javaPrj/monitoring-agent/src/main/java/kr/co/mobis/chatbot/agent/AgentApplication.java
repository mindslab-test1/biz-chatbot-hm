package kr.co.mobis.chatbot.agent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
//@ComponentScan(basePackages={"kr.co.mobis.chatbot.agent"})
@EnableAsync
public class AgentApplication {

    private static Logger logger = LoggerFactory.getLogger(AgentApplication.class);

    public static void main(String[] args) {
        logger.info("==============AgentApplication Process started.==============");
        SpringApplication.run(AgentApplication.class, args);
    }
}