package kr.co.mobis.chatbot.agent.scheduler.dao;

import kr.co.mobis.chatbot.agent.scheduler.dao.data.MonitorSVO;
import kr.co.mobis.chatbot.agent.scheduler.dao.data.MonitorVO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface  MonitoringMapper {
    public int insertServerName(MonitorSVO svo) throws Exception;
    public int insetMonitorStatus(MonitorVO vo) throws Exception;
}
