package kr.co.mobis.chatbot.agent.scheduler;


import com.sun.management.OperatingSystemMXBean;
import kr.co.mobis.chatbot.agent.scheduler.dao.data.MonitorSVO;
import kr.co.mobis.chatbot.agent.scheduler.dao.data.MonitorVO;
import kr.co.mobis.chatbot.agent.scheduler.service.MonitorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.nio.file.FileStore;
import java.nio.file.FileSystems;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

@Component
public class MonitoringTask {

    private static Logger logger = LoggerFactory.getLogger(MonitoringTask.class);

    @Value("${server.name}")
    String serverName;

    @Autowired
    MonitorService service;

    SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd hh:mm:00");
    @PostConstruct
    public void initRun() {
        runOnce();
    }

    @Scheduled(cron = "0 0/5 * * * *")
//	@Scheduled(fixedRate=5000)// test
    public void userLocking() {
        logger.info("start gather system monitoring");
        monitoringServerStatus();
        logger.info("End gather system monitoring");

    }

    public void runOnce() {
        logger.info("Start gather system monitoring");

        dt.setTimeZone(TimeZone.getTimeZone("UTC"));

        int result = 0;
        try {
            InetAddress  local = InetAddress.getLocalHost();
            String ip = local.getHostAddress();

            MonitorSVO svo = new MonitorSVO();
            svo.setServerName(this.serverName);
            svo.setServerIp(ip);
            result = service.insertServerName(svo);

            if (result > 0) {
                logger.info("sucess monitoring : " + svo);
            }

            logger.info("End gather system monitoring");
        }catch (Exception e){
            logger.warn("Error insertServerName {}" + this.serverName);
        }
    }


    /**
     * CPU, MEM, DISK 사용량&사용률 구하기(by system)
     */
    public void monitoringServerStatus(){

        OperatingSystemMXBean osBean =
                (com.sun.management.OperatingSystemMXBean)ManagementFactory.getOperatingSystemMXBean();

        dt.setTimeZone(TimeZone.getTimeZone("UTC"));

        double cpuLoad = 0;
        double memTotal = 0;
        double memUsage = 0;
        double memUsed = 0;
        double diskTotal = 0;
        double diskUsage = 0;
        double diskUsed = 0;

        BigDecimal per = null;

        try {
            InetAddress  local = InetAddress.getLocalHost();
            String ip = local.getHostAddress();


            while (true) {
                cpuLoad = osBean.getSystemCpuLoad();
                //Nan 이 나올때도 있어서...
                if ( Double.isNaN(cpuLoad) || cpuLoad <= 0.0) {
                    continue;
                } else {
                    cpuLoad = this.getDoubleValue(cpuLoad);
                    break;
                }
            }

            memTotal = this.toMB(osBean.getTotalPhysicalMemorySize());
            memUsage = this.toMB(osBean.getTotalPhysicalMemorySize() - osBean.getFreePhysicalMemorySize());
            memUsed = (memUsage / memTotal);

            memUsed = this.getDoubleValue(memUsed);

            try {

                for (FileStore store : FileSystems.getDefault().getFileStores()) {

                    //vm 이 붙은건 버추얼...이라서 ..제외.
                    if(store.toString().indexOf("vm") == -1) {
                        diskTotal += toMB(store.getTotalSpace()) ;
                    }
                    diskUsage += toMB(store.getTotalSpace() - store.getUnallocatedSpace());
                }

                diskUsed = (diskUsage / diskTotal);
                diskUsed = this.getDoubleValue(diskUsed);

            }catch(Exception e) {
                logger.error("error disk Gathered : " + e.getMessage());
            }


            MonitorVO vo = new MonitorVO();

            vo.setServerIp(ip);
            vo.setCpuUsed(cpuLoad);
            vo.setMemTotal(memTotal);
            vo.setMemUsage(memUsage);
            vo.setMemUsed(memUsed);
            vo.setDiskTotal(diskTotal);
            vo.setDiskUsage(diskUsage);
            vo.setDiskUsed(diskUsed);

            int result = 0;

            result = service.insetMonitorStatus(vo);

            if(result > 0){
                logger.info("sucess monitoring : " + vo);
            }



        }catch (Exception e){
            e.printStackTrace();
        }

    }



    private double toMB(long size) {
        double value = size/(1024*1024);
        return new BigDecimal(value).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }


    /* 소수점 2째자리 맞추기 위한 메서드 */
    public double getDoubleValue(double value){
       BigDecimal per = new BigDecimal(value* 100).setScale(2, BigDecimal.ROUND_HALF_UP);
       return per.doubleValue();
    }

}
