package kr.co.mobis.chatbot.agent.scheduler.dao.data;

import java.io.Serializable;

public class MonitorSVO implements Serializable {
    private Long id ;
    private String serverName;
    private String serverIp;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }
}
