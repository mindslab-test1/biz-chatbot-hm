package kr.co.mobis.chatbot.agent.scheduler.dao.data;


import java.io.Serializable;

public class MonitorVO implements Serializable {

    private String serverIp;
    private String createdTime;
    private Double cpuUsed;
    private Double memTotal;
    private Double memUsage;
    private Double memUsed;
    private Double diskTotal;
    private Double diskUsage;
    private Double diskUsed;



    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public Double getCpuUsed() {
        return cpuUsed;
    }

    public void setCpuUsed(Double cpuUsed) {
        this.cpuUsed = cpuUsed;
    }

    public Double getMemTotal() {
        return memTotal;
    }

    public void setMemTotal(Double memTotal) {
        this.memTotal = memTotal;
    }

    public Double getMemUsage() {
        return memUsage;
    }

    public void setMemUsage(Double memUsage) {
        this.memUsage = memUsage;
    }

    public Double getMemUsed() {
        return memUsed;
    }

    public void setMemUsed(Double memUsed) {
        this.memUsed = memUsed;
    }

    public Double getDiskTotal() {
        return diskTotal;
    }

    public void setDiskTotal(Double diskTotal) {
        this.diskTotal = diskTotal;
    }

    public Double getDiskUsage() {
        return diskUsage;
    }

    public void setDiskUsage(Double diskUsage) {
        this.diskUsage = diskUsage;
    }

    public Double getDiskUsed() {
        return diskUsed;
    }

    public void setDiskUsed(Double diskUsed) {
        this.diskUsed = diskUsed;
    }



    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("server: " +    this.getServerIp() +",  \n");
        sb.append("cpuUsed: " +   this.getCpuUsed() +", \n");
        sb.append("memTotal: " +  this.getMemTotal() +",  \n");
        sb.append("memUsage: " +  this.getMemUsage() +", \n");
        sb.append("memUsed: " +   this.getMemUsed() +", \n");
        sb.append("diskTotal: " + this.getDiskTotal() +",  \n");
        sb.append("diskUsage: " + this.getDiskUsage() +", \n");
        sb.append("diskUsed: " +  this.getDiskUsed() +", \n");

        return sb.toString();
    }

}
