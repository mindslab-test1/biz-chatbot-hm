package ai.mindslab.bizchatbot.bqa.dao.data;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties({ "INDEX_STATUS_SUCCESS","INDEX_STATUS_FAIL","INDEX_STATUS_ERROR","ALL_TARGET_DOMAIN","INDEX_TYPE_FULL","INDEX_TYPE_ADD","INDEX_TYPE_REMOVE","INDEX_TYPE_DOMAIN" })
public class QaIndexingHistoryVo implements Serializable {

	@Override
	public String toString() {
		return "QaIndexingHistoryVo [domainId=" + domainId + ", total=" + total + ", msg=" + msg + ", status=" + status
				+ ", createdDtm=" + createdDtm + ", creatorId=" + creatorId + ", committedDtm=" + committedDtm
				+ ", takenTime=" + takenTime + ", indexingStartedDtm=" + indexingStartedDtm + "]";
	}

	public final char INDEX_STATUS_SUCCESS = 'S';
	public final char INDEX_STATUS_FAIL = 'F'; // data wrong
	public final char INDEX_STATUS_ERROR = 'E'; // system error

	public final int ALL_TARGET_DOMAIN = 0;

	public final char INDEX_TYPE_FULL = 'F';
	public final char INDEX_TYPE_ADD = 'A';
	public final char INDEX_TYPE_DOMAIN = 'D';
	public final char INDEX_TYPE_REMOVE = 'R';

	private int historyId;
	private char type;
	private int domainId;
	private long total;
	private String msg;
	private char status;
	private LocalDateTime createdDtm;
	private String creatorId;
	private LocalDateTime committedDtm;
	private String takenTime;
	private LocalDateTime indexingStartedDtm;

	public LocalDateTime getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(LocalDateTime createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public int getHistoryId() {
		return historyId;
	}

	public void setHistoryId(int historyId) {
		this.historyId = historyId;
	}

	public int getDomainId() {
		return domainId;
	}

	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public char getType() {
		return type;
	}

	public void setType(char type) {
		this.type = type;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public LocalDateTime getCommittedDtm() {
		return committedDtm;
	}

	public void setCommittedDtm(LocalDateTime committedDtm) {
		this.committedDtm = committedDtm;
	}

	public String getTakenTime() {
		return takenTime;
	}

	public void setTakenTime(String takenTime) {
		this.takenTime = takenTime;
	}

	public LocalDateTime getIndexingStartedDtm() {
		return indexingStartedDtm;
	}

	public void setIndexingStartedDtm(LocalDateTime indexingStartedDtm) {
		this.indexingStartedDtm = indexingStartedDtm;
	}

	
	
}
