package ai.mindslab.bizchatbot.bqa.service;

import ai.mindslab.bizchatbot.bqa.dao.*;
import ai.mindslab.bizchatbot.bqa.dao.data.*;
import ai.mindslab.bizchatbot.bqa.nlp.NlpAnalyzeClient;
import ai.mindslab.bizchatbot.bqa.util.ExcelUtils;
import ai.mindslab.bizchatbot.commons.codes.IBasicQAConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseListObject;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.utils.PagingHelper;
import ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao.QaRichMapper;
import ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao.data.QaRichVo;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.common.LangOuterClass.LangCode;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * @author park Chatbot Admin Data Management
 */
@Service
public class BqaManagementService implements IRestCodes,IBasicQAConst {

	private Logger logger = LoggerFactory.getLogger(BqaManagementService.class);
	public final String USE_YN_Y = "Y";
	public final String USE_YN_N = "N";

	@Autowired
	private QaQuestionMapper questionMapper;

	@Autowired
	private QaAnswerMapper answerMapper;

	@Autowired
	private QaExcelMapper excelMapper;

	@Autowired
	private QaIndexMapper indexMapper;

	@Autowired
	private QaQnaParaphraseMapper qnaParaphrase;

	@Autowired
	private NlpAnalyzeClient nlpAnalyzeClient;

	@Autowired
	private PagingHelper pgHelper;

	@Autowired
	private QaRichMapper richMapper;

	@Value("${maum.chunk.query.count :100}")
	private int queryCount;

/*	public BaseListObject<QaQuestionVo> getsQuestion(int pg, int pgSize, String search, int type, int... domainIds) throws BizChatbotException {
		logger.info("Service getsQuestion parameter pg{},domainIds {}, search{}, type{}", pg, domainIds,search,type);

		List<QaQuestionVo> list = null;
		int totalCount = 0;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("domainIds", domainIds);
			params.put("useYn", USE_YN_Y);
			params.put("search", search);
			params.put("type", type);

			if (domainIds != null && domainIds[0] != 0) {
				params.put("domain", 1);
			} else {
				params.put("domain", 0);
			}

			totalCount = questionMapper.getCountByDomain(params);
			if (totalCount > 0) {

				pgHelper.setPageInfo(params, pg, pgSize);
				list = questionMapper.gets(params);

				for (int i = 0; i < list.size(); i++) {

					QaQuestionVo questionVo = list.get(i);
					int count = answerMapper.getCountByQuestionId(questionVo.getQuestionId());
					if (count > 0) {

						List<QaAnswerVo> answerList = answerMapper.getsByQuestionId(questionVo.getQuestionId());

						for (QaAnswerVo qaAnswerVo : answerList) {
							if(qaAnswerVo.getRespCode() != null) {
								QaRichVo qaRichVo = richMapper.getQaRich(qaAnswerVo.getRespCode());
								qaAnswerVo.setQaRichVo(qaRichVo);
							}
						}

						questionVo.setAnswerList(answerList);
					}
				}
			}
		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_SELECT_ERROR_Q, e);
			throw new BizChatbotException(ERR_CODE_MGMT_SELECT_ERROR_Q, ERR_MSG_MGMT_SELECT_ERROR_Q);
		}

		BaseListObject<QaQuestionVo> obj = new BaseListObject<QaQuestionVo>(totalCount, list);

		return obj;
	}*/

    public BaseListObject<QaBqaVo> getsQuestion(int pg, int pgSize, String search, int type, int... domainIds) throws BizChatbotException {
        logger.info("Service getsQuestion parameter pg{},domainIds {}, search{}, type{}", pg, domainIds,search,type);

        List<QaBqaVo> list =  new ArrayList<>();
        int totalCount = 0;
        try {
            HashMap<String, Object> params = new HashMap<>();
            params.put("domainIds", domainIds);
            params.put("useYn", USE_YN_Y);
            params.put("search", search);
            params.put("type", type);

            if (domainIds != null && domainIds[0] != 0) {
                params.put("domain", 1);
            } else {
                params.put("domain", 0);
            }

            totalCount = answerMapper.getCountByDomain(params);
            if (totalCount > 0) {

                pgHelper.setPageInfo(params, pg, pgSize);
                List<QaAnswerVo> answerList = answerMapper.gets(params);

               for (int i = 0; i < answerList.size(); i++) {
                   QaAnswerVo answerVo = answerList.get(i);
                   QaBqaVo qaBqaVo = new QaBqaVo();

                   if(answerVo.getRespCode() != null) {
                       QaRichVo qaRichVo = richMapper.getQaRich(answerVo.getRespCode());
                       answerVo.setQaRichVo(qaRichVo);
                   }

                   qaBqaVo.setAnswerGroup(answerVo);

                   params.clear();
                   params.put("answerId", answerVo.getAnswerId());

                   List<QaQuestionsVo> questionList = questionMapper.getQuestionByAnswer(params);

                   qaBqaVo.setQuestionList(questionList);

                   list.add(qaBqaVo);
                }

               /* for (int i = 0; i < list.size(); i++) {

                    QaQuestionVo questionVo = list.get(i);
                    int count = answerMapper.getCountByQuestionId(questionVo.getQuestionId());
                    if (count > 0) {

                        List<QaAnswerVo> answerList = answerMapper.getsByQuestionId(questionVo.getQuestionId());

                        for (QaAnswerVo qaAnswerVo : answerList) {
                            if(qaAnswerVo.getRespCode() != null) {
                                QaRichVo qaRichVo = richMapper.getQaRich(qaAnswerVo.getRespCode());
                                qaAnswerVo.setQaRichVo(qaRichVo);
                            }
                        }

                        questionVo.setAnswerList(answerList);
                    }
                }*/
            }
        } catch (Exception e) {
            logger.warn(ERR_MSG_MGMT_SELECT_ERROR_Q, e);
            throw new BizChatbotException(ERR_CODE_MGMT_SELECT_ERROR_Q, ERR_MSG_MGMT_SELECT_ERROR_Q);
        }

        BaseListObject<QaBqaVo> obj = new BaseListObject<>(totalCount, list);

        return obj;
    }

/*	public BaseListObject<QaQuestionsVo> getsQuestion(int pg, int pgSize, String search, int type, int... domainIds) throws BizChatbotException {
		logger.info("Service getsQuestion parameter pg{},domainIds {}, search{}, type{}", pg, domainIds,search,type);

		List<QaQuestionsVo> list = null;
		int totalCount = 0;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("domainIds", domainIds);
			params.put("useYn", USE_YN_Y);
			params.put("search", search);
			params.put("type", type);

			if (domainIds != null && domainIds[0] != 0) {
				params.put("domain", 1);
			} else {
				params.put("domain", 0);
			}

			totalCount = questionMapper.getCountByDomain(params);
			if (totalCount > 0) {

				pgHelper.setPageInfo(params, pg, pgSize);
				list = questionMapper.gets(params);

				for (int i = 0; i < list.size(); i++) {

					QaQuestionsVo questionVo = list.get(i);
					int count = answerMapper.getCountByQuestionId(questionVo.getQuestionId());
					if (count > 0) {

						List<QaAnswerVo> answerList = answerMapper.getsByQuestionId(questionVo.getQuestionId());

						for (QaAnswerVo qaAnswerVo : answerList) {
							if(qaAnswerVo.getRespCode() != null) {
								QaRichVo qaRichVo = richMapper.getQaRich(qaAnswerVo.getRespCode());
								qaAnswerVo.setQaRichVo(qaRichVo);
							}
						}

					//	questionVo.setAnswerList(answerList);
					}
				}
			}
		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_SELECT_ERROR_Q, e);
			throw new BizChatbotException(ERR_CODE_MGMT_SELECT_ERROR_Q, ERR_MSG_MGMT_SELECT_ERROR_Q);
		}

		BaseListObject<QaQuestionsVo> obj = new BaseListObject<QaQuestionsVo>(totalCount, list);

		return obj;
	}*/

	public BaseListObject<NlpAnalysisVo> nlpTest(String text) throws BizChatbotException {
		logger.info("Service nlpTest parameter text {}", text);

		int totalCount = 0;
		List<NlpAnalysisVo> list;
		try {

			list = nlpAnalyzeClient.nlpTestAnalyze(text);
			totalCount = list.size();
		} catch (Exception e) {
			logger.warn("nlpTest Error/{}", e);
			throw new BizChatbotException(ERR_CODE_NLP_ANALYSIS_ERROR, ERR_MSG_NLP_ANALYSIS_ERROR);
		}

		BaseListObject<NlpAnalysisVo> obj = new BaseListObject<NlpAnalysisVo>(totalCount, list);

		return obj;
	}

/*	@Transactional(rollbackFor = { Exception.class })
	public int createQna(QaQuestionVo q) throws BizChatbotException {
		logger.info("Service createQna parameter {}", q);

		int count = 0;
		try {
			List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
			q.setQuestionId(0);
			q.setUseYn(USE_YN_Y);
			q.setMainYn("Y");

			int resultCount = questionMapper.update(q);
			count += resultCount;
			if (resultCount == 0 && q.getQuestionId() == 0) {
				throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_Q, ERR_MSG_MGMT_UPDATE_FAIL_Q);
			}

			List<QaIndexVo> qaQnaIndexList = this.addParaphrase(q, paraphraseList);
			qaQnaIndexList = nlpAnalyzeClient.analyzeList(qaQnaIndexList);

			Map<String, Object> param = new HashMap<>();
			param.put("list", qaQnaIndexList);

			int resultIndex = indexMapper.insertList(param);
			logger.debug("createQna resultIndex {}", resultIndex);

			if (resultIndex == 0) {
				throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_Q_INDEX, ERR_MSG_MGMT_UPDATE_FAIL_Q_INDEX);
			}

			int resultAnswerCount = 0;
			int resultRichCount = 0;
			List<QaAnswerVo> answerList = q.getAnswerList();
			for (int y = 0; y < answerList.size(); y++) {
				QaAnswerVo qaAnswerVo = answerList.get(y);
				qaAnswerVo.setAnswerId(0);
				qaAnswerVo.setQuestionId(q.getQuestionId());
				qaAnswerVo.setUseYn(USE_YN_Y);
				qaAnswerVo.setCreatorId(q.getCreatorId()); // 수정
				qaAnswerVo.setUpdatorId(q.getUpdatorId());
				int answerCnt = answerMapper.update(qaAnswerVo);

				QaRichVo qaRichVo = qaAnswerVo.getQaRichVo();
				if (answerCnt > 0 && qaRichVo != null && !StringUtils.isEmpty(qaRichVo.getRespCode())) {	// RichContent는 미리 생성되어있음
					String respCode = q.getQuestionId() + "_" + qaAnswerVo.getAnswerId();
					String tempRespCode = qaRichVo.getRespCode();
					if (!StringUtils.equals(respCode, tempRespCode)) {
						qaAnswerVo.setRespCode(respCode);
						answerCnt = answerMapper.update(qaAnswerVo); // respCode 추가해서 업데이트

						param.clear();
						param.put("respCode", respCode);
						param.put("tempRespCode", tempRespCode);
						param.put("updatorId", q.getUpdatorId());

						int richCnt = richMapper.updateQaRichRespCode(param);
						resultRichCount += richCnt;
					}
				}
				resultAnswerCount += answerCnt;
			}

			logger.debug("createQna resultAnswerCount {}", resultAnswerCount);
			if (resultAnswerCount == 0) {
				throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_A, ERR_MSG_MGMT_UPDATE_FAIL_A);
			}

			logger.debug("createQna resultRichCount {}", resultRichCount);

		} catch (Exception e) {

			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_ERROR_Q, ERR_MSG_MGMT_UPDATE_ERROR_Q);
		}
		return count;
	}*/

	@Transactional(rollbackFor = { Exception.class })
	public int createQna(QaBqaVo q) throws BizChatbotException {
		logger.info("Service createQna parameter {}", q);

		Map<String, Object> param = new HashMap<>();
		int resultAnswerCount = 0;
		int resultRichCount = 0;
		int count  =0;
		try {

			QaAnswerVo qaAnswerVo = q.getAnswerGroup();

			qaAnswerVo.setAnswerId(0);
			qaAnswerVo.setUseYn(USE_YN_Y);
			qaAnswerVo.setCreatorId(q.getCreatorId()); // 수정
			qaAnswerVo.setUpdatorId(q.getUpdatorId());
			qaAnswerVo.setDomainId(q.getDomainId());
			qaAnswerVo.setTeamCode(q.getAttribute1());
            qaAnswerVo.setUserId(q.getAttribute2());
			int answerCnt = answerMapper.update(qaAnswerVo);

			QaRichVo qaRichVo = qaAnswerVo.getQaRichVo();
			if (answerCnt > 0 && qaRichVo != null && !StringUtils.isEmpty(qaRichVo.getRespCode())) {	// RichContent는 미리 생성되어있음
				String respCode = q.getDomainId() + "_" + qaAnswerVo.getAnswerId();
				String tempRespCode = qaRichVo.getRespCode();
				if (!StringUtils.equals(respCode, tempRespCode)) {
					qaAnswerVo.setRespCode(respCode);
					answerCnt = answerMapper.update(qaAnswerVo); // respCode 추가해서 업데이트
					param.clear();
					param.put("respCode", respCode);
					param.put("tempRespCode", tempRespCode);
					param.put("updatorId", q.getUpdatorId());

					int richCnt = richMapper.updateQaRichRespCode(param);
					resultRichCount += richCnt;
				}
			}

			resultAnswerCount += answerCnt;


			if (resultAnswerCount == 0) {
				throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_A, ERR_MSG_MGMT_UPDATE_FAIL_A);
			}



			List<QaQuestionsVo> list = q.getQuestionList();

			//질문들 넣기
			for(QaQuestionsVo qVo :  list ) {

				List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
				qVo.setQuestionId(0);
				qVo.setUseYn(USE_YN_Y);
				qVo.setMainYn("Y");
				qVo.setAnswerId(qaAnswerVo.getAnswerId());

				int resultCount = questionMapper.update(qVo);
				count += resultCount;

				if (resultCount == 0 && qVo.getQuestionId() == 0) {
					throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_Q, ERR_MSG_MGMT_UPDATE_FAIL_Q);
				}

				List<QaIndexsVo> qaQnaIndexList = this.addParaphrase(qVo, paraphraseList);
				qaQnaIndexList = nlpAnalyzeClient.analyzeList(qaQnaIndexList);

				param.put("list", qaQnaIndexList);

				int resultIndex = indexMapper.insertList(param);
				logger.debug("createQna resultIndex {}", resultIndex);

				if (resultIndex == 0) {
					throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_Q_INDEX, ERR_MSG_MGMT_UPDATE_FAIL_Q_INDEX);
				}

			}


		} catch (Exception e) {

			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_ERROR_Q, ERR_MSG_MGMT_UPDATE_ERROR_Q);
		}
		return count;
	}

	/*@Transactional(rollbackFor = { Exception.class })
	public int updateQna(QaQuestionVo qaQuestionVo) throws BizChatbotException {
		logger.info("Service updateQna parameter {}", qaQuestionVo);
		int resultCount = 0;
		int questionId = qaQuestionVo.getQuestionId();
		try {

			resultCount = questionMapper.update(qaQuestionVo);
			logger.debug("questionMapper update resultCount={}", resultCount);
			if (resultCount == 0) {
				throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_Q, ERR_MSG_MGMT_UPDATE_FAIL_Q);
			}

			Map<String, Object> param = new HashMap<>();
			param.put("useYn", USE_YN_N);
			param.put("questionId", questionId);
			param.put("updatorId", 1);

			int resultIndex = indexMapper.updateUseYn(param);
			logger.debug("indexMapper updateUseYn resultIndex={}", resultIndex);
			if (qaQuestionVo.getUseYn().equals(USE_YN_Y)) {

				List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
				List<QaIndexVo> qaQnaIndexList = this.addParaphrase(qaQuestionVo, paraphraseList);
				qaQnaIndexList = nlpAnalyzeClient.analyzeList(qaQnaIndexList);

				Map<String, Object> paramIndex = new HashMap<>();
				paramIndex.put("list", qaQnaIndexList);

				resultIndex = indexMapper.insertList(paramIndex);
				logger.debug("indexMapper updateUseYn resultIndex={}", resultIndex);

				if (resultIndex == 0) {
					throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_Q_INDEX, ERR_MSG_MGMT_UPDATE_FAIL_Q_INDEX);
				}

				List<QaAnswerVo> answerList = qaQuestionVo.getAnswerList();
				answerMapper.updateUseYn(param);

				int resultAnswerCount = 0;
				int resultRichCount = 0;
				for (QaAnswerVo answer : answerList) {
					if (answer.getAnswerId() == 0) {
						answer.setQuestionId(questionId);
					}
					answer.setUpdatorId(qaQuestionVo.getUpdatorId());
					int answerCnt = answerMapper.update(answer);

					QaRichVo qaRichVo = answer.getQaRichVo();
					if (answerCnt > 0 && qaRichVo != null && !StringUtils.isEmpty(qaRichVo.getRespCode())) {	// RichContent는 미리 생성되어있음
						if (answer.getQuestionId() == 0) {
							answer.setQuestionId(questionId);
						}

						String respCode = answer.getQuestionId() + "_" + answer.getAnswerId();
						String tempRespCode = qaRichVo.getRespCode();
						if (!StringUtils.equals(respCode, tempRespCode)) {
							answer.setRespCode(respCode);
							answerCnt = answerMapper.update(answer); // respCode 추가해서 업데이트

							param.clear();
							param.put("respCode", respCode);
							param.put("tempRespCode", tempRespCode);
							param.put("updatorId", qaQuestionVo.getUpdatorId());

							int richCnt = richMapper.updateQaRichRespCode(param);
							resultRichCount += richCnt;
						}
					}
					resultAnswerCount += answerCnt;

				}

				logger.debug("answerMapper update resultAnswerCount={}", resultAnswerCount);
				logger.debug("updateQna resultRichCount {}", resultRichCount);
			} else {

				int resultAnswerCount = answerMapper.updateUseYn(param);
				logger.debug("answerMapper insertList resultAnswerCount={}", resultAnswerCount);
			}

		} catch (BizChatbotException e) {
			throw e;
		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_ERROR_Q, ERR_MSG_MGMT_UPDATE_ERROR_Q);
		}
		return resultCount;
	}*/


	@Transactional(rollbackFor = { Exception.class })
	public int updateQna(QaBqaVo qaBqaVo) throws BizChatbotException {
		logger.info("Service updateQna parameter {}", qaBqaVo);
		int resultCount = 0;
		int questionId = 0;
		try {

			//answer객체 가져오기
			QaAnswerVo answer = qaBqaVo.getAnswerGroup();

			Map<String, Object> param = new HashMap<>();
			param.put("useYn", USE_YN_N);
			param.put("updatorId", 1);
			param.put("answerId", answer.getAnswerId());

			answerMapper.updateUseYn(param);

			int resultAnswerCount = 0;
			int resultRichCount = 0;

			answer.setUpdatorId(qaBqaVo.getUpdatorId());
			answer.setTeamCode(qaBqaVo.getAttribute1());
			answer.setUserId(qaBqaVo.getAttribute2());

			int answerCnt = answerMapper.update(answer);

			QaRichVo qaRichVo = answer.getQaRichVo();

			if (answerCnt > 0 && qaRichVo != null && !StringUtils.isEmpty(qaRichVo.getRespCode())) {	// RichContent는 미리 생성되어있음
				if (answer.getQuestionId() == 0) {
					answer.setQuestionId(questionId);
				}

				String respCode = answer.getQuestionId() + "_" + answer.getAnswerId();
				String tempRespCode = qaRichVo.getRespCode();
				if (!StringUtils.equals(respCode, tempRespCode)) {
					answer.setRespCode(respCode);
					answerCnt = answerMapper.update(answer); // respCode 추가해서 업데이트

					param.clear();
					param.put("respCode", respCode);
					param.put("tempRespCode", tempRespCode);
					param.put("updatorId", qaBqaVo.getUpdatorId());

					int richCnt = richMapper.updateQaRichRespCode(param);
					resultRichCount += richCnt;
				}
			}

			resultAnswerCount += answerCnt;

			logger.debug("answerMapper update resultAnswerCount={}", resultAnswerCount);
			logger.debug("updateQna resultRichCount {}", resultRichCount);


			List<QaQuestionsVo> qaQuestionsVoList = qaBqaVo.getQuestionList();

			for(QaQuestionsVo qaQuestionVo : qaQuestionsVoList) {
				questionId = qaQuestionVo.getQuestionId(); // 기존 question id;
				resultCount = questionMapper.update(qaQuestionVo);  //새로 업데이트

				param.clear();

				param.put("useYn", USE_YN_N);
				param.put("questionId", questionId);
				param.put("updatorId", 1);

				int resultIndex = indexMapper.updateUseYn(param);
				logger.debug("indexMapper updateUseYn resultIndex={}", resultIndex);

				logger.debug("questionMapper update resultCount={}", resultCount);

				if (resultCount == 0) {
					throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_Q, ERR_MSG_MGMT_UPDATE_FAIL_Q);
				}

				resultIndex = indexMapper.updateUseYn(param);
				logger.debug("indexMapper updateUseYn resultIndex={}", resultIndex);


				if (qaQuestionVo.getUseYn().equals(USE_YN_Y)) {

					List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
					List<QaIndexsVo> qaQnaIndexList = this.addParaphrase(qaQuestionVo, paraphraseList);
					qaQnaIndexList = nlpAnalyzeClient.analyzeList(qaQnaIndexList);

					Map<String, Object> paramIndex = new HashMap<>();
					paramIndex.put("list", qaQnaIndexList);

					resultIndex = indexMapper.insertList(paramIndex);
					logger.debug("indexMapper updateUseYn resultIndex={}", resultIndex);

					if (resultIndex == 0) {
						throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_Q_INDEX, ERR_MSG_MGMT_UPDATE_FAIL_Q_INDEX);
					}


				} else {

					resultAnswerCount = answerMapper.updateUseYn(param);
					logger.debug("answerMapper insertList resultAnswerCount={}", resultAnswerCount);
				}

			}

		} catch (BizChatbotException e) {
			throw e;
		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_ERROR_Q, ERR_MSG_MGMT_UPDATE_ERROR_Q);
		}
		return resultCount;
	}

	/*@Transactional(rollbackFor = { Exception.class })
	public int insertQnaList(List<QaQuestionVo> questionList) throws BizChatbotException {
		logger.info("Service insertQnaList questionList {}", questionList);

		int resultCount = 0;
		try {

			Map<String, Object> param = new HashMap<>();
			param.put("list", questionList);

			resultCount = questionMapper.insertList(param);

			logger.debug("questionMapper update resultCount={}", resultCount);
			if (resultCount == 0) {
				throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_Q, ERR_MSG_MGMT_UPDATE_FAIL_Q);
			}

			List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
			List<QaIndexVo> indexList = this.addParaphraseList(questionList, paraphraseList);
			indexList = nlpAnalyzeClient.analyzeList(indexList);

			Map<String, Object> paramIndex = new HashMap<>();
			param.put("list", indexList);

			int resultIndex = indexMapper.insertList(paramIndex);
			logger.debug("insertQnaList resultIndex {}", resultIndex);
			if (resultIndex == 0) {
				throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_Q_INDEX, ERR_MSG_MGMT_UPDATE_FAIL_Q_INDEX);
			}

			List<QaAnswerVo> answerList = new ArrayList<>();
			for (QaQuestionVo qaQuestionVo : questionList) {
				answerList.addAll(qaQuestionVo.getAnswerList());
			}
			Map<String, Object> params = new HashMap<>();
			params.put("list", answerList);

			int resultAnswerCount = answerMapper.insertList(params);
			logger.debug("insertQnaList resultAnswerCount {}", resultAnswerCount);
			if (resultAnswerCount == 0) {
				throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_A, ERR_MSG_MGMT_UPDATE_FAIL_A);
			}
		} catch (BizChatbotException e) {
			throw e;
		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_ERROR_Q, ERR_MSG_MGMT_UPDATE_ERROR_Q);
		}
		return resultCount;
	}*/

	/*@Transactional(rollbackFor = { Exception.class })
	public int deleteQna(QaQuestionVo qaQuestionVo) throws BizChatbotException {
		logger.info("Service deleteQna parameter {}", qaQuestionVo);
		int resultCount = 0;
		int questionId = qaQuestionVo.getQuestionId();
		try {

			Map<String, Object> param = new HashMap<>();
			param.put("useYn", USE_YN_N);
			param.put("questionId", questionId);
			param.put("updatorId", "system");

				int resultIndex = indexMapper.updateUseYn(param);
			logger.debug("indexMapper updateUseYn resultIndex={}", resultIndex);

			int resultAnswerCount = answerMapper.updateUseYn(param);
			logger.debug("answerMapper insertList resultAnswerCount={}", resultAnswerCount);

			resultCount = questionMapper.updateUseYn(param);
			logger.debug("questionMapper updateUseYn resultCount={}", resultCount);

		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new BizChatbotException(ERR_CODE_MGMT_DELETE_ERROR_Q, ERR_MSG_MGMT_DELETE_ERROR_Q);
		}
		return resultCount;
	}*/

	@Transactional(rollbackFor = { Exception.class })
	public int deleteQna(QaBqaVo qaBqaVo) throws BizChatbotException {
		logger.info("Service deleteQna parameter {}", qaBqaVo);
		int resultCount = 0;
		Map<String, Object> param = new HashMap<>();

		List<QaQuestionsVo> list = qaBqaVo.getQuestionList();
		int answerId = qaBqaVo.getAnswerGroup().getAnswerId();

		try {

			if(list != null) {

				for(QaQuestionsVo vo : list) {

					param.put("useYn", USE_YN_N);
					param.put("questionId", vo.getQuestionId());
					param.put("updatorId", "system");

					int resultIndex = indexMapper.updateUseYn(param);
					logger.debug("indexMapper updateUseYn resultIndex={}", resultIndex);

					resultCount = questionMapper.updateUseYn(param);
					logger.debug("questionMapper updateUseYn resultCount={}", resultCount);

				}

			}

			param.clear();

			param.put("useYn", USE_YN_N);
			param.put("answerId", answerId);
			param.put("updatorId", "system");

			int resultAnswerCount = answerMapper.updateUseYn(param);
			logger.debug("answerMapper insertList resultAnswerCount={}", resultAnswerCount);



		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new BizChatbotException(ERR_CODE_MGMT_DELETE_ERROR_Q, ERR_MSG_MGMT_DELETE_ERROR_Q);
		}
		return resultCount;
	}


	@Transactional(rollbackFor = { Exception.class })
	public int deleteQnaByDomainId(int[] domainIds) throws BizChatbotException {
		logger.info("Service deleteQna domainIds {}", domainIds);

		int resultCount = 0;

		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("domainIds", domainIds);
			params.put("useYn", USE_YN_Y);
			if (domainIds != null && domainIds[0] != 0) {
				params.put("domain", 1);
			} else {
				params.put("domain", 0);
			}
			List<QaQuestionVo> list = questionMapper.gets(params);
			ArrayList<Integer> arrayList = new ArrayList<Integer>();
			for(int i = 0 ; i < list.size() ; i++) {

				arrayList.add(i,  list.get(i).getQuestionId());
			}

			if(arrayList.size() > 0) {
				Map<String, Object> param = new HashMap<>();
				param.put("useYn", USE_YN_N);
				param.put("questionIds", arrayList);
				param.put("updatorId", "system");

				int resultIndex = indexMapper.updateUseYn(param);
				logger.debug("indexMapper updateUseYn resultIndex={}", resultIndex);

				int resultAnswerCount = answerMapper.updateUseYn(param);
				logger.debug("answerMapper insertList resultAnswerCount={}", resultAnswerCount);

				resultCount = questionMapper.updateUseYn(param);
				logger.debug("questionMapper updateUseYn resultCount={}", resultCount);
			}
		} catch (Exception e) {
			logger.warn(ERR_MSG_MGMT_UPDATE_ERROR_Q + "{}", e);
			throw new BizChatbotException(ERR_CODE_MGMT_DELETE_ERROR_Q, ERR_MSG_MGMT_DELETE_ERROR_Q);
		}
		return resultCount;
	}

	/*public List<QaIndexVo> addParaphrase(QaQuestionVo qaQuestionVo, List<QaQnaParaphraseVo> paraphraseList) {

		List<QaIndexVo> qaIndexList = new ArrayList<QaIndexVo>();

		int seq = 1;
		qaIndexList.add(new QaIndexVo(qaQuestionVo, seq, qaQuestionVo.getMainYn()));
		seq++;

		// ALL Paraphrase while
		for (int j = 0; j < paraphraseList.size(); j++) {

			String question = qaQuestionVo.getQuestion();
			String mainWord = paraphraseList.get(j).getMainWord();
			StringBuilder questionBuilder = new StringBuilder(question);
			int lastIndex = StringUtils.lastIndexOf(question, mainWord);

			if (lastIndex > -1 && StringUtils.endsWith(question, mainWord)) {

				String[] paraphraseArray = StringUtils.split(paraphraseList.get(j).getParaphraseWord(), ",");

				// Same Paraphrase while
				for (int x = 0; x < paraphraseArray.length; x++) {

					StringBuilder paraphraseBuilder = questionBuilder.replace(lastIndex, questionBuilder.length(),
							paraphraseArray[x]);
					qaQuestionVo.setQuestion(paraphraseBuilder.toString());

					qaIndexList.add(new QaIndexVo(qaQuestionVo, seq, "N"));
					seq++;
				}
			}
		}
		return qaIndexList;
	}*/


	public List<QaIndexsVo> addParaphrase(QaQuestionsVo qaQuestionVo, List<QaQnaParaphraseVo> paraphraseList) {

		List<QaIndexsVo> qaIndexList = new ArrayList<QaIndexsVo>();

		int seq = 1;
		qaIndexList.add(new QaIndexsVo(qaQuestionVo, seq, qaQuestionVo.getMainYn()));
		seq++;

		// ALL Paraphrase while
		for (int j = 0; j < paraphraseList.size(); j++) {

			String question = qaQuestionVo.getQuestion();
			String mainWord = paraphraseList.get(j).getMainWord();
			StringBuilder questionBuilder = new StringBuilder(question);
			int lastIndex = StringUtils.lastIndexOf(question, mainWord);

			if (lastIndex > -1 && StringUtils.endsWith(question, mainWord)) {

				String[] paraphraseArray = StringUtils.split(paraphraseList.get(j).getParaphraseWord(), ",");

				// Same Paraphrase while
				for (int x = 0; x < paraphraseArray.length; x++) {

					StringBuilder paraphraseBuilder = questionBuilder.replace(lastIndex, questionBuilder.length(),
							paraphraseArray[x]);
					qaQuestionVo.setQuestion(paraphraseBuilder.toString());

					qaIndexList.add(new QaIndexsVo(qaQuestionVo, seq, "N"));
					seq++;
				}
			}
		}
		return qaIndexList;
	}

	/*public List<QaIndexVo> addParaphraseList(List<QaQuestionVo> qaQuestionList,
											 List<QaQnaParaphraseVo> paraphraseList) {

		List<QaIndexVo> qaIndexList = new ArrayList<QaIndexVo>();

		// ALL Q&A while
		for (int i = 0; i < qaQuestionList.size(); i++) {

			qaIndexList.addAll(addParaphrase(qaQuestionList.get(i), paraphraseList));
		}
		return qaIndexList;
	}*/

	public BaseListObject<QaExcelVo> getsExcel(int domainId) throws BizChatbotException {
		logger.debug("Service getsExcel parameter domainId {}", domainId);

		List<QaExcelVo> qList = null;
		int totalCount = 0;
		try {
			HashMap<String, Object> paramQ = new HashMap<>();
			paramQ.put("domainId", domainId);
			paramQ.put("useYn", USE_YN_Y);

			qList = excelMapper.getsExcel(paramQ);
			if(qList != null) {
				totalCount = qList.size();
			}
		} catch (Exception e) {
			logger.warn(ERR_MSG_EXCEL_CREATE_ERROR, e);
			throw new BizChatbotException(ERR_CODE_EXCEL_CREATE_ERROR, ERR_MSG_EXCEL_CREATE_ERROR);
		}
		BaseListObject<QaExcelVo> obj = new BaseListObject<QaExcelVo>(totalCount, qList);
		return obj;
	}

	/*@Transactional(rollbackFor = { Exception.class })
	public int insertExcel(List<QaExcelVo> list) throws BizChatbotException {

		logger.debug("Servcice uploadFile list", list);
		int resultCount = 0;
		int resultAnswerCount = 0;
		try {
			List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
			List<QaIndexsVo> qaQnaIndexList = new ArrayList<>();
			List<QaAnswerVo> answerList = new ArrayList<>();
			logger.debug("======================================================================================");
			int questionId = 0;
//			int beforeQuestionId = 0;
			boolean qFlag = false, aFlag = false;
			for (int i = 0; i < list.size(); i++) {

				QaExcelVo e = list.get(i);
				QaQuestionsVo q = new QaQuestionsVo();

				if (i == 0 || qFlag) { // 최초 질문일때  or 새로운 Question 입력
					q.setQuestionId(0);
					q.setDomainId(e.getDomainId());
					q.setQuestion(e.getQuestion());
					q.setUseYn(USE_YN_Y);
					q.setMainYn("Y");
					q.setCreatorId(e.getUserId()); // 수정
					q.setUpdatorId(e.getUserId());
					//	q.setAttribute1(e.getAttribute1());
					//	q.setAttribute2(e.getAttribute2());

					resultCount += questionMapper.update(q);
					questionId = q.getQuestionId();
					qaQnaIndexList.addAll(this.addParaphrase(q, paraphraseList));
				}
				int count = 0;
				// Excel 같은 질문일때
				if (i > 0 && e.getQuestionId() == list.get(i - 1).getQuestionId()) {
					qFlag = false;
					aFlag = false;
					count++;
				}else { // Excel 다른 질문일때
					aFlag = true;
					qFlag = true;
				}

				if (list.size() == i + 1) { // 마지막 순서 처리
					aFlag = true;
				}

				if (aFlag) { // Answer List 입력
					for (int x = i - count; x <= i; x++) {

						QaAnswerVo qaAnswerVo = new QaAnswerVo();
						QaExcelVo ea = list.get(x);
						logger.debug("{} 번째 응답", x, ea);
						qaAnswerVo.setAnswerId(0);
						qaAnswerVo.setAnswer(ea.getAnswer());
						qaAnswerVo.setQuestionId(questionId);
						qaAnswerVo.setUseYn(USE_YN_Y);
						qaAnswerVo.setCreatorId(ea.getUserId()); // 수정
						qaAnswerVo.setUpdatorId(ea.getUserId());

						answerList.add(qaAnswerVo);
					}
				}
			}
			logger.debug("createQna answerList size ={}", answerList.size());

			if(answerList.size() > 100) {
				int pageCount =  answerList.size() / queryCount ;
				for(int i = 0 ; i < pageCount ; i++) {
					Map<String, Object> paramA = new HashMap<>();
					paramA.put("list", answerList.subList(i*queryCount, ((i+1)*(queryCount))));
					resultAnswerCount += answerMapper.insertList(paramA);
				}
				Map<String, Object> paramA = new HashMap<>();
				paramA.put("list", answerList.subList((queryCount*pageCount)+1, answerList.size()));
				resultAnswerCount += answerMapper.insertList(paramA);
			}else {
				Map<String, Object> paramA = new HashMap<>();
				paramA.put("list", answerList);
				resultAnswerCount += answerMapper.insertList(paramA);
			}
			logger.debug("createQna answerList resultAnswerCount ={}", resultAnswerCount);
			StreamObserver<Document> responseObserver = new StreamObserver<Document>() {

				int index = 0;

				@Override
				public void onNext(Document document) {

					Map<String,Object> map = nlpAnalyzeClient.modify(document.getSentencesList());

					String questionMorph = map.get("questionMorph").toString();
					int morphCount = (Integer)map.get("morphCount");
					qaQnaIndexList.get(index).setMorphCount(morphCount);
					qaQnaIndexList.get(index).setQuestionMorph(questionMorph); // 분석 결과 삽입
					logger.info("question {}, questionMorph , {}", qaQnaIndexList.get(index).getQuestion(),
							questionMorph);
					index++;
				}

				@Override
				public void onError(Throwable t) {

					logger.error("analyzeMultiple Failed: {}", Status.fromThrowable(t));
					nlpAnalyzeClient.countDownLatch();
				}

				@Override
				public void onCompleted() {

					nlpAnalyzeClient.countDownLatch();
					int result = 0;
					if(qaQnaIndexList.size() > queryCount) {
						int pageCount =  qaQnaIndexList.size() / queryCount ;
						for(int i = 0 ; i < pageCount ; i++) {
							result += insertIndexList(qaQnaIndexList.subList(i*queryCount, ((i+1)*(queryCount))));
						}
						result = insertIndexList(qaQnaIndexList.subList((queryCount*pageCount)+1, qaQnaIndexList.size()));
					}else {
						result = insertIndexList(qaQnaIndexList);
					}
					logger.info("insertIndexList result count: {}", result);
				}
			};

			requestAnalyze(responseObserver, qaQnaIndexList);

			logger.debug("======================================================================================");
			logger.debug("resultCount = {}, resultAnswerCount = {}", resultCount, resultAnswerCount);
		} catch (Exception e) {
			logger.warn(e.toString(), e);
			throw new BizChatbotException(ERR_CODE_EXCEL_UPLOAD_ERROR, ERR_MSG_EXCEL_UPLOAD_ERROR);
		}
		return resultCount;
	}*/


	@Transactional(rollbackFor = { Exception.class })
	public int insertExcel(List<QaExcelVo> list) throws BizChatbotException {

		logger.debug("Servcice uploadFile list", list);
		int resultCount = 0;
		int resultAnswerCount = 0;
		try {
			List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();
			List<QaIndexsVo> qaQnaIndexList = new ArrayList<>();
			List<QaAnswerVo> answerList = new ArrayList<>();
			List<QaQuestionsVo> questionList = new ArrayList<>();

			logger.debug("======================================================================================");
			int answerId =0;
			boolean aFlag = false;

			for (int i = 0; i < list.size(); i++) {

				QaExcelVo e = list.get(i);
				QaAnswerVo answer = new QaAnswerVo();

				if(i == 0 || e.getQuestionId() != list.get(i - 1).getQuestionId()) {
					aFlag = false;
				}else{
					aFlag = true;
				}

				if (!aFlag) { // 최초 질문일때  or 새로운 Question 입력

					answer.setAnswerId(0);
					answer.setDomainId(e.getDomainId());
					answer.setAnswer(e.getAnswer());
					answer.setUseYn(USE_YN_Y);
					answer.setTeamCode(e.getAttribute1());
					answer.setUserId(e.getAttribute2());
					answer.setCreatorId(e.getUserId()); // 수정
					answer.setUpdatorId(e.getUserId());

					resultAnswerCount = answerMapper.update(answer);

					if(resultAnswerCount < 0){
						throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_A, ERR_MSG_MGMT_UPDATE_FAIL_A);
					}

					logger.debug("createQna answerList resultAnswerCount ={}", resultAnswerCount);
					answerId = answer.getAnswerId();

				}

				//question 모음.
				QaQuestionsVo qaQuestionsVo = new QaQuestionsVo();

				logger.debug("{} 번째 응답",(i+1), e);

				qaQuestionsVo.setQuestionId(0);
				qaQuestionsVo.setAnswerId(answerId);
				qaQuestionsVo.setDomainId(e.getDomainId());
				qaQuestionsVo.setQuestion(e.getQuestion());
				qaQuestionsVo.setUseYn(USE_YN_Y);
				qaQuestionsVo.setMainYn("Y");
				qaQuestionsVo.setCreatorId(e.getUserId()); // 수정
				qaQuestionsVo.setUpdatorId(e.getUserId());

				resultCount += questionMapper.update(qaQuestionsVo);

				if(resultCount < 0){
					throw new BizChatbotException(ERR_CODE_MGMT_UPDATE_FAIL_A, ERR_MSG_MGMT_UPDATE_FAIL_A);
				}

				qaQnaIndexList.addAll(this.addParaphrase(qaQuestionsVo, paraphraseList));


			}

			StreamObserver<Document> responseObserver = new StreamObserver<Document>() {

				int index = 0;

				@Override
				public void onNext(Document document) {

					Map<String,Object> map = nlpAnalyzeClient.modify(document.getSentencesList());

					String questionMorph = map.get("questionMorph").toString();
					int morphCount = (Integer)map.get("morphCount");
					qaQnaIndexList.get(index).setMorphCount(morphCount);
					qaQnaIndexList.get(index).setQuestionMorph(questionMorph); // 분석 결과 삽입
					logger.info("question {}, questionMorph , {}", qaQnaIndexList.get(index).getQuestion(),
							questionMorph);
					index++;
				}

				@Override
				public void onError(Throwable t) {

					logger.error("analyzeMultiple Failed: {}", Status.fromThrowable(t));
					nlpAnalyzeClient.countDownLatch();
				}

				@Override
				public void onCompleted() {

					nlpAnalyzeClient.countDownLatch();
					int result = 0;
					if(qaQnaIndexList.size() > queryCount) {
						int pageCount =  qaQnaIndexList.size() / queryCount ;
						for(int i = 0 ; i < pageCount ; i++) {
							result += insertIndexList(qaQnaIndexList.subList(i*queryCount, ((i+1)*(queryCount))));
						}
						result = insertIndexList(qaQnaIndexList.subList((queryCount*pageCount)+1, qaQnaIndexList.size()));
					}else {
						result = insertIndexList(qaQnaIndexList);
					}
					logger.info("insertIndexList result count: {}", result);
				}
			};

			requestAnalyze(responseObserver, qaQnaIndexList);

			logger.debug("======================================================================================");
			logger.debug("resultCount = {}, resultAnswerCount = {}", resultCount, resultAnswerCount);
		} catch (Exception e) {
			logger.warn(e.toString(), e);
			throw new BizChatbotException(ERR_CODE_EXCEL_UPLOAD_ERROR, ERR_MSG_EXCEL_UPLOAD_ERROR);
		}
		return resultCount;
	}

	// index db insert 요청
	/*public int insertIndexList(List<QaIndexVo> qaIndexList) {

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("list", qaIndexList);

		int result = indexMapper.insertList(paramMap);

		return result;
	}*/

	public int insertIndexList(List<QaIndexsVo> qaIndexList) {

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("list", qaIndexList);

		int result = indexMapper.insertList(paramMap);

		return result;
	}

/*	public void requestAnalyze(StreamObserver<Document> responseObserver, List<QaIndexVo> qaIndexList) {

		StreamObserver<InputText> requestObserver = nlpAnalyzeClient.analyzeMultiple(responseObserver);

		for (int i = 0; i < qaIndexList.size(); ++i) {

			String question = qaIndexList.get(i).getQuestion();
			InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor).setSplitSentence(true)
					.setUseTokenizer(true).setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
					.setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();

			requestObserver.onNext(inputText);
		}
		requestObserver.onCompleted();
	}

	@Transactional(rollbackFor = { Exception.class })
	public int insertParaphrase(Map<String, Object> map) throws BizChatbotException  {
		logger.info("Service insertParaphrase parameter  {}", map);
		int resultMain = 0,resultParaphrase = 0, resultRelation =0;
		try {
			if(map != null && map.size() > 0) {
				Iterator<String> keys = map.keySet().iterator();
				while( keys.hasNext() ){
					String key = keys.next();
					System.out.println( "key : " + key + ", value : " + map.get(key));
					QaParaphraseMainWordVo qaParaphraseMainWordVo = new QaParaphraseMainWordVo();
					qaParaphraseMainWordVo.setMainWord(key);
					qaParaphraseMainWordVo.setUseYn("Y");
					resultMain += qnaParaphrase.insertMain(qaParaphraseMainWordVo);

					String data =String.valueOf(map.get(key));
					String[] array = data.split(",");
					for(int i = 0 ; i < array.length ; i++) {
						QaParaphraseWordVo qaParaphraseWordVo = new QaParaphraseWordVo();
						qaParaphraseWordVo.setWord(array[i]);
						resultParaphrase += qnaParaphrase.insertParaphrase(qaParaphraseWordVo);
						QaParaphraseRelationVo qaParaphraseRelationVo = new QaParaphraseRelationVo();
						qaParaphraseRelationVo.setMainWordId(qaParaphraseMainWordVo.getMainWordId());
						qaParaphraseRelationVo.setWordId(qaParaphraseWordVo.getWordId());
						resultRelation += qnaParaphrase.insertRelation(qaParaphraseRelationVo);
					}
				}
			}
		}catch(Exception e) {
			logger.warn(e.getMessage());

			throw new BizChatbotException(ERR_CODE_PARAPHRASE_INSERT_ERROR, ERR_MSG_PARAPHRASE_INSERT_ERROR);
		}finally {
			logger.info("Service insertParaphrase resultMain = {},resultParaphrase = {},resultRelation = {}", resultMain, resultParaphrase, resultRelation);
		}

		return resultMain;
	}*/


	public void requestAnalyze(StreamObserver<Document> responseObserver, List<QaIndexsVo> qaIndexList) {

		StreamObserver<InputText> requestObserver = nlpAnalyzeClient.analyzeMultiple(responseObserver);

		for (int i = 0; i < qaIndexList.size(); ++i) {

			String question = qaIndexList.get(i).getQuestion();
			InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor).setSplitSentence(true)
					.setUseTokenizer(true).setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
					.setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();

			requestObserver.onNext(inputText);
		}
		requestObserver.onCompleted();
	}

	@Transactional(rollbackFor = { Exception.class })
	public int insertParaphrase(Map<String, Object> map) throws BizChatbotException  {
		logger.info("Service insertParaphrase parameter  {}", map);
		int resultMain = 0,resultParaphrase = 0, resultRelation =0;
		try {
			if(map != null && map.size() > 0) {
				Iterator<String> keys = map.keySet().iterator();
				while( keys.hasNext() ){
					String key = keys.next();
					System.out.println( "key : " + key + ", value : " + map.get(key));
					QaParaphraseMainWordVo qaParaphraseMainWordVo = new QaParaphraseMainWordVo();
					qaParaphraseMainWordVo.setMainWord(key);
					qaParaphraseMainWordVo.setUseYn("Y");
					resultMain += qnaParaphrase.insertMain(qaParaphraseMainWordVo);

					String data =String.valueOf(map.get(key));
					String[] array = data.split(",");
					for(int i = 0 ; i < array.length ; i++) {
						QaParaphraseWordVo qaParaphraseWordVo = new QaParaphraseWordVo();
						qaParaphraseWordVo.setWord(array[i]);
						resultParaphrase += qnaParaphrase.insertParaphrase(qaParaphraseWordVo);
						QaParaphraseRelationVo qaParaphraseRelationVo = new QaParaphraseRelationVo();
						qaParaphraseRelationVo.setMainWordId(qaParaphraseMainWordVo.getMainWordId());
						qaParaphraseRelationVo.setWordId(qaParaphraseWordVo.getWordId());
						resultRelation += qnaParaphrase.insertRelation(qaParaphraseRelationVo);
					}
				}
			}
		}catch(Exception e) {
			logger.warn(e.getMessage());

			throw new BizChatbotException(ERR_CODE_PARAPHRASE_INSERT_ERROR, ERR_MSG_PARAPHRASE_INSERT_ERROR);
		}finally {
			logger.info("Service insertParaphrase resultMain = {},resultParaphrase = {},resultRelation = {}", resultMain, resultParaphrase, resultRelation);
		}

		return resultMain;
	}

	public List<QaExcelVo> uploadExcelData(String userId, String domainId, MultipartFile file) throws  BizChatbotException {

		Workbook workbook = ExcelUtils.getExcelWorkBook(file);
		Sheet sheet = (Sheet) workbook.getSheetAt(0);

		int rowCount = sheet.getPhysicalNumberOfRows();
		int lastCellNum = 0;

		List<QaExcelVo> list = new ArrayList<>();
		try {

			for (int i = 1; i < rowCount; i++) {
				Row row = sheet.getRow(i);

				if (row != null) {
					QaExcelVo excelVo = new QaExcelVo();
					excelVo.setQuestionId(Integer.parseInt(ExcelUtils.getCellValue(row.getCell(0))));
					excelVo.setQuestion(ExcelUtils.getCellValue(row.getCell(1)));
					excelVo.setAnswerId(Integer.parseInt(ExcelUtils.getCellValue(row.getCell(2))));
					excelVo.setAnswer(ExcelUtils.getCellValue(row.getCell(3)));
					excelVo.setDomainId(Integer.parseInt(domainId));
					excelVo.setUserId(userId);
					list.add(excelVo);
				}
			}



		}catch (Exception e){
			throw new BizChatbotException(ERR_CODE_EXCEL_UPLOAD_ERROR, ERR_MSG_EXCEL_UPLOAD_ERROR);
		}

		return list;
	}

}
