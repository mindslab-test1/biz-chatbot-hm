package ai.mindslab.bizchatbot.bqa.service;

import ai.mindslab.bizchatbot.bqa.dao.QaAnswerMapper;
import ai.mindslab.bizchatbot.bqa.dao.data.QaAnswerVo;
import ai.mindslab.bizchatbot.bqa.dao.data.RichExcelVo;
import ai.mindslab.bizchatbot.bqa.util.ExcelUtils;
import ai.mindslab.bizchatbot.bqa.util.ImageUtils;
import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotApiMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotRichContentMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.*;
import ai.mindslab.bizchatbot.commons.codes.IBasicQAConst;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseListObject;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.utils.StrUtils;
import ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao.QaRichMapper;
import ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao.data.QaRichVo;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BqaRichContentService implements IRestCodes, IBasicQAConst {
	
	private Logger logger = LoggerFactory.getLogger(BqaRichContentService.class);
	
	@Autowired
	protected ChatbotRichContentMapper rich;

	@Autowired
	private QaRichMapper richMapper;
	
	@Autowired
	protected ChatbotApiMapper chatbotMapper;

	@Autowired
	protected QaAnswerMapper answerMapper;
	
	@Autowired
	@Qualifier("imageUtils")
	private ImageUtils imgUtils;

	@Transactional(rollbackFor = { Exception.class })
	public QaRichVo getBqaRichInfo(String respCode) throws BizChatbotException{
		logger.info("getRespInfo/{}",respCode);

		QaRichVo qaRichVo = null;
		try {
			qaRichVo = richMapper.getQaRich(respCode);
		}catch(Exception e) {
			logger.warn("getRespInfo/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return qaRichVo;
	}

	@Transactional(rollbackFor = { Exception.class })
	public long insertBqaRichContent(String respCode, String contentTypeCd, String userId) throws BizChatbotException{
		logger.info("insertBqaRichContent/{},{},{}", respCode, contentTypeCd, userId);
		long nRet = 0;

		QaRichVo currRel = null;
		try {
			currRel = richMapper.getQaRich(respCode);
		}catch(Exception e) {
			logger.warn("insertBqaRichContent/can't get rel info. {}",e.getMessage());
			throw e;
		}

		try {

			long rcId = 0;

			HashMap<String, Object> richParams = new HashMap<>();
			if(IChatbotConst.RICH_CONTENT_TYPE_API.equals(contentTypeCd)) {
				//not insert.
				rcId = 0;
			} else {
				richParams.put("contentTypeCd", contentTypeCd);
				richParams.put("creatorId", userId);
				richParams.put("updatorId", userId);
				nRet = chatbotMapper.insertRichContent(richParams);

				rcId = (long)richParams.get("rcId");
			}

			if(currRel == null) {

				QaRichVo respRel = new QaRichVo();
				respRel.setRespCode(respCode);
				respRel.setContentTypeCd(contentTypeCd);
				respRel.setRcId((int) rcId);
				respRel.setCreatorId(userId);
				respRel.setUpdatorId(userId);

				logger.warn("insertBqaRichContent/insert new rel:{}",respRel.toString());
				nRet = (long) richMapper.insertQaRich(respRel);
			} else {
				currRel.setContentTypeCd(contentTypeCd);
				currRel.setRcId((int) rcId);
				currRel.setUpdatorId(userId);

				logger.warn("insertBqaRichContent/update new rel:{}",currRel.toString());
				nRet = (long) richMapper.updateQaRich(currRel);
			}

		}catch(Exception e) {
			logger.warn("insertBqaRichContent/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return nRet;
	}

	@Transactional(rollbackFor = { Exception.class })
	public long deleteBqaRichContent(String respCode, String contentTypeCd) throws BizChatbotException{
		logger.info("deleteBqaRichContent/{},{}",respCode, contentTypeCd);

		long nRet = 0;
		QaRichVo qaRichVo = null;

		try {
			qaRichVo = richMapper.getQaRich(respCode);
			if(qaRichVo != null) {
				if(!contentTypeCd.equals(qaRichVo.getContentTypeCd())) {
					throw new Exception("Invalid Rich Content Type: " + contentTypeCd);
				}

				chatbotMapper.deleteRichContent(qaRichVo.getRcId());

				switch(contentTypeCd) {
					case IChatbotConst.RICH_CONTENT_TYPE_BUTTON:
						nRet = chatbotMapper.deleteButtonById(qaRichVo.getRcId());
						break;
					case IChatbotConst.RICH_CONTENT_TYPE_IMAGE:
						nRet = chatbotMapper.deleteImageById(qaRichVo.getRcId());
						break;
					case IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL:
						nRet = chatbotMapper.deleteCarouselById(qaRichVo.getRcId());
						break;
					case IChatbotConst.RICH_CONTENT_TYPE_API:
						//delete api는 없음.
						break;
					case IChatbotConst.RICH_CONTENT_TYPE_NONE:
						//just return
						return nRet;
					default:
						throw new Exception("Invalid rich content type: "+contentTypeCd);
				}
				logger.debug("delete rich content count : {}, rcId: {} ", nRet, qaRichVo.getRcId());
			} else {
				throw new Exception("QaRich Not Found");
			}
		}catch(Exception e) {
			logger.warn("deleteBqaRichContent/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return nRet;
	}

	@Transactional(rollbackFor = { Exception.class })
	public ChatbotRichContent getBqaRichContent(String respCode) throws BizChatbotException{
		logger.info("getBqaRichContent/{}", respCode);
		
		ChatbotRichContent richContent = null;
		QaRichVo qaRichVo = null;
		try {
			qaRichVo = richMapper.getQaRich(respCode);
			if(qaRichVo != null) {
				String type = qaRichVo.getContentTypeCd();

				switch(type) {
					case IChatbotConst.RICH_CONTENT_TYPE_BUTTON:
						richContent = rich.getRichConent(qaRichVo.getRcId());
						if(richContent != null) {
							List<ChatbotButton> btns = rich.getButtonList(qaRichVo.getRcId());
							richContent.getButtons().addAll(btns);
						}
						break;
					case IChatbotConst.RICH_CONTENT_TYPE_IMAGE:
						richContent = rich.getRichConent(qaRichVo.getRcId());
						if(richContent != null) {
							ChatbotRichImage img = rich.getRichImage(qaRichVo.getRcId());
							//transfer image path => url
							// if(img != null && !StringUtils.isEmpty(img.getImagePath())) {
							// 	img.setImagePath(imgUtils.transferImagePath(IChatbotConst.RICH_CONTENT_TYPE_IMAGE, "bqa", img.getImagePath()));
							// }
							richContent.setImage(img);
						}
						break;
					case IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL:
						richContent = rich.getRichConent(qaRichVo.getRcId());
						if(richContent != null) {
							List<ChatbotRichCarousel> list = rich.getRichCarousel(qaRichVo.getRcId());
							richContent.getCarousels().addAll(list);
							//transfer image path
							// if(list != null) {
							//	for(ChatbotRichCarousel ca: list) {
							//		ca.setImagePath(imgUtils.transferImagePath(IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL, "bqa", ca.getImagePath()));
							//	}
							// }
						}
						break;
				default:
					throw new Exception("Invalid Rich Content type " + type);
				}
			}
		}catch(Exception e) {
			logger.warn("getBqaRichContent/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return richContent;
	}



	@Transactional(rollbackFor = { Exception.class })
	public ChatbotApiInfo getBqaApiInfo(String respCode) throws BizChatbotException{
		logger.info("getBqaApiInfo/{},{}", respCode);
		
		ChatbotApiInfo apiInfo = null;
		QaRichVo qaRichVo = null;
		try {
			qaRichVo = richMapper.getQaRich(respCode);
			if(qaRichVo != null) {
				String type = qaRichVo.getContentTypeCd();
				if(IChatbotConst.RICH_CONTENT_TYPE_API.equals(type)) {
					apiInfo = chatbotMapper.getApiInfo(qaRichVo.getRcId());
					if(apiInfo != null) {
						List<ChatbotApiParam> pars = chatbotMapper.getParams(qaRichVo.getRcId());
						apiInfo.getParamList().addAll(pars);
						ChatbotApiJsonTemplate tpl = chatbotMapper.getJsonTemplate(qaRichVo.getRcId());
						apiInfo.setJsonTemplate(tpl);
					}
				}
			}
		}catch(Exception e) {
			logger.warn("getBqaApiInfo/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return apiInfo;
	}

	@Transactional(rollbackFor = { Exception.class })
	public BaseListObject<ChatbotApiInfoDisp> getBqaApiList(int workspaceId) throws BizChatbotException{
		logger.info("getBqaApiList/{}",workspaceId);
		
		BaseListObject<ChatbotApiInfoDisp> out = new BaseListObject<>();
		List<ChatbotApiInfoDisp> list = null;
		long tot = 0;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("workspaceId", workspaceId);
			//all data
			list = chatbotMapper.getApiList(params);
			tot = chatbotMapper.getApiListCount(params);
			out.setTotalCount(tot);
			out.setList(list);
		} catch(Exception e) {
			logger.warn("getBqaApiList/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return out;
	}

	@Transactional(rollbackFor = { Exception.class })
	public long setBqaApi(String respCode, int rcId, String userId) throws BizChatbotException{
		logger.info("setBqaApi/{},{},{}", respCode, rcId, userId);

		QaRichVo qaRichVo = null;
		long nRet = 0;

		try {
			qaRichVo = richMapper.getQaRich(respCode);
			if(qaRichVo != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_API.equals(qaRichVo.getContentTypeCd())) {
					throw new Exception("Invalid Rich Content Type " + qaRichVo.getContentTypeCd());
				}

				QaRichVo param = new QaRichVo();
				param.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_API);
				param.setRcId(rcId);
				param.setUpdatorId(userId);
				param.setRespCode(respCode);

				nRet = richMapper.updateQaRich(param);
			} else {
				throw new Exception("QaRich Not Found");
			}
		}catch(Exception e) {
			logger.warn("setBqaApi/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return nRet;
	}

	@Transactional(rollbackFor = { Exception.class })
	public long addSingleButton(String respCode, int btnOrder, String title, String utter, String updatorId) throws BizChatbotException{
		logger.info("addSingleButton/{},{},{},{},{}", respCode, btnOrder, title, utter, updatorId);
		
		long nRet = 0;
		QaRichVo qaRichVo = null;
		HashMap<String, Object> params = new HashMap<>();

		try {
			qaRichVo = richMapper.getQaRich(respCode);
			if(qaRichVo != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_BUTTON.equals(qaRichVo.getContentTypeCd())) {
					throw new Exception("Invalid Rich Content Type " + qaRichVo.getContentTypeCd());
				}

				params.put("rcId", qaRichVo.getRcId());
				params.put("title", title);
				params.put("userUtter", utter);
				if(btnOrder > 0) {
					params.put("btnOrder", btnOrder);//dummy value
				}
				params.put("creatorId", updatorId);
				params.put("updatorId", updatorId);
				if(btnOrder > 0) {
					nRet = chatbotMapper.updateSingleButton(params);
					logger.debug("updated button count:{}", nRet);
				}else {
					nRet = chatbotMapper.addSingleButton(params);
					logger.debug("added button count:{}", nRet);
				}
			} else {
				throw new Exception("QaRich Not Found");
			}
		}catch(Exception e) {
			logger.warn("addSingleButton/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return nRet;
	}

	@Transactional(rollbackFor = { Exception.class })
	public long deleteSingleButton(String respCode, int btnOrder) throws BizChatbotException{
		logger.info("deleteSingleButton/{},{}", respCode, btnOrder);

		long nRet = 0;
		QaRichVo qaRichVo = null;
		HashMap<String, Object> params = new HashMap<>();

		try {
			qaRichVo = richMapper.getQaRich(respCode);
			if(qaRichVo != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_BUTTON.equals(qaRichVo.getContentTypeCd())) {
					throw new BizChatbotException("Invalid Rich Content Type " + qaRichVo.getContentTypeCd());
				}

				params.put("rcId", qaRichVo.getRcId());
				params.put("btnOrder",btnOrder);

				nRet = chatbotMapper.deleteSingleButton(params);
				logger.debug("deleted button count:{}", nRet);
			} else {
				throw new Exception("QaRich Not Found");
			}
		}catch(Exception e) {
			logger.warn("deleteSingleButton/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return nRet;
	}


	@Transactional(rollbackFor = { Exception.class })
	public long addImage(String respCode, String imgText, String imgLink, String userId, HttpServletRequest request) throws BizChatbotException{
		logger.info("addImage/{},{},{},{}", respCode, imgText, imgLink, userId);

		long nRet = 0;
		QaRichVo qaRichVo = null;
		HashMap<String, Object> params = new HashMap<>();

		try {
			qaRichVo = richMapper.getQaRich(respCode);
			if(qaRichVo != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_IMAGE.equals(qaRichVo.getContentTypeCd())) {
					throw new Exception("Invalid Rich Content Type " + qaRichVo.getContentTypeCd());
				}

				String savedFile = "";
				if(request != null) {
					//returns filename only (not path)
					savedFile = imgUtils.SaveFile(request, "bqa", qaRichVo.getContentTypeCd(), qaRichVo.getRcId(), 0);
				}

				// file is not imported
				if(StringUtils.isEmpty(savedFile)) {
					ChatbotRichImage img = rich.getRichImage(qaRichVo.getRcId());
					if(img != null && !StringUtils.isEmpty(img.getImagePath())) {
						savedFile = img.getImagePath();
					}
				}

				params.clear();
				params.put("rcId", qaRichVo.getRcId());
				params.put("imagePath", savedFile);
				params.put("imageText", imgText);
				params.put("linkUrl", imgLink);
				params.put("creatorId", userId);
				params.put("updatorId", userId);

				nRet = chatbotMapper.insertRichImage(params);
				logger.debug("updated image count:{}", nRet);

			} else {
				throw new Exception("QaRich Not Found");
			}
		}catch(Exception e) {
			logger.warn("addImage/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return nRet;
	}


	@Transactional(rollbackFor = { Exception.class })
	public long addCarousel(String respCode, String imgLink, String userId, HttpServletRequest request) throws BizChatbotException{
		logger.info("addCarousel/{},{},{}", respCode, imgLink, userId);

		long nRet = 0;
		QaRichVo qaRichVo = null;
		HashMap<String, Object> params = new HashMap<>();

		try {
			qaRichVo = richMapper.getQaRich(respCode);
			if(qaRichVo != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL.equals(qaRichVo.getContentTypeCd())) {
					throw new Exception("Invalid Rich Content Type " + qaRichVo.getContentTypeCd());
				}

				//long cnt = chatbotMapper.getCarouselCount(respRel.getIfId());

				//returns filename only (not path)
				List<String> savedFiles = imgUtils.SaveFileMulti(request, "bqa", qaRichVo.getContentTypeCd(), qaRichVo.getRcId());
				MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
				Map<String, String[]> multiParams = multipartHttpServletRequest.getParameterMap();
				String[] titles = multiParams.get("title");
				String[] subtitles = multiParams.get("subtitle");
				String[] utters = multiParams.get("utter");
				String[] orders = multiParams.get("carousel_order");
				String[] hasfiles = multiParams.get("hasfile");

				ArrayList<Integer> emptyIndex = new ArrayList<>();
				if(hasfiles != null) {
					for(int i=0;i<hasfiles.length;i++) {
						if(hasfiles[i]=="0") {
							emptyIndex.add(i);
						}
					}
					if(emptyIndex.size() > 0) {
						for(Integer idx:emptyIndex) {
							savedFiles.add(idx,"");
						}
					}
				}

//				if(cnt != titles.length) {
//					cnt = chatbotMapper.deleteCarouselById(respRel.getIfId());
//					logger.debug("reset carousel, cnt:{}",cnt);
//				}

				params.clear();
				if(savedFiles != null) {
					for(int idx = 0;idx < titles.length; idx++) {
						params.put("rcId", qaRichVo.getRcId());
						params.put("carouselOrder", orders[idx]);
						try {
							params.put("imagePath", savedFiles.get(idx));
						}catch(Exception e) {
							params.put("imagePath", "");
						}
						params.put("title", titles[idx]);
						params.put("subTitle", subtitles[idx]);
						params.put("userUtter", utters[idx]);
						params.put("creatorId", userId);
						params.put("updatorId", userId);

						if(StrUtils.getNum(orders[idx], 0) > 0) {
							nRet += chatbotMapper.updateSingleCarousel(params);
						}else {
							nRet += chatbotMapper.insertRichCarousel(params);
						}
						logger.debug("updated carousel count:{}", nRet);
					}
				}
			} else {
				throw new Exception("QaRich Not Found");
			}
		}catch(Exception e) {
			logger.warn("addCarousel/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return nRet;
	}

	@Transactional(transactionManager="chatbotTransactionManager")
	public long deleteCarousel(String respCode, int carouselOrder) throws BizChatbotException{
		logger.info("deleteCarousel/{},{}", respCode, carouselOrder);

		long nRet = 0;
		QaRichVo qaRichVo = null;
		HashMap<String, Object> params = new HashMap<>();

		try {
			qaRichVo = richMapper.getQaRich(respCode);
			if(qaRichVo != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL.equals(qaRichVo.getContentTypeCd())) {
					throw new Exception("Invalid Rich Content Type " + qaRichVo.getContentTypeCd());
				}
				params.clear();
				params.put("rcId", qaRichVo.getRcId());
				params.put("carouselOrder",carouselOrder);

				nRet = chatbotMapper.deleteCarouselByPos(params);
				logger.debug("deleted carousel count:{}", nRet);
			} else {
				throw new Exception("QaRich Not Found");
			}
		}catch(Exception e) {
			logger.warn("deleteCarousel/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		return nRet;
	}

	public List<Map<String, Object>> uploadBqaRichExcelData(String userId, MultipartFile file) throws  BizChatbotException {

		Workbook workbook = ExcelUtils.getExcelWorkBook(file);
		Sheet sheet = (Sheet) workbook.getSheetAt(0);

		int rowCount = sheet.getPhysicalNumberOfRows();
		int lastCellNum = 0;

		List<Map<String, Object>> list = new ArrayList<>();
		try {

			for (int i = 1; i < rowCount; i++) {
				Row row = sheet.getRow(i);

				if (row != null) {
					Map<String, Object> paramMap = new HashMap<>();
					paramMap.put("userId", userId);
					paramMap.put("answerId", Integer.parseInt(ExcelUtils.getCellValue(row.getCell(0)).trim()));

					String type = ExcelUtils.getCellValue(row.getCell(1)).trim();
					paramMap.put("type", type);
					if(StringUtils.equals(type, "BUTTON")) {
						paramMap.put("btnOrder", Integer.parseInt(ExcelUtils.getCellValue(row.getCell(2)).trim()));
						paramMap.put("btnTitle", ExcelUtils.getCellValue(row.getCell(3)).trim());
						paramMap.put("btnUserUtter", ExcelUtils.getCellValue(row.getCell(4)).trim());
					} else if(StringUtils.equals(type, "IMAGE")) {
						paramMap.put("imgText", ExcelUtils.getCellValue(row.getCell(5)).trim());
						paramMap.put("imgLinkUrl", ExcelUtils.getCellValue(row.getCell(6)).trim());
					} else if(StringUtils.equals(type, "API")) {
						paramMap.put("apiId", Integer.parseInt(ExcelUtils.getCellValue(row.getCell(7)).trim()));
					} else {
						paramMap = null;
					}

					list.add(paramMap);
				}
			}

		}catch (Exception e){
			throw new BizChatbotException(ERR_CODE_EXCEL_UPLOAD_ERROR, ERR_MSG_EXCEL_UPLOAD_ERROR);
		}

		return list;
	}

//	@Transactional(rollbackFor = { Exception.class })
	public int insertBqaRichExcelfile(List<Map<String, Object>> list) throws BizChatbotException {
		logger.debug("Service insertBqaRichExcelfile list", list);
		int resultCount = 0;

		try {
			for (int i=0; i < list.size(); i++) {
				Map<String, Object> paramMap = list.get(i);

				// type값이 잘못입력된 경우는 skip
				if(paramMap == null) {
					continue;
				}

				String userId = (String) paramMap.get("userId");
				/*int questionId = (Integer) paramMap.get("questionId");*/
				int answerId = (Integer) paramMap.get("answerId");
				String type = (String) paramMap.get("type");
				String contentTypeCd = null;
				switch(type) {
					case "BUTTON":
						contentTypeCd = IChatbotConst.RICH_CONTENT_TYPE_BUTTON;
						break;
					case "IMAGE":
						contentTypeCd = IChatbotConst.RICH_CONTENT_TYPE_IMAGE;
						break;
//					case "CAROUSEL":
//						contentTypeCd = IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL;
//						break;
					case "API":
						contentTypeCd = IChatbotConst.RICH_CONTENT_TYPE_API;
						break;
					default:
						break;
				}

				QaAnswerVo answerVo = answerMapper.getAnswer(paramMap);
				String respCode = answerVo.getRespCode();
				if(StringUtils.isEmpty(respCode)) {
					respCode = answerVo.getDomainId() + "_" + answerVo.getAnswerId();
					this.insertBqaRichContent(respCode, contentTypeCd, userId);

					// qa_answer update
					answerVo.setRespCode(respCode);
					answerMapper.update(answerVo);
				}

				switch(contentTypeCd) {
					case IChatbotConst.RICH_CONTENT_TYPE_BUTTON:
						this.addSingleButton(respCode, (Integer) paramMap.get("btnOrder"), (String) paramMap.get("btnTitle"), (String) paramMap.get("btnUserUtter"), userId);
						break;
					case IChatbotConst.RICH_CONTENT_TYPE_IMAGE:
						this.addImage(respCode, (String) paramMap.get("imgText"), (String) paramMap.get("imgLinkUrl"), userId, null);
						break;
//					case IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL:
//						nRet = chatbotMapper.deleteCarouselById(qaRichVo.getRcId());
//						break;
					case IChatbotConst.RICH_CONTENT_TYPE_API:
						this.setBqaApi(respCode, (Integer) paramMap.get("apiId"), userId);
						break;
					default:
						throw new Exception("Invalid rich content type: "+contentTypeCd);
				}

				resultCount++;
			}
		} catch (Exception e) {
			logger.warn(e.toString(), e);
			throw new BizChatbotException(ERR_CODE_EXCEL_UPLOAD_ERROR, ERR_MSG_EXCEL_UPLOAD_ERROR);
		}
		return resultCount;
	}


    /**
     * multi-part 안쓴버전..
     * 테스트안해봄...나중을 위해..
     * 지우지는 않았음...
     */
/*    public int insertBqaRichExcelfile(List<RichExcelVo> list) throws BizChatbotException {
        logger.debug("Service insertBqaRichExcelfile list", list);
        int resultCount = 0;

        try {
            for (int i=0; i < list.size(); i++) {
                RichExcelVo vo = list.get(i);

                // type값이 잘못입력된 경우는 skip
                if(vo == null) {
                    continue;
                }

                String userId =  vo.getUserId();
                Long questionId = vo.getQuestionId();
                String type =  vo.getType();
                String contentTypeCd = null;
                switch(type) {
                    case "BUTTON":
                        contentTypeCd = IChatbotConst.RICH_CONTENT_TYPE_BUTTON;
                        break;
                    case "IMAGE":
                        contentTypeCd = IChatbotConst.RICH_CONTENT_TYPE_IMAGE;
                        break;
//					case "CAROUSEL":
//						contentTypeCd = IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL;
//						break;
                    case "API":
                        contentTypeCd = IChatbotConst.RICH_CONTENT_TYPE_API;
                        break;
                    default:
                        break;
                }

                Map<String, Object> paramMap = new HashMap<>();
                paramMap.put("questionId", questionId);

                QaAnswerVo answerVo = answerMapper.getAnswer(paramMap);
                String respCode = answerVo.getRespCode();
                if(StringUtils.isEmpty(respCode)) {
                    respCode = questionId + "_" + answerVo.getAnswerId();
                    this.insertBqaRichContent(respCode, contentTypeCd, userId);

                    // qa_answer update
                    answerVo.setRespCode(respCode);
                    answerMapper.update(answerVo);
                }

                switch(contentTypeCd) {
                    case IChatbotConst.RICH_CONTENT_TYPE_BUTTON:
                        this.addSingleButton(respCode, vo.getBtnOrder(), vo.getBtnTitle(), vo.getBtnUtter(), userId);
                        break;
                    case IChatbotConst.RICH_CONTENT_TYPE_IMAGE:
                        this.addImage(respCode, vo.getImgText(), vo.getImgLinkUrl(), userId, null);
                        break;
//					case IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL:
//						nRet = chatbotMapper.deleteCarouselById(qaRichVo.getRcId());
//						break;
                    case IChatbotConst.RICH_CONTENT_TYPE_API:
                        this.setBqaApi(respCode, vo.getApiId(), userId);
                        break;
                    default:
                        throw new Exception("Invalid rich content type: "+contentTypeCd);
                }

                resultCount++;
            }
        } catch (Exception e) {
            logger.warn(e.toString(), e);
            throw new BizChatbotException(ERR_CODE_EXCEL_UPLOAD_ERROR, ERR_MSG_EXCEL_UPLOAD_ERROR);
        }
        return resultCount;
    }*/

}
