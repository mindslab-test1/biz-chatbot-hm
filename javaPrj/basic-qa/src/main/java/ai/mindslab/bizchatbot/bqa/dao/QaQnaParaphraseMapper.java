package ai.mindslab.bizchatbot.bqa.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.bqa.dao.data.QaParaphraseMainWordVo;
import ai.mindslab.bizchatbot.bqa.dao.data.QaParaphraseRelationVo;
import ai.mindslab.bizchatbot.bqa.dao.data.QaParaphraseWordVo;
import ai.mindslab.bizchatbot.bqa.dao.data.QaQnaParaphraseVo;
@Mapper
public interface QaQnaParaphraseMapper {
	
	public List<QaQnaParaphraseVo> getsParaphrase();
	public int insertMain(QaParaphraseMainWordVo qaParaphraseMainWordVo);
	public int insertParaphrase(QaParaphraseWordVo qaParaphraseWordVo);
	public int insertRelation(QaParaphraseRelationVo qaParaphraseRelationVo);
}
