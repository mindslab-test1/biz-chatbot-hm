package ai.mindslab.bizchatbot.bqa.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.bqa.dao.data.QaExcelVo;

@Mapper
public interface QaExcelMapper {

	public List<QaExcelVo> getsExcel(Map<String, Object> paramMap);
}
