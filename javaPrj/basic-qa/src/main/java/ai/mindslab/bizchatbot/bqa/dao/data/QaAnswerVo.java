package ai.mindslab.bizchatbot.bqa.dao.data;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.utils.DateUtils;
import ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao.data.QaRichVo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("serial")
@JsonIgnoreProperties({"createdDtm","updatedDtm","creatorId","updatorId"})
public class QaAnswerVo implements Serializable {

	@Override
	public String toString() {
		return "QaAnswerVo [answerId=" + answerId + ", answer=" + answer + ", questionId=" + questionId + ", useYn="
				+ useYn +  ", respCode=" + respCode + "]";
	}
	private int answerId;
	private String answer;
	private int questionId;
	private String useYn;
	private String respCode;

	private String createdDtm;
	private String updatedDtm;
	private String creatorId;
	private String updatorId;

	private QaRichVo qaRichVo;
	private ChatbotRichContent richContent;
	private ChatbotApiInfo apiInfo;

	private int domainId;

	//QQQ
	private int usedDomainId;
	private String usedDomainName;

	private String   teamCode;
	private String   userId;

	public int getAnswerId() {
		return answerId;
	}
	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}
	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRespCode() {
		return respCode;
	}
	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}
	public String getUpdatorId() {
		return updatorId;
	}
	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getCreatedDtm() {
		return DateUtils.convertUtcToLocalDate(createdDtm);
	}
	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}
	public String getUpdatedDtm() {
		return  DateUtils.convertUtcToLocalDate(updatedDtm);
	}
	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public QaRichVo getQaRichVo() {
		return qaRichVo;
	}
	public void setQaRichVo(QaRichVo qaRichVo) {
		this.qaRichVo = qaRichVo;
	}
	public ChatbotRichContent getRichContent() {
		return richContent;
	}
	public void setRichContent(ChatbotRichContent richContent) {
		this.richContent = richContent;
	}
	public ChatbotApiInfo getApiInfo() {
		return apiInfo;
	}
	public void setApiInfo(ChatbotApiInfo apiInfo) {
		this.apiInfo = apiInfo;
	}
	public int getUsedDomainId() {
		return usedDomainId;
	}
	public void setUsedDomainId(int usedDomainId) {
		this.usedDomainId = usedDomainId;
	}
	public String getUsedDomainName() {
		return usedDomainName;
	}
	public void setUsedDomainName(String usedDomainName) {
		this.usedDomainName = usedDomainName;
	}

	public int getDomainId() {
		return domainId;
	}

	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}

    public String getTeamCode() {
        return teamCode;
    }

    public void setTeamCode(String teamCode) {
        this.teamCode = teamCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
