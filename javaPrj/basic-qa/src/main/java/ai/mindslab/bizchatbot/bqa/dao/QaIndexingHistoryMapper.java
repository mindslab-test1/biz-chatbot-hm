package ai.mindslab.bizchatbot.bqa.dao;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.bqa.dao.data.QaIndexingHistoryVo;

@Mapper
public interface QaIndexingHistoryMapper {

	public int insert(QaIndexingHistoryVo qaIndexingHistoryVo);
	
	public int update(QaIndexingHistoryVo qaIndexingHistoryVo);
}
