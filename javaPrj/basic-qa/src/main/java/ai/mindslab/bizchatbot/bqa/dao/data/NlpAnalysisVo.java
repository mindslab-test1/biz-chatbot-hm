package ai.mindslab.bizchatbot.bqa.dao.data;

import java.util.List;

public class NlpAnalysisVo {

	private String text;
	private int textSeq;
	private List<NlpNesVo> nesList;
	private List<NlpMorpVo> morpList;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getTextSeq() {
		return textSeq;
	}
	public void setTextSeq(int textSeq) {
		this.textSeq = textSeq;
	}
	public List<NlpMorpVo> getMorpList() {
		return morpList;
	}
	public void setMorpList(List<NlpMorpVo> morpList) {
		this.morpList = morpList;
	}
	public List<NlpNesVo> getNesList() {
		return nesList;
	}
	public void setNesList(List<NlpNesVo> nesList) {
		this.nesList = nesList;
	}
}
