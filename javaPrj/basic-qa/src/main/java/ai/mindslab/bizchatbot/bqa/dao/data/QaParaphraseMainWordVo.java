package ai.mindslab.bizchatbot.bqa.dao.data;

import java.io.Serializable;
import java.sql.Date;

@SuppressWarnings("serial")
public class QaParaphraseMainWordVo implements Serializable {
	
	private int mainWordId;
	private String mainWord;
	private String useYn;
	public int getMainWordId() {
		return mainWordId;
	}
	public void setMainWordId(int mainWordId) {
		this.mainWordId = mainWordId;
	}
	public String getMainWord() {
		return mainWord;
	}
	public void setMainWord(String mainWord) {
		this.mainWord = mainWord;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

}
