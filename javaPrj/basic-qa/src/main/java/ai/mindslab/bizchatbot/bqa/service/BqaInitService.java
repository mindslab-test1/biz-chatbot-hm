package ai.mindslab.bizchatbot.bqa.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.bizchatbot.bqa.dao.QaIndexMapper;
import ai.mindslab.bizchatbot.bqa.dao.QaQnaParaphraseMapper;
import ai.mindslab.bizchatbot.bqa.dao.QaQuestionMapper;
import ai.mindslab.bizchatbot.bqa.dao.data.QaIndexVo;
import ai.mindslab.bizchatbot.bqa.dao.data.QaQnaParaphraseVo;
import ai.mindslab.bizchatbot.bqa.dao.data.QaQuestionVo;
import ai.mindslab.bizchatbot.bqa.nlp.NlpAnalyzeClient;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.common.LangOuterClass.LangCode;

@Service
public class BqaInitService {

	private Logger logger = LoggerFactory.getLogger(BqaInitService.class);

	int limit = 0;

	BqaInitService(@Value("${maum.chunk.count:100000}") int limit) {
		this.limit = limit;
	}

	@Autowired
	private QaQuestionMapper qaQuestionMapper;

	@Autowired
	private QaIndexMapper indexMapper;

	@Autowired
	private QaQnaParaphraseMapper qnaParaphrase;

	@Autowired
	private NlpAnalyzeClient nlpAnalyzeClient;

	@Transactional(rollbackFor = { Exception.class })
	public String init() {

		try {
			if (this.limit <= 0)
				this.limit = 100000;
			
			int totalCount = qaQuestionMapper.getCount();
			int delete = indexMapper.deleteAll();
			logger.info("indexMapper Delete count = {}", delete);
			// 배치 로직
			if (totalCount > 0 ) {

				int totalPage = (totalCount / limit) + (totalCount % limit == 0 ? 0 : 1);

				for (int page = 0; page < totalPage; page++) {

					List<QaQuestionVo> qaQuestionList = gets(page, limit);
					List<QaIndexVo> qaIndexList = addParaphraseList(qaQuestionList);
					StreamObserver<Document> responseObserver = new StreamObserver<Document>() {

						int index = 0;

						@Override
						public void onNext(Document document) {

							Map<String,Object> map = nlpAnalyzeClient.modify(document.getSentencesList());
							
							String questionMorph = map.get("questionMorph").toString();
							int morphCount = (Integer)map.get("morphCount");
							qaIndexList.get(index).setMorphCount(morphCount);
							qaIndexList.get(index).setQuestionMorph(questionMorph); // 분석 결과 삽입
							logger.info("question {}, questionMorph , {}", qaIndexList.get(index).getQuestion(),
									questionMorph);
							index++;
						}

						@Override
						public void onError(Throwable t) {

							logger.error("analyzeMultiple Failed: {}", Status.fromThrowable(t));
							nlpAnalyzeClient.countDownLatch();
						}

						@Override
						public void onCompleted() {

							nlpAnalyzeClient.countDownLatch();
							int result = insertIndexList(qaIndexList);

							logger.info("insertIndexList result count: {}", result);
						}
					};

					requestAnalyze(responseObserver, qaIndexList);
				}
			}

		} catch (Exception e) {
			logger.error("init error", e);
			throw e;
		}
		return "loading";
	};

	public void requestAnalyze(StreamObserver<Document> responseObserver, List<QaIndexVo> qaIndexList) {

		StreamObserver<InputText> requestObserver = nlpAnalyzeClient.analyzeMultiple(responseObserver);

		for (int i = 0; i < qaIndexList.size(); ++i) {

			String question = qaIndexList.get(i).getQuestion();
			InputText inputText = InputText.newBuilder().setText(question).setLang(LangCode.kor).setSplitSentence(true)
					.setUseTokenizer(true).setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
					.setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();

			requestObserver.onNext(inputText);
		}
		requestObserver.onCompleted();
	}

	// index db insert 요청
	public int insertIndexList(List<QaIndexVo> qaIndexList) {

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("list", qaIndexList);

		int result = indexMapper.insertList(paramMap);

		return result;
	}

	public List<QaQuestionVo> gets(int page, int pgSize) {

		int startIndex = page * pgSize;
//		int startIndex = (page - 1) * pgSize;
		
		Map<String, Object> paramMap = new HashMap<String, Object>();

		paramMap.put("startIndex", startIndex);
		paramMap.put("pgSize", pgSize);
		
		return qaQuestionMapper.gets(paramMap);
	}

	public List<QaIndexVo> addParaphraseList(List<QaQuestionVo> qaQuetionList) {

		List<QaIndexVo> qaIndexList = new ArrayList<QaIndexVo>();
		List<QaQnaParaphraseVo> paraphraseList = qnaParaphrase.getsParaphrase();

		// ALL Q&A while
		for (int i = 0; i < qaQuetionList.size(); i++) {
			int seq = 1;
			qaIndexList.add(new QaIndexVo(qaQuetionList.get(i), seq, qaQuetionList.get(i).getMainYn()));
			seq++;
			// ALL Paraphrase while
			for (int j = 0; j < paraphraseList.size(); j++) {

				String question = qaQuetionList.get(i).getQuestion();
				String mainWord = paraphraseList.get(j).getMainWord();
				StringBuilder questionBuilder = new StringBuilder(question);
				int lastIndex = StringUtils.lastIndexOf(question, mainWord);

				if (lastIndex > -1 && StringUtils.endsWith(question, mainWord)) {

					String[] paraphraseArray = StringUtils.split(paraphraseList.get(j).getParaphraseWord(), ",");

					// Same Paraphrase while
					for (int x = 0; x < paraphraseArray.length; x++) {

						StringBuilder paraphraseBuilder = questionBuilder.replace(lastIndex, questionBuilder.length(),
								paraphraseArray[x]);
						qaQuetionList.get(i).setQuestion(paraphraseBuilder.toString());
						logger.debug("paraphrase {}", paraphraseBuilder.toString());

						qaIndexList.add(new QaIndexVo(qaQuetionList.get(i), seq, "N"));
						seq++;
					}
				}
			}
		}

		return qaIndexList;
	}

}
