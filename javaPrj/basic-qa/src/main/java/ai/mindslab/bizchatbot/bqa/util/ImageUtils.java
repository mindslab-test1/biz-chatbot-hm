package ai.mindslab.bizchatbot.bqa.util;

import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.activation.FileTypeMap;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;

@Component("imageUtils")
public class ImageUtils {

	@Value("${rich.image.path}")
	private String imageRoot;
	
	@Value("${rich.image.url}")
	private String adminImageUrl;
	
//	@Autowired
//	private ServletContext servletContext;
	
	/**
	 * Image Root 이후의 Path를 리턴한다.
	 * @param request
	 * @param domain
	 * @param richTypeCd
	 * @param rcId
	 * @param idx
	 * @return
	 * @throws Exception
	 */
	public String SaveFile(HttpServletRequest request, String domain, String richTypeCd, int rcId, int idx) throws Exception{
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
    	Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
    	MultipartFile multipartFile = null;    	
    	
    	String strPath = imageRoot + "/temp";
    	String strReturn = "";
    	File file = new File(strPath);
        if(file.exists() == false){
        	file.mkdirs();
        }
        if(iterator.hasNext()) {
			multipartFile = multipartHttpServletRequest.getFile(iterator.next());
			if (multipartFile.isEmpty() == false) {
				file = new File(strPath + multipartFile.getOriginalFilename());
				multipartFile.transferTo(file);

				//move to img directory
				String destDir = "";
				if (IChatbotConst.RICH_CONTENT_TYPE_IMAGE.equals(richTypeCd)) {
					destDir = imageRoot + "/" + IChatbotConst.DIR_RICH_IMG + "/" + domain + "/";

				} else if (IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL.equals(richTypeCd)) {
					destDir = imageRoot + "/" + IChatbotConst.DIR_RICH_CAROUSEL + "/" + domain + "/";
				} else {
					destDir = imageRoot + "/" + "etc/" + domain + "/";
				}

				String newFile = destDir + genFileName(richTypeCd, rcId, idx, FilenameUtils.getExtension(file.getName()));
				File destFile = new File(newFile);
				if (destFile.exists()) {
					//rename
					FileUtils.moveFile(destFile, new File(newFile + "_" + fileDupSurffix()));
				}
				FileUtils.moveFile(file, destFile);

				strReturn = newFile.substring(imageRoot.length());

				//QQQ sync to other servers?

			}
		}
    	
    	return strReturn;
	}
	
	public List<String> SaveFileMulti(HttpServletRequest request, String domain, String richTypeCd, int rcId) throws Exception{
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
    	//Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
		List<MultipartFile> files = multipartHttpServletRequest.getFiles("imagefile");
    	Map<String, String[]> params = multipartHttpServletRequest.getParameterMap();
    	String[] hasfiles = params.get("hasfile");
    	MultipartFile multipartFile = null;    	
    	
    	String strPath = imageRoot + "/temp";
    	String strReturn = "";
    	File file = new File(strPath);
        if(file.exists() == false){
        	file.mkdirs();
        }
        
        int idx = 0;
        ArrayList<String> names = new ArrayList<>();
        //while(iterator.hasNext()) {
        for(int i=0;i<hasfiles.length;i++) {
        	if("0".equals(hasfiles[i])) {
        		names.add("");
        		continue;
        	}
//	        multipartFile = multipartHttpServletRequest.getFile(iterator.next());
        	multipartFile = files.get(i);
	    	if(multipartFile.isEmpty() == false){    		
		        file = new File(strPath + multipartFile.getOriginalFilename());
				multipartFile.transferTo(file);
				
				//move to img directory
				String destDir = "";
				if(IChatbotConst.RICH_CONTENT_TYPE_IMAGE.equals(richTypeCd)) {
					destDir = imageRoot + "/" + IChatbotConst.DIR_RICH_IMG + "/" + domain + "/";
				}else if(IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL.equals(richTypeCd)) {
					destDir = imageRoot + "/" + IChatbotConst.DIR_RICH_CAROUSEL + "/" + domain + "/";
				}else {
					destDir = imageRoot + "/" + "etc/" + domain + "/";
				}
				
				//String newFile = destDir+genFileName(richTypeCd, rcId, ++idx, FilenameUtils.getExtension(file.getName()));
				String newFile = destDir+genFileName(richTypeCd, rcId, (i+1), FilenameUtils.getExtension(file.getName()));
				File destFile = new File(newFile);
				if(destFile.exists()) {
					//rename
					FileUtils.moveFile(destFile, new File(newFile+"_"+fileDupSurffix()));
				}
				FileUtils.moveFile(file, destFile);
				
				strReturn = newFile.substring(imageRoot.length());
				
				names.add(strReturn);
	    	}
        }
        //QQQ sync to other servers?
    	return names;
	}
	
	private String fileDupSurffix() {
		return ""+Calendar.getInstance().getTime().getTime();
	}
	
	private String genFileName(String richTypeCd, int rcId, int idx, String ext) {
		return getTypeName(richTypeCd) + "_" + rcId+"_"+idx + "." + ext;
	}
	
	private String getTypeName(String richTypeCd) {
		switch(richTypeCd) {
		case IChatbotConst.RICH_CONTENT_TYPE_IMAGE:
			return "image";
		case IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL:
			return "carousel";
		default:
			return "etc";
		}
	}
	
	public ResponseEntity<byte[]> getImage(String fileName) throws Exception{
		File file = new File(imageRoot + "/" + fileName);
//		return ResponseEntity.ok()
//    			//.header("Content-Disposition", "attachment; filename=" +file.getName())
//                .contentType(MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(file)))
//                .body(Files.readAllBytes(file.toPath()));
		HttpHeaders headers = new HttpHeaders();
//	    InputStream in = servletContext.getResourceAsStream(imageRoot + "/" + fileName);
		FileInputStream in = new FileInputStream(file);
	    byte[] media = IOUtils.toByteArray(in);
	    headers.setCacheControl(CacheControl.noCache().getHeaderValue());
//	    headers.setCacheControl(CacheControl.maxAge(300,TimeUnit.SECONDS).getHeaderValue());
	    headers.setContentType(MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(file)));
	    
	    ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);
	    return responseEntity;
	}
	
	public String transferImagePath(String richTypeCd, String domain, String imgPath) throws Exception{
		if(StringUtils.isEmpty(imgPath)) {
			return "";
		}
		if(IChatbotConst.RICH_CONTENT_TYPE_IMAGE.equals(richTypeCd)) {
			return adminImageUrl + getTypeName(richTypeCd) + "/" + domain + "/" + imgPath.substring(imgPath.lastIndexOf("/"));
		}else if(IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL.equals(richTypeCd)) {
			return adminImageUrl + getTypeName(richTypeCd) + "/" + domain + "/" + imgPath.substring(imgPath.lastIndexOf("/"));
		}else {
			throw new Exception("Invalid Rich Content type: " + richTypeCd);
		}
	}
}
