package ai.mindslab.bizchatbot.bqa.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao.data.QaRichVo;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.bizchatbot.bqa.dao.CmDomainMapper;
import ai.mindslab.bizchatbot.bqa.dao.QaAnswerMapper;
import ai.mindslab.bizchatbot.bqa.dao.QaIndexMapper;
import ai.mindslab.bizchatbot.bqa.dao.data.CmDomainVo;
import ai.mindslab.bizchatbot.bqa.dao.data.NlpAnalysisVo;
import ai.mindslab.bizchatbot.bqa.dao.data.QaAnswerVo;
import ai.mindslab.bizchatbot.bqa.dao.data.QaDebugVo;
import ai.mindslab.bizchatbot.bqa.dao.data.QaIndexVo;
import ai.mindslab.bizchatbot.bqa.dao.data.QaQuestionVo;
import ai.mindslab.bizchatbot.bqa.nlp.NlpAnalyzeClient;
import ai.mindslab.bizchatbot.bqa.solr.BqaSolrClient;
import ai.mindslab.bizchatbot.commons.codes.IBasicQAConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseListObject;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;

@Service
public class BqaSearchService implements IRestCodes, IBasicQAConst, IChatbotConst {

	private Logger logger = LoggerFactory.getLogger(BqaSearchService.class);

	@Autowired
	private QaIndexMapper indexMapper;

	@Autowired
	private QaAnswerMapper answerMapper;

	@Autowired
	private CmDomainMapper domainMapper;

	@Autowired
	private BqaRichContentService bqaRichContentService;

	@Autowired
	private NlpAnalyzeClient nlpAnalyzeClient;

	@Autowired
	private BqaSolrClient bqaSolrClient;

	@Value("${bqa.search.flow.type : 1}")
	private int searchType;

	@Value("${bqa.search.random.result : 0}")
	private int randomFlag;
	
	@Value("${bqa.search.solror.range :2}")
	private int range;

	@Value("${solr.bqa.search.or.result.count:3}")
	int solrOrResultCount;
	
	public BaseResponse<Object> search(String question,int searchFlowType, int morphRange, int... domains) throws BizChatbotException {
		BaseResponse<Object> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		logger.info("search question{}, searchFlowType{}, morphRange{}, domains{}",question, searchFlowType,morphRange,domains );

		if(searchFlowType == 0) {
		}else{
			if(searchFlowType == SEARCH_TYPE_DB_SOLRAND_SOLROR) {
				if(morphRange == 0) {
				}else {
					range = morphRange;
				}
			}
			searchType = searchFlowType;
		}
		QaIndexVo qaIndexVo = null;
		QaAnswerVo qaAnswerVo = null;
		
		BaseListObject<QaIndexVo> iList= null;
		switch (this.searchType) {
		case SEARCH_TYPE_DB:
			qaIndexVo = searchByDb(question, domains);
			if (qaIndexVo == null || qaIndexVo.getIndexId() == 0) {
				throw new BizChatbotException(ERR_CODE_SEARCH_DB_FAILURE, ERR_MSG_SEARCH_DB_FAILURE);	
			}
			break;
		case SEARCH_TYPE_SOLRAND:
			qaIndexVo = searchBySolr(question, domains);
			if (qaIndexVo == null || qaIndexVo.getIndexId() == 0) {
				throw new BizChatbotException(ERR_CODE_SEARCH_SOLR_FAILURE, ERR_MSG_SEARCH_SOLR_FAILURE);	
			}
			break;
		case SEARCH_TYPE_DB_SOLRAND:
			qaIndexVo = searchByDb(question, domains);
			if (qaIndexVo == null) {
				qaIndexVo = searchBySolr(question, domains);
			}if (qaIndexVo == null  || qaIndexVo.getIndexId() == 0) {
				throw new BizChatbotException(ERR_CODE_SEARCH_DB_SOLR_FAILURE, ERR_MSG_SEARCH_DB_SOLR_FAILURE);	
			}
			break;
		case SEARCH_TYPE_DB_SOLRAND_SOLROR:
			qaIndexVo = searchByDb(question, domains);
			if (qaIndexVo == null) {
				qaIndexVo = searchBySolr(question, domains);
			}if (qaIndexVo == null  || qaIndexVo.getIndexId() == 0) {
				
				List<QaIndexVo> indexList = null;
				indexList = searchBySolrOr(question, range, domains);
				BaseListObject<QaIndexVo> obj = new BaseListObject<QaIndexVo>(indexList.size(), indexList);
				if(indexList.size() > 0) {
					resp.setData(obj);
					resp.setCode(ERR_CODE_SEARCH_SOLROR_SUCCESS);
					resp.setMsg(ERR_MSG_SEARCH_SOLROR_SUCCESS);
				}else {
					throw new BizChatbotException(ERR_CODE_SEARCH_SOLROR_FAILURE, ERR_MSG_SEARCH_SOLROR_FAILURE);
				}
				return resp;
			}
			break;
		default:
		}
		
		logger.debug("qaIndexVo = {}",qaIndexVo.toString());

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("questionId", qaIndexVo.getQuestionId());
		paramMap.put("randomFlag", randomFlag);
		qaAnswerVo = answerMapper.getAnswer(paramMap);
		
		//QQQ domain id & name
		if(qaIndexVo.getDomainId() > 0) {
			String usedDomainName = domainMapper.getUsedDomainName(qaIndexVo.getDomainId());
			if(usedDomainName == null) {
				usedDomainName ="";
			}
			qaAnswerVo.setUsedDomainId(qaIndexVo.getDomainId());
			qaAnswerVo.setUsedDomainName(usedDomainName);
		}
		
		resp.setData(qaAnswerVo);
		logger.debug("qaAnswerVo = {}",qaAnswerVo.toString());

		if(qaAnswerVo == null || qaAnswerVo.getAnswerId() == 0 ||  qaAnswerVo.getAnswer().isEmpty()) {
			throw new BizChatbotException(ERR_CODE_SEARCH_NO_ANSWER, ERR_MSG_SEARCH_NO_ANSWER);
		}

		// RichContent 설정 (Null check Validation도 함수에 포함)
		setRichContent(qaAnswerVo);
	
		return resp;
	}

	public BaseResponse<Object> searchForDebug(String question,int searchFlowType, int morphRange, int... domainIds) throws BizChatbotException {
		BaseResponse<Object> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		logger.info("searchForDebug question{}, searchFlowType{}, morphRange{}, domains{}",question, searchFlowType,morphRange,domainIds );
		
		if(searchFlowType == 0) {
		}else{
			if(searchFlowType == SEARCH_TYPE_DB_SOLRAND_SOLROR) {
				if(morphRange == 0) {
				}else {
					range = morphRange;
				}
			}
			searchType = searchFlowType;
		}
		
		switch (this.searchType) {
		case SEARCH_TYPE_DB:
			resp = searchByDbForDebug(question, domainIds);
			
			break;
		case SEARCH_TYPE_SOLRAND:
			resp = searchBySolrForDebug(question, domainIds);
			
			break;
		case SEARCH_TYPE_DB_SOLRAND:
			resp = searchByDbForDebug(question, domainIds);
			if (resp.getCode() != ERR_CODE_SUCCESS) {
				resp = searchBySolrForDebug(question, domainIds);
			}
			break;
		case SEARCH_TYPE_DB_SOLRAND_SOLROR:
			resp = searchByDbForDebug(question, domainIds);
			
			if (resp.getCode() != ERR_CODE_SUCCESS) {
				resp = searchBySolrForDebug(question, domainIds);
				if (resp.getCode() != ERR_CODE_SUCCESS) {
					resp = searchBySolrOrForDebug(question, range, domainIds);
				}
			}
			break;
		default:
		}
		QaDebugVo qaDebugVo = (QaDebugVo)resp.getData();
		
		/*domain info*/
//		Map<String, Object> paramD = new HashMap<String, Object>();
//		paramD.put("list", domainIds);
//		List<CmDomainVo> domainList = domainMapper.gets(paramD);
//		qaDebugVo.setDomainList(domainList);
		/*Search Type*/
		qaDebugVo.setSearchFlowType(searchType);
		/*nlp info*/
//		List<NlpAnalysisVo> nlpList = nlpAnalyzeClient.nlpTestAnalyze(question);	
//		qaDebugVo.setNlpList(nlpList);
		
		qaDebugVo.setMorphRange(range);
		qaDebugVo.setDebugYn("Y");
		qaDebugVo.setQuestion(question);
		logger.debug("QaDebugVo = {}",qaDebugVo);
	
		return resp;
	}
	
	
	private BaseResponse<Object> searchByDbForDebug(String question, int... domainIds) {
		BaseResponse<Object> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		QaDebugVo qaDebugVo = new QaDebugVo();
		logger.info("searchByDbForDebug question{}, domainIds {}",question, domainIds);
		try {

			Map<String, Object> paramMap = new HashMap<String, Object>();

			if(domainIds != null && domainIds[0] != 0) {
				paramMap.put("domain", 1);
			}else {
				paramMap.put("domain", 0);
			}
//			question = question.replaceAll(".,!?", "");
			paramMap.put("question", question);
			paramMap.put("domainIds", domainIds);
			paramMap.put("randomFlag", randomFlag);
			
			QaIndexVo qaIndexVo = indexMapper.search(paramMap);
			if (qaIndexVo == null || qaIndexVo.getIndexId() == 0) {
				logger.debug("qaIndexVo.getIndexId() == 0");
				resp.setCode(ERR_CODE_SEARCH_DB_FAILURE);
				resp.setMsg(ERR_MSG_SEARCH_DB_FAILURE);
				qaDebugVo.setStatus(SEARCH_RESULT_DB_FAIL);
			}else {
				List<QaAnswerVo> answerList = answerMapper.getsByQuestionId(qaIndexVo.getQuestionId());
				if(answerList.size() ==0) {
					logger.debug("answerList.size() == 0");
					resp.setCode(ERR_CODE_SEARCH_NO_ANSWER);
					resp.setMsg(ERR_MSG_SEARCH_NO_ANSWER);	
				}else {
					qaIndexVo.setAnswerList(answerList);
					List<QaIndexVo> indexList = new ArrayList<>();
					qaDebugVo.setUsedDomainId(qaIndexVo.getDomainId());
					String usedDomainName = domainMapper.getUsedDomainName(qaIndexVo.getDomainId());
					if(usedDomainName == null)
						usedDomainName ="";
					qaDebugVo.setUsedDomainName(usedDomainName);
					indexList.add(qaIndexVo);
					qaDebugVo.setList(indexList);
					Collections.shuffle(answerList);
					QaAnswerVo answer = answerList.get(0);
					qaDebugVo.setAnswer(answer.getAnswer());
					qaDebugVo.setAnswerId(answer.getAnswerId());
					qaDebugVo.setStatus(SEARCH_RESULT_DB_SUCCESS);

					setRichContentForDebug(qaDebugVo, answer);
				}
			}
			logger.debug("searchByDbForDebug qaIndexVo{}",qaIndexVo);
		} catch (Exception e) {

			logger.warn("DB search Error/{}", e);
			resp.setCode(ERR_CODE_SEARCH_DB_FAILURE);
			resp.setMsg(ERR_MSG_SEARCH_DB_FAILURE);
			qaDebugVo.setStatus(SEARCH_RESULT_DB_FAIL);
		}finally {
			logger.debug("searchByDbForDebug qaDebugVo{}",qaDebugVo);
			resp.setData(qaDebugVo);
		}
		return resp;
	}

	public QaIndexVo searchByDb(String question, int... domainIds) throws BizChatbotException {
		QaIndexVo qaIndexVo = null;
		logger.info("searchByDb question{}, domainIds {}",question,domainIds);
		try {

			Map<String, Object> paramMap = new HashMap<String, Object>();

			if(domainIds != null && domainIds[0] != 0) {
				paramMap.put("domain", 1);
			}else {
				paramMap.put("domain", 0);
			}
//			question = question.replaceAll(".,!?", "");
			paramMap.put("question", question);
			paramMap.put("domainIds", domainIds);
			paramMap.put("randomFlag", randomFlag);
			
			qaIndexVo = indexMapper.search(paramMap);
			logger.debug("searchByDb qaIndexVo{}",qaIndexVo);
		} catch (Exception e) {

			logger.warn("DB search Error/{}", e);
			throw new BizChatbotException(ERR_CODE_SEARCH_DB_FAILURE, ERR_MSG_SEARCH_DB_FAILURE);
		}
		return qaIndexVo;
	}

	public QaIndexVo searchBySolr(String question, int... domainIds) throws BizChatbotException {
		QaIndexVo qaIndexVo = null;
		logger.info("searchBySolr question{}, domainIds {}",question,domainIds);
		try {
			String questionMorph = nlpAnalyzeClient.analyze(question);
			if(questionMorph == null || questionMorph.length() == 0) {
				throw new BizChatbotException(ERR_CODE_SEARCH_NLP_RESULT_NOTHING_ERROR, ERR_MSG_SEARCH_NLP_RESULT_NOTHING_ERROR);	
			}
			qaIndexVo = bqaSolrClient.search(questionMorph, domainIds);
		} catch (SolrServerException e) {
			logger.warn("searchBySolr SolrServerException search Error/{}", e.getMessage());
			throw new BizChatbotException(ERR_CODE_SEARCH_SOLR_SYSTEM_ERROR, ERR_MSG_SEARCH_SOLR_SYSTEM_ERROR);
		} catch (IOException e) {
			logger.warn("searchBySolr IOException search Error/{}", e.getMessage());
			throw new BizChatbotException(ERR_CODE_SEARCH_SOLR_IO_ERROR, ERR_MSG_SEARCH_SOLR_IO_ERROR);
		} catch (Exception e) {
			logger.warn("searchBySolr Solr search Error/{}", e.getMessage());
			throw new BizChatbotException(ERR_CODE_SEARCH_SOLR_FAILURE, ERR_MSG_SEARCH_SOLR_FAILURE);
		}
		return qaIndexVo;
	}
	
	public BaseResponse<Object> searchBySolrForDebug(String question, int... domainIds) throws BizChatbotException {
		BaseResponse<Object> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		logger.info("searchBySolrForDebug question = {}, domainIds {}",question,domainIds);
		QaDebugVo qaDebugVo = null;
		try {
			String questionMorph = nlpAnalyzeClient.analyze(question);
			if(questionMorph == null || questionMorph.length() == 0) {
				throw new BizChatbotException(ERR_CODE_SEARCH_NLP_RESULT_NOTHING_ERROR, ERR_MSG_SEARCH_NLP_RESULT_NOTHING_ERROR);	
			}
			qaDebugVo = bqaSolrClient.searchForDebug(questionMorph, domainIds);

			if(qaDebugVo.getStatus() == SEARCH_RESULT_SOLRAND_FAIL) {
			
				resp.setCode(ERR_CODE_SEARCH_SOLR_FAILURE);
				resp.setMsg(ERR_MSG_SEARCH_SOLR_FAILURE);		
			}else {
				List<QaIndexVo> indexList = qaDebugVo.getList();
				if(indexList.size() > 0) {
					for(int i = 0 ; i < indexList.size() ; i++) {
						QaIndexVo qaIndexVo = indexList.get(i);
						List<QaAnswerVo> answerList = answerMapper.getsByQuestionId(qaIndexVo.getQuestionId());
						qaIndexVo.setAnswerList(answerList);
						if(qaIndexVo.getRes() == QaIndexVo.SEARCH_CODE_SUCCESS ) {
							qaDebugVo.setQuestionMorph(qaIndexVo.getQuestionMorph());
							qaDebugVo.setUsedDomainId(qaIndexVo.getDomainId());
							String usedDomainName = domainMapper.getUsedDomainName(qaIndexVo.getDomainId());
							if(usedDomainName == null)
								usedDomainName ="";
							qaDebugVo.setUsedDomainName(usedDomainName);
							Collections.shuffle(answerList);
							QaAnswerVo answer = answerList.get(0);
							qaDebugVo.setAnswerId(answer.getAnswerId());
							qaDebugVo.setAnswer(answer.getAnswer());

							setRichContentForDebug(qaDebugVo, answer);
						}
					}
					qaDebugVo.setList(indexList);
						
				}
			}
		} catch (BizChatbotException e){
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			qaDebugVo.setStatus(SEARCH_RESULT_SOLRAND_FAIL);
		} catch (SolrServerException e) {
			logger.warn("searchBySolrForDebug SolrServerException search Error/{}", e.getMessage());
			resp.setCode(ERR_CODE_SEARCH_SOLR_SYSTEM_ERROR);
			resp.setMsg(ERR_MSG_SEARCH_SOLR_SYSTEM_ERROR);
			qaDebugVo.setStatus(SEARCH_RESULT_SOLRAND_FAIL);
		} catch (IOException e) {
			logger.warn("searchBySolrForDebug IOException search Error/{}", e.getMessage());
			resp.setCode(ERR_CODE_SEARCH_SOLR_IO_ERROR);
			resp.setMsg(ERR_MSG_SEARCH_SOLR_IO_ERROR);
			qaDebugVo.setStatus(SEARCH_RESULT_SOLRAND_FAIL);
		} catch (Exception e) {
			logger.warn("searchBySolrForDebug Solr search Error/{}", e.getMessage());
			resp.setCode(ERR_CODE_SEARCH_SOLR_FAILURE);
			resp.setMsg(ERR_MSG_SEARCH_SOLR_FAILURE);
			qaDebugVo.setStatus(SEARCH_RESULT_SOLRAND_FAIL);
		}finally {
			resp.setData(qaDebugVo);
		}
		return resp;
	}
	
	public List<QaIndexVo> searchBySolrOr(String question, int range, int... domainIds) throws BizChatbotException {
		List<QaIndexVo> i = null;
		logger.info("searchBySolrOr question{}, domainIds {}",question,domainIds);
		try {
			String questionMorph = nlpAnalyzeClient.analyze(question);
			if(questionMorph == null || questionMorph.length() == 0) {
				throw new BizChatbotException(ERR_CODE_SEARCH_NLP_RESULT_NOTHING_ERROR, ERR_MSG_SEARCH_NLP_RESULT_NOTHING_ERROR);	
			}
			i = bqaSolrClient.searchOr(randomFlag, questionMorph, range, domainIds);
		} catch (BizChatbotException e){
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}catch (SolrServerException e) {
			logger.warn("searchBySolrOr SolrServerException search Error/{}", e.getMessage());
			throw new BizChatbotException(ERR_CODE_SEARCH_SOLR_SYSTEM_ERROR, ERR_MSG_SEARCH_SOLR_SYSTEM_ERROR);
		} catch (IOException e) {
			logger.warn("searchBySolrOr IOException search Error/{}", e.getMessage());
			throw new BizChatbotException(ERR_CODE_SEARCH_SOLR_IO_ERROR, ERR_MSG_SEARCH_SOLR_IO_ERROR);
		} catch (Exception e) {
			logger.warn("searchBySolrOr Solr search Error/{}", e.getMessage());
			throw new BizChatbotException(ERR_CODE_SEARCH_SOLR_FAILURE, ERR_MSG_SEARCH_SOLR_FAILURE);
		}
		return i;
	}
	
	public BaseResponse<Object> searchBySolrOrForDebug(String question, int morphRange, int... domainIds) throws BizChatbotException {
		BaseResponse<Object> resp = new BaseResponse<>(ERR_CODE_SEARCH_SOLROR_SUCCESS, ERR_MSG_SEARCH_SOLROR_SUCCESS);
		logger.info("searchBySolrForDebug question{}, domainIds {}",question,domainIds);
		QaDebugVo qaDebugVo = new QaDebugVo();
		try {
			String questionMorph = nlpAnalyzeClient.analyze(question);
			if(questionMorph == null || questionMorph.length() == 0) {
				throw new BizChatbotException(ERR_CODE_SEARCH_NLP_RESULT_NOTHING_ERROR, ERR_MSG_SEARCH_NLP_RESULT_NOTHING_ERROR);	
			}
			qaDebugVo = bqaSolrClient.searchOrForDebug(questionMorph, morphRange, domainIds);
			qaDebugVo.setQuestionMorph(questionMorph);
			logger.debug("searchBySolrForDebug qaDebugVo{}",qaDebugVo);
			List<QaIndexVo> indexList = qaDebugVo.getList(); 
			if(indexList.size() >0) {
				for(int i = 0 ; i < indexList.size() ; i++) {
					QaIndexVo qaIndexVo = indexList.get(i);
					List<QaAnswerVo> answerList = answerMapper.getsByQuestionId(qaIndexVo.getQuestionId());
					qaIndexVo.setAnswerList(answerList);
				}
				qaDebugVo.setList(indexList);
				qaDebugVo.setStatus(SEARCH_RESULT_SOLROR_SUCCESS);
			}else {
				resp.setCode(ERR_CODE_SEARCH_SOLROR_FAILURE);
				resp.setMsg(ERR_MSG_SEARCH_SOLROR_FAILURE);
				qaDebugVo.setStatus(SEARCH_RESULT_SOLROR_FAIL);	
			}
		} catch (BizChatbotException e){
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			qaDebugVo.setStatus(SEARCH_RESULT_SOLROR_FAIL);
		} catch (SolrServerException e) {
			logger.warn("searchBySolrOrForDebug SolrServerException search Error/{}", e.getMessage());
			resp.setCode(ERR_CODE_SEARCH_SOLR_SYSTEM_ERROR);
			resp.setMsg(ERR_MSG_SEARCH_SOLR_SYSTEM_ERROR);
			qaDebugVo.setStatus(SEARCH_RESULT_SOLROR_FAIL);
		} catch (IOException e) {
			logger.warn("searchBySolrOrForDebug IOException search Error/{}", e.getMessage());
			resp.setCode(ERR_CODE_SEARCH_SOLR_IO_ERROR);
			resp.setMsg(ERR_MSG_SEARCH_SOLR_IO_ERROR);
			qaDebugVo.setStatus(SEARCH_RESULT_SOLROR_FAIL);
		} catch (Exception e) {
			logger.warn("searchBySolrOrForDebug Solr search Error/{}", e.getMessage());
			resp.setCode(ERR_CODE_SEARCH_SOLROR_FAILURE);
			resp.setMsg(ERR_MSG_SEARCH_SOLROR_FAILURE);
			qaDebugVo.setStatus(SEARCH_RESULT_SOLROR_FAIL);
		}finally {
			resp.setData(qaDebugVo);
		}
		return resp;
	}

	public String nlpAnalyze(String text) throws BizChatbotException {
		logger.info("Service nlpAnalyze parameter text {}", text);

		String morph;
		try {
			String[] morpTypes = new String[] { "nc", "np", "nb", "nk", "nr" };
			morph = nlpAnalyzeClient.analyzeEx(text, morpTypes);
		} catch (Exception e) {
			logger.warn("nlpAnalyze Error/{}", e);
			throw new BizChatbotException(ERR_CODE_NLP_ANALYSIS_ERROR, ERR_MSG_NLP_ANALYSIS_ERROR);
		}
		return morph;
	}

	private void setRichContent(QaAnswerVo qaAnswerVo) throws BizChatbotException {
		String respCode = qaAnswerVo.getRespCode();

		try {
			if (!StringUtils.isEmpty(respCode)) {
				QaRichVo qaRichVo = bqaRichContentService.getBqaRichInfo(respCode);
				if(qaRichVo != null) {
					qaAnswerVo.setQaRichVo(qaRichVo);
					String type = qaRichVo.getContentTypeCd();
					if(IChatbotConst.RICH_CONTENT_TYPE_API.equals(type)) {
						ChatbotApiInfo apiInfo = bqaRichContentService.getBqaApiInfo(respCode);
						qaAnswerVo.setApiInfo(apiInfo);
					} else {
						ChatbotRichContent richContent = bqaRichContentService.getBqaRichContent(respCode);
						qaAnswerVo.setRichContent(richContent);
					}
				}
			}
		}catch(Exception e) {
			logger.warn("setRichContent/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
	}

	private void setRichContentForDebug(QaDebugVo qaDebugVo, QaAnswerVo qaAnswerVo) throws BizChatbotException  {

		try {
			// RichContent 설정 (Null check Validation도 함수에 포함)
			setRichContent(qaAnswerVo);
			qaDebugVo.setRespCode(qaAnswerVo.getRespCode());
			qaDebugVo.setQaRichVo(qaAnswerVo.getQaRichVo());
			qaDebugVo.setRichContent(qaAnswerVo.getRichContent());
			qaDebugVo.setApiInfo(qaAnswerVo.getApiInfo());
		} catch (Exception e) {
			logger.warn("setRichContentForDebug/{}",e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
	}
}
