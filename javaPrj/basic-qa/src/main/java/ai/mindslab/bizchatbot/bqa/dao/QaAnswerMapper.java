package ai.mindslab.bizchatbot.bqa.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.bqa.dao.data.QaAnswerVo;

@Mapper
public interface QaAnswerMapper {

	public int update(QaAnswerVo qaQnaVo);

	public int insertList(Map<String, Object> paramMap);

	public int updateUseYn(Map<String, Object> paramMap);

	public int getCountByDomain(Map<String, Object> paramMap);

	public QaAnswerVo getAnswer(Map<String, Object> paramMap);

	public List<QaAnswerVo> gets(Map<String, Object> paramMap);

	public List<QaAnswerVo> getsByQuestionId(int questionId);
	
	public int getCountByQuestionId(int questionId);

	public int updateList(Map<String, Object> paramAnswer);

	public List<String> getsDuplicateAnswerQuestionIds();
}
