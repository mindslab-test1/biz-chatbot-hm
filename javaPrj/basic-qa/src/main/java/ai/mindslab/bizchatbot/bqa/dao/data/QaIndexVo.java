package ai.mindslab.bizchatbot.bqa.dao.data;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("serial")
@JsonIgnoreProperties({"createdDtm","updatedDtm","useYn"})
public class QaIndexVo implements Serializable {


	@Override
	public String toString() {
		return "QaIndexVo [indexId=" + indexId + ", questionId=" + questionId + ", questionNo=" + questionNo
				+ ", question=" + question + ", questionMorph=" + questionMorph + ", morphCount=" + morphCount
				+ ", domainId=" + domainId + ", createdDtm=" + createdDtm + ", updatedDtm="
				+ updatedDtm + ", useYn=" + useYn + ", res=" + res + ", score=" + score + "]";
	}

	public static final int SEARCH_CODE_SUCCESS = 1;
	public static final int SEARCH_CODE_FAIL = 0;
	
	private int indexId;
 	private int questionId;
	private int questionNo;
	private String question;
	private String questionMorph;
	private int morphCount;
	private int domainId;
	private LocalDateTime createdDtm;
	private LocalDateTime updatedDtm;
	private String useYn;
	private int res;
	private Float score;
	private String mainYn;
	private List<QaAnswerVo> answerList;
	public QaIndexVo() {
	}
	public QaIndexVo(QaQuestionVo qna, int no, String mainYn){
		
		this.setQuestionId(qna.getQuestionId());
		this.setQuestionNo(no);
		this.setQuestion(qna.getQuestion());
		this.setDomainId(qna.getDomainId());
		this.setUseYn(qna.getUseYn());
		this.setMainYn(mainYn);
	}
	public int getIndexId() {
		return indexId;
	}
	public void setIndexId(int indexId) {
		this.indexId = indexId;
	}
	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQuestionMorph() {
		return questionMorph;
	}

	public void setQuestionMorph(String questionMorph) {
		this.questionMorph = questionMorph;
	}


	public int getDomainId() {
		return domainId;
	}

	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}

	
	public LocalDateTime getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(LocalDateTime createdDtm) {
		this.createdDtm = createdDtm;
	}

	public LocalDateTime getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(LocalDateTime updatedDtm) {
		this.updatedDtm = updatedDtm;
	}


	public String getUseYn() {
		return useYn;
	}


	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public int getRes() {
		return res;
	}


	public void setRes(int res) {
		this.res = res;
	}
	public Float getScore() {
		return score;
	}
	public void setScore(Float score) {
		this.score = score;
	}
	
	public QaIndexVo getRandomList(List<QaIndexVo> list) {
		Random random = new Random();
		int index = random.nextInt(list.size());
	    return list.get(index);
	}
	public int getMorphCount() {
		return morphCount;
	}
	public void setMorphCount(int morphCount) {
		this.morphCount = morphCount;
	}
	public int getQuestionNo() {
		return questionNo;
	}
	public void setQuestionNo(int questionNo) {
		this.questionNo = questionNo;
	}
	public String getMainYn() {
		return mainYn;
	}
	public void setMainYn(String mainYn) {
		this.mainYn = mainYn;
	}
	public List<QaAnswerVo> getAnswerList() {
		return answerList;
	}
	public void setAnswerList(List<QaAnswerVo> answerList) {
		this.answerList = answerList;
	}
}

