package ai.mindslab.bizchatbot.bqa.dao.data;

import java.io.Serializable;

public class RichExcelVo implements Serializable {

    private String userId;
    private Long questionId;
    private int btnOrder;
    private String type;
    private String btnTitle;
    private String btnUtter;
    private String imgText;
    private String imgLinkUrl;
    private int apiId;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public int getBtnOrder() {
        return btnOrder;
    }

    public void setBtnOrder(int btnOrder) {
        this.btnOrder = btnOrder;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBtnTitle() {
        return btnTitle;
    }

    public void setBtnTitle(String btnTitle) {
        this.btnTitle = btnTitle;
    }

    public String getBtnUtter() {
        return btnUtter;
    }

    public void setBtnUtter(String btnUtter) {
        this.btnUtter = btnUtter;
    }

    public String getImgText() {
        return imgText;
    }

    public void setImgText(String imgText) {
        this.imgText = imgText;
    }

    public String getImgLinkUrl() {
        return imgLinkUrl;
    }

    public void setImgLinkUrl(String imgLinkUrl) {
        this.imgLinkUrl = imgLinkUrl;
    }

    public int getApiId() {
        return apiId;
    }

    public void setApiId(int apiId) {
        this.apiId = apiId;
    }
}
