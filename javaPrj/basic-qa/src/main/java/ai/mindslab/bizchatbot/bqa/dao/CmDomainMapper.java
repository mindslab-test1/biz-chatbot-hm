package ai.mindslab.bizchatbot.bqa.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.bqa.dao.data.CmDomainVo;


@Mapper
public interface CmDomainMapper {
	
	public CmDomainVo get(int domainId);

	public List<CmDomainVo> gets(Map<String, Object> paramMap);

	public String getUsedDomainName(int domainId);
}
