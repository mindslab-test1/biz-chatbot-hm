package ai.mindslab.bizchatbot.bqa.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.bqa.dao.data.QaQnaParaphraseVo;

@Mapper
public interface NlpEntityCodeMapper {
	
	public String getCodeDesc(String code);
}
