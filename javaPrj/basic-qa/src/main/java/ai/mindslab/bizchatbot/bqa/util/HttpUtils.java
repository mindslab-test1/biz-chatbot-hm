package ai.mindslab.bizchatbot.bqa.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HttpUtils {


	public static String getUserId(HttpServletRequest req) {
		String userId = "";
		Cookie[] cookies = req.getCookies();
		if(cookies != null) {
			for(Cookie c: cookies) {
				if("$LoopBackSDK$userId".equals(c.getName())) {
					userId = c.getValue();
					break;
				}
			}
		}
		
		return userId;
	}
	
	public static int getWorkspaceId(HttpServletRequest req) {
		String jsonString = "";
		Cookie[] cookies = req.getCookies();
		if(cookies != null) {
			for(Cookie c: cookies) {
				if("$LoopBackSDK$user".equals(c.getName())) {
					jsonString = c.getValue();
					break;
				}
			}
		}
		
		//"workspace_id":5,"team_id":4
		String foundId = "-1";
		Pattern p;
		Matcher m;
		p = Pattern.compile(".*workspace_id\"\\:([0-9]{1,3}),\"team.*");
		m = p.matcher(jsonString);
		if(m.find()) {
			foundId = m.group(1);
		}
		
		int id = -1;
		try{
			id = Integer.parseInt(foundId);
		}catch(Exception e) {
			id = -1;	
		}
		return id;
	}
	
}
