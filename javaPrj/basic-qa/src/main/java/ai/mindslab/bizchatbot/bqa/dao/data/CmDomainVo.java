package ai.mindslab.bizchatbot.bqa.dao.data;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CmDomainVo implements Serializable {

	public static final String SEARCH_USED_Y = "Y";
	public static final String SEARCH_USED_N = "N";
	private int domainId;
	private int teamId;
	private int parentDomainId;
	private int depth;
	private String domainName;
	private String descript;
	private Date createdDtm;
	private Date  updatedDtm;
	private String creatorId;
	private String  updatorId;
	private String status;
	@Override
	public String toString() {
		return "CmDomainVo [domainId=" + domainId + ", teamId=" + teamId + ", parentDomainId=" + parentDomainId
				+ ", depth=" + depth + ", domainName=" + domainName + ", descript=" + descript + ", createdDtm="
				+ createdDtm + ", updatedDtm=" + updatedDtm + ", creatorId=" + creatorId + ", updatorId=" + updatorId
				+ "]";
	}
	
	public int getDomainId() {
		return domainId;
	}
	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}
	public int getTeamId() {
		return teamId;
	}
	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}
	public int getParentDomainId() {
		return parentDomainId;
	}
	public void setParentDomainId(int parentDomainId) {
		this.parentDomainId = parentDomainId;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public String getDescript() {
		return descript;
	}
	public void setDescript(String descript) {
		this.descript = descript;
	}
	public Date getCreatedDtm() {
		return createdDtm;
	}
	public void setCreatedDtm(Date createdDtm) {
		this.createdDtm = createdDtm;
	}
	public Date getUpdatedDtm() {
		return updatedDtm;
	}
	public void setUpdatedDtm(Date updatedDtm) {
		this.updatedDtm = updatedDtm;
	}
	public String getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}
	public String getUpdatorId() {
		return updatorId;
	}
	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
