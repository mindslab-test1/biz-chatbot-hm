package ai.mindslab.bizchatbot.bqa.dao.data;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import ai.mindslab.bizchatbot.commons.utils.DateUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@SuppressWarnings("serial")
public class QaQuestionVo implements Serializable {

	public final static int USE_YN_Y = 1;
	public final static int USE_YN_N = 0;


	@Override
	public String toString() {
		return "QaQuestionVo [questionId=" + questionId + ", question=" + question
				+ ", questionMorph=" + questionMorph + ", domainId=" + domainId + ", useYn=" + useYn
				+ ", mainYn=" + mainYn + ", attribute1=" + attribute1 + ", attribute2=" + attribute2
				+ ", createdDtm=" + createdDtm + ", updatedDtm=" + updatedDtm
				+ ", creatorId=" + creatorId + ", updatorId=" + updatorId + "]";
	}

	private int questionId;
	private String question;
	private String questionMorph;
	private int domainId;
	private String useYn;
	private String createdDtm;
	private String updatedDtm;
	private String creatorId;
	private String updatorId;
	private QaAnswerVo qaAnswerVo;
	private List<QaAnswerVo> answerList;
	private String mainYn;
	private Float score;
	private String attribute1;
	private String attribute2;

	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getQuestionMorph() {
		return questionMorph;
	}
	public void setQuestionMorph(String questionMorph) {
		this.questionMorph = questionMorph;
	}
	public int getDomainId() {
		return domainId;
	}
	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}
	public String getUpdatorId() {
		return updatorId;
	}
	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}
	public int getQuestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	public QaAnswerVo getQaAnswerVo() {
		return qaAnswerVo;
	}
	public void setQaAnswerVo(QaAnswerVo qaAnswerVo) {
		this.qaAnswerVo = qaAnswerVo;
	}
	public List<QaAnswerVo> getAnswerList() {
		return answerList;
	}
	public void setAnswerList(List<QaAnswerVo> answerList) {
		this.answerList = answerList;
	}
	public String getCreatedDtm() {
		return  DateUtils.convertUtcToLocalDate(createdDtm);
	}
	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}
	public String getUpdatedDtm() {
		return  DateUtils.convertUtcToLocalDate(updatedDtm);
	}
	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getMainYn() {
		return mainYn;
	}
	public void setMainYn(String mainYn) {
		this.mainYn = mainYn;
	}
	public Float getScore() {
		return score;
	}
	public void setScore(Float score) {
		this.score = score;
	}
	public String getAttribute1() {
		return attribute1;
	}
	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}
	public String getAttribute2() {
		return attribute2;
	}
	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}

}
