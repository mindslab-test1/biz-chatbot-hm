package ai.mindslab.bizchatbot.bqa.dao.data;

import java.io.Serializable;
import java.sql.Date;

@SuppressWarnings("serial")
public class QaParaphraseRelationVo implements Serializable {

	private int mainWordId;
	private int wordId;
	public int getMainWordId() {
		return mainWordId;
	}
	public void setMainWordId(int mainWordId) {
		this.mainWordId = mainWordId;
	}
	public int getWordId() {
		return wordId;
	}
	public void setWordId(int wordId) {
		this.wordId = wordId;
	}
}
