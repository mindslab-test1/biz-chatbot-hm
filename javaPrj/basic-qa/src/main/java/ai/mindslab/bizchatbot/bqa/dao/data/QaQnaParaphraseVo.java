package ai.mindslab.bizchatbot.bqa.dao.data;

import java.io.Serializable;
import java.sql.Date;

@SuppressWarnings("serial")
public class QaQnaParaphraseVo implements Serializable {

	private int mainWordId;
	private String mainWord;
	private String paraphraseWord;
	private int[] wordId;
	private Date createAt;

	public int getMainWordId() {
		return mainWordId;
	}

	public void setMainWordId(int mainWordId) {
		this.mainWordId = mainWordId;
	}

	public String getMainWord() {
		return mainWord;
	}

	public void setMainWord(String mainWord) {
		this.mainWord = mainWord;
	}

	public String getParaphraseWord() {
		return paraphraseWord;
	}

	public void setParaphraseWord(String paraphraseWord) {
		this.paraphraseWord = paraphraseWord;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public int[] getWordId() {
		return wordId;
	}

	public void setWordId(int[] wordId) {
		this.wordId = wordId;
	}
}
