package ai.mindslab.bizchatbot.bqa.dao;

import java.util.List;
import java.util.Map;

import ai.mindslab.bizchatbot.bqa.dao.data.QaQuestionsVo;
import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.bqa.dao.data.QaQuestionVo;

@Mapper
public interface QaQuestionMapper {

	public int getCount();

	public int getCountByDomain(Map<String, Object> paramMap);

	public int update(QaQuestionsVo qaQnaVo);

	public int updateUseYn(Map<String, Object> paramMap);

	public List<QaQuestionVo> gets(Map<String, Object> paramMap);

	public List<QaQuestionsVo> getQuestionByAnswer(Map<String, Object> paramMap);
	
	public int insertList(Map<String, Object> paramMap);
}
