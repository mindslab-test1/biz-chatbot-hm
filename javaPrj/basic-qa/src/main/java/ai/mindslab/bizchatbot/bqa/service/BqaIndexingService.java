package ai.mindslab.bizchatbot.bqa.service;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.bizchatbot.bqa.dao.QaIndexMapper;
import ai.mindslab.bizchatbot.bqa.dao.QaIndexingHistoryMapper;
import ai.mindslab.bizchatbot.bqa.dao.data.QaIndexVo;
import ai.mindslab.bizchatbot.bqa.dao.data.QaIndexingHistoryVo;
import ai.mindslab.bizchatbot.bqa.solr.BqaSolrClient;
import ai.mindslab.bizchatbot.commons.codes.IBasicQAConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;

@Service
public class BqaIndexingService implements IRestCodes, IBasicQAConst {

	@Autowired
	BqaSolrClient bqaSolrClient;

	@Autowired
	private QaIndexingHistoryMapper qnaIdxHis;

	@Autowired
	private QaIndexMapper indexMapper;

	private Logger logger = LoggerFactory.getLogger(BqaIndexingService.class);

	@Transactional(rollbackFor = { Exception.class })
	public QaIndexingHistoryVo fullIndexing(String creatorId) throws BizChatbotException {

		QaIndexingHistoryVo qaIndexingHistoryVo = new QaIndexingHistoryVo();
		qaIndexingHistoryVo.setHistoryId(0);
		qaIndexingHistoryVo.setType(qaIndexingHistoryVo.INDEX_TYPE_FULL);
		qaIndexingHistoryVo.setDomainId(qaIndexingHistoryVo.ALL_TARGET_DOMAIN);
		qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_FAIL);
		qaIndexingHistoryVo.setCreatorId(creatorId);

		try {
			qaIndexingHistoryVo = bqaSolrClient.fullIndexing(qaIndexingHistoryVo);

		} catch (Exception e) {
			logger.error(ERR_MSG_INDEX_SOLR_SYSTEM_ERROR, e);
			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_ERROR);
			throw new BizChatbotException(ERR_CODE_INDEX_SOLR_SYSTEM_ERROR, ERR_MSG_INDEX_SOLR_SYSTEM_ERROR);
		}

		if (qaIndexingHistoryVo.INDEX_STATUS_SUCCESS == qaIndexingHistoryVo.getStatus()) {
			logger.debug("fullIndexing qaIndexingHistoryVo INDEX STATUS SUCCESS {}" , qaIndexingHistoryVo);
			qaIndexingHistoryVo.setTotal(indexMapper.count()); 
			qnaIdxHis.insert(qaIndexingHistoryVo);
		}
		return qaIndexingHistoryVo;
	}

	//
	@Transactional(rollbackFor = { Exception.class })
	public QaIndexingHistoryVo addIndexing(String creatorId) throws BizChatbotException {

		QaIndexingHistoryVo qaIndexingHistoryVo = new QaIndexingHistoryVo();

		qaIndexingHistoryVo.setType(qaIndexingHistoryVo.INDEX_TYPE_ADD);
		qaIndexingHistoryVo.setDomainId(qaIndexingHistoryVo.ALL_TARGET_DOMAIN);
		qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_FAIL);
		qaIndexingHistoryVo.setCreatorId(creatorId);
		int[] removeIds = indexMapper.getRemoveList();
		try {
			qaIndexingHistoryVo = bqaSolrClient.addIndexing(qaIndexingHistoryVo, removeIds);
		} catch (Exception e) {
			logger.error(ERR_MSG_INDEX_SOLR_SYSTEM_ERROR, e);
			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_ERROR);
			throw new BizChatbotException(ERR_CODE_INDEX_SOLR_SYSTEM_ERROR, ERR_MSG_INDEX_SOLR_SYSTEM_ERROR);
		}

		if (qaIndexingHistoryVo.INDEX_STATUS_SUCCESS == qaIndexingHistoryVo.getStatus()) {
			logger.debug("addIndexing qaIndexingHistoryVo INDEX STATUS SUCCESS {}" , qaIndexingHistoryVo);
			qaIndexingHistoryVo.setTotal(indexMapper.getAddIdxCount());
			qnaIdxHis.insert(qaIndexingHistoryVo);
		}
		return qaIndexingHistoryVo;
	}

	// 도메인별 인덱싱
	@Transactional(rollbackFor = { Exception.class })
	public QaIndexingHistoryVo domainIndexing(int domainId, String creatorId) throws BizChatbotException {

		QaIndexingHistoryVo qaIndexingHistoryVo = new QaIndexingHistoryVo();

		qaIndexingHistoryVo.setType(qaIndexingHistoryVo.INDEX_TYPE_DOMAIN);
		qaIndexingHistoryVo.setDomainId(domainId);
		qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_FAIL);
		qaIndexingHistoryVo.setCreatorId(creatorId);

		List<QaIndexVo> indexList = indexMapper.getsByDomainId(domainId);

		try {
			if (indexList == null || indexList.size() == 0) {
				throw new BizChatbotException(ERR_CODE_INDEX_DOMAIN_NOTHING, ERR_MSG_INDEX_DOMAIN_NOTHING);
			} else {
				qaIndexingHistoryVo = bqaSolrClient.domainIndexing(qaIndexingHistoryVo, indexList);
			}
		} catch (Exception e) {
			logger.error(ERR_MSG_INDEX_SOLR_SYSTEM_ERROR, e);
			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_ERROR);
			throw new BizChatbotException(ERR_CODE_INDEX_SOLR_SYSTEM_ERROR, ERR_MSG_INDEX_SOLR_SYSTEM_ERROR);
		}

		if (qaIndexingHistoryVo.INDEX_STATUS_SUCCESS == qaIndexingHistoryVo.getStatus()) {
			
			logger.debug("domainIndexing qaIndexingHistoryVo INDEX STATUS SUCCESS {}" , qaIndexingHistoryVo);
			qnaIdxHis.insert(qaIndexingHistoryVo);
		}
		return qaIndexingHistoryVo;
	}

	// MGMT 삭제된 데이터 SOLR 에 반영
	public boolean removeIndexing() throws BizChatbotException {

		int[] removeIds = indexMapper.getRemoveList();
		try {
			return bqaSolrClient.removeIndexing(removeIds);
		} catch (Exception e) {
			logger.error(ERR_MSG_INDEX_SOLR_SYSTEM_ERROR, e);
			throw new BizChatbotException(ERR_CODE_INDEX_SOLR_SYSTEM_ERROR, ERR_MSG_INDEX_SOLR_SYSTEM_ERROR);
		}
	}

}
