package ai.mindslab.bizchatbot.bqa.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

import ai.mindslab.bizchatbot.bqa.dao.data.QaIndexVo;

@Mapper
public interface QaIndexMapper {

	public int insertList(Map<String, Object> paramMap);

	public int updateUseYn(Map<String, Object> paramMap);

	public int[] getRemoveList();
	
	public int count();
	
	public int getAddIdxCount();
	
	public QaIndexVo search(Map<String, Object> paramMap);

	public List<QaIndexVo> getsByDomainId(int domainId);
	
	public int deleteAll();
}
