package ai.mindslab.bizchatbot.bqa.nlp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import ai.mindslab.bizchatbot.bqa.dao.data.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import ai.mindslab.bizchatbot.bqa.dao.NlpEntityCodeMapper;
import ai.mindslab.bizchatbot.bqa.dao.QaQuestionMapper;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceBlockingStub;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceFutureStub;
import maum.brain.nlp.NaturalLanguageProcessingServiceGrpc.NaturalLanguageProcessingServiceStub;
import maum.brain.nlp.Nlp.Document;
import maum.brain.nlp.Nlp.InputText;
import maum.brain.nlp.Nlp.KeywordFrequencyLevel;
import maum.brain.nlp.Nlp.Morpheme;
import maum.brain.nlp.Nlp.NamedEntity;
import maum.brain.nlp.Nlp.NlpAnalysisLevel;
import maum.brain.nlp.Nlp.Sentence;
import maum.common.LangOuterClass.LangCode;


@Repository
public class NlpAnalyzeClient implements IRestCodes {

	@Autowired
	private  NlpEntityCodeMapper nlpEntityCodeMapper;
	private Logger logger = LoggerFactory.getLogger(NlpAnalyzeClient.class);
	private final ManagedChannel channel;
	private final NaturalLanguageProcessingServiceStub asyncStub;
	private final NaturalLanguageProcessingServiceBlockingStub blockingStub;
	private final String[] morpTypes = new String[] { "nc", "np", "nb", "nn", "nr", "pa", "pv" };
	private StreamObserver<InputText> requestObserver;
	private final CountDownLatch finishLatch;
	private StreamObserver<Document> responseObserver;

	@Autowired
	public NlpAnalyzeClient(@Value("${maum.brain.nlp.ip}") String host, @Value("${maum.brain.nlp.port}") int port) {

		this.channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext(true).build();
		this.asyncStub = NaturalLanguageProcessingServiceGrpc.newStub(channel);
		this.blockingStub = NaturalLanguageProcessingServiceGrpc.newBlockingStub(channel);
		this.finishLatch = new CountDownLatch(1);
	}
	protected void finalize() throws Throwable {
		this.channel.shutdown();
    }

	public void countDownLatch() {
		this.finishLatch.countDown();
	}

	public StreamObserver<InputText> getRequestObserver() {
		return requestObserver;
	}

	public void setRequestObserver(StreamObserver<InputText> requestObserver) {
		this.requestObserver = requestObserver;
	}

	public StreamObserver<Document> getResponseObserver() {
		return responseObserver;
	}

	public void setResponseObserver(StreamObserver<Document> responseObserver) {
		this.responseObserver = responseObserver;
	}

	public StreamObserver<InputText> analyzeMultiple(StreamObserver<Document> responseObserver) {
		logger.debug("analyzeMultiple{}", responseObserver);
		StreamObserver<InputText> requestObserver = asyncStub.analyzeMultiple(responseObserver);
		return requestObserver;
	}

	/*public List<QaIndexVo> analyzeList(List<QaIndexVo> indexList) throws BizChatbotException {

			// Paraphrase list
		for (int i = 0; i < indexList.size(); i++) {
			String questionMorph = analyze(indexList.get(i).getQuestion()); 
			if(questionMorph == null || questionMorph.length() == 0) {
				throw new BizChatbotException(ERR_CODE_SEARCH_NLP_RESULT_NOTHING_ERROR, ERR_MSG_SEARCH_NLP_RESULT_NOTHING_ERROR);	
			}
			indexList.get(i).setQuestionMorph(questionMorph);
			indexList.get(i).setMorphCount(questionMorph.split(" ").length);
		}
		return indexList;
	}*/

	public List<QaIndexsVo> analyzeList(List<QaIndexsVo> indexList) throws BizChatbotException {

		// Paraphrase list
		for (int i = 0; i < indexList.size(); i++) {
			String questionMorph = analyze(indexList.get(i).getQuestion());
			if(questionMorph == null || questionMorph.length() == 0) {
				throw new BizChatbotException(ERR_CODE_SEARCH_NLP_RESULT_NOTHING_ERROR, ERR_MSG_SEARCH_NLP_RESULT_NOTHING_ERROR);
			}
			indexList.get(i).setQuestionMorph(questionMorph);
			indexList.get(i).setMorphCount(questionMorph.split(" ").length);
		}
		return indexList;
	}

	public Map<String, Object> modify(List<Sentence> sentenceList) {
		
		Map<String, Object> map = new HashMap<>();
		List<String> strList = new ArrayList<>();
		for (Sentence sentence : sentenceList) {

			List<Morpheme> morpsList = sentence.getMorpsList();
			for (int i = 0; i < morpsList.size(); i++) {
				
				Morpheme morpheme = morpsList.get(i);
				logger.debug("before morpheme, {}",  morpheme.toString());
				logger.debug("before getLemma, {}", morpheme.getLemma());
				// nc: 자립(일반)명사, np: 대명사, nb: 의존명사, nn: 수사, nr: 고유명사, pv: 동사, pa: 형용사, XSN 접미사
				// 체언 접두사 etn 명사형 전성 어미
				if (i > 0 && ((StringUtils.equalsAny(morpsList.get(i - 1).getType(), "nc", "np", "nb", "nn") // 명사
						&& StringUtils.equals(morpheme.getType(), "xsn")) // 접미사
						|| StringUtils.equals(morpsList.get(i - 1).getType(), "pv") // 동사
								&& StringUtils.equals(morpheme.getType(), "etn"))) { // 접두사

					String temp = strList.get(strList.size() - 1);
					strList.remove(strList.size() - 1);
					strList.add(temp + morpheme.getLemma());
					logger.debug("temp + morpheme.getLemma(), {}", temp + morpheme.getLemma());

				} else if (StringUtils.equalsAny(morpheme.getType(), morpTypes)) {

					strList.add(morpheme.getLemma());
					logger.debug("after morpheme, {}", morpheme);
					logger.debug("after getLemma, {}", morpheme.getLemma());
				}
			}
		}
		int morphCount = strList.size();
		String questionMorph = StringUtils.join(strList, " ");
		map.put("morphCount", morphCount);
		map.put("questionMorph", questionMorph);
		
		logger.debug("Nlp Result questionMorph, {}", questionMorph);
		logger.debug("Nlp Result morphCount , {}", morphCount);
		return map;
	}

	public String analyze(String question) {
		String questionMorph = null;

		InputText inputText = InputText.newBuilder().setText(question)
				.setLang(LangCode.kor).setSplitSentence(true).setUseTokenizer(true)
				.setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
				.setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
	
		Document document = blockingStub.analyze(inputText);
		Map<String,Object> map = this.modify(document.getSentencesList());
		questionMorph = map.get("questionMorph").toString();
		logger.debug("questionMorph Result , {}", questionMorph);
			
		return questionMorph;
	}
	
	public List<NlpAnalysisVo> nlpTestAnalyze(String question) {
		
		InputText inputText = InputText.newBuilder().setText(question)
				.setLang(LangCode.kor).setSplitSentence(true).setUseTokenizer(true)
				.setLevel(NlpAnalysisLevel.NLP_ANALYSIS_ALL)
				.setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();
	
		Document document = blockingStub.analyze(inputText);
		
		List<Sentence> sentenceList = document.getSentencesList();
		
		List<NlpAnalysisVo> nlpList = new ArrayList<>();
		
		for (int x  = 0 ; x < sentenceList.size() ; x++ ){
			
			Sentence sentence = sentenceList.get(x);
			logger.debug("nlpTestAnalyze sentence{}",sentence);
			NlpAnalysisVo nlpAnalysisVo = new NlpAnalysisVo();
			nlpAnalysisVo.setText(sentence.getText());
			nlpAnalysisVo.setTextSeq(x);
			
			List<Morpheme> morpList = sentence.getMorpsList();
			List<NlpMorpVo> MorpsVoList = new ArrayList<>();
			for (int i = 0; i < morpList.size(); i++) {
			
				Morpheme morpheme = morpList.get(i);
				NlpMorpVo nlpMorpsVo = new NlpMorpVo();
				nlpMorpsVo.setTextSeq(x);
				
				String type = StringUtils.upperCase(morpheme.getType());
				nlpMorpsVo.setLemmaType(type);
				nlpMorpsVo.setLemma(morpheme.getLemma());
				nlpMorpsVo.setLemmaSeq(i);				
				try {
					String lemmaTypeDesc = NlpWordClass.valueOf(type).getWordClass();
					nlpMorpsVo.setLemmaTypeDesc(lemmaTypeDesc);
				}catch(Exception e) {
					
					logger.warn(StringUtils.upperCase(type)+ " is not defind{}",e.getMessage());
				}
				
				MorpsVoList.add(nlpMorpsVo);
			}
			
			nlpAnalysisVo.setMorpList(MorpsVoList);
			
			List<NamedEntity> nesList = sentence.getNesList();
			List<NlpNesVo> nesVoList = new ArrayList<>();
			for (int i = 0; i < nesList.size(); i++) {
			
				NamedEntity namedEntity = nesList.get(i);
				NlpNesVo nlpNesVo = new NlpNesVo();
				String nesType = StringUtils.upperCase(namedEntity.getType());
				nlpNesVo.setTextSeq(x);
				nlpNesVo.setNes(namedEntity.getText());
				
				try {
					String codeDesc = nlpEntityCodeMapper.getCodeDesc(nesType);
					nlpNesVo.setNesDesc(codeDesc);	
				}catch(Exception e) {
					logger.debug(nesType +" is not defined{]", e.getMessage());
				}
				nlpNesVo.setNesType(nesType);
				nlpNesVo.setNesSeq(i);
				
				nesVoList.add(nlpNesVo);
			}
			
			nlpAnalysisVo.setNesList(nesVoList);
			nlpList.add(nlpAnalysisVo);
		}
			
		return nlpList;
	}

	public String analyzeEx(String question, String[] morpTypes) {
		String questionMorph = null;

		if(morpTypes == null || morpTypes.length < 1) {
			logger.debug("analyzeEx NO morpTypes to extract");
			return questionMorph;
		}

		InputText inputText = InputText.newBuilder().setText(question)
				.setLang(LangCode.kor).setSplitSentence(true).setUseTokenizer(true)
				.setLevel(NlpAnalysisLevel.NLP_ANALYSIS_MORPHEME)
				.setKeywordFrequencyLevel(KeywordFrequencyLevel.KEYWORD_FREQUENCY_NONE).build();

		Document document = blockingStub.analyze(inputText);
		Map<String,Object> map = this.modifyEx(document.getSentencesList(), morpTypes);
		questionMorph = map.get("questionMorph").toString();
		logger.debug("analyzeEx questionMorph Result , {}", questionMorph);

		return questionMorph;
	}

	public Map<String, Object> modifyEx(List<Sentence> sentenceList, String[] morpTypes) {

		Map<String, Object> map = new HashMap<>();
		List<String> strList = new ArrayList<>();
		for (Sentence sentence : sentenceList) {

			List<Morpheme> morpsList = sentence.getMorpsList();
			for (int i = 0; i < morpsList.size(); i++) {

				Morpheme morpheme = morpsList.get(i);
				if (StringUtils.equalsAny(morpheme.getType(), morpTypes)) {
					strList.add(morpheme.getLemma());
				}
			}
		}
		int morphCount = strList.size();
		String questionMorph = StringUtils.join(strList, " ");
		map.put("morphCount", morphCount);
		map.put("questionMorph", questionMorph);

		logger.debug("Nlp Result modifyEx questionMorph, {}", questionMorph);
		logger.debug("Nlp Result modifyEx morphCount , {}", morphCount);
		return map;
	}
}
