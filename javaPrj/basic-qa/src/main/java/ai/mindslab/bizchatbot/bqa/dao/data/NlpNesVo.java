package ai.mindslab.bizchatbot.bqa.dao.data;

public class NlpNesVo {
	private int textSeq;
	private String nes;
	private int nesSeq;
	private String nesType;
	private String nesDesc;
	public int getTextSeq() {
		return textSeq;
	}
	public void setTextSeq(int textSeq) {
		this.textSeq = textSeq;
	}
	public String getNes() {
		return nes;
	}
	public void setNes(String nes) {
		this.nes = nes;
	}
	public int getNesSeq() {
		return nesSeq;
	}
	public void setNesSeq(int nesSeq) {
		this.nesSeq = nesSeq;
	}
	public String getNesType() {
		return nesType;
	}
	public void setNesType(String nesType) {
		this.nesType = nesType;
	}
	public String getNesDesc() {
		return nesDesc;
	}
	public void setNesDesc(String nesDesc) {
		this.nesDesc = nesDesc;
	}
}
