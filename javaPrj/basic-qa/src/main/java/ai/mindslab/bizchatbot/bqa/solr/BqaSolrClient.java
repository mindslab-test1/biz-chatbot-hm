package ai.mindslab.bizchatbot.bqa.solr;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

import ai.mindslab.bizchatbot.bqa.dao.QaAnswerMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import ai.mindslab.bizchatbot.bqa.dao.data.QaDebugVo;
import ai.mindslab.bizchatbot.bqa.dao.data.QaIndexVo;
import ai.mindslab.bizchatbot.bqa.dao.data.QaIndexingHistoryVo;
import ai.mindslab.bizchatbot.commons.codes.IBasicQAConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;

@Repository
public class BqaSolrClient implements IRestCodes, IBasicQAConst {

	private Logger logger = LoggerFactory.getLogger(BqaSolrClient.class);

	SolrClient bqaSolrClient;

	String urlString;

	public final String SOLR_QUERY_TYPE_AND = " AND ";
	public final String SOLR_QUERY_TYPE_OR = " OR ";

	@Value("${solr.bqa.search.max:10}")
	int maxCount;
	@Value("${maum.chunk.query.count:100}")
	int queryCount;
	@Value("${solr.bqa.search.or.result.count:3}")
	int solrOrResultCount;
	@Value("${solr.bqa.server.zone:Asia/Seoul}")
	String zone;

	@Autowired
	private QaAnswerMapper answerMapper;
	
	private static Map<String, Object> cacheMap = new HashMap<>();

	BqaSolrClient(@Value("${solr.bqa.server.url}") String urlString) {
		if (urlString.isEmpty()) {
			urlString = "http://localhost:8983/solr/basicQa";
		}
		bqaSolrClient = new HttpSolrClient.Builder(urlString).build();
		logger.debug("Solr urlString : {}", urlString);
	}

	protected void finalize() throws Throwable {
		bqaSolrClient.close();
	}

	public QaIndexingHistoryVo setResult(QaIndexingHistoryVo qaIndexingHistoryVo, QueryResponse res) {
		String statusMessages = res.getResponse().get("statusMessages").toString();
		qaIndexingHistoryVo.setMsg(statusMessages);

		logger.debug("fullIndexing setResult{}", statusMessages);
		String[] data = statusMessages.split(",");
		try {
			for (int x = 0; x < data.length; x++) {
				if (data[x].indexOf("Committed=") > 0) {
					String date = data[x].replaceFirst("Committed=", "");
					SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date d = transFormat.parse(date);
					Instant instant = Instant.ofEpochMilli(d.getTime());
					LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.of(zone));
					qaIndexingHistoryVo.setCommittedDtm(ldt);

				} else if (data[x].toLowerCase().indexOf("dump started=") > 0) {
					String date = data[x].toLowerCase().split("dump started=")[1];
					SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date d = transFormat.parse(date);
					Instant instant = Instant.ofEpochMilli(d.getTime());
					LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.of(zone));
					qaIndexingHistoryVo.setIndexingStartedDtm(ldt);
				} else if (data[x].indexOf("Time taken=") > 0) {
					String takenTime = data[x].replaceFirst("Time taken=", "").replaceFirst("}", "");
					String[] t = takenTime.split(":");
					String sm = t[2];
					String s = sm.substring(0, sm.indexOf("."));
					String m = sm.substring(sm.indexOf(".") + 1, sm.length());

					SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
					Calendar now = Calendar.getInstance();
					now.set(Calendar.HOUR_OF_DAY, Integer.parseInt(t[0].trim()));
					now.set(Calendar.MINUTE, Integer.parseInt(t[1].trim()));
					now.set(Calendar.SECOND, Integer.parseInt(s));
					now.set(Calendar.MILLISECOND, Integer.parseInt(m));
					qaIndexingHistoryVo.setTakenTime(sdf.format(now.getTime()));
				}

			}
			logger.debug("fullIndexing qaIndexingHistoryVo{}", qaIndexingHistoryVo.toString());
		} catch (Exception e) {
			logger.warn("Solr result parsing error : {}", e);
		}

		return qaIndexingHistoryVo;
	}

	public QaIndexingHistoryVo fullIndexing(QaIndexingHistoryVo qaIndexingHistoryVo)
			throws SolrServerException, IOException {
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("qt", "/dataimport");
		params.set("clean", "true");
		params.set("command", "full-import");

		QueryResponse res = bqaSolrClient.query(params);
		qaIndexingHistoryVo = this.setResult(qaIndexingHistoryVo, res);
		logger.debug("Solr QueryResponse res: {}", res);

		UpdateResponse commit = bqaSolrClient.commit();
		logger.debug("Solr QueryResponse commit: {}", commit);
		if (commit.getStatus() == 0) {

			if (qaIndexingHistoryVo.getCommittedDtm() == null) {

				Instant instant = Instant.ofEpochMilli(new Date().getTime());
				LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.of(zone));
				qaIndexingHistoryVo.setCommittedDtm(ldt);
			}

			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_SUCCESS);
		} else {

			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_FAIL);
		}

		// qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_ERROR);

		return qaIndexingHistoryVo;
	}

	public QaIndexingHistoryVo addIndexing(QaIndexingHistoryVo qaIndexingHistoryVo, int[] removeIds)
			throws SolrServerException, IOException {
		logger.debug("Solr urlString : {}", urlString);

		for (int i = 0; i < removeIds.length; i++) {
			bqaSolrClient.deleteById(String.valueOf(removeIds[i]));
			logger.debug("addIndexing removeIds[i]{}", removeIds[i]);
		}

		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("qt", "/dataimport");
		params.set("command", "delta-import");

		QueryResponse res = bqaSolrClient.query(params);
		UpdateResponse commit = bqaSolrClient.commit();
		logger.debug("addIndexing res{}", res);

		qaIndexingHistoryVo = this.setResult(qaIndexingHistoryVo, res);

		if (commit.getStatus() == 0) {

			logger.info("qaIndexingHistoryVo.getCommittedDtm() is null{}", qaIndexingHistoryVo.getCommittedDtm());
			if (qaIndexingHistoryVo.getCommittedDtm() == null) {

				Instant instant = Instant.ofEpochMilli(new Date().getTime());
				LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneId.of(zone));
				qaIndexingHistoryVo.setCommittedDtm(ldt);
			}
			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_SUCCESS);
		} else {

			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_FAIL);
		}

		return qaIndexingHistoryVo;
	}

	public QaIndexingHistoryVo domainIndexing(QaIndexingHistoryVo qaIndexingHistoryVo, List<QaIndexVo> indexList)
			throws SolrServerException, IOException {

		logger.info("domainIndexing indexList{}", indexList.size());

		bqaSolrClient.deleteByQuery("domain_id:" + String.valueOf(qaIndexingHistoryVo.getDomainId()));

		Collection<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
		qaIndexingHistoryVo.setIndexingStartedDtm(LocalDateTime.now());
		for (int i = 0; i < indexList.size(); i++) {
			QaIndexVo qaIndexVo = indexList.get(i);
			SolrInputDocument solrInputDocument = new SolrInputDocument();
			solrInputDocument.addField("id", qaIndexVo.getIndexId());
			solrInputDocument.addField("question_id", qaIndexVo.getQuestionId());
			solrInputDocument.addField("question_no", qaIndexVo.getQuestionNo());
			solrInputDocument.addField("question", qaIndexVo.getQuestion());
			solrInputDocument.addField("morph_count", qaIndexVo.getMorphCount());
			solrInputDocument.addField("main_yn", qaIndexVo.getMainYn());
			solrInputDocument.addField("question_morph", qaIndexVo.getQuestionMorph());
			solrInputDocument.addField("domain_id", qaIndexVo.getDomainId());

			docs.add(solrInputDocument);
		}
		UpdateResponse res = bqaSolrClient.add(docs);

		UpdateResponse commit = bqaSolrClient.commit();
		qaIndexingHistoryVo.setCommittedDtm(LocalDateTime.now());

		try {
			LocalDateTime startTime = qaIndexingHistoryVo.getIndexingStartedDtm();
			LocalDateTime endTime = qaIndexingHistoryVo.getCommittedDtm();
			long millis = startTime.until(endTime, ChronoUnit.MILLIS);
			qaIndexingHistoryVo.setTakenTime(DurationFormatUtils.formatDuration(millis, "HH:mm:ss.SSS"));
		} catch (Exception e) {
			logger.warn("setTakenTime error", e);
		}

		logger.debug("domainIndexing qaIndexingHistoryVo{}", qaIndexingHistoryVo);

		qaIndexingHistoryVo.setTotal(indexList.size());
		qaIndexingHistoryVo.setMsg(res.getResponse().toString());

		if (commit.getStatus() == 0) {

			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_SUCCESS);
		} else {

			qaIndexingHistoryVo.setStatus(qaIndexingHistoryVo.INDEX_STATUS_FAIL);
		}

		return qaIndexingHistoryVo;
	}

	public boolean removeIndexing(int... qnaIndexId) throws SolrServerException, IOException {
		logger.info("removeIndexing");
		boolean result = false;

		logger.debug("Solr urlString : {}", urlString);

		for (int i = 0; i < qnaIndexId.length; i++) {
			bqaSolrClient.deleteById(String.valueOf(qnaIndexId));
		}
		UpdateResponse res = bqaSolrClient.commit();

		if (res != null && res.getStatus() == 0) {
			result = true;
		}
		return result;
	}

	public QaDebugVo searchForDebug(String questionMorph, int... domainIds)
			throws SolrServerException, IOException, BizChatbotException {
		logger.debug("Repository searchForDebug() ,{},{}", questionMorph, domainIds);

		List<QaIndexVo> indexList = new ArrayList<>();
		QaDebugVo qaDebugVo = new QaDebugVo();
		qaDebugVo.setStatus(SEARCH_RESULT_SOLRAND_FAIL);
		SolrQuery solrQuery = buildAndQuery(questionMorph, domainIds);
		QueryResponse queryResponse = bqaSolrClient.query(solrQuery);

		SolrDocumentList documentList = queryResponse.getResults();
		logger.info("documentList size : " + documentList.size());

		if (documentList.size() > maxCount) {

			throw new BizChatbotException(ERR_CODE_SEARCH_SOLR_TOO_MANY_RESULT, ERR_MSG_SEARCH_SOLR_TOO_MANY_RESULT);
		} else if (documentList.size() >0) {

			for (int i = 0; i < documentList.size(); i++) {
				SolrDocument document = documentList.get(i);
				logger.debug("document {}", document);
				QaIndexVo qaIndexVo = new QaIndexVo();
				qaIndexVo.setIndexId(Integer.parseInt(String.valueOf(document.get("id"))));
				qaIndexVo.setDomainId(Integer.parseInt(String.valueOf(document.get("domain_id"))));

				qaIndexVo.setQuestionMorph(String.valueOf(document.get("question_morph")));
				qaIndexVo.setQuestion(String.valueOf(document.get("question")));
				qaIndexVo.setQuestionId(Integer.parseInt(String.valueOf(document.get("question_id"))));
				qaIndexVo.setQuestionNo(Integer.parseInt(String.valueOf(document.get("question_no"))));
				qaIndexVo.setQuestion(String.valueOf(document.get("question")));
				qaIndexVo.setScore(Float.parseFloat(String.valueOf(document.get("score"))));
				qaIndexVo.setMainYn(String.valueOf(document.get("main_yn")));
				qaIndexVo.setQuestionNo(Integer.parseInt(String.valueOf(document.get("morph_count"))));
				if (i == 0) { // selected Question Index
					qaIndexVo.setRes(QaIndexVo.SEARCH_CODE_SUCCESS);
				} else {
					qaIndexVo.setRes(QaIndexVo.SEARCH_CODE_FAIL);
				}
				logger.debug("document qaIndexVo{}", qaIndexVo);

				indexList.add(qaIndexVo);
			}

			qaDebugVo.setList(indexList);
			qaDebugVo.setStatus(SEARCH_RESULT_SOLRAND_SUCCESS);
		}
		
		return qaDebugVo;
	}

	public QaIndexVo search(String questionMorph, int... domainIds)
			throws SolrServerException, IOException, BizChatbotException {
		logger.debug("Repository search() ,{},{}", questionMorph, domainIds);
		QaIndexVo qaIndexVo = new QaIndexVo();
		qaIndexVo.setRes(QaIndexVo.SEARCH_CODE_FAIL);

		SolrQuery solrQuery = buildAndQuery(questionMorph, domainIds);
		QueryResponse queryResponse = bqaSolrClient.query(solrQuery);
		SolrDocumentList documentList = queryResponse.getResults();

		logger.info("documentList size : " + documentList.size());
		SolrDocument document = null;

		// 너무많은 결과가 나왔을떄 처리
		if (documentList.size() > maxCount) {

			throw new BizChatbotException(ERR_CODE_SEARCH_SOLR_TOO_MANY_RESULT, ERR_MSG_SEARCH_SOLR_TOO_MANY_RESULT);
		} else if (documentList.size() >0) {

			document = documentList.get(0);
			logger.debug("document {}", document);
			qaIndexVo.setIndexId(Integer.parseInt(String.valueOf(document.get("id"))));
			qaIndexVo.setDomainId(Integer.parseInt(String.valueOf(document.get("domain_id"))));
			qaIndexVo.setQuestionMorph(String.valueOf(document.get("question_morph")));
			qaIndexVo.setQuestionId(Integer.parseInt(String.valueOf(document.get("question_id"))));
			qaIndexVo.setQuestionNo(Integer.parseInt(String.valueOf(document.get("question_no"))));
			qaIndexVo.setQuestion(String.valueOf(document.get("question")));
			qaIndexVo.setMainYn(String.valueOf(document.get("main_yn")));
			qaIndexVo.setScore(Float.parseFloat(String.valueOf(document.get("score"))));
			qaIndexVo.setQuestionNo(Integer.parseInt(String.valueOf(document.get("morph_count"))));

			qaIndexVo.setRes(QaIndexVo.SEARCH_CODE_SUCCESS);
			logger.debug("document qaIndexVo{}", qaIndexVo);
		}
		return qaIndexVo;
	}

	public List<QaIndexVo> searchOr(int randomFlag, String questionMorph, int range, int... domainIds)
			throws SolrServerException, IOException, BizChatbotException {

		logger.debug("Repository searchOr() questionMorph,{},domainIds{},range{},solrOrResultCount{}", questionMorph,
				domainIds, range, solrOrResultCount);

		List<String> dupAnswerQuestionIdList = this.getsDuplicateAnswerQuestionIds();
		List<String> tempQuestionIdsList = new ArrayList<>();
		List<QaIndexVo> indexList = new ArrayList<>();
		String[] morphQuery = StringUtils.split(questionMorph, " ");
		int questionMorphCount = morphQuery.length;
		int minWordCount = questionMorphCount, maxWordCount = questionMorphCount;
		SolrQuery solrQuery = buildOrQuery(questionMorph, range, domainIds);
		QueryResponse queryResponse = bqaSolrClient.query(solrQuery);
		SolrDocumentList documentList = queryResponse.getResults();

		if (range > questionMorphCount) {
			minWordCount = 0;
		} else {
			minWordCount -= range;
			
		}
		maxWordCount += range;
		
		logger.debug("searchOr documentList size : {} , minWordCount {}, maxWordCount{}", documentList.size(), minWordCount, maxWordCount);
		SolrDocument document = null;
		int count = 0;
		if (documentList.size() > 0) {
			index: for (int x = 0; x < documentList.size(); x++) {
				document = documentList.get(x);
				logger.debug("document {}", document);
				int indexMorphCount = Integer.parseInt(String.valueOf(document.get("morph_count")));
				if (count == solrOrResultCount || (indexMorphCount < minWordCount) || indexMorphCount > maxWordCount) {
					break;
				}

				String questionId = String.valueOf(document.get("question_id"));
				for (String questionIds : dupAnswerQuestionIdList) {
					if (questionIds.contains(questionId)) {
						if (tempQuestionIdsList.contains(questionIds)) {
							continue index;
						}
						tempQuestionIdsList.add(questionIds);
						break;
					}
				}

				QaIndexVo qaIndexVo = new QaIndexVo();
				qaIndexVo.setIndexId(Integer.parseInt(String.valueOf(document.get("id"))));
				qaIndexVo.setDomainId(Integer.parseInt(String.valueOf(document.get("domain_id"))));
				qaIndexVo.setQuestionMorph(String.valueOf(document.get("question_morph")));
				qaIndexVo.setQuestionId(Integer.parseInt(String.valueOf(document.get("question_id"))));
				qaIndexVo.setQuestionNo(Integer.parseInt(String.valueOf(document.get("question_no"))));
				qaIndexVo.setQuestion(String.valueOf(document.get("question")));
				qaIndexVo.setScore(Float.parseFloat(String.valueOf(document.get("score"))));
				qaIndexVo.setRes(QaIndexVo.SEARCH_CODE_SUCCESS);
				qaIndexVo.setMainYn(String.valueOf(document.get("main_yn")));
				qaIndexVo.setMorphCount(indexMorphCount);
				logger.debug("document qaIndexVo{}", qaIndexVo);
				count++;
				indexList.add(qaIndexVo);
			}
		}else {
			throw new BizChatbotException(ERR_CODE_SEARCH_SOLROR_NOTHING, ERR_MSG_SEARCH_SOLROR_NOTHING);
		}

		return indexList;
	}

	public QaDebugVo searchOrForDebug(String questionMorph, int range, int... domainIds)
			throws SolrServerException, IOException, BizChatbotException {

		logger.debug("Repository searchOrForDebug() questionMorph,{},domainIds{},range{},solrOrResultCount{}", questionMorph,
				domainIds, range, solrOrResultCount);
		QaDebugVo qaDebugVo = new QaDebugVo();
		qaDebugVo.setStatus(SEARCH_RESULT_SOLRAND_FAIL);

		List<String> dupAnswerQuestionIdList = this.getsDuplicateAnswerQuestionIds();
		List<String> tempQuestionIdsList = new ArrayList<>();
		List<QaIndexVo> indexList = new ArrayList<>();
		String[] morphQuery = StringUtils.split(questionMorph, " ");
		int questionMorphCount = morphQuery.length;
		int minWordCount = questionMorphCount, maxWordCount = questionMorphCount;
		SolrQuery solrQuery = buildOrQuery(questionMorph, range, domainIds);
		QueryResponse queryResponse = bqaSolrClient.query(solrQuery);
		SolrDocumentList documentList = queryResponse.getResults();

		if (range > questionMorphCount) {
			minWordCount = 0;
		} else {
			minWordCount -= range;
		}
		maxWordCount += range;
		
		logger.debug("searchOrForDebug documentList size : {} , minWordCount {}, maxWordCount{}", documentList.size(), minWordCount, maxWordCount);
		SolrDocument document = null;
		int count = 0;
		if (documentList.size() > 0) {
			index: for (int x = 0; x < documentList.size(); x++) {
				document = documentList.get(x);
				logger.debug("document {}", document);
				int indexMorphCount = Integer.parseInt(String.valueOf(document.get("morph_count")));
				if (count == solrOrResultCount || (indexMorphCount < minWordCount) || indexMorphCount > maxWordCount) {
					break;
				}

				String questionId = String.valueOf(document.get("question_id"));
				for (String questionIds : dupAnswerQuestionIdList) {
					if (questionIds.contains(questionId)) {
						if (tempQuestionIdsList.contains(questionIds)) {
							continue index;
						}
						tempQuestionIdsList.add(questionIds);
						break;
					}
				}

				QaIndexVo qaIndexVo = new QaIndexVo();
				qaIndexVo.setIndexId(Integer.parseInt(String.valueOf(document.get("id"))));
				qaIndexVo.setDomainId(Integer.parseInt(String.valueOf(document.get("domain_id"))));
				qaIndexVo.setQuestionMorph(String.valueOf(document.get("question_morph")));
				qaIndexVo.setQuestionId(Integer.parseInt(String.valueOf(document.get("question_id"))));
				qaIndexVo.setQuestionNo(Integer.parseInt(String.valueOf(document.get("question_no"))));
				qaIndexVo.setQuestion(String.valueOf(document.get("question")));
				qaIndexVo.setScore(Float.parseFloat(String.valueOf(document.get("score"))));
				qaIndexVo.setRes(QaIndexVo.SEARCH_CODE_SUCCESS);
				qaIndexVo.setMainYn(String.valueOf(document.get("main_yn")));
				qaIndexVo.setMorphCount(indexMorphCount);
				logger.debug("document qaIndexVo{}", qaIndexVo);
				count++;
				indexList.add(qaIndexVo);
			}
			qaDebugVo.setStatus(SEARCH_RESULT_SOLRAND_SUCCESS);
			qaDebugVo.setList(indexList);
		}else {
			throw new BizChatbotException(ERR_CODE_SEARCH_SOLROR_NOTHING, ERR_MSG_SEARCH_SOLROR_NOTHING);
		}

		return qaDebugVo;
	}

	private SolrQuery buildAndQuery(String questionMorph, int[] domainIds) {
		logger.info("buildAndQuery:questionMorph {}, domainIds {}", questionMorph, domainIds);
		SolrQuery solrQuery = new SolrQuery();
		String[] morphQuery = StringUtils.split(questionMorph, " ");
		int morph_count = morphQuery.length;
		logger.debug("buildAndQuery:morphQuery {}, morph_count {}", morphQuery, morph_count);
		for (int i = 0; i < morphQuery.length; i++) {
			morphQuery[i] = "question_morph:" + morphQuery[i];
		}
		StringBuffer qbuffer = new StringBuffer("(" + StringUtils.join(morphQuery, SOLR_QUERY_TYPE_AND) + ")");
		logger.debug("qbuffer.toString{}", qbuffer.toString());

		if (domainIds != null && domainIds.length > 0 && domainIds[0] != 0) {
			String[] domainQuery = new String[domainIds.length];
			for (int i = 0; i < domainQuery.length; i++) {
				domainQuery[i] = "domain_id:" + String.valueOf(domainIds[i]);
			}
			qbuffer.append(SOLR_QUERY_TYPE_AND + "(" + StringUtils.join(domainQuery, SOLR_QUERY_TYPE_OR) + ")");
		}
		// morph_count : nlp token count
		qbuffer.append(SOLR_QUERY_TYPE_AND + "( morph_count:" + morph_count + ")");
		logger.debug("qbuffer.toString{}", qbuffer.toString());
		solrQuery.setQuery(qbuffer.toString());
		solrQuery.setFields("*", "score");
		solrQuery.addSort("score", ORDER.desc);

		logger.info("buildAndQuery: {}", solrQuery.toString());
		return solrQuery;
	}

	private SolrQuery buildOrQuery(String questionMorph, int range, int[] domainIds) {

		logger.info("buildOrQuery questionMorph {}, domainIds {}", questionMorph, domainIds);
		SolrQuery solrQuery = new SolrQuery();
		String[] morphQuery = StringUtils.split(questionMorph, " ");
		logger.debug("buildOrQuery morphQuery {}", morphQuery);
		for (int i = 0; i < morphQuery.length; i++) {
			morphQuery[i] = "question_morph:" + morphQuery[i];
		}
		StringBuffer qbuffer = new StringBuffer("(" + StringUtils.join(morphQuery, SOLR_QUERY_TYPE_OR) + ")");
		logger.debug("buildOrQuery qbuffer.toString{}", qbuffer.toString());

		if (domainIds != null && domainIds.length > 0 && domainIds[0] != 0) {
			String[] domainQuery = new String[domainIds.length];
			for (int i = 0; i < domainQuery.length; i++) {
				domainQuery[i] = "domain_id:" + String.valueOf(domainIds[i]);
			}
			qbuffer.append(SOLR_QUERY_TYPE_AND + "(" + StringUtils.join(domainQuery, SOLR_QUERY_TYPE_OR) + ")");
		}
		qbuffer.append(SOLR_QUERY_TYPE_AND + "( main_yn: Y)");
		logger.debug("qbuffer.toString{}", qbuffer.toString());
		solrQuery.setQuery(qbuffer.toString());
		solrQuery.setFields("*", "score");
		solrQuery.addSort("score", ORDER.desc);
		logger.info("buildOrQuery: {}", solrQuery.toString());
		return solrQuery;
	}

	@SuppressWarnings("unchecked")
	private List<String> getsDuplicateAnswerQuestionIds() {
		
		final int DENOMINATOR_TIME = 10 * 60 * 1000;
		List<String> rtnList = (List<String>) cacheMap.get("list");

		if(rtnList != null) {
			Long cacheTime = (Long) cacheMap.get("time");

			long diff = new Date().getTime() - cacheTime;
			long diffTime = diff / DENOMINATOR_TIME;
			
			// 캐시 만료시
			if(diffTime >= 1) {
				List<String> list = answerMapper.getsDuplicateAnswerQuestionIds();
				cacheMap.put("time", new Date().getTime());
				cacheMap.put("list", list);

				rtnList = list;
			}
		} else {
			cacheMap = new HashMap<>();

			List<String> list = answerMapper.getsDuplicateAnswerQuestionIds();
			cacheMap.put("time", new Date().getTime());
			cacheMap.put("list", list);

			rtnList = list;
		}
		
		return rtnList;
	}

}
