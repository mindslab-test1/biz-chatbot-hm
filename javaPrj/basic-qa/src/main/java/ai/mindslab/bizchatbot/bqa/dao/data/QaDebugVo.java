package ai.mindslab.bizchatbot.bqa.dao.data;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao.data.QaRichVo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import ai.mindslab.bizchatbot.commons.codes.IBasicQAConst;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties({ "createdDtm" })
public class QaDebugVo implements Serializable, IBasicQAConst {

	@Override
	public String toString() {
		return "QaDebugVo [usedDomainName=" + usedDomainName + ", usedDomainId=" + usedDomainId + ", answerId="
				+ answerId + ", answer=" + answer + ", question=" + question + ", questionMorph=" + questionMorph
				+ ", createdDtm=" + createdDtm + ", list=" + list + ", totalCount=" + totalCount + ", SearchFlowType="
				+ SearchFlowType + ", SearchFlowTypeName=" + SearchFlowTypeName + ", morphRange=" + morphRange
				+ ", debugYn=" + debugYn + ", status=" + status + ", result=" + result + "]";
	}

	private String usedDomainName;
	private int usedDomainId;
	private int answerId;
	private String answer;
	private String question;
	private String questionMorph;
	private LocalDateTime createdDtm;
	private List<QaIndexVo> list;
	private int totalCount;
	private int SearchFlowType;
	private String SearchFlowTypeName;
	private int morphRange;
	// private List<CmDomainVo> domainList;
	private String debugYn = "N";
	// private List<NlpAnalysisVo> nlpList;
	private int status;
	private String result;

	private String respCode;
	private QaRichVo qaRichVo;
	private ChatbotRichContent richContent;
	private ChatbotApiInfo apiInfo;

	public QaDebugVo() {
		this.createdDtm = LocalDateTime.now();
	}

	public int getAnswerId() {
		return answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public LocalDateTime getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(LocalDateTime createdDtm) {
		this.createdDtm = createdDtm;
	}

	public int getSearchFlowType() {
		return SearchFlowType;
	}

	public void setSearchFlowType(int searchFlowType) {
		SearchFlowType = searchFlowType;
		if (searchFlowType == SEARCH_TYPE_DB_SOLRAND) {
			SearchFlowTypeName = "DB,SOLRAND";
		} else if (searchFlowType == SEARCH_TYPE_DB) {
			SearchFlowTypeName = "DB";
		} else if (searchFlowType == SEARCH_TYPE_SOLRAND) {
			SearchFlowTypeName = "SOLRAND";
		} else if (searchFlowType == SEARCH_TYPE_DB_SOLRAND_SOLROR) {
			SearchFlowTypeName = "DB,SOLRAND,SOLROR";
		}
	}

	public String getSearchFlowTypeName() {
		return SearchFlowTypeName;
	}

	public void setSearchFlowTypeName(String searchFlowTypeName) {
		SearchFlowTypeName = searchFlowTypeName;
	}

	// public List<CmDomainVo> getDomainList() {
	// return domainList;
	// }
	// public void setDomainList(List<CmDomainVo> domainList) {
	// this.domainList = domainList;
	// }
	public int getMorphRange() {
		return morphRange;
	}

	public void setMorphRange(int morphRange) {
		this.morphRange = morphRange;
	}

	// public List<NlpAnalysisVo> getNlpList() {
	// return nlpList;
	// }
	// public void setNlpList(List<NlpAnalysisVo> nlpList) {
	// this.nlpList = nlpList;
	// }
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;

		switch (status) {
		case -1:
			setResult(" SEARCH RESULT : DB FAIL");
			break;
		case 1:
			setResult(" SEARCH RESULT : DB SUCCESS");
			break;
		case -2:
			setResult(" SEARCH RESULT : SOLRAND FAIL");
			break;
		case 2:
			setResult(" SEARCH RESULT : SOLRAND SUCCESS");
			break;
		case -3:
			setResult(" SEARCH RESULT : SOLROR FAIL");
			break;
		case 3:
			setResult(" SEARCH RESULT : SOLROR SUCCESS");
			break;

		default:
			break;
		}

	}

	public int getUsedDomainId() {
		return usedDomainId;
	}

	public void setUsedDomainId(int usedDomainId) {
		this.usedDomainId = usedDomainId;
	}

	public String getDebugYn() {
		return debugYn;
	}

	public void setDebugYn(String debugYn) {
		this.debugYn = debugYn;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<QaIndexVo> getList() {
		return list;
	}

	public void setList(List<QaIndexVo> list) {
		this.totalCount = list.size();
		this.list = list;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getUsedDomainName() {
		return usedDomainName;
	}

	public void setUsedDomainName(String usedDomainName) {
		this.usedDomainName = usedDomainName;
	}

	public String getQuestionMorph() {
		return questionMorph;
	}

	public void setQuestionMorph(String questionMorph) {
		this.questionMorph = questionMorph;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public QaRichVo getQaRichVo() {
		return qaRichVo;
	}

	public void setQaRichVo(QaRichVo qaRichVo) {
		this.qaRichVo = qaRichVo;
	}

	public ChatbotRichContent getRichContent() {
		return richContent;
	}

	public void setRichContent(ChatbotRichContent richContent) {
		this.richContent = richContent;
	}

	public ChatbotApiInfo getApiInfo() {
		return apiInfo;
	}

	public void setApiInfo(ChatbotApiInfo apiInfo) {
		this.apiInfo = apiInfo;
	}
}
