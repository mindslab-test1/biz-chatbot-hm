package ai.mindslab.bizchatbot.bqa.dao.data;

import ai.mindslab.bizchatbot.commons.utils.DateUtils;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class QaQuestionsVo implements Serializable {

	public final static int USE_YN_Y = 1;
	public final static int USE_YN_N = 0;

    private int domainId;
	private int questionId;
	private int answerId;
	private String question;
	private String questionMorph;
	private String useYn;
	private String createdDtm;
	private String updatedDtm;
	private String creatorId;
	private String updatorId;

	private String mainYn;
	private Float score;

	public String getMainYn() {
		return mainYn;
	}

	public void setMainYn(String mainYn) {
		this.mainYn = mainYn;
	}

	public Float getScore() {
		return score;
	}

	public void setScore(Float score) {
		this.score = score;
	}

	public static int getUseYnY() {
		return USE_YN_Y;
	}

	public static int getUseYnN() {
		return USE_YN_N;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public int getAnswerId() {
		return answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQuestionMorph() {
		return questionMorph;
	}

	public void setQuestionMorph(String questionMorph) {
		this.questionMorph = questionMorph;
	}

	public int getDomainId() {
		return domainId;
	}

	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}
}
