package ai.mindslab.bizchatbot.bqa.dao.data;

import java.io.Serializable;
import java.util.List;

public class QaBqaVo  implements Serializable {

    private List<QaQuestionsVo> questionList;
    private  QaAnswerVo answerGroup;
    private int domainId;
    private String attribute1;
    private String attribute2;
    private String creatorId;
    private String updatorId;



    public List<QaQuestionsVo> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<QaQuestionsVo> questionList) {
        this.questionList = questionList;
    }

    public QaAnswerVo getAnswerGroup() {
        return answerGroup;
    }

    public void setAnswerGroup(QaAnswerVo answerGroup) {
        this.answerGroup = answerGroup;
    }

    public String getAttribute1() {
        return attribute1;
    }

    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public String getAttribute2() {
        return attribute2;
    }

    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2;
    }


    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId;
    }

    public int getDomainId() {
        return domainId;
    }

    public void setDomainId(int domainId) {
        this.domainId = domainId;
    }
}
