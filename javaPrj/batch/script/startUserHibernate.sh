#!/usr/bin/bash

export P_NM="admin-hibernate-$(hostname)"

echo "profile ${CHATBOT_PROFILE}"

count=`ps -ef|grep ${P_NM} |grep -v grep| awk '{print $2}'|wc -l`

if [ $count -gt 0 ]; then
	echo "${P_NM} Process is already running...";
else
	nohup java -jar ../lib/chatbot-batch.jar -Dspring.profiles.active=${CHATBOT_PROFILE} -Dp.name=${P_NM} -DinitRun=false &
fi
