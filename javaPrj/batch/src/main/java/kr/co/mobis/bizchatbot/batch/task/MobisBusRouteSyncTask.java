package kr.co.mobis.bizchatbot.batch.task;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.co.mobis.bizchatbot.batch.service.MobisBusService;

@Component
public class MobisBusRouteSyncTask {
	
	private static Logger logger = LoggerFactory.getLogger(MobisBusRouteSyncTask.class);

	@Autowired
	private MobisBusService busSvc;
	
	@PostConstruct
	public void initRun() {
		runOnce();
	}
	
	public MobisBusRouteSyncTask() {
	}
	
	public void runOnce() {
		String initRun = System.getProperty("initRun", "false");
		boolean bInitRun = false;
		try {
			bInitRun = Boolean.valueOf(initRun);
		}catch(Exception e) {
		}
		if(!bInitRun) {
			logger.info("initial running/runOnce skip...============");
			return;
		}
		logger.info("initial running/runOnce run============");
		runSyncBusRoute();
		logger.info("runOnce finished============");
	}
	
	
	//monday am 05:00:00
	@Scheduled(cron = "0 0 5 ? * 2")
	public void syncBusRouteInfo() {
		logger.info("syncBusRouteInfo run============");
		runSyncBusRoute();
		logger.info("syncBusRouteInfo finished============");
	}
	
	public void runSyncBusRoute() {
		logger.info("runSyncBusRoute/START");
		
		busSvc.syncBusOp();
		
		logger.info("runSyncBusRoute/END");
	}

}
