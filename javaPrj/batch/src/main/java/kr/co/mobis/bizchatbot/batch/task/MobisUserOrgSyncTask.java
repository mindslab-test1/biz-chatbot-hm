package kr.co.mobis.bizchatbot.batch.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.co.mobis.bizchatbot.batch.dao.data.MobisOrgData;
import kr.co.mobis.bizchatbot.batch.dao.data.MobisUserData;
import kr.co.mobis.bizchatbot.batch.service.MobisUserOrgSyncSerivce;

@Component
public class MobisUserOrgSyncTask {

	private static Logger logger = LoggerFactory.getLogger(MobisUserOrgSyncTask.class);
	
	@Autowired
	private MobisUserOrgSyncSerivce syncService;
	
	private static final int pgSize = 1000;
	
	@PostConstruct
	public void initRun() {
		runOnce();
	}
	
	@Scheduled(cron = "0 0 1 ? * *")
//	@Scheduled(fixedRate=30000)
	public void syncOrgUserInfo() {
		logger.info("syncOrgUserInfo run============");
		runSyncOrg();
		runSyncUser();
		logger.info("syncOrgUserInfo finished============");
	}
	
	public void runOnce() {
		String initRun = System.getProperty("initRun", "false");
		boolean bInitRun = false;
		try {
			bInitRun = Boolean.valueOf(initRun);
		}catch(Exception e) {
		}
		if(!bInitRun) {
			logger.info("initial running/runOnce skip...============");
			return;
		}
		logger.info("initial running/runOnce run============");
		runSyncOrg();
		runSyncUser();
		logger.info("runOnce finished============");
	}
	
	
	private void runSyncOrg() {
		logger.info("runSyncOrg");
		Map<String, Object> log = new HashMap<>();
		log.put("batchId", "runSyncOrg");
		try {
			int rowCount = 0;
			int tot = syncService.getMobisOrgListCount();
			int pgCount = tot / pgSize + (tot%pgSize>0 ? 1:0);
			List<MobisOrgData> targets = null;
			for(int i=0;i<pgCount;i++) {
				targets = syncService.getMobisOrgList(i+1, pgSize);
				syncService.syncOrgList(targets);
				rowCount += targets.size();
			}

			if(rowCount > 0){
				log.put("msg", "Success_runSyncOrg");
				log.put("msgCode", 200);
				syncService.insertBatchLog(log);
			}else{
				log.put("msg", "Fail_runSyncOrg");
				log.put("msgCode", 400);
				syncService.insertBatchLog(log);
				new Exception("fail sync runSyncOrg");
			}

		}catch(Exception e) {
			try{
				log.put("msg",  "Error insert runSyncOrg");
				log.put("msgCode", 400);
				syncService.insertBatchLog(log);
			}catch (Exception ex){
				logger.warn("runSyncOrg/{}", ex.getMessage());
			}
			logger.warn("runSyncOrg/{}",e.getMessage());
		}
	}
	
	private void runSyncUser() {
		logger.info("runSyncUser");
		Map<String, Object> log = new HashMap<>();
		log.put("batchId", "runSyncUser");
		try {
			int rowCount = 0;
			int tot = syncService.getMobisUserListCount();
			int pgCount = tot / pgSize + (tot%pgSize>0 ? 1:0);
			List<MobisUserData> targets = null;
			for(int i=0;i<pgCount;i++) {
				targets = syncService.getMobisUserList(i+1, pgSize);
				syncService.syncUserList(targets);
				rowCount += targets.size();
			}

			if(rowCount > 0){
				log.put("msg", "Success_runSyncUser");
				log.put("msgCode", 200);
				syncService.insertBatchLog(log);
			}else{
				log.put("msg", "Fail_runSyncUser");
				log.put("msgCode", 400);
				syncService.insertBatchLog(log);
				new Exception("fail sync runSyncUser");
			}

		}catch(Exception e) {
			try{
				log.put("msg",  "Error insert runSyncUser");
				log.put("msgCode", 400);
				syncService.insertBatchLog(log);
			}catch (Exception ex){
				logger.warn("runSyncUser /{}", ex.getMessage());
			}
			logger.warn("runSyncUser/{}",e.getMessage());
		}
	}
}
