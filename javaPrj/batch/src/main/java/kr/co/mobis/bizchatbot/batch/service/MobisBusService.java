package kr.co.mobis.bizchatbot.batch.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.bizchatbot.batch.config.BatchConfig;
import ai.mindslab.bizchatbot.batch.dao.BusOpMapper;
import kr.co.mobis.bizchatbot.batch.bus.dao.MobisBusRouteMapper;
import kr.co.mobis.bizchatbot.batch.dao.data.MobisBusRouteData;

@Service
public class MobisBusService {

	private static Logger logger = LoggerFactory.getLogger(MobisBusService.class);
	private static final int PG_SIZE = 100;

	@Autowired
	private MobisBusRouteMapper mobisBus;

	@Autowired
	private BusOpMapper bus;

	public MobisBusService() {
	}

	@Transactional(transactionManager="mobis-bus-route-sessionfactory", readOnly=true)
	public List<MobisBusRouteData> getBusRouteList(int pg, int pgSize) throws Exception{
		logger.info("getBusRouteList/{},{}", pg, pgSize);

		List<MobisBusRouteData> list = null;

		HashMap<String, String> params = new HashMap<>();
		params.put("pg", pg+"");
		params.put("pgSize", pgSize+"");
		list = mobisBus.getBusRoute(params);
		return list;
	}

	@Transactional(transactionManager="mobis-bus-route-sessionfactory", readOnly=true)
	public int getBusRouteListCount() throws Exception{
		logger.info("getBusRouteList");
		int tot = mobisBus.getBusRouteCount();
		return tot;
	}

	public int insertBusOp(MobisBusRouteData busData) throws Exception{
		int nRet = 0;

		nRet = (int)bus.insertBusOp(busData);

		return nRet;
	}

	public int insertBatchLog( Map<String, Object> param) throws Exception{
		int nRet = 0;
		nRet = bus.insertBatchLog(param);

		return nRet;
	}

	@Transactional(transactionManager=BatchConfig.TRANSACTION_MANAGER_MAIN)
	public int insertBusOpList(List<MobisBusRouteData> busList) throws Exception{
		logger.info("insertBusOpList/START");
		int nRet = 0;
		if(busList == null) {
			return nRet;
		}
		try {
			for(MobisBusRouteData data:busList) {
				nRet += insertBusOp(data);
			}
			logger.info("inserted bus route:{}", nRet);
		}catch(Exception e) {
			logger.warn("insertBusOpList/{}", e.getMessage());
			throw e;
		}finally {
			logger.info("insertBusOpList/END");
		}

		return nRet;
	}

	public void syncBusOp() {
		logger.info("syncBusOp/START");
		Map<String, Object> log = new HashMap<>();
		log.put("batchId", "syncBusOp");
		try {
			int nRet = 0;
			int insertCount  = 0;
			int tot = getBusRouteListCount();
			int pgCount = tot / PG_SIZE + (tot % PG_SIZE > 0 ? 1:0);
			for(int iter=0; iter<pgCount;iter++) {
				List<MobisBusRouteData> list = getBusRouteList(iter+1, PG_SIZE);
				nRet += insertBusOpList(list);
				insertCount += list.size();
			}

			logger.info("syncBusOp/sync. total count: {}", insertCount);

			if(insertCount > 0){
				log.put("msg", "Success_syncBusOp");
				log.put("msgCode", 200);
				nRet =  this.insertBatchLog(log);
			}else{
				log.put("msg", "Fail_syncBusOp");
				log.put("msgCode", 400);
				this.insertBatchLog(log);
				new Exception("fail sync syncBusOp");
			}

		}catch(Exception e) {
			try{
				log.put("msg",  "Error insert syncBusOp");
				log.put("msgCode", 400);
				this.insertBatchLog(log);
			}catch (Exception ex){
				logger.warn("syncBusOp/{}", ex.getMessage());
			}
			logger.warn("syncBusOp/{}", e.getMessage());
		}
		logger.info("syncBusOp/END");
	}

}
