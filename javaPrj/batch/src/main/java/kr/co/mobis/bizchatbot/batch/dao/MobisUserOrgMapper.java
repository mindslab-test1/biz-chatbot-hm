package kr.co.mobis.bizchatbot.batch.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import kr.co.mobis.bizchatbot.batch.dao.data.MobisOrgData;
import kr.co.mobis.bizchatbot.batch.dao.data.MobisUserData;

@Mapper
public interface MobisUserOrgMapper {

	public List<MobisUserData> getUserList(HashMap<String, Integer> params);
	public int getUserListCount();
	public List<MobisOrgData> getOrgList(HashMap<String, Integer> params);
	public int getOrgListCount();
	
//	public long insertChatbotUserList(MobisUserData data);
//	public long insertChatbotOrgList(MobisOrgData data);
}
