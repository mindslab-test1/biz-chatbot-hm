package kr.co.mobis.bizchatbot.batch.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.bizchatbot.batch.config.BatchConfig;
import ai.mindslab.bizchatbot.batch.dao.UserOrgMapper;
import kr.co.mobis.bizchatbot.batch.dao.MobisUserOrgMapper;
import kr.co.mobis.bizchatbot.batch.dao.data.MobisOrgData;
import kr.co.mobis.bizchatbot.batch.dao.data.MobisUserData;


@Service
public class MobisUserOrgSyncSerivce {

	private static Logger logger = LoggerFactory.getLogger(MobisUserOrgSyncSerivce.class);
	
	@Autowired
	private MobisUserOrgMapper mapper;
	
	@Autowired
	private UserOrgMapper chatbotMapper;
	
	@Transactional(transactionManager=BatchConfig.TRANSACTION_MANAGER_MOBIS, readOnly=true)
	public List<MobisUserData> getMobisUserList(int pg, int pgSize) throws Exception{
		logger.info("getMobisUserList/{},{}", pg, pgSize);
		
		List<MobisUserData> list = null;
		
		try {
			int tot = mapper.getUserListCount();
			if(tot > 0) {
				HashMap<String, Integer> params = new HashMap<>();
				params.put("pg", pg);
				params.put("pgSize", pgSize);
				
				list = mapper.getUserList(params);
			}
		}catch(Exception e) {
			logger.warn("getMobisUserList/{}", e.getMessage());
			throw e;
		}
		
		return list;
	}
	
	@Transactional(transactionManager=BatchConfig.TRANSACTION_MANAGER_MOBIS, readOnly=true)
	public int getMobisUserListCount() throws Exception{
		logger.info("getMobisUserListCount/");
		
		int tot = 0;
		try {
			tot = mapper.getUserListCount();
		}catch(Exception e) {
			logger.warn("getMobisUserList/{}", e.getMessage());
			throw e;
		}
		
		return tot;
	}
	
	@Transactional(transactionManager=BatchConfig.TRANSACTION_MANAGER_MOBIS, readOnly=true)
	public List<MobisOrgData> getMobisOrgList(int pg, int pgSize) throws Exception{
		logger.info("getMobisOrgList/{},{}", pg, pgSize);
		
		List<MobisOrgData> list = null;
		
		try {
			int tot = mapper.getOrgListCount();
			if(tot > 0) {
				HashMap<String, Integer> params = new HashMap<>();
				params.put("pg", pg);
				params.put("pgSize", pgSize);
				
				list = mapper.getOrgList(params);
			}
		}catch(Exception e) {
			logger.warn("getMobisOrgList/{}", e.getMessage());
			throw e;
		}
		
		return list;
	}
	
	@Transactional(transactionManager=BatchConfig.TRANSACTION_MANAGER_MOBIS, readOnly=true)
	public int getMobisOrgListCount() throws Exception{
		logger.info("getMobisOrgListCount/");
		
		int tot = 0;
		try {
			tot = mapper.getOrgListCount();
		}catch(Exception e) {
			logger.warn("getMobisOrgListCount/{}", e.getMessage());
			throw e;
		}
		
		return tot;
	}
	
	@Transactional(transactionManager=BatchConfig.TRANSACTION_MANAGER_MAIN)
	public long syncOrgList(List<MobisOrgData> list) throws Exception{
		logger.info("syncOrgList/START");
		if(list == null || list.size() == 0) {
			logger.info("No Org data to update");
			return 0;
		}
		long nRet = 0;
		for(int i=0;i<list.size();i++) {
			long ret = 0;
			MobisOrgData data = list.get(i);
			ret = chatbotMapper.insertChatbotOrgList(data);
			logger.debug("{}/ret={}",data,ret);
			nRet += ret;
		}
		
		logger.debug("tot updated count:{}",nRet);
		logger.info("syncOrgList/END");
		return nRet;
	}
	
	@Transactional(transactionManager=BatchConfig.TRANSACTION_MANAGER_MAIN)
	public long syncUserList(List<MobisUserData> list) throws Exception{
		logger.info("syncUserList/START");
		if(list == null || list.size() == 0) {
			logger.info("No User data to update");
			return 0;
		}
		long nRet = 0;
		for(int i=0;i<list.size();i++) {
			long ret = 0;
			MobisUserData data = list.get(i);
			ret = chatbotMapper.insertChatbotUserList(data);
			logger.debug("{}/ret={}",data,ret);
			nRet += ret;
		}
		
		logger.debug("tot updated count:{}",nRet);
		logger.info("syncUserList/END");
		return nRet;
	}

	public int insertBatchLog(Map<String, Object> param)throws Exception{
		return chatbotMapper.insertBatchLog(param);
	}
}
