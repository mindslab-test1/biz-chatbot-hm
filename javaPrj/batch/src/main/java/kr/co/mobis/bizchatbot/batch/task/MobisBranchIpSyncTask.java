package kr.co.mobis.bizchatbot.batch.task;

import kr.co.mobis.bizchatbot.batch.service.MobisBranchIpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;


@Component
public class MobisBranchIpSyncTask {
    private static Logger logger = LoggerFactory.getLogger(MobisBranchIpSyncTask.class);

    @Autowired
    MobisBranchIpService service;

    @PostConstruct
    public void initRun() {
        runOnce();
    }


    public void runOnce() {
        String initRun = System.getProperty("initRun", "false");
        boolean bInitRun = false;
        try {
            bInitRun = Boolean.valueOf(initRun);
        }catch(Exception e) {
        }
        if(!bInitRun) {
            logger.info("initial running/runOnce branchIp skip...============");
            return;
        }
        logger.info("initial running branchIp /runOnce run============");
        syncBranchIpInfo();
        logger.info("runOnce branchIp finished ============");
    }

    //매일 01:00:00
    @Scheduled(cron = "0 0 1 ? * *")
    public void syncBranchIpInfo() {
        logger.info("runBranchIp/START");
        this.service.syncBranchIp();
        logger.info("runBranchIp/END");
    }
}
