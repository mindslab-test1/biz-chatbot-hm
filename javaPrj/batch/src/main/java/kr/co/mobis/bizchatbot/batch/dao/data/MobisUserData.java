package kr.co.mobis.bizchatbot.batch.dao.data;

import java.io.Serializable;

public class MobisUserData implements Serializable{
	
	protected String wfName;
	protected String wfNo;
	protected String userId;
	protected String userNm;
	protected String userEnglishNm;
	protected String domain;
	protected String userGradeCode;
	protected String jwNm;
	protected String jwNmHome;
	protected String dutyNmHome;
	protected String dutyNmEng;
	protected String dutyCd;
	protected String userWerks;
	protected String userWerksTxt;
	protected String userDivCode;
	protected String userDivNm;
	protected String userSilCode;
	protected String userSilNm;
	protected String deptCd;
	protected String deptNm;
	protected String deptNmEng;
	protected String userPartCode;
	protected String userPartNm;
	protected String userCurrentCode;
	protected String userCurrentNm;
	protected String branchNm;
	protected String branchCd;
	protected String userEmail;
	protected String userMobile;
	protected String userTelephone;
	protected String userZzzip;
	protected String userZzzipText;
	protected String userPartId;
	protected String userTeamId;
	protected String userSilId;
	protected String userDivId;
	protected String retireFlag;
	protected String userFlag;
	protected String jobinfo;
	protected String jobdetail1;
	protected String jobdetail2;
	protected String jobdetail3;
	protected String userKoreaNm;
	protected String wfAplyDate;
	protected String wfAplyTime;
	protected String wfAplyYn;
	protected String wfAplySys;
	
	public MobisUserData() {
		
	}

	public String getWfName() {
		return wfName;
	}

	public void setWfName(String wfName) {
		this.wfName = wfName;
	}

	public String getWfNo() {
		return wfNo;
	}

	public void setWfNo(String wfNo) {
		this.wfNo = wfNo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserNm() {
		return userNm;
	}

	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}

	public String getUserEnglishNm() {
		return userEnglishNm;
	}

	public void setUserEnglishNm(String userEnglishNm) {
		this.userEnglishNm = userEnglishNm;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getUserGradeCode() {
		return userGradeCode;
	}

	public void setUserGradeCode(String userGradeCode) {
		this.userGradeCode = userGradeCode;
	}

	public String getJwNm() {
		return jwNm;
	}

	public void setJwNm(String jwNm) {
		this.jwNm = jwNm;
	}

	public String getJwNmHome() {
		return jwNmHome;
	}

	public void setJwNmHome(String jwNmHome) {
		this.jwNmHome = jwNmHome;
	}

	public String getDutyNmHome() {
		return dutyNmHome;
	}

	public void setDutyNmHome(String dutyNmHome) {
		this.dutyNmHome = dutyNmHome;
	}

	public String getDutyNmEng() {
		return dutyNmEng;
	}

	public void setDutyNmEng(String dutyNmEng) {
		this.dutyNmEng = dutyNmEng;
	}

	public String getDutyCd() {
		return dutyCd;
	}

	public void setDutyCd(String dutyCd) {
		this.dutyCd = dutyCd;
	}

	public String getUserWerks() {
		return userWerks;
	}

	public void setUserWerks(String userWerks) {
		this.userWerks = userWerks;
	}

	public String getUserWerksTxt() {
		return userWerksTxt;
	}

	public void setUserWerksTxt(String userWerksTxt) {
		this.userWerksTxt = userWerksTxt;
	}

	public String getUserDivCode() {
		return userDivCode;
	}

	public void setUserDivCode(String userDivCode) {
		this.userDivCode = userDivCode;
	}

	public String getUserDivNm() {
		return userDivNm;
	}

	public void setUserDivNm(String userDivNm) {
		this.userDivNm = userDivNm;
	}

	public String getUserSilCode() {
		return userSilCode;
	}

	public void setUserSilCode(String userSilCode) {
		this.userSilCode = userSilCode;
	}

	public String getUserSilNm() {
		return userSilNm;
	}

	public void setUserSilNm(String userSilNm) {
		this.userSilNm = userSilNm;
	}

	public String getDeptCd() {
		return deptCd;
	}

	public void setDeptCd(String deptCd) {
		this.deptCd = deptCd;
	}

	public String getDeptNm() {
		return deptNm;
	}

	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}

	public String getDeptNmEng() {
		return deptNmEng;
	}

	public void setDeptNmEng(String deptNmEng) {
		this.deptNmEng = deptNmEng;
	}

	public String getUserPartCode() {
		return userPartCode;
	}

	public void setUserPartCode(String userPartCode) {
		this.userPartCode = userPartCode;
	}

	public String getUserPartNm() {
		return userPartNm;
	}

	public void setUserPartNm(String userPartNm) {
		this.userPartNm = userPartNm;
	}

	public String getUserCurrentCode() {
		return userCurrentCode;
	}

	public void setUserCurrentCode(String userCurrentCode) {
		this.userCurrentCode = userCurrentCode;
	}

	public String getUserCurrentNm() {
		return userCurrentNm;
	}

	public void setUserCurrentNm(String userCurrentNm) {
		this.userCurrentNm = userCurrentNm;
	}

	public String getBranchNm() {
		return branchNm;
	}

	public void setBranchNm(String branchNm) {
		this.branchNm = branchNm;
	}

	public String getBranchCd() {
		return branchCd;
	}

	public void setBranchCd(String branchCd) {
		this.branchCd = branchCd;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getUserTelephone() {
		return userTelephone;
	}

	public void setUserTelephone(String userTelephone) {
		this.userTelephone = userTelephone;
	}

	public String getUserZzzip() {
		return userZzzip;
	}

	public void setUserZzzip(String userZzzip) {
		this.userZzzip = userZzzip;
	}

	public String getUserZzzipText() {
		return userZzzipText;
	}

	public void setUserZzzipText(String userZzzipText) {
		this.userZzzipText = userZzzipText;
	}

	public String getUserPartId() {
		return userPartId;
	}

	public void setUserPartId(String userPartId) {
		this.userPartId = userPartId;
	}

	public String getUserTeamId() {
		return userTeamId;
	}

	public void setUserTeamId(String userTeamId) {
		this.userTeamId = userTeamId;
	}

	public String getUserSilId() {
		return userSilId;
	}

	public void setUserSilId(String userSilId) {
		this.userSilId = userSilId;
	}

	public String getUserDivId() {
		return userDivId;
	}

	public void setUserDivId(String userDivId) {
		this.userDivId = userDivId;
	}

	public String getRetireFlag() {
		return retireFlag;
	}

	public void setRetireFlag(String retireFlag) {
		this.retireFlag = retireFlag;
	}

	public String getUserFlag() {
		return userFlag;
	}

	public void setUserFlag(String userFlag) {
		this.userFlag = userFlag;
	}

	public String getJobinfo() {
		return jobinfo;
	}

	public void setJobinfo(String jobinfo) {
		this.jobinfo = jobinfo;
	}

	public String getJobdetail1() {
		return jobdetail1;
	}

	public void setJobdetail1(String jobdetail1) {
		this.jobdetail1 = jobdetail1;
	}

	public String getJobdetail2() {
		return jobdetail2;
	}

	public void setJobdetail2(String jobdetail2) {
		this.jobdetail2 = jobdetail2;
	}

	public String getJobdetail3() {
		return jobdetail3;
	}

	public void setJobdetail3(String jobdetail3) {
		this.jobdetail3 = jobdetail3;
	}

	public String getUserKoreaNm() {
		return userKoreaNm;
	}

	public void setUserKoreaNm(String userKoreaNm) {
		this.userKoreaNm = userKoreaNm;
	}

	public String getWfAplyDate() {
		return wfAplyDate;
	}

	public void setWfAplyDate(String wfAplyDate) {
		this.wfAplyDate = wfAplyDate;
	}

	public String getWfAplyTime() {
		return wfAplyTime;
	}

	public void setWfAplyTime(String wfAplyTime) {
		this.wfAplyTime = wfAplyTime;
	}

	public String getWfAplyYn() {
		return wfAplyYn;
	}

	public void setWfAplyYn(String wfAplyYn) {
		this.wfAplyYn = wfAplyYn;
	}

	public String getWfAplySys() {
		return wfAplySys;
	}

	public void setWfAplySys(String wfAplySys) {
		this.wfAplySys = wfAplySys;
	}

	@Override
	public String toString() {
		return "MobisUserData [wfName=" + wfName + ", wfNo=" + wfNo + ", userId=" + userId + ", userNm=" + userNm
				+ ", userEnglishNm=" + userEnglishNm + ", domain=" + domain + ", userGradeCode=" + userGradeCode
				+ ", jwNm=" + jwNm + ", jwNmHome=" + jwNmHome + ", dutyNmHome=" + dutyNmHome + ", dutyNmEng="
				+ dutyNmEng + ", dutyCd=" + dutyCd + ", userWerks=" + userWerks + ", userWerksTxt=" + userWerksTxt
				+ ", userDivCode=" + userDivCode + ", userDivNm=" + userDivNm + ", userSilCode=" + userSilCode
				+ ", userSilNm=" + userSilNm + ", deptCd=" + deptCd + ", deptNm=" + deptNm + ", deptNmEng=" + deptNmEng
				+ ", userPartCode=" + userPartCode + ", userPartNm=" + userPartNm + ", userCurrentCode="
				+ userCurrentCode + ", userCurrentNm=" + userCurrentNm + ", branchNm=" + branchNm + ", branchCd="
				+ branchCd + ", userEmail=" + userEmail + ", userMobile=" + userMobile + ", userTelephone="
				+ userTelephone + ", userZzzip=" + userZzzip + ", userZzzipText=" + userZzzipText + ", userPartId="
				+ userPartId + ", userTeamId=" + userTeamId + ", userSilId=" + userSilId + ", userDivId=" + userDivId
				+ ", retireFlag=" + retireFlag + ", userFlag=" + userFlag + ", jobinfo=" + jobinfo + ", jobdetail1="
				+ jobdetail1 + ", jobdetail2=" + jobdetail2 + ", jobdetail3=" + jobdetail3 + ", userKoreaNm="
				+ userKoreaNm + ", wfAplyDate=" + wfAplyDate + ", wfAplyTime=" + wfAplyTime + ", wfAplyYn=" + wfAplyYn
				+ ", wfAplySys=" + wfAplySys + "]";
	}
}
