package kr.co.mobis.bizchatbot.batch.dao.data;

import java.io.Serializable;

public class MobisBusRouteData implements Serializable{

	protected int opNo;
	protected String opName;
	protected String opKind;
	protected String opWeek;
	protected String busKind;
	protected String opStartTime;
	protected String opEndTime;
	protected String startStation;
	protected String endStation;
	protected String note;
	protected String route;
	
	public MobisBusRouteData() {
	}

	public int getOpNo() {
		return opNo;
	}

	public void setOpNo(int opNo) {
		this.opNo = opNo;
	}

	public String getOpName() {
		return opName;
	}

	public void setOpName(String opName) {
		this.opName = opName;
	}

	public String getOpKind() {
		return opKind;
	}

	public void setOpKind(String opKind) {
		this.opKind = opKind;
	}

	public String getOpWeek() {
		return opWeek;
	}

	public void setOpWeek(String opWeek) {
		this.opWeek = opWeek;
	}

	public String getBusKind() {
		return busKind;
	}

	public void setBusKind(String busKind) {
		this.busKind = busKind;
	}

	public String getOpStartTime() {
		return opStartTime;
	}

	public void setOpStartTime(String opStartTime) {
		this.opStartTime = opStartTime;
	}

	public String getOpEndTime() {
		return opEndTime;
	}

	public void setOpEndTime(String opEndTime) {
		this.opEndTime = opEndTime;
	}

	public String getStartStation() {
		return startStation;
	}

	public void setStartStation(String startStation) {
		this.startStation = startStation;
	}

	public String getEndStation() {
		return endStation;
	}

	public void setEndStation(String endStation) {
		this.endStation = endStation;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	@Override
	public String toString() {
		return "MobisBusRouteData [opNo=" + opNo + ", opName=" + opName + ", opKind=" + opKind + ", opWeek=" + opWeek
				+ ", busKind=" + busKind + ", opStartTime=" + opStartTime + ", opEndTime=" + opEndTime
				+ ", startStation=" + startStation + ", endStation=" + endStation + ", note=" + note + ", route="
				+ route + "]";
	}

}
