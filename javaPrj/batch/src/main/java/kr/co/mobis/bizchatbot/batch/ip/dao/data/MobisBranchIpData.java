package kr.co.mobis.bizchatbot.batch.ip.dao.data;

import java.io.Serializable;

public class MobisBranchIpData implements Serializable {
    private int id;
    private String spotName;
    private String spotName2;
    private String spotType;
    private int vlanId;
    private String  adaptorName;
    private String startIp;
    private String endIp;
    private String gateWay;
    private String dnsMain;
    private String dnsSub;
    private String netMask;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpotName() {
        return spotName;
    }

    public void setSpotName(String spotName) {
        this.spotName = spotName;
    }

    public String getSpotName2() {
        return spotName2;
    }

    public void setSpotName2(String spotName2) {
        this.spotName2 = spotName2;
    }

    public String getSpotType() {
        return spotType;
    }

    public void setSpotType(String spotType) {
        this.spotType = spotType;
    }

    public int getVlanId() {
        return vlanId;
    }

    public void setVlanId(int vlanId) {
        this.vlanId = vlanId;
    }

    public String getAdaptorName() {
        return adaptorName;
    }

    public void setAdaptorName(String adaptorName) {
        this.adaptorName = adaptorName;
    }

    public String getStartIp() {
        return startIp;
    }

    public void setStartIp(String startIp) {
        this.startIp = startIp;
    }

    public String getEndIp() {
        return endIp;
    }

    public void setEndIp(String endIp) {
        this.endIp = endIp;
    }

    public String getGateWay() {
        return gateWay;
    }

    public void setGateWay(String gateWay) {
        this.gateWay = gateWay;
    }

    public String getDnsMain() {
        return dnsMain;
    }

    public void setDnsMain(String dnsMain) {
        this.dnsMain = dnsMain;
    }

    public String getDnsSub() {
        return dnsSub;
    }

    public void setDnsSub(String dnsSub) {
        this.dnsSub = dnsSub;
    }

    public String getNetMask() {
        return netMask;
    }

    public void setNetMask(String netMask) {
        this.netMask = netMask;
    }

    @Override
    public String toString() {
        StringBuilder str =  new StringBuilder();
        str.append("id+ "+ this.getId() +", ");
        str.append("spotName+ "+ this.getSpotName() +", ");
        str.append("spotName1+ "+ this.getSpotName2() +", ");
        str.append("vlanId+ "+ this.getVlanId() +", ");
        str.append("adaptorName+ "+this. getAdaptorName() +", ");
        str.append("startIp+ "+ this.getStartIp() +", ");
        str.append("endIp+ "+ this.getEndIp() +", ");
        str.append("gateWay+ "+ this.getGateWay() +", ");
        str.append("dnsMain+ "+ this.getDnsMain() +", ");
        str.append("dnssSub+ "+ this.getDnsSub() +", ");
        str.append("netMask+ "+ this.getNetMask() +", ");
        return str.toString();
    }
}
