package kr.co.mobis.bizchatbot.batch.service;

import ai.mindslab.bizchatbot.batch.config.BatchConfig;
import ai.mindslab.bizchatbot.batch.dao.BranchIpMapper;
import kr.co.mobis.bizchatbot.batch.dao.data.MobisBusRouteData;
import kr.co.mobis.bizchatbot.batch.ip.dao.MobisBranchIpMapper;
import kr.co.mobis.bizchatbot.batch.ip.dao.data.MobisBranchIpData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MobisBranchIpService {

    private static Logger logger = LoggerFactory.getLogger(MobisBranchIpService.class);
    private static final int PG_SIZE = 100;

    @Autowired
    BranchIpMapper branchIpMapper;

    @Autowired
    MobisBranchIpMapper mobisBranchIpMapper;


    @Transactional(transactionManager="mobis-ip-route-sessionfactory", readOnly=true)
    public List<MobisBranchIpData> getBranchIpList(int pg, int pgSize) throws Exception {

        logger.info("getBranchIpList/{},{}", pg, pgSize);

        List<MobisBranchIpData> list = null;

        Map<String, Object> params = new HashMap<>();
        params.put("start", (pg*100));
        params.put("end", (pg+1)*pgSize);
        list = mobisBranchIpMapper.getSpotIpList(params);

        return list;
    }


    public int getBranchIpListCount(){
        int res = 0;

        try {
            res = mobisBranchIpMapper.getBranchIpListCount();
        }catch (Exception e) {
            logger.error("error getBranchCount : " + e.getMessage());
        }
        return res;
    }

    @Transactional(transactionManager= BatchConfig.TRANSACTION_MANAGER_MAIN)
    public int insertBranchIpList(List<MobisBranchIpData> branchList) throws Exception{
        logger.info("insertBusOpList/START");
        int nRet = 0;
        if(branchList == null) {
            return nRet;
        }
        try {
            for(MobisBranchIpData data:branchList) {
                nRet += insertBranchIp(data);
            }
            logger.info("inserted branch ip:{}", nRet);
        }catch(Exception e) {
            logger.warn("insertBusOpList/{}", e.getMessage());
            throw e;
        }finally {
            logger.info("insertBusOpList/END");
        }

        return nRet;
    }


    public int insertBranchIp(MobisBranchIpData branchData) throws Exception{
        int nRet = 0;

        nRet = branchIpMapper.insertBranchIpList(branchData);

        return nRet;
    }

    public int insertBatchLog( Map<String, Object> param) throws Exception{
        int nRet = 0;
        logger.info("syncBranchIp/ insert batchLog");
        nRet = branchIpMapper.insertBatchLog(param);
        return nRet;
    }


    public void syncBranchIp() {
        logger.info("syncBranchIp/START");

        Map<String, Object> log = new HashMap<>();
        log.put("batchId", "syncBranchIp");
        int nRet = 0;
        try {

            int insertCount  = 0;
            int tot = getBranchIpListCount();
            int pgCount = tot / PG_SIZE + (tot % PG_SIZE > 0 ? 1:0);

            for(int index=0; index < pgCount; index++) {
                List<MobisBranchIpData> list = getBranchIpList(index, PG_SIZE);
                nRet += insertBranchIpList(list);
                insertCount += list.size();
            }
            logger.info("syncBranchIp/sync. total count: {}", insertCount);

            if(insertCount > 0){
                log.put("msg", "Success_syncBranchIp");
                log.put("msgCode", 200);
                nRet =  this.insertBatchLog(log);
            }else{
                log.put("msg", "Fail_syncBranchIp");
                log.put("msgCode", 400);
                this.insertBatchLog(log);
                new Exception("fail sync syncBranchIp");
            }


        }catch(Exception e) {
            try{
                log.put("msg",  "Error init syncBranchIp");
                log.put("msgCode", 400);
                this.insertBatchLog(log);
            }catch (Exception ex){
                logger.warn("syncBranchIp/{}", ex.getMessage());
            }

            logger.warn("syncBranchIp/{}", e.getMessage());
        }
        logger.info("syncBranchIp/END");
    }
}
