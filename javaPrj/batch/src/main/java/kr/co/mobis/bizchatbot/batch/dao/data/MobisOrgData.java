package kr.co.mobis.bizchatbot.batch.dao.data;

import java.io.Serializable;

public class MobisOrgData implements Serializable{

	protected String objid;
	protected String stext;
	protected String parObjid;
	protected String zorgtxEngl;
	protected String zorgehLevl;
	protected String levelText;
	protected String priox;
	protected String managerPernr;
	protected String enddt;
	protected String ifid;
	protected String ifjobnumber;
	protected String createtime;
	protected String updatetime;
	protected String kostl;
	protected String ktext;
	
	protected MobisOrgData() {
		
	}

	public String getObjid() {
		return objid;
	}

	public void setObjid(String objid) {
		this.objid = objid;
	}

	public String getStext() {
		return stext;
	}

	public void setStext(String stext) {
		this.stext = stext;
	}

	public String getParObjid() {
		return parObjid;
	}

	public void setParObjid(String parObjid) {
		this.parObjid = parObjid;
	}

	public String getZorgtxEngl() {
		return zorgtxEngl;
	}

	public void setZorgtxEngl(String zorgtxEngl) {
		this.zorgtxEngl = zorgtxEngl;
	}

	public String getZorgehLevl() {
		return zorgehLevl;
	}

	public void setZorgehLevl(String zorgehLevl) {
		this.zorgehLevl = zorgehLevl;
	}

	public String getLevelText() {
		return levelText;
	}

	public void setLevelText(String levelText) {
		this.levelText = levelText;
	}

	public String getPriox() {
		return priox;
	}

	public void setPriox(String priox) {
		this.priox = priox;
	}

	public String getManagerPernr() {
		return managerPernr;
	}

	public void setManagerPernr(String managerPernr) {
		this.managerPernr = managerPernr;
	}

	public String getEnddt() {
		return enddt;
	}

	public void setEnddt(String enddt) {
		this.enddt = enddt;
	}

	public String getIfid() {
		return ifid;
	}

	public void setIfid(String ifid) {
		this.ifid = ifid;
	}

	public String getIfjobnumber() {
		return ifjobnumber;
	}

	public void setIfjobnumber(String ifjobnumber) {
		this.ifjobnumber = ifjobnumber;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}

	public String getKostl() {
		return kostl;
	}

	public void setKostl(String kostl) {
		this.kostl = kostl;
	}

	public String getKtext() {
		return ktext;
	}

	public void setKtext(String ktext) {
		this.ktext = ktext;
	}

	@Override
	public String toString() {
		return "MobisOrgData [objid=" + objid + ", stext=" + stext + ", parObjid=" + parObjid + ", zorgtxEngl="
				+ zorgtxEngl + ", zorgehLevl=" + zorgehLevl + ", levelText=" + levelText + ", priox=" + priox
				+ ", managerPernr=" + managerPernr + ", enddt=" + enddt + ", ifid=" + ifid + ", ifjobnumber="
				+ ifjobnumber + ", createtime=" + createtime + ", updatetime=" + updatetime + ", kostl=" + kostl
				+ ", ktext=" + ktext + "]";
	}
}
