package kr.co.mobis.bizchatbot.batch.bus.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import kr.co.mobis.bizchatbot.batch.dao.data.MobisBusRouteData;

@Mapper
public interface MobisBusRouteMapper {
	public List<MobisBusRouteData> getBusRoute(HashMap<String,String> params);
	public int getBusRouteCount();
}
