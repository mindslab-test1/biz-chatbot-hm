package kr.co.mobis.bizchatbot.batch.ip.dao;


import kr.co.mobis.bizchatbot.batch.ip.dao.data.MobisBranchIpData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface MobisBranchIpMapper {

    int getBranchIpListCount() throws Exception;
    List<MobisBranchIpData> getSpotIpList(Map<String, Object> param) throws Exception;
}
