package ai.mindslab.bizchatbot.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackages={"ai.mindslab.bizchatbot.batch", "kr.co.mobis.bizchatbot.batch"})
@EnableAsync
public class Application {

	private static Logger logger = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args) {
		logger.info("==============maumChatbot Batch Process started.==============");
		SpringApplication.run(Application.class, args);
	}
}
