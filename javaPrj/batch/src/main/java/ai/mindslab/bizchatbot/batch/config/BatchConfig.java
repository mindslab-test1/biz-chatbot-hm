package ai.mindslab.bizchatbot.batch.config;

import java.util.concurrent.Executor;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;


@Configuration
//@MapperScan(basePackages= {"ai.mindslab.bizchatbot.batch"})
//@EnableScheduling
//@EnableAsync
public class BatchConfig {

	public static final String DATASOURCE_MAIN = "dataSource1";
	public static final String DATASOURCE_MOBIS = "mobisDatasource";
	
	public static final String TRANSACTION_MANAGER_MAIN = "transactionManager1";
	public static final String TRANSACTION_MANAGER_MOBIS = "mobisTransactionManager";
	
	public static final String TRANSACTION_SESSION_FACTORY_MAIN = "sqlSessionFactory1";
	public static final String TRANSACTION_SESSION_FACTORY_MOBIS = "mobisSqlSessionFactory";
	
	Logger logger = LoggerFactory.getLogger(BatchConfig.class);
	

	@Bean
	public TaskScheduler taskScheduler() {
		ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
		scheduler.setPoolSize(10);
		scheduler.setWaitForTasksToCompleteOnShutdown(true);
		return scheduler;
	}
	
	@Bean
	public Executor taskExecutor() {
		return new SimpleAsyncTaskExecutor();
	}
}
