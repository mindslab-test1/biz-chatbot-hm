package ai.mindslab.bizchatbot.batch.service;

import ai.mindslab.bizchatbot.batch.config.adminuser.AdminUserLockTask;
import ai.mindslab.bizchatbot.batch.dao.UserSecureConfirmDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserSecureConfirmService {

    private static Logger logger = LoggerFactory.getLogger(UserSecureConfirmService.class);

    @Autowired
    UserSecureConfirmDao dao;

    public void initUserSecureinit() {
        Map<String, Object> param = new HashMap<>();
        Map<String, Object> log = new HashMap<>();

        try{
            List<String> userList = dao.getSecuerConfirmUser();

            if(userList !=null &&  userList.size() > 0){
                log.put("batchId", "initUserSecureConfirm");
                param.put("userIds", userList);
                int result = dao.initUserSecureConfirm(param);

                if(result  > 0){
                    logger.info("success init UserSecureConfirm");
                    log.put("msg", "Success_initUserSecureConfirm");
                    log.put("msgCode", 200);
                    dao.insertBatchLog(log);
                }else {
                    log.put("msg", "Fail_initUserSecureConfirm");
                    log.put("msgCode", 400);
                    dao.insertBatchLog(log);
                    new Exception("fail init UserSecureConfirm");
                }
            }

        }catch (Exception e){
            try{
                log.put("msg",  "Error init UserSecureConfirm");
                log.put("msgCode", 400);
                dao.insertBatchLog(log);
            }catch (Exception ex){
                logger.warn("initUserSecureConfirm /{}", ex.getMessage());
            }
            logger.error("error, {}", e.getMessage());
        }
    }
}
