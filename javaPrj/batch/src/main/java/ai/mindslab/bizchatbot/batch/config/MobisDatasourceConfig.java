package ai.mindslab.bizchatbot.batch.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@MapperScan(basePackages= {"kr.co.mobis.bizchatbot.batch.dao"},sqlSessionFactoryRef=BatchConfig.TRANSACTION_SESSION_FACTORY_MOBIS)
public class MobisDatasourceConfig {

	@Autowired
	private MybatisConfigurationSupport mybatisConfigurationSupport;
	
	@Bean(name=BatchConfig.DATASOURCE_MOBIS, destroyMethod="close")
	@ConfigurationProperties(prefix="mobisuser.datasource")
	public DataSource mobisDataSource() {
		DataSource ds = DataSourceBuilder.create().build();
		return ds;
	}
	
	@Bean(name=BatchConfig.TRANSACTION_MANAGER_MOBIS)
	public PlatformTransactionManager mobisTransactionManager() {
		return new DataSourceTransactionManager(mobisDataSource());
	}
	
	@Bean(name=BatchConfig.TRANSACTION_SESSION_FACTORY_MOBIS)
	public SqlSessionFactory mobisSqlSessionFactory(@Qualifier(BatchConfig.DATASOURCE_MOBIS) DataSource dataSource) throws Exception{
		return mybatisConfigurationSupport.build(dataSource);
	}
	
	@Bean(name="MOBIS_USER_SESSION_TEMPLATE")
    public SqlSessionTemplate mobisSqlSessionTemplate(@Qualifier(BatchConfig.TRANSACTION_SESSION_FACTORY_MOBIS)SqlSessionFactory sqlSessionFactory) throws Exception{
    		return new SqlSessionTemplate(sqlSessionFactory);
    }

}
