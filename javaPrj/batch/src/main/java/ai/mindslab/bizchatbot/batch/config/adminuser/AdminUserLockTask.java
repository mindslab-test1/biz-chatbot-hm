package ai.mindslab.bizchatbot.batch.config.adminuser;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ai.mindslab.bizchatbot.batch.service.AdminUserLockService;

@Component
public class AdminUserLockTask {

	private static Logger logger = LoggerFactory.getLogger(AdminUserLockTask.class);
	
	@Autowired
	private AdminUserLockService lockService;
	
	@PostConstruct
	public void initRun() {
		runOnce();
	}
	
	@Scheduled(cron = "0 5 * * * *")
//	@Scheduled(fixedRate=5000)// test
	public void userLocking() {
		logger.info("userLocking run - not logined for last 90 days");
		userHibernate();
	}
	
	public void runOnce() {
		String initRun = System.getProperty("initRun", "false");
		boolean bInitRun = false;
		try {
			bInitRun = Boolean.valueOf(initRun);
		}catch(Exception e) {
		}
		if(!bInitRun) {
			logger.info("initial running/runOnce skip...============");
			return;
		}
		logger.info("initial running/runOnce run - not logined for last 90 days");
		userHibernate();
	}
	
	
	private void userHibernate() {
		logger.info("userHibernate/START");
		
		lockService.runAdminUserHibernate();
		
		logger.info("userHibernate/END");
	}
}
