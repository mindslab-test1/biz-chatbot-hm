package ai.mindslab.bizchatbot.batch.dao;

import org.apache.ibatis.annotations.Mapper;

import kr.co.mobis.bizchatbot.batch.dao.data.MobisBusRouteData;

import java.util.Map;

@Mapper
public interface BusOpMapper {

	long insertBusOp(MobisBusRouteData busOp);
	int insertBatchLog(Map<String, Object> param)throws Exception;
}
