package ai.mindslab.bizchatbot.batch.dao;

import kr.co.mobis.bizchatbot.batch.ip.dao.data.MobisBranchIpData;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface BranchIpMapper {

    int insertBranchIpList(MobisBranchIpData list) throws Exception;
    int insertBatchLog(Map<String, Object> param)throws Exception;
}
