package ai.mindslab.bizchatbot.batch.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@MapperScan(basePackages= {"kr.co.mobis.bizchatbot.batch.bus.dao"},sqlSessionFactoryRef="mobis-bus-route-sessionfactory")
public class MobisBusDatasourceConfig {

	@Autowired
	private MybatisConfigurationSupport mybatisConfigurationSupport;
	
	@Bean(name="busDataSource", destroyMethod="close")
	@ConfigurationProperties(prefix="mobisbus.datasource")
	public DataSource busDataSource() {
		DataSource ds = DataSourceBuilder.create().build();
		return ds;
	}
	
	@Bean(name="mobis-bus-route-transactionmanager")
	public PlatformTransactionManager busTransactionManager() {
		return new DataSourceTransactionManager(busDataSource());
	}
	
	@Bean(name="mobis-bus-route-sessionfactory")
	public SqlSessionFactory busSqlSessionFactory(@Qualifier("busDataSource") DataSource dataSource) throws Exception{
		return mybatisConfigurationSupport.build(dataSource);
	}
	
	@Bean(name="mobis-bus-route-sessiontemplate")
    public SqlSessionTemplate busSqlSessionTemplate(@Qualifier("mobis-bus-route-sessionfactory")SqlSessionFactory sqlSessionFactory) throws Exception{
    		return new SqlSessionTemplate(sqlSessionFactory);
    }

}
