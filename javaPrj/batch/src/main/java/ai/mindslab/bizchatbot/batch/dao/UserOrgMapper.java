package ai.mindslab.bizchatbot.batch.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import kr.co.mobis.bizchatbot.batch.dao.data.MobisOrgData;
import kr.co.mobis.bizchatbot.batch.dao.data.MobisUserData;

@Mapper
public interface UserOrgMapper {

	public long insertChatbotUserList(MobisUserData data);
	public long insertChatbotOrgList(MobisOrgData data);
	int insertBatchLog(Map<String, Object> param)throws Exception;
}
