package ai.mindslab.bizchatbot.batch.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages= {"kr.co.mobis.bizchatbot.batch.ip.dao"},sqlSessionFactoryRef="mobis-ip-route-sessionfactory")
public class MobisbranchIpDatasourceConfig {

	@Autowired
	private MybatisConfigurationSupport mybatisConfigurationSupport;
	
	@Bean(name="branchDataSource", destroyMethod="close")
	@ConfigurationProperties(prefix="mobisbranch.ip.datasource")
	public DataSource branchDataSource() {
		DataSource ds = DataSourceBuilder.create().build();
		return ds;
	}
	
	@Bean(name="mobis-ip-route-transactionmanager")
	public PlatformTransactionManager branchTransactionManager() {
		return new DataSourceTransactionManager(branchDataSource());
	}
	
	@Bean(name="mobis-ip-route-sessionfactory")
	public SqlSessionFactory branchSqlSessionFactory(@Qualifier("branchDataSource") DataSource dataSource) throws Exception{
		return mybatisConfigurationSupport.build(dataSource);
	}
	
	@Bean(name="mobis-ip-route-sessiontemplate")
    public SqlSessionTemplate branchSqlSessionTemplate(@Qualifier("mobis-ip-route-sessionfactory")SqlSessionFactory sqlSessionFactory) throws Exception{
    		return new SqlSessionTemplate(sqlSessionFactory);
    }

}
