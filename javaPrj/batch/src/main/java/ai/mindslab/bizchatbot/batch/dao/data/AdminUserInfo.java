package ai.mindslab.bizchatbot.batch.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class AdminUserInfo implements Serializable{
	
	protected String userId;
	protected String username;
	protected int teamId;
	protected String roleId;
	protected String isHibernated = "N";
	
	public AdminUserInfo() {
		
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getIsHibernated() {
		return isHibernated;
	}

	public void setIsHibernated(String isHibernated) {
		this.isHibernated = isHibernated;
	}

	@Override
	public String toString() {
		return "AdminUserInfo [userId=" + userId + ", username=" + username + ", teamId=" + teamId + ", roleId="
				+ roleId + ", isHibernated=" + isHibernated + "]";
	}

}
