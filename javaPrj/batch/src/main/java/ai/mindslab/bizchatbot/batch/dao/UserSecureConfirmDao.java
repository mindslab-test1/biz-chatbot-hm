package ai.mindslab.bizchatbot.batch.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserSecureConfirmDao {
    List<String> getSecuerConfirmUser() throws Exception;
    int initUserSecureConfirm(Map<String, Object> param) throws Exception;
    int insertBatchLog(Map<String, Object> param)throws Exception;
}
