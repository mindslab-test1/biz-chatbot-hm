package ai.mindslab.bizchatbot.batch.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.bizchatbot.batch.config.BatchConfig;
import ai.mindslab.bizchatbot.batch.dao.AdminUserInfoMapper;
import ai.mindslab.bizchatbot.batch.dao.data.AdminUserInfo;

@Service
public class AdminUserLockService {

	private static Logger logger = LoggerFactory.getLogger(AdminUserLockService.class);
	
	@Autowired
	private AdminUserInfoMapper adminUserDao;
	
	@Transactional(transactionManager=BatchConfig.TRANSACTION_MANAGER_MAIN)
	public List<AdminUserInfo> getHibernateTargets(){
		logger.info("getHibernateTargets/START/");
		
		List<AdminUserInfo> targets = null;
		try {
			targets = adminUserDao.getHibernateTargetList();
		}catch(Exception e) {
			logger.warn("getHibernateTargets exception {}", e.getMessage());
		}finally {
			logger.info("getHibernateTargets/END/");
		}
		return targets;
	}
	
	@Transactional(transactionManager=BatchConfig.TRANSACTION_MANAGER_MAIN)
	public long updateHibernateTargets(List<AdminUserInfo> targets) throws Exception{
		logger.info("updateHibernateTargets/START/");
		long nRet = 0;
		StringBuilder buff = new StringBuilder();
		ArrayList<String> userIds = new ArrayList<>();
		try {
			if(targets != null && targets.size() > 0) {
				logger.info("Targets size:{}", targets.size());
				
				for(AdminUserInfo user: targets) {
					buff.append(user.getUserId()).append(",");
					
					userIds.add(user.getUserId());
				}
				logger.info(buff.toString());
				
				HashMap<String, List<String>> params = new HashMap<>();
				params.put("users", userIds);
				nRet = adminUserDao.updateAdminUserHibernate(params);
			}
		}catch(Exception e) {
			logger.warn("updateHibernateTargets exception {}", e.getMessage());
			//throw e;
		}finally {
			logger.info("updateHibernateTargets/END/");
		}
		return nRet;
	}
	
	
	public void runAdminUserHibernate() {
		logger.info("runAdminUserHibernate/START/");
		List<AdminUserInfo> targets = getHibernateTargets();
		try {
			long nRet = updateHibernateTargets(targets);
			logger.info("updated count: {}", nRet);
		}catch(Exception e) {
			logger.warn("runAdminUserHibernate/{}", e.getMessage());
		}finally {
			logger.info("runAdminUserHibernate/END/");
		}
		
	}
	
}
