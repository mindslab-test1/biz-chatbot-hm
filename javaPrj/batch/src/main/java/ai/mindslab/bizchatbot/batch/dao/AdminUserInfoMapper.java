package ai.mindslab.bizchatbot.batch.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.batch.dao.data.AdminUserInfo;

@Mapper
public interface AdminUserInfoMapper {

	public List<AdminUserInfo> getHibernateTargetList();
	public long updateAdminUserHibernate(HashMap<String, List<String>> targetUsers);
}
