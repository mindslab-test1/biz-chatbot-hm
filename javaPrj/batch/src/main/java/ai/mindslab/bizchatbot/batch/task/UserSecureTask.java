package ai.mindslab.bizchatbot.batch.task;

import ai.mindslab.bizchatbot.batch.service.UserSecureConfirmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class UserSecureTask {
    private static Logger logger = LoggerFactory.getLogger(UserSecureTask.class);

    @Autowired
    UserSecureConfirmService service;

    @PostConstruct
    public void initRun() {
        /* 그냥 테스트용임.,... */
        // runOnce();
    }

    public void runOnce(){
        logger.info("initial UserSecureTask  running/runOnce run - rest user SecureConfirm Status");
        InitUserSecureConfrim();
    }

    @Scheduled(cron = "0 0 0 1 1 ?")
    public void runUserInitUserSecure() {
        logger.info("UserSecureTask run -  init Status");
        InitUserSecureConfrim();
    }

    public void InitUserSecureConfrim(){
        logger.info("UserSecureTask/START");
        service.initUserSecureinit();
        logger.info("UserSecureTask/END");
    }
}
