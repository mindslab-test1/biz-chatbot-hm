package first.view.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRespInterfaceRelData;
import ai.mindslab.bizchatbot.commons.utils.StrUtils;
import ai.mindslab.bizchatbot.sdsadmin.service.ChatbotAdminService;
import ai.mindslab.bizchatbot.sdsadmin.service.SdsAdminService;
import first.common.common.CommandMap;
import first.common.service.CommonService;
import first.common.util.AES256Util;
import first.common.util.CommonUtils;
import first.common.util.Ports;
import first.common.util.Unescape;
import first.view.service.ViewService;


@Controller
public class ViewController {	
	Logger log = LoggerFactory.getLogger(this.getClass());	
	private String key = "etri-dialog-key!@"; //key 16�� �̻�
	private String[] arrUsingPort = {};
	
	@Autowired
	@Qualifier("viewService")
	private ViewService viewService;
	
	@Autowired
	private ChatbotAdminService adminService;
	
	@Autowired
	private SdsAdminService sdsService;
	
	private ModelAndView SetMenu(ModelAndView mv, CommandMap commandMap, HttpServletRequest request) throws Exception {
		String depth1 = "0";
		String depth2 = "0";
    	String depth3 = "0";
    	
		if (mv.getViewName().contains("agent")) {
			depth1 = "1";									
		} else if (mv.getViewName().contains("slot")) {
			depth1 = "2";
		} else if (mv.getViewName().contains("useruttr")) {
			depth1 = "3";
		} else if (mv.getViewName().contains("know")) {
			depth1 = "4";
		} else if (mv.getViewName().contains("usermng")) {
			depth1 = "5";
		}				
		
		//���� �޴� ���� ��� 1������ ������ 1
		if(commandMap.get("depth2") != null && commandMap.get("depth3") != null) {
			if(commandMap.get("depth2").toString().equals("0") && commandMap.get("depth3").toString().equals("0")) {
				depth1 = commandMap.get("depth1").toString();
			}
			else {
				depth1 = "1";
			}
			depth2 = (String)commandMap.get("depth2");
			depth3 = (String)commandMap.get("depth3");			
		}
		
		boolean bLoginChk = true;
		if(request.getSession().getAttribute("USER_TYPE") != null){
			mv.addObject("USER_TYPE", request.getSession().getAttribute("USER_TYPE"));
			bLoginChk = false;
		}

		if (bLoginChk) {
			mv = new ModelAndView("index");
//			mv = new ModelAndView("redirect:/chatbotadmin");
			return mv;
		}

		// UNIQID
		if(commandMap.get("UNIQID") != null) {
			request.getSession().setAttribute("UNIQID", commandMap.get("UNIQID"));
		}

//		Cookie[] cookies = request.getCookies();
//    	if(cookies != null){
//            for(int i=0; i < cookies.length; i++) {
//                if (cookies[i].getName().equals("USER_TYPE")) {
//                	mv.addObject("USER_TYPE", cookies[i].getValue());
//                	bLoginChk = false;
//				}
//            }
//        }
		
		List<Map<String, Object>> prjlist = null;
		
		prjlist = SetProject(0, request);

		//QQQ
//		if (request.getSession().getAttribute("ServerPort") == null) {
//			request.getSession().setAttribute("ServerPort", viewService.GetPort(arrUsingPort));
//		}
		
		List<Map<String, Object>> submenulist = new ArrayList<Map<String, Object>>();
		if(request.getSession().getAttribute("ProjectNo") == null && prjlist.size() > 0) {
			request.getSession().setAttribute("ProjectNo", prjlist.get(0).get("SEQ_NO"));
			request.getSession().setAttribute("ProjectName", prjlist.get(0).get("PROJECT_NAME"));
			//request.getSession().setAttribute("ServerPort", prjlist.get(0).get("SERVER_PORT"));
		}
		else if(request.getSession().getAttribute("ProjectNo") != null && prjlist.size() == 0) {
			request.getSession().removeAttribute("ProjectNo");
			request.getSession().removeAttribute("ProjectName");
			//request.getSession().removeAttribute("ServerPort");
		}
		else {
			boolean bPrjChk = false;
			for (Map<String, Object> prjitem : prjlist) {
				/*if (commandMap.containsKey("PROJECT_NAME")) {
					if (prjitem.get("PROJECT_NAME").toString().equals(commandMap.get("PROJECT_NAME").toString())) {
						mv.addObject("SERVER_PORT", prjitem.get("SERVER_PORT"));
					}
				}*/
				
				if (prjitem.get("PROJECT_NAME").toString().equals(request.getSession().getAttribute("ProjectName").toString())) {
					bPrjChk = true;
					break;
				}
			}
			if (!bPrjChk) {
				request.getSession().removeAttribute("ProjectNo");
				request.getSession().setAttribute("ProjectName", "���õ� �������� �����ϴ�.");
			}
		}
		
		if (request.getSession().getAttribute("ProjectNo") != null) {
			CommandMap cmmap = new CommandMap();
			cmmap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
			submenulist = viewService.selectAgents(cmmap.getMap());
		}	
		
		if (commandMap.get("AGENT_SEQ") != null && !commandMap.get("AGENT_SEQ").toString().equals("0") && !commandMap.get("AGENT_SEQ").toString().isEmpty()) {
			mv.addObject("AGENT_SEQ", commandMap.get("AGENT_SEQ"));	
		}
    	    
		mv.addObject("depth1", depth1);
		mv.addObject("depth2", depth2);
		mv.addObject("depth3", depth3);		
		mv.addObject("prjlist", prjlist);
		mv.addObject("submenu", submenulist);
		
		return mv;
	}
		
	private List<Map<String, Object>> SetProject(int SeqNo, HttpServletRequest request) throws Exception {
		CommandMap cmmap = new CommandMap();
		cmmap.put("SEQ_NO", SeqNo);
		cmmap.put("USER_SEQ", request.getSession().getAttribute("USER_SEQ"));
		cmmap.put("USER_TYPE", request.getSession().getAttribute("USER_TYPE"));
		
//		Cookie[] cookies = request.getCookies();
//    	if(cookies != null){
//            for(int i=0; i < cookies.length; i++){
//                if (cookies[i].getName().equals("USER_SEQ")) {
//                	cmmap.put("USER_SEQ", cookies[i].getValue());
//				}
//                else if (cookies[i].getName().equals("USER_TYPE")) {
//                	cmmap.put("USER_TYPE", cookies[i].getValue());
//				}
//                else if (cookies[i].getName().equals("USER_NAME")) {
//                	request.getSession().setAttribute("USER_NAME", Unescape.unescape(cookies[i].getValue()));
//                }
//                else if (cookies[i].getName().equals("ProjectNo")) {
//                	request.getSession().setAttribute("ProjectNo", cookies[i].getValue());
//                }
//                else if (cookies[i].getName().equals("ProjectName")) {
//                	request.getSession().setAttribute("ProjectName", cookies[i].getValue());
//                }
//                /*else if (cookies[i].getName().equals("ServerPort")) {
//                	request.getSession().setAttribute("ServerPort", cookies[i].getValue());
//                }*/
//                else if (cookies[i].getName().equals("UNIQID")) {
//                	request.getSession().setAttribute("UNIQID", cookies[i].getValue());
//                }
//            }
//        }
		List<Map<String, Object>> prjlist = viewService.selectProject(cmmap.getMap());
		return prjlist;
	}
		
	@RequestMapping(value="/view/setprojectlist.do")
    public ModelAndView setProjectList(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/agentlist.do");
		
//		Cookie[] cookies = request.getCookies();
//    	if(cookies != null){
//            for(int i=0; i < cookies.length; i++){
//            	if (cookies[i].getName().equals("ProjectNo")) {
//					cookies[i].setValue(commandMap.get("SEQ_NO").toString());
//				}
//				else if (cookies[i].getName().equals("ProjectName")) {
//					cookies[i].setValue(commandMap.get("PROJECT_NAME").toString());
//				}
//				/*else if (cookies[i].getName().equals("ServerPort")) {
//					cookies[i].setValue(commandMap.get("SERVER_PORT").toString());
//				}*/
//            }
//        }
    	
//		Cookie prjno = new Cookie("ProjectNo", commandMap.get("SEQ_NO").toString());
//		Cookie prjname = new Cookie("ProjectName", commandMap.get("PROJECT_NAME").toString());
		//Cookie prjport = new Cookie("ServerPort", commandMap.get("SERVER_PORT").toString());
//		prjno.setMaxAge(60 * 60 * 24 * 90);
//		prjname.setMaxAge(60 * 60 * 24 * 90);
		//prjport.setMaxAge(60 * 60 * 24 * 90);
//		prjno.setPath("/");
//		prjname.setPath("/");
		//prjport.setPath("/");
//		response.addCookie(prjno);
//		response.addCookie(prjname);
		//response.addCookie(prjport);


		request.getSession().setAttribute("ProjectNo", commandMap.get("SEQ_NO"));
		request.getSession().setAttribute("ProjectName", commandMap.get("PROJECT_NAME"));
		//request.getSession().setAttribute("ServerPort", commandMap.get("SERVER_PORT"));

    	return SetMenu(mv, commandMap, request);
    }

	@RequestMapping(value="/view/projectlist.do")
    public ModelAndView openProjectdList(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/project/ProjectList");    	
    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/projectwrite.do")
    public ModelAndView openProjectdWrite(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/project/ProjectWrite");    	
    	
    	return SetMenu(mv, commandMap, request);
    }
		
	@RequestMapping(value="/view/projectupdate.do")
    public ModelAndView openProjectdUpdate(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/project/ProjectWrite");
    
    	List<Map<String, Object>> prjlist = SetProject(Integer.parseInt(commandMap.get("SEQ_NO").toString()), request);
    	
    	if(prjlist.size() > 0) {
    		mv.addObject("SEQ_NO", prjlist.get(0).get("SEQ_NO"));
    		mv.addObject("PROJECT_NAME", prjlist.get(0).get("PROJECT_NAME"));
    		mv.addObject("DESCRIPTION", prjlist.get(0).get("DESCRIPTION"));
    	}

    	return SetMenu(mv, commandMap, request);
    }	
	
	@RequestMapping(value="/view/insertproject.do")
    public ModelAndView insertProject(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("redirect:/view/setprojectlist.do");    	
    	
    	if (commandMap.get("SEQ_NO").toString().isEmpty()) {
    		commandMap.put("TYPE", "I");

    		CommandMap cmmap = new CommandMap();
    		if(request.getSession().getAttribute("USER_SEQ") != null) {
				commandMap.put("USER_SEQ", request.getSession().getAttribute("USER_SEQ"));
				cmmap.put("USER_SEQ", request.getSession().getAttribute("USER_SEQ"));
			}
			cmmap.put("AGENT_NAME", "END");
			cmmap.put("RESET_SLOT", "0");
			cmmap.put("TASK_TYPE", "essential");
			cmmap.put("RELATED_SLOTS", "");
    		
//    		Cookie[] cookies = request.getCookies();
//        	if(cookies != null){
//
//                for(int i=0; i < cookies.length; i++){
//                    if (cookies[i].getName().equals("USER_SEQ")) {
//                    	commandMap.put("USER_SEQ", cookies[i].getValue());
//                    	cmmap.put("USER_SEQ", cookies[i].getValue());
//    				}
//                }
//                cmmap.put("AGENT_NAME", "END");
//                cmmap.put("RESET_SLOT", "0");
//                cmmap.put("TASK_TYPE", "essential");
//                cmmap.put("RELATED_SLOTS", "");
//            }
        	
        	viewService.insertProject(commandMap.getMap(), request);
        	
//        	if(cookies != null){
        	if(cmmap.get("USER_SEQ") != null) { //TODO 의도 불분명
        		cmmap.put("PROJECT_SEQ", commandMap.get("SEQ_NO"));
            	viewService.insertUserDomain(cmmap.getMap());
            	viewService.insertOnlyAgent(cmmap.getMap());
			}        	
		}
		else {
			viewService.updateProject(commandMap.getMap(), request);
		}
    	
    	mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
    	mv.addObject("PROJECT_NAME", commandMap.get("PROJECT_NAME"));
    	
    	//SetProject(0, request);
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/selectprojectname.do")
    public ModelAndView selectProjectName(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		
		List<Map<String, Object>> prjlist = viewService.SelectProjectName(commandMap.getMap());
		
		mv.addObject("list", prjlist);
    	return mv;
    }
	
	
	@RequestMapping(value="/view/deletproject.do")
    public ModelAndView deleteProject(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
    	ModelAndView mv = new ModelAndView("/project/ProjectList");    	
    	
    	viewService.deleteProject(commandMap.getMap(), request);    	
    	//SetProject(0, request);
    	    	
    	return SetMenu(mv, commandMap, request);
    }		
	
	@RequestMapping(value="/view/agentlist.do")
    public ModelAndView openAgentList(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/agent/AgentList");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String, Object>> list = viewService.selectAgentGraph(commandMap.getMap());
    	mv.addObject("conlist", list);
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/openagentgraph.do")
    public ModelAndView openAgentGraph(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("/popup/AgentGraph");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String, Object>> list = viewService.selectAgentGraph(commandMap.getMap());
    	mv.addObject("conlist", list);
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/updateagentposition.do")
    public ModelAndView updateAgentPosition(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = null;
    	if (commandMap.get("TYPE").toString().equals("M")) {
    		mv = new ModelAndView("redirect:/view/agentlist.do");
		}
    	else {
    		mv = new ModelAndView("redirect:/view/openagentgraph.do");
    	}
        
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	viewService.updateAgentPosition(commandMap.getMap());    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/insertagentgraph.do")
    public ModelAndView insertAgentGraph(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
        
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	viewService.insertAgentGraph(commandMap.getMap());
    	mv.addObject("status", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/deleteagentgraph.do")
    public ModelAndView deleteAgentGraph(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
        
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	viewService.deleteAgentGraph(commandMap.getMap());
    	mv.addObject("status", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/openfunctionwrite.do")
    public ModelAndView openFunctionWrite(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/FunctionWork");    	
    	mv.addObject("obj", commandMap.get("obj"));
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/openfliingslotsearch.do")
    public ModelAndView openFillingSlotSearch(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/SlotSearch");
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	mv.addObject("obj", commandMap.get("obj"));
    	List<Map<String, Object>> list = viewService.selectFliingSlot(commandMap.getMap());
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
	   	
    	mv.addObject("SLOT_LIST2", CommonUtils.SetSlotListToJson(slist));
    	mv.addObject("SLOT_LIST", list);
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/requestslotsearch.do")
    public ModelAndView RequestSlotSearch(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/SlotSearchRequest");
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	mv.addObject("obj", commandMap.get("obj"));
    	List<Map<String, Object>> list = viewService.selectFliingSlot(commandMap.getMap());
		List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
	   	
    	mv.addObject("SLOT_LIST2", CommonUtils.SetSlotListToJson(slist));
    	mv.addObject("SLOT_LIST", list);    	    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/openfunctionsearch.do")
    public ModelAndView openFunctionSearh(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/FunctionSearch");    	

    	mv.addObject("search", commandMap.get("FUNCTION_NAME")); 
    	mv.addObject("fnlist", viewService.selectFunction(commandMap.getMap()));
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/agentsub.do")
    public ModelAndView openAgentSub(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = null;
    	String depth3 = "0";
    	
		if(commandMap.get("depth2") != null && commandMap.get("depth3") != null) {
			depth3 = (String)commandMap.get("depth3");
			
			if(depth3.equals("1")) {				
				mv = new ModelAndView("redirect:/view/slotlist.do");		
			}
			else if(depth3.equals("2")) {
				mv = new ModelAndView("redirect:/view/useruttrlist.do");
			}
			else if(depth3.equals("3")) {
				mv = new ModelAndView("redirect:/view/agentedit.do");
			}						
		}
		else
			mv = new ModelAndView("/agent/AgentWrite");

    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/agentwrite.do")
    public ModelAndView openAgentWrite(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/agent/AgentWrite");    	
        	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/agentedit.do")
    public ModelAndView openAgentEdit(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/agent/AgentWrite");    
    	
    	//QQQ
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	if(StringUtils.isEmpty(domain)) {
    		//retrieve domain infor from agentSeq
    		int agentSeq = StrUtils.getNum((String)commandMap.get("AGENT_SEQ"), 0);
    		Map<String,Object> prj = viewService.getDomainByAgentSeq(agentSeq);
    		if(prj != null) {
    			domain = (String)prj.get("PROJECT_NAME");
    			request.getSession().setAttribute("ProjectName", domain);
    		}
    	}
            	
    	Map<String,Object> Agent = viewService.selectAgent(commandMap.getMap());
    	List<Map<String,Object>> SlotFillinglist = viewService.selectAgentSlotFilling(commandMap.getMap());
    	List<Map<String,Object>> Intentionlist = viewService.selectAgentIntention(commandMap.getMap());
    	List<Map<String,Object>> Objectlist = viewService.selectAgentObject(commandMap.getMap());
    	List<Map<String,Object>> ObjectDtllist = viewService.selectAgentObjectdtl(commandMap.getMap());
    	
    	List<Map<String,Object>> Intentionlist_1 = new ArrayList<Map<String, Object>>();
    	List<Map<String,Object>> Intentionlist_2 = new ArrayList<Map<String, Object>>();
    	List<Map<String,Object>> Intentionlist_3 = new ArrayList<Map<String, Object>>();
    	List<Integer> lstIn1 = new ArrayList<Integer>();
    	List<Integer> lstIn2 = new ArrayList<Integer>();
    	List<Integer> lstIn3 = new ArrayList<Integer>();
    	
    	for (Iterator iterator = Intentionlist.iterator(); iterator.hasNext();) {
			Map<String, Object> map = (Map<String, Object>) iterator.next();
			if (map.get("INTENTION_TYPE").toString().equals("1")) {
				Intentionlist_1.add(map);
				lstIn1.add((int)map.get("SEQ_NO"));
			}
			else if (map.get("INTENTION_TYPE").toString().equals("2")) {
				Intentionlist_2.add(map);
				lstIn2.add((int)map.get("SEQ_NO"));
			}
			else {
				Intentionlist_3.add(map);
				lstIn3.add((int)map.get("SEQ_NO"));
			}
		}
    	
    	List<Map<String,Object>> Objectlist_1 = new ArrayList<Map<String, Object>>();
    	List<Map<String,Object>> Objectlist_2 = new ArrayList<Map<String, Object>>();
    	List<Map<String,Object>> Objectlist_3 = new ArrayList<Map<String, Object>>();
    	List<Integer> lstOb1 = new ArrayList<Integer>();
    	List<Integer> lstOb2 = new ArrayList<Integer>();
    	List<Integer> lstOb3 = new ArrayList<Integer>();
    	for (Iterator iterator = Objectlist.iterator(); iterator.hasNext();) {
			Map<String, Object> map = (Map<String, Object>) iterator.next();
			if (lstIn1.contains(map.get("INTENTION_SEQ"))) {
				Objectlist_1.add(map);
				lstOb1.add((int)map.get("SEQ_NO"));
			}
			else if (lstIn2.contains(map.get("INTENTION_SEQ"))) {
				Objectlist_2.add(map);
				lstOb2.add((int)map.get("SEQ_NO"));
			}
			else {
				Objectlist_3.add(map);
				lstOb3.add((int)map.get("SEQ_NO"));
			}
		}
    	
    	List<Map<String,Object>> ObjectDtllist_1 = new ArrayList<Map<String, Object>>();
    	List<Map<String,Object>> ObjectDtllist_2 = new ArrayList<Map<String, Object>>();
    	List<Map<String,Object>> ObjectDtllist_3 = new ArrayList<Map<String, Object>>();
    	
    	for (Iterator iterator = ObjectDtllist.iterator(); iterator.hasNext();) {
			Map<String, Object> map = (Map<String, Object>) iterator.next();
			if (lstOb1.contains(map.get("OBJECT_SEQ"))) {
				ObjectDtllist_1.add(map);
			}
			else if (lstOb2.contains(map.get("OBJECT_SEQ"))) {
				ObjectDtllist_2.add(map);				
			}
			else {
				ObjectDtllist_3.add(map);				
			}
		}
    	
    	mv.addObject("Agent", Agent);
    	mv.addObject("SlotFillinglist", SlotFillinglist);
    	mv.addObject("Intentionlist_1", Intentionlist_1);
    	mv.addObject("Intentionlist_2", Intentionlist_2);
    	mv.addObject("Intentionlist_3", Intentionlist_3);
    	mv.addObject("Objectlist_1", Objectlist_1);
    	mv.addObject("Objectlist_2", Objectlist_2);
    	mv.addObject("Objectlist_3", Objectlist_3);
    	mv.addObject("ObjectDtllist_1", ObjectDtllist_1);
    	mv.addObject("ObjectDtllist_2", ObjectDtllist_2);
    	mv.addObject("ObjectDtllist_3", ObjectDtllist_3);
    	mv.addObject("SEQ_NO", commandMap.get("AGENT_SEQ"));	
    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/insertagent.do")
    public ModelAndView insertAgent(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		viewService.insertAgent(commandMap.getMap());
		
		//mv.addObject("AGENT_SEQ", commandMap.get("AGENT_SEQ"));
		mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
		mv.addObject("SAVE_FLAG", "Y");
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/deleteagent.do")
    public ModelAndView deleteAgent(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/agentlist.do");
		
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));		
		viewService.deleteAgent(commandMap.getMap());
    	return SetMenu(mv, commandMap, request);
    }
		
	@RequestMapping(value="/view/slotlist.do")
    public ModelAndView openSlotList(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/slot/SlotList");    	
    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	String strWhere = "";
    	if (commandMap.get("AGENT_SEQ") != null && !commandMap.get("AGENT_SEQ").toString().equals("0") && !commandMap.get("AGENT_SEQ").toString().isEmpty()) {
    		strWhere = "AND (AGENT_SEQ IN (" + commandMap.get("AGENT_SEQ").toString() + ", 0) OR AGENT_SEQ IS NULL)";
		}
    		
    	commandMap.put("WHERE_SQL", strWhere);    	
    	List<Map<String, Object>> clist = viewService.selectSlotClass(commandMap.getMap());    	
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());	   	    
    	
    	mv.addObject("CLASS_LIST", clist);    	   
//    	log.debug(slist);
    	mv.addObject("SLOT_LIST", CommonUtils.SetSlotListToJson(slist));
    	
    	return SetMenu(mv, commandMap, request);
    }
		
	
	@RequestMapping(value="/view/getslotdefine.do")
    public ModelAndView getSlotDefine(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String,Object>> list = viewService.selectSlotDefine(commandMap.getMap());
    	mv.addObject("list", list);    	    	
    	return mv;
    }
	
	@RequestMapping(value="/view/getslotlist.do")
    public ModelAndView selectSlotList(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	
    	List<Map<String,Object>> slotlist = viewService.selectSlot2(commandMap.getMap());
    	mv.addObject("list", slotlist);    	    	
    	return mv;
    }
	
	@RequestMapping(value="/view/opentypesearch.do")
    public ModelAndView openTypeSearchh(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/TypeSearch");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String, Object>> list = viewService.selectSlotType(commandMap.getMap());
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
	   	
    	mv.addObject("SLOT_LIST", CommonUtils.SetSlotListToJson(slist));
    	
    	mv.addObject("TYPE_LIST", list);    	    	
    	return SetMenu(mv, commandMap, request);
    }
		
	@RequestMapping(value="/view/getparentclass.do")
    public ModelAndView selectSlotTypeClass(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String, Object>> list = viewService.selectSlotTypeClass(commandMap.getMap());
    	mv.addObject("list", list);    	    	
    	return mv;
    }
	
	@RequestMapping(value="/view/slotinstansces.do")
    public ModelAndView openSlotInstans(CommandMap commandMap, HttpServletRequest request) throws Exception{
		/*ModelAndView mv = new ModelAndView("/slot/SlotInstances");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
	   	
		mv.addObject("SLOT_LIST", SetSlotListToJson(slist));
        mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
        mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));*/
    	ModelAndView mv = new ModelAndView("/slot/SlotInstances");
    
    	//List<Map<String,Object>> slotlist = viewService.selectSlot2(commandMap.getMap());    	
    	List<Map<String,Object>> slotlist = GetSlotList(commandMap);
    	mv.addObject("slotlist", slotlist);
    	mv.addObject("SLOT_LIST", commandMap.get("SLOT_LIST"));
        mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
        mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
    	return SetMenu(mv, commandMap, request);
    }
	
	private List<Map<String,Object>> GetSlotList(CommandMap commandMap) {		
		List<Map<String,Object>> slotlist = new ArrayList<Map<String,Object>>();
    	
    	JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;
                 
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)commandMap.get("SLOT_LIST"));			
			JSONArray objArray = (JSONArray)jsonObj.get(commandMap.get("SEQ_NO"));
			slotRecursive(slotlist, commandMap.get("SEQ_NO").toString(), "", jsonObj, objArray, "");			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return slotlist;
	}

	private void slotRecursive(List<Map<String,Object>> slotlist, String SeqNo, String SlotSeqNo, JSONObject jsonObj, JSONArray objArray, String SlotName) {				
		for(int i = 0 ; i < objArray.size() ; i++){
			JSONObject SlotObj = (JSONObject)objArray.get(i);
			
			if (SlotObj.get("SLOT_TYPE").toString().equals("C")) {
				if (SlotName.isEmpty()) {
					slotRecursive(slotlist, SeqNo, SlotObj.get("SEQ_NO").toString(), jsonObj, (JSONArray)jsonObj.get(SlotObj.get("TYPE_SEQ").toString()), SlotObj.get("SLOT_NAME").toString());
				}
				else {
					slotRecursive(slotlist, SeqNo, SlotSeqNo, jsonObj, (JSONArray)jsonObj.get(SlotObj.get("TYPE_SEQ").toString()), SlotName + "." + SlotObj.get("SLOT_NAME"));
				}
			}
			else {
				Map<String,Object> sublist = new HashMap<String,Object>();				
				
				if (SlotName.isEmpty()) {
					sublist.put("VIEW_COL", SlotObj.get("SLOT_NAME"));
					sublist.put("QUERY_COL", SeqNo + "_" + SlotObj.get("SEQ_NO") + "_" + SlotObj.get("SLOT_NAME"));
				}
				else {
					sublist.put("VIEW_COL", SlotName + "." + SlotObj.get("SLOT_NAME"));
					sublist.put("QUERY_COL", SeqNo + "_" + SlotSeqNo + "_" + SlotObj.get("SLOT_NAME"));
				}
				slotlist.add(sublist);
			}
		}
	}	
	
	@RequestMapping(value="/view/selectslotinstansces.do")
    public ModelAndView selectSlotInstans(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	
    	List<Map<String,Object>> list = viewService.selectSlotInstances(commandMap.getMap());    	
    	mv.addObject("list", list);
    	if(list.size() > 0){
    		mv.addObject("TOTAL", list.get(0).get("TOTAL_COUNT"));
    	}
    	else{
    		mv.addObject("TOTAL", 0);
    	}
    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/insertslotinstansces.do")
    public ModelAndView insertSlotInstans(CommandMap commandMap, HttpServletRequest request) throws Exception{		
    	/*ModelAndView mv = new ModelAndView("jsonView");
    	viewService.insertSlotInstans(commandMap.getMap());
    	mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));*/
		ModelAndView mv = new ModelAndView("/slot/SlotInstances");
    	viewService.insertSlotInstans(commandMap.getMap());
    	
    	List<Map<String,Object>> slotlist = GetSlotList(commandMap);
    	
    	mv.addObject("slotlist", slotlist);
    	mv.addObject("SLOT_LIST", commandMap.get("SLOT_LIST"));
        mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
        mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
        
        return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/updateslotinstansces.do")
    public ModelAndView updateSlotInstans(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		viewService.updateSlotInstans(commandMap.getMap());
		
		mv.addObject("status", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/deleteslotinstansces.do")
    public ModelAndView deleteSlotInstans(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		
		viewService.deleteSlotInstans(commandMap.getMap());
		
		mv.addObject("status", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/getslotinstanscesexcel.do")
    public ModelAndView getSlotInstansExcel(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("excelView");    	
    	
        mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
        mv.addObject("HEADER_LIST", commandMap.get("HEADER_LIST"));
        mv.addObject("COL_LIST", commandMap.get("COL_LIST"));
        //mv.addObject("CLASS_LIST", commandMap.get("CLASS_LIST"));
        mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
        mv.addObject("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
    	mv.addObject("TYPE", "I");
    	return mv;
    }
	
	@RequestMapping(value="/view/setslotinstanscesexcel.do")
	public ModelAndView setSlotInstansExcel(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("/slot/SlotInstances");		
		
		viewService.insertSlotInstansExcel(commandMap.getMap(), request);		
		List<Map<String,Object>> slotlist = GetSlotList(commandMap);		
		mv.addObject("slotlist", slotlist);
    	mv.addObject("SLOT_LIST", commandMap.get("SLOT_LIST"));
        mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
        mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
    	return SetMenu(mv, commandMap, request);
	}
	
	@RequestMapping(value="/view/slotwrite.do")
    public ModelAndView openSlotWrite(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/slot/SlotWrite");    	
        
    	List<Map<String, Object>> list = viewService.selectSlotObject(commandMap.getMap());
    	mv.addObject("OBJ_LIST", list);
    	mv.addObject("SLOT_SEQ_NO", commandMap.get("SLOT_SEQ_NO"));
    	mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
    	mv.addObject("SLOT_NAME", commandMap.get("SLOT_NAME"));    	
    	return SetMenu(mv, commandMap, request);
    }
		
	@RequestMapping(value="/view/getslotobjectdetail.do")
    public ModelAndView getSlotObjectDetail(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
        
    	List<Map<String, Object>> list = viewService.selectSlotObjectDetail(commandMap.getMap());
    	mv.addObject("DETAIL_LIST", list); 
    	mv.addObject("PARENT_SEQ_NO", commandMap.get("PARENT_SEQ_NO"));
    	return mv;
    }	
	
	@RequestMapping(value="/view/getslotobjectexcel.do")
    public ModelAndView getSlotObjectExcel(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("excelView");    	
    	
    	List<Map<String, Object>> objlist = viewService.selectSlotObject(commandMap.getMap());
    	List<Map<String, Object>> objdtllist = viewService.selectSlotObjectDetailExcel(commandMap.getMap());
    	mv.addObject("OBJECT_LIST", objlist);
    	mv.addObject("DETAIL_LIST", objdtllist);
    	mv.addObject("SLOT_NAME", commandMap.get("SLOT_NAME"));
    	mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
    	mv.addObject("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
    	mv.addObject("TYPE", "O");
    	return mv;
    }
	
	@RequestMapping(value="/view/setslotobjectexcel.do")
	public ModelAndView setSlotObjectExcel(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("/slot/SlotWrite");		
		
		try {
			viewService.insertSlotObjectExcel(commandMap.getMap(), request);	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		List<Map<String, Object>> list = viewService.selectSlotObject(commandMap.getMap());
    	mv.addObject("OBJ_LIST", list);    	
    	mv.addObject("SLOT_SEQ_NO", commandMap.get("SLOT_SEQ_NO"));
    	mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
    	mv.addObject("SLOT_NAME", commandMap.get("SLOT_NAME"));    	
    	mv.addObject("STATUS", commandMap.get("STATUS"));
    	
    	return SetMenu(mv, commandMap, request);
	}
	
	
	@RequestMapping(value="/view/insertslotclass.do")
    public ModelAndView insertSlotClass(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/slotlist.do");
		
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		if (commandMap.get("SEQ_NO").toString().isEmpty()) {
			viewService.insertSlotClass(commandMap.getMap());			
			
			CommandMap cdb = new CommandMap();			
			cdb.put("SEQ_NO", commandMap.get("SEQ_NO"));
			cdb.put("COL_NAME", "SEQ_NO INT NOT NULL AUTO_INCREMENT PRIMARY KEY");			
		    viewService.CreateTable(cdb.getMap());
		}
		else {
			viewService.updateSlotClass(commandMap.getMap());
		}
		
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/insertslot.do")
    public ModelAndView insertSlot(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/slotlist.do");		
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		String ColType = "varchar(200)";//�ּ�ǰ
		/*if(commandMap.get("SLOT_TYPE").toString().equals("S")) {
			if (commandMap.get("TYPE_NAME").toString().contains("int")) {
				ColType = "int";
			}
			else if (commandMap.get("TYPE_NAME").toString().contains("float")) {
				ColType = "float";
			}
		}*/
		
		CommandMap cdb = new CommandMap();			
		cdb.put("SEQ_NO", commandMap.get("CLASS_SEQ_NO"));			
		cdb.put("COL_TYPE", ColType); //�ּ�ǰ
		cdb.put("SLOT_LIST", commandMap.get("SLOT_LIST"));		
		cdb.put("TYPE_SEQ", commandMap.get("TYPE_SEQ"));
		cdb.put("OLD_TYPE_SEQ", commandMap.get("OLD_TYPE_SEQ"));
		boolean bInsertChk = false;
		
		if (commandMap.get("SEQ_NO").toString().isEmpty()) {			
			viewService.insertSlot(commandMap.getMap());
			cdb.put("COL_NAME", commandMap.get("CLASS_SEQ_NO") + "_" + commandMap.get("SEQ_NO") + "_" + commandMap.get("SLOT_NAME"));
			bInsertChk = true;
		    //viewService.CreateColumn(cdb.getMap());
		}
		else {
			cdb.put("COL_NAME", commandMap.get("CLASS_SEQ_NO") + "_" + commandMap.get("SEQ_NO") + "_" + commandMap.get("OLD_SLOT_NAME"));
			cdb.put("NEW_COL_NAME", commandMap.get("CLASS_SEQ_NO") + "_" + commandMap.get("SEQ_NO") + "_" + commandMap.get("SLOT_NAME"));
			viewService.updateSlot(commandMap.getMap());
			//viewService.AlterColumn(cdb.getMap());
		}
		
		cdb.put("SLOT_SEQ_NO", commandMap.get("SEQ_NO"));
		
		if (commandMap.get("SLOT_TYPE").toString().equals("C")) {			
			if (bInsertChk || (!bInsertChk && !commandMap.get("OLD_SLOT_TYPE").toString().equals("C"))) {
				//�μ�Ʈ
				viewService.insertSlotTypeClass(commandMap.getMap());
			}
			else {
				if (commandMap.get("OLD_SLOT_TYPE").toString().equals("C") && 
						!commandMap.get("TYPE_SEQ").toString().equals(commandMap.get("OLD_TYPE_SEQ").toString())) {
					//���� �� �μ�Ʈ
					viewService.deleteSlotTypeClass(commandMap.getMap());
					viewService.insertSlotTypeClass(commandMap.getMap());
				}
			}
			
			//���̺� �÷�
			if (bInsertChk) {				
				viewService.CreateMultiColumn(cdb.getMap());
			}
			else {						
				if (commandMap.get("OLD_SLOT_TYPE").toString().equals("C")) {
					if(!commandMap.get("TYPE_SEQ").toString().equals(commandMap.get("OLD_TYPE_SEQ").toString())) {
						//���� �� ���� �� ������ �߰�						
						viewService.DropMultiColumn(cdb.getMap());
						//���� �� Ŭ������ȣ  ����
						//cdb.put("COL_NAME", commandMap.get("CLASS_SEQ_NO") + "_" + commandMap.get("SLOT_NAME"));
						viewService.CreateMultiColumn(cdb.getMap());
					}
				}
				else {
					//1�� �÷� ���� �� ������ �߰�					
					viewService.DropColumn(cdb.getMap());
					//���� �� Ŭ������ȣ  ����
					//cdb.put("COL_NAME", commandMap.get("CLASS_SEQ_NO") + "_" + commandMap.get("SLOT_NAME"));
					viewService.CreateMultiColumn(cdb.getMap());
				}		
			}
		}
		else {			
			if (!bInsertChk) {
				if (commandMap.get("OLD_SLOT_TYPE").toString().equals("C")) {
					//����
					viewService.deleteSlotTypeClass(commandMap.getMap());
					//������ ���� �� ���� �߰�					
					viewService.DropMultiColumn(cdb.getMap());
					//���� �� Ŭ������ȣ  ����
					cdb.put("COL_NAME", commandMap.get("CLASS_SEQ_NO") + "_" + commandMap.get("SEQ_NO") + "_" + commandMap.get("SLOT_NAME"));
					viewService.CreateColumn(cdb.getMap());
				}
				else {					
					viewService.AlterColumn(cdb.getMap()); //�߰�
				}
			}
			else { //�߰�
				viewService.CreateColumn(cdb.getMap());
			}		
		}
		
		List<Map<String, Object>> list = viewService.selectSlotTypeClass(commandMap.getMap());
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Map<String, Object> map = (Map<String, Object>) iterator.next();
			cdb.put("SEQ_NO", map.get("CLASS_SEQ_NO"));
			cdb.put("SLOT_SEQ_NO", map.get("SLOT_SEQ_NO"));
			
			if (bInsertChk) {
				cdb.put("COL_NAME", map.get("CLASS_SEQ_NO") + "_" + map.get("SLOT_SEQ_NO") + "_" + commandMap.get("SLOT_NAME"));							
			}
			else {
				cdb.put("COL_NAME", map.get("CLASS_SEQ_NO") + "_" + map.get("SLOT_SEQ_NO") + "_" + commandMap.get("OLD_SLOT_NAME"));
				cdb.put("NEW_COL_NAME", map.get("CLASS_SEQ_NO") + "_" + map.get("SLOT_SEQ_NO") + "_" + commandMap.get("SLOT_NAME"));			
			}
			
			if (commandMap.get("SLOT_TYPE").toString().equals("C")) {							
				//���̺� �÷�
				if (bInsertChk) {					
					viewService.CreateMultiColumn(cdb.getMap());
				}
				else {						
					if (commandMap.get("OLD_SLOT_TYPE").toString().equals("C")) {
						if(!commandMap.get("TYPE_SEQ").toString().equals(commandMap.get("OLD_TYPE_SEQ").toString())) {
							//���� �� ���� �� ������ �߰�							
							viewService.DropMultiColumn(cdb.getMap());
							//���� �� Ŭ������ȣ  ����
							cdb.put("COL_NAME", map.get("CLASS_SEQ_NO") + "_" + map.get("SLOT_SEQ_NO") + "_" + commandMap.get("SLOT_NAME"));
							viewService.CreateMultiColumn(cdb.getMap());
						}
					}
					else {
						//1�� �÷� ���� �� ������ �߰�						
						viewService.DropColumn(cdb.getMap());
						//���� �� Ŭ������ȣ  ����
						cdb.put("COL_NAME", map.get("CLASS_SEQ_NO") + "_" + map.get("SLOT_SEQ_NO") + "_" + commandMap.get("SLOT_NAME"));
						viewService.CreateMultiColumn(cdb.getMap());
					}		
				}
			}
			else {			
				if (!bInsertChk) {
					if (commandMap.get("OLD_SLOT_TYPE").toString().equals("C")) {					
						viewService.DropMultiColumn(cdb.getMap());
						//���� �� Ŭ������ȣ  ����
						cdb.put("COL_NAME", map.get("CLASS_SEQ_NO") + "_" + map.get("SLOT_SEQ_NO") + "_" + commandMap.get("SLOT_NAME"));
						viewService.CreateColumn(cdb.getMap());
					}
					else {						
						viewService.AlterColumn(cdb.getMap()); //�߰�
					}
				}
				else { //�߰�
					viewService.CreateColumn(cdb.getMap()); 
				}		
			}
		}
						
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/deleteslotclass.do")
    public ModelAndView deleteSlotClass(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/slotlist.do");
				
		viewService.deleteSlotClass(commandMap.getMap());
		viewService.deleteSlotClassSeq(commandMap.getMap());
		viewService.deleteSlotTypeClassAll(commandMap.getMap());
	    viewService.DropTable(commandMap.getMap());
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/deleteslot.do")
    public ModelAndView deleteSlot(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/slotlist.do");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		viewService.deleteSlot(commandMap.getMap());
		viewService.deleteSlotTypeClass(commandMap.getMap());
		
		CommandMap cdb = new CommandMap();			
		cdb.put("SEQ_NO", commandMap.get("CLASS_SEQ_NO"));
		cdb.put("SLOT_LIST", commandMap.get("SLOT_LIST"));
		
		if (commandMap.get("SLOT_TYPE").toString().equals("C")) {
			cdb.put("OLD_TYPE_SEQ", commandMap.get("TYPE_SEQ"));
			viewService.DropMultiColumn(cdb.getMap());
		}
		else {
			cdb.put("COL_NAME", commandMap.get("CLASS_SEQ_NO") + "_" + commandMap.get("SEQ_NO") + "_" + commandMap.get("OLD_SLOT_NAME"));
			viewService.DropColumn(cdb.getMap());
		}
				
		List<Map<String, Object>> list = viewService.selectSlotTypeClass(commandMap.getMap());
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Map<String, Object> map = (Map<String, Object>) iterator.next();
			cdb.put("SEQ_NO", map.get("CLASS_SEQ_NO"));
			cdb.put("SLOT_SEQ_NO", map.get("SLOT_SEQ_NO"));
			
			if (commandMap.get("SLOT_TYPE").toString().equals("C")) {				
				viewService.DropMultiColumn(cdb.getMap());
			}
			else {
				cdb.put("COL_NAME", map.get("CLASS_SEQ_NO") + "_" + map.get("SLOT_SEQ_NO") + "_" + commandMap.get("OLD_SLOT_NAME"));
				viewService.DropColumn(cdb.getMap());
			}
		}
	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/insertslotobject.do")
    public ModelAndView insertSlotObject(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/slotwrite.do");
    	//ModelAndView mv = new ModelAndView("jsonView");    	
    	viewService.insertSlotObject(commandMap.getMap());
    	
    	List<Map<String, Object>> list = viewService.selectSlotObject(commandMap.getMap());
    	mv.addObject("OBJ_LIST", list);    	
    	mv.addObject("SLOT_SEQ_NO", commandMap.get("SLOT_SEQ_NO"));
    	mv.addObject("CLASS_NAME", commandMap.get("CLASS_NAME"));
    	mv.addObject("SLOT_NAME", commandMap.get("SLOT_NAME"));
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/deleteslotobject.do")
    public ModelAndView deleteSlotObject(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		
		viewService.deleteSlotObject(commandMap.getMap());
		viewService.deleteSlotObjectDetail(commandMap.getMap());
		
    	return mv;
    }
	
	@RequestMapping(value="/view/updateslotsort.do")
    public ModelAndView updateSlotSort(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		
		
		viewService.updateSlotSort(commandMap.getMap());
		
    	return mv;
    }
	
	@RequestMapping(value="/view/useruttrlist.do")
    public ModelAndView openUserUttrList(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/useruttr/UserUttrList");
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	
    	String strWhere = "";
    	if (commandMap.get("AGENT_SEQ") != null && !commandMap.get("AGENT_SEQ").toString().equals("0") && !commandMap.get("AGENT_SEQ").toString().isEmpty()) {
    		//strWhere = "AND (AGENT_SEQ IN (" + commandMap.get("AGENT_SEQ").toString() + ", 0) OR AGENT_SEQ IS NULL)";    		
    		strWhere = "AND AGENT_SEQ = " + commandMap.get("AGENT_SEQ").toString();
		}
    	
    	commandMap.put("WHERE_SQL", strWhere);
    	List<Map<String,Object>> list = viewService.selectIntents(commandMap.getMap());
    	
    	mv.addObject("list", list);
    	mv.addObject("INTENTS_LIST", CommonUtils.SetIntentListToJson(list));
    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/useruttrwrite.do")
    public ModelAndView openUserUttrWrite(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/useruttr/UserUttrWrite");    	

		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
    	        	       
    	mv.addObject("SLOT_LIST2", CommonUtils.SetSlotListToJson(slist));
    	mv.addObject("INTENTS_LIST", commandMap.get("INTENTS_LIST"));
    	mv.addObject("AGENT_SEQ", 0);
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/useruttredit.do")
    public ModelAndView openUserUttrEdit(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/useruttr/UserUttrWrite");    	
    	List<Map<String,Object>> UserSaylist;
    	List<Map<String,Object>> SlotList = new ArrayList<Map<String,Object>>();
    
    	if (commandMap.get("SLOT_SEQ_NO").toString().isEmpty()) {
       		UserSaylist = viewService.selectIntentsUserSays(commandMap.getMap());
       	}
    	else {
       		UserSaylist = viewService.selectIntentsUserSays2(commandMap.getMap());
       		SlotList = viewService.selectSlotIntents(commandMap.getMap());
    	}
    
    	List<Map<String,Object>> Intentionlist = viewService.selectIntentIntention(commandMap.getMap());
    	List<Map<String,Object>> Objectlist = viewService.selectIntentsObejct(commandMap.getMap());
    	List<Map<String,Object>> ObjectDtllist = viewService.selectIntentsObjectDtl(commandMap.getMap());
    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));    	
    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());
    	
    	mv.addObject("SLOT_LIST2", CommonUtils.SetSlotListToJson(slist));
    	mv.addObject("UserSaylist", UserSaylist);
    	mv.addObject("Intentionlist", Intentionlist);
    	mv.addObject("Objectlist", Objectlist);
    	mv.addObject("ObjectDtllist", ObjectDtllist);
    	mv.addObject("SlotList", SlotList);
    	mv.addObject("SEQ_NO", commandMap.get("INTENT_SEQ"));
    	mv.addObject("INTENT_NAME", commandMap.get("INTENT_NAME"));
    	mv.addObject("SLOT_SEQ_NO", commandMap.get("SLOT_SEQ_NO"));
    	mv.addObject("INTENTS_LIST", commandMap.get("INTENTS_LIST"));
    	if (commandMap.get("AGENT_SEQ").toString().isEmpty()) {
    		mv.addObject("AGENT_SEQ", 0);
		}
    	else {
    		mv.addObject("AGENT_SEQ", commandMap.get("AGENT_SEQ"));
    	}
    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/insertintent.do")
    public ModelAndView insertIntent(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
		
		viewService.insertIntent(commandMap.getMap());
		
		Map<String,Object> ObjIntent = viewService.selectIntent(commandMap.getMap());
		mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
		mv.addObject("INTENT_SEQ", commandMap.get("SEQ_NO"));
		mv.addObject("INTENT_NAME", ObjIntent.get("INTENT_NAME"));
		mv.addObject("SLOT_SEQ_NO", "");

    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/deleteintent.do")
    public ModelAndView deleteIntent(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/useruttrlist.do");
		
		viewService.deleteIntent(commandMap.getMap());
		viewService.deleteIntentsUserSays(commandMap.getMap());
		viewService.deleteIntentsIntention(commandMap.getMap());
		viewService.deleteIntentsObject(commandMap.getMap());
		viewService.deleteIntentsObjectDetail(commandMap.getMap());
				
    	return SetMenu(mv, commandMap, request);
    }
 	
	@RequestMapping(value="/view/deleteintentusersay.do")
    public ModelAndView deleteIntentUserSay(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		
		viewService.deleteIntentUserSay(commandMap.getMap());
				
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/deleteintentusersaydtl.do")
    public ModelAndView deleteIntentUserSayDtl(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		
		viewService.deleteIntentUserSayDetail(commandMap.getMap());
				
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/getslotobjectlist.do")
    public ModelAndView selectSlotObjectList(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	List<Map<String,Object>> list = viewService.selectVSlotObject(commandMap.getMap());
    	mv.addObject("list", list);    	    	
    	return mv;
	}
	
	@RequestMapping(value="/view/getslotcolor.do")
    public ModelAndView selectSlotColor(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	mv.addObject("COLOR", viewService.selectSlotColor(commandMap.getMap()));   	
    	return mv;
	}
	
	@RequestMapping(value="/view/knowledgelist.do")
    public ModelAndView openKnowledgeList(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/knowledge/KnowledgeList");    	
        	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/knowledgedetaillist.do")
    public ModelAndView openKnowledgeDetailList(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/knowledge/KnowledgeDetailList");    	
        	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/dialogslearning.do")
    public ModelAndView DialogsLearning(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	
    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
    	commandMap.put("PROJECT_NAME", request.getSession().getAttribute("ProjectName"));
    	
    	viewService.DialogsLearning(commandMap.getMap(), request);
    	mv.addObject("STATUS", "OK");
    	return mv;
    }
	
	@RequestMapping(value="/view/userlist.do")
    public ModelAndView openUserList(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/UserList");    	    	
    	    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/selectUserList.do")
    public ModelAndView selectUserList(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	    	
    	
    	List<Map<String,Object>> list = viewService.selectUserList(commandMap.getMap());
    	if(list.size() > 0){
    		mv.addObject("TOTAL", list.get(0).get("TOTAL_COUNT"));
    	}
    	else{
    		mv.addObject("TOTAL", 0);
    	}
    	mv.addObject("list", list);
    	return SetMenu(mv, commandMap, request);
    }	
	
	@RequestMapping(value="/view/userwrite.do")
    public ModelAndView openUserWrite(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/UserWrite");
    	
    	if (!commandMap.get("SEQ_NO").toString().equals("0")) {
    		Map<String,Object> userinfo = viewService.selectUser(commandMap.getMap());
    		AES256Util aes256 = new AES256Util(key);
    		URLCodec codec = new URLCodec();
    		userinfo.put("USER_PWD", aes256.aesDecode(codec.decode(userinfo.get("USER_PWD").toString())));
    		mv.addObject("userinfo", userinfo);
    	}
    	
    	mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/userdomain.do")
    public ModelAndView openUserDomain(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/popup/DomainSearch");    	
    	
    	commandMap.put("SEQ_NO", 0);
    	List<Map<String, Object>> prjlist = null;
    	List<Map<String, Object>> list = null;
    	
    	commandMap.put("SEQ_NO", request.getParameter("userno"));
    	
    	if (commandMap.get("type").toString().equals("v")) {
    		prjlist = viewService.selectUserDomainList(commandMap.getMap());	
		}
    	else if (commandMap.get("type").toString().equals("c")) {
    		prjlist = SetProject(0, request);
    	}
    	else {
    		prjlist = SetProject(0, request);
    		list = viewService.selectUserDomain(commandMap.getMap());
    	}    	    
    	     	
    	mv.addObject("prjlist", prjlist);
    	mv.addObject("list", list);
    	
    	return mv;
    }
	
	@RequestMapping(value="/view/lualist.do")
    public ModelAndView openLuaList(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/LuaList");    	    	
    	    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/selectlualist.do")
    public ModelAndView selectLuaList(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("jsonView");    	    	
    	
    	List<Map<String,Object>> list = viewService.selectLuaList(commandMap.getMap());
    	if(list.size() > 0){
    		mv.addObject("TOTAL", list.get(0).get("TOTAL_COUNT"));
    	}
    	else{
    		mv.addObject("TOTAL", 0);
    	}
    	mv.addObject("list", list);
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/luawrite.do")
    public ModelAndView openLuaWrite(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/LuaWrite");
    	
    	if (!commandMap.get("SEQ_NO").toString().equals("0")) {
    		Map<String,Object> luainfo = viewService.selectLua(commandMap.getMap());    		
    		mv.addObject("luainfo", luainfo);
    	}
    	
    	mv.addObject("SEQ_NO", commandMap.get("SEQ_NO"));
    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/copydomain.do")
    public ModelAndView copyDomain(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		List<Map<String, Object>> prjlist = SetProject(0, request);
		
		List<String> namelist = viewService.copyDomain(commandMap.getMap(), prjlist, request);
		
		mv.addObject("namelist", namelist);
		mv.addObject("state", "OK");		
					
    	return mv;
    }
	
	@RequestMapping(value="/view/userinsert.do")
    public ModelAndView InsertUser(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/UserList");;    	

    	AES256Util aes256 = new AES256Util(key);
		URLCodec codec = new URLCodec();		
		String encpwd = codec.encode(aes256.aesEncode(commandMap.get("USER_PWD").toString()));
		
    	if (commandMap.get("SEQ_NO").toString().equals("0")) {		
	    	List<Map<String,Object>> list = viewService.selectUserIdChk(commandMap.getMap());
	    	
	    	if (list.size() == 0) {
	    		new ModelAndView("/usermng/UserList");
	    			    				    		
	    		commandMap.put("USER_PWD", encpwd);
	
	    		viewService.insertUser(commandMap.getMap());	    		
			}
	    	else {
	    		mv = new ModelAndView("/usermng/UserWrite");
	    		mv.addObject("USING", "Y");
	    		mv.addObject("SEQ_NO", commandMap.get("SEQ_NO").toString());
	    		mv.addObject("USER_ID", commandMap.get("USER_ID").toString());
	    		mv.addObject("USER_PWD", commandMap.get("USER_PWD").toString());
	    		mv.addObject("USER_NAME", commandMap.get("USER_NAME").toString());
	    		mv.addObject("USER_TYPE", commandMap.get("USER_TYPE").toString());
	    		mv.addObject("REMARKS", commandMap.get("REMARKS").toString());	    		
	    	}
    	}
    	else {    		
    		commandMap.put("USER_PWD", encpwd);    		
			viewService.updateUser(commandMap.getMap());
		}
    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/userdelete.do")
    public ModelAndView DeleteUser(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/userlist.do");    	
    	    	
    	viewService.deleteUser(commandMap.getMap());
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/luainsert.do")
    public ModelAndView InsertLua(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	ModelAndView mv = new ModelAndView("/usermng/LuaList");;    	    
    	if (commandMap.get("SEQ_NO").toString().equals("0")) {		
	    	viewService.insertLua(commandMap.getMap());
    	}
    	else {    		    		
			viewService.updateLua(commandMap.getMap());
		}
    	
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/luadelete.do")
    public ModelAndView DeleteLua(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/view/lualist.do");    	
    	    	
    	viewService.deleteLua(commandMap.getMap());
    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/insertuserdomain.do")
    public ModelAndView insertuserdomain(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");
		if (commandMap.get("FLAG").toString().equals("I")) {
			viewService.insertUserDomain(commandMap.getMap());
		}
		else {			
			viewService.deleteUserDomain(commandMap.getMap());
		}
		return mv;
    }
	
	@RequestMapping(value="/view/login.do")
    public ModelAndView Login(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("jsonView");		

		AES256Util aes256 = new AES256Util(key);
		URLCodec codec = new URLCodec();	
		String encpwd = codec.encode(aes256.aesEncode(commandMap.get("USER_PWD").toString()));
		
		commandMap.put("USER_PWD", encpwd);
		
		Map<String,Object> userinfo = viewService.loginUser(commandMap.getMap());
		
		if (userinfo == null) {
			mv.addObject("STATUS", "FA");
		} 
		else {
			mv.addObject("STATUS", "OK");
			mv.addObject("userinfo", userinfo);

			request.getSession().setAttribute("USER_TYPE", userinfo.get("USER_TYPE"));
			request.getSession().setAttribute("USER_SEQ", userinfo.get("SEQ_NO"));
			request.getSession().setAttribute("USER_NAME", userinfo.get("USER_NAME"));

			try { 
				Runtime rt = Runtime.getRuntime(); 				
	            Process proc = rt.exec("ps aux |grep run.daemon.dial.pl |grep --perl-regexp -o 'port [0-9]{5}' |sort |uniq |awk -F' ' '{print $2}' |tr -s '\n' ' '");	            
	            proc.waitFor();
	            	           
	            InputStream is = proc.getInputStream(); 
	            InputStreamReader isr = new InputStreamReader(is);
	            BufferedReader br = new BufferedReader(isr);
	            String line;
	            String port;
	            while((line=br.readLine())!= null){
	                if (!line.isEmpty()) {	                	
	                	//System.out.println(line);
	                	arrUsingPort = line.split(",");
	                	port = viewService.GetPort(arrUsingPort);
	                	mv.addObject("PORT_LIST", line);
	                	mv.addObject("PORT", port);
	                	request.getSession().setAttribute("ServerPort", port);
	                	break;
					}
	            }
	            
	            if (request.getSession().getAttribute("ServerPort") == null) {
	            	request.getSession().setAttribute("ServerPort", viewService.GetPort(arrUsingPort));
	            	//QQQ session timeout 24hr
	            	request.getSession().setMaxInactiveInterval(24*60*60);
	    		}
	        } catch (IOException e) {
		        	mv.addObject("PORT_ERROR", e.getMessage());
	//	        	System.out.println(e.getMessage()); 
	//	            e.printStackTrace();
	        }
		}	
		
		return mv;
    }
	
	@RequestMapping(value="/view/noitce.do")
    public ModelAndView Notice(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("/popup/Notice");
		
		mv.addObject("STR", viewService.Notice(commandMap.getMap(), request));
		return mv;
    }
	
	@RequestMapping(value="/view/logout.do")
    public ModelAndView LogOut(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("redirect:/");		
		
		/*Cookie[] cookies = request.getCookies();
    	if(cookies != null){            
            for(int i=0; i < cookies.length; i++){
                if (cookies[i].getName().equals("USER_ID") ||
                		cookies[i].getName().equals("USER_SEQ") ||
                		cookies[i].getName().equals("USER_NAME") ||
                		cookies[i].getName().equals("USER_TYPE") ||
                		cookies[i].getName().equals("UNIQID")) {
                	cookies[i].setMaxAge(0);
                	cookies[i].setPath("/");
                	
                	response.addCookie(cookies[i]);
				}
            }
        }*/
		return mv;
    }
	
	@RequestMapping(value="/view/tabstask.do")
	public ModelAndView showTablsTask(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		ModelAndView mv = new ModelAndView("/bizchatbot/tabs_task");
		
		//QQQ
		if(commandMap.get("PROJECT_NAME") != null && commandMap.get("SEQ_NO") != null) {
			adminService.updateProject((String)commandMap.get("PROJECT_NAME"), Integer.parseInt((String)commandMap.get("SEQ_NO")), request);
//			Cookie prjno = new Cookie("ProjectNo", commandMap.get("SEQ_NO").toString());
//			prjno.setMaxAge(3600);
//			Cookie prjname = new Cookie("ProjectName", commandMap.get("PROJECT_NAME").toString());
//			prjname.setMaxAge(3600);
//			response.addCookie(prjname);
		}
		
//		ModelAndView mv = new ModelAndView("/agent/AgentList");    	
	    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
	    	//QQQ
	    	commandMap.put("PROJECT_SEQ", commandMap.get("SEQ_NO"));
	    	List<Map<String, Object>> list = viewService.selectAgentGraph(commandMap.getMap());
	    	mv.addObject("conlist", list);
	    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/tabsentity.do")
	public ModelAndView showTablsEntity(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("/bizchatbot/tabs_entity");
		
//		ModelAndView mv = new ModelAndView("/slot/SlotList");    	
    	
	    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
	    	String strWhere = "";
	    	if (commandMap.get("AGENT_SEQ") != null && !commandMap.get("AGENT_SEQ").toString().equals("0") && !commandMap.get("AGENT_SEQ").toString().isEmpty()) {
	    		strWhere = "AND (AGENT_SEQ IN (" + commandMap.get("AGENT_SEQ").toString() + ", 0) OR AGENT_SEQ IS NULL)";
			}
	    		
	    	commandMap.put("WHERE_SQL", strWhere);    	
	    	List<Map<String, Object>> clist = viewService.selectSlotClass(commandMap.getMap());    	
	    	List<Map<String, Object>> slist = viewService.selectSlot(commandMap.getMap());	   	    
	    	
	    	mv.addObject("CLASS_LIST", clist);    	   
	//    	log.debug(slist);
	    	mv.addObject("SLOT_LIST", CommonUtils.SetSlotListToJson(slist));
	    	
	    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/tabsintent.do")
	public ModelAndView showTablsIntent(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("/bizchatbot/tabs_intent");
		
//		ModelAndView mv = new ModelAndView("/useruttr/UserUttrList");
	    	commandMap.put("PROJECT_SEQ", request.getSession().getAttribute("ProjectNo"));
	    	
	    	String strWhere = "";
	    	if (commandMap.get("AGENT_SEQ") != null && !commandMap.get("AGENT_SEQ").toString().equals("0") && !commandMap.get("AGENT_SEQ").toString().isEmpty()) {
	    		//strWhere = "AND (AGENT_SEQ IN (" + commandMap.get("AGENT_SEQ").toString() + ", 0) OR AGENT_SEQ IS NULL)";    		
	    		strWhere = "AND AGENT_SEQ = " + commandMap.get("AGENT_SEQ").toString();
			}
	    	
	    	commandMap.put("WHERE_SQL", strWhere);
	    	List<Map<String,Object>> list = viewService.selectIntents(commandMap.getMap());
	    	
	    	mv.addObject("list", list);
	    	mv.addObject("INTENTS_LIST", CommonUtils.SetIntentListToJson(list));
	    	
	    	return SetMenu(mv, commandMap, request);
    }
	
	@RequestMapping(value="/view/showresppop.do")
	public ModelAndView showRespPop(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("/popup/respPop");
		
		String obj = StrUtils.getStr((String)commandMap.get("obj"), "");//respCode
		String projectName = (String)request.getSession().getAttribute("ProjectName");
		int agentSeq = StrUtils.getNum((String)commandMap.get("agentSeq"), 0);
		String uiPos = StrUtils.getStr((String)commandMap.get("uiPos"), "");
		int uiPosVal = StrUtils.getNum((String)commandMap.get("uiPosVal"), 0);
		String msg = "";
		String code = "";
		if(!StringUtils.isEmpty(obj)) {
			ChatbotRespInterfaceRelData data = sdsService.getRespInfoByPos(projectName, agentSeq, uiPos, uiPosVal);
			if(data != null) {
				mv.addObject("currResp", data);
				mv.addObject("exist", true);
				mv.addObject("respTypeCd", data.getRespTypeCd());
				code = data.getRespCode();
				msg = data.getRespMsg();
			}else {
				mv.addObject("exist", false);
				
				code = StrUtils.extractRespSystemCode(obj);
				if(!StringUtils.isEmpty(code)) {
					msg = StrUtils.removeSystemCode(obj);
				}else {
					String rndCode = sdsService.generateRespCode(projectName, agentSeq);
					code = rndCode;
					msg = obj;
				}
			}
			
			
		}else {
			String rndCode = sdsService.generateRespCode(projectName, agentSeq);
			code = rndCode;
		}
		
		if(StringUtils.isEmpty(msg)) {
			//get message form intent utter
		}
		
		mv.addObject("respCode", StrUtils.removePrefix(code));
		mv.addObject("respMsg", msg);
		mv.addObject("agentSeq", agentSeq);
		mv.addObject("uiPos", uiPos);
		mv.addObject("uiPosVal", uiPosVal);
		mv.addObject("obj", obj);
		return mv;
	}
	
	@RequestMapping(value="/view/showresppopintent.do")
	public ModelAndView showRespPopIntent(CommandMap commandMap, HttpServletRequest request) throws Exception{
		ModelAndView mv = new ModelAndView("/popup/respPopIntent");
		
		String obj = StrUtils.getStr((String)commandMap.get("obj"), "");//respCode
		String projectName = (String)request.getSession().getAttribute("ProjectName");
		int intentSeq = StrUtils.getNum((String)commandMap.get("intentSeq"), 0);
		String msg = "";
		String code = "";
		if(!StringUtils.isEmpty(obj)) {
			ChatbotRespInterfaceRelData data = sdsService.getRespInfoByIntent(projectName, intentSeq);
			if(data != null) {
				mv.addObject("currResp", data);
				mv.addObject("exist", true);
				mv.addObject("respTypeCd", data.getRespTypeCd());
				code = data.getRespCode();
				msg = data.getRespMsg();
			}else {
				mv.addObject("exist", false);
				
				code = StrUtils.extractRespSystemCode(obj);
				if(!StringUtils.isEmpty(code)) {
					msg = StrUtils.removeSystemCode(obj);
				}else {
					String rndCode = sdsService.generateRespCodeWIntent(projectName, intentSeq);
					code = rndCode;
					msg = obj;
				}
			}
			
			
		}else {
			String rndCode = sdsService.generateRespCodeWIntent(projectName, intentSeq);
			code = rndCode;
		}
		
		if(StringUtils.isEmpty(msg)) {
			//get message form intent utter
		}
		
		mv.addObject("respCode", StrUtils.removePrefix(code));
		mv.addObject("respMsg", msg);
		mv.addObject("agentSeq", 0);
		mv.addObject("uiPos", "");
		mv.addObject("uiPosVal", 0);
		mv.addObject("intentSeq", intentSeq);
		mv.addObject("obj", obj);
		return mv;
	}
	
	@RequestMapping(value="/view/loginmodule.do")
	public ModelAndView loginmodule(HttpServletRequest req, HttpServletResponse resp) throws Exception{
		ModelAndView mv = new ModelAndView("/chatbotadmin");	
		return mv;
	}
}
