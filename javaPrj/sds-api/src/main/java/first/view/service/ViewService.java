package first.view.service;

import java.io.File;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface ViewService {
	void CreateTable(Map<String, Object> map);
	
	void DropTable(Map<String, Object> map);
	
	void CreateColumn(Map<String, Object> map);
	
	void CreateMultiColumn(Map<String, Object> map);
	
	void AlterColumn(Map<String, Object> map);

	void DropColumn(Map<String, Object> map);
	
	void DropMultiColumn(Map<String, Object> map);
	
	void insertProject(Map<String, Object> map, HttpServletRequest request);

	void insertSlotClass(Map<String, Object> map);	
	
	void insertSlot(Map<String, Object> map);

	String selectSlotColor(Map<String, Object> map);
	
	void insertSlotTypeClass(Map<String, Object> map);
	
	void deleteSlotTypeClass(Map<String, Object> map);
	
	void deleteSlotTypeClassAll(Map<String, Object> map);
	
	void deleteProject(Map<String, Object> map, HttpServletRequest request);
	
	void deleteSlotClass(Map<String, Object> map);
	
	void deleteSlotClassSeq(Map<String, Object> map);
		
	void deleteSlot(Map<String, Object> map) throws Exception;
	
	void updateProject(Map<String, Object> map, HttpServletRequest request);
	
	void updateSlotClass(Map<String, Object> map);
	
	void updateSlot(Map<String, Object> map);
	
	void updateSlotSort(Map<String, Object> map);
	
	void insertSlotObjectDetail(Map<String, Object> map);

	void insertSlotObject(Map<String, Object> map);

	void deleteSlotObject(Map<String, Object> map);

	void deleteSlotObjectDetail(Map<String, Object> map);
	
	void updateSlotObject(Map<String, Object> map);
	
	void insertSlotInstans(Map<String, Object> map);
	
	void updateSlotInstans(Map<String, Object> map);
	
	void deleteSlotInstans(Map<String, Object> map);
	
	void insertSlotInstansExcel(Map<String, Object> map, HttpServletRequest request);
	
	void insertSlotObjectExcel(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	void insertIntent(Map<String, Object> map);
	
	void deleteIntent(Map<String, Object> map);
	
	void deleteIntentUserSay(Map<String, Object> map);
	
	void deleteIntentUserSayDetail(Map<String, Object> map);
	
	void deleteIntentsUserSays(Map<String, Object> map);
	
	void deleteIntentsIntention(Map<String, Object> map);
	
	void deleteIntentsObject(Map<String, Object> map);
	
	void deleteIntentsObjectDetail(Map<String, Object> map);	
		
	void insertAgent(Map<String, Object> map);
	
	void insertOnlyAgent(Map<String, Object> map);
	
	void updateAgentPosition(Map<String, Object> map);
	
	void insertAgentGraph(Map<String, Object> map);
	
	void deleteAgentGraph(Map<String, Object> map);
	
	void deleteAgent(Map<String, Object> map);
	
	void insertUser(Map<String, Object> map);

	void updateUser(Map<String, Object> map);

	void deleteUser(Map<String, Object> map);
	
	void insertLua(Map<String, Object> map);

	void updateLua(Map<String, Object> map);
	
	void deleteLua(Map<String, Object> map);
	
	void insertUserDomain(Map<String, Object> map);
	
	void deleteUserDomain(Map<String, Object> map);
	
	List<String> copyDomain(Map<String, Object> map, List<Map<String, Object>> prjlist, HttpServletRequest request) throws Exception;
	
	List<Map<String, Object>> selectProject(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> SelectProjectName(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotClass(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlot(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlot2(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotIntents(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectSlotDefine(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectFunction(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotType(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectSlotObject(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotObjectDetail(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotObjectDetailExcel(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectSlotInstances(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotInstances2(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectSlotTypeClass(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectVSlotObject(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntents(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntentsUserSays(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntentsUserSays2(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntentIntention(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntentsObejct(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectIntentsObjectDtl(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectFliingSlot(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgents(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectAgent(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentSlotFilling(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentIntention(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentObject(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentObjectdtl(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectAgentGraph(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectUserIdChk(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectUserList(Map<String, Object> map) throws Exception;	
	
	List<Map<String, Object>> selectLuaList(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectLua(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectUser(Map<String, Object> map) throws Exception;
	
	Map<String, Object> loginUser(Map<String, Object> map) throws Exception;
	
	Map<String, Object> selectIntent(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectUserDomain(Map<String, Object> map) throws Exception;
	
	List<Map<String, Object>> selectUserDomainList(Map<String, Object> map) throws Exception;
	
	Boolean DialogsLearning(Map<String, Object> map, HttpServletRequest request) throws Exception;

	String Notice(Map<String, Object> map, HttpServletRequest request) throws Exception;
	
	String GetPort(String[] arrUsingPort) throws Exception;
	
	public Map<String, Object> getDomainByAgentSeq(int agentSeq);
}
