package first.view.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import first.common.common.CommandMap;
import first.common.util.Color;
import first.common.util.CommonUtils;
import first.common.util.ExcelUtils;
import first.common.util.FileUtils;
import first.common.util.Ports;
import first.common.util.ReadOption;
import first.view.dao.ViewDAO;


@Service("viewService")
public class ViewServiceImpl implements ViewService {
Logger log = LoggerFactory.getLogger(this.getClass());
int idx = 1;

//	@Resource(name="viewDAO")
	@Autowired
	@Qualifier("viewDAO")
	private ViewDAO viewDAO;

//	@Resource(name="fileUtils")
	@Autowired
	@Qualifier("fileUtils")
	private FileUtils fileUtils;
	
	@Autowired
	@Qualifier("viewService")
	private ViewServiceImpl viewService;
	
	
	@Value("${enginePath}")
	private String enginePath;


	
	@Override
	public void CreateTable(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.CreateTable(map);
	}
	
	@Override
	public void DropTable(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.DropTable(map);
	}
	
	@Override
	public void CreateColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.CreateColumn(map);
	}
	
	@Override
	public void CreateMultiColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;
                 
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("SLOT_LIST"));
			JSONArray objArray = (JSONArray)jsonObj.get(map.get("TYPE_SEQ").toString());
			ColRecursive(map, jsonObj, objArray, 1);			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	private void ColRecursive(Map<String, Object> map, JSONObject jsonObj, JSONArray objArray, int Type) {
		CommandMap SlotObjMap = new CommandMap();
		for(int i = 0 ; i < objArray.size() ; i++){			
			JSONObject SlotObj = (JSONObject)objArray.get(i);
			
			if (SlotObj.get("SLOT_TYPE").toString().equals("C")) {
				ColRecursive(map, jsonObj, (JSONArray)jsonObj.get(SlotObj.get("TYPE_SEQ").toString()), Type);
			}
			else {
				SlotObjMap.put("SEQ_NO", map.get("SEQ_NO"));
				SlotObjMap.put("COL_NAME", map.get("SEQ_NO") + "_" + map.get("SLOT_SEQ_NO") + "_" + SlotObj.get("SLOT_NAME"));
				SlotObjMap.put("COL_TYPE", "varchar(200)");
				
				if (Type == 1) {
					viewDAO.CreateColumn(SlotObjMap.getMap());
				}
				else {
					viewDAO.DropColumn(SlotObjMap.getMap());
				}				
			}
		}
	}		
	
	@Override
	public void AlterColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.AlterColumn(map);
	}
	
	@Override
	public void DropColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.DropColumn(map);
	}
	
	@Override
	public void DropMultiColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;
                 
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("SLOT_LIST"));			
			JSONArray objArray = (JSONArray)jsonObj.get(map.get("OLD_TYPE_SEQ"));
			ColRecursive(map, jsonObj, objArray, 2);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	@Override
	public void insertProject(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub
		viewDAO.insertProject(map);
		//viewDAO.updatePorts(map);				
		
		String emptyPath = "";
		String copyPath = "";
		
		//emptyPath = "E:\\dev\\file\\webtool\\www_data\\dialog_domain\\empty";
		//copyPath = "E:\\dev\\file\\webtool\\www_data\\dialog_domain\\" + map.get("PROJECT_NAME").toString();
//		emptyPath = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/empty";
//		copyPath = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/" + map.get("PROJECT_NAME").toString();
		emptyPath = enginePath + "/dialog_domain/empty";
		copyPath = enginePath + "/dialog_domain/" + map.get("PROJECT_NAME").toString();
		log.debug("insertProject/Make Domain files {}->{}", emptyPath, copyPath);
		
		File s = new File(emptyPath);
		File t = new File(copyPath);
		if(!t.exists()) {
			t.mkdir();
		}
		
		Directorycopy(s, t);
		
		File oldsql = new File(copyPath + "/empty_DB.sqlite3");
		File renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + "_DB.sqlite3");
		oldsql.renameTo(renamesql);
		oldsql = new File(copyPath + "/empty.task.txt");
		renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".task.txt");
		oldsql.renameTo(renamesql);
		oldsql = new File(copyPath + "/empty.slot.txt");
		renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".slot.txt");
		oldsql.renameTo(renamesql);
		oldsql = new File(copyPath + "/empty.dialogLib.txt");
		renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".dialogLib.txt");
		oldsql.renameTo(renamesql);
		oldsql = new File(copyPath + "/empty.DAtype.txt");
		renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".DAtype.txt");
		oldsql.renameTo(renamesql);
		oldsql = new File(copyPath + "/empty.svm");
		renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".svm");
		oldsql.renameTo(renamesql);
		/*if (map.get("TYPE").toString().equals("I")) {
			//emptyPath = "E:\\dev\\file\\webtool\\www_data\\dialog_domain\\empty";
			//copyPath = "E:\\dev\\file\\webtool\\www_data\\dialog_domain\\" + map.get("PROJECT_NAME").toString();
			emptyPath = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/empty";
			copyPath = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/" + map.get("PROJECT_NAME").toString();
		}
		else {
			//emptyPath = "E:\\dev\\file\\webtool\\www_data\\dialog_domain\\" + map.get("OLD_PROJECT_NAME").toString();
			//copyPath = "E:\\dev\\file\\webtool\\www_data\\dialog_domain\\" + map.get("PROJECT_NAME").toString();
			emptyPath = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/" + map.get("OLD_PROJECT_NAME").toString();
			copyPath = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/" + map.get("PROJECT_NAME").toString();
		}
		
		File s = new File(emptyPath);
		File t = new File(copyPath);
		if(!t.exists()) {
			t.mkdir();
		}
		
		Directorycopy(s, t);
		
		if (map.get("TYPE").toString().equals("I")) {
			File oldsql = new File(copyPath + "/empty_DB.sqlite3");
			File renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + "_DB.sqlite3");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/empty.task.txt");
			renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".task.txt");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/empty.slot.txt");
			renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".slot.txt");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/empty.dialogLib.txt");
			renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".dialogLib.txt");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/empty.DAtype.txt");
			renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".DAtype.txt");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/empty.svm");
			renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".svm");
			oldsql.renameTo(renamesql);
		}
		else {
			File oldsql = new File(copyPath + "/" + map.get("OLD_PROJECT_NAME").toString() + "_DB.sqlite3");
			File renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + "_DB.sqlite3");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/" + map.get("OLD_PROJECT_NAME").toString() + ".task.txt");
			renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".task.txt");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/" + map.get("OLD_PROJECT_NAME").toString() + ".slot.txt");
			renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".slot.txt");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/" + map.get("OLD_PROJECT_NAME").toString() + ".dialogLib.txt");
			renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".dialogLib.txt");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/" + map.get("OLD_PROJECT_NAME").toString() + ".DAtype.txt");
			renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".DAtype.txt");
			oldsql.renameTo(renamesql);
			oldsql = new File(copyPath + "/" + map.get("OLD_PROJECT_NAME").toString() + ".svm");
			renamesql = new File(copyPath + "/" + map.get("PROJECT_NAME").toString() + ".svm");
			oldsql.renameTo(renamesql);
		}*/
	}
	
	@Override
	public void insertSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.insertSlotClass(map);
	}
	
	@Override
	public void insertSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub	
		
		if (!map.get("SLOT_TYPE").toString().equals("C")) {
			Random random = new Random();
			SlotColorCheck(Color.City[random.nextInt(109)], map);		
		}
		
		viewDAO.insertSlot(map);
		SlotDefine(map);
	}
	
	@Override
	public String selectSlotColor(Map<String, Object> map) {
		// TODO Auto-generated method stub				
		Map<String, Object> obj = viewDAO.selectSlotColorCheck(map);
		
		if (obj != null) {
			return obj.get("COLOR").toString();
		}
		else {
			Random random = new Random();
			SlotColorCheck(Color.City[random.nextInt(109)], map);
			viewDAO.insertSlotColor(map);
			return map.get("COLOR").toString();
		}
	}
	
	private void SlotColorCheck(String color, Map<String, Object> map) {		
		map.put("COLOR", color);
		Map<String, Object> obj = viewDAO.selectSlotColor(map);
		
		if (obj != null) {
			Random random = new Random();
			SlotColorCheck(Color.City[random.nextInt(109)], map);
		} 
	}
	
	private void SlotDefine(Map<String, Object> map) {
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap DefineObjMap;
        viewDAO.deleteSlotDefine(map);    
		try {			
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("DEFINE_ITEM"));
			
			if (jsonObj.containsKey("DEFINE_ITEM")) {
				JSONArray objArray = (JSONArray)jsonObj.get("DEFINE_ITEM");
							
				for(int i=0 ; i< objArray.size() ; i++){
	                JSONObject DefineObj = (JSONObject)objArray.get(i);
	                DefineObjMap = new CommandMap();
	                DefineObjMap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
	                DefineObjMap.put("AGENT_SEQ", map.get("AGENT_SEQ"));
	                DefineObjMap.put("SLOT_SEQ_NO", map.get("SEQ_NO"));
	                DefineObjMap.put("TARGET_NAME", DefineObj.get("TARGET_NAME"));
	                DefineObjMap.put("TARGET_SEQ", DefineObj.get("TARGET_SEQ"));
	                DefineObjMap.put("CON", DefineObj.get("CON"));
	                DefineObjMap.put("ACT", DefineObj.get("ACT"));
	                DefineObjMap.put("TARGET_SUB_SEQ", DefineObj.get("TARGET_SUB_SEQ"));
	                
	                viewDAO.insertSlotDefine(DefineObjMap.getMap());                
	            }
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void insertSlotTypeClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.insertSlotTypeClass(map);
	}
	
	@Override
	public void deleteSlotTypeClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotTypeClass(map);
	}
	
	@Override
	public void deleteSlotTypeClassAll(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotTypeClassAll(map);
	}
	
	@Override
	public void deleteProject(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub
		map.put("PROJECT_SEQ", map.get("SEQ_NO"));
		List<Map<String, Object>> clist = viewDAO.selectSlotClass2(map);
		for (Iterator iterator = clist.iterator(); iterator.hasNext();) {
			Map<String, Object> citem = (Map<String, Object>) iterator.next();
			
			DropTable(citem);
		}
		
		viewDAO.deleteProject(map);
		//viewDAO.updatePorts2(map);
		
		//String Path = "E:\\dev\\file\\webtool\\www_data\\dialog_domain\\" + map.get("PROJECT_NAME").toString();
		//String Path = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/" + map.get("PROJECT_NAME").toString();
		String Path = enginePath + "/dialog_domain/" + map.get("PROJECT_NAME").toString();
		log.debug("deleteProject path:{}",Path);
		deleteAllFiles(Path);
	}
	
	@Override
	public void deleteSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotClass(map);
	}
	
	@Override
	public void deleteSlotClassSeq(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotClassSeq(map);
	}
	
	@Override
	public void deleteSlot(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub		
		viewDAO.deleteSlot(map);
		viewDAO.deleteSlotDefine(map);
	}
	
	@Override
	public void updateProject(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub
		viewDAO.updateProject(map);					
		
		if (!map.get("OLD_NAME").toString().equals(map.get("PROJECT_NAME").toString())) {
			//String orgPath = "E:\\dev\\file\\webtool\\www_data\\dialog_domain\\" + map.get("OLD_NAME").toString();
			//String newPath = "E:\\dev\\file\\webtool\\www_data\\dialog_domain\\" + map.get("PROJECT_NAME").toString();
//			String orgPath = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/" + map.get("OLD_NAME").toString();
//			String newPath = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/" + map.get("PROJECT_NAME").toString();
			String orgPath = enginePath + "/dialog_domain/" + map.get("OLD_NAME").toString();
			String newPath = enginePath + "/dialog_domain/" + map.get("PROJECT_NAME").toString();
						
			File orgfile = new File(orgPath);
			File newfile = new File(newPath);
			
			orgfile.renameTo(newfile);
						
			//orgfile = new File(newPath + "\\data\\" + map.get("OLD_NAME").toString() + "_DB.sqlite3");
			//newfile = new File(newPath + "\\data\\" + map.get("PROJECT_NAME").toString() + "_DB.sqlite3");
			orgfile = new File(newPath + "/data/" + map.get("OLD_NAME").toString() + "_DB.sqlite3");
			newfile = new File(newPath + "/data/" + map.get("PROJECT_NAME").toString() + "_DB.sqlite3");
			orgfile.renameTo(newfile);
		}				
	}
	
	@Override
	public void updateSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateSlotClass(map);
	}
	
	@Override
	public void updateSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateSlot(map);
		SlotDefine(map);
	}
	
	@Override
	public void updateSlotSort(Map<String, Object> map) {
		// TODO Auto-generated method stub
		CommandMap SlotObjMap = new CommandMap();
		SlotObjMap.put("SEQ_NO", map.get("S_SEQ_NO"));
		SlotObjMap.put("SORT_NO", map.get("EINDEX"));
		viewDAO.updateSlotSort(SlotObjMap.getMap());
		SlotObjMap = new CommandMap();
		SlotObjMap.put("SEQ_NO", map.get("E_SEQ_NO"));
		SlotObjMap.put("SORT_NO", map.get("SINDEX"));			
		viewDAO.updateSlotSort(SlotObjMap.getMap());
	}
	
	@Override
	public void insertSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap SlotObjMap;
        CommandMap SlotObjDetailMap;
                 
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("OBJECT_ITEM"));
			JSONArray objArray = (JSONArray)jsonObj.get("OBJECT_ITEM");
						
			for(int i=0 ; i< objArray.size() ; i++){
                JSONObject SlotObj = (JSONObject)objArray.get(i);
                SlotObjMap = new CommandMap();
                SlotObjMap.put("OBJECT_NAME", SlotObj.get("OBJECT_NAME"));
                SlotObjMap.put("SLOT_SEQ_NO", map.get("SLOT_SEQ_NO"));
                
                if (SlotObj.get("SEQ_NO").toString().equals("0")) {
                	viewDAO.insertSlotObject(SlotObjMap.getMap());                    
				}
                else {                	
        			SlotObjMap.put("SEQ_NO", SlotObj.get("SEQ_NO"));
                    
                	viewDAO.updateSlotObject(SlotObjMap.getMap());
                	viewDAO.deleteSlotObjectDetail(SlotObjMap.getMap());
                }     
                
                JSONArray objDetailArray = (JSONArray)SlotObj.get("OBJECT_DETAIL");
                for(int j=0 ; j< objDetailArray.size() ; j++) {
                	JSONObject SlotObjDetail = (JSONObject)objDetailArray.get(j);
                	SlotObjDetailMap = new CommandMap();
                	SlotObjDetailMap.put("OBJECT_NAME", SlotObjDetail.get("OBJECT_NAME"));
                	SlotObjDetailMap.put("PARENT_SEQ_NO", SlotObjMap.get("SEQ_NO"));
                	SlotObjDetailMap.put("SLOT_SEQ_NO", SlotObjMap.get("SLOT_SEQ_NO"));
                	
                	viewDAO.insertSlotObjectDetail(SlotObjDetailMap.getMap());
                }
            }								
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}                
	}
	
	@Override
	public void insertSlotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.insertSlotObjectDetail(map);
	}
	
	@Override
	public void deleteSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotObject(map);
	}
	
	@Override
	public void deleteSlotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotObjectDetail(map);
	}
	
	@Override
	public void updateSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateSlotObject(map);
	}
	
	@Override
	public void insertSlotInstans(Map<String, Object> map) {
		// TODO Auto-generated method stub
		//viewDAO.insertSlotInstans(map);
		JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj;      
        CommandMap SlotInstansMap;        
                
		try {
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("VALUES_ITEM"));
			JSONArray objArray = (JSONArray)jsonObj.get("ITEM");
			
			for(int i=0 ; i< objArray.size() ; i++){
                JSONObject SlotObj = (JSONObject)objArray.get(i);
                SlotInstansMap = new CommandMap();
                SlotInstansMap.put("INSERT", map.get("INSERT_ITEM"));
                SlotInstansMap.put("VALUES", SlotObj.get("ITEM_DETAIL"));
                                
            	viewDAO.insertSlotInstans(SlotInstansMap.getMap());                    				
            }									
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}            
	}
	
	@Override
	public void updateSlotInstans(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateSlotInstans(map);
	}

	@Override
	public void deleteSlotInstans(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteSlotInstans(map);
	}	
	
	@Override	
	public void insertSlotInstansExcel(Map<String, Object> map, HttpServletRequest request) {
		// TODO Auto-generated method stub
		String strFilePath = "";
		
		try {
			strFilePath = fileUtils.SaveFile(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug(e.getMessage());
		}				
		
		String[] arrHeader = map.get("HEADER_LIST").toString().split(",");
		String[] arrCeader = map.get("COL_LIST").toString().split(",");
		List<String> lstHeader = new ArrayList<String>();
		List<String> insertHeader = new ArrayList<String>();
		
		for (int i = 1; i <= arrHeader.length; i++) {
			lstHeader.add(IntToLetter(i));			
		}
			
		ReadOption readOption = new ReadOption();
		readOption.setFilePath(strFilePath);
		readOption.setOutputColumns(lstHeader);
		readOption.setStartRow(1);
		
		ExcelUtils exu = new ExcelUtils();
		List<Map<String, String>> excelContent = exu.read(readOption);
		
		StringBuilder strinsert = new StringBuilder();
		
		/*strinsert.append("INSERT INTO slot_class_" + map.get("SEQ_NO") + "(");
		for (int i = 0; i < arrCeader.length; i++) {		
			if ((1+i) != arrCeader.length) {
				strinsert.append(arrCeader[i] + ", ");
			}
			else
				strinsert.append(arrCeader[i]);
		}
		strinsert.append(")");*/
		
		StringBuilder strvalues = new StringBuilder();
		int iCnt = 0;
		
		for(Map<String, String> article : excelContent){
			//if (iCnt == 0) {�� �ȿ� ���� ���� �߰� ������ else�ȿ� �ִ°�)
			if (iCnt == 0) {				
				for (int k = 0; k < lstHeader.size(); k++) {
					for (int j = 0; j < arrHeader.length; j++) {						
						if (arrHeader[j].equals(article.get(lstHeader.get(k)).toString())) {
							insertHeader.add(arrCeader[j]);							
							break;
						}
					}													
				}	
				
				strinsert.append("INSERT INTO slot_class_" + map.get("SEQ_NO") + "(");
				for (int i = 0; i < insertHeader.size(); i++) {		
					if ((1+i) != insertHeader.size()) {
						strinsert.append(insertHeader.get(i) + ", ");
					}
					else
						strinsert.append(insertHeader.get(i));
				}
				strinsert.append(")");
			}
			else {
				strvalues.setLength(0);
				strvalues.append("VALUES(");
				for (int i = 0; i < lstHeader.size(); i++) {
					if ((1+i) != lstHeader.size()) 
						strvalues.append("\"" + article.get(lstHeader.get(i)) + "\", ");
					else
						strvalues.append("\"" + article.get(lstHeader.get(i)) + "\"");													
				}
				strvalues.append(")");
				
				map.put("INSERT", strinsert.toString());
				map.put("VALUES",  strvalues.toString());
				
				viewDAO.insertSlotInstans(map);
			}
			iCnt++;;
		}
	}
	
	@Override	
	public void insertSlotObjectExcel(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		
		String strFilePath = "";
		try {
			strFilePath = fileUtils.SaveFile(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.debug(e.getMessage());
		}
			
		List<String> lstHeader = new ArrayList<String>();
		List<String> lstObjval = new ArrayList<String>();
				
		for (int i = 1; i < 256; i++) {
			lstHeader.add(IntToLetter(i));
		}
		
		ReadOption readOption = new ReadOption();
		readOption.setFilePath(strFilePath);
		readOption.setOutputColumns(lstHeader);
		readOption.setStartRow(1);
		
		ExcelUtils exu = new ExcelUtils();
		List<Map<String, String>> excelContent = exu.read(readOption);
		
		CommandMap SlotObjMap = new CommandMap();
		CommandMap SlotObjDetailMap;
		map.put("STATUS", "S");
		
		for(Map<String, String> article : excelContent) {
//			log.debug(article);
			for (int i = 0; i < article.size(); i++) {
				if (!article.get(lstHeader.get(i)).isEmpty()) {						
					if (i == 0) {
						if (lstObjval.contains(article.get(lstHeader.get(i)).toString())) {
							map.put("STATUS", "F");
							throw new Exception();
						}
						else
							lstObjval.add(article.get(lstHeader.get(i)).toString());
						SlotObjMap = new CommandMap();
						SlotObjMap.put("OBJECT_NAME", article.get(lstHeader.get(i)).toString());
		                SlotObjMap.put("SLOT_SEQ_NO", map.get("SLOT_SEQ_NO"));				
	                	viewDAO.insertSlotObject(SlotObjMap.getMap());
					}
					else {
						SlotObjDetailMap = new CommandMap();
	                	SlotObjDetailMap.put("OBJECT_NAME", article.get(lstHeader.get(i)).toString());
	                	SlotObjDetailMap.put("PARENT_SEQ_NO", SlotObjMap.get("SEQ_NO"));
	                	SlotObjDetailMap.put("SLOT_SEQ_NO", SlotObjMap.get("SLOT_SEQ_NO"));
                		viewDAO.insertSlotObjectDetail(SlotObjDetailMap.getMap());
					}
				}
				else {
					break;
				}
			}
		}	
	}
	
	public static String IntToLetter(int Int) {
		if (Int<27){
			return Character.toString((char)(Int+96)).toUpperCase();
		} else {
			if (Int%26==0) {
				return IntToLetter((Int/26)-1)+IntToLetter(((Int-1)%26+1));
			} else {
				return IntToLetter(Int/26)+ IntToLetter(Int%26);
			}
		}
	}
	
	@Override
	public void insertIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonItObj;
        JSONObject UserSayObj;
        CommandMap UserSayMap;
        CommandMap IntentItMap;
        CommandMap IntentObjMap;
        CommandMap IntentObjDetailMap;
        
        int iIntentNo;        
		try {
			if (map.get("SEQ_NO").toString().isEmpty()) {				
				viewDAO.insertIntent(map);
				iIntentNo = (int)map.get("SEQ_NO");
			}
			else {
				iIntentNo = Integer.parseInt(map.get("SEQ_NO").toString());				
				CommandMap ObjDelMap = new CommandMap();				
				viewDAO.updateIntent(map);
				ObjDelMap.put("SEQ_NO", iIntentNo);
				if (map.get("SLOT_SEQ_NO").toString().isEmpty()) {
					viewDAO.deleteIntentsIntention(ObjDelMap.getMap());
					viewDAO.deleteIntentsObject(ObjDelMap.getMap());
					viewDAO.deleteIntentsObjectDetail(ObjDelMap.getMap());
				}
				else {
					ObjDelMap.put("SLOT_SEQ_NO", map.get("SLOT_SEQ_NO"));					
					viewDAO.deleteIntentsObjectDetail2(ObjDelMap.getMap());
					viewDAO.deleteIntentsObject2(ObjDelMap.getMap());
					viewDAO.deleteIntentsIntention2(ObjDelMap.getMap());									
				}
			}
													
			UserSayObj = (JSONObject)jsonParser.parse((String)map.get("USER_SAYS"));
			JSONArray UserSayArray = (JSONArray)UserSayObj.get("USER_SAYS");
			
			for(int i=0 ; i< UserSayArray.size() ; i++){
				
				JSONObject UsObj = (JSONObject)UserSayArray.get(i);
				UserSayMap = new CommandMap();
				UserSayMap.put("SAY", UsObj.get("SAY"));
				UserSayMap.put("SAY_TAG", UsObj.get("SAY_TAG"));
				UserSayMap.put("SLOT_TAG", UsObj.get("SLOT_TAG"));
				UserSayMap.put("SLOT_SEQ_NO", UsObj.get("SLOT_SEQ_NO"));
				UserSayMap.put("SLOT_NAME", UsObj.get("SLOT_NAME"));
				UserSayMap.put("SLOT_MAP", UsObj.get("SLOT_MAP"));
				UserSayMap.put("SLOT_MAPDA", UsObj.get("SLOT_MAPDA"));
				UserSayMap.put("SEQ_NO", UsObj.get("SEQ_NO"));
				UserSayMap.put("INTENT_SEQ", iIntentNo);
				
				if (UsObj.get("SEQ_NO").toString().equals("0")) {
					viewDAO.insertIntentUserSay(UserSayMap.getMap());					
				}
				else {
					viewDAO.updateIntentUserSay(UserSayMap.getMap());
				}				
			}
			
			jsonItObj = (JSONObject)jsonParser.parse((String)map.get("OBJECT_ITEM"));
			JSONArray ItObjArray = (JSONArray)jsonItObj.get("OBJECT_ITEM");
						
			for(int i=0 ; i< ItObjArray.size() ; i++){
                JSONObject IntentItObj = (JSONObject)ItObjArray.get(i);
                IntentItMap = new CommandMap();
                IntentItMap.put("UTTR", IntentItObj.get("UTTR"));
                IntentItMap.put("ACT", IntentItObj.get("ACT"));               
                IntentItMap.put("SLOT_SEQ_NO", IntentItObj.get("SLOT_SEQ_NO"));
                IntentItMap.put("INTENT_SEQ", iIntentNo);
                
                viewDAO.insertIntentIntention(IntentItMap.getMap());                     
                                
    			JSONArray objArray = (JSONArray)IntentItObj.get("OBJECT");
                for(int k=0 ; k< objArray.size() ; k++){
                    JSONObject IntentObj = (JSONObject)objArray.get(k);
                    IntentObjMap = new CommandMap();            
                    IntentObjMap.put("INTENTION_SEQ", IntentItMap.get("SEQ_NO"));
                    IntentObjMap.put("OBJECT_VALUE", IntentObj.get("OBJECT_VALUE"));                
                    IntentObjMap.put("INTENT_SEQ", iIntentNo);
                    
                    viewDAO.insertIntentObject(IntentObjMap.getMap());       
                                
                
	                JSONArray objDetailArray = (JSONArray)IntentObj.get("OBJECT_DETAIL");
	                for(int j=0 ; j< objDetailArray.size() ; j++) {
	                	JSONObject IntentObjDetail = (JSONObject)objDetailArray.get(j);
	                	IntentObjDetailMap = new CommandMap();
	                	IntentObjDetailMap.put("INTENT_SEQ", iIntentNo);
	                	IntentObjDetailMap.put("OBJECT_SEQ", IntentObjMap.get("SEQ_NO"));
	                	IntentObjDetailMap.put("OBJECT_TYPE", IntentObjDetail.get("TYPE"));
	                	IntentObjDetailMap.put("OBJECT_VALUE", IntentObjDetail.get("OBJECT_VALUE"));
	                	
	                	viewDAO.insertIntentObjectDetail(IntentObjDetailMap.getMap());
	                }
                }
            }								
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}           
	}
		
	@Override
	public void deleteIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntent(map);	
	}
	
	@Override
	public void deleteIntentUserSay(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntentUserSay(map);
	}
	
	@Override
	public void deleteIntentUserSayDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntentUserSayDetail(map);
	}
	
	@Override
	public void deleteIntentsUserSays(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntentsUserSays(map);
	}
	
	@Override
	public void deleteIntentsIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntentsIntention(map);
	}
	
	@Override
	public void deleteIntentsObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntentsObject(map);
	}
	
	@Override
	public void deleteIntentsObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteIntentsObjectDetail(map);
	}
	
	@Override
	public void insertAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonItObj;
        JSONObject FillSlotObj;
        CommandMap FillSlotMap;
        CommandMap IntentItMap;
        CommandMap IntentObjMap;
        CommandMap IntentObjDetailMap;
//        log.debug(map);
        int iAgentNo;        
		try {
			if (map.get("SEQ_NO").toString().isEmpty()) {				
				viewDAO.insertAgent(map);
				iAgentNo = (int)map.get("SEQ_NO");
			}
			else {
				iAgentNo = Integer.parseInt(map.get("SEQ_NO").toString());				
				CommandMap ObjDelMap = new CommandMap();				
				viewDAO.updateAgent(map);
				ObjDelMap.put("SEQ_NO", iAgentNo);
				
				viewDAO.deleteAgentFillingSlot(ObjDelMap.getMap());				
				viewDAO.deleteAgentIntention(ObjDelMap.getMap());
				viewDAO.deleteAgentIntentsObject(ObjDelMap.getMap());
				viewDAO.deleteAgentIntentsObjectDetail(ObjDelMap.getMap());
			}
																
			
			jsonItObj = (JSONObject)jsonParser.parse((String)map.get("OBJECT_ITEM"));
			JSONArray ItObjArray = (JSONArray)jsonItObj.get("INTENTION_ITEM");
						
			for(int i=0 ; i< ItObjArray.size() ; i++){
                JSONObject IntentItObj = (JSONObject)ItObjArray.get(i);
                IntentItMap = new CommandMap();
                IntentItMap.put("AGENT_SEQ", iAgentNo);
                IntentItMap.put("FILLING_SEQ", 0);
                IntentItMap.put("UTTR", IntentItObj.get("UTTR"));
                IntentItMap.put("ACT", IntentItObj.get("ACT"));
                IntentItMap.put("INTENTION_TYPE", IntentItObj.get("INTENTION_TYPE"));
                
                viewDAO.insertAgentIntent(IntentItMap.getMap());                     
                                
    			JSONArray objArray = (JSONArray)IntentItObj.get("OBJECT");
                for(int k=0 ; k< objArray.size() ; k++){
                    JSONObject IntentObj = (JSONObject)objArray.get(k);
                    IntentObjMap = new CommandMap();            
                    IntentObjMap.put("AGENT_SEQ", iAgentNo);
                    IntentObjMap.put("INTENTION_SEQ", IntentItMap.get("SEQ_NO"));
                    IntentObjMap.put("OBJECT_VALUE", IntentObj.get("OBJECT_VALUE"));                
                    
                    viewDAO.insertAgentObject(IntentObjMap.getMap());       
                            
	                JSONArray objDetailArray = (JSONArray)IntentObj.get("OBJECT_DETAIL");
	                for(int j=0 ; j< objDetailArray.size() ; j++) {
	                	JSONObject IntentObjDetail = (JSONObject)objDetailArray.get(j);
	                	IntentObjDetailMap = new CommandMap();
	                	IntentObjDetailMap.put("AGENT_SEQ", iAgentNo);
	                	IntentObjDetailMap.put("OBJECT_SEQ", IntentObjMap.get("SEQ_NO"));
	                	IntentObjDetailMap.put("OBJECT_TYPE", IntentObjDetail.get("TYPE"));
	                	IntentObjDetailMap.put("OBJECT_VALUE", IntentObjDetail.get("OBJECT_VALUE"));
	                	
	                	viewDAO.insertAgentObjectDetail(IntentObjDetailMap.getMap());
	                }
                }
            }	
			
			JSONArray jsonSlotFillObj = (JSONArray)jsonItObj.get("SLOT_FILLING_ITEM");
			
			ItObjArray.clear();
			
			for (int s = 0; s < jsonSlotFillObj.size(); s++) {
				FillSlotObj = (JSONObject)jsonSlotFillObj.get(s);
				FillSlotMap = new CommandMap();
				FillSlotMap.put("AGENT_SEQ", iAgentNo);
				FillSlotMap.put("SLOT_SEQ", FillSlotObj.get("SLOT_SEQ"));
				FillSlotMap.put("SLOT_NAME", FillSlotObj.get("SLOT_NAME"));
				FillSlotMap.put("CONDITION", FillSlotObj.get("CONDITION"));
				FillSlotMap.put("PROGRESS_SLOT", FillSlotObj.get("PROGRESS_SLOT"));
				viewDAO.insertAgentFillingSlot(FillSlotMap.getMap());
				
				ItObjArray = (JSONArray)FillSlotObj.get("INTENTION_ITEM");
				for(int i=0 ; i< ItObjArray.size() ; i++){
	                JSONObject IntentItObj = (JSONObject)ItObjArray.get(i);
	                
	                IntentItMap = new CommandMap();
	                IntentItMap.put("AGENT_SEQ", iAgentNo);
	                IntentItMap.put("FILLING_SEQ", FillSlotMap.get("SEQ_NO"));
	                IntentItMap.put("UTTR", IntentItObj.get("UTTR"));
	                IntentItMap.put("ACT", IntentItObj.get("ACT"));	        
	                IntentItMap.put("INTENTION_TYPE", IntentItObj.get("INTENTION_TYPE"));
	                
	                viewDAO.insertAgentIntent(IntentItMap.getMap());                     
	                                
	                JSONArray objArray = (JSONArray)IntentItObj.get("OBJECT");
	                for(int k=0 ; k< objArray.size() ; k++){
	                    JSONObject IntentObj = (JSONObject)objArray.get(k);
	                    IntentObjMap = new CommandMap();    
	                    IntentObjMap.put("AGENT_SEQ", iAgentNo);
	                    IntentObjMap.put("INTENTION_SEQ", IntentItMap.get("SEQ_NO"));
	                    IntentObjMap.put("OBJECT_VALUE", IntentObj.get("OBJECT_VALUE"));                	           
	                    
	                    viewDAO.insertAgentObject(IntentObjMap.getMap());       
	                                
		                JSONArray objDetailArray = (JSONArray)IntentObj.get("OBJECT_DETAIL");
		                for(int j=0 ; j< objDetailArray.size() ; j++) {
		                	JSONObject IntentObjDetail = (JSONObject)objDetailArray.get(j);
		                	IntentObjDetailMap = new CommandMap();
		                	IntentObjDetailMap.put("AGENT_SEQ", iAgentNo);
		                	IntentObjDetailMap.put("OBJECT_SEQ", IntentObjMap.get("SEQ_NO"));
		                	IntentObjDetailMap.put("OBJECT_TYPE", IntentObjDetail.get("TYPE"));
		                	IntentObjDetailMap.put("OBJECT_VALUE", IntentObjDetail.get("OBJECT_VALUE"));
		                	
		                	viewDAO.insertAgentObjectDetail(IntentObjDetailMap.getMap());
		                }
	                }
	            }
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void insertOnlyAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.insertAgent(map);
	}
	
	@Override
	public void updateAgentPosition(Map<String, Object> map) {
		// TODO Auto-generated method stub
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObj;
        CommandMap AgentMap;
        CommandMap ConnetcionMap;
                
		try {										
			jsonObj = (JSONObject)jsonParser.parse((String)map.get("OBJECT"));
			JSONArray AgentArray = (JSONArray)jsonObj.get("Agent");
			JSONArray ConnectionArray = (JSONArray)jsonObj.get("Connection");
			
			for(int i=0 ; i< AgentArray.size() ; i++){
				
				JSONObject AgentObj = (JSONObject)AgentArray.get(i);
				AgentMap = new CommandMap();
				AgentMap.put("SEQ_NO", AgentObj.get("Seq"));
				AgentMap.put("LEFT_POS", AgentObj.get("Left"));
				AgentMap.put("TOP_POS", AgentObj.get("Top"));				
				
				viewDAO.updateAgentPosition(AgentMap.getMap());
			}
			
			viewDAO.deleteAgentGraph(map);
			
			for(int i=0 ; i< ConnectionArray.size() ; i++){
				
				JSONObject ConnectionObj = (JSONObject)ConnectionArray.get(i);
				ConnetcionMap = new CommandMap();
				ConnetcionMap.put("SOURCE_ID", ConnectionObj.get("SourceId"));
				ConnetcionMap.put("TARGET_ID", ConnectionObj.get("TargetId"));
				ConnetcionMap.put("CONDITION", ConnectionObj.get("Condition"));				
				ConnetcionMap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
				viewDAO.insertAgentGraph(ConnetcionMap.getMap());
			}
			
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void insertAgentGraph(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteAgentGraph3(map);
		viewDAO.insertAgentGraph(map);								
	}
	
	@Override
	public void deleteAgentGraph(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteAgentGraph3(map);
	}
	
	@Override
	public void deleteAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		map.put("PROJECT_SEQ", map.get("SEQ_NO"));
		List<Map<String, Object>> clist = viewDAO.selectSlotClass3(map);
		for (Iterator iterator = clist.iterator(); iterator.hasNext();) {
			Map<String, Object> citem = (Map<String, Object>) iterator.next();
			
			DropTable(citem);
		}
		
		viewDAO.deleteAgent(map);
		/*viewDAO.deleteAgentFillingSlot(map);
		viewDAO.deleteAgentIntention(map);
		viewDAO.deleteAgentIntentsObject(map);
		viewDAO.deleteAgentIntentsObjectDetail(map);
		viewDAO.deleteAgentGraph2(map);*/
	}
	
	@Override
	public void insertUser(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		viewDAO.insertUser(map);
	}
	
	@Override
	public void updateUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateUser(map);
	}
	
	@Override
	public void deleteUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteUser(map);
	}
	
	@Override
	public void insertLua(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		viewDAO.insertLua(map);
	}
	
	@Override
	public void updateLua(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.updateLua(map);
	}
	
	@Override
	public void deleteLua(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteLua(map);
	}
	
	@Override
	public void insertUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.insertUserDomain(map);
	}
	
	@Override
	public void deleteUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		viewDAO.deleteUserDomain(map);
	}
	
	@Override
	public List<Map<String, Object>> selectProject(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		List<Map<String, Object>> rtnList = new ArrayList<Map<String, Object>>();
		if(map.get("USER_TYPE") != null) {
			if (!map.get("USER_TYPE").toString().equals("SA")) {
				rtnList = viewDAO.SelectProjectGu(map);
			}
			else {
				rtnList =  viewDAO.SelectProjectSa(map);
			}
		}
		return rtnList;

	}
	
	@Override
	public List<Map<String, Object>> SelectProjectName(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.SelectProjectName(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotClass(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotClass(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlot(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlot(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlot2(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlot2(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotIntents(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotIntents(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotDefine(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotDefine(map);
	}
	
	@Override
	public List<Map<String, Object>> selectFunction(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectFunction(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotType(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotType(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotObject(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotObject(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotObjectDetail(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotObjectDetail(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotObjectDetailExcel(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotObjectDetailExcel(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotInstances(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotInstances(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotInstances2(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectSlotInstances2(map);
	}
	
	@Override
	public List<Map<String, Object>> selectSlotTypeClass(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		
		List<Map<String, Object>> list = viewDAO.selectSlotTypeClass(map);
		List<Map<String, Object>> returnlist = new ArrayList<Map<String, Object>>();
		GetParentCalss(list, returnlist, map.get("CLASS_SEQ_NO").toString());
		return returnlist;
	}
	
	private void GetParentCalss(List<Map<String, Object>> list, List<Map<String, Object>> returnlist, String ClassSeq) {
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
			
			if (item.get("TYPE_SEQ_NO").toString().equals(ClassSeq)) {
				Map<String, Object> addmap = new HashMap<String, Object>();
				addmap.put("SLOT_SEQ_NO", item.get("SLOT_SEQ_NO"));
				addmap.put("CLASS_SEQ_NO", item.get("CLASS_SEQ_NO"));
				returnlist.add(addmap);
				GetParentCalss(list, returnlist, item.get("CLASS_SEQ_NO").toString());
			}
		}
	}

	@Override
	public List<Map<String, Object>> selectVSlotObject(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectVSlotObject(map);
	}
	
	@Override
	public List<Map<String, Object>> selectIntents(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntents(map);
	}
	
	@Override
	public List<Map<String, Object>> selectIntentsUserSays(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntentsUserSays(map);
	}
	
	@Override
	public List<Map<String, Object>> selectIntentsUserSays2(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntentsUserSays2(map);
	}
	
	@Override
	public List<Map<String, Object>> selectIntentIntention(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntentsIntention(map);
	}
	
	@Override
	public List<Map<String, Object>> selectIntentsObejct(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntentsObejct(map);
	}
	
	@Override
	public List<Map<String, Object>> selectIntentsObjectDtl(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntentsObjectDtl(map);
	}
	
	@Override
	public List<Map<String, Object>> selectFliingSlot(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectFillingSlot(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgents(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgents(map);
	}
	
	@Override
	public Map<String, Object> selectAgent(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgent(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentSlotFilling(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentSlotFilling(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentIntention(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentIntention(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentObject(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentObject(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentObjectdtl(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentObjectdtl(map);
	}
	
	@Override
	public List<Map<String, Object>> selectAgentGraph(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectAgentGraph(map);
	}
	
	@Override
	public List<Map<String, Object>> selectUserIdChk(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectUserIdChk(map);
	}
	
	@Override
	public List<Map<String, Object>> selectUserList(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectUserList(map);
	}
	
	@Override
	public List<Map<String, Object>> selectLuaList(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectLuaList(map);
	}
	
	@Override
	public Map<String, Object> selectLua(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectLua(map);
	}
	
	@Override
	public Map<String, Object> selectUser(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectUser(map);
	}
	
	@Override
	public Map<String, Object> loginUser(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.loginUser(map);
	}
	
	@Override
	public Map<String, Object> selectIntent(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectIntent(map);
	}
	
	@Override
	public List<Map<String, Object>> selectUserDomain(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectUserDomain(map);
	}
	
	@Override
	public List<Map<String, Object>> selectUserDomainList(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return viewDAO.selectUserDomainList(map);
	}
	
	@Override
	public Boolean DialogsLearning(Map<String, Object> map, HttpServletRequest request) throws Exception {		
		//String Path = "E:\\dev\\file\\webtool\\www_data\\dialog_domain\\" + map.get("PROJECT_NAME").toString() + "\\" + map.get("PROJECT_NAME").toString();
		//String Cmdpath = Path;
//		String Path = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/" + map.get("PROJECT_NAME").toString() + "/" + map.get("PROJECT_NAME").toString();
//		String Cmdpath = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/" + map.get("PROJECT_NAME").toString();
		String Path = enginePath + "/dialog_domain/" + map.get("PROJECT_NAME").toString() + "/" + map.get("PROJECT_NAME").toString();
		String Cmdpath = enginePath + "/dialog_domain/" + map.get("PROJECT_NAME").toString();
		
		LeanTask(map, Path);
		LeanSlot(map, Path);
 		LeanDAType(map, Path);
 		LeanDialogLib(map, Path);
		LeanDomain(map, Path);
		CreateSql(map, Path, request);
		return true;
	}
	
	public void LeanTask(Map<String, Object> map, String Path) throws Exception {		
		List<Map<String, Object>> tasklist = selectAgents(map);
		List<Map<String, Object>> fliinglist;
		List<Map<String, Object>> relatedslotlist;
		List<Map<String, Object>> nexttasklist;
		CommandMap taskmap = new CommandMap();
		
		StringBuilder sb = new StringBuilder();			
		
		int iCnt = 1;
			
		for (Iterator iterator = tasklist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
			if (!item.get("AGENT_NAME").toString().equals("END")) {
				sb.append("<task>\n");	
				
				sb.append("<task_name>\n");
				sb.append(item.get("AGENT_NAME").toString() + "\n");
				sb.append("</task_name>\n");
				
				String Condition = "";
				taskmap.put("AGENT_SEQ", item.get("SEQ_NO"));
				nexttasklist = viewDAO.selectLeanNextAgent(taskmap.getMap());
				for (Iterator iterator2 = nexttasklist.iterator(); iterator2.hasNext();) {
					Map<String, Object> nextitem = (Map<String, Object>) iterator2.next();
					
					if (nexttasklist.size() == 1) {
						Condition = nextitem.get("_CONDITION").toString();
					}
					else {
						if (Condition.isEmpty()) {
							Condition = String.format("(%s)", nextitem.get("_CONDITION").toString());
						}
						else {
							Condition +=  String.format(" or (%s)", nextitem.get("_CONDITION").toString());
						}
					}
				}
				
				sb.append("<task_goal>\n");
				sb.append(Condition + "\n");
				sb.append("</task_goal>\n");
				
				sb.append("<task_type>\n");
				sb.append(item.get("TASK_TYPE").toString() + "\n");
				sb.append("</task_type>\n");
				
				sb.append("<fill_slot>\n");							
				
				fliinglist = selectAgentSlotFilling(taskmap.getMap());
				
				for (Iterator iterator2 = fliinglist.iterator(); iterator2.hasNext();) {
					Map<String, Object> fillingitem = (Map<String, Object>) iterator2.next();
					sb.append("<slot>\n");
					sb.append(fillingitem.get("SLOT_NAME").toString() + ", 1, progress\n");
					sb.append("</slot>\n");
				}
				sb.append("</fill_slot>\n");
				
				sb.append("<related_slot>\n");
				/*relatedslotlist = viewDAO.selectLeanTaskSlot(taskmap.getMap());
				
				if (relatedslotlist.size() > 0) {					
					for (Iterator ireslot = relatedslotlist.iterator(); ireslot.hasNext();) {
						Map<String, Object> slotitem = (Map<String, Object>) ireslot.next();
					
						String[] slots = slotitem.get("SLOT_NAME").toString().split(",");
						
						for (int i = 0; i < slots.length; i++) {
							sb.append(slots[i].replaceAll("=value", "") + "\n");
						}
					}					
				}
				else {
					sb.append("\n");
				}*/
				
				if (!item.containsKey("RELATED_SLOTS")) {
					sb.append("\n");
				}
				else if (item.get("RELATED_SLOTS").toString().isEmpty()) {
					sb.append("\n");
				}
				else {
					String[] slots = item.get("RELATED_SLOTS").toString().split(",");
					
					for (int i = 0; i < slots.length; i++) {
						sb.append(slots[i].trim() + "\n");
					}
				}
				sb.append("</related_slot>\n");
	
				sb.append("<next_task>\n");
				
				for (Iterator iterator2 = nexttasklist.iterator(); iterator2.hasNext();) {
					Map<String, Object> nextitem = (Map<String, Object>) iterator2.next();				
					sb.append("<next_task_item>\n");
									    
				    sb.append("<task_name>\n");
					sb.append(nextitem.get("AGENT_NAME").toString() + "\n");
					sb.append("</task_name>\n");
				    				    
				    sb.append("<condition>\n");
					sb.append(nextitem.get("_CONDITION").toString() + "\n");
					sb.append("</condition>\n");
				    
				    sb.append("<controller>\n");
					sb.append("system\n");
					sb.append("</controller>\n");
				   
				    sb.append("</next_task_item>\n");
				}
				sb.append("</next_task>\n");
				
				sb.append("<reset_slot>\n");
				if (item.get("RESET_SLOT").toString().equals("0")) {
					sb.append("no\n");
				}
				else {
					sb.append("yes\n");
				}
				
				sb.append("</reset_slot>\n");								
				sb.append("</task>\n\n");
				iCnt++;
			}
		}		

		// 4. ���Ͽ� ���
		//FileWriter writer = new FileWriter(Path + ".task.txt");
		FileWriterWithEncoding writer = new FileWriterWithEncoding(Path + ".task.txt", "EUC-KR");		
		writer.write(sb.toString());
		writer.close();
	}
	
	@SuppressWarnings("unused")
	public void LeanSlot(Map<String, Object> map, String Path) throws Exception {
		List<Map<String, Object>> classlist = selectSlotClass(map);
		List<Map<String, Object>> slotlist = selectSlot(map);
		
		JSONObject slotobj = SetSlotListToJson(slotlist);
		
		StringBuilder sb = new StringBuilder();
		StringBuilder sbslot = new StringBuilder();
		
		for (Iterator iterator = classlist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
				
			sb.append("<class>\n");
			
			sb.append("<name>\n");
			sb.append(item.get("CLASS_NAME").toString() + "\n");
			sb.append("</name>\n");
		
			sb.append("<source>\n");
			sb.append("user\n");
			sb.append("</source>\n");

			sb.append("<slots>\n");						
			AddSlot(sb, sbslot, slotobj, item.get("SEQ_NO").toString(), map.get("PROJECT_SEQ").toString(), item.get("CLASS_NAME").toString() + ".", 1, item.get("CLASS_NAME").toString() + ".");			
			sb.append("</slots>\n");

			sb.append("<description>\n");
			sb.append("<korean>" + item.get("DESCRIPTION") + "</korean>\n");
			sb.append("</description>\n");
			
			sb.append("<key>\n");
			sb.append("\n");
			sb.append("</key>\n");
			
			sb.append("</class>\n\n");
		}
		
		sb.append(sbslot.toString());	
		
		//FileWriter writer = new FileWriter(Path + ".slot.txt");
		FileWriterWithEncoding writer = new FileWriterWithEncoding(Path + ".slot.txt", "EUC-KR");
		writer.write(sb.toString());
		writer.close();
	}
	
	@SuppressWarnings("unused")
	public void LeanDAType(Map<String, Object> map, String Path) throws Exception {
		List<Map<String, Object>> intentlist = selectIntents(map);
		
		StringBuilder sb = new StringBuilder();
		List<String> arrNotDatype = new ArrayList<String>();
		arrNotDatype.add("inform");
		arrNotDatype.add("request");
		arrNotDatype.add("reqalts");
		arrNotDatype.add("reqmore");
		arrNotDatype.add("search");
		arrNotDatype.add("confirm");
		arrNotDatype.add("confreq");
		arrNotDatype.add("select");
		arrNotDatype.add("affirm");
		arrNotDatype.add("negate");
		arrNotDatype.add("hello");
		arrNotDatype.add("greet");
		arrNotDatype.add("bye");
		arrNotDatype.add("command");
		arrNotDatype.add("silence");
		arrNotDatype.add("hangup");
		arrNotDatype.add("repeat");
		arrNotDatype.add("help");
		arrNotDatype.add("restart");
		arrNotDatype.add("unknown");
		arrNotDatype.add("misunderstanding");
		arrNotDatype.add("thankyou");
		arrNotDatype.add("welcome");
		arrNotDatype.add("ack");
		arrNotDatype.add("hereis");
		arrNotDatype.add("complain_misunderstanding");
		arrNotDatype.add("non_response");
		String IntentName = "";
		List<String> arrDatype = new ArrayList<String>();
				
		for (Iterator iterator = intentlist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
			
			if (item.get("INTENT_NAME").toString().indexOf("(") > -1) {
				IntentName = item.get("INTENT_NAME").toString().substring(0, item.get("INTENT_NAME").toString().indexOf("("));
			}
			else {
				IntentName = item.get("INTENT_NAME").toString();	
			}
			
			if (!arrNotDatype.contains(IntentName) && !arrDatype.contains(IntentName)) {				
				arrDatype.add(IntentName);
				sb.append("<define>\n");
				
				sb.append("<DA_class>\n");
				sb.append("default\n");
				sb.append("</DA_class>\n");
			
				sb.append("<DA_type>\n");
				sb.append(IntentName + "\n");
				sb.append("</DA_type>\n");
				
				sb.append("</define>\n\n");
			}	
		}
		intentlist.clear();
		
		intentlist = viewDAO.selectLeanDatype(map); 
		for (Iterator iterator = intentlist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
			if (item.get("INTENT_NAME").toString().indexOf("(") > -1) {
				IntentName = item.get("INTENT_NAME").toString().substring(0, item.get("INTENT_NAME").toString().indexOf("("));
			}
			else {
				IntentName = item.get("INTENT_NAME").toString();	
			}
			
			if (!arrNotDatype.contains(IntentName) && !arrDatype.contains(IntentName)) {
				arrDatype.add(IntentName);
				sb.append("<define>\n");
				
				sb.append("<DA_class>\n");
				sb.append("default\n");
				sb.append("</DA_class>\n");
			
				sb.append("<DA_type>\n");
				sb.append(IntentName + "\n");
				sb.append("</DA_type>\n");
				
				sb.append("</define>\n\n");
			}
		}
		
		//FileWriter writer = new FileWriter(Path + ".DAtype.txt");
		FileWriterWithEncoding writer = new FileWriterWithEncoding(Path + ".DAtype.txt", "EUC-KR");
		writer.write(sb.toString());
		writer.close();
	}
	
	@SuppressWarnings("unused")
	public void LeanDialogLib(Map<String, Object> map, String Path) throws Exception {
		List<Map<String, Object>> tasklist = selectAgents(map);
		
		StringBuilder sb = new StringBuilder();
		StringBuilder sbsub = new StringBuilder();
		
		//transaction
		for (Iterator iterator = tasklist.iterator(); iterator.hasNext();) {
			Map<String, Object> taskitem = (Map<String, Object>) iterator.next();
			//start
			sbsub.append("<dialog_node>\n");
			
			sbsub.append("<talker>\n");
			sbsub.append("system\n");
			sbsub.append("</talker>\n");
			
			sbsub.append("<dialog_type>\n");
			sbsub.append("transaction\n");
			sbsub.append("</dialog_type>\n");
			
			sbsub.append("<task>\n");
			sbsub.append(taskitem.get("AGENT_NAME").toString() + "\n");
			sbsub.append("</task>\n");
			
			sbsub.append("<subtype>\n");
			sbsub.append("start\n");
			sbsub.append("</subtype>\n");
			
			sbsub.append("<related_task>\n");
			sbsub.append("\n");
			sbsub.append("</related_task>\n");
			
			sbsub.append("<progress_slot>\n");
			sbsub.append("\n");
			sbsub.append("</progress_slot>\n");
			
			sbsub.append("<condition>\n");
			sbsub.append("\n");
			sbsub.append("</condition>\n");
								
			sbsub.append("<request_utterance>\n");
			sbsub.append("\n");
			sbsub.append("</request_utterance>\n");			
			
			map.put("AGENT_SEQ", taskitem.get("SEQ_NO"));
			List<Map<String, Object>> t1list = viewDAO.selectLeanDialogLib1_1(map);

			sbsub.append("<utterance_set>\n");
			for (Iterator iterator2 = t1list.iterator(); iterator2.hasNext();) {
				Map<String, Object> titem = (Map<String, Object>) iterator2.next();
				sbsub.append("<utterance>\n");
				
				sbsub.append("<condition>\n");
				sbsub.append(titem.get("UTTR").toString() + "\n");
				sbsub.append("</condition>\n");
				
				sbsub.append("<template>\n");
				
				sbsub.append("<weight>\n");
				sbsub.append(" 1.0 \n");
				sbsub.append("</weight>\n");
				
				map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
				List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib1Obj(map);
				for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
					Map<String, Object> objitem = (Map<String, Object>) iterator21.next();										
					map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
																							
					if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
						sbsub.append("<intention>\n");					
				
						sbsub.append("<DA>\n");							
						
						List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib1ObjDtl(map);
						
						for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
							if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
								sbsub.append("request(" + objdtlitem.get("OBJECT_VALUE").toString() + ")\n");
								sbsub.append("</DA>\n");
							}
							else {
								sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
							}
						}
						sbsub.append("</intention>\n");						
					}
					else {
						sbsub.append("<intention>\n");					
						
						sbsub.append("<DA>\n");
						sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
						sbsub.append("</DA>\n");
						
						List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib1ObjDtl(map);
						
						for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
							sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");						
						}
						sbsub.append("</intention>\n");	
					}																			
				}
				
				sbsub.append("</template>\n");
				if (!titem.get("ACT").toString().isEmpty()) 
					sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
				else
					sbsub.append("<action></action>\n");
				sbsub.append("</utterance>\n");
			}		
			sbsub.append("</utterance_set>\n");											
			
			sbsub.append("</dialog_node>\n\n");
			if (t1list.size() > 0) {
				sb.append(sbsub.toString());
			}
			sbsub.setLength(0);
			
			//restart
			sbsub.append("<dialog_node>\n");
			
			sbsub.append("<talker>\n");
			sbsub.append("system\n");
			sbsub.append("</talker>\n");

			sbsub.append("<dialog_type>\n");
			sbsub.append("transaction\n");
			sbsub.append("</dialog_type>\n");
			
			sbsub.append("<task>\n");
			sbsub.append(taskitem.get("AGENT_NAME").toString() + "\n");
			sbsub.append("</task>\n\n");
			
			sbsub.append("<subtype>\n");
			sbsub.append("restart\n");
			sbsub.append("</subtype>\n");
			
			sbsub.append("<related_task>\n");
			sbsub.append("\n");
			sbsub.append("</related_task>\n");
			
			sbsub.append("<progress_slot>\n");
			sbsub.append("\n");
			sbsub.append("</progress_slot>\n");
			
			sbsub.append("<condition>\n");
			sbsub.append("\n");
			sbsub.append("</condition>\n");
						
			sbsub.append("<request_utterance>\n");
			sbsub.append("\n");
			sbsub.append("</request_utterance>\n");
			
			map.put("AGENT_SEQ", taskitem.get("SEQ_NO"));
			List<Map<String, Object>> t2list = viewDAO.selectLeanDialogLib1_2(map);

			sbsub.append("<utterance_set>\n");
			for (Iterator iterator2 = t2list.iterator(); iterator2.hasNext();) {
				Map<String, Object> titem = (Map<String, Object>) iterator2.next();
				sbsub.append("<utterance>\n");
				
				sbsub.append("<condition>\n");
				sbsub.append(titem.get("UTTR").toString() + "\n");
				sbsub.append("</condition>\n");
				
				sbsub.append("<template>\n");
				
				sbsub.append("<weight>\n");
				sbsub.append(" 1.0 \n");
				sbsub.append("</weight>\n");
				
				map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
				List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib1Obj(map);
				for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
					Map<String, Object> objitem = (Map<String, Object>) iterator21.next();
					map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
					
					if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
						sbsub.append("<intention>\n");					
				
						sbsub.append("<DA>\n");							
						
						List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib1ObjDtl(map);
						
						for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
							if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
								sbsub.append("request(" + objdtlitem.get("OBJECT_VALUE").toString() + ")\n");
								sbsub.append("</DA>\n");
							}
							else {
								sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
							}
						}
						sbsub.append("</intention>\n");						
					}
					else {
						sbsub.append("<intention>\n");					
						
						sbsub.append("<DA>\n");
						sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
						sbsub.append("</DA>\n");
						
						List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib1ObjDtl(map);
						
						for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
							sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");						
						}
						sbsub.append("</intention>\n");	
					}												
				}
				
				sbsub.append("</template>\n");
				if (!titem.get("ACT").toString().isEmpty())
					sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
				else
					sbsub.append("<action></action>\n");
				sbsub.append("</utterance>\n");
			}		
			sbsub.append("</utterance_set>\n");
					
			sbsub.append("</dialog_node>\n\n");
			if (t2list.size() > 0) {
				sb.append(sbsub.toString());
			}
			sbsub.setLength(0);
		}
		
		//response
		for (Iterator iterator = tasklist.iterator(); iterator.hasNext();) {
			Map<String, Object> taskitem = (Map<String, Object>) iterator.next();
			
			map.put("AGENT_SEQ", taskitem.get("SEQ_NO"));
			List<Map<String, Object>> rmainllist = viewDAO.selectLeanDialogLib2Main(map);
			
			for (Iterator iterator2 = rmainllist.iterator(); iterator2.hasNext();) {
				Map<String, Object> rmainitem = (Map<String, Object>) iterator2.next();
				map.put("INTENT_SEQ", rmainitem.get("SEQ_NO"));
				List<Map<String, Object>> usersaylist = viewDAO.selectLeanDialogLib2UserSay(map);
				
				//DAtype slot�� �ִ°��
				for (Iterator usiterator = usersaylist.iterator(); usiterator.hasNext();) {
					Map<String, Object> usersayitem = (Map<String, Object>) usiterator.next();
					
					sbsub.append("<dialog_node>\n");
					
					sbsub.append("<talker>\n");
					sbsub.append("system\n");
					sbsub.append("</talker>\n");
				
					sbsub.append("<dialog_type>\n");
					sbsub.append("response\n");
					sbsub.append("</dialog_type>\n");
					
					sbsub.append("<task>\n");
					sbsub.append("\n");
					sbsub.append("</task>\n");
					
					sbsub.append("<subtype>\n");
					sbsub.append("\n");
					sbsub.append("</subtype>\n");
					
					sbsub.append("<related_task>\n");
					sbsub.append(taskitem.get("AGENT_NAME").toString() + "\n");
					sbsub.append("</related_task>\n");
					
					sbsub.append("<progress_slot>\n");
					sbsub.append("\n");
					sbsub.append("</progress_slot>\n");
					
					sbsub.append("<condition>\n");
					sbsub.append("\n");
					sbsub.append("</condition>\n");										
					
					sbsub.append("<request_utterance>\n");
					sbsub.append("<talker> user </talker>\n");
					sbsub.append("<condition> 1 </condition>\n");
					
					if (rmainitem.get("INTENT_NAME").toString().indexOf("(") > -1) {									
						if (rmainitem.get("INTENT_NAME").toString().indexOf("(") + 1 == rmainitem.get("INTENT_NAME").toString().indexOf(")")) {
							String intentName = rmainitem.get("INTENT_NAME").toString().substring(0, rmainitem.get("INTENT_NAME").toString().indexOf("("));
							sbsub.append(String.format("<DA> %s(%s) </DA>\n", intentName, usersayitem.get("SLOT_NAME").toString()));
						}
						else {												
							if (usersayitem.get("SLOT_NAME").toString().isEmpty()) {							
								sbsub.append(String.format("<DA> %s </DA>\n", rmainitem.get("INTENT_NAME").toString()));
							}
							else {
								String intentName = rmainitem.get("INTENT_NAME").toString().substring(0, rmainitem.get("INTENT_NAME").toString().indexOf("("));
								String tempMapda = rmainitem.get("INTENT_NAME").toString().substring(rmainitem.get("INTENT_NAME").toString().indexOf("(") + 1, rmainitem.get("INTENT_NAME").toString().indexOf(")"));
								sbsub.append(String.format("<DA> %s(%s, %s) </DA>\n", intentName, tempMapda, usersayitem.get("SLOT_NAME").toString()));
							}
						}
					}
					else {
						sbsub.append(String.format("<DA> %s(%s) </DA>\n", rmainitem.get("INTENT_NAME").toString(), usersayitem.get("SLOT_NAME").toString()));
					}
					
					
					//sbsub.append("<DA> " + rmainitem.get("INTENT_NAME").toString() + "(" + usersayitem.get("SLOT_NAME").toString() + ") </DA>\n");				 
					sbsub.append("</request_utterance>\n");
													
					map.put("SLOT_SEQ_NO", usersayitem.get("SLOT_SEQ_NO"));
					List<Map<String, Object>> rlist = viewDAO.selectLeanDialogLib2_1(map);
													
					sbsub.append("<utterance_set>\n");
					for (Iterator riterator = rlist.iterator(); riterator.hasNext();) {
						Map<String, Object> titem = (Map<String, Object>) riterator.next();
																	
						sbsub.append("<utterance>\n");
						
						sbsub.append("<condition>\n");
						sbsub.append(titem.get("UTTR").toString() + "\n");
						sbsub.append("</condition>\n");
						
						sbsub.append("<template>\n");
						
						sbsub.append("<weight>\n");
						sbsub.append(" 1.0 \n");
						sbsub.append("</weight>\n");
						
						map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
						List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib2Obj(map);
						for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
							Map<String, Object> objitem = (Map<String, Object>) iterator21.next();
							map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
							
							if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
								sbsub.append("<intention>\n");					
						
								sbsub.append("<DA>\n");							
								
								List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
								
								for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
									Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
									if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
										sbsub.append("request(" + objdtlitem.get("OBJECT_VALUE").toString() + ")\n");
										sbsub.append("</DA>\n");
									}
									else {
										sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
									}
								}
								sbsub.append("</intention>\n");						
							}
							else {
								sbsub.append("<intention>\n");					
								
								sbsub.append("<DA>\n");
								sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
								sbsub.append("</DA>\n");
								
								List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
								
								for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
									Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
									sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");						
								}
								sbsub.append("</intention>\n");	
							}
						}

						sbsub.append("</template>\n");
						if (!titem.get("ACT").toString().isEmpty())
							sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
						else
							sbsub.append("<action></action>\n");
						sbsub.append("</utterance>\n");
					}
					sbsub.append("</utterance_set>\n");
					
					sbsub.append("</dialog_node>\n\n");
					
					if (rlist.size() > 0) {
						sb.append(sbsub.toString());
					}
					sbsub.setLength(0);
				}
				
				//�⺻ DAType
				sbsub.append("<dialog_node>\n");
				
				sbsub.append("<talker>\n");
				sbsub.append("system\n");
				sbsub.append("</talker>\n");						
				
				sbsub.append("<dialog_type>\n");
				sbsub.append("response\n");
				sbsub.append("</dialog_type>\n");
				
				sbsub.append("<task>\n");
				sbsub.append("\n");
				sbsub.append("</task>\n");
				
				sbsub.append("<subtype>\n");
				sbsub.append("\n");
				sbsub.append("</subtype>\n");
				
				sbsub.append("<related_task>\n");
				sbsub.append(taskitem.get("AGENT_NAME").toString() + "\n");
				sbsub.append("</related_task>\n");
				
				sbsub.append("<progress_slot>\n");
				sbsub.append("\n");
				sbsub.append("</progress_slot>\n");
				
				sbsub.append("<condition>\n");
				sbsub.append("\n");
				sbsub.append("</condition>\n");
			
				sbsub.append("<request_utterance>\n");
				sbsub.append("<talker> user </talker>\n");
				sbsub.append("<condition> 1 </condition>\n");
				if (rmainitem.get("INTENT_NAME").toString().indexOf("(") > -1) {									
					sbsub.append(String.format("<DA> %s </DA>\n", rmainitem.get("INTENT_NAME").toString()));
				}
				else {
					sbsub.append(String.format("<DA> %s() </DA>\n", rmainitem.get("INTENT_NAME").toString()));
				}
				
				//sbsub.append("<DA> " + rmainitem.get("INTENT_NAME").toString() + "() </DA>\n");				 							
				sbsub.append("</request_utterance>\n");
				
				List<Map<String, Object>> rlist = viewDAO.selectLeanDialogLib2_2(map);
				
				sbsub.append("<utterance_set>\n");
				for (Iterator riterator = rlist.iterator(); riterator.hasNext();) {
					Map<String, Object> titem = (Map<String, Object>) riterator.next();
					sbsub.append("<utterance>\n");
					
					sbsub.append("<condition>\n");
					sbsub.append(titem.get("UTTR").toString() + "\n");
					sbsub.append("</condition>\n");
					
					sbsub.append("<template>\n");
					
					sbsub.append("<weight>\n");
					sbsub.append(" 1.0 \n");
					sbsub.append("</weight>\n");
					
					map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
					List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib2Obj(map);
					for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
						Map<String, Object> objitem = (Map<String, Object>) iterator21.next();
						map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
						
						if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
							sbsub.append("<intention>\n");					
					
							sbsub.append("<DA>\n");							
							
							List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
							
							for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
								Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
								if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
									sbsub.append("request(" + objdtlitem.get("OBJECT_VALUE").toString() + ")\n");
									sbsub.append("</DA>\n");
								}
								else {
									sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
								}
							}
							sbsub.append("</intention>\n");						
						}
						else {
							sbsub.append("<intention>\n");					
							
							sbsub.append("<DA>\n");
							sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
							sbsub.append("</DA>\n");
							
							List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
							
							for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
								Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
								sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");						
							}
							sbsub.append("</intention>\n");	
						}
					}

					sbsub.append("</template>\n");
					if (!titem.get("ACT").toString().isEmpty())
						sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
					else
						sbsub.append("<action></action>\n");
					sbsub.append("</utterance>\n");	
				}
				sbsub.append("</utterance_set>\n");
								
				sbsub.append("</dialog_node>\n\n");
				if (rlist.size() > 0) {
					sb.append(sbsub.toString());
				}
				sbsub.setLength(0);
			}
		}
		
		//response2
		String taskName = "";
		for (Iterator iterator = tasklist.iterator(); iterator.hasNext();) {
			Map<String, Object> tasknameitem = (Map<String, Object>) iterator.next();
			
			if (taskName.isEmpty()) {
				taskName += tasknameitem.get("AGENT_NAME").toString();
			}
			else {
				taskName += String.format(", %s", tasknameitem.get("AGENT_NAME").toString());
			}
		}
		
		map.put("AGENT_SEQ", 0);
		List<Map<String, Object>> rmainllist = viewDAO.selectLeanDialogLib2Main(map);
		
		for (Iterator iterator2 = rmainllist.iterator(); iterator2.hasNext();) {
			Map<String, Object> rmainitem = (Map<String, Object>) iterator2.next();
			map.put("INTENT_SEQ", rmainitem.get("SEQ_NO"));
			List<Map<String, Object>> usersaylist = viewDAO.selectLeanDialogLib2UserSay(map);
			
			//DAtype slot�� �ִ°��
			for (Iterator usiterator = usersaylist.iterator(); usiterator.hasNext();) {
				Map<String, Object> usersayitem = (Map<String, Object>) usiterator.next();
				
				sbsub.append("<dialog_node>\n");
				
				sbsub.append("<talker>\n");
				sbsub.append("system\n");
				sbsub.append("</talker>\n");
			
				sbsub.append("<dialog_type>\n");
				sbsub.append("response\n");
				sbsub.append("</dialog_type>\n");
				
				sbsub.append("<task>\n");
				sbsub.append("\n");
				sbsub.append("</task>\n");
				
				sbsub.append("<subtype>\n");
				sbsub.append("\n");
				sbsub.append("</subtype>\n");
				
				sbsub.append("<related_task>\n");
				sbsub.append(taskName + "\n");
				sbsub.append("</related_task>\n");
				
				sbsub.append("<progress_slot>\n");
				sbsub.append("\n");
				sbsub.append("</progress_slot>\n");
				
				sbsub.append("<condition>\n");
				sbsub.append("\n");
				sbsub.append("</condition>\n");										
				
				sbsub.append("<request_utterance>\n");
				sbsub.append("<talker> user </talker>\n");
				sbsub.append("<condition> 1 </condition>\n");
				
				if (rmainitem.get("INTENT_NAME").toString().indexOf("(") > -1) {									
					if (rmainitem.get("INTENT_NAME").toString().indexOf("(") + 1 == rmainitem.get("INTENT_NAME").toString().indexOf(")")) {
						String intentName = rmainitem.get("INTENT_NAME").toString().substring(0, rmainitem.get("INTENT_NAME").toString().indexOf("("));
						sbsub.append(String.format("<DA> %s(%s) </DA>\n", intentName, usersayitem.get("SLOT_NAME").toString()));
					}
					else {												
						if (usersayitem.get("SLOT_NAME").toString().isEmpty()) {							
							sbsub.append(String.format("<DA> %s </DA>\n", rmainitem.get("INTENT_NAME").toString()));
						}
						else {
							String intentName = rmainitem.get("INTENT_NAME").toString().substring(0, rmainitem.get("INTENT_NAME").toString().indexOf("("));
							String tempMapda = rmainitem.get("INTENT_NAME").toString().substring(rmainitem.get("INTENT_NAME").toString().indexOf("(") + 1, rmainitem.get("INTENT_NAME").toString().indexOf(")"));
							sbsub.append(String.format("<DA> %s(%s, %s) </DA>\n", intentName, tempMapda, usersayitem.get("SLOT_NAME").toString()));
						}
					}
				}
				else {
					sbsub.append(String.format("<DA> %s(%s) </DA>\n", rmainitem.get("INTENT_NAME").toString(), usersayitem.get("SLOT_NAME").toString()));
				}
				
				
				//sbsub.append("<DA> " + rmainitem.get("INTENT_NAME").toString() + "(" + usersayitem.get("SLOT_NAME").toString() + ") </DA>\n");				 
				sbsub.append("</request_utterance>\n");
												
				map.put("SLOT_SEQ_NO", usersayitem.get("SLOT_SEQ_NO"));
				List<Map<String, Object>> rlist = viewDAO.selectLeanDialogLib2_1(map);
												
				sbsub.append("<utterance_set>\n");
				for (Iterator riterator = rlist.iterator(); riterator.hasNext();) {
					Map<String, Object> titem = (Map<String, Object>) riterator.next();
																
					sbsub.append("<utterance>\n");
					
					sbsub.append("<condition>\n");
					sbsub.append(titem.get("UTTR").toString() + "\n");
					sbsub.append("</condition>\n");
					
					sbsub.append("<template>\n");
					
					sbsub.append("<weight>\n");
					sbsub.append(" 1.0 \n");
					sbsub.append("</weight>\n");
					
					map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
					List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib2Obj(map);
					for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
						Map<String, Object> objitem = (Map<String, Object>) iterator21.next();
						map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
						
						if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
							sbsub.append("<intention>\n");					
					
							sbsub.append("<DA>\n");							
							
							List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
							
							for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
								Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
								if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
									sbsub.append("request(" + objdtlitem.get("OBJECT_VALUE").toString() + ")\n");
									sbsub.append("</DA>\n");
								}
								else {
									sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
								}
							}
							sbsub.append("</intention>\n");						
						}
						else {
							sbsub.append("<intention>\n");					
							
							sbsub.append("<DA>\n");
							sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
							sbsub.append("</DA>\n");
							
							List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
							
							for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
								Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
								sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");						
							}
							sbsub.append("</intention>\n");	
						}
					}

					sbsub.append("</template>\n");
					if (!titem.get("ACT").toString().isEmpty())
						sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
					else
						sbsub.append("<action></action>\n");
					sbsub.append("</utterance>\n");
				}
				sbsub.append("</utterance_set>\n");
				
				sbsub.append("</dialog_node>\n\n");
				
				if (rlist.size() > 0) {
					sb.append(sbsub.toString());
				}
				sbsub.setLength(0);
			}
			
			//�⺻ DAType
			sbsub.append("<dialog_node>\n");
			
			sbsub.append("<talker>\n");
			sbsub.append("system\n");
			sbsub.append("</talker>\n");						
			
			sbsub.append("<dialog_type>\n");
			sbsub.append("response\n");
			sbsub.append("</dialog_type>\n");
			
			sbsub.append("<task>\n");
			sbsub.append("\n");
			sbsub.append("</task>\n");
			
			sbsub.append("<subtype>\n");
			sbsub.append("\n");
			sbsub.append("</subtype>\n");
			
			sbsub.append("<related_task>\n");
			sbsub.append(taskName + "\n");
			sbsub.append("</related_task>\n");
			
			sbsub.append("<progress_slot>\n");
			sbsub.append("\n");
			sbsub.append("</progress_slot>\n");
			
			sbsub.append("<condition>\n");
			sbsub.append("\n");
			sbsub.append("</condition>\n");
		
			sbsub.append("<request_utterance>\n");
			sbsub.append("<talker> user </talker>\n");
			sbsub.append("<condition> 1 </condition>\n");
			if (rmainitem.get("INTENT_NAME").toString().indexOf("(") > -1) {									
				sbsub.append(String.format("<DA> %s </DA>\n", rmainitem.get("INTENT_NAME").toString()));
			}
			else {
				sbsub.append(String.format("<DA> %s() </DA>\n", rmainitem.get("INTENT_NAME").toString()));
			}
			
			//sbsub.append("<DA> " + rmainitem.get("INTENT_NAME").toString() + "() </DA>\n");				 							
			sbsub.append("</request_utterance>\n");
			
			List<Map<String, Object>> rlist = viewDAO.selectLeanDialogLib2_2(map);
			
			sbsub.append("<utterance_set>\n");
			for (Iterator riterator = rlist.iterator(); riterator.hasNext();) {
				Map<String, Object> titem = (Map<String, Object>) riterator.next();
				sbsub.append("<utterance>\n");
				
				sbsub.append("<condition>\n");
				sbsub.append(titem.get("UTTR").toString() + "\n");
				sbsub.append("</condition>\n");
				
				sbsub.append("<template>\n");
				
				sbsub.append("<weight>\n");
				sbsub.append(" 1.0 \n");
				sbsub.append("</weight>\n");
				
				map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
				List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib2Obj(map);
				for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
					Map<String, Object> objitem = (Map<String, Object>) iterator21.next();
					map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
					
					if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
						sbsub.append("<intention>\n");					
				
						sbsub.append("<DA>\n");							
						
						List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
						
						for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
							if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
								sbsub.append("request(" + objdtlitem.get("OBJECT_VALUE").toString() + ")\n");
								sbsub.append("</DA>\n");
							}
							else {
								sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
							}
						}
						sbsub.append("</intention>\n");						
					}
					else {
						sbsub.append("<intention>\n");					
						
						sbsub.append("<DA>\n");
						sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
						sbsub.append("</DA>\n");
						
						List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib2ObjDtl(map);
						
						for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
							Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
							sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");						
						}
						sbsub.append("</intention>\n");	
					}
				}

				sbsub.append("</template>\n");
				if (!titem.get("ACT").toString().isEmpty())
					sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
				else
					sbsub.append("<action></action>\n");
				sbsub.append("</utterance>\n");	
			}
			sbsub.append("</utterance_set>\n");
							
			sbsub.append("</dialog_node>\n\n");
			if (rlist.size() > 0) {
				sb.append(sbsub.toString());
			}
			sbsub.setLength(0);
		}
	
		
		//progress
		for (Iterator iterator = tasklist.iterator(); iterator.hasNext();) {
			Map<String, Object> taskitem = (Map<String, Object>) iterator.next();
			
			map.put("AGENT_SEQ", taskitem.get("SEQ_NO"));
			List<Map<String, Object>> pmainlist = viewDAO.selectLeanDialogLib3Main(map);
			
			for (Iterator pmainiterator = pmainlist.iterator(); pmainiterator.hasNext();) {
				Map<String, Object> pmainitem = (Map<String, Object>) pmainiterator.next();
				
				sbsub.append("<dialog_node>\n");
				
				sbsub.append("<talker>\n");
				sbsub.append("system\n");
				sbsub.append("</talker>\n");				
				
				sbsub.append("<dialog_type>\n");
				sbsub.append("progress\n");
				sbsub.append("</dialog_type>\n");
							
				sbsub.append("<task>\n");
				sbsub.append("\n");
				sbsub.append("</task>\n");
				
				sbsub.append("<subtype>\n");
				sbsub.append("\n");
				sbsub.append("</subtype>\n");
				
				sbsub.append("<related_task>\n");
				sbsub.append(taskitem.get("AGENT_NAME").toString() + "\n");
				sbsub.append("</related_task>\n");
				
				sbsub.append("<progress_slot>\n");
				sbsub.append(pmainitem.get("PROGRESS_SLOT").toString() + "\n");
				sbsub.append("</progress_slot>\n");
				
				sbsub.append("<condition>\n");
				sbsub.append(pmainitem.get("_CONDITION").toString() + "\n");
				sbsub.append("</condition>\n");
				
				sbsub.append("<request_utterance>\n");
				sbsub.append("\n");
				sbsub.append("</request_utterance>\n");
								
				map.put("FILLING_SEQ", pmainitem.get("SEQ_NO"));
				List<Map<String, Object>> t1list = viewDAO.selectLeanDialogLib3(map);

				sbsub.append("<utterance_set>\n");
				for (Iterator iterator2 = t1list.iterator(); iterator2.hasNext();) {
					Map<String, Object> titem = (Map<String, Object>) iterator2.next();
					sbsub.append("<utterance>\n");
					
					sbsub.append("<condition>\n");
					sbsub.append(titem.get("UTTR").toString() + "\n");
					sbsub.append("</condition>\n");
					
					sbsub.append("<template>\n");
					
					sbsub.append("<weight>\n");
					sbsub.append(" 1.0 \n");
					sbsub.append("</weight>\n");
					
					map.put("INTENTION_SEQ", titem.get("SEQ_NO"));
					List<Map<String, Object>> dalist = viewDAO.selectLeanDialogLib1Obj(map);
					for (Iterator iterator21 = dalist.iterator(); iterator21.hasNext();) {
						Map<String, Object> objitem = (Map<String, Object>) iterator21.next();
						map.put("OBJECT_SEQ", objitem.get("SEQ_NO"));
						
						if (objitem.get("OBJECT_VALUE").toString().contains("request")) {
							sbsub.append("<intention>\n");					
					
							sbsub.append("<DA>\n");							
							
							List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib1ObjDtl(map);
							
							for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
								Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
								if (objdtlitem.get("OBJECT_TYPE").toString().equals("R")) {
									sbsub.append("request(" + objdtlitem.get("OBJECT_VALUE").toString() + ")\n");
									sbsub.append("</DA>\n");
								}
								else {
									sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");
								}
							}
							sbsub.append("</intention>\n");						
						}
						else {
							sbsub.append("<intention>\n");					
							
							sbsub.append("<DA>\n");
							sbsub.append(objitem.get("OBJECT_VALUE").toString() + "\n");
							sbsub.append("</DA>\n");
							
							List<Map<String, Object>> patlist = viewDAO.selectLeanDialogLib1ObjDtl(map);
							
							for (Iterator iterator3 = patlist.iterator(); iterator3.hasNext();) {
								Map<String, Object> objdtlitem = (Map<String, Object>) iterator3.next();
								sbsub.append("<pattern>" + objdtlitem.get("OBJECT_VALUE").toString() + "</pattern>\n");						
							}
							sbsub.append("</intention>\n");	
						}																		
					}
					
					sbsub.append("</template>\n");
					if (!titem.get("ACT").toString().isEmpty())
						sbsub.append("<action>" + titem.get("ACT").toString() + "</action>\n");
					else
						sbsub.append("<action></action>\n");
					sbsub.append("</utterance>\n");
				}		
				sbsub.append("</utterance_set>\n");											
				
				sbsub.append("</dialog_node>\n\n");
				if (t1list.size() > 0) {
					sb.append(sbsub.toString());
				}
				sbsub.setLength(0);
			}
		}
		
		//FileWriter writer = new FileWriter(Path + ".dialogLib.txt");
		FileWriterWithEncoding writer = new FileWriterWithEncoding(Path + ".dialogLib.txt", "EUC-KR");
		writer.write(sb.toString());
		writer.close();
	}
	
	public void LeanDomain(Map<String, Object> map, String Path) throws Exception {
		List<Map<String, Object>> usersaylist = viewDAO.selectleanDomain(map);
		
		StringBuilder sb = new StringBuilder();
		
		for (Iterator iterator = usersaylist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
			
			if (item.get("INTENT_NAME").toString().indexOf("(") > -1) {
				if (item.get("INTENT_NAME").toString().indexOf("(") + 1 == item.get("INTENT_NAME").toString().indexOf(")")) {
					String intentName = item.get("INTENT_NAME").toString().substring(0, item.get("INTENT_NAME").toString().indexOf("("));
					sb.append(String.format("%s\t%s\t%s\n", item.get("SAY").toString(), item.get("SLOT_MAP").toString(), String.format("%s(%s)", intentName, item.get("SLOT_MAPDA").toString())));
				}
				else {
					String intentName = item.get("INTENT_NAME").toString().substring(0, item.get("INTENT_NAME").toString().indexOf("("));
					String tempMapda = item.get("INTENT_NAME").toString().substring(item.get("INTENT_NAME").toString().indexOf("(") + 1, item.get("INTENT_NAME").toString().indexOf(")"));
					
					if (item.get("SLOT_MAPDA").toString().isEmpty()) {
						sb.append(String.format("%s\t%s\t%s\n", item.get("SAY").toString(), item.get("SLOT_MAP").toString(), item.get("INTENT_NAME").toString()));
					}
					else {
						sb.append(String.format("%s\t%s\t%s\n", item.get("SAY").toString(), item.get("SLOT_MAP").toString(), String.format("%s(%s, %s)", intentName, tempMapda, item.get("SLOT_MAPDA").toString())));
					}
				}
			}
			else {
				sb.append(String.format("%s\t%s\t%s\n", item.get("SAY").toString(), item.get("SLOT_MAP").toString(), String.format("%s(%s)", item.get("INTENT_NAME").toString(), item.get("SLOT_MAPDA").toString())));	
			}			
		}
		
		//FileWriter writer = new FileWriter(Path + ".DAtype.txt");
		FileWriterWithEncoding writer = new FileWriterWithEncoding(Path + ".txt", "EUC-KR");
		writer.write(sb.toString());
		writer.close();
	}
	
	public void CreateSql(Map<String, Object> map, String Path, HttpServletRequest request) throws Exception {
		StringBuilder sb = new StringBuilder();
		StringBuilder sbticol = new StringBuilder();
		StringBuilder sbti = new StringBuilder();
		StringBuilder sbtiinsertcol = new StringBuilder();
		StringBuilder sbtiinsert = new StringBuilder();
		StringBuilder sbtiinstance = new StringBuilder();
		
		
		sb.append("PRAGMA foreign_keys=OFF;\n");
		sb.append("BEGIN TRANSACTION;\n");
		//sb.append("DROP TABLE scenario;\n");
		sb.append("DROP TABLE canonical_form;\n");
		sb.append("CREATE TABLE canonical_form (_idx TEXT DEFAULT '', _key_from TEXT DEFAULT '', _key_to TEXT DEFAULT '', slot_from TEXT DEFAULT '', slot_to TEXT DEFAULT '', _norm_type TEXT DEFAULT '');\n");		
		
		List<Map<String, Object>> classlist = selectSlotClass(map);
		List<Map<String, Object>> sqlslotlist = viewDAO.selectSqlLiteSlot(map);
		
		List<Map<String, Object>> slotlist = selectSlot(map);
		List<Map<String, Object>> instancelist = null;
		JSONObject sqslotobj = SetSlotListToJson(sqlslotlist);		
		JSONObject slotobj = SetSlotListToJson(slotlist);
		
		idx = 1;
		int iCnt = 1;
		CommandMap timap = new CommandMap();
		for (Iterator iterator = classlist.iterator(); iterator.hasNext();) {
			Map<String, Object> item = (Map<String, Object>) iterator.next();
		
			AddSqlSlot(sb, sqslotobj, item.get("SEQ_NO").toString(), item.get("CLASS_NAME").toString() + ".", "cf");	
			AddSqlSlot(sbticol, slotobj, item.get("SEQ_NO").toString(), item.get("CLASS_NAME").toString() + ".", "ti");
			AddSqlInstanceCol(sbtiinsertcol, slotobj, item.get("SEQ_NO").toString(), item.get("SEQ_NO").toString(), "", "", item.get("CLASS_NAME").toString() + ".", "tc");
			AddSqlInstanceCol(sbti, slotobj, item.get("SEQ_NO").toString(), item.get("SEQ_NO").toString(), "", "", item.get("CLASS_NAME").toString() + ".", "ic");			
			timap.put("SELECT_SQL", sbti.toString());
			timap.put("CLASS_SEQ", item.get("SEQ_NO").toString());
			if (!sbti.toString().isEmpty()) {
				instancelist = viewDAO.selectsqlLiteInstance(timap.getMap());	
					
					
				String[] arrInstanceCol = sbti.toString().split(",");
				
				for (Iterator iinstance = instancelist.iterator(); iinstance.hasNext();) {
					Map<String, Object> instanceitem = (Map<String, Object>) iinstance.next();
					
					for (int i = 0; i < arrInstanceCol.length; i++) {				
						if (i == 0) {
							sbtiinsert.append(String.format("'%s'", instanceitem.get(arrInstanceCol[i].trim())));	
						}
						else {
							sbtiinsert.append(String.format(", '%s'", instanceitem.get(arrInstanceCol[i].trim())));
						}
						
					}
					
					sbtiinstance.append(String.format("INSERT INTO task_information (_idx%s) VALUES ('%s', %s);\n", sbtiinsertcol.toString(), iCnt, sbtiinsert.toString()));
					iCnt++;
					sbtiinsert.setLength(0);				
				}
			}
			sbti.setLength(0);
			sbtiinsertcol.setLength(0);
		}				
		sb.append("DROP TABLE task_information;\n");
		sb.append(String.format("CREATE TABLE task_information (_idx TEXT DEFAULT ''%s);\n", sbticol.toString()));
		sb.append(sbtiinstance.toString());

		sb.append("CREATE INDEX canonical_form_idx ON canonical_form ('_idx','_key_from','_key_to','slot_from','slot_to','_norm_type');\n");
		sb.append("CREATE INDEX task_information_idx ON task_information ('_idx');\n");
		sb.append("COMMIT;");		
		
		FileWriterWithEncoding writer = new FileWriterWithEncoding(Path + ".sql", "UTF-8");
		writer.write(sb.toString());
		writer.close();		
		
		try { 
			//String Cmdpath = String.format("%s/dialog_domain/%s/%s", request.getSession().getServletContext().getInitParameter("enginePath"), map.get("PROJECT_NAME").toString(), map.get("PROJECT_NAME").toString());
			String Cmdpath = String.format("%s/dialog_domain/%s/%s", enginePath, map.get("PROJECT_NAME").toString(), map.get("PROJECT_NAME").toString());
					
			Runtime rt = Runtime.getRuntime(); 
                        
            //Process proc = rt.exec(String.format("chmod -R 777 %s/dialog_domain/%s", request.getSession().getServletContext().getInitParameter("enginePath"), map.get("PROJECT_NAME").toString()));
			Process proc = rt.exec(String.format("chmod -R 777 %s/dialog_domain/%s", enginePath, map.get("PROJECT_NAME").toString()));
            proc.waitFor();
            //proc = rt.exec(String.format("chmod -R 777 %s/dialog_domain/%s/log", request.getSession().getServletContext().getInitParameter("enginePath"), map.get("PROJECT_NAME").toString()));
            proc = rt.exec(String.format("chmod -R 777 %s/dialog_domain/%s/log", enginePath, map.get("PROJECT_NAME").toString()));
            proc.waitFor();
            
            
            proc = Runtime.getRuntime().exec(new String[]{"sh","-c",String.format("sqlite3 %s_DB.sqlite3 < %s.sql", Path, Path)});
            InputStream is = proc.getInputStream(); 
            InputStreamReader isr = new InputStreamReader(is); 
            BufferedReader br = new BufferedReader(isr);
 
            String line;
            while((line=br.readLine())!= null){
//                System.out.println(line);
//                System.out.flush();
            	log.debug("CreateSql/{}", line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }		
	}
		
	@SuppressWarnings("unused")
	private JSONObject SetSlotListToJson(List<Map<String, Object>> slist) throws JsonGenerationException, JsonMappingException, IOException {
		List<Map<String, Object>> templist = new ArrayList<Map<String, Object>>();
    	Map<String, List<Map<String, Object>>> tempmap = new HashMap<String, List<Map<String, Object>>>();
    	
    	String classSeq = "";
    	int iCnt = 0;
    	for (Iterator iterator = slist.iterator(); iterator.hasNext();) {    		
			Map<String, Object> item = (Map<String, Object>) iterator.next();			
			if(iCnt > 0) {
				if (classSeq.equals(item.get("CLASS_SEQ_NO").toString())) {
					templist.add(item);
				}
				else {
					tempmap.put(classSeq, templist);
					classSeq = item.get("CLASS_SEQ_NO").toString();
					templist = new ArrayList<Map<String, Object>>();
					templist.add(item);
				}
			}
			else {
				classSeq = item.get("CLASS_SEQ_NO").toString();
				templist.add(item);
			}
			iCnt++;
		}
    	tempmap.put(classSeq, templist);
    	String jsonData = new ObjectMapper().writeValueAsString(tempmap);
    	
    	JSONParser jsonParser = new JSONParser();
        JSONObject jsonObj = null;
             
		try {
			jsonObj = (JSONObject)jsonParser.parse(jsonData);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	return jsonObj;
	}
	
	@SuppressWarnings("unused")
	private void AddSlot(StringBuilder sb, StringBuilder sbslot, JSONObject jsonobj, String ClassSeq, String ProjectSeq, String SlotName, int Type, String ClassName) {
		JSONArray slotArray = (JSONArray)jsonobj.get(ClassSeq);
		if (slotArray != null) {
			for (int i = 0; i < slotArray.size(); i++) {
				JSONObject slotobj = (JSONObject)slotArray.get(i);
		
				if (slotobj.get("SLOT_TYPE").toString().equals("C")) {	
					AddSlot(sb, sbslot, jsonobj, slotobj.get("TYPE_SEQ").toString(), ProjectSeq, SlotName + slotobj.get("SLOT_NAME").toString() + ".", Type, ClassName + slotobj.get("TYPE_NAME").toString() + ".");
				}
				else {					
					sb.append(SlotName + slotobj.get("SLOT_NAME") + "\n");
					
	
					sbslot.append("<slot>\n");

					sbslot.append("<name>\n");
					sbslot.append(SlotName + slotobj.get("SLOT_NAME") + "\n");
					sbslot.append("</name>\n");
					
					sbslot.append("<source>\n");
					if (slotobj.get("INSTANCE_FLAG").toString().equals("Y"))
						sbslot.append("KB\n");
					else 
						sbslot.append("system\n");					
					sbslot.append("</source>\n");
					
					sbslot.append("<sense>\n");
					sbslot.append(slotobj.get("SENSE_NAME") + "\n");
					sbslot.append("</sense>\n");										
					
					sbslot.append("<preceding_slots>\n");
					sbslot.append(slotobj.get("PRCE_SLOT_NAME") + "\n");
					sbslot.append("</preceding_slots>\n");
					
					sbslot.append("<type>\n");
					if (slotobj.get("SLOT_TYPE").toString().equals("S")) {
						sbslot.append(slotobj.get("TYPE_NAME").toString().substring(4) + "\n");
					}
					else {
						sbslot.append(slotobj.get("TYPE_NAME").toString() + "\n");
					}
					sbslot.append("</type>\n");
					
					sbslot.append("<description>\n");
					sbslot.append(slotobj.get("SLOT_DESCRIPTION") + "\n");
					sbslot.append("</description>\n");
					
					CommandMap defineCmd = new CommandMap();
					defineCmd.put("PROJECT_SEQ", ProjectSeq);
					defineCmd.put("SLOT_SEQ_NO", slotobj.get("SEQ_NO"));
					
					List<Map<String, Object>> definelist = viewDAO.selectSlotDefine(defineCmd.getMap());
					
					sbslot.append("<value_define>\n");
					if (definelist.size() == 0) {
						sbslot.append("\n");
					}
					else {
						for (Iterator defiter = definelist.iterator(); defiter.hasNext();) {
							Map<String, Object> defineitem = (Map<String, Object>) defiter.next();
							sbslot.append("<define>\n");
							sbslot.append(String.format("<condition>%s</condition>\n", defineitem.get("CON")));
							sbslot.append(String.format("<target_slot>%s</target_slot>\n", defineitem.get("TARGET_NAME")));
							sbslot.append(String.format("<action>%s</action>\n", defineitem.get("ACT")));
							sbslot.append("</define>\n");
						}
					}
					sbslot.append("</value_define>\n");
		
					sbslot.append("</slot>\n\n");
				}
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void AddSqlSlot(StringBuilder sb, JSONObject jsonobj, String ClassSeq, String SlotName, String Type) {
		JSONArray slotArray = (JSONArray)jsonobj.get(ClassSeq);		
		if (slotArray != null) {
			for (int i = 0; i < slotArray.size(); i++) {				
				JSONObject slotobj = (JSONObject)slotArray.get(i);
		
				if (slotobj.get("SLOT_TYPE").toString().equals("C")) {	
					AddSqlSlot(sb, jsonobj, slotobj.get("TYPE_SEQ").toString(), SlotName + slotobj.get("SLOT_NAME").toString() + ".", Type);
				}
				else {					
					if (Type == "cf") {
						if (!slotobj.get("OBJECT_DETAIL_NAME").toString().isEmpty()) {							
							sb.append(String.format("INSERT INTO canonical_form (_idx, slot_from, slot_to) VALUES ('%s', '%s=\"%s\"', '%s=\"%s\"');\n", idx, SlotName + slotobj.get("SLOT_NAME").toString(), slotobj.get("OBJECT_DETAIL_NAME"), SlotName + slotobj.get("SLOT_NAME").toString(), slotobj.get("OBJECT_NAME")));
							idx++;
						}
					}
					else {
						sb.append(String.format(", '%s' TEXT DEFAULT ''", SlotName + slotobj.get("SLOT_NAME")));
					}					
				}
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void AddSqlInstanceCol(StringBuilder sb, JSONObject jsonobj, String ClassSeq, String MainClassSeq, String TopSlotSeq, String SlotSeq, String SlotName, String Type) {		
		JSONArray slotArray = (JSONArray)jsonobj.get(ClassSeq);
		if (slotArray != null) {
			for (int i = 0; i < slotArray.size(); i++) {
				JSONObject slotobj = (JSONObject)slotArray.get(i);
				
				if (slotobj.get("SLOT_TYPE").toString().equals("C")) {
					if (TopSlotSeq.isEmpty()){
						TopSlotSeq = slotobj.get("SEQ_NO").toString();
					}
						
					AddSqlInstanceCol(sb, jsonobj, slotobj.get("TYPE_SEQ").toString(), MainClassSeq, TopSlotSeq, slotobj.get("SEQ_NO").toString(), SlotName + slotobj.get("SLOT_NAME").toString() + ".", Type);
				}
				else {			
					if (Type == "tc") {
						sb.append(String.format(", '%s'", SlotName + slotobj.get("SLOT_NAME")));
					}
					else {
						String TempSlotSeq = "";
						
						if (SlotSeq.isEmpty())
							TempSlotSeq = slotobj.get("SEQ_NO").toString();
						else
							TempSlotSeq = TopSlotSeq;
						
						if (sb.toString().isEmpty()) {
							sb.append(String.format("%s_%s_%s", MainClassSeq, TempSlotSeq, slotobj.get("SLOT_NAME")));
						}
						else {
							sb.append(String.format(", %s_%s_%s", MainClassSeq, TempSlotSeq, slotobj.get("SLOT_NAME")));
						}
					}
				}
			}
		}
	}
	
	private void Directorycopy(File sourceF, File targetF){
		
		File[] ff = sourceF.listFiles();
		for (File file : ff) {
			File temp = new File(targetF.getAbsolutePath() + File.separator + file.getName());
			
			if(file.isDirectory()){
				temp.mkdir();
				Directorycopy(file, temp);
			} else {
				FileInputStream fis = null;
				FileOutputStream fos = null;
				try {
					fis = new FileInputStream(file);
					fos = new FileOutputStream(temp) ;
					byte[] b = new byte[4096];
					int cnt = 0;
					while((cnt=fis.read(b)) != -1){
						fos.write(b, 0, cnt);
					}
				} catch (Exception e) {
					log.debug(e.getMessage());
					e.printStackTrace();					
				} finally{
					try {
						fis.close();
						fos.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	private void deleteAllFiles(String path) {
		File file = new File(path);
		
		//������ ������ �迭�� �����´�.
		File[] tempFile = file.listFiles();
		if(tempFile != null && tempFile.length > 0){
			for (int i = 0; i < tempFile.length; i++) {
				if(tempFile[i].isFile()){
					tempFile[i].delete();
				} else {
					//����Լ�
					deleteAllFiles(tempFile[i].getPath());
				}
				tempFile[i].delete();
			}
			file.delete();
		}
	}
	
	@Override
	public List<String> copyDomain(Map<String, Object> map, List<Map<String, Object>> prjlist, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		String[] ProjectSeqs = map.get("PROJECT_SEQ").toString().split(",");
		
		CommandMap cdb = new CommandMap();
		CommandMap cmmap = new CommandMap();
		CommandMap tempmap = new CommandMap();
		List<CommandMap> slotmaplist = new ArrayList<CommandMap>();
		List<String> namelist = new ArrayList<String>();
		List<String> returnlist = new ArrayList<String>();
		Boolean nameChk = true;		
		
		for (int i = 0; i < ProjectSeqs.length; i++) {			
			for (Iterator iterator = prjlist.iterator(); iterator.hasNext();) {
				Map<String, Object> item = (Map<String, Object>) iterator.next();
				
				if (item.get("SEQ_NO").toString().equals(ProjectSeqs[i])) {					
					cmmap.put("OLD_PROJECT_NAME", item.get("PROJECT_NAME"));
					cmmap.put("PROJECT_NAME", String.format("%s_%s", item.get("PROJECT_NAME"), map.get("USER_ID")));
					cmmap.put("DESCRIPTION", item.get("DESCRIPTION"));
					cmmap.put("TYPE", "C");
					cmmap.put("USER_SEQ", map.get("USER_SEQ"));	
					//break;
				}
				else {
					namelist.add(item.get("PROJECT_NAME").toString());
				}
			}
			
			if (namelist.contains(cmmap.get("PROJECT_NAME"))) {
				returnlist.add(cmmap.get("PROJECT_NAME").toString());
				nameChk = false;
			}
			

			if (nameChk) {
				insertProject(cmmap.getMap(), request);					
				tempmap.put("PROJECT_SEQ", cmmap.get("SEQ_NO"));
				tempmap.put("USER_SEQ", map.get("USER_SEQ"));
				insertUserDomain(tempmap.getMap());
				tempmap.clear();
				
				cmmap.put("PROJECT_SEQ", map.get("PROJECT_SEQ"));
		    	
		    	List<Map<String, Object>> clist = viewDAO.selectSlotClass2(cmmap.getMap());
		    	for (Iterator iterator = clist.iterator(); iterator.hasNext();) {
		    		
					Map<String, Object> citem = (Map<String, Object>) iterator.next();
					
					tempmap.put("CLASS_NAME", citem.get("CLASS_NAME"));
					tempmap.put("PROJECT_SEQ", cmmap.get("SEQ_NO"));
					tempmap.put("CLASS_SEQ_NO", citem.get("SEQ_NO"));
					tempmap.put("DESCRIPTION", citem.get("DESCRIPTION"));
					tempmap.put("AGENT_SEQ", citem.get("AGENT_SEQ")); //sp���� ������Ʈ ó����
					
					insertSlotClass(tempmap.getMap());
					
					cdb.put("SEQ_NO", tempmap.get("SEQ_NO"));
					cdb.put("COL_NAME", "SEQ_NO INT NOT NULL AUTO_INCREMENT PRIMARY KEY");			
				    CreateTable(cdb.getMap());
					
					viewDAO.insertSlot2(tempmap.getMap());
					
					CommandMap tempsc = new CommandMap();
					tempsc.put("SEQ_NO", tempmap.get("SEQ_NO"));
					tempsc.put("OLD_SEQ_NO", citem.get("SEQ_NO"));
					slotmaplist.add(tempsc);
				}
		    	
		    	viewDAO.updateSlot2(cmmap.getMap());
		    	
		    	tempmap.clear();
		    	tempmap.put("PROJECT_SEQ", cmmap.get("SEQ_NO"));					
									
		    	List<Map<String, Object>> slist = selectSlot(tempmap.getMap());
		    	
		    	for (Iterator iterator = slotmaplist.iterator(); iterator.hasNext();) {
					CommandMap commandMap = (CommandMap) iterator.next();
					
					List<Map<String,Object>> slotlist = viewDAO.selectSlot2(commandMap.getMap());
					
					for (Iterator iterator2 = slotlist.iterator(); iterator2.hasNext();) {
						Map<String, Object> sitem = (Map<String, Object>) iterator2.next();
						
						cdb.put("SEQ_NO", commandMap.get("SEQ_NO"));
						cdb.put("COL_TYPE", "varchar(200)"); 					
						cdb.put("COL_NAME", String.format("%s_%s_%s", commandMap.get("SEQ_NO"), sitem.get("SEQ_NO"), sitem.get("SLOT_NAME")));
											
						if (sitem.get("SLOT_TYPE").toString().equals("C")) {
							cdb.put("SLOT_LIST", CommonUtils.SetSlotListToJson(slist));
							cdb.put("SLOT_SEQ_NO", sitem.get("SEQ_NO"));
							cdb.put("TYPE_SEQ", sitem.get("TYPE_SEQ"));						
							CreateMultiColumn(cdb.getMap());
						}
						else {
							CreateColumn(cdb.getMap());
						}
					}
					viewDAO.insertSlotInstans2(commandMap.getMap());
				}
		    	
		    	List<Map<String,Object>> slotmaplst = viewDAO.copyDomain(cmmap.getMap());
		    	Map<String, String> slotmap = new HashMap<String, String>();
		    	
		    	for (Iterator iterator = slotmaplst.iterator(); iterator.hasNext();) {
					Map<String, Object> slotmapitem = (Map<String, Object>) iterator.next();
					
					slotmap.put(slotmapitem.get("OLD_SEQ_NO").toString(), slotmapitem.get("NEW_SEQ_NO").toString());				
				}
		    	
		    	String[] SlotSeqNos = null;	    	
		    	String SlotSeqNo = "";
		    	
		    	List<Map<String,Object>> intentionlist = viewDAO.selectIntentsIntention2(cmmap.getMap());
		    	for (Iterator iterator = intentionlist.iterator(); iterator.hasNext();) {
					Map<String, Object> intentionitem = (Map<String, Object>) iterator.next();
					SlotSeqNos = intentionitem.get("SLOT_SEQ_NO").toString().split(",");
					
					for (int j = 0; j < SlotSeqNos.length; j++) {
						if (j == 0) {
							SlotSeqNo = SlotSeqNos[j]; 
						}
						else {
							SlotSeqNo += ", " + SlotSeqNos[j];
						}
					}
					tempmap.clear();
					viewDAO.updateIntentsIntention(tempmap.getMap());
				}
		    	
		    	SlotSeqNos = null;	    	
		    	SlotSeqNo = "";
		    	List<Map<String,Object>> usersaylist = viewDAO.selectIntentsUserSays3(cmmap.getMap());
		    	for (Iterator iterator = usersaylist.iterator(); iterator.hasNext();) {
					Map<String, Object> usersayitem = (Map<String, Object>) iterator.next();
					tempmap.clear();
					SlotSeqNos = usersayitem.get("SLOT_SEQ_NO").toString().split(",");
					
					for (int j = 0; j < SlotSeqNos.length; j++) {
						if (j == 0) {
							SlotSeqNo = slotmap.get(SlotSeqNos[j]); 
						}
						else {
							SlotSeqNo += ", " + slotmap.get(SlotSeqNos[j]);
						}
					}
					
					tempmap.put("SEQ_NO", usersayitem.get("SEQ_NO"));
					tempmap.put("SLOT_SEQ_NO", SlotSeqNo);
					viewDAO.updateCopyIntentUsersay(tempmap.getMap());					
				}
			}
			nameChk = false;
		}		
		return returnlist;
	}
		
	@Override
	public String Notice(Map<String, Object> map, HttpServletRequest request) throws Exception {
		// TODO Auto-generated method stub
		//String Path = String.format("%s/notice.txt", request.getSession().getServletContext().getInitParameter("enginePath"));
		String Path = String.format("%s/notice.txt", enginePath);
				
		StringBuilder sb = new StringBuilder();
								
		File f = new File(Path);
		if (f.exists()) {
			try (BufferedReader br = new BufferedReader(new FileReader(f))) {
		        String line = null;
		        while ((line = br.readLine()) != null) {
		        	sb.append(String.format("%s<br/>", line));
		        }
		    } catch (IOException e) {
		    	log.debug(e.getMessage());
		    	e.printStackTrace();		        
		    }
		}		
		return sb.toString();
	}
	
	@Override
	public String GetPort(String[] arrUsingPort) throws Exception {
		Random random = new Random();
		Ports.SetPort();
		String tempPort = Ports.Port.get(random.nextInt(1119));
		Boolean bChk = false;
		for (int i = 0; i < arrUsingPort.length; i++) {
			if (arrUsingPort[i].equals(tempPort)) {
				bChk = true;
				break;
			}
		}
		if (bChk) {
			return GetPort(arrUsingPort);
		}
		else {
			return tempPort;
		}
	}
	
	public Map<String, Object> getDomainByAgentSeq(int agentSeq) {
		Map<String, Object> params = new HashMap<>();
		params.put("AGENT_SEQ", agentSeq);
		return viewDAO.getDomainByAgentSeq(params);
	}
}
