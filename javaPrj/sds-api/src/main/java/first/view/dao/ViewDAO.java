package first.view.dao;

import java.util.List;
import java.util.Map;

//import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import first.common.dao.AbstractDAO;

@Repository("viewDAO")
//@Mapper
//@Qualifier("viewDAO")
public class ViewDAO extends AbstractDAO {
	public void CreateTable(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.createtable", map);
	}
	
	public void DropTable(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.droptable", map);
	}
	
	public void CreateColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.createcolumn", map);
	}
	
	public void AlterColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.altercolumn", map);
	}
	
	public void DropColumn(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deletecolumn", map);
	}
	
	public void insertProject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertProject", map);	
	}
	
	public void updatePorts(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.updateports", map);	
	}
	
	public void updatePorts2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.updateports2", map);	
	}
	
	public void insertSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotclass", map);	
	}
	
	public void insertSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslot", map);	
	}
	
	public void insertSlotColor(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotcolor", map);	
	}
	
	public void insertSlot2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslot2", map);	
	}
	
	public void insertSlotDefine(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotdefine", map);	
	}
	
	public void insertSlotTypeClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslottypeclass", map);	
	}
	
	public void deleteSlotDefine(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotdefine", map);
	}	
	
	public void deleteSlotTypeClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslottypeclass", map);
	}	
	
	public void deleteSlotTypeClassAll(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslottypeclassall", map);
	}
	
	public void deleteProject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteproject", map);	
	}
	
	public void deleteSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotclass", map);	
	}
	
	public void deleteSlotClassSeq(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotclassseq", map);	
	}	

	public void deleteSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslot", map);	
	}
	
	public void updateProject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateproject", map);	
	}
	
	public void updateSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateslotclass", map);	
	}
	
	public void updateSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		update("view.updateslot", map);
	}
	
	public void updateSlot2(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		update("view.updateslot2", map);
	}
	
	public void updateSlotSort(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		update("view.updateslotsort", map);
	}
	
	public void deleteSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotobject", map);
	}
	
	public void deleteSlotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotobjectdetail", map);
	}
	
	public void updateSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.updateslotobject", map);
	}
	
	public void insertSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotobject", map);
	}
	
	public void insertSlotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotobjectdetail", map);
	}
	
	
	public void insertSlotInstans(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotinstans", map);
	}
	
	public void insertSlotInstans2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertslotinstans2", map);
	}
	
	public void updateSlotInstans(Map<String, Object> map) {
		// TODO Auto-generated method stub		
		update("view.updateslotinstans", map);
	}
	
	public void deleteSlotInstans(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteslotinstans", map);
	}
	
	public void insertIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertintent", map);
	}
	
	public void updateIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.updateintent", map);
	}
	
	public void insertIntentUserSay(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertintentusersay", map);
	}
	
	public void updateIntentUserSay(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateintentusersay", map);
	}
	
	public void insertIntentIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertintentintention", map);
	}
	
	public void insertIntentObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertintentobject", map);
	}
	
	public void insertIntentObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertintentobjectdetail", map);
	}	
	
	public void deleteIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintent", map);
	}
	
	public void deleteIntentUserSay(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentusersay", map);
	}
	
	public void deleteIntentUserSayDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentusersaydetail", map);
	}
	
	public void deleteIntentsUserSays(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsusersays", map);
	}
	
	public void updateCopyIntentUsersay(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updatecopyintentusersay", map);
	}
	
	public void deleteIntentsIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsintention", map);
	}
	
	public void deleteIntentsIntention2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsintention2", map);
	}
	
	public void updateIntentsIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateintentsintention", map);
	}
	
	public void deleteIntentsObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsobject", map);
	}
	
	public void deleteIntentsObject2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsobject2", map);
	}
	
	public void deleteIntentsObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsobjectdetail", map);
	}
	
	public void deleteIntentsObjectDetail2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteintentsobjectdetail2", map);
	}
	
		
	
	public void insertAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagent", map);
	}
	
	public void insertAgentFillingSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentfillingslot", map);
	}
	
	public void insertAgentFillingSlotIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentfillingslotintention", map);
	}
	
	public void insertAgentIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentintention", map);
	}
	
	public void insertAgentObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentobject", map);
	}
	
	public void insertAgentObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentobjectdetail", map);
	}
	
	public void deleteAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagent", map);
	}
	
	public void deleteAgentFillingSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentfillingslot", map);
	}
	
	public void deleteAgentIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentintention", map);
	}
	
	public void deleteAgentIntentsObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentintentsobject", map);
	}
	
	public void deleteAgentIntentsObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentintentsobjectdetail", map);
	}
	
	public void deleteAgentGraph2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentgraph2", map);
	}
	
	public void deleteAgentGraph3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentgraph3", map);
	}
	
	public void updateAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateagent", map);
	}
	
	public void updateAgentPosition(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateagentposition", map);
	}
	
	public void insertAgentGraph(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertagentgraph", map);
	}
	
	public void deleteAgentGraph(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteagentgraph", map);
	}
	
	public void insertUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertuser", map);
	}
	
	public void updateUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updateuser", map);
	}
	
	public void deleteUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteuser", map);
	}
	
	public void insertLua(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertlua", map);
	}
	
	public void updateLua(Map<String, Object> map) {
		// TODO Auto-generated method stub
		update("view.updatelua", map);
	}
	
	public void deleteLua(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deletelua", map);
	}
	
	public void insertUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		insert("view.insertuserdomain", map);
	}
	
	public void deleteUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		delete("view.deleteuserdomain", map);
	}
		
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> SelectProjectSa(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectProjectListSa", map);	
	}
	
	public List<Map<String, Object>> SelectProjectGu(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectProjectListGu", map);	
	}
	
	public List<Map<String, Object>> SelectProjectName(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectProjectName", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotclass", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotClass2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotclass2", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotClass3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotclass3", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslot", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlot2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslot2", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotIntents(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotintents", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotDefine(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotdefine", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectFunction(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectfunction", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotType(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslottype", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotobject", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotObjectDetail(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotobjectdetail", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotObjectDetailExcel(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotobjectdetailexcel", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotInstances(Map<String, Object> map) {
		// TODO Auto-generated method stub		//
		return (List<Map<String, Object>>)selectPagingList("view.selectslotinstansces", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotInstances2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslotclassintances2", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSlotTypeClass(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectslottypeclass", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectVSlotObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectvslotobject", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntents(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintens", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsUserSays(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensusersays", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsUserSays2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensusersays2", map);
	}	
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsUserSays3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensusersays3", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensintention", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsObejct(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensobject", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsObjectDtl(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintensobjectdtl", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectIntentsIntention2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectintentsintention2", map);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectFillingSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectfillingslot", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgents(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagents", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectagent", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentSlotFilling(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentslotfilling", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentIntention(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentintention", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentObject(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentobject", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentObjectdtl(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentobjectdtl", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectAgentGraph(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectagentgraph", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanNextAgent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleannextagent", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLibGroup1(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglibgroup1", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib1_1(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib1_1", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib1_2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib1_2", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib1Obj(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib1obj", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib1ObjDtl(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib1objdtl", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib2Main(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib2main", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib2UserSay(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib2usersay", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib2_1(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib2_1", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib2_2(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib2_2", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib2Obj(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib2obj", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib2ObjDtl(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib2objdtl", map);	
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib3Main(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib3main", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDialogLib3(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandialoglib3", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanTaskSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleantaskslot", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectleanDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandomain", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLeanDatype(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectleandatype", map);	
	}	
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectSqlLiteSlot(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectsqlliteslot", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectsqlLiteInstance(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectsqlliteinstance", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectUserIdChk(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectuseridchk", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectUserList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectPagingList("view.selectuserlist", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectLuaList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectPagingList("view.selectlualist", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectLua(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectlua", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectuser", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> loginUser(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.loginuser", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectIntent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectintent", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectSlotColorCheck(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectslotcolorcheck", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectSlotColor(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.selectslotcolor", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectUserDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectuserdomain", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectUserDomainList(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.selectuserdomainlist", map);	
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> copyDomain(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (List<Map<String, Object>>)selectList("view.copydomain", map);	
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getDomainByAgentSeq(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return (Map<String, Object>)selectOne("view.getDomainByAgentSeq", map);	
	}
}
