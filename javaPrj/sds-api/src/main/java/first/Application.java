package first;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@SpringBootApplication
@ComponentScan(basePackages={"first", "ai.mindslab"}, excludeFilters=@ComponentScan.Filter(type=FilterType.ASPECTJ, pattern = {"ai.mindslab.bizchatbot.commons.hazelcast.*","ai.mindslab.bizchatbot.commons.cache.service.*", "ai.mindslab.bizchatbot.commons.session.*"}))
public class Application extends SpringBootServletInitializer {
	
	private static Logger logger = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args) {
		logger.info("SDS API Server newly started.");
		
		SpringApplication.run(Application.class, args);
	}
	
//    @Bean
//    public HttpMessageConverter<String> responseBodyConverter() {
//        return new StringHttpMessageConverter(Charset.forName("UTF-8"));
//    }
// 
//    @Order(Ordered.HIGHEST_PRECEDENCE)
//    @Bean
//    public Filter characterEncodingFilter() {
//        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
//        characterEncodingFilter.setEncoding("UTF-8");
//        characterEncodingFilter.setForceEncoding(true);
//        return characterEncodingFilter;
//    }
}

