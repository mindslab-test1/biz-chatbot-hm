package first.common.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class CommonUtils {
	private static final Logger log = LoggerFactory.getLogger(CommonUtils.class);
	
	public static String getRandomString(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
	public static void printMap(Map<String,Object> map){
		Iterator<Entry<String,Object>> iterator = map.entrySet().iterator();
		Entry<String,Object> entry = null;
		log.debug("--------------------printMap--------------------\n");
		while(iterator.hasNext()){
			entry = iterator.next();
			log.debug("key : "+entry.getKey()+",\tvalue : "+entry.getValue());
		}
		log.debug("");
		log.debug("------------------------------------------------\n");
	}
	
	public static void printList(List<Map<String,Object>> list){
		Iterator<Entry<String,Object>> iterator = null;
		Entry<String,Object> entry = null;
		log.debug("--------------------printList--------------------\n");
		int listSize = list.size();
		for(int i=0; i<listSize; i++){
			log.debug("list index : "+i);
			iterator = list.get(i).entrySet().iterator();
			while(iterator.hasNext()){
				entry = iterator.next();
				log.debug("key : "+entry.getKey()+",\tvalue : "+entry.getValue());
			}
			log.debug("\n");
		}
		log.debug("------------------------------------------------\n");
	}
	
	public static String SetSlotListToJson(List<Map<String, Object>> slist) throws JsonGenerationException, JsonMappingException, IOException {
		List<Map<String, Object>> templist = new ArrayList<Map<String, Object>>();
    	Map<String, List<Map<String, Object>>> tempmap = new HashMap<String, List<Map<String, Object>>>();
    	
    	String classSeq = "";
    	int iCnt = 0;
    	for (Iterator iterator = slist.iterator(); iterator.hasNext();) {    		
			Map<String, Object> item = (Map<String, Object>) iterator.next();			
			if(iCnt > 0) {
				if (classSeq.equals(item.get("CLASS_SEQ_NO").toString())) {
					templist.add(item);
				}
				else {
					tempmap.put(classSeq, templist);
					classSeq = item.get("CLASS_SEQ_NO").toString();
					templist = new ArrayList<Map<String, Object>>();
					templist.add(item);
				}
			}
			else {
				classSeq = item.get("CLASS_SEQ_NO").toString();
				templist.add(item);
			}
			iCnt++;
		}
    	tempmap.put(classSeq, templist);
    	String jsonData = new ObjectMapper().writeValueAsString(tempmap);
    	return jsonData;
	}
	
	public static String SetIntentListToJson(List<Map<String, Object>> slist) throws JsonGenerationException, JsonMappingException, IOException {
		List<Map<String, Object>> templist = new ArrayList<Map<String, Object>>();
    	Map<String, List<Map<String, Object>>> tempmap = new HashMap<String, List<Map<String, Object>>>();
    	
    	String agentSeq = "";
    	int iCnt = 0;
    	for (Iterator iterator = slist.iterator(); iterator.hasNext();) {    		
			Map<String, Object> item = (Map<String, Object>) iterator.next();			
			if(iCnt > 0) {
				if (agentSeq.equals(item.get("AGENT_SEQ").toString())) {
					templist.add(item);
				}
				else {
					tempmap.put(agentSeq, templist);
					agentSeq = item.get("AGENT_SEQ").toString();
					templist = new ArrayList<Map<String, Object>>();
					templist.add(item);
				}
			}
			else {
				agentSeq = item.get("AGENT_SEQ").toString();
				templist.add(item);
			}
			iCnt++;
		}
    	tempmap.put(agentSeq, templist);
    	String jsonData = new ObjectMapper().writeValueAsString(tempmap);
    	return jsonData;
	}
}
