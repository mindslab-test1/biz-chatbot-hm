package first.common.util;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.servlet.view.AbstractView;
/*import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.web.servlet.view.document.AbstractExcelView;*/

import first.common.common.CommandMap;
import first.view.dao.ViewDAO;
import first.view.service.ViewService;

public class ExcelUtils extends AbstractView {
	Logger log = LoggerFactory.getLogger(this.getClass());
//	@Resource(name="viewService")
	@Autowired
	@Qualifier("viewService")
	private ViewService viewService;
	
	private static final String CONTENT_TYPE = "application/vnd.ms-excel";
	
	public ExcelUtils() {
	    setContentType(CONTENT_TYPE);
	}
	
	protected void renderMergedOutputModel(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) throws Exception {
	    try {
	        Workbook workbook = new XSSFWorkbook();	       
	        Sheet worksheet;
	        Row row;
	        
	        // Doing
	        String excelName = "Entry";
	        String Type = map.get("TYPE").toString();
	      
	        
	        if(Type == "O") {			
				excelName = String.format("[Slot]%s#%s.%s", map.get("PROJECT_NAME").toString(), map.get("CLASS_NAME").toString(), map.get("SLOT_NAME").toString());
				worksheet = workbook.createSheet(map.get("SLOT_NAME").toString());
				
				@SuppressWarnings("unchecked")
				List<Map<String, Object>> objlist = (List<Map<String, Object>>)map.get("OBJECT_LIST");
				List<Map<String, Object>> objdtllist = (List<Map<String, Object>>)map.get("DETAIL_LIST");			
				
				int iCol = 0;				
				for(int i = 0; i < objlist.size(); i++){
					iCol = 0;
					row = worksheet.createRow(i);
					
					row.createCell(iCol).setCellValue(objlist.get(i).get("OBJECT_NAME").toString());
					for (int j = 0; j < objdtllist.size(); j++) {
						if (objlist.get(i).get("SEQ_NO").equals(objdtllist.get(j).get("PARENT_SEQ_NO"))) {
							iCol++;						
							row.createCell(iCol).setCellValue(objdtllist.get(j).get("OBJECT_NAME").toString());
						}												
					}					
				}             			 
			}
	        else if(Type == "I") {
	        	excelName = String.format("[Instance]%s#%s", map.get("PROJECT_NAME").toString(), map.get("CLASS_NAME").toString());
				worksheet = workbook.createSheet(map.get("CLASS_NAME").toString());
				String[] arrHeader = map.get("HEADER_LIST").toString().split(",");
				String[] arrCeader = map.get("COL_LIST").toString().split(",");
				String[] arrTemp;
			
		        int iRow = 1;
		        int iCol = 0;
		        CommandMap ObjMap;
		        List<Map<String, Object>> list;
		        
		        row = worksheet.createRow(0);
		        Row instancRow;
		        for (int i = 0; i < arrHeader.length; i++) {		        	
					row.createCell(i).setCellValue(arrHeader[i]);									
				}
		        
		        ObjMap = new CommandMap();					
				ObjMap.put("SEQ_NO", map.get("SEQ_NO"));
				ObjMap.put("COL_LIST", map.get("COL_LIST"));
				
				list = viewService.selectSlotInstances2(ObjMap.getMap());
				for (Iterator iterator = list.iterator(); iterator.hasNext();) {
					row = worksheet.createRow(iRow);
					
					Map<String, Object> obj = (Map<String, Object>) iterator.next();
					
					
					for (int j = 0; j < arrCeader.length; j++) {
						row.createCell(j).setCellValue(obj.get(arrCeader[j]).toString());
					}
					
		        	iRow++;	
				}
				/*String[] arrHeader = map.get("HEADER_LIST").toString().split(",");
				String[] arrTemp;
			
		        int iRow = 1;
		        int iCol = 0;
		        CommandMap ObjMap;
		        List<Map<String, Object>> list;
		        
		        row = worksheet.createRow(0);
		        Row instancRow;
		        for (int i = 0; i < arrHeader.length; i++) {
		        	arrTemp = arrHeader[i].split("��");
					row.createCell(i).setCellValue(arrTemp[0]);
					
					ObjMap = new CommandMap();
					ObjMap.put("SLOT_SEQ_NO", arrTemp[1]);
					ObjMap.put("CLASS_SEQ_NO", map.get("SEQ_NO"));
					iRow = 1;
					list = viewService.selectSlotInstances2(ObjMap.getMap());
					for (Iterator iterator = list.iterator(); iterator.hasNext();) {
						Map<String, Object> obj = (Map<String, Object>) iterator.next();
					
						instancRow = worksheet.getRow(iRow);
			        	if (instancRow == null) {
			        		instancRow = worksheet.createRow(iRow);
						}
			        	instancRow.createCell(i).setCellValue(obj.get("INSTANCE_VAL").toString());
			        	iRow++;	
					}
				}*/		 
	        }
	        
	        log.debug(excelName);
	        excelName = getDisposition(excelName, getBrowser(request));
	        log.debug(excelName);
	        //response.setContentType("application/vnd.ms-excel; charset=euc-kr");
	        response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+".xlsx");
	        response.setHeader("Content-Transfer-Encoding", "binary");
	        ServletOutputStream out = response.getOutputStream();
	        workbook.write(out);
	
	        if (out != null) out.close();
	        
	    } catch (Exception e) {
	        throw e;
	    }
	}
	
	public List<Map<String, String>> read(ReadOption readOption) {				
		Workbook wb = FileType.getWorkbook(readOption.getFilePath());		
		Sheet sheet = wb.getSheetAt(0);				
		int numOfRows = sheet.getPhysicalNumberOfRows();
		int numOfCells = 0;
		
		Row row = null;
		Cell cell = null;	
		String cellName = "";		
		Map<String, String> map = null;		
		
		List<Map<String, String>> result = new ArrayList<Map<String, String>>(); 
		
		for(int rowIndex = readOption.getStartRow() - 1; rowIndex < numOfRows; rowIndex++) {				
			row = sheet.getRow(rowIndex);
			
			if(row != null) {				
				numOfCells = row.getPhysicalNumberOfCells();				
				map = new HashMap<String, String>();

				for(int cellIndex = 0; cellIndex < numOfCells; cellIndex++) {
					cell = row.getCell(cellIndex);
					cellName = CellRef.getName(cell, cellIndex);					

					/*
					 * ���� ��� �÷����� Ȯ���Ѵ�.
					 * ���� ��� �÷��� �ƴ϶��
					 * for�� �ٽ� �ö󰣴�. 
					 */
					if( !readOption.getOutputColumns().contains(cellName) ) {						
						continue;
					}
					
					/*
					 * �� ��ü�� Cell�� �̸��� Ű(Key)�� �����͸� ��´�.
					 */
					map.put(cellName, CellRef.getValue(cell));
				}								
				result.add(map);
				
			}
			
		}		
		return result;		
	}
	
	private String getBrowser(HttpServletRequest request) { 
		String header = request.getHeader("User-Agent"); 
		if (header.indexOf("MSIE") > -1) { 
			return "MSIE"; 
		} 
		else if (header.indexOf("Chrome") > -1) { 
			return "Chrome"; 
		} 
		else if (header.indexOf("Opera") > -1) { 
			return "Opera"; 
		}
		else if (header.indexOf("Trident/7.0") > -1) { 
			//IE 11 �̻� //IE ���� �� üũ >> Trident/6.0(IE 10) , Trident/5.0(IE 9) , Trident/4.0(IE 8) 
			return "MSIE"; 
		} 
		
		return "Firefox";
	}
	
	private String getDisposition(String filename, String browser) throws Exception { 
		String encodedFilename = null; 
		if (browser.equals("MSIE")) { 
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20"); 
		}
		else if (browser.equals("Firefox")) { 
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\""; 
		}
		else if (browser.equals("Opera")) { 
			encodedFilename = "\"" + new String(filename.getBytes("UTF-8"), "8859_1") + "\""; 
		} 
		else if (browser.equals("Chrome")) { 
			StringBuffer sb = new StringBuffer(); 
			for (int i = 0; i < filename.length(); i++) { 
				char c = filename.charAt(i); 
				if (c > '~') { 
					sb.append(URLEncoder.encode("" + c, "UTF-8")); 
				} 
				else { 
					sb.append(c); 
				} 
			} 
			encodedFilename = sb.toString(); 
		} 
		else { 
			throw new RuntimeException("Not supported browser"); 
		} 
		return encodedFilename; 
	}
			
	/*@Override
	protected void buildExcelDocument(Map<String, Object> map, HSSFWorkbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		String Type = map.get("TYPE").toString();
		String excelName = "";
		HSSFSheet worksheet = null;
        HSSFRow row = null;
        
        log.debug(map);        
        
		if(Type == "O") {			
			excelName = URLEncoder.encode("Entry","UTF-8");
			worksheet = workbook.createSheet(map.get("SLOT_NAME").toString());
			
			@SuppressWarnings("unchecked")
			List<Map<String, Object>> objlist = (List<Map<String, Object>>)map.get("OBJECT_LIST");
			List<Map<String, Object>> objdtllist = (List<Map<String, Object>>)map.get("DETAIL_LIST");			
			
			row = worksheet.createRow(0);
			for(int i = 0; i < objlist.size(); i++){
				row.createCell(i).setCellValue(objlist.get(i).get("OBJECT_NAME").toString());            	
			}             			 
		}
		
		response.setContentType("Application/Msexcel");
        response.setHeader("Content-Disposition", "ATTachment; Filename="+excelName+"-excel");
	}*/
}
