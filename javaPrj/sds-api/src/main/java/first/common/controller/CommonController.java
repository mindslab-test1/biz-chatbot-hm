package first.common.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import first.common.common.CommandMap;
import first.common.service.CommonService;

@Controller
public class CommonController {
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	@Qualifier("commonService")
	private CommonService commonService;
	
	@RequestMapping(value="/common/downloadFile.do")
	public void downloadFile(CommandMap commandMap, HttpServletResponse response) throws Exception{		
		Map<String,Object> map = commonService.selectFileInfo(commandMap.getMap());
		String storedFileName = (String)map.get("STORED_FILE_NAME");
		String originalFileName = (String)map.get("ORIGINAL_FILE_NAME");
		
		byte fileByte[] = FileUtils.readFileToByteArray(new File("E:\\dev\\file\\"+storedFileName));
		
		response.setContentType("application/octet-stream");
		response.setContentLength(fileByte.length);
		response.setHeader("Content-Disposition", "attachment; fileName=\"" + URLEncoder.encode(originalFileName,"UTF-8")+"\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.getOutputStream().write(fileByte);
		
		response.getOutputStream().flush();
		response.getOutputStream().close();
	}
	
	@RequestMapping(value="/common/downloadzipFile.do")
	public void downloadZipFile(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response) throws Exception{
		//String copyPath = "E:\\dev\\file\\webtool\\www_data\\dialog_domain\\" + commandMap.get("PROJECT_NAME").toString() + "\\";
		String copyPath = request.getSession().getServletContext().getInitParameter("enginePath") + "/dialog_domain/" + commandMap.get("PROJECT_NAME").toString() + "/";
		
		String fileName = copyPath + commandMap.get("PROJECT_NAME").toString() + ".zip";
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(fileName));
		byte fileByte[] = new byte[4096];
		
		Vector DataFiles = new Vector();
		File sourceF = new File(copyPath);
		File[] Files = sourceF.listFiles();
		
		for (File file : Files) {
			//log.debug(file.getName());
			if(!file.isDirectory() && !file.getName().contains(".zip")) {
				//���� ���� �ϰ�, ��� �ٿ�				
				FileInputStream fis = new FileInputStream(file);			
				out.putNextEntry(new ZipEntry(file.getName()));
				//fileByte = FileUtils.readFileToByteArray(file);
				
				int len = 0;
				while((len = fis.read(fileByte)) > 0) {
					out.write(fileByte, 0, len);						
				}
				out.closeEntry();
				fis.close();
				
				/*if (file.getName().contains(commandMap.get("PROJECT_NAME").toString() + ".DAtype") ||
						file.getName().contains(commandMap.get("PROJECT_NAME").toString() + ".dialogLib") ||
						file.getName().contains(commandMap.get("PROJECT_NAME").toString() + ".slot") ||
						file.getName().contains(commandMap.get("PROJECT_NAME").toString() + ".task") ||
						file.getName().contains(commandMap.get("PROJECT_NAME").toString() + ".txt") ||
						file.getName().contains(commandMap.get("PROJECT_NAME").toString() + ".sql")) {		
					FileInputStream fis = new FileInputStream(file);			
					out.putNextEntry(new ZipEntry(file.getName()));
					//fileByte = FileUtils.readFileToByteArray(file);
					
					int len = 0;
					while((len = fis.read(fileByte)) > 0) {
						out.write(fileByte, 0, len);						
					}
					out.closeEntry();
					fis.close();
				}*/
				
			}
		}		
		out.close();
		
		response.setContentType("application/x-zip-compressed; charset=utf-8");
		response.setHeader("Content-Disposition", "inline; filename="+new File(fileName).getName());
		response.setHeader("Content-Transfer-Encoding", "binary");
		
		InputStream in = new FileInputStream(new File(fileName));
		int read = in.read(fileByte);
		while (read != -1) {
			response.getOutputStream().write(fileByte, 0, read);
			read = in.read(fileByte);
		}
		response.getOutputStream().flush();
		response.getOutputStream().close();
	}
}
