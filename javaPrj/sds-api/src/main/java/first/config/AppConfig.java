package first.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@MapperScan(basePackages= {"first.common.dao", "first.view.dao", "ai.mindslab.sds.dao"}, sqlSessionFactoryRef = "sdsSessionFactory")
//@MapperScan(basePackages= {"ai.mindslab.sds.dao","ai.mindslab.bizchatbot.commons.chatbot.dao"}, sqlSessionFactoryRef = "sdsSessionFactory")
public class AppConfig {
	
	@Autowired
	private MybatisConfigurationSupport mybatisConfigurationSupport;
	
	@Bean(name="sdsDataSource", destroyMethod="close")
	@ConfigurationProperties(prefix="sds.datasource")
	@Primary
	public DataSource sdsDataSource() {
		DataSource ds = DataSourceBuilder.create().build();
		return ds;
	}
	
	@Bean(name="sdsTransactionManager")
	@Primary
	public PlatformTransactionManager sdsTransactionManager() {
		return new DataSourceTransactionManager(sdsDataSource());
	}
	
	@Bean(name="sdsSessionFactory")
	@Primary
	public SqlSessionFactory sqlSessionFactory(@Qualifier("sdsDataSource")DataSource dataSource) throws Exception{
		return mybatisConfigurationSupport.build(dataSource);
	}

    @Bean(name="sdsSqlSessionTemplate")
    @Primary
    public SqlSessionTemplate sqlSessionTemplate(@Qualifier("sdsSessionFactory")SqlSessionFactory sqlSessionFactory) throws Exception{
    		return new SqlSessionTemplate(sqlSessionFactory);
    }
}
