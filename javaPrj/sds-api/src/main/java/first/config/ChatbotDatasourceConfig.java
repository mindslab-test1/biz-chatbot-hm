package first.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@MapperScan(basePackages= {"ai.mindslab.bizchatbot.commons.chatbot.dao", "ai.mindslab.bizchatbot.sdsadmin.dao"}, sqlSessionFactoryRef = "chatbotSessionFactory")
public class ChatbotDatasourceConfig {

	@Autowired
	private MybatisConfigurationSupport mybatisConfigurationSupport;
	
	@Bean(name="chatbotDataSource", destroyMethod="close")
	@ConfigurationProperties(prefix="spring.datasource")
	public DataSource chatbotDataSource() {
		DataSource ds = DataSourceBuilder.create().build();
		return ds;
	}
	
	@Bean(name="chatbotTransactionManager")
	public PlatformTransactionManager chatbotTransactionManager() {
		return new DataSourceTransactionManager(chatbotDataSource());
	}
	
	@Bean(name="chatbotSessionFactory")
	public SqlSessionFactory sqlSessionFactory(@Qualifier("chatbotDataSource") DataSource dataSource) throws Exception{
		return mybatisConfigurationSupport.build(dataSource);
	}

	@Bean(name="chatbotSqlSessionTemplate")
    public SqlSessionTemplate sqlSessionTemplate(@Qualifier("chatbotSessionFactory")SqlSessionFactory sqlSessionFactory) throws Exception{
    		return new SqlSessionTemplate(sqlSessionFactory);
    }
}
