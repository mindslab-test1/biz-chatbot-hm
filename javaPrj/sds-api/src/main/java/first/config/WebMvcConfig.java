package first.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.HazelcastInstance;
//import com.hazelcast.core.HazelcastInstance;
//import com.hazelcast.spring.cache.HazelcastCacheManager;
import com.hazelcast.spring.cache.HazelcastCacheManager;

import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import first.common.resolver.CustomMapArgumentResolver;

@Configuration
//@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter{

	@Autowired
	private CustomMapArgumentResolver mapArgumentResolver;
	
	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(mapArgumentResolver);
//		super.addArgumentResolvers(argumentResolvers);
	}

	@Bean
	public MappingJackson2JsonView jsonView() {
		return new MappingJackson2JsonView();
	}
	
	@Bean public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() { 
		MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter(); 
		ObjectMapper objectMapper = new ObjectMapper(); 
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		jsonConverter.setObjectMapper(objectMapper); 
		return jsonConverter;
	}

	@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
//		super.extendMessageConverters(converters);
		converters.add(mappingJackson2HttpMessageConverter());
	}

//EnableWebMvc 빼면 없어져도 됨.->property	
//	@Bean
//    public ViewResolver getViewResolver() {
//        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
//        resolver.setPrefix("/WEB-INF/views/");
//        resolver.setSuffix(".jsp");
//        return resolver;
//    }
//
//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		registry.addResourceHandler("/**").addResourceLocations("/webapp/");
//		super.addResourceHandlers(registry);
//	}
	
//	@Value("${hazelcast.server.ip}")
//	private String[] hazelIps;
//	
//	@Value("${hazelcast.group.name}")
//	private String groupName;
//	
//	@Value("${hazelcast.group.password}")
//	private String groupPass;
//	
//	@Autowired
//	@Qualifier("hazelClientCommon")
//	private HazelClientCommon hazelClient;
	
	
//	@Bean
//	@Qualifier("hazelCacheManager")
//	public CacheManager cacheManager() {
//		return new HazelcastCacheManager(hazelcastInstance());
//	}
//	
//	@Bean
//    public HazelcastInstance hazelcastInstance() {
//		return hazelClient.getClient();
//    }
//	
//	@Bean
//    public KeyGenerator keyGenerator() {
//        return new SimpleKeyGenerator();
//    }
}
