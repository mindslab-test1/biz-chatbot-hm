package ai.mindslab.bizchatbot.sdsadmin.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SdsAdminMapper {

	public Map<String, Object> getIfbyAgentIntent(HashMap<String, Object> params);
	public Map<String, Object> getRichContent(HashMap<String, Object> params);
	public long deleteRichContent(int rcId);
	public List<Map<String, Object>> getRichButtons(HashMap<String, Object> params);
	public List<Map<String, Object>> getRichImage(HashMap<String, Object> params);
	public List<Map<String, Object>> getRichCarousel(HashMap<String, Object> params);
	
	public List<Map<String, Object>> getParams(HashMap<String, Object> params);
	public Map<String, Object> getApiInfo(HashMap<String, Object> params);
	public List<Map<String, Object>> getApiResultMappingInfo(HashMap<String, Object> params);
	public Map<String, Object> getJsonTemplate(HashMap<String, Object> params);
}
