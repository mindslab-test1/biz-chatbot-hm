package ai.mindslab.bizchatbot.sdsadmin.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.net.URLCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import ai.mindslab.bizchatbot.sdsadmin.controller.data.SDSSettingReq;
import ai.mindslab.bizchatbot.sdsadmin.service.ChatbotAdminService;
import first.common.common.CommandMap;
import first.common.util.AES256Util;
import first.view.service.ViewService;


@RestController
@RequestMapping(path="/admin/sds/auth")
public class ChatbotAdminController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ChatbotAdminService adminService;
	
	@Autowired
	@Qualifier("viewService")
	private ViewService viewService;
	
	private String key = "etri-dialog-key!@";
	
	
	@RequestMapping(value="/change", consumes="application/json", method = {RequestMethod.POST})
	public ModelAndView changeSetting(@RequestBody SDSSettingReq setting, HttpServletRequest req, HttpServletResponse resp) throws Exception{
		
		logger.info("/admin/sds/auth/change/{}",setting);
		
		//store info.
		adminService.setSdsSetting(setting);
		adminService.updateSession(req);
		adminService.updateCookie(resp);
		
		ModelAndView mv = new ModelAndView("jsonView");
		
		mv.setStatus(HttpStatus.OK);
		mv.addObject("data", setting);
		mv.addObject("code", 0);
		mv.addObject("errMsg", "SUCCESS");
		return mv;
	}
	
	@RequestMapping(value="/loginajax", consumes="application/x-www-form-urlencoded", method = {RequestMethod.POST})
	public ModelAndView loginajax(@RequestParam(name="userId", required=true)String userId, @RequestParam(name="pass", required=true)String pass, HttpServletRequest req, HttpServletResponse resp) throws Exception{
//		public ModelAndView loginajax(@RequestParam(name="USER_ID", required=true)String userId, @RequestParam(name="USER_PWD", required=true)String pass, HttpServletRequest req, HttpServletResponse resp) throws Exception{
		
		logger.info("/admin/sds/auth/loginajax/{}",userId);
		
		ModelAndView mv = new ModelAndView("jsonView");	
		
		AES256Util aes256 = new AES256Util(key);
		URLCodec codec = new URLCodec();	
		String encpwd = codec.encode(aes256.aesEncode(pass));
		
		CommandMap commandMap = new CommandMap();
		commandMap.put("USER_ID", userId);
		commandMap.put("USER_PWD", encpwd);
		
		Map<String,Object> userinfo = viewService.loginUser(commandMap.getMap());
		
		SDSSettingReq userSetting = new SDSSettingReq();
		userSetting.setUserName((String)userinfo.get("USER_NAME"));
		userSetting.setUserSeq((Integer)userinfo.get("SEQ_NO"));
		userSetting.setUserType((String)userinfo.get("USER_TYPE"));
		
		adminService.setSdsSetting(userSetting);
		adminService.updateSession(req);
		adminService.updateCookie(resp);
		
		mv.addObject("userinfo", userinfo);
		return mv;
	}
}
