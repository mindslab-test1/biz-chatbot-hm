package ai.mindslab.bizchatbot.sdsadmin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotApiMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotRichContentMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfoDisp;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiJsonTemplate;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiParam;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotButton;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRespInterfaceRelData;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichCarousel;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichImage;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.data.BaseListObject;
import ai.mindslab.bizchatbot.commons.utils.StrUtils;
import ai.mindslab.bizchatbot.sdsadmin.dao.SdsAdminMapper;
import ai.mindslab.bizchatbot.sdsadmin.util.HttpUtils;
import ai.mindslab.bizchatbot.sdsadmin.util.ImageUtils;

@Service
public class SdsAdminService {
	
	private Logger logger = LoggerFactory.getLogger(SdsAdminService.class);
	
//	@Autowired
//	private SdsAdminMapper adminMapper; 
	
	@Autowired
	protected ChatbotRichContentMapper rich;
	
	@Autowired
	protected SdsAdminMapper sdsAdminMapper;
	
	@Autowired
	protected ChatbotApiMapper chatbotMapper;
	
	@Autowired
	@Qualifier("imageUtils")
	private ImageUtils imgUtils;

//	@Transactional(transactionManager="chatbotTransactionManager")
//	public Map<String, Object> getRichContent(int agentSeq, int intentSeq, String intentName) throws Exception{
//		logger.info("getRichContent/{},{}", agentSeq, intentSeq);
//		
//		HashMap<String, Object> params = new HashMap<>();
//		params.put("agent_seq", agentSeq);
//		params.put("intent_seq", intentSeq);
//		params.put("resp_code", intentName);
//		
//		Map<String, Object> ifRel = adminMapper.getIfbyAgentIntent(params);
//		Map<String, Object> out = null;
//		int ifId = 0;
//		String ifType = "";
//		if(ifRel != null) {
//			ifId = (Integer)ifRel.get("if_id");
//			ifType = (String)ifRel.get("resp_type_cd");
//			
//			if(IChatbotConst.RICH_CONTENT_TYPE_BUTTON.equals(ifType)) {
//				params.clear();
//				params.put("rc_id", ifId);
//				out = adminMapper.getRichButtons(params);
//			}else if(IChatbotConst.RICH_CONTENT_TYPE_IMAGE.equals(ifType)) {
//				
//			}else if(IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL.equals(ifType)) {
//				
//			}else if(IChatbotConst.RICH_CONTENT_TYPE_API.equals(ifType)) {
//				params.clear();
//				params.put("if_id", ifId);
//				out = adminMapper.getApiInfo(params);
//				
//				Map<String,Object> apiParam = adminMapper.getParams(params);
//				if(apiParam != null) {
//					out.put("apiParam", apiParam);
//				}
//				if(apiParam != null && ((String)apiParam.get("content_type")).equals("application/json")) {
//					Map<String,Object> json = adminMapper.getJsonTemplate(params);
//					out.put("apiJson", json);
//				}
//			}
//		}
//		
//		return out;
//	}
	
	public String extractIntentName(String intent) {
		if(StringUtils.isEmpty(intent)) {
			return "";
		}
		String respCode = "";
		try {
			respCode = intent.replace("\\(.*\\)$", "");
		}catch(Exception e) {
			respCode = intent;
		}
		return respCode;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public ChatbotRespInterfaceRelData getRespInfo(int agentSeq, int intentSeq, String intentName) throws Exception{
		logger.info("getRespInfo/{},{},{}",agentSeq, intentSeq, intentName);
		
		HashMap<String, Object> params = new HashMap<>();
		String respCode = extractIntentName(intentName);
		params.put("agentSeq", agentSeq);
		params.put("intentSeq", intentSeq);
		params.put("respCode", respCode);
		
		ChatbotRespInterfaceRelData respRel = null;
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithSdsInfo(params);
		}catch(Exception e) {
			logger.warn("getRespInfo/{}",e.getMessage());
			throw e;
		}
		return respRel;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public ChatbotRespInterfaceRelData getRespInfo(String domain, String respCode) throws Exception{
		logger.info("getRespInfo/{}",respCode);
		
		ChatbotRespInterfaceRelData respRel = null;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("domain", domain);
			params.put("respCode", respCode);
			respRel = chatbotMapper.getResponseInterfaceRelWithRespCode(params);
		}catch(Exception e) {
			logger.warn("getRespInfo/{}",e.getMessage());
			throw e;
		}
		return respRel;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public ChatbotRespInterfaceRelData getRespInfoByPos(String domain, int agentSeq, String pos,int posVal) throws Exception{
		logger.info("getRespInfoByPos/{},{},{},{}",domain, agentSeq, pos, posVal);
		
		ChatbotRespInterfaceRelData respRel = null;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("domain", domain);
			params.put("agentSeq", agentSeq);
			params.put("uiPos", pos);
			params.put("uiPosVal", posVal);
			respRel = chatbotMapper.getResponseInterfaceRelWithPos(params);
		}catch(Exception e) {
			logger.warn("getRespInfoByPos/{}",e.getMessage());
			throw e;
		}
		return respRel;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long insertRespInfoByPos(String domain, int agentSeq, String pos, int posVal, String respCode, String respMsg, String respTypeCd, String userId) throws Exception{
		logger.info("insertRespInfoByPos/{},{},{},{},{},{},{},{}",domain, agentSeq, pos, posVal, respCode, respMsg, respTypeCd, userId);
		long nRet = 0;
		
		//<-MOBIS get current rel info
		ChatbotRespInterfaceRelData currRel = null;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("domain", domain);
			params.put("agentSeq", agentSeq);
			params.put("uiPos", pos);
			params.put("uiPosVal", posVal);
			currRel = chatbotMapper.getResponseInterfaceRelWithPos(params);
		}catch(Exception e) {
			logger.warn("insertRespInfoByPos/can't get rel info. {}",e.getMessage());
			throw e;
		}
		//-> MOBIS
		try {
			
			if(respTypeCd.equals(IChatbotConst.RICH_CONTENT_TYPE_NONE)) {
				ChatbotRespInterfaceRelData respRel = new ChatbotRespInterfaceRelData();
				respRel.setDomain(domain);
				respRel.setAgentSeq(agentSeq);
				respRel.setUiPos(pos);
				respRel.setUiPosVal(posVal);
				respRel.setIntentSeq(0);
				
				nRet = chatbotMapper.deleteResponseInterfaceRelWithRespCode(respRel);
				logger.debug("insertRespInfoByPos/deleted for NONE type. deleted count: {}", nRet);
				return nRet;
			}
			
//			int ifId = 0;
			long rcId = 0;
			HashMap<String, Object> richParams = new HashMap<>();
			if(IChatbotConst.RICH_CONTENT_TYPE_API.equals(respTypeCd)) {
				//not insert.
				rcId = 0;
			}else {
				//QQQ get rich content id or generate id
				
				//MOBIS if currRel exist with Rich Content -> skip insert
				if(currRel == null) {
					richParams.put("contentTypeCd", respTypeCd);
					richParams.put("creatorId", userId);
					richParams.put("updatorId", userId);
					nRet = chatbotMapper.insertRichContent(richParams);
					
					rcId = (long)richParams.get("rcId");
				}else {
					rcId = currRel.getIfId();
				}
			}
			
			
			ChatbotRespInterfaceRelData respRel = new ChatbotRespInterfaceRelData();
			respRel.setDomain(domain);
			respRel.setAgentSeq(agentSeq);
			respRel.setUiPos(pos);
			respRel.setUiPosVal(posVal);
			respRel.setRespCode(respCode);
			respRel.setRespMsg(respMsg);
			respRel.setCreatorId(userId);
			respRel.setUpdatorId(userId);
			respRel.setRespTypeCd(respTypeCd);
			respRel.setIfId((int)rcId);
			
			logger.debug("insertRespInfoByPos/insert new rel:{}",respRel.toString());
			nRet = chatbotMapper.insertResponseInterfaceRelWithRespCode(respRel);
			
		}catch(Exception e) {
			logger.warn("insertRespInfoByPos/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long insertRespInfoByIntent(String domain, int intentSeq, String respCode, String respMsg, String respTypeCd, String userId) throws Exception{
		logger.info("insertRespInfoByIntent/{},{},{},{},{},{}",domain, intentSeq, respCode, respMsg, respTypeCd, userId);
		long nRet = 0;
		try {
			int agentSeq = 0;
			String uiPos = "";
			int uiPosVal = 0;
			
			if(respTypeCd.equals(IChatbotConst.RICH_CONTENT_TYPE_NONE)) {
				ChatbotRespInterfaceRelData respRel = new ChatbotRespInterfaceRelData();
				respRel.setDomain(domain);
				respRel.setAgentSeq(0);
				respRel.setUiPos("");
				respRel.setUiPosVal(0);
				respRel.setIntentSeq(intentSeq);
				
				nRet = chatbotMapper.deleteResponseInterfaceRelWithRespCode(respRel);
				logger.debug("insertRespInfoByIntent/deleted for NONE type. deleted count: {}", nRet);
				return nRet;
			}
			
//			int ifId = 0;
			long rcId = 0;
			HashMap<String, Object> richParams = new HashMap<>();
			if(IChatbotConst.RICH_CONTENT_TYPE_API.equals(respTypeCd)) {
				//not insert.
				rcId = 0;
			}else {
				//QQQ get rich content id or generate id
				
				richParams.put("contentTypeCd", respTypeCd);
				richParams.put("creatorId", userId);
				richParams.put("updatorId", userId);
				nRet = chatbotMapper.insertRichContent(richParams);
				
				rcId = (long)richParams.get("rcId"); 
			}
			
			
			ChatbotRespInterfaceRelData respRel = new ChatbotRespInterfaceRelData();
			respRel.setDomain(domain);
			respRel.setAgentSeq(agentSeq);
			respRel.setUiPos(uiPos);
			respRel.setUiPosVal(uiPosVal);
			respRel.setRespCode(respCode);
			respRel.setRespMsg(respMsg);
			respRel.setCreatorId(userId);
			respRel.setUpdatorId(userId);
			respRel.setRespTypeCd(respTypeCd);
			respRel.setIfId((int)rcId);
			respRel.setIntentSeq(intentSeq);
			
			nRet = chatbotMapper.insertResponseInterfaceRelWithRespCode(respRel);
			
		}catch(Exception e) {
			logger.warn("insertRespInfoByIntent/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
//	@Transactional(transactionManager="chatbotTransactionManager")
//	public ChatbotRichContent getRichContent(int agentSeq, int intentSeq, String intentName) throws Exception{
//		logger.info("getRichContent/{},{},{}",agentSeq, intentSeq, intentName);
//		HashMap<String, Object> params = new HashMap<>();
//		String respCode = extractIntentName(intentName);
//		params.put("agentSeq", agentSeq);
//		params.put("intentSeq", intentSeq);
//		params.put("respCode", respCode);
//		
//		ChatbotRichContent richContent = null;
//		ChatbotRespInterfaceRelData respRel = null;
//		try {
//			respRel = chatbotMapper.getResponseInterfaceRelWithSdsInfo(params);
//			if(respRel != null) {
//				String type = respRel.getRespTypeCd();
//				
//				switch(type) {
//				case IChatbotConst.RICH_CONTENT_TYPE_BUTTON:
//					richContent = rich.getRichConent(respRel.getIfId());
//					if(richContent != null) {
//						List<ChatbotButton> btns = rich.getButtonList(respRel.getIfId());
//						richContent.getButtons().addAll(btns);
//					}
//					break;
//				case IChatbotConst.RICH_CONTENT_TYPE_IMAGE:
//					richContent = rich.getRichConent(respRel.getIfId());
//					if(richContent != null) {
//						ChatbotRichImage img = rich.getRichImage(respRel.getIfId());
//						richContent.setImage(img);
//					}
//					break;
//				case IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL:
//					richContent = rich.getRichConent(respRel.getIfId());
//					if(richContent != null) {
//						List<ChatbotRichCarousel> list = rich.getRichCarousel(respRel.getIfId());
//						richContent.getCarousels().addAll(list);
//					}
//					break;
//				case IChatbotConst.RICH_CONTENT_TYPE_API:
//	//				ChatbotApiInfo apiInfo = chatbotMapper.getApiInfo(respRel.getIfId());
//	//				if(apiInfo != null) {
//	//					List<ChatbotApiParam> pars = chatbotMapper.getParams(respRel.getIfId());
//	//					apiInfo.getParamList().addAll(pars);
//	//					ChatbotApiJsonTemplate tpl = chatbotMapper.getJsonTemplate(respRel.getIfId());
//	//					apiInfo.setJsonTemplate(tpl);
//	//				}
//	//				break;
//				default:
//					throw new Exception("Invalid Rich Content type " + type);
//				}
//			}
//		}catch(Exception e) {
//			logger.warn("getApiInfo/{}",e.getMessage());
//			throw e;
//		}
//		return richContent;
//	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public ChatbotRichContent getRichContent(String respTypeCd, String domain, int ifId) throws Exception{
		logger.info("getRichContent/{},{},{}",respTypeCd, domain, ifId);
		HashMap<String, Object> params = new HashMap<>();
		params.put("respTypeCd", respTypeCd);
		params.put("ifId", ifId);
		
		ChatbotRichContent richContent = null;
		try {
			switch(respTypeCd) {
			case IChatbotConst.RICH_CONTENT_TYPE_BUTTON:
				richContent = rich.getRichConent(ifId);
				if(richContent != null) {
					List<ChatbotButton> btns = rich.getButtonList(ifId);
					richContent.getButtons().addAll(btns);
				}
				break;
			case IChatbotConst.RICH_CONTENT_TYPE_IMAGE:
				richContent = rich.getRichConent(ifId);
				if(richContent != null) {
					ChatbotRichImage img = rich.getRichImage(ifId);
					//transfer image path => url
					if(img != null) {
						img.setImagePath(imgUtils.transferImagePath(IChatbotConst.RICH_CONTENT_TYPE_IMAGE, domain, img.getImagePath()));
					}
					richContent.setImage(img);
				}
				break;
			case IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL:
				richContent = rich.getRichConent(ifId);
				if(richContent != null) {
					List<ChatbotRichCarousel> list = rich.getRichCarousel(ifId);
					richContent.getCarousels().addAll(list);
					//transfer image path
					if(list != null) {
						for(ChatbotRichCarousel ca: list) {
							ca.setImagePath(imgUtils.transferImagePath(IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL, domain, ca.getImagePath()));
						}
					}
				}
				break;
			case IChatbotConst.RICH_CONTENT_TYPE_API:
//				ChatbotApiInfo apiInfo = chatbotMapper.getApiInfo(respRel.getIfId());
//				if(apiInfo != null) {
//					List<ChatbotApiParam> pars = chatbotMapper.getParams(respRel.getIfId());
//					apiInfo.getParamList().addAll(pars);
//					ChatbotApiJsonTemplate tpl = chatbotMapper.getJsonTemplate(respRel.getIfId());
//					apiInfo.setJsonTemplate(tpl);
//				}
//				break;
			default:
				throw new Exception("Invalid Rich Content type " + respTypeCd);
			}
		}catch(Exception e) {
			logger.warn("getApiInfo/{}",e.getMessage());
			throw e;
		}
		return richContent;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public ChatbotRichContent getRichContent(String domain, String respCode) throws Exception{
		logger.info("getRichContent/{},{}",domain, respCode);
		
		ChatbotRichContent richContent = null;
		ChatbotRespInterfaceRelData respRel = null;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("domain", domain);
			params.put("respCode", respCode);
			respRel = chatbotMapper.getResponseInterfaceRelWithRespCode(params);
			if(respRel != null) {
				String type = respRel.getRespTypeCd();
				
				switch(type) {
				case IChatbotConst.RICH_CONTENT_TYPE_BUTTON:
					richContent = rich.getRichConent(respRel.getIfId());
					if(richContent != null) {
						List<ChatbotButton> btns = rich.getButtonList(respRel.getIfId());
						richContent.getButtons().addAll(btns);
					}
					break;
				case IChatbotConst.RICH_CONTENT_TYPE_IMAGE:
					richContent = rich.getRichConent(respRel.getIfId());
					if(richContent != null) {
						ChatbotRichImage img = rich.getRichImage(respRel.getIfId());
						richContent.setImage(img);
					}
					break;
				case IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL:
					richContent = rich.getRichConent(respRel.getIfId());
					if(richContent != null) {
						List<ChatbotRichCarousel> list = rich.getRichCarousel(respRel.getIfId());
						richContent.getCarousels().addAll(list);
					}
					break;
				case IChatbotConst.RICH_CONTENT_TYPE_API:
	//				ChatbotApiInfo apiInfo = chatbotMapper.getApiInfo(respRel.getIfId());
	//				if(apiInfo != null) {
	//					List<ChatbotApiParam> pars = chatbotMapper.getParams(respRel.getIfId());
	//					apiInfo.getParamList().addAll(pars);
	//					ChatbotApiJsonTemplate tpl = chatbotMapper.getJsonTemplate(respRel.getIfId());
	//					apiInfo.setJsonTemplate(tpl);
	//				}
	//				break;
				default:
					throw new Exception("Invalid Rich Content type " + type);
				}
			}
		}catch(Exception e) {
			logger.warn("getApiInfo/{}",e.getMessage());
			throw e;
		}
		return richContent;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public ChatbotApiInfo getApiInfo(int agentSeq, int intentSeq, String intentName) throws Exception{
		logger.info("getApiInfo/{},{},{}",agentSeq, intentSeq, intentName);
		HashMap<String, Object> params = new HashMap<>();
		String respCode = extractIntentName(intentName);
		params.put("agentSeq", agentSeq);
		params.put("intentSeq", intentSeq);
		params.put("respCode", respCode);
		
		ChatbotApiInfo apiInfo = null;
		ChatbotRespInterfaceRelData respRel = null;
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithSdsInfo(params);
			if(respRel != null) {
				String type = respRel.getRespTypeCd();
				if(IChatbotConst.RICH_CONTENT_TYPE_API.equals(type)) {
					apiInfo = chatbotMapper.getApiInfo(respRel.getIfId());
					if(apiInfo != null) {
						List<ChatbotApiParam> pars = chatbotMapper.getParams(respRel.getIfId());
						apiInfo.getParamList().addAll(pars);
						ChatbotApiJsonTemplate tpl = chatbotMapper.getJsonTemplate(respRel.getIfId());
						apiInfo.setJsonTemplate(tpl);
					}
				}
			}
		}catch(Exception e) {
			logger.warn("getApiInfo/{}",e.getMessage());
			throw e;
		}
		return apiInfo;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public ChatbotApiInfo getApiInfo(String domain, String respCode) throws Exception{
		logger.info("getApiInfo/{},{}",domain, respCode);
		
		ChatbotApiInfo apiInfo = null;
		ChatbotRespInterfaceRelData respRel = null;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("domain", domain);
			params.put("respCode", respCode);
			respRel = chatbotMapper.getResponseInterfaceRelWithRespCode(params);
			if(respRel != null) {
				String type = respRel.getRespTypeCd();
				if(IChatbotConst.RICH_CONTENT_TYPE_API.equals(type)) {
					apiInfo = chatbotMapper.getApiInfo(respRel.getIfId());
					if(apiInfo != null) {
						List<ChatbotApiParam> pars = chatbotMapper.getParams(respRel.getIfId());
						apiInfo.getParamList().addAll(pars);
						ChatbotApiJsonTemplate tpl = chatbotMapper.getJsonTemplate(respRel.getIfId());
						apiInfo.setJsonTemplate(tpl);
					}
				}
			}
		}catch(Exception e) {
			logger.warn("getApiInfo/{}",e.getMessage());
			throw e;
		}
		return apiInfo;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public ChatbotApiInfo getApiInfo(String respTypeCd, int ifId) throws Exception{
		logger.info("getApiInfo/{},{}",respTypeCd, ifId);
		
		ChatbotApiInfo apiInfo = null;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("respTypeCd", respTypeCd);
			params.put("if_id", ifId);
			if(IChatbotConst.RICH_CONTENT_TYPE_API.equals(respTypeCd)) {
				apiInfo = chatbotMapper.getApiInfo(ifId);
				if(apiInfo != null) {
					List<ChatbotApiParam> pars = chatbotMapper.getParams(ifId);
					apiInfo.getParamList().addAll(pars);
					ChatbotApiJsonTemplate tpl = chatbotMapper.getJsonTemplate(ifId);
					apiInfo.setJsonTemplate(tpl);
				}
			}
		}catch(Exception e) {
			logger.warn("getApiInfo/{}",e.getMessage());
			throw e;
		}
		return apiInfo;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public BaseListObject<ChatbotApiInfoDisp> getApiList(int workspaceId) throws Exception{
		logger.info("getApiList/{}",workspaceId);
		
		BaseListObject<ChatbotApiInfoDisp> out = new BaseListObject<>();
		List<ChatbotApiInfoDisp> list = null;
		long tot = 0;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("workspaceId", workspaceId);
			//all data
			list = chatbotMapper.getApiList(params);
			tot = chatbotMapper.getApiListCount(params);
			out.setTotalCount(tot);
			out.setList(list);
		}catch(Exception e) {
			logger.warn("getApiList/{}",e.getMessage());
			throw e;
		}
		return out;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long setApi(String domain, int agentSeq, String uiPos, int uiPosVal, int ifId, String userId) throws Exception{
		logger.info("setApi/{},{},{},{},{},{}",domain,agentSeq,uiPos,uiPosVal,ifId,userId);
		
		long nRet = 0;
		try {
			ChatbotRespInterfaceRelData param = new ChatbotRespInterfaceRelData();
			param.setIfId(ifId);
			param.setAgentSeq(agentSeq);
			param.setDomain(domain);
			param.setUiPos(uiPos);
			param.setUiPosVal(uiPosVal);
			param.setUpdatorId(userId);
			
			nRet = chatbotMapper.updateResponseInterfaceRelWithIfidPos(param);
		}catch(Exception e) {
			logger.warn("setApi/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long setApiIntent(String domain, int agentSeq, int intentSeq, int ifId, String userId) throws Exception{
		logger.info("setApiIntent/{},{},{},{},{},{}",domain,agentSeq,intentSeq,ifId,userId);
		
		long nRet = 0;
		try {
			ChatbotRespInterfaceRelData param = new ChatbotRespInterfaceRelData();
			param.setIfId(ifId);
			param.setAgentSeq(agentSeq);
			param.setDomain(domain);
			param.setUiPos("");
			param.setUiPosVal(0);
			param.setUpdatorId(userId);
			param.setIntentSeq(intentSeq);
			
			nRet = chatbotMapper.updateResponseInterfaceRelWithIfidIntent(param);
		}catch(Exception e) {
			logger.warn("setApi/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long deleteSingleButton(String domain, int agentSeq, int intentSeq, int orderNo) throws Exception{
		logger.info("deleteSingleButton/{},{},{}",agentSeq, intentSeq, orderNo);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("agentSeq", agentSeq);
		params.put("intentSeq", intentSeq);
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithIntent(params);
			if(respRel != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_BUTTON.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type " + respRel.getRespTypeCd());
				}
				params.clear();
				params.put("rcId", respRel.getIfId());
				params.put("btnOrder",orderNo);
				
				nRet = chatbotMapper.deleteSingleButton(params);
				logger.debug("deleted button count:{}", nRet);
			}
		}catch(Exception e) {
			logger.warn("deleteSingleButton/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long deleteSingleButtonPos(String domain, int agentSeq, String uiPos, int uiPosVal, int btnOrder) throws Exception{
		logger.info("deleteSingleButtonPos/{},{},{},{},{}",domain,agentSeq, uiPos, uiPosVal, btnOrder);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("agentSeq", agentSeq);
		params.put("uiPos", uiPos);
		params.put("uiPosVal", uiPosVal);
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithPos(params);
			if(respRel != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_BUTTON.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type " + respRel.getRespTypeCd());
				}
				params.clear();
				params.put("rcId", respRel.getIfId());
				params.put("btnOrder",btnOrder);
				
				nRet = chatbotMapper.deleteSingleButton(params);
				logger.debug("deleted button count:{}", nRet);
			}
		}catch(Exception e) {
			logger.warn("deleteSingleButtonPos/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long deleteCarouselPos(String domain, int agentSeq, String uiPos, int uiPosVal, int carouselOrder) throws Exception{
		logger.info("deleteCarouselPos/{},{},{},{},{}",domain,agentSeq, uiPos, uiPosVal, carouselOrder);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("agentSeq", agentSeq);
		params.put("uiPos", uiPos);
		params.put("uiPosVal", uiPosVal);
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithPos(params);
			if(respRel != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type " + respRel.getRespTypeCd());
				}
				params.clear();
				params.put("rcId", respRel.getIfId());
				params.put("carouselOrder",carouselOrder);
				
				nRet = chatbotMapper.deleteCarouselByPos(params);
				logger.debug("deleted carousel count:{}", nRet);
			}
		}catch(Exception e) {
			logger.warn("deleteCarouselPos/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long deleteCarouselIntent(String domain, int agentSeq, int intentSeq, int carouselOrder) throws Exception{
		logger.info("deleteCarouselIntent/{},{},{},{}",domain,agentSeq, intentSeq, carouselOrder);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("agentSeq", agentSeq);
		params.put("agentSeq", agentSeq);
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithIntent(params);
			if(respRel != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type " + respRel.getRespTypeCd());
				}
				params.clear();
				params.put("rcId", respRel.getIfId());
				params.put("carouselOrder",carouselOrder);
				
				nRet = chatbotMapper.deleteCarouselByPos(params);
				logger.debug("deleted carousel count:{}", nRet);
			}
		}catch(Exception e) {
			logger.warn("deleteCarouselIntent/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long updateSingleButton(String domain, int agentSeq, int intentSeq, int orderNo, String title, String utter, String updatorId) throws Exception{
		logger.info("updateSingleButton/{},{},{},{},{},{},{}",domain, agentSeq, intentSeq, orderNo, title, utter, updatorId);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("agentSeq", agentSeq);
		params.put("intentSeq", intentSeq);
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithIntent(params);
			if(respRel != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_BUTTON.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type " + respRel.getRespTypeCd());
				}
				params.clear();
				params.put("rcId", respRel.getIfId());
				params.put("btnOrder",orderNo);
				params.put("title", title);
				params.put("userUtter", utter);
				params.put("updatorId", updatorId);
				
				nRet = chatbotMapper.updateSingleButton(params);
				logger.debug("updated button count:{}", nRet);
			}
		}catch(Exception e) {
			logger.warn("updateSingleButton/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long addSingleButton(String domain, int agentSeq, int intentSeq, String title, String utter, String updatorId) throws Exception{
		logger.info("addSingleButton/{},{},{},{},{},{}",domain, agentSeq, intentSeq, title, utter, updatorId);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("agentSeq", agentSeq);
		params.put("intentSeq", intentSeq);
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithIntent(params);
			if(respRel != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_BUTTON.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type " + respRel.getRespTypeCd());
				}
				params.clear();
				params.put("rcId", respRel.getIfId());
				params.put("title", title);
				params.put("userUtter", utter);
				params.put("btnOrder", 1);//dummy
				params.put("creatorId", updatorId);
				params.put("updatorId", updatorId);
				
				nRet = chatbotMapper.addSingleButton(params);
				logger.debug("updated button count:{}", nRet);
			}
		}catch(Exception e) {
			logger.warn("updateSingleButton/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long addSingleButtonById(int relId, String title, String utter, String updatorId) throws Exception{
		logger.info("updateSingleButton/{},{},{},{}",relId, title, utter, updatorId);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("relId", relId);
		
		try {
			respRel = chatbotMapper.getResponseInterfaceRelById(params);
			if(respRel != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_BUTTON.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type " + respRel.getRespTypeCd());
				}
				params.clear();
				params.put("rcId", respRel.getIfId());
				params.put("title", title);
				params.put("userUtter", utter);
				params.put("creatorId", updatorId);
				params.put("updatorId", updatorId);
				
				nRet = chatbotMapper.addSingleButton(params);
				logger.debug("updated button count:{}", nRet);
			}
		}catch(Exception e) {
			logger.warn("updateSingleButton/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long addSingleButtonPos(String domain, int agentSeq, String uiPos, int uiPosVal, int btnOrder, String title, String utter, String updatorId) throws Exception{
		logger.info("addSingleButtonPos/{},{},{},{},{},{},{},{}",domain, agentSeq, uiPos, uiPosVal, btnOrder, title, utter, updatorId);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("agentSeq", agentSeq);
		params.put("uiPos", uiPos);
		params.put("uiPosVal", uiPosVal);
		
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithPos(params);
			if(respRel != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_BUTTON.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type " + respRel.getRespTypeCd());
				}
				params.clear();
				params.put("rcId", respRel.getIfId());
				params.put("title", title);
				params.put("userUtter", utter);
				if(btnOrder > 0) {
					params.put("btnOrder", btnOrder);//dummy value
				}
				params.put("creatorId", updatorId);
				params.put("updatorId", updatorId);
				if(btnOrder > 0) {
					nRet = chatbotMapper.updateSingleButton(params);
					logger.debug("updated button count:{}", nRet);
				}else {
					nRet = chatbotMapper.addSingleButton(params);
					logger.debug("added button count:{}", nRet);
				}
				
			}
		}catch(Exception e) {
			logger.warn("updateSingleButton/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long addImagePos(String domain, int agentSeq, String uiPos, int uiPosVal, String imgLink, String userId, HttpServletRequest request) throws Exception{
		logger.info("addImagePos/{},{},{},{},{},{}",domain, agentSeq, uiPos, uiPosVal, imgLink, userId);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("agentSeq", agentSeq);
		params.put("uiPos", uiPos);
		params.put("uiPosVal", uiPosVal);
		
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithPos(params);
			if(respRel != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_IMAGE.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type " + respRel.getRespTypeCd());
				}
				
				//returns filename only (not path)
				String savedFile = imgUtils.SaveFile(request, domain, respRel.getRespTypeCd(), respRel.getIfId(), 0);
				
				params.clear();
				params.put("rcId", respRel.getIfId());
				params.put("imagePath", savedFile);
				params.put("imageText", null);		// Default Value
				params.put("linkUrl", imgLink);
				params.put("creatorId", userId);
				params.put("updatorId", userId);
				
				nRet = chatbotMapper.insertRichImage(params);
				logger.debug("updated image count:{}", nRet);
				
			}
		}catch(Exception e) {
			logger.warn("addImagePos/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long addImageIntent(String domain, int agentSeq, int intentSeq, String imgLink, String userId, HttpServletRequest request) throws Exception{
		logger.info("addImageIntent/{},{},{},{},{}",domain, agentSeq, intentSeq, imgLink, userId);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("agentSeq", agentSeq);
		params.put("intentSeq", intentSeq);
		
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithIntent(params);
			if(respRel != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_IMAGE.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type " + respRel.getRespTypeCd());
				}
				
				//returns filename only (not path)
				String savedFile = imgUtils.SaveFile(request, domain, respRel.getRespTypeCd(), respRel.getIfId(), 0);
				
				params.clear();
				params.put("rcId", respRel.getIfId());
				params.put("imagePath", savedFile);
				params.put("imageText", null);		// Default Value
				params.put("linkUrl", imgLink);
				params.put("creatorId", userId);
				params.put("updatorId", userId);
				
				nRet = chatbotMapper.insertRichImage(params);
				logger.debug("updated image count:{}", nRet);
				
			}
		}catch(Exception e) {
			logger.warn("addImageIntent/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	public String generateRespCode(String domain, int agentSeq) {
		Random r = new Random();
		int idx = r.nextInt(999);
		return domain + "_" + agentSeq+ "_" + idx;
	}
	
	public String generateRespCodeWIntent(String domain, int intentSeq) {
		Random r = new Random();
		int idx = r.nextInt(9999);
		return domain + "_0_" + intentSeq + "_" + idx;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long addCarouselPos(String domain, int agentSeq, String uiPos, int uiPosVal, String imgLink, String userId, HttpServletRequest request) throws Exception{
		logger.info("addCarouselPos/{},{},{},{},{},{}",domain, agentSeq, uiPos, uiPosVal, imgLink, userId);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("agentSeq", agentSeq);
		params.put("uiPos", uiPos);
		params.put("uiPosVal", uiPosVal);
		
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithPos(params);
			if(respRel != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type " + respRel.getRespTypeCd());
				}
				
				//long cnt = chatbotMapper.getCarouselCount(respRel.getIfId());
				
				//returns filename only (not path)
				List<String> savedFiles = imgUtils.SaveFileMulti(request, domain, respRel.getRespTypeCd(), respRel.getIfId());
				MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
				Map<String, String[]> multiParams = multipartHttpServletRequest.getParameterMap();
				String[] titles = multiParams.get("title");
				String[] subtitles = multiParams.get("subtitle");
				String[] utters = multiParams.get("utter");
				String[] orders = multiParams.get("carousel_order");
				String[] hasfiles = multiParams.get("hasfile");
				
				ArrayList<Integer> emptyIndex = new ArrayList<>();
				if(hasfiles != null) {
					for(int i=0;i<hasfiles.length;i++) {
						if(hasfiles[i]=="0") {
							emptyIndex.add(i);
						}
					}
					if(emptyIndex.size() > 0) {
						for(Integer idx:emptyIndex) {
							savedFiles.add(idx,"");
						}
					}
				}
				
//				if(cnt != titles.length) {
//					cnt = chatbotMapper.deleteCarouselById(respRel.getIfId());
//					logger.debug("reset carousel, cnt:{}",cnt);
//				}
				
				params.clear();
				if(savedFiles != null) {
					for(int idx = 0;idx < titles.length; idx++) {
						params.put("rcId", respRel.getIfId());
						params.put("carouselOrder", orders[idx]);
						try {
							params.put("imagePath", savedFiles.get(idx));
						}catch(Exception e) {
							params.put("imagePath", "");
						}
						params.put("title", titles[idx]);
						params.put("subTitle", subtitles[idx]);
						params.put("userUtter", utters[idx]);
						params.put("creatorId", userId);
						params.put("updatorId", userId);
						
						if(StrUtils.getNum(orders[idx], 0) > 0) {
							nRet += chatbotMapper.updateSingleCarousel(params);
						}else {
							nRet += chatbotMapper.insertRichCarousel(params);
						}
						logger.debug("updated carousel count:{}", nRet);
					}
				}
			}
		}catch(Exception e) {
			logger.warn("addCarouselPos/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long addCarouselIntent(String domain, int agentSeq, int intentSeq, String imgLink, String userId, HttpServletRequest request) throws Exception{
		logger.info("addCarouselIntent/{},{},{},{},{}",domain, agentSeq, intentSeq, imgLink, userId);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("agentSeq", agentSeq);
		params.put("intentSeq", intentSeq);
		
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithIntent(params);
			if(respRel != null) {
				if(!IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type " + respRel.getRespTypeCd());
				}
				
				//long cnt = chatbotMapper.getCarouselCount(respRel.getIfId());
				
				//returns filename only (not path)
				List<String> savedFiles = imgUtils.SaveFileMulti(request, domain, respRel.getRespTypeCd(), respRel.getIfId());
				MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
				Map<String, String[]> multiParams = multipartHttpServletRequest.getParameterMap();
				String[] titles = multiParams.get("title");
				String[] subtitles = multiParams.get("subtitle");
				String[] utters = multiParams.get("utter");
				String[] orders = multiParams.get("carousel_order");
				String[] hasfiles = multiParams.get("hasfile");
				
				ArrayList<Integer> emptyIndex = new ArrayList<>();
				if(hasfiles != null) {
					for(int i=0;i<hasfiles.length;i++) {
						if(hasfiles[i]=="0") {
							emptyIndex.add(i);
						}
					}
					if(emptyIndex.size() > 0) {
						for(Integer idx:emptyIndex) {
							savedFiles.add(idx,"");
						}
					}
				}
				
//				if(cnt != titles.length) {
//					cnt = chatbotMapper.deleteCarouselById(respRel.getIfId());
//					logger.debug("reset carousel, cnt:{}",cnt);
//				}
				
				params.clear();
				if(savedFiles != null) {
					for(int idx = 0;idx < titles.length; idx++) {
						params.put("rcId", respRel.getIfId());
						params.put("carouselOrder", orders[idx]);
						try {
							params.put("imagePath", savedFiles.get(idx));
						}catch(Exception e) {
							params.put("imagePath", "");
						}
						params.put("title", titles[idx]);
						params.put("subTitle", subtitles[idx]);
						params.put("userUtter", utters[idx]);
						params.put("creatorId", userId);
						params.put("updatorId", userId);
						
						if(StrUtils.getNum(orders[idx], 0) > 0) {
							nRet += chatbotMapper.updateSingleCarousel(params);
						}else {
							nRet += chatbotMapper.insertRichCarousel(params);
						}
						logger.debug("updated carousel count:{}", nRet);
					}
				}
			}
		}catch(Exception e) {
			logger.warn("addCarouselPos/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long deleteRichPos(String domain, int agentSeq, String uiPos, int uiPosVal, String richTypeCd) throws Exception{
		logger.info("deleteRichPos/{},{},{},{},{},{}",domain, agentSeq, uiPos, uiPosVal, richTypeCd);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("agentSeq", agentSeq);
		params.put("uiPos", uiPos);
		params.put("uiPosVal", uiPosVal);
		
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithPos(params);
			if(respRel != null) {
				if(!richTypeCd.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type: " + richTypeCd);
				}
				
				chatbotMapper.deleteRichContent(respRel.getIfId());
				
				params.clear();
				
				switch(richTypeCd) {
				case IChatbotConst.RICH_CONTENT_TYPE_BUTTON:
					nRet = chatbotMapper.deleteButtonById(respRel.getIfId());
					break;
				case IChatbotConst.RICH_CONTENT_TYPE_IMAGE:
					nRet = chatbotMapper.deleteImageById(respRel.getIfId());
					break;
				case IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL:
					nRet = chatbotMapper.deleteCarouselById(respRel.getIfId());
					break;
				case IChatbotConst.RICH_CONTENT_TYPE_API:
					//delete api는 없음.
					break;
				case IChatbotConst.RICH_CONTENT_TYPE_NONE:
					//just return
					return nRet;
				default:
					throw new Exception("Invalid rich content type: "+richTypeCd);
				}
				logger.debug("delete rich content count : {}, rcId: {} ", nRet, respRel.getIfId());
			}
		}catch(Exception e) {
			logger.warn("deleteRichPos/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public long deleteRichByIntent(String domain, int intentSeq, String richTypeCd) throws Exception{
		logger.info("deleteRichByIntent/{},{},{}",domain, intentSeq, richTypeCd);
		
		long nRet = 0;
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("intentSeq", intentSeq);
		
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithIntent(params);
			if(respRel != null) {
				if(!richTypeCd.equals(respRel.getRespTypeCd())) {
					throw new Exception("Invlid Rich Content Type: " + richTypeCd);
				}
				
				chatbotMapper.deleteRichContent(respRel.getIfId());
				
				params.clear();
				
				switch(richTypeCd) {
				case IChatbotConst.RICH_CONTENT_TYPE_BUTTON:
					nRet = chatbotMapper.deleteButtonById(respRel.getIfId());
					break;
				case IChatbotConst.RICH_CONTENT_TYPE_IMAGE:
					nRet = chatbotMapper.deleteImageById(respRel.getIfId());
					break;
				case IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL:
					nRet = chatbotMapper.deleteCarouselById(respRel.getIfId());
					break;
				case IChatbotConst.RICH_CONTENT_TYPE_API:
					//delete api는 없음.
					break;
				case IChatbotConst.RICH_CONTENT_TYPE_NONE:
					//just return
					return nRet;
				default:
					throw new Exception("Invalid rich content type: "+richTypeCd);
				}
				logger.debug("delete rich content count : {}, rcId: {} ", nRet, respRel.getIfId());
			}
		}catch(Exception e) {
			logger.warn("deleteRichByIntent/{}",e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	@Transactional(transactionManager="chatbotTransactionManager")
	public ChatbotRespInterfaceRelData getRespInfoByIntent(String domain, int intentSeq) throws Exception{
		logger.info("getRespInfoByIntent/{},{}",domain, intentSeq);
		
		ChatbotRespInterfaceRelData respRel = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("domain", domain);
		params.put("intentSeq", intentSeq);
		
		try {
			respRel = chatbotMapper.getResponseInterfaceRelWithIntent(params);
		}catch(Exception e) {
			logger.warn("getRespInfoByIntent/{}",e.getMessage());
			throw e;
		}
		return respRel;
	}
	
	public String getRichTableName(String richTypeCd) {
		switch(richTypeCd) {
		case IChatbotConst.RICH_CONTENT_TYPE_BUTTON:
			return "chatbot_rich_buttons_tb";
		case IChatbotConst.RICH_CONTENT_TYPE_IMAGE:
			return "chatbot_rich_image_tb"; 
		case IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL:
			return "chatbot_rich_carousel_tb";
		default:
			return "";
		}
	}
}
