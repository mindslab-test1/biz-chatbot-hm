package ai.mindslab.bizchatbot.sdsadmin.service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.sdsadmin.controller.data.SDSSettingReq;

@Service
public class ChatbotAdminService {

	private Logger logger = LoggerFactory.getLogger(ChatbotAdminService.class);
	
	public static final String UNIQID = "admin.0000000000";
	
	private SDSSettingReq storedAdminSetting;
	
	public void setSdsSetting(SDSSettingReq setting) {
		logger.info("setSdsSetting/{}",setting);
		this.storedAdminSetting = setting;
	}

	public SDSSettingReq getSdsSetting() {
		return this.storedAdminSetting;
	}
	
	public void updateSession(HttpServletRequest req) {
		req.getSession().setAttribute("USER_NAME", storedAdminSetting.getUserName());
		req.getSession().setAttribute("USER_SEQ", storedAdminSetting.getUserSeq());
		req.getSession().setAttribute("UNIQID", UNIQID);
		req.getSession().setAttribute("USER_TYPE", "SA");
		req.getSession().setAttribute("ProjectName", storedAdminSetting.getProjectName());
		req.getSession().setAttribute("ProjectNo", storedAdminSetting.getProjectNo());
	}
	
	public void updateCookie(HttpServletResponse resp) {
		eraseCookie(resp, "USER_NAME");
		resp.addCookie(makeCookie("USER_NAME", storedAdminSetting.getUserName()));
		eraseCookie(resp, "USER_SEQ");
		resp.addCookie(makeCookie("USER_SEQ", storedAdminSetting.getUserSeq()+""));
		eraseCookie(resp, "UNIQID");
		resp.addCookie(makeCookie("UNIQID", UNIQID));
		eraseCookie(resp, "USER_TYPE");
		resp.addCookie(makeCookie("USER_TYPE", storedAdminSetting.getUserType()));
		eraseCookie(resp, "ProjectName");
		resp.addCookie(makeCookie("ProjectName", storedAdminSetting.getProjectName()));
		eraseCookie(resp, "ProjectNo");
		resp.addCookie(makeCookie("ProjectNo", storedAdminSetting.getProjectNo()+""));
	}
	
	public void updateProject(String projectName, int projectSeq, HttpServletRequest req) {
		if(storedAdminSetting == null) {
			storedAdminSetting = new SDSSettingReq();
		}
		storedAdminSetting.setProjectName(projectName);
		storedAdminSetting.setProjectNo(projectSeq);
		
		req.getSession().setAttribute("ProjectName", storedAdminSetting.getProjectName());
		req.getSession().setAttribute("ProjectNo", storedAdminSetting.getProjectNo());
	}
	
	private Cookie makeCookie(String key, String value) {
		Cookie cookie = new Cookie(key, value);
		cookie.setPath("/");
		cookie.setMaxAge(3600);
		return cookie;
	}
	
	private void eraseCookie(HttpServletResponse resp, String key) {
		Cookie cookie = new Cookie(key, "");
		cookie.setPath("/");
		cookie.setMaxAge(0);
		resp.addCookie(cookie);

	}
}
