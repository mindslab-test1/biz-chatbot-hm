package ai.mindslab.bizchatbot.sdsadmin.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.session.data.SdsData;
import ai.mindslab.bizchatbot.sds.client.SdsTrainerClient;
import ai.mindslab.bizchatbot.sdsadmin.controller.data.SDSSettingReq;
import ai.mindslab.bizchatbot.sdsadmin.controller.data.SdsIntentUserSayReq;
import ai.mindslab.bizchatbot.sdsadmin.service.ChatbotAdminService;
import ai.mindslab.bizchatbot.sdsadmin.service.SdsService;
import ai.mindslab.bizchatbot.sdsadmin.util.HttpUtils;
import ai.mindslab.sds.dao.data.SdsProjectData;
import first.view.service.ViewServiceImpl;


@RestController
@RequestMapping(path = "/admin/sds")
public class SdsIntentFinderController {

	private Logger logger = LoggerFactory.getLogger(SdsIntentFinderController.class);
	
	@Autowired
	private SdsService sdsService;
	
	@Autowired
	private ChatbotAdminService adminService;
	
	@Autowired
	@Qualifier("viewService")
	private ViewServiceImpl viewService;
	
	@Value("${enginePath}")
	private String enginePath;
	
	@Value("${engineUrl")
	private String engineUrl;
	
	@Value("${sds.ip}")
	private String sdsIp;
	
	@Value("${sds.learn.port}")
	private String sdsPort;
	
	@RequestMapping(value="/insertCorpus" ,method = {RequestMethod.POST})
	public BaseResponse<String> getIntentUserSays(@RequestBody SdsIntentUserSayReq says, HttpServletRequest req) throws Exception{
		logger.info("/admin/sds/insertUserSay/{}",says);
		
		BaseResponse<String> resp = new BaseResponse<>();
		try {
			sdsService.insertItfCorpus(says.getUserId(), says.getDomain(), says.getIntentName(), says.getUserSays());
		}catch(Exception e) {
			resp.setCode(IRestCodes.ERR_CODE_FAILURE);
			resp.setMsg(e.getMessage());
		}
		
		return resp;
	}
	
	@RequestMapping(value = "/getproject",method = {RequestMethod.POST})
	public BaseResponse<SdsProjectData> gettest(@RequestParam(name="projectName", required=true)String projectName, HttpServletRequest req) throws Exception{
		logger.info("/admin/sds/gettest/{}",projectName);
	
		BaseResponse<SdsProjectData> resp = new BaseResponse<SdsProjectData>();
		try {
			SdsProjectData data = sdsService.getProject(projectName);
			resp.setData(data);
		}catch(Exception e) {
			resp.setCode(IRestCodes.ERR_CODE_FAILURE);
			resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
		}
		return resp;
	}
	
	@RequestMapping(value = "/dialogslearing",method = {RequestMethod.POST})
	public BaseResponse<String> dialoglearn(@RequestParam(name="projectName", required=true)String projectName, HttpServletRequest req) throws Exception{
		logger.info("/admin/sds/dialogslearing/{}",projectName);
	
		BaseResponse<String> resp = new BaseResponse<>();
		try {
			SDSSettingReq setting = adminService.getSdsSetting();
			if(setting == null) {
				throw new Exception("Sds-admin setting is null");
			}
			HashMap<String, Object> map = makeMap(setting);
			DialogsLearning(map, req);
			//<-- prompt Ready
			
//			Random random = new Random();
//			int rnd = random.nextInt(999);
//			
//			String userId = HttpUtils.getUserId(req);
//			
//			SdsData sdsData = new SdsData();
////			sdsData.setUserName(userId);
//			sdsData.setUserName("admin");
//			sdsData.setProjectName(projectName);
//			sdsData.setProjectPort(10000+rnd);//QQQ generate port -> hazelcast client need, but sds-api can't include this.
//			
//			SdsTrainerClient sdsTrainer = SdsTrainerClient.makeJob(sdsIp, sdsPort, sdsData);
//
//			Future<String> f = sdsTrainer.runLearningSds(5);
//			try {
//				String out = f.get();
//				logger.debug("learn {} result:", sdsData.getProjectName(), out);
//				resp.setData(out);
//			} catch (InterruptedException e) {
//				logger.debug("/admin/sds/dialogslearing interruped, {}",e.getMessage());
//				throw e;
//			} catch (ExecutionException e) {
//				logger.debug("/admin/sds/dialogslearing execution error, {}",e.getMessage());
//				throw e;
//			}
		}catch(Exception e) {
			logger.warn("/admin/sds/dialogslearing/{}",e.getMessage());
			resp.setCode(IRestCodes.ERR_CODE_FAILURE);
			resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
		}
		return resp;
	}
	
	@RequestMapping(value = "/itf_learn",method = {RequestMethod.POST})
	public @ResponseBody Callable<BaseResponse<String>> dialoglearnAsync(@RequestBody String projectName, HttpServletRequest req, HttpServletResponse response) throws Exception{
		logger.info("/admin/sds/itf_learn/{}",projectName);
	
		Callable<BaseResponse<String>> callable = new Callable<BaseResponse<String>>() {
			@Override
			public BaseResponse<String> call() throws Exception {
				BaseResponse<String> resp = new BaseResponse<>();
				try {
//					SDSSettingReq setting = adminService.getSdsSetting();
//					if(setting == null) {
//						throw new Exception("Sds-admin setting is null");
//					}
//					HashMap<String, Object> map = makeMap(setting);
					SdsProjectData prj = sdsService.getProject(projectName);
					HashMap<String, Object> map = new HashMap<>();
					map.put("PROJECT_NAME", prj.getProjectName());
					map.put("PROJECT_SEQ", prj.getSeqNo());
					
					DialogsLearning(map, req);
					//<-- prompt Ready
					
					Random random = new Random();
					int rnd = random.nextInt(999);
					
					String userId = HttpUtils.getUserId(req);
					
					SdsData sdsData = new SdsData();
//					sdsData.setUserName(userId);
					sdsData.setUserName("admin");
					sdsData.setProjectName(projectName);
					sdsData.setProjectPort(10000+rnd);//QQQ generate port -> hazelcast client need, but sds-api can't include this.
					
					logger.debug("make Train Job. {}:{}, {}",sdsIp, sdsPort, sdsData);
					SdsTrainerClient sdsTrainer = SdsTrainerClient.makeJob(sdsIp, sdsPort, sdsData);

					String wholeLog = sdsTrainer.runLearingSdsBlocking(20);
					resp.setData(wholeLog);
				}catch(Exception e) {
					logger.warn("/admin/sds/itf_learn/{}",e.getMessage());
					resp.setCode(IRestCodes.ERR_CODE_FAILURE);
					resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
				}
				return resp;
			}
			
		};
		return callable;
	}
	
	private HashMap<String, Object> makeMap(SDSSettingReq setting){
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("PROJECT_NAME", setting.getProjectName());
		map.put("PROJECT_SEQ", setting.getProjectNo());
		
		return map;
	}
	
	public Boolean DialogsLearning(Map<String, Object> map, HttpServletRequest request) throws Exception {	
		String loc = "/admin/sds/dialogslearing";
		logger.info("{}/DialogsLearining/START",loc);
		String Path = enginePath + "/dialog_domain/" + map.get("PROJECT_NAME").toString() + "/" + map.get("PROJECT_NAME").toString();
		String Cmdpath = enginePath + "/dialog_domain/" + map.get("PROJECT_NAME").toString();
		
		try {
			logger.info("{}/DialogsLearining/{}",loc,"LearnTask");
			viewService.LeanTask(map, Path);
			logger.info("{}/DialogsLearining/{}",loc,"LeanSlot");
			viewService.LeanSlot(map, Path);
			logger.info("{}/DialogsLearining/{}",loc,"LeanDAType");
			viewService.LeanDAType(map, Path);
			logger.info("{}/DialogsLearining/{}",loc,"LeanDialogLib");
			viewService.LeanDialogLib(map, Path);
			logger.info("{}/DialogsLearining/{}",loc,"LeanDomain");
			viewService.LeanDomain(map, Path);
			logger.info("{}/DialogsLearining/{}",loc,"CreateSql");
			viewService.CreateSql(map, Path, request);
			logger.info("{}/DialogsLearining/{}",loc,"Ready");
		}catch(Exception e) {
//			logger.warn("{}/admin/sds/dialogslearing/{}",loc,e.getMessage());
			throw e;
		}
		return true;
	}

}
