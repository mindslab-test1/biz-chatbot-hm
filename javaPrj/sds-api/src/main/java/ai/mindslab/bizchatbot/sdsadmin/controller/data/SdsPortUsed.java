package ai.mindslab.bizchatbot.sdsadmin.controller.data;

import java.io.Serializable;

public class SdsPortUsed implements Serializable{
 
	private static final long serialVersionUID = 1L;
	private long timestamp = 0L;
	private int port;
	public SdsPortUsed() {
		
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	@Override
	public String toString() {
		return "SdsPortUsed [timestamp=" + timestamp + ", port=" + port + "]";
	}
}
