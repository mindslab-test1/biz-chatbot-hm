package ai.mindslab.bizchatbot.sdsadmin.controller.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SDSSettingReq implements Serializable{

	private String projectName;
	private int projectNo;
	private String userName;
	private int userSeq;
	private String userType = "SA";
	private String uniqid;
	
	public SDSSettingReq() {
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public int getProjectNo() {
		return projectNo;
	}

	public void setProjectNo(int projectNo) {
		this.projectNo = projectNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getUserSeq() {
		return userSeq;
	}

	public void setUserSeq(int userSeq) {
		this.userSeq = userSeq;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUniqid() {
		return uniqid;
	}

	public void setUniqid(String uniqid) {
		this.uniqid = uniqid;
	}

	@Override
	public String toString() {
		return "SDSSettingReq [projectName=" + projectName + ", projectNo=" + projectNo + ", userName=" + userName
				+ ", userSeq=" + userSeq + ", userType=" + userType + ", uniqid=" + uniqid + "]";
	}

}
