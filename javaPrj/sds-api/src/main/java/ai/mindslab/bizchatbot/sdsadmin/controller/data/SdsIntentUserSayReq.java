package ai.mindslab.bizchatbot.sdsadmin.controller.data;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class SdsIntentUserSayReq implements Serializable{

	private String userId;
	private String domain;
	private String intentName;
	private ArrayList<String> userSays;
	
	public SdsIntentUserSayReq() {
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getIntentName() {
		return intentName;
	}

	public void setIntentName(String intentName) {
		this.intentName = intentName;
	}

	public ArrayList<String> getUserSays() {
		if(userSays == null) {
			userSays = new ArrayList<String>();
		}
		return userSays;
	}

	public void setUserSays(ArrayList<String> userSays) {
		this.userSays = userSays;
	}

	@Override
	public String toString() {
		return "SdsIntentUserSayReq [userId=" + userId + ", domain=" + domain + ", intentName=" + intentName
				+ ", userSays=" + userSays + "]";
	}
}
