package ai.mindslab.bizchatbot.sdsadmin.controller;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.SocketUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfoDisp;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRespInterfaceRelData;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichApi;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseListObject;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.utils.StrUtils;
import ai.mindslab.bizchatbot.sdsadmin.controller.data.SdsPortUsed;
import ai.mindslab.bizchatbot.sdsadmin.service.SdsAdminService;
import ai.mindslab.bizchatbot.sdsadmin.util.HttpUtils;
import ai.mindslab.bizchatbot.sdsadmin.util.ImageUtils;
import ai.mindslab.sds.dao.data.SdsProjectData;
import first.common.common.CommandMap;
import first.view.service.ViewServiceImpl;

@RestController
@RequestMapping(path="/admin/api")
public class SdsApiController {

	private Logger logger = LoggerFactory.getLogger(SdsApiController.class);
	
	private ConcurrentHashMap<Integer, SdsPortUsed> mapPort = new ConcurrentHashMap<>(1000);
	
	@Value("${rich.image.path}")
	private String imageRoot;
	
	@Autowired
	@Qualifier("viewService")
	private ViewServiceImpl viewService;
	
	@Autowired
	@Qualifier("imageUtils")
	private ImageUtils imageUtils;
	
	@Autowired
	private SdsAdminService sdsAdminService;
	
	@RequestMapping(value="/projectlist", method = {RequestMethod.POST})
	public BaseResponse<?> changeSetting(HttpServletRequest req, HttpServletResponse resp) throws Exception{
		logger.info("/admin/api/projectlist/");
		
		BaseResponse<BaseListObject<SdsProjectData>> response = new BaseResponse<>();
		BaseListObject<SdsProjectData> data = new BaseListObject<>();
		Map<String, Object> map = new HashMap<>();
		map.put("USER_TYPE", "SA");
		map.put("SEQ_NO", 0);
		try {
			List<Map<String, Object>> list = viewService.selectProject(map);
			List<SdsProjectData> outList = new ArrayList<SdsProjectData>();
			if(list != null) {
				for(Map<String,Object> m: list) {
					outList.add(mapToSdsProjectData(m));
				}
			}
			data.setList(outList);
			data.setTotalCount(outList.size());
			response.setData(data);
		}catch(Exception e) {
			response.setCode(IRestCodes.ERR_CODE_FAILURE);
			response.setMsg(e.getMessage());
		}
		
		return response;
	}
	
	private SdsProjectData mapToSdsProjectData(Map<String, Object> map) {
		
		SdsProjectData data = new SdsProjectData();
		data.setProjectName((String)map.get("PROJECT_NAME"));
		data.setDescription((String)map.get("DESCRIPTION"));
		data.setSeqNo(Integer.parseInt(""+map.get("SEQ_NO")));
		data.setUserSeqNo(Integer.parseInt(""+map.get("USER_SEQ_NO")));
		
		return data;
	}
	
	
	@RequestMapping(value="/getRelContentType",  method = {RequestMethod.POST})
    public BaseResponse<?> getRleContent(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	
		BaseResponse<ChatbotRespInterfaceRelData> resp = new BaseResponse<>();
		
    	int agentSeq = 0;
    	int intentSeq = 0;
    	String intentName = "";
    	agentSeq = Integer.parseInt((String)commandMap.get("AGENT_SEQ"));
    	intentSeq = Integer.parseInt((String)commandMap.get("INTENT_SEQ"));
    	intentName = (String)commandMap.get("INTENT_NAME");
    	logger.info("/admin/api/getRelContentType/{},{},{}",agentSeq, intentSeq, intentName);
    	
    	try {
	    	ChatbotRespInterfaceRelData respRel = sdsAdminService.getRespInfo(agentSeq, intentSeq, intentName);
	    	if(respRel != null) {
	    		resp.setData(respRel);
	    	}
    	}catch(Exception e) {
    		resp.setCode(IRestCodes.ERR_CODE_FAILURE);
    		resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
    	}
    	
    	return resp;
    }
	
	@RequestMapping(value="/getrich",  method = {RequestMethod.POST})
    public BaseResponse<?> getRich(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<?> response = null;
    	
    	int agentSeq = 0;
    	int intentSeq = 0;
    	String intentName = "";
    	agentSeq = StrUtils.getNum((String)commandMap.get("AGENT_SEQ"), 0);
    	intentSeq = StrUtils.getNum((String)commandMap.get("INTENT_SEQ"), 0);
    	intentName = (String)commandMap.get("INTENT_NAME");
    	logger.info("/admin/api/getrich/{},{},{}",agentSeq, intentSeq, intentName);
    	
    	try {
    		String domain = (String)request.getSession().getAttribute("ProjectName");
    		ChatbotRespInterfaceRelData respRel = sdsAdminService.getRespInfo(agentSeq, intentSeq, intentName);
	    	if(respRel != null && !IChatbotConst.RICH_CONTENT_TYPE_API.equals(respRel.getRespTypeCd())) {
	    		BaseResponse<ChatbotRichContent> resp = new BaseResponse<ChatbotRichContent>();
	    		ChatbotRichContent rich = sdsAdminService.getRichContent(respRel.getRespTypeCd(), domain, respRel.getIfId());
	    		resp.setData(rich);
	    		
	    		response = resp;
	    	}else if(respRel != null && IChatbotConst.RICH_CONTENT_TYPE_API.equals(respRel.getRespTypeCd())) {
	    		BaseResponse<ChatbotRichApi> resp = new BaseResponse<>();
	    		ChatbotApiInfo api = sdsAdminService.getApiInfo(respRel.getRespTypeCd(), respRel.getIfId());
//	    		resp.setData(api);
	    		
	    		ChatbotRichApi apiData = new ChatbotRichApi();
	    		apiData.setIfId(api.getIfId());
	    		apiData.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_API);
	    		apiData.setApiData(api);
	    		resp.setData(apiData);
	    		
	    		response = resp;
	    	}else {
	    		throw new Exception("Invalid Rich Content Type");//or no data
	    	}
    	}catch(Exception e) {
    		response = new BaseResponse<String>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/getrichbypos",  method = {RequestMethod.POST})
    public BaseResponse<?> getrichbypos(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<?> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	
    	int agentSeq = StrUtils.getNum((String)commandMap.get("AGENT_SEQ"), 0);
    	String pos = StrUtils.getStr((String)commandMap.get("uiPos"), "");
    	int posVal = StrUtils.getNum((String)commandMap.get("uiPosVal"), 0);
    	logger.info("/admin/api/getrichbypos/{},{},{},{}",domain, agentSeq, pos, posVal);
    	
    	try {
    		ChatbotRespInterfaceRelData respRel = sdsAdminService.getRespInfoByPos(domain, agentSeq, pos, posVal);
	    	if(respRel != null && !IChatbotConst.RICH_CONTENT_TYPE_API.equals(respRel.getRespTypeCd())) {
	    		BaseResponse<ChatbotRichContent> resp = new BaseResponse<ChatbotRichContent>();
	    		ChatbotRichContent rich = sdsAdminService.getRichContent(respRel.getRespTypeCd(), domain, respRel.getIfId());
	    		resp.setData(rich);
	    		
	    		response = resp;
	    	}else if(respRel != null && IChatbotConst.RICH_CONTENT_TYPE_API.equals(respRel.getRespTypeCd())) {
	    		BaseResponse<ChatbotRichApi> resp = new BaseResponse<>();
	    		if(respRel.getIfId() == 0) {
	    			//init status(not configured for api), send dummy data
	    			ChatbotRichApi apiData = new ChatbotRichApi();
	    			apiData.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_API);
	    			resp.setData(apiData);
	    		}else {
		    		ChatbotApiInfo api = sdsAdminService.getApiInfo(respRel.getRespTypeCd(), respRel.getIfId());
		    		
		    		ChatbotRichApi apiData = new ChatbotRichApi();
		    		apiData.setIfId(api.getIfId());
		    		apiData.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_API);
		    		apiData.setApiData(api);
		    		resp.setData(apiData);
	    		}
	    		response = resp;
	    	}else {
	    		throw new Exception("Invalid Rich Content Type");//or no data
	    	}
    	}catch(Exception e) {
    		response = new BaseResponse<String>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/getrichbyintent",  method = {RequestMethod.POST})
    public BaseResponse<?> getrichbyintent(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<?> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	
    	int intentSeq = StrUtils.getNum((String)commandMap.get("intentSeq"), 0);
    	logger.info("/admin/api/getrichbyintent/{},{}",domain, intentSeq);
    	
    	try {
    		ChatbotRespInterfaceRelData respRel = sdsAdminService.getRespInfoByIntent(domain, intentSeq);
	    	if(respRel != null && !IChatbotConst.RICH_CONTENT_TYPE_API.equals(respRel.getRespTypeCd())) {
	    		BaseResponse<ChatbotRichContent> resp = new BaseResponse<ChatbotRichContent>();
	    		ChatbotRichContent rich = sdsAdminService.getRichContent(respRel.getRespTypeCd(), domain, respRel.getIfId());
	    		resp.setData(rich);
	    		
	    		response = resp;
	    	}else if(respRel != null && IChatbotConst.RICH_CONTENT_TYPE_API.equals(respRel.getRespTypeCd())) {
	    		BaseResponse<ChatbotRichApi> resp = new BaseResponse<>();
	    		if(respRel.getIfId() == 0) {
	    			//init status(not configured for api), send dummy data
	    			ChatbotRichApi apiData = new ChatbotRichApi();
	    			apiData.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_API);
	    			resp.setData(apiData);
	    		}else {
		    		ChatbotApiInfo api = sdsAdminService.getApiInfo(respRel.getRespTypeCd(), respRel.getIfId());
		    		
		    		ChatbotRichApi apiData = new ChatbotRichApi();
		    		apiData.setIfId(api.getIfId());
		    		apiData.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_API);
		    		apiData.setApiData(api);
		    		resp.setData(apiData);
	    		}
	    		response = resp;
	    	}else {
	    		throw new Exception("Invalid Rich Content Type");//or no data
	    	}
    	}catch(Exception e) {
    		response = new BaseResponse<String>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/getrichbycode",  method = {RequestMethod.POST})
    public BaseResponse<?> getRichbycode(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<?> response = null;
    	
    	String domain = (String)commandMap.get("domain");
    	String respCode = (String)commandMap.get("respCode");
    	logger.info("/admin/api/getrichbycode/{},{}",domain,respCode);
    	
    	try {
    		ChatbotRespInterfaceRelData respRel = sdsAdminService.getRespInfo(domain, respCode);
	    	if(respRel != null && !IChatbotConst.RICH_CONTENT_TYPE_API.equals(respRel.getRespTypeCd())) {
	    		BaseResponse<ChatbotRichContent> resp = new BaseResponse<ChatbotRichContent>();
	    		ChatbotRichContent rich = sdsAdminService.getRichContent(domain, respCode);
	    		resp.setData(rich);
	    		
	    		response = resp;
	    	}else if(respRel != null && IChatbotConst.RICH_CONTENT_TYPE_API.equals(respRel.getRespTypeCd())) {
	    		BaseResponse<ChatbotRichApi> resp = new BaseResponse<>();
	    		ChatbotApiInfo api = sdsAdminService.getApiInfo(domain, respCode);
//	    		resp.setData(api);
	    		
	    		ChatbotRichApi apiData = new ChatbotRichApi();
	    		apiData.setIfId(api.getIfId());
	    		apiData.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_API);
	    		apiData.setApiData(api);
	    		resp.setData(apiData);
	    		
	    		response = resp;
	    	}else {
	    		throw new Exception("Invalid Rich Content Type");
	    	}
    	}catch(Exception e) {
    		response = new BaseResponse<String>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/delbutton",  method = {RequestMethod.POST})
    public BaseResponse<?> delbutton(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<String> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	
    	int agentSeq = 0;
    	int intentSeq = 0;
    	String intentName = "";
    	int orderNo = 1;
    	agentSeq = StrUtils.getNum((String)commandMap.get("agentSeq"), 0);
    	intentSeq = StrUtils.getNum((String)commandMap.get("intentSeq"), 0);
    	orderNo = StrUtils.getNum((String)commandMap.get("btnOrder"), 0);
    	
    	logger.info("/admin/api/delbutton/{},{},{},{}",domain, agentSeq, intentSeq, orderNo);
    	
    	try {
    		sdsAdminService.deleteSingleButton(domain, agentSeq, intentSeq, orderNo);
    		response = new BaseResponse<>();
    	}catch(Exception e) {
    		response = new BaseResponse<String>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/delbuttonpos",  method = {RequestMethod.POST})
    public BaseResponse<?> delbuttonPos(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<?> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int agentSeq = StrUtils.getNum((String)commandMap.get("AGENT_SEQ"), 0);
    	String uiPos = StrUtils.getStr((String)commandMap.get("uiPos"), "");
    	int uiPosVal = StrUtils.getNum((String)commandMap.get("uiPosVal"), 0);
    	int btnOrder = StrUtils.getNum((String)commandMap.get("btnOrder"), 0);
    	
    	logger.info("/admin/api/delbuttonpos/{},{},{},{}",agentSeq, uiPos, uiPosVal, btnOrder);
    	
    	try {
    		response = new BaseResponse<>();
    		long nRet = sdsAdminService.deleteSingleButtonPos(domain, agentSeq, uiPos, uiPosVal, btnOrder);
    		logger.debug("deleted button count {}",nRet);
    		if(nRet < 1) {
    			throw new Exception("button is not exist.");
    		}
    	}catch(Exception e) {
    		response = new BaseResponse<String>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/delrich",  method = {RequestMethod.POST})
    public BaseResponse<?> delrich(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<?> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int agentSeq = StrUtils.getNum((String)commandMap.get("AGENT_SEQ"), 0);
    	String uiPos = StrUtils.getStr((String)commandMap.get("uiPos"), "");
    	int uiPosVal = StrUtils.getNum((String)commandMap.get("uiPosVal"), 0);
    	String richTypeCd = StrUtils.getStr((String)commandMap.get("richTypeCd"), "");
    	
    	logger.info("/admin/api/delrich/{},{},{},{},{}",domain, agentSeq, uiPos, uiPosVal, richTypeCd);
    	
    	try {
    		response = new BaseResponse<>();
    		long nRet = sdsAdminService.deleteRichPos(domain, agentSeq, uiPos, uiPosVal, richTypeCd);
    		logger.debug("deleted button count {}",nRet);
    		if(nRet < 1) {
    			//throw new Exception("button is not exist.");
    		}
    	}catch(Exception e) {
    		response = new BaseResponse<String>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/delrichintent",  method = {RequestMethod.POST})
    public BaseResponse<?> delrichintent(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<?> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int intentSeq = StrUtils.getNum((String)commandMap.get("intentSeq"), 0);
    	String richTypeCd = StrUtils.getStr((String)commandMap.get("richTypeCd"), "");
    	
    	logger.info("/admin/api/delrichintent/{},{},{}",domain, intentSeq, richTypeCd);
    	
    	try {
    		response = new BaseResponse<>();
    		long nRet = sdsAdminService.deleteRichByIntent(domain, intentSeq, richTypeCd);
    		logger.debug("deleted button count {}",nRet);
    		if(nRet < 1) {
    			//throw new Exception("button is not exist.");
    		}
    	}catch(Exception e) {
    		response = new BaseResponse<String>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/updatebutton",  method = {RequestMethod.POST})
    public BaseResponse<?> updatebutton(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<?> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	
    	int agentSeq = 0;
    	int intentSeq = 0;
    	String intentName = "";
    	int orderNo = 1;
    	agentSeq = StrUtils.getNum((String)commandMap.get("AGENT_SEQ"), 0);
    	intentSeq = StrUtils.getNum((String)commandMap.get("INTENT_SEQ"), 0);
    	intentName = (String)commandMap.get("INTENT_NAME");
    	orderNo = StrUtils.getNum((String)commandMap.get("btnOrder"), 0);
    	String title = (String)commandMap.get("TITLE");
    	String utter = (String)commandMap.get("UTTER");
    	String userId = HttpUtils.getUserId(request);
    	
    	logger.info("/admin/api/updatebutton/{},{},{},{}",domain, agentSeq, intentSeq, orderNo);
    	
    	try {
    		sdsAdminService.updateSingleButton(domain, agentSeq, intentSeq, orderNo, title, utter, userId);
    	}catch(Exception e) {
    		response = new BaseResponse<String>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/addbutton",  method = {RequestMethod.POST})
    public BaseResponse<?> addbutton(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<String> response = null;	
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	
    	int btnOrder = StrUtils.getNum((String)commandMap.get("btnOrder"), 0);
    	int agentSeq = StrUtils.getNum((String)commandMap.get("agentSeq"), 0);
    	int intentSeq = StrUtils.getNum((String)commandMap.get("intentSeq"), 0);
    	String title = (String)commandMap.get("TITLE");
    	String utter = (String)commandMap.get("UTTER");
    	String userId = HttpUtils.getUserId(request);
    	
    	logger.info("/admin/api/addbutton/{},{},{},{},{},{}",domain, agentSeq, intentSeq, btnOrder, title, utter);
    	
    	try {
    		if(btnOrder > 0) {
    			sdsAdminService.updateSingleButton(domain, agentSeq, intentSeq, btnOrder, title, utter, userId);
    		}else {
    			sdsAdminService.addSingleButton(domain, agentSeq, intentSeq, title, utter, userId);
    		}
    		response = new BaseResponse<>();
    	}catch(Exception e) {
    		response = new BaseResponse<String>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/addbuttonpos",  method = {RequestMethod.POST})
    public BaseResponse<?> addbuttonPos(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<?> response = null;	
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int agentSeq = StrUtils.getNum((String)commandMap.get("AGENT_SEQ"), 0);
    	String title = (String)commandMap.get("TITLE");
    	String utter = (String)commandMap.get("UTTER");
    	int btnOrder = StrUtils.getNum((String)commandMap.get("btnOrder"), 0);
    	String uiPos = (String)commandMap.get("uiPos");
    	int uiPosVal = StrUtils.getNum((String)commandMap.get("uiPosVal"),0);
    	String userId = HttpUtils.getUserId(request);
    	
    	logger.info("/admin/api/addbuttonpos/{},{},{},{},{},{}",agentSeq, uiPos, uiPosVal, title, utter, btnOrder);
    	
    	try {
    		response = new BaseResponse<>();
    		sdsAdminService.addSingleButtonPos(domain, agentSeq, uiPos, uiPosVal, btnOrder, title, utter, userId);
    	}catch(Exception e) {
    		response = new BaseResponse<String>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/isvalidresponse",  method = {RequestMethod.POST})
    public BaseResponse<?> isvalidresponse(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<ChatbotRespInterfaceRelData> response = null;
    	String domain = StrUtils.getStr((String)commandMap.get("domain"), "");
    	String respCode = StrUtils.getStr((String)commandMap.get("respCode"), "");
    	logger.info("/admin/api/isvalidresponse/{},{}",domain, respCode);
    	try {
    		
    		ChatbotRespInterfaceRelData data = sdsAdminService.getRespInfo(domain, respCode);
    		response = new BaseResponse<>();
    		if(data != null) {
    			response.setData(data);
    		}
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/respinterfaceinfo",  method = {RequestMethod.POST})
    public BaseResponse<?> respinterfaceinfo(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<ChatbotRespInterfaceRelData> response = null;
    	
    	String domain = StrUtils.getStr((String)commandMap.get("domain"), "");
    	String respCode = StrUtils.getStr((String)commandMap.get("respCode"), "");
    	logger.info("/admin/api/respinterfaceinfo/{},{}",domain, respCode);
    	try {
    		ChatbotRespInterfaceRelData data = sdsAdminService.getRespInfo(domain, respCode);
    		response = new BaseResponse<>();
    		if(data != null) {
    			response.setData(data);
    		}
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/insertrespinterfaceinfo",  method = {RequestMethod.POST})
    public BaseResponse<?> addrespinterfaceinfo(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<ChatbotRespInterfaceRelData> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int agentSeq = StrUtils.getNum((String)commandMap.get("agentSeq"), 0);
    	String uiPos = StrUtils.getStr((String)commandMap.get("uiPos"), "");
    	int uiPosVal = StrUtils.getNum((String)commandMap.get("uiPosVal"), 0);
    	String respCode = StrUtils.getStr((String)commandMap.get("respCode"), "");
    	String respMsg = StrUtils.getStr((String)commandMap.get("respMsg"), "");
    	String respTypeCd = StrUtils.getStr((String)commandMap.get("respTypeCd"), "");
    	logger.info("/admin/api/addrespinterfaceinfo/{},{},{},{},{},{}",domain, agentSeq, uiPos, uiPosVal, respCode, respMsg);
    	try {
    		String userId = HttpUtils.getUserId(request);
    		long nRet = sdsAdminService.insertRespInfoByPos(domain, agentSeq, uiPos, uiPosVal, respCode, respMsg, respTypeCd, userId);
    		//if(nRet > 0) {
    			response = new BaseResponse<>();
//    		}else {
//    			throw new Exception("insert/updated count 0");
//    		}
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/insertrespinterfaceinfointent",  method = {RequestMethod.POST})
    public BaseResponse<?> insertrespinterfaceinfointent(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<ChatbotRespInterfaceRelData> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	String respCode = StrUtils.getStr((String)commandMap.get("respCode"), "");
    	String respMsg = StrUtils.getStr((String)commandMap.get("respMsg"), "");
    	String respTypeCd = StrUtils.getStr((String)commandMap.get("respTypeCd"), "");
    	int intentSeq = StrUtils.getNum((String)commandMap.get("intentSeq"), 0);
    	logger.info("/admin/api/insertrespinterfaceinfointent/{},{},{},{},{},{}",domain, intentSeq, respCode, respMsg);
    	try {
    		String userId = HttpUtils.getUserId(request);
    		long nRet = sdsAdminService.insertRespInfoByIntent(domain, intentSeq, respCode, respMsg, respTypeCd, userId);
    		//if(nRet > 0) {
    			response = new BaseResponse<>();
//    		}else {
//    			throw new Exception("insert/updated count 0");
//    		}
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/addimage",  method = {RequestMethod.POST})
    public BaseResponse<?> addimage(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<ChatbotRespInterfaceRelData> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int agentSeq = StrUtils.getNum((String)commandMap.get("agentSeq"), 0);
    	String uiPos = StrUtils.getStr((String)commandMap.get("uiPos"), "");
    	int uiPosVal = StrUtils.getNum((String)commandMap.get("uiPosVal"), 0);
    	//String imagePath = StrUtils.getStr((String)commandMap.get("imagePath"), "");
    	String imageLink = StrUtils.getStr((String)commandMap.get("imageLink"), "");
    	logger.info("/admin/api/addimage/{},{},{},{},{},{}",domain, agentSeq, uiPos, uiPosVal, imageLink);
    	try {
    		String userId = HttpUtils.getUserId(request);
    		
    		long nRet = sdsAdminService.addImagePos(domain, agentSeq, uiPos, uiPosVal, imageLink, userId, request);
    		if(nRet > 0) {
    			response = new BaseResponse<>();
    		}else {
    			throw new Exception("insert/updated count 0");
    		}
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/addimageintent",  method = {RequestMethod.POST})
    public BaseResponse<?> addimageIntent(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<ChatbotRespInterfaceRelData> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int agentSeq = StrUtils.getNum((String)commandMap.get("agentSeq"), 0);
    	int intentSeq = StrUtils.getNum((String)commandMap.get("intentSeq"), 0);
    	//String imagePath = StrUtils.getStr((String)commandMap.get("imagePath"), "");
    	String imageLink = StrUtils.getStr((String)commandMap.get("imageLink"), "");
    	logger.info("/admin/api/addimageIntent/{},{},{},{},{},{}",domain, agentSeq, intentSeq, imageLink);
    	try {
    		String userId = HttpUtils.getUserId(request);
    		
    		long nRet = sdsAdminService.addImageIntent(domain, agentSeq, intentSeq, imageLink, userId, request);
    		if(nRet > 0) {
    			response = new BaseResponse<>();
    		}else {
    			throw new Exception("insert/updated count 0");
    		}
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/addcarousel",  method = {RequestMethod.POST})
    public BaseResponse<?> addcarousel(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<ChatbotRespInterfaceRelData> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int agentSeq = StrUtils.getNum((String)commandMap.get("agentSeq"), 0);
    	String uiPos = StrUtils.getStr((String)commandMap.get("uiPos"), "");
    	int uiPosVal = StrUtils.getNum((String)commandMap.get("uiPosVal"), 0);
    	//String imagePath = StrUtils.getStr((String)commandMap.get("imagePath"), "");
    	String imageLink = StrUtils.getStr((String)commandMap.get("imageLink"), "");
    	logger.info("/admin/api/addcarousel/{},{},{},{},{},{}",domain, agentSeq, uiPos, uiPosVal, imageLink);
    	try {
    		String userId = HttpUtils.getUserId(request);
    		
    		long nRet = sdsAdminService.addCarouselPos(domain, agentSeq, uiPos, uiPosVal, imageLink, userId, request);
    		if(nRet > 0) {
    			response = new BaseResponse<>();
    		}else {
    			throw new Exception("insert/updated count 0");
    		}
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/addcarouselintent",  method = {RequestMethod.POST})
    public BaseResponse<?> addcarouselintent(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<ChatbotRespInterfaceRelData> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int agentSeq = StrUtils.getNum((String)commandMap.get("agentSeq"), 0);
    	int intentSeq = StrUtils.getNum((String)commandMap.get("intentSeq"), 0);
    	//String imagePath = StrUtils.getStr((String)commandMap.get("imagePath"), "");
    	String imageLink = StrUtils.getStr((String)commandMap.get("imageLink"), "");
    	logger.info("/admin/api/addcarouselintent/{},{},{},{}",domain, agentSeq, intentSeq, imageLink);
    	try {
    		String userId = HttpUtils.getUserId(request);
    		
    		long nRet = sdsAdminService.addCarouselIntent(domain, agentSeq, intentSeq, imageLink, userId, request);
    		if(nRet > 0) {
    			response = new BaseResponse<>();
    		}else {
    			throw new Exception("insert/updated count 0");
    		}
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/delcarousel",  method = {RequestMethod.POST})
    public BaseResponse<?> delcarousel(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<ChatbotRespInterfaceRelData> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int agentSeq = StrUtils.getNum((String)commandMap.get("agentSeq"), 0);
    	String uiPos = StrUtils.getStr((String)commandMap.get("uiPos"), "");
    	int uiPosVal = StrUtils.getNum((String)commandMap.get("uiPosVal"), 0);
    	int carouselOrder = StrUtils.getNum((String)commandMap.get("carouselOrder"), 0);
    	logger.info("/admin/api/delcarousel/{},{},{},{},{}",domain, agentSeq, uiPos, uiPosVal, carouselOrder);
    	
    	try {
    		long nRet = sdsAdminService.deleteCarouselPos(domain, agentSeq, uiPos, uiPosVal, carouselOrder);
    		if(nRet > 0) {
    			response = new BaseResponse<>();
    		}else {
    			throw new Exception("deleted count 0");
    		}
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	@RequestMapping(value="/delcarouselintent",  method = {RequestMethod.POST})
    public BaseResponse<?> delcarouselintent(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<ChatbotRespInterfaceRelData> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int agentSeq = StrUtils.getNum((String)commandMap.get("agentSeq"), 0);
    	int intentSeq = StrUtils.getNum((String)commandMap.get("intentSeq"), 0);
    	int carouselOrder = StrUtils.getNum((String)commandMap.get("carouselOrder"), 0);
    	logger.info("/admin/api/delcarouselintent/{},{},{},{},{}",domain, agentSeq, intentSeq, carouselOrder);
    	
    	try {
    		long nRet = sdsAdminService.deleteCarouselIntent(domain, agentSeq, intentSeq, carouselOrder);
    		if(nRet > 0) {
    			response = new BaseResponse<>();
    		}else {
    			throw new Exception("deleted count 0");
    		}
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
	
	//serve image for admin
	@RequestMapping(value="/richimage/{type}/{domain}/{loc:.+}",  method = {RequestMethod.GET})
    public ResponseEntity<byte[]> getimage(@PathVariable("type") String richType, @PathVariable("domain") String domain, @PathVariable("loc") String filename, HttpServletRequest request) throws Exception{
    	logger.info("/admin/api/richimage/{},{},{}",richType, domain, filename);
    	String path = "";
    	if(richType.equals(IChatbotConst.DIR_RICH_IMG)) {
    		path = IChatbotConst.DIR_RICH_IMG + "/" + domain + "/" + filename;
    	}else if(richType.equals(IChatbotConst.DIR_RICH_CAROUSEL)) {
    		path = IChatbotConst.DIR_RICH_CAROUSEL + "/" + domain + "/" + filename;
    	}
    	return imageUtils.getImage(path);
    }
    
    @RequestMapping(value="/apilist",  method = {RequestMethod.POST})
    public BaseResponse<?> apilist(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<BaseListObject<ChatbotApiInfoDisp>> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int workspaceId = HttpUtils.getWorkspaceId(request);
    	logger.info("/admin/api/delcarousel/{},{}",domain, workspaceId);
    	
    	try {
    		BaseListObject<ChatbotApiInfoDisp> list = sdsAdminService.getApiList(workspaceId);
    		response = new BaseResponse<>();
    		response.setData(list);
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
    
    @RequestMapping(value="/setapi",  method = {RequestMethod.POST})
    public BaseResponse<?> setapi(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<String> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int agentSeq = StrUtils.getNum((String)commandMap.get("agentSeq"), 0);
    	String uiPos = StrUtils.getStr((String)commandMap.get("uiPos"), "");
    	int uiPosVal = StrUtils.getNum((String)commandMap.get("uiPosVal"), 0);
    	int ifId = StrUtils.getNum((String)commandMap.get("ifId"), 0);
    	String userId = HttpUtils.getUserId(request);
    	logger.info("/admin/api/setapi/{},{},{},{},{}",domain, agentSeq, uiPos, uiPosVal, ifId);
    	
    	try {
    		long nRet = sdsAdminService.setApi(domain, agentSeq, uiPos, uiPosVal, ifId, userId);
    		logger.debug("updated response relation table count:{}", nRet);
    		response = new BaseResponse<>();
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
    
    @RequestMapping(value="/setapiintent",  method = {RequestMethod.POST})
    public BaseResponse<?> setapiintent(CommandMap commandMap, HttpServletRequest request) throws Exception{
    	BaseResponse<String> response = null;
    	
    	String domain = (String)request.getSession().getAttribute("ProjectName");
    	int agentSeq = StrUtils.getNum((String)commandMap.get("agentSeq"), 0);
    	int intentSeq = StrUtils.getNum((String)commandMap.get("intentSeq"), 0);
    	int ifId = StrUtils.getNum((String)commandMap.get("ifId"), 0);
    	String userId = HttpUtils.getUserId(request);
    	logger.info("/admin/api/setapiintent/{},{},{},{},{}",domain, agentSeq, intentSeq, ifId);
    	
    	try {
    		long nRet = sdsAdminService.setApiIntent(domain, agentSeq, intentSeq, ifId, userId);
    		logger.debug("updated response relation table count:{}", nRet);
    		response = new BaseResponse<>();
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	
    	return response;
    }
    
    @RequestMapping(value="/getport", method = {RequestMethod.GET, RequestMethod.POST})
    public BaseResponse<Integer> getPort(@RequestParam(name="port",required=true) int port, @RequestParam(name="maxport",required=true, defaultValue="58000" ) int maxport) throws Exception {
    	BaseResponse<Integer> response = null;
    	
    	int maxMargin = 10;
    	int currPort = port;
    	try {
//	    	ServerSocket server = new ServerSocket(port);
//	    	server.close();
    		int max = Math.max(currPort, maxport);
    		int min = Math.min(currPort, maxport);
//    		int nextPort = SocketUtils.findAvailableTcpPort(min, max);//not working correctly
    		
    		int nextPort = findFreePort(min,max);
    		SdsPortUsed tmp = null;
    		for(int i=min;i<=max+maxMargin;i++) {
				tmp = mapPort.get(i);
				if(tmp == null) {
					//valid
					tmp = new SdsPortUsed();
					tmp.setPort(i);
					tmp.setTimestamp(System.currentTimeMillis());
					mapPort.put(i, tmp);
					break;
				}else {
					if(System.currentTimeMillis() - tmp.getTimestamp() > 5*60*1000) {//temp 5 min?
						//valid
						tmp.setTimestamp(System.currentTimeMillis());
						mapPort.remove(i);
						mapPort.put(i, tmp);
						break;
					}else {
						logger.debug("getport/{} is in use", tmp.getPort());
						continue;
					}
				}
			}
			if(tmp != null) {
				nextPort = tmp.getPort();
			}
    		
	    	response = new BaseResponse<>();
	    	response.setData(nextPort);
	    	
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	return response;
    }
    
    @RequestMapping(value="removeport", method = {RequestMethod.GET, RequestMethod.POST})
    public BaseResponse<Integer> removeort(@RequestParam(name="port",required=true) int port) throws Exception {
    	BaseResponse<Integer> response = null;
    	
    	int currPort = port;
    	try {
	    	SdsPortUsed tmpUsed = mapPort.get(currPort);
	    	if(tmpUsed == null) {
	    		//success
	    	}else {
//	    		mapPort.put(currPort, null);
	    		mapPort.remove(currPort);
	    	}
    		
	    	response = new BaseResponse<>();
	    	response.setData(currPort);
	    	
    	}catch(Exception e) {
    		response = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
    	}
    	return response;
    }
    
	private static boolean available(final int port) {
		ServerSocket serverSocket = null;
		DatagramSocket dataSocket = null;
		try {
			serverSocket = new ServerSocket(port);
			serverSocket.setReuseAddress(true);
			dataSocket = new DatagramSocket(port);
			dataSocket.setReuseAddress(true);
			return true;
		} catch (final IOException e) {
			return false;
		} finally {
			if (dataSocket != null) {
				dataSocket.close();
			}
			if (serverSocket != null) {
				try {
					serverSocket.close();
				} catch (final IOException e) {
					// can never happen
				}
			}
		}
	}
	
	public synchronized static int findFreePort(int minPort, int maxPort) {
		for (int i = minPort; i <= maxPort; i++) {
			if (available(i)) {
				return i;
			}
		}
		throw new RuntimeException("Could not find an available port between " + minPort + " and " + maxPort);
	}
}
