var localStorage;
// seqno: prject seq_no, project name, server port
function fnSelectProjectEx(seqno, name, port) {
	fnInitStorage();
	if(seqno){
		localStorage.setItem('seqno', seqno);
	}
	if(name){
		localStorage.setItem('name', name);
	}
	
	// setCookie('ProjectName', name);
	
	var comSubmit = new ComSubmit();
	comSubmit.setUrl(ctxPath+'view/tabstask.do');		
	comSubmit.addParam("SEQ_NO", seqno);
	comSubmit.addParam("PROJECT_NAME", name);
	comSubmit.addParam("SERVER_PORT", port);
	comSubmit.submit();
}

function fnGetSeqNo(){
	fnInitStorage();
	return localStorage.getItem('seqno');
}

function fnGetName(){
	fnInitStorage();
	return localStorage.getItem('name');
}

function fnStorageReset(){
	if(!localStorage){
		return;
	}
	localStorage.removeItem('seqno');
	localStorage.removeItem('name');
}

function fnInitStorage(){
	if(!localStorage){
		localStorage = window['localStorage'];
	}
}

function fnGoHome(){
	var comSubmit = new ComSubmit();
	comSubmit.setUrl(ctxPath+'view/projectlist.do');		
	comSubmit.submit();
}

var gPanelShow = false;
function fnShowTraining(){
	gPanelShow = !gPanelShow;
	var btnTitle_prefix = '';
	if(gPanelShow){
		btnTitle_prefix = "Hide";
		
		$('#right').parent().show();
	}else{
		btnTitle_prefix = "Show";
		$('#right').parent().hide();
	}
	$('#btnTrain').html(btnTitle_prefix+' Training');
	//fnLearning();
}

function composedPath(el) {
    var path = [];

    while (el) {
        path.push(el);

        if (el.tagName === 'HTML') {
            path.push(document);
            path.push(window);
            return path;
        }
        el = el.parentElement;
    }
}