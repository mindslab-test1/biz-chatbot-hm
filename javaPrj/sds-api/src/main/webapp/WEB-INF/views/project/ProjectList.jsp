<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/include/header.jsp"%>
<script type="text/javascript">
		window.onload = function() {
					
		}
		
		function fnGoDetail() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/useruttrwrite.do' />");
			comSubmit.submit();
		}
		
		function fnOpenCreatDatype() {		
			fnGoDetail();
			fnpreventDefault(event);
		}				
		
		function fnProejctDelete(prjseq, prjname) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/deletproject.do' />");		
				comSubmit.addParam("SEQ_NO", prjseq);
				comSubmit.addParam("PROJECT_NAME", prjname);
				comSubmit.submit();
			}
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf"%>
	<div class="project_list">
		<div class="cont_title">
			<h2>Domains</h2>
			<a id="aCreateDomain" href="#">Create Domain</a>
		</div>
		<div class="user_utterance_contents" style="margin-top: 25px;">
			<dl class="class_name">
				<c:set var="UserSeq" value=""></c:set>
				<c:forEach items="${prjlist }" var="row">						
					<c:if test="${UserSeq.toString() ne row.USER_SEQ_NO.toString()}">
						<h3 style="font-size: 20px; font-weight: normal; line-height: 24px; margin: 10px 45px 10px 45px;">${row.USER_ID} / ${row.USER_NAME }</h3>							
						<c:set var="UserSeq" value="${row.USER_SEQ_NO.toString()}"></c:set>
					</c:if>
					<dd style="border-top: 1px solid #e1e1e1;">
						<a
							onclick="fnSelectProject(${row.SEQ_NO}, '${row.PROJECT_NAME }', '${row.SERVER_PORT }');"
							href="#" class="class_name_list">${row.PROJECT_NAME }</a> <a
							onclick="fnProejctModify(${row.SEQ_NO});" href="#"
							class="modify_btn"><span class="hide">수정</span></a> <a
							onclick="fnProejctDelete(${row.SEQ_NO}, '${row.PROJECT_NAME }');" href="#"
							class="delete_btn"><span class="hide">삭제</span></a>
					</dd>
				</c:forEach>
			</dl>
		</div>
	</div>
	<%@ include file="/WEB-INF/include/footer.jspf"%>
</body>
</html>