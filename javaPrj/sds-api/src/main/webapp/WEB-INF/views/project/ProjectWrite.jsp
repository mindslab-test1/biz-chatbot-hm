<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>	
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">	
		var bSaveChk = true;
		window.onload = function() {
			if(!gfn_isNull('${PROJECT_NAME}')) {
				$("#hidPrjSeq").val('${SEQ_NO}');
				$("#txtProjectName").val('${PROJECT_NAME}');
				$("#txtDescription").val('${DESCRIPTION}');
			}
		}
		
		function fnSave(){
			if (bSaveChk) {
				bSaveChk = false;
				
				var ProejctName = $('#txtProjectName').val().trim();
																
				if(ProejctName == '' || ProejctName == 'Domain name') {
					alert('도메인명을 입력하세요.');
					$('#txtProjectName').focus();
					bSaveChk = true;
					fnpreventDefault(event);
					return;	
				}
				
				var reg = /[^a-zA-Z0-9_-]/; 
				if (reg.test(ProejctName)) {
					alert('도메인명에 한글,공백,특수문자를 입력 할 수 없습니다.');
					$('#txtProjectName').focus();
					bSaveChk = true;
					fnpreventDefault(event);
					return;
				}
				
				if($('#txtDescription').val() == 'Describe your domain')
					$('#txtDescription').val('');
				
				var comAjax = new ComAjax();
				comAjax.setUrl("<c:url value='/view/selectprojectname.do' />");
				comAjax.setCallback("fnSaveCallBack");
				comAjax.addParam("PROJECT_NAME", ProejctName);
				comAjax.ajax();
			}
			fnpreventDefault(event);
		}
		
		function fnSaveCallBack(data) {						
			if (data.list.length == 0) {
				fnLoading();
				var comSubmit = new ComSubmit("frm");
				comSubmit.setUrl("<c:url value='/view/insertproject.do' />");
				comSubmit.submit();	
			}
			else {
				alert('이미 존재하는 도메인명 입니다.');
				bSaveChk = true;
			}	
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>
	<form id="frm" name="frm">
		<!--Agent_name_box start-->
        <div class="Agent_name_box">
            <div class="s_word">
                <label for="search">
                    <input type="text" id="txtProjectName" name="PROJECT_NAME" value="Domain name" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" />
                    <a href="#" id="saveproject" onclick="fnSave();" title="저장"><span class="project_save">Save</span></a>
                </label>
            </div>
        </div>
        <!--Agent_name_box end-->
        <div class="description">
            <dl class="active-result">
                <dt>DESCRIPTION</dt>
                <dd>
                    <!--DESCRIPTION_box start-->
                    <div class="description_box">
                        <div class="s_word">
                            <label for="search">
                                <input id="txtDescription" name="DESCRIPTION" type="text" value="Describe your domain" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" />
                            </label>
                        </div>
                    </div>
                    <!--DESCRIPTION_box end-->
                </dd>
            </dl>
        </div>
        <input type="hidden" id="hidPrjSeq" name="SEQ_NO" value="" />
        <input type="hidden" id="hidOldName" name="OLD_NAME" value="${PROJECT_NAME}" />
	</form>
	<%@ include file="/WEB-INF/include/footer.jspf" %>
</body>
</html>