<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		var $WorkhidObj;
		var blockvalue = '';
		var UserSayRange;
		var arrSlot = new Array();
		var arrSlotList = new Array();
		var iSeqNo = '';
		var jsondata = jQuery.parseJSON('${INTENTS_LIST}'.replace(/=\"/g, '=').replace(/\"\)/g, ')'));
		var slotdata2 = jQuery.parseJSON('${SLOT_LIST2}');
		var slotstr = '';
		
		window.onload = function() {
			AddRow();
			
			$('#divUserSays').on({				
				'click focusin': function(e) {
					if($(this).parent().next().length == 0) {
						AddRow();
					}
				},
				mouseup: function(e) {
					blockvalue = window.getSelection().toString();
					if(!gfn_isNull(blockvalue)) {												
						UserSayRange = window.getSelection().getRangeAt(0);
						$WorkhidObj = $(this).next();					
						var comAjax = new ComAjax();
						comAjax.setUrl("<c:url value='/view/getslotobjectlist.do' />");
						comAjax.setCallback("fnSlotSearchCallBack");
						comAjax.addParam("OBJECT_NAME", blockvalue);
						comAjax.ajax();
					}
				},
				focusout: function(e) {
																							
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						$(this).parent().prev().find('[name=OBJECT_NAME]').focus();
					} else if(e.keyCode == 40) {
						$(this).parent().next().find('[name=OBJECT_NAME]').focus();
					} else if(e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 39) {
					
					} else {
						bWorkChk = true;
					/* 	var $this = $(this);
					
						if(!gfn_isNull($this.attr('id'))) 
							$('#hidSaveChk' + $this.attr('id').replace('txt', '')).val('Y'); */
					}
				}			
			}, '[name=USER_SAYS]');
			
			$('#divreplies').on({				
				'click focusin': function(e) {						
					if($(this).parent().parent().next().length == 0) {
						AddObj($(this).parent().parent().parent());
					}
				},
				focusout: function(e) {				
					$this = $(this);
					if ($this.text().toLowerCase().indexOf('request') > -1) {
						if ($this.parent().next().find('[name=OBJECT_DETAIL_NAME]').length > 0) {
							$this.parent().next().empty();
						}
						if ($this.parent().next().find('[name=OBJECT_REQUEST]').length == 0) {
							$this.parent().next().prepend('<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
							$this.parent().next().append('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
						}
					}
					else {
						if ($this.parent().next().find('[name=OBJECT_REQUEST]').length > 0) {
							$this.parent().next().empty();
							$this.parent().next().append('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
						}
					}
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						$(this).parent().parent().prev().find('th').find('div').eq(0).focus();
					} else if(e.keyCode == 40) {
						$(this).parent().parent().next().find('th').find('div').eq(0).focus();
					} else if(e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 39) {
					
					} else {
						bWorkChk = true;
					/* 	var $this = $(this);
					
						if(!gfn_isNull($this.attr('id'))) 
							$('#hidSaveChk' + $this.attr('id').replace('txt', '')).val('Y'); */
					}
				}			
			}, '[name=OBJECT_NAME]');
			
			$('#divreplies').on({				
				'click': function(e) {
					/* if($(this).parent().next().find('div').eq(0).attr('name') == undefined || $(this).parent().next().find('div').eq(0).attr('name') == 'OBJECT_DETAIL_NAME') {
						$(this).parent().after('<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
					} */
					$(this).attr('id', 'tempid');
					fnWinPop("<c:url value='/view/requestslotsearch.do' />", "SlotSearchpop", 430, 550, 0, 0);
				},
				'change':function(e) {
					bWorkChk = true;
				}
			}, '[name=OBJECT_REQUEST]');
			
			$('#divreplies').on({				
				'click focusin': function(e) {
					if($(this).parent().next().length == 0) {
						$(this).parent().after('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
					}
				},
				'change':function(e) {
					bWorkChk = true;
				}
			}, '[name=OBJECT_REQUEST_PATTERN]');
			
			$('#divreplies').on({				
				'click focusin': function(e) {						
					var $this = $(this);
					
					if($this.parent().next().length == 0) {
						$this.parent().parent().append('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
					}
				},
				focusout: function(e) {
																							
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						$(this).parent().parent().parent().prev().find('td div [name=OBJECT_DETAIL_NAME]').eq(0).focus();
					} else if(e.keyCode == 40) {
						$(this).parent().parent().parent().next().find('td div [name=OBJECT_DETAIL_NAME]').eq(0).focus();
					} else if(e.keyCode == 9) {						
						//$(this).next().focus();
						$(this).parent().next().children().eq(0).focus();
						fnCursorEnd($(this).parent().next().children().eq(0).get(0));
						fnpreventDefault(e);
					}
					else {
						bWorkChk = true;	
					}
				}			
			}, '[name=OBJECT_DETAIL_NAME]');
			
			$('#divreplies').on('click', '[name=imgAction]', function(e) {
				$txtAction = $(this).parent().next().find('input');
				$imgActionDel = $(this).parent().next().next().find('img');
				if($txtAction.css('display') == 'none') {
					$txtAction.show();
					$imgActionDel.show();
				}
				else {
					$txtAction.hide();
					$imgActionDel.hide();
				}
				fnpreventDefault(e);
			});
			
			$('#divreplies').on('click', '[name=imgActionDel]', function(e) {
				$(this).parent().prev().find('input').val('');
				fnpreventDefault(e);
			});
			
			$('#divreplies').on('click', '.close_btn', function(e) {				
				if($(this).parent().siblings().length > 0)				
					$(this).parent().remove();
				else {
					if($(this).parent().parent().parent().siblings().length > 0)
						$(this).parent().parent().parent().remove();					
				}
				bWorkChk = true;
				fnpreventDefault(e);
			});
			
			$('[name=useruttrw]').each(function() {
				if ($(this).find('.uuw_tab').length == 0) {
					$(this).append($('#divtbAdd').html());
				}	
			});
			
			$('[name=OBJECT_REQUEST]').each(function() {
				if($(this).parent().next().length == 0) {
					$(this).parent().after('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
				}
			});
			
			$('#txtIntentsName').change(function() {
				bWorkChk = true;
			});
			
			<c:forEach items="${SlotList}" var="row">
				var objSlot = new Object();
				objSlot.seqno = ${row.SEQ_NO};
				objSlot.slotname = '${row.SLOT_NAME}';
				objSlot.color = '${row.COLOR}';
				
				arrSlotList['${row.SLOT_NAME}'] = objSlot;					
			</c:forEach>
		}			
		
		function AddRow() {
			$('#divUserSays').append($('#divAdd').html());
		}
		
		function AddObj(obj) {
			var str = '<tr><th>';
			str += '<div name="OBJECT_NAME" contenteditable="true">inform()</div></th>';
			str += '<td class="slot_modify01">';
			str += '<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>';
			str += '</td></tr>';
			obj.append(str);
		}
		
		function fnSlotSearchCallBack(data) {
			var divTop = event.clientY - 40; //상단 좌표 위치 안맞을시 e.pageY
			var divLeft = event.clientX; //좌측 좌표 위치 안맞을시 e.pageX
			
			slotstr = '';
			if(data.list.length > 0) {
				$.each(data.list, function(key, value){					
					slotstr += '<a href="#" onclick="fnSetSlot(' + value.SEQ_NO + ', \'' + value.CLASS_NAME + '.' + value.SLOT_NAME + '\', \'' + value.COLOR + '\', true);">' + value.CLASS_NAME + '.' + value.SLOT_NAME + '</a>';
					
					if (value.PARENT_CLASS_SEQ_NO != 0) {
						fnGetNotUsingClass(value.CLASS_SEQ_NO, value.SEQ_NO, value.SLOT_NAME, value.COLOR);
					}
				});
			}
			else
				slotstr = '일치하는 Slot이 없습니다. <a href="#" onclick="fnOpenSlot();">&lt;직접선택&gt;</a>';
			
			$('#divSlotLayerCon').empty().append(slotstr);
			$('#divSlotLayer').css({
			     "top": divTop
			     ,"left": divLeft
			     , "position": "absolute"
			}).show();
		}
		
		function fnGetNotUsingClass(classSeq, slotSeq, slotName, slotColor) {
			$('#hidSlotAdd').val(classSeq + '|' + slotSeq + '|' + slotName + '|' + slotColor)
			var comAjax = new ComAjax();
			comAjax.addParam("CLASS_SEQ_NO", classSeq);			
			comAjax.setUrl("<c:url value='/view/getparentclass.do' />");			
			comAjax.setCallback("fnGetNotUsingClassCallBack");
			comAjax.ajax();
		}
		
		function fnGetNotUsingClassCallBack(data) {
			try {
				var arrobj = $('#hidSlotAdd').val().split('|');
				$.each(data.list, function(key, value){
					$.each(slotdata2[value.CLASS_SEQ_NO], function(key, value){
						if (value.TYPE_SEQ == arrobj[0]) {
							arrobj[0] = value.CLASS_SEQ_NO;
							arrobj[2] = value.SLOT_NAME + '.' + arrobj[2];
							fnGetSlotColor(arrobj[0], arrobj[2]);
							slotstr += '<a href="#" onclick="fnSetSlot(\'' + value.CLASS_SEQ_NO + '_' + arrobj[1] + '\', \'' + value.CLASS_NAME + '.' + arrobj[2] + '\', \'' + $('#hidSlotColor').val() + '\', true);">' + value.CLASS_NAME + '.' + arrobj[2] + '</a>';
							$('#hidSlotColor').val('');
						}
					});
				});
				$('#hidSlotAdd').val('');	
			} catch (e) {
				// TODO: handle exception				
			}
		}
		
		function fnGetSlotColor(classSeq, slotName) {			
			var comAjax = new ComAjax();
			comAjax.addParam("CLASS_SEQ_NO", classSeq);
			comAjax.addParam("SLOT_NAME", slotName);
			comAjax.setUrl("<c:url value='/view/getslotcolor.do' />");			
			comAjax.setCallback("fnGetSlotColorCallBack");
			comAjax.ajax();
		}
		
		function fnGetSlotColorCallBack(data) {
			try {
				$('#hidSlotColor').val(data.COLOR);
			} catch (e) {
				// TODO: handle exception
			}	
		}
		
		function fnSetSlot(seqno, slotname, color, bevent) {			
			var spos = UserSayRange.startOffset;
			var epos = UserSayRange.endOffset;
			var content = UserSayRange.startContainer.textContent;
			var text = UserSayRange.toString();
			
			var $WorkdivView = $WorkhidObj.prev(); 
			
						
			var sindex = $WorkdivView.html().indexOf(content);
			$WorkdivView.html($WorkdivView.html().substring(0, sindex + spos) + '<span style="color:' + color + '">' + text + '</span>' + $WorkdivView.html().substring(sindex + epos));
			var objSlot = new Object();
			objSlot.seqno = seqno;
			objSlot.slotname = slotname;
			objSlot.color = color;
			objSlot.text = text;
			
			arrSlot[text + '_' + color] = objSlot;
			bWorkChk = true;
		
			fnSlotClose();
			if (bevent) {
				fnpreventDefault(event);	
			}
		}
		
		function fnTagDeep(obj) {
			var returnval = ''
			$(obj).each(function() {
				if($(this).find('span').length == 0) {
					returnval = $(this).text(); 
				}
				else 
					returnval = fnTagDeep(this);
			});
			return returnval;
		}
		
		function fnSlotClose() {
			$('#divSlotLayer').hide();
		}
	
		function fnResponseAdd() {
			$('#divreplies').append($('#divrepliesAdd').html().replace(/§/g, $('#divreplies [name=useruttrw]').length));			
			fnpreventDefault(event);
		}			
		
		function fnResponseCancel(type) {
			$.modal.close();
			$("#divResponse").hide();
			fnpreventDefault(event);
		}
		
		function fnOpenPop(obj) {
			fnWinPop("<c:url value='/view/openfunctionwrite.do' />" + "?obj=" + $(obj).attr('id'), "IntentsPop", 571, 388, 0, 0);	
		}
		
		function fnOpenSlot() {
			fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=&Type=U", "AgentSearchpop", 430, 550, 0, 0);
		}
		
		function fnSave() {			
			var bNameChk = false;
			var bIntentSaveChk = false;
			var focusObj;
			
			$txtIntentsName = $('#txtIntentsName');
		
			if ($txtIntentsName.val() == 'Intent name' || $txtIntentsName.val().trim() == '') {
				alert('Intent명을 입력 하세요.');
				$txtIntentsName.focus();
				fnpreventDefault(event);
				return false;
			}
			
			var reg = /[^a-zA-Z0-9_\-=()."]/;
			if (reg.test($txtIntentsName.val())) {
				alert('Intent명에 한글,공백,특수문자를 입력 할 수 없습니다.');
				$txtIntentsName.focus();
				fnpreventDefault(event);
				return false;
			}
				
			if (jsondata[${AGENT_SEQ}] != undefined) {
				var str = '';		
				$.each(jsondata[${AGENT_SEQ}], function(key, value){
					if('${SEQ_NO}' == '' && iSeqNo == '') {
						if (value.INTENT_NAME == $txtIntentsName.val()) {
							bNameChk = true;
							return;
						}
					}
					else if (iSeqNo != ''){
						if (value.SEQ_NO.toString() != iSeqNo && value.INTENT_NAME == $txtIntentsName.val()) {
							bNameChk = true;
							return;
						}
					}
					else {
						if (value.SEQ_NO.toString() != '${SEQ_NO}' && value.INTENT_NAME == $txtIntentsName.val()) {
							bNameChk = true;
							return;
						}
					}
				});							
			}
			
			if (bNameChk) {
				alert('이미 존재하는 Intent명 입니다.');
				fnpreventDefault(event);
				return false;
			}
			
			fnLoading();
		
			var objusersay = new Object();			
			var arrusersay = new Array();
			var bSaveChk = true;
									
			$('#divUserSays [name=USER_SAYS]').each(function(idx, e) {
				$ObjUserSay = $(this);
				if(!gfn_isNull($ObjUserSay.text())) {
					var usitem = new Object();
					usitem.SAY = $ObjUserSay.text();
					usitem.SAY_TAG = $ObjUserSay.html();
									
					var slotarray = new Array();					
					var arrslotseq = new Array();
					var arrslotname = new Array();
					var arrslotmapda = new Array();
					var slottag = usitem.SAY_TAG;
					var slotmap = usitem.SAY_TAG;
							
					try {
						$(this).find('span').each(function() {
							var text = $(this).text();//fnTagDeep(this);					
							var arr = arrSlot[text + '_' + fnHexc($(this).css('color'))];
							arrslotseq.push(arr.seqno);
							arrslotname.push(arr.slotname + '=value');
							arrslotmapda.push(arr.slotname + '="' + text.trim() + '"');
							slottag = slottag.replace('>' + text + '</', '>@' + arr.slotname + '</');
							var reg = new RegExp('<span style=\"color:#\\w+\">' + text + '<\/span>','g');;
							slotmap = slotmap.replace(reg, '<' + arr.slotname + '=' + text.trim() + '>');
						});	
					} catch (e) {
						bSaveChk = false;
						fnLoading();
						alert('User says 저장 중 [' + usitem.SAY + '] 문장에서 문제를 발견 하였습니다. 문장을 다시 작성해주세요.');
						return;
					}
					
					
					arrslotseq.sort();
					
					usitem.SLOT_SEQ_NO = arrslotseq.toString();
					usitem.SLOT_TAG = slottag;
					usitem.SLOT_NAME = arrslotname.toString();
					usitem.SLOT_MAP = slotmap;
					
					if (usitem.SLOT_MAP == '') {
						usitem.SLOT_MAP = usitem.SAY; 
					}
					
					if(gfn_isNull($ObjUserSay.attr('id')))
						usitem.SEQ_NO = '0';
					else
						usitem.SEQ_NO = $ObjUserSay.attr('id');
					
					usitem.SLOT_MAPDA = arrslotmapda.toString().replace(/,/g, ', ');
					arrusersay.push(usitem);					
				}				
			});
			
			if (bSaveChk) {						
				objusersay.USER_SAYS = arrusersay;
				var usersayobj = JSON.stringify(objusersay);
				
				var arr = new Object();
				var mainarrobj = new Array();
				
				$('#divreplies [name=useruttrw]').each(function(idx, e) {							
					var $this = $(this);
					var mainitem = new Object();
					var arrobj = new Array();
									
					mainitem.UTTR = $this.find('[name=txtUttr]').val();
					mainitem.ACT = $this.find('[name=txtAction]').val().replace(/\+/g, "%2B");
					mainitem.SLOT_SEQ_NO = '${SLOT_SEQ_NO}';
					
					if(mainitem.UTTR.trim() == '')
						mainitem.UTTR = 'true';
					
					if(!gfn_isNull(mainitem.UTTR)) {
						$this.find('[name=tbOBJ_LIST] tr').each(function(idx, e) {
							var $obj = $(this);
							var item = new Object();
							var arrobjsub = new Array();
							
							item.OBJECT_VALUE = $obj.find('[name=OBJECT_NAME]').text();
							
							if (reg.test(item.OBJECT_VALUE)) {
								bIntentSaveChk = true;
								focusObj = $obj.find('[name=OBJECT_NAME]');
								return false;							
							}
							
							if(!gfn_isNull(item.OBJECT_VALUE)) {
								if (item.OBJECT_VALUE.toLowerCase().indexOf('request') > -1) {
									$obj.find('[name=OBJECT_REQUEST]').each(function() {
										if(!gfn_isNull($(this).text())) {
											var subitem = new Object();
											subitem.OBJECT_VALUE = fnReplaceSpace($(this).text());
											subitem.TYPE = 'R';
											arrobjsub.push(subitem);
										}
									});
									
									$obj.find('[name=OBJECT_REQUEST_PATTERN]').each(function() {
										if(!gfn_isNull($(this).text())) {
											var subitem = new Object();
											subitem.OBJECT_VALUE = fnReplaceSpace($(this).text());
											subitem.TYPE = 'P';
											arrobjsub.push(subitem);
										}
									});
								}
								else {
									$obj.find('[name=OBJECT_DETAIL_NAME]').each(function() {
										if(!gfn_isNull($(this).text())) {
											var subitem = new Object();
											subitem.TYPE = 'T';
											subitem.OBJECT_VALUE = fnReplaceSpace($(this).text());						
											arrobjsub.push(subitem);
										}
									});		
								}
							}											
							
							if (arrobjsub.length > 0) {
								item.OBJECT_DETAIL = arrobjsub;
								
								arrobj.push(item);	
							}						
						});
						mainitem.OBJECT = arrobj;
						mainarrobj.push(mainitem);
					}
				});
				
				if (bIntentSaveChk) {
					fnLoading();
					alert('DA명에 한글,공백,특수문자를 입력 할 수 없습니다.');
					focusObj.focus();
					return false;
				}
				
				arr.OBJECT_ITEM = mainarrobj;
				var jsoobj = JSON.stringify(arr);
				
				var comAjax = new ComAjax();				
				comAjax.setUrl("<c:url value='/view/insertintent.do' />");
				comAjax.setCallback("fnSaveComplete");
				comAjax.addParam("INTENT_NAME", $txtIntentsName.val());
				comAjax.addParam("USER_SAYS", usersayobj);
				comAjax.addParam("OBJECT_ITEM", jsoobj);
				comAjax.addParam("SLOT_SEQ_NO", '${SLOT_SEQ_NO}');
				if('${SEQ_NO}' == '') {
					if (iSeqNo == '')
						comAjax.addParam("SEQ_NO", '');
					else
						comAjax.addParam("SEQ_NO", iSeqNo);
				}
				else
					comAjax.addParam("SEQ_NO", '${SEQ_NO}');
				comAjax.addParam("depth1", '${depth1}');
				comAjax.addParam("depth2", '${depth2}');
				comAjax.addParam("depth3", '${depth3}');
				if('${AGENT_SEQ}' == '')
					comAjax.addParam("AGENT_SEQ", '0');
				else
					comAjax.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comAjax.ajax();
				/* var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/insertintent.do' />");
				comSubmit.addParam("INTENT_NAME", $txtIntentsName.val());
				comSubmit.addParam("USER_SAYS", usersayobj);
				comSubmit.addParam("OBJECT_ITEM", jsoobj);
				comSubmit.addParam("SLOT_SEQ_NO", '${SLOT_SEQ_NO}');
				if('${SEQ_NO}' == '') 
					comSubmit.addParam("SEQ_NO", '');
				else
					comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				if('${AGENT_SEQ}' == '')
					comSubmit.addParam("AGENT_SEQ", '0');
				else
					comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.submit(); */			
				fnpreventDefault(event);
			}
		}
		
		function fnUserSayDelete(SlotSeqNo, Seqno, obj) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comAjax = new ComAjax();
				if (SlotSeqNo == '') 
					comAjax.setUrl("<c:url value='/view/deleteintentusersaydtl.do' />");	
				else
					comAjax.setUrl("<c:url value='/view/deleteintentusersay.do' />");
				comAjax.setCallback("");
				comAjax.addParam("SLOT_SEQ_NO", SlotSeqNo);
				comAjax.addParam("SEQ_NO", Seqno);
				comAjax.ajax();	
				$(obj).parent().remove();
			}
			fnpreventDefault(event);
		}
		
		function fnUserSayEdit(Seqno, obj) {			
			if ('${SLOT_SEQ_NO}' != '') {
				var arrMapList = $(obj).prev().val().split(',');
				var sname;
				var tempidx;
				var text;
				
				for (var i = 0; i < arrMapList.length; i++) {
					arrMapList[i] = arrMapList[i].trim();
					sname = arrMapList[i].substr(0, arrMapList[i].indexOf("="));
					tempidx = arrMapList[i].indexOf("=") + 2;
					text = arrMapList[i].substr(tempidx, arrMapList[i].length - tempidx - 1);
					
					var objSlot = new Object();
					objSlot.seqno = arrSlotList[sname].seqno;
					objSlot.slotname = arrSlotList[sname].slotname;
					objSlot.color = arrSlotList[sname].color;
					objSlot.text = text;
					
					arrSlot[text + '_' + objSlot.color] = objSlot;
				}				
			}
			$('#' + Seqno).attr("contenteditable", "true").attr("name", "USER_SAYS");
			fnCursorEnd($('#' + Seqno).get(0));
			
			fnpreventDefault(event);
		}
		
		function fnUserSayUndo(Seqno, obj) {
			$UsreSays = $('#' + Seqno);
			if ('${SLOT_SEQ_NO}' != '') {
				$UsreSays.html($UsreSays.text());					
			}
			$UsreSays.attr("contenteditable", "true").attr("name", "USER_SAYS");
			fnCursorEnd($UsreSays.get(0));
			
			fnpreventDefault(event);
		}
		
		function fnUserSayDetailDelete(SeqNo, obj) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comAjax = new ComAjax();
				comAjax.setUrl("<c:url value='/view/deleteintentusersaydtl.do' />");
				comAjax.setCallback("");
				comAjax.addParam("SEQ_NO", SeqNo);
				comAjax.ajax();
				$(obj).parent().remove();
			}
			fnpreventDefault(event);
		}
		
		function fnUserObjectDelete(SeqNo, obj) {					
			$(obj).parent().remove();
			bWorkChk = true;
		}		
		
		function fnUserSayDetailList(SlotNo, IntentSeq) {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/useruttredit.do' />");			
			comSubmit.addParam("SLOT_SEQ_NO", SlotNo);
			comSubmit.addParam("INTENT_SEQ", IntentSeq);
			comSubmit.addParam("INTENT_NAME", '${INTENT_NAME}');
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.addParam("INTENTS_LIST", '${INTENTS_LIST}');
			comSubmit.submit();			
			
			/* var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/getintentusersays.do' />");
			comAjax.setCallback("fnUserSayDetailListCallBack");
			comAjax.addParam("SLOT_SEQ_NO", SlotNo);			
			comAjax.ajax(); */	
		}
		
		function fnUserSayDetailListCallBack(data) {			
			if(data.list.length > 0) {
				var str = '<h3 class="user_says_icon">User says<a onclick="fnList();" class="class_name_instances" href="#">List</a></h3>';
				$.each(data.list, function(key, value){						
					str += '<div class="user_says_add_con">';																
					str += '<div class="user_says_add_con_inp" name="EDIT_USER_SAYS">' + value.SAY_TAG + '</div>';
					str += '<a href="#" onclick="fnUserSayDetailDelete(' + value.SEQ_NO + ', this);" class="delete_btn"><span class="hide">삭제</span></a>';					
					str += '</div>';
				});
				$('#divUserSaysDetail').empty().append(str).show();
				$('#divUserSays').hide();
			}
		}
		
		function fnList() {
			$('#divUserSaysDetail').hide();
			$('#divUserSays').show();
		}
		
		function fnSetIntentsName(intentName, slotName) {
			var viewintentName = '';
		
			if (intentName.indexOf("(") > -1) {
				if (intentName.indexOf("(") + 1 == intentName.indexOf(")")) {
					var tempName = intentName.substring(0, intentName.indexOf("("));
					viewintentName = tempName + '(' + slotName + ')';
				}
				else {
					var tempName = intentName.substring(0, intentName.indexOf("("));
					var tempMapda = intentName.substring(intentName.indexOf("(") + 1, intentName.indexOf(")"));
					
					if (slotName == '') {
						viewintentName = intentName;
					}
					else {
						viewintentName = tempName + '(' + tempMapda + ', ' + slotName + ')';
					}
				}
			}
			else {
				viewintentName = intentName + '(' + slotName + ')';	
			}
			$('#viewIntentsName').val(viewintentName);
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>	
	<div class="user_utterance">
		<div class="Agent_name_box">
			<div class="Agent_name_box_word">
	            <div class="s_word">
	                <label for="search">
	                	<c:choose>
	                		<c:when test="${null eq INTENT_NAME}">
	                			<input type="text" id="txtIntentsName" name="txtIntentsName" value="Intent name" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" />
	                		</c:when>
	                		<c:otherwise>
	                			<c:choose>
	                				<c:when test="${empty SLOT_SEQ_NO}">
	                					<input type="text" id="txtIntentsName" name="txtIntentsName" value='${INTENT_NAME}' />
                					</c:when>
                					<c:otherwise>                						
                						<input type="text" id="viewIntentsName" readonly="readonly" value='' />
                						<input type="hidden" id="txtIntentsName" name="txtIntentsName" value='${INTENT_NAME}' />
                						<script>fnSetIntentsName('${INTENT_NAME}', '${UserSaylist[0].SLOT_NAME }');</script>
                					</c:otherwise>
               					</c:choose>
	                		</c:otherwise>
	                	</c:choose>
	                    
	                    <a href="#" id="saveIntents" onclick="fnSave();" title="저장"><span class="project_save">Save</span></a>
	                </label>
	            </div>
            </div>
        </div>
		<div class="user_utterance_contents">
			<div id="divSlotLayer" class="slot_layer" style="display:none;">
				<a href="#" class="hide divSlotLayer_c" onclick="fnSlotClose();">닫기</a>
				<div id="divSlotLayerCon">
				</div>				
			</div>
			<!-- step01 -->
			<div class="user_utterance_step01">
				<div id="divUserSays" class="sortable01">
					<h3 class="user_says_icon">User says</h3>
																			
					<c:forEach items="${UserSaylist }" var="row">
						<c:choose>						
							<c:when test="${empty SLOT_SEQ_NO}">																
								<div class="user_says_add_con">																
									<div class="user_says_add_con_inp" id="${row.SEQ_NO }" name="EDIT_USER_SAYS" >${row.SLOT_TAG}</div>
									<a href="#" onclick="fnUserSayDelete('${row.SLOT_SEQ_NO}', ${row.SEQ_NO }, this);" class="delete_btn"><span class="hide">삭제</span></a>									
									<c:choose>						
										<c:when test='${row.SLOT_SEQ_NO ne ""}'>
											<a href="#" onclick="fnUserSayDetailList('${row.SLOT_SEQ_NO}', ${SEQ_NO });" class="list_btn"><span class="hide">리스트</span></a>
										</c:when>
										<c:otherwise>											
											<a href="#" onclick="fnUserSayEdit(${row.SEQ_NO }, this);" class="modify_btn"><span class="hide">리스트</span></a>
											<a href="#" onclick="fnUserSayUndo(${row.SEQ_NO }, this);" class="undo_btn"><span class="hide">리스트</span></a>
										</c:otherwise>
									</c:choose>
								</div>	
							</c:when>
							<c:otherwise>								
								<div class="user_says_add_con">																
									<div class="user_says_add_con_inp" id="${row.SEQ_NO }" name="EDIT_USER_SAYS">${row.SAY_TAG }</div>									
									<a href="#" onclick="fnUserSayDetailDelete(${row.SEQ_NO}, this);" class="delete_btn"><span class="hide">삭제</span></a>
									<input type="hidden" name="hidUserSayEdit" value='${row.SLOT_MAPDA }' />
									<a href="#" onclick="fnUserSayEdit(${row.SEQ_NO }, this);" class="modify_btn"><span class="hide">리스트</span></a>
									<a href="#" onclick="fnUserSayUndo(${row.SEQ_NO }, this);" class="undo_btn"><span class="hide">리스트</span></a>
								</div>
							</c:otherwise>
						</c:choose>					
						
					</c:forEach>
				</div>
				
				<div id="divUserSaysDetail" class="sortable01" style="display:none;">								
				</div>
			</div>			
			
			<!-- step04 -->
			<div class="user_utterance_step04">
				<div id="divreplies" class="sortable04">
					<h3 class="speech_response_icon">System replies : <a onclick="fnResponseAdd();" href="#">+ Add</a></h3>
					<c:choose>						
						<c:when test="${fn:length(Intentionlist) > 0}">
							<c:forEach items="${Intentionlist}" var="row">
								<div class="useruttrw" name="useruttrw">
									<a class="useruttrw_del" href="#" onclick="fnUserObjectDelete(${row.SEQ_NO}, this);">delete</a>
									<div class="useruttrw01">								
										<input type="text" readonly="readonly" onclick="fnOpenPop(this);" id="txtEUttr${row.SEQ_NO}" name="txtUttr" value='${row.UTTR}' />

										<div class="useruttrw02">																
											<table>
												<colgroup><col width="27px" /><col width="*" /><col width="27px" /></colgroup>
												<tr>
													<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
													<td><input type="text" <c:if test="${empty row.ACT}">style="display:none;"</c:if> readonly="readonly" onclick="fnOpenPop(this);" id="txtEAction${row.SEQ_NO}" name="txtAction" value='${row.ACT}' /></td>
													<td style="text-align:center;"><img <c:if test="${empty row.ACT}">style="display:none;"</c:if> name="imgActionDel" src="../images/close_btn.png" /></td>
												</tr>
											</table>										
										</div>
									</div>
									<div class="uuw_tab">
										<table name="tbOBJ_LIST" class="slot_modify">
											<c:choose>															
												<c:when test="${fn:length(Objectlist) > 0}">
													<c:forEach items="${Objectlist}" var="orow">
														<c:if test="${row.SEQ_NO eq orow.INTENTION_SEQ}">															
															<tr>
																<th>
																	<div name="OBJECT_NAME" contenteditable="true">${orow.OBJECT_VALUE}</div>
																</th>
																<td class="slot_modify01">
																	<c:forEach items="${ObjectDtllist}" var="drow">
																		<c:if test="${orow.SEQ_NO eq drow.OBJECT_SEQ}">													
																			<c:choose>						
																				<c:when test='${drow.OBJECT_TYPE eq "T"}'>
																					<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"><c:out value="${drow.OBJECT_VALUE }"/></div><a class="close_btn f_right" href="#"></a></div>
																				</c:when>
																				<c:when test='${drow.OBJECT_TYPE eq "R"}'>
																					<div class="slot_modify_set02 style_bu"><div class="dp_ib" name="OBJECT_REQUEST" contenteditable="true"><c:out value="${drow.OBJECT_VALUE }"/></div><a class="close_btn f_right" href="#"></a></div>
																				</c:when>
																				<c:otherwise>																					
																					<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_REQUEST_PATTERN" contenteditable="true"><c:out value="${drow.OBJECT_VALUE }"/></div><a class="close_btn f_right" href="#"></a></div>
																				</c:otherwise>
																			</c:choose>			
																		</c:if>											
																	</c:forEach>																																							
																</td>
															</tr>														
														</c:if>
													</c:forEach>											
												</c:when>
												<c:otherwise>										
													<tr>
														<th>
															<div name="OBJECT_NAME" contenteditable="true">inform()</div>
														</th>
														<td class="slot_modify01">																			
															<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>									
														</td>
													</tr>										
												</c:otherwise>										
											</c:choose>			
										</table>
									</div>						
								</div>
							</c:forEach>							
						</c:when>
						<%-- <c:otherwise>
							<div class="useruttrw" name="useruttrw">
								<a class="useruttrw_del" href="#" onclick="fnUserObjectDelete(0, this);">delete</a>
								<div class="useruttrw01">								
									<input type="text" readonly="readonly" onclick="fnOpenPop(this);" id="txtUttr0" name="txtUttr" />
									
									<div class="useruttrw02">															
										<table>
											<colgroup><col width="27px" /><col width="*" /></colgroup>
											<tr>
												<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
												<td><input type="text" style="display:none;" readonly="readonly" onclick="fnOpenPop(this);" id="txtAction0" name="txtAction" /></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="uuw_tab">
									<table name="tbOBJ_LIST" class="slot_modify">
										<tr>
											<th>
												<div name="OBJECT_NAME" contenteditable="true">inform()</div>
											</th>
											<td class="slot_modify01">																			
												<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>									
											</td>
										</tr>
									</table>
								</div>
							</div>	
						</c:otherwise> --%>
					</c:choose>
					
					
					<!-- <ul id="sortable04">
						<li class="ui-state-default">
							<span>1</span>
							<a class="ui-state-default-list" href="#">$Time시를 선택 하셨습니다.</a><a href="#" class="delete_btn"><span class="hide">삭제</span></a>
						</li>
						<li class="ui-state-default">
							<span>2</span>
							<a class="ui-state-default-list" href="#">$Time시를 선택 하셨습니다.</a><a href="#" class="delete_btn"><span class="hide">삭제</span></a>
						</li>
						<li class="ui-state-default">
							<span>3</span>
							<a class="ui-state-default-list" href="#">$Time시를 선택 하셨습니다.</a><a href="#" class="delete_btn"><span class="hide">삭제</span></a>
						</li>
						<li class="ui-state-default">
							<span>4</span>
							<a class="ui-state-default-list" href="#">$Time시를 선택 하셨습니다.</a><a href="#" class="delete_btn"><span class="hide">삭제</span></a>
						</li>
					</ul> -->
				</div>
			</div>
			<!-- step04 -->
			
			
			
		</div>
	</div>	
	
	<div id="divAdd" style="display:none;">
		<div class="user_says_add_con">																
			<div class="user_says_add_con_inp" name="USER_SAYS" contenteditable="true"></div>			
			<input type="hidden" name="hidUserSays" />
			<!-- <input type="hidden" name="hidSlotTag" />
			<input type="hidden" name="hidSlotName" />
			<input type="hidden" name="hidSlotLeng" />
			<input type="hidden" name="hidSlotMap" />
			<input type="hidden" name="hidSlotMapDa" /> -->
			<!-- <a href="#" class="delete_btn"><span class="hide">삭제</span></a>
			<a href="#" class="list_btn"><span class="hide">리스트</span></a> -->
		</div>
	</div>
	
	<div id="divrepliesAdd" style="display:none;">
		<div class="useruttrw" name="useruttrw">
			<a class="useruttrw_del" href="#" onclick="fnUserObjectDelete(0, this);">delete</a>
			<div class="useruttrw01">								
				<input type="text" readonly="readonly" onclick="fnOpenPop(this);" id="txtUttr§" name="txtUttr" value="true" />
				
				<div class="useruttrw02">										
					<table>
						<colgroup><col width="27px" /><col width="*" /><col width="27px" /></colgroup>
						<tr>
							<td><img name="imgAction" src="../images/arr_uuw03.png" /></td>																
							<td><input type="text" style="display:none;" readonly="readonly" onclick="fnOpenPop(this);" id="txtAction§" name="txtAction" /></td>
							<td style="text-align:center;"><img style="display:none;" name="imgActionDel" src="../images/close_btn.png" /></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="uuw_tab">
				<table name="tbOBJ_LIST" class="slot_modify">
					<tr>
						<th>
							<div name="OBJECT_NAME" contenteditable="true">inform()</div>
						</th>
						<td class="slot_modify01">																			
							<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div id="divtbAdd" style="display:none;">
		<div class="uuw_tab">
			<table name="tbOBJ_LIST" class="slot_modify">
				<tr>
					<th>
						<div name="OBJECT_NAME" contenteditable="true">inform()</div>
					</th>
					<td class="slot_modify01">																			
						<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>									
					</td>
				</tr>
			</table>
		</div>
	</div>
	<input type="hidden" id="hidSlotAdd" />
	<input type="hidden" id="hidSlotColor" />
	<%@ include file="/WEB-INF/include/footer.jspf" %>
</body>
</html>