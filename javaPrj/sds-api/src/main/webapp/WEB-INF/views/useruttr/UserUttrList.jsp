<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		window.onload = function() {
			
		}

		function fnSearch() {
			var comSubmit = new ComSubmit();
			if ($('#slot_search').val() == 'Search intents…') 
				comSubmit.addParam("INTENT_NAME", "");				
			else
				comSubmit.addParam("INTENT_NAME", $('#slot_search').val());
			
			comSubmit.setUrl("<c:url value='/view/useruttrlist.do' />");
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.submit();
			fnpreventDefault(event);
		}
		
		function fnOpenCreatDatype() {		
			fnGoDetail(0);
			fnpreventDefault(event);
		}
		
		function fnGoDetail(Seq, AgentSeq) {
			var Name = $('#hid' + Seq).val();
			var comSubmit = new ComSubmit();
			if (Seq == 0) {
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.setUrl("<c:url value='/view/useruttrwrite.do' />");	
			}
			else {
				comSubmit.setUrl("<c:url value='/view/useruttredit.do' />");
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", AgentSeq);
				comSubmit.addParam("INTENT_SEQ", Seq);
				comSubmit.addParam("INTENT_NAME", Name);
				comSubmit.addParam("SLOT_SEQ_NO", "");
			}			
			comSubmit.addParam("INTENTS_LIST", '${INTENTS_LIST}');
			comSubmit.submit();
			fnpreventDefault(event);
		}		
		
		function fnDelete(Seq) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comSubmit = new ComSubmit();
				comSubmit.addParam("SEQ_NO", Seq);			
				comSubmit.setUrl("<c:url value='/view/deleteintent.do' />");
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.submit();
				fnpreventDefault(event);
			}
		} 		
		
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>
	<div class="user_utterance">
		<div class="cont_title">
			<h2>Intents</h2>
			<a onclick="fnOpenCreatDatype();" href="#">Create Intent</a>
		</div>
		<div class="user_utterance_contents">
		
			<!--search box start-->
			<div class="search_box">
				<div class="s_word">
					<label for="slot_search">
						<input type="text" value="Search intents…" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" id="slot_search" name="slot_search" /><a href="#" onclick="fnSearch();" title="검색" class="solt_search"><span class="hide">search</span></a>
					</label>
				</div>
			</div>
			<!--search box end-->
			
			<dl class="class_name">
				<!-- <dt>
					<a href="#" class="class_name_list">Default Fallback lntent</a><a href="#" class="delete_btn"><span class="hide">삭제</span></a>
				</dt>
				<dd>
					<a href="#" class="class_name_list">영화 선택</a><span class="twice">안녕하세요.</span><a href="#" class="delete_btn"><span class="hide">삭제</span></a>
				</dd> -->
				<c:set var="AgentName" value=""></c:set>
				<c:forEach items="${list }" var="row">			
					<c:choose>
						<c:when test="${null eq param.AGENT_SEQ && AgentName ne row.AGENT_NAME}">
							<h3 style="font-size: 20px; font-weight: normal; line-height: 24px; margin: 10px 45px 10px 45px;">${row.AGENT_NAME }</h3>
							<dd>
							<c:set var="AgentName" value="${row.AGENT_NAME}"></c:set>
						</c:when>									
						<c:otherwise>
							<dd>
						</c:otherwise>									
					</c:choose>						
						<a href="#" onclick="fnGoDetail(${row.SEQ_NO}, ${row.AGENT_SEQ});" class="class_name_list">${row.INTENT_NAME }</a><a href="#" class="delete_btn" onclick="fnDelete(${row.SEQ_NO});"><span class="hide">삭제</span></a>
						<input type="hidden" id="hid${row.SEQ_NO}" value='${row.INTENT_NAME }' />
					</dd>
				</c:forEach>
			</dl>
		</div>
	</div>
	<%@ include file="/WEB-INF/include/footer.jspf" %>
</body>
</html>