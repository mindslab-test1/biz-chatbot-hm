<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>Class/Slot 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery.fs.picker.css'/>" />		
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.fs.picker.js'/>"></script>	
	
	<script type="text/javascript">
		var bCopy = false;
		$(function () {
			if ('${param.type}' == 'v') {
				
			} else if ('${param.type}' == 'c') {
				$('#aCopy').show();
				
				$('[id^=popupck]').click(function() {
					$('[id^=popupck]').removeAttr('checked');
					this.checked = true;
				});
			}
			else if ('${param.type}' == 's') {
				<c:forEach items="${list }" var="row">
					$('#popupck${row.PROJECT_SEQ}').attr('checked', true);
				</c:forEach>				
			}					
		});
			
		function fnCheck(obj, prjseq) {			
			var flag = 'D';
			if ($(obj).is(":checked") == true) {
				flag = 'I';
			}
						
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/insertuserdomain.do' />");
			comAjax.addParam("USER_SEQ", ${param.userno});
			comAjax.addParam("PROJECT_SEQ", prjseq);
			comAjax.addParam("FLAG", flag);
			comAjax.ajax();					
		}
		
		function fnCopy() {	
			if (bCopy) {
				alert('복사중 입니다.');
				return false;
			}
			bCopy = true;
			fnLoading();
		
			var arrDomain = new Array();
			$('[id^=popupck]').each(function(e) {
				if ($(this).is(":checked") == true) {
					arrDomain.push($(this).val());
				}
			});
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/copydomain.do' />");
			comAjax.setCallback("fnCopyCallBack");
			comAjax.addParam("USER_SEQ", ${param.userno});
			comAjax.addParam("USER_ID", '${param.userid}');
			comAjax.addParam("PROJECT_SEQ", arrDomain.toString());							
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnCopyCallBack(data) {
			fnLoading();
			bCopy = false;
			if(data.state == 'OK') {
				if (data.namelist.length == 0) {
					alert('복사 되었습니다.');
					fnWinClose();	
				}
				else {
					var prjname = '';
					for (var i = 0; i < data.namelist.length; i++) {
						if (i == 0) 
							prjname += data.namelist[i];	
						else
							prjname += ", " + data.namelist[i];
					}
					alert(prjname + ' 프로젝트는 이미 존재 합니다.');					
				}					
			}
			else {
				alert('복사 중 오류가 발생 되었습니다.');
			}
		}
	</script>
</head>
<body style="min-width:485px;">
	<div class="popup_function">
    	<p>도메인 리스트</p>
        <dl class="function_list">
        	<dt>도메인</dt>
        	<c:choose>
        		<c:when test="${param.type eq 'v'}">
        			<c:forEach items="${prjlist }" var="row">
		    	        <dd class="function_list_bg01 ck_box">
			            	<span class="check popupck_d1">
			                    <label>${row.PROJECT_NAME}</label>
			                </span>
			            </dd>        							                
		        	</c:forEach>
        		</c:when>
        		<c:when test="${param.type eq 'c'}">
        			<c:forEach items="${prjlist }" var="row">
		    	        <dd class="function_list_bg01 ck_box">
			            	<span class="check popupck_d1">
			                    <input type="checkbox" id="popupck${row.SEQ_NO}" name="popupck${row.SEQ_NO}" value="${row.SEQ_NO}" style="margin: 8px;"/><label for="popupck${row.SEQ_NO}" style="vertical-align: text-top;">${row.PROJECT_NAME}</label>
			                </span>
			            </dd>        							                
		        	</c:forEach>
        		</c:when>
        		<c:when test="${param.type eq 's'}">
	        		<c:forEach items="${prjlist }" var="row">
		    	        <dd class="function_list_bg01 ck_box">
			            	<span class="check popupck_d1">
			                    <input type="checkbox" id="popupck${row.SEQ_NO}" name="popupck${row.SEQ_NO}" onclick="fnCheck(this, ${row.SEQ_NO});" style="margin: 8px;"/><label for="popupck${row.SEQ_NO}" style="vertical-align: text-top;">${row.PROJECT_NAME}</label>
			                </span>
			            </dd>        							                
		        	</c:forEach>
        		</c:when>
        	</c:choose>	        	
        </dl>
        <div class="layer_box_btn mt30">
        	<a id="aCopy" href="#" style="display:none;" onclick="fnCopy();">복사</a>            
            <a href="#" onclick="javascript:window.close();">닫기</a>
        </div>
    </div>
    
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>