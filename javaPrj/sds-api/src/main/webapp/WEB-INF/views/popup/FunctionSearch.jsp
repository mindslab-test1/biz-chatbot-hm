<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>함수 검색</title>

<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
<script type="text/javascript">
	$(function() {
		if(${fn:length(fnlist)} == 0 ) {
			$("#ddDefault").show();
		} else if(${fn:length(fnlist)} == 1 ) {
			$('.fna').click();
		}
		
		if('${search}' != '') {
			$('#txtSearch').val('${search}');
		}
		
		$('#txtSearch').keypress(function(e) {
			if(e.keyCode==13)
				fnSearch();
		});
	});
	
	function fnClose(fnname, paramcount) {
		opener.$('#txtSearch').val(fnname);
		opener.fnPramSet(paramcount, $('#' + fnname).val());
		fnWinClose();
	}
	
	function fnSearch() {
		var comSubmit = new ComSubmit();
		comSubmit.setUrl("<c:url value='/view/openfunctionsearch.do' />");
		comSubmit.addParam("FUNCTION_NAME", ($('#txtSearch').val() == 'search functions…') ? '' : $('#txtSearch').val());			
		comSubmit.submit();			
		fnpreventDefault(event);	
	}	
</script>

</head>
<body style="min-width:0px; overflow:auto;">
	<div class="popup_function">
    	<p>함수 검색</p>
        
        <!--search box start-->
        <div class="search_box">
            <div class="s_word">
                <label for="slot_search">
                    <input type="text" id="txtSearch" value="search functions…" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" />
                    <a onclick="fnSearch();" href="#" title="검색" class="solt_search"><span class="hide">search</span></a>
                </label>
            </div>
        </div>
        <!--search box end-->
        <dl class="function_list" style="margin-bottom:30px;">
        	<dt>함수명</dt>        	
			<c:forEach items="${fnlist }" var="row">				
				<c:choose>						
					<c:when test="${iCnt == 1}">
						<c:set var="iCnt" value="2"></c:set>
						<dd class="function_list_bg01"><a class="fna" onclick="fnClose('${row.FUNCTION_NAME }', ${row.PARAM_COUNT });" href="#">${row.FUNCTION_NAME }${row.DESCRIPTION }</a></dd>
						<input type="hidden" id="${row.FUNCTION_NAME }" value='${row.DESCRIPTION }' />			
					</c:when>
					<c:otherwise>
						<c:set var="iCnt" value="1"></c:set>
						<dd class="function_list_bg02"><a class="fna" onclick="fnClose('${row.FUNCTION_NAME }', ${row.PARAM_COUNT });" href="#">${row.FUNCTION_NAME }${row.DESCRIPTION }</a></dd>
						<input type="hidden" id="${row.FUNCTION_NAME }" value='${row.DESCRIPTION }' />
					</c:otherwise>
				</c:choose>
			</c:forEach>	
			<dd id="ddDefault" class="function_list_bg01" style="text-align:center; display:none;">검색 결과가 없습니다.</dd>		            
        </dl>
    </div>
    
    <form id="commonForm" name="commonForm">
	</form>
</body>
</html>