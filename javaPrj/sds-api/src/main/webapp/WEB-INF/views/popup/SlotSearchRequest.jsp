<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>Class/Slot 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery.fs.picker.css'/>" />		
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.fs.picker.js'/>"></script>	
	
	<script type="text/javascript">		
		var slotdata = jQuery.parseJSON('${SLOT_LIST2}');
		window.onload = function() {			
			$('.popupck dt a').click(function(e) {
				if($(this).attr('class') == 'ck_box_on') {
					$(this).attr('class', 'ck_box_off');
					$(this).parent().siblings().hide();
				}
				else {
					$(this).attr('class', 'ck_box_on');
					$(this).parent().siblings().show();
				}
				
				fnpreventDefault(e);
			});
			
			$('[id^=dd_]').each(function() {
				var arrid = $(this).attr('id').substr(3).split('_');				
				var str = '';
				
				str += fnAddSlotClass(arrid[0], arrid[1], arrid[0], $(this).text().trim(), '', 30);							
				//$(this).append($('#popupckC' + seqno).parents('dl').html()).find('dt').remove();
				$(this).append(str);
			});
			
			//checkbox
			$('input[type=checkbox]').picker({
				customClass: 'list_check'
			});
			//checkbox_end
			//$('#popupck${param.Type}${param.Seq}').picker('check');	
		}
		
		function fnAddSlotClass(typeseqno, seqno, idseqno, parentname, prevtype, leftpoint) {
			var str = '';			
			
			$.each(slotdata[typeseqno], function(key, value) {
				if (value.SLOT_TYPE == 'C') {					
					parentname += '.' + value.SLOT_NAME;
					
					if (prevtype == 'C') {						
						var id = 'O' + idseqno + '_' + value.SEQ_NO;
						str += '<dl><dt style="margin-left:' + leftpoint + 'px;"><a href="#" class="ck_box_on"></a><span class="check popupck_d1">';
						str += '<input type="checkbox" id="popupck' + id + '" name="popupck' + id + '" onclick="fnCheck(this, \'' + parentname + '\');" /><label for="popupck' + id + '" style="font-weight:normal;">' + parentname + '</label>';						
						str += '</span></dt>';
						leftpoint += 30;
					}					
					str += fnAddSlotClass(value.TYPE_SEQ, value.SEQ_NO, idseqno + '_' + value.TYPE_SEQ, parentname, value.SLOT_TYPE, leftpoint);
					
					if (prevtype == 'C')
						str += '</dl>';			
				}
				else {
					var id = 'O' + idseqno + '_' + value.SEQ_NO;
					str += '<dd style="margin-left:' + leftpoint + 'px;"><span class="check popupck_d2">';
					str += '<input type="checkbox" id="popupck' + id + '" name="popupck' + id + '" onclick="fnCheck(this, \'' + parentname + '.' + value.SLOT_NAME + '\');" /><label for="popupck' + id + '">' + parentname + '.' + value.SLOT_NAME + '</label>';
					str += '</span></dd>';
				}
			});
			
			return str;
		}
		
		function fnCheck(obj, slotname) {			
			//$('[id^=popupck]').picker("uncheck");
			$obj = $(obj);
			
			if ($obj.parent().find('input:hidden').length == 0) {
				$obj.picker('check');
				
				slotname = $(obj).parents().siblings('dt').eq(0).text().trim() + '.' + slotname;
				$obj.parent().append('<input type="hidden" name="SLOT_NAME" value="' + slotname + '" />');	
			}
			else {
				$obj.picker('uncheck');
				$obj.parent().find('input:hidden').remove();
			}						
		}
		
		function fnApply() {
			var iCnt = 0;
			var slotName = '';
			$('[name=SLOT_NAME]').each(function() {
				if (iCnt == 0)
					slotName = $(this).val();				
				else 
					slotName += ', ' + $(this).val();
				
				iCnt++;
			});
			
			if (iCnt == 0) {
				alert('슬롯을 선택하세요.');
			}
			else {
				opener.$('#tempid').text(slotName);
				opener.$('#tempid').removeAttr('id');
				opener.bWorkChk = true;
				fnWinClose();
			}
		}
		
	</script>
    <style>		
		.popupck {margin-bottom:25px; position:relative;}
		.popupck dt {margin-left:30px;}
		.popupck dt .picker-label {line-height:25px; padding-left:3px; font-size:16px; font-weight:bold; color:#474747;}
		.popupck dd {margin-left:55px; margin-top:5px;}
		.popupck dd .picker-label {line-height:25px; padding-left:3px; font-size:14px; color:#474747;}
		.popupck .picker-handle,
		.popupck .picker-handle {float:left !important; display:inline-block; vertical-align:middle;}
		.popupck .ck_box_on {width:20px; height:20px; display:inline-block; background: url(../images/btn_minus_off.png) no-repeat center center !important; background-size:18px 18px !important; position:absolute; left:0px; top:3px;}
		.popupck .ck_box_off {width:20px; height:20px; display:inline-block; background: url(../images/btn_Plus_off.png) no-repeat center center !important; background-size:18px 18px !important; position:absolute; left:0px; top:3px;}
    </style>
</head>
<body style="min-width:413px;">
	<div class="popup_function class_popup" style="width:360px !important; min-width:319px;">
		<p>Class/Slot 검색</p>
		
		<!--search box start-->
		<div class="search_box" style="display:none;">
			<div class="s_word">
				<label for="slot_search">
					<input type="text" style="width:80%;" id="txtSearch" value="search type…" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" /><a href="#" title="검색" class="solt_search" onclick="fnSearch();"><span class="hide">search</span></a>
				</label>
			</div>
		</div>
		<!--search box end-->
		<div class="popupck">
			<dl>				
				<c:forEach items="${SLOT_LIST }" var="row">			
					<c:choose>
						<c:when test="${row.SLOT_TYPE eq 'O'}">
							<dd <c:if test="${row.SUB_SLOT_TYPE eq 'C'}">id="dd_${row.TYPE_SEQ}_${row.SEQ_NO }"</c:if>>
								<span class="check popupck_d2">
									<input type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="popupck${row.SLOT_TYPE}${row.SEQ_NO}" onclick="fnCheck(this, '${row.TYPE_NAME}');" /><label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.TYPE_NAME}</label>									
								</span>
							</dd>							
						</c:when>
						<c:otherwise>
						</dl>
					</div>
							<div class="popupck">
								<dl>
									<dt>
										<a href="#" class="ck_box_on"></a>
										<span class="check popupck_d1">
											<label>${row.TYPE_NAME}</label>
										</span>
									</dt>
						</c:otherwise>
					</c:choose>					
				</c:forEach>	
			</dl>
		</div>
	
		<div class="layer_box_btn" style="margin-bottom:20px;">
			<a href="#" onclick="fnApply();">확인</a>
			<a href="#" onclick="fnWinClose();">취소</a>
		</div>
	</div>
	
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>