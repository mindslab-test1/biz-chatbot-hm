<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Cache-Control" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<title>Class/Slot 검색</title>
	
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/jquery.fs.picker.css'/>" />		
	<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/jquery.fs.picker.js'/>"></script>	
	
	<script type="text/javascript">	
		var slotdata = jQuery.parseJSON('${SLOT_LIST}');
		
		window.onload = function() {						
			$('.popupck dt a').click(function(e) {
				if($(this).attr('class') == 'ck_box_on') {
					$(this).attr('class', 'ck_box_off');
					$(this).parent().siblings().hide();
				}
				else {
					$(this).attr('class', 'ck_box_on');
					$(this).parent().siblings().show();
				}
				
				fnpreventDefault(e);
			});
											
			if ('${param.pType}' == 'T') {
				$('#popupckC${param.ClassSeq}').parent().parent().parent().parent().hide();
				fnGetNotUsingClass();
			}
			else {
				$('.popupck').eq(0).hide();
			}
				
			$('[id^=dd_]').each(function() {
				var arrid = $(this).attr('id').substr(3).split('_');				
				var str = '';
				
				str += fnAddSlotClass(arrid[0], arrid[1], arrid[0], $(this).text().trim(), '', 30);							
				//$(this).append($('#popupckC' + seqno).parents('dl').html()).find('dt').remove();
				$(this).append(str);
			});
			
			//checkbox
			$('input[type=checkbox]').picker({
				customClass: 'list_check'
			});
			//checkbox_end
			if ('${param.SubSeq}' == '')
				$('#popupck${param.Type}${param.Seq}').picker('check');
			else {
				if('${param.pType}' == 'T')
					$('#popupck${param.SubSeq}').picker('check');
			}
		}
		
		function fnAddSlotClass(typeseqno, seqno, idseqno, parentname, prevtype, leftpoint) {
			var str = '';			
			
			$.each(slotdata[typeseqno], function(key, value) {
				if (value.SLOT_TYPE == 'C') {					
					parentname += '.' + value.SLOT_NAME;
					
					if (prevtype == 'C') {						
						var id = 'O' + idseqno + '_' + value.SEQ_NO;
						str += '<dl><dt style="margin-left:' + leftpoint + 'px;"><a href="#" class="ck_box_on"></a><span class="check popupck_d1">';
						str += '<input type="checkbox" id="popupck' + id + '" name="popupck' + id + '" onclick="fnCheck(this, \'O\', ' + value.SEQ_NO + ', \'' + parentname + '\', \'' + id + '\');" /><label for="popupck' + id + '" style="font-weight:normal;">' + parentname + '</label>';
						str += '</span></dt>';
						leftpoint += 30;
					}					
					str += fnAddSlotClass(value.TYPE_SEQ, value.SEQ_NO, idseqno + '_' + value.TYPE_SEQ, parentname, value.SLOT_TYPE, leftpoint);
					
					if (prevtype == 'C')
						str += '</dl>';			
				}
				else {
					var id = 'O' + idseqno + '_' + value.SEQ_NO;
					str += '<dd style="margin-left:' + leftpoint + 'px;"><span class="check popupck_d2">';
					str += '<input type="checkbox" id="popupck' + id + '" name="popupck' + id + '" onclick="fnCheck(this, \'O\', ' + value.SEQ_NO + ', \'' + parentname + '.' + value.SLOT_NAME + '\', \'' + id + '\');" /><label for="popupck' + id + '">' + parentname + '.' + value.SLOT_NAME + '</label>';
					str += '</span></dd>';
				}
			});
			
			return str;
		}
		
		function fnGetNotUsingClass() {
			var comAjax = new ComAjax();
			comAjax.addParam("CLASS_SEQ_NO", ${param.ClassSeq});			
			comAjax.setUrl("<c:url value='/view/getparentclass.do' />");
			comAjax.setCallback("fnGetNotUsingClassCallBack");
			comAjax.ajax();
		}
		
		function fnGetNotUsingClassCallBack(data) {
			$.each(data.list, function(key, value){
				$('#popupckC' + value.CLASS_SEQ_NO).parent().parent().parent().parent().hide();				
			});
		}
		
		function fnCheck(obj, slottype, typeseq, slotname, subid) {			
			$('[id^=popupck]').picker("uncheck");			
			$(obj).picker('check');
						
			if(slottype == 'S')
				slotname = 'Sys.' + slotname;
			else if(slottype == '') {
				if (type == '')
					slotname = $(obj).parent().parent().siblings('dt').text().trim() + '.' + slotname;	
				else {
					slotname = slotname;
				}
			}
				
			
			if ('${param.pType}' == 'T') {
				opener.$('#hidslottype').val(slottype);
				opener.$('#hidtypeseq').val(typeseq);
				opener.$('#TYPE_NAME').val(slotname);
				opener.$('#hidtypesubseq').val(subid)
			}
			else if ('${param.pType}' == 'P') {				
				opener.$('#hidPreceSlotSeq').val(typeseq);
				opener.$('#PRCE_SLOT_NAME').val(slotname);
				opener.$('#hidPreceSlotSubSeq').val(subid)
			}
			else if ('${param.pType}' == 'S') {
				opener.$('#hidSenseSeq').val(typeseq);
				opener.$('#SENSE_NAME').val(slotname);
			}
			else if ('${param.pType}' == 'D') {
				opener.$('#hidTargetSeq${param.hidSeq}').val(typeseq);
				opener.$('#TARGET_NAME${param.hidSeq}').val(slotname);
				opener.$('#hidTargetSubSeq${param.hidSeq}').val(subid);
			}
			
			fnWinClose();
			/* $('#hidslottype').val(slottype);
			$('#hidtypeseq').val(typeseq);
			$('#hidslotname').val(slotname); */			
		}
		
		function fnApply() {
			opener.$('#hidslottype').val($('#hidslottype').val());
			opener.$('#hidtypeseq').val($('#hidtypeseq').val());
			opener.$('#TYPE_NAME').val($('#hidslotname').val());
			fnWinClose();
		}
				
		function fnSearch() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/opentypesearch.do' />");
			comSubmit.addParam("TYPE_NAME", ($('#txtSearch').val() == 'search type…') ? '' : $('#txtSearch').val());			
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
	</script>
    <style>		
		.popupck {margin-bottom:25px; position:relative;}
		.popupck dt {margin-left:30px;}
		.popupck dt .picker-label {line-height:25px; padding-left:3px; font-size:16px; font-weight:bold; color:#474747;}
		.popupck dd {margin-left:55px; margin-top:5px;}
		.popupck dd .picker-label {line-height:25px; padding-left:3px; font-size:14px; color:#474747;}
		.popupck .picker-handle,
		.popupck .picker-handle {float:left !important; display:inline-block; vertical-align:middle;}
		.popupck .ck_box_on {width:20px; height:20px; display:inline-block; background: url(../images/btn_minus_off.png) no-repeat center center !important; background-size:18px 18px !important; position:absolute; left:0px; top:3px;}
		.popupck .ck_box_off {width:20px; height:20px; display:inline-block; background: url(../images/btn_Plus_off.png) no-repeat center center !important; background-size:18px 18px !important; position:absolute; left:0px; top:3px;}
    </style>
</head>
<body style="min-width:413px;">
	<div class="popup_function class_popup" style="width:360px !important; min-width:319px;">
		<p>Class/Slot 검색</p>
		
		<!--search box start-->
		<div class="search_box" style="display:none;">
			<div class="s_word">
				<label for="slot_search">
					<input type="text" style="width:80%;" id="txtSearch" value="search type…" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" /><a href="#" title="검색" class="solt_search" onclick="fnSearch();"><span class="hide">search</span></a>
				</label>
			</div>
		</div>
		<!--search box end-->
		<div class="popupck">
			<dl>
				<dt style="height: 20px; line-height: 25px;">
					<a href="#" class="ck_box_on"></a>
					<span class="check popupck_d1">
						<label>Sys</label>
					</span>
				</dt>			
		<c:forEach items="${TYPE_LIST }" var="row">			
			<c:choose>
				<c:when test="${row.SLOT_TYPE eq 'S' || row.SLOT_TYPE eq 'O'}">
					
					<dd <c:if test="${row.SUB_SLOT_TYPE eq 'C'}">id="dd_${row.TYPE_SEQ}_${row.SEQ_NO }"</c:if>>
						<span class="check popupck_d2">
							<input type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="popupck${row.SLOT_TYPE}${row.SEQ_NO}" onclick="fnCheck(this, '${row.SLOT_TYPE}', ${row.SEQ_NO}, '${row.TYPE_NAME}', '');" /><label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.TYPE_NAME}</label>
						</span>
					</dd>							
				</c:when>
				<c:otherwise>
						</dl>
					</div>
					<div class="popupck">
						<dl>
							<dt>
								<a href="#" class="ck_box_on"></a>
								<span class="check popupck_d1">
									<input type="checkbox" id="popupck${row.SLOT_TYPE}${row.SEQ_NO}" name="popupck${row.SLOT_TYPE}${row.SEQ_NO}" onclick="fnCheck(this, '${row.SLOT_TYPE}', ${row.SEQ_NO}, '${row.TYPE_NAME}', '');" /><label for="popupck${row.SLOT_TYPE}${row.SEQ_NO}">${row.TYPE_NAME}</label>
								</span>
							</dt>
				</c:otherwise>
			</c:choose>					
		</c:forEach>		
			</dl>
		</div>
		
		<input type="hidden" id="hidslottype" />
		<input type="hidden" id="hidtypeseq" />
		<input type="hidden" id="hidslotname" />		
		
		<div class="layer_box_btn" style="margin-bottom:20px;">
			<!-- <a href="#" onclick="fnApply();">적용</a> -->
			<a href="#" onclick="fnWinClose();">취소</a>
		</div>
	</div>
	
	<form id="commonForm" name="commonForm">
	</form>
</body>
</html>