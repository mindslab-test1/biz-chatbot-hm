<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<link rel="Stylesheet" type="text/css" href="<c:url value='/css/statemachine.css?ver=2'/>" />	
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/jsBezier-0.8.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/mottle-0.7.4.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/biltong-0.3.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/katavorio-0.18.0.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/util.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/browser-util.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/jsPlumb.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/dom-adapter.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/overlay-component.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/endpoint.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/connection.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/anchors.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/defaults.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/connectors-bezier.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/connectors-statemachine.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/renderers-svg.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/base-library-adapter.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/statemachinelib/dom.jsPlumb.js'/>"></script>
	
	<script type="text/javascript">
		jsPlumb.ready(function () {
			
		    // setup some defaults for jsPlumb.
		    var instance = jsPlumb.getInstance({
		        Endpoint: ["Dot", {radius: 2}],
		        Connector:"StateMachine",
		        HoverPaintStyle: {stroke: "#ea5404", strokeWidth: 2 },
		        ConnectionOverlays: [
		            [ "Arrow", {
		                location: 1,
		                id: "arrow",
		                length: 14,
		                foldback: 0.8
		            } ]/* ,
		            [ "Label", { label: "FOO", id: "label", cssClass: "aLabel" }] */
		        ],
		        Container: "canvas"
		    });
	
		    instance.registerConnectionType("basic", { anchor:"Continuous", connector:"StateMachine" });
	
		    window.jsp = instance;
	
		    var canvas = document.getElementById("canvas");
		    var windows = jsPlumb.getSelector(".statemachine-demo .w");
	
		    // bind a click listener to each connection; the connection is deleted. you could of course
		    // just do this: jsPlumb.bind("click", jsPlumb.detach), but I wanted to make it clear what was
		    // happening.
		    /* instance.bind("click", function (c) {
		    	if (confirm('해당 연결을 삭제 하시겠습니다?')) {
		    		instance.detach(c);	
				}		        
		    }); */
		    
		    
		    var fnClick = function(c) {
		    	fnLayerOpen(c.sourceId, c.targetId, $(c.source).text().trim(), $(c.target).text().trim());		    	
		    }
		    
		    var fnDblClick = function(c) {
		    	if (confirm('해당 연결을 삭제 하시겠습니다?')) {
		    		$('#hid' + c.sourceId + '_' + c.targetId).remove(); 
		    		instance.detach(c);
		    		//bWorkChk = true;
		    		fnDeleteCondition(c.sourceId, c.targetId);
		    		fnLayerClose();
				}
		    }
		    		    
		    var clickCnt = 0, timer = null;
		    
		    instance.bind("click", function (c) {
		    	clickCnt++;
	
		    	if (clickCnt === 1) {
		    		// click
		    		timer = setTimeout(function() {
		    			fnClick(c);
		    			clickCnt = 0;		
		            }, 300);		    				    	
		    	} else {
		    		// double click
		    		clearTimeout(timer);
		    		fnDblClick(c);
		    		clickCnt = 0;
		    	}		      
		    });
		    
		    instance.bind('dblclick', function(c, e) {
		    	// 기존 더블클릭 이벤트 우회
		    	fnpreventDefault(e);
		    });
		    		    	
		    // bind a connection listener. note that the parameter passed to this function contains more than
		    // just the new connection - see the documentation for a full list of what is included in 'info'.
		    // this listener sets the connection's internal
		    // id as the label overlay's text.
		    instance.bind("connection", function (info) {
		    	/* if (info.sourceId == info.targetId) {		    		
					alert('자기 자신은 연결 할 수 없습니다.');
					instance.detach(info);
					return false;
				} */
		    	
		    	fnLayerOpen(info.sourceId, info.targetId, $(info.source).text().trim(), $(info.target).text().trim());
		    	info.connection.getOverlay("label").setLabel(info.connection.id);		    	
		    });
	
		    // bind a double click listener to "canvas"; add new node when this occurs.
		    jsPlumb.on(canvas, "dblclick", function(e) {
		    	if (e.path[0].tagName == 'DIV' && !gfn_isNull(e.path[0].id) && e.path[0].id != 'canvas') {
		    		if (confirm('해당 Agent를 삭제 하시겠습니다?')) {
		    			fnDelete(e.path[0].id);	
			    	}	
				}
		        //newNode(e.offsetX, e.offsetY);
		    });
		    
		    jsPlumb.on(canvas, "mouseup", function(e) {
		    	//bWorkChk = true;
		    	
		    	if (e.path[0].tagName == 'DIV' && !gfn_isNull(e.path[0].id) && e.path[0].id != 'canvas') {
		    		$('#lnb .b2_btn_off').attr('class','b2_btn');
		    		$('#lnb .sub .on').removeClass('on'); 
		    		$('#lnb .sub02').slideUp();
		    		$spobj = $('#sp' + e.path[0].innerText);
		    		$spobj.attr('class','b2_btn_off');
		    		$spobj.parent().attr('class', 'on');
		    		$spobj.parent().parent().children('.sub02').slideDown();
		    	}
		        //newNode(e.offsetX, e.offsetY);
		    });
		    
		    //
		    // initialise element as connection targets and source.
		    //
		    var initNode = function(el) {
	
		        // initialise draggable elements.
		        instance.draggable(el);
	
		        instance.makeSource(el, {
		            filter: ".ep",
		            anchor: "Continuous",
		            connectorStyle: { stroke: "#ea5404", strokeWidth: 2, outlineStroke: "transparent", outlineWidth: 4 },
		            connectionType:"basic",
		            extract:{
		                "action":"the-action"
		            }/* ,
		            maxConnections: 2,
		            onMaxConnections: function (info, e) {
		                alert("Maximum connections (" + info.maxConnections + ") reached");
		            } */
		        });
	
		        instance.makeTarget(el, {
		            dropOptions: { hoverClass: "dragHover" },
		            anchor: "Continuous",
		            allowLoopback: true
		        });
	
		        // this is not part of the core demo functionality; it is a means for the Toolkit edition's wrapped
		        // version of this demo to find out about new nodes being added.
		        //
		        instance.fire("jsPlumbDemoNodeAdded", el);
		    };
	
		    var newNode = function(x, y) {
		        var d = document.createElement("div");
		        var id = jsPlumbUtil.uuid();
		        d.className = "w";
		        d.id = id;
		        d.innerHTML = id.substring(0, 7) + "<div class=\"ep\"></div>";
		        d.style.left = x + "px";
		        d.style.top = y + "px";
		        instance.getContainer().appendChild(d);
		        initNode(d);
		        return d;
		    };
	
		    // suspend drawing and initialise.
		    instance.batch(function () {
		        for (var i = 0; i < windows.length; i++) {
		            initNode(windows[i], true);
		        }
		        // and finally, make a few connections
		        var str = '';
		        <c:forEach items="${conlist}" var="row">
		        	instance.connect({ source: "${row.SOURCE_ID}", target: "${row.TARGET_ID}", type:"basic" });		        	
		        	str += '<input type="hidden" id="hid${row.SOURCE_ID}_${row.TARGET_ID}" name="hidConnection" value=\'${row._CONDITION}\' />';					
				</c:forEach>
				$('#hidgroup').append(str);
				fnLayerClose();
	/* 	        instance.connect({ source: "opened", target: "phone1", type:"basic" });
		        instance.connect({ source: "phone1", target: "phone1", type:"basic" });
		        instance.connect({ source: "phone1", target: "inperson", type:"basic" });
				
		        instance.connect({
		            source:"phone2",
		            target:"rejected",
		            type:"basic"
		        }); */
		    });
	
		    jsPlumb.fire("jsPlumbDemoLoaded", instance);
		    $("body").scrollTop(0);
		});
		
		var bConWork = false;
		window.onload = function() {
			$('#txtCon').bind({
				focusout: function(e) {
					if (bConWork) {
						fnSetCondition();
					}
				},
				keydown: function(e) {
					bConWork = true;
				}
			});
		}
	
		function fnLayerOpen(sourceId, targetId, sourceText, targetText) {			
			/* var offset = $('#' + targetId).offset();
			$('#divSlotLayer').css({
			     "top": offset.top - 25
			     ,"left": offset.left
			     , "position": "absolute"
			}).show(); */
			$('#divCon').show();
			$('#hConTitle').text('Transition Condition : ' + sourceText + ' ㅡ> ' + targetText);
			$('#txtCon').val($('#hid' + sourceId + '_' + targetId).val()).focus();
	    	$('#hidSourceId').val(sourceId);
	    	$('#hidTargetId').val(targetId);
		}
		function fnLayerClose() {
			$('#divCon').hide();
			$('#txtCon').val('');
			$('#hidSourceId').val('');
	    	$('#hidTargetId').val('');
			$('#divSlotLayer').hide();
			fnpreventDefault(event);
		}
		
		function fnOpenPop(obj) {
			fnWinPop("<c:url value='/view/openfunctionwrite.do' />" + "?obj=txtCon&Type=G", "AgentPop", 571, 388, 0, 0);	
		}
		
		function fnSetCondition() {			
			if ($('#txtCon').val().trim() == '') {
				alert('condition을 입력 하세요.');			
			}
			else {
				var comAjax = new ComAjax();
				comAjax.setUrl("<c:url value='/view/insertagentgraph.do' />");
				comAjax.setCallback("fnSetConditionCallBack");			
				comAjax.addParam("SOURCE_ID", $('#hidSourceId').val());
				comAjax.addParam("TARGET_ID", $('#hidTargetId').val());
				comAjax.addParam("CONDITION", $('#txtCon').val().trim());			
				comAjax.ajax();
								
				/* var $hid = $('#hid' + $('#hidSourceId').val() + '_' + $('#hidTargetId').val());	
			
				if ($hid.length == 0) {
					var str = '<input type="hidden" id="hid' + $('#hidSourceId').val() + '_' + $('#hidTargetId').val() + '" name="hidConnection" value=\'' + $('#txtCon').val() + '\' />';
					$('#hidgroup').append(str);	
				}
				else
					$hid.val($('#txtCon').val()); */
				
				$('#hid' + $('#hidSourceId').val() + '_' + $('#hidTargetId').val()).val($('#txtCon').val().trim());
				//alert('적용 되었습니다.');
				//fnLayerClose();
			}
			fnpreventDefault(event);
		}
		
		function fnLayerOpen(sourceId, targetId, sourceText, targetText) {			
			/* var offset = $('#' + targetId).offset();
			$('#divSlotLayer').css({
			     "top": offset.top - 25
			     ,"left": offset.left
			     , "position": "absolute"
			}).show(); */
			if (bConWork) {
				fnSetCondition();
			}
			$('#divCon').show();
			$('#hConTitle').text('Transition Condition : ' + sourceText + ' ㅡ> ' + targetText);
			$('#txtCon').val($('#hid' + sourceId + '_' + targetId).val()).focus();
	    	$('#hidSourceId').val(sourceId);
	    	$('#hidTargetId').val(targetId);
		}
		function fnLayerClose() {
			$('#divCon').hide();
			$('#txtCon').val('');
			$('#hidSourceId').val('');
	    	$('#hidTargetId').val('');
			$('#divSlotLayer').hide();
			fnpreventDefault(event);
		}
		
		function fnOpenPop(obj) {
			fnWinPop("<c:url value='/view/openfunctionwrite.do' />" + "?obj=txtCon&Type=G", "AgentPop", 571, 388, 0, 0);	
		}
		
		function fnSetCondition() {			
			bConWork = false;
/* 			if ($('#txtCon').val().trim() == '') {
				$('#txtCon').val('true');			
			} */
			
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/insertagentgraph.do' />");
			comAjax.setCallback("fnSetConditionCallBack");			
			comAjax.addParam("SOURCE_ID", $('#hidSourceId').val());
			comAjax.addParam("TARGET_ID", $('#hidTargetId').val());
			comAjax.addParam("CONDITION", $('#txtCon').val().trim());			
			comAjax.ajax();
							
			/* var $hid = $('#hid' + $('#hidSourceId').val() + '_' + $('#hidTargetId').val());	
		
			if ($hid.length == 0) {
				var str = '<input type="hidden" id="hid' + $('#hidSourceId').val() + '_' + $('#hidTargetId').val() + '" name="hidConnection" value=\'' + $('#txtCon').val() + '\' />';
				$('#hidgroup').append(str);	
			}
			else
				$hid.val($('#txtCon').val()); */
			
			$('#hid' + $('#hidSourceId').val() + '_' + $('#hidTargetId').val()).val($('#txtCon').val().trim());
			//alert('적용 되었습니다.');
			//fnLayerClose();
			
			fnpreventDefault(event);
		}
		
		function fnSetConditionCallBack(data) {			
			if (data.status != 'OK') {
				alert('연결식 저장 중 오류가 발생 하였습니다.');
			}
		}
		
		function fnSave() {
			var jsonObj = new Object();
			var agentArray = new Array();
			$('[name=divAgent]').each(function() {
				var subObj = new Object();
				var $this = $(this);
				subObj.Seq = $this.attr('id');
				subObj.Left = $this.css('left').replace('px', '');
				subObj.Top = $this.css('top').replace('px', '');
				agentArray.push(subObj);
			});
			
			/* var conArray = new Array();
			$('#hidgroup [name=hidConnection]').each(function() {
				var subObj = new Object();
				var conid = $(this).attr('id').replace('hid', '');				
				subObj.SourceId = conid.substring(0, conid.indexOf('_'));
				subObj.TargetId = conid.substring(conid.indexOf('_') + 1);
				subObj.Condition = $(this).val();
				conArray.push(subObj);
			}); */
			
			jsonObj.Agent = agentArray;
			//jsonObj.Connection = conArray;
			var strjson = JSON.stringify(jsonObj);
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/updateagentposition.do' />");			
			comSubmit.addParam("OBJECT", strjson);
			comSubmit.addParam("TYPE", "M");
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
		function fnDelete(Seq) {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/deleteagent.do' />");			
			comSubmit.addParam("SEQ_NO", Seq);
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
		function fnDeleteCondition(sourceId, targetId) {
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/deleteagentgraph.do' />");
			comAjax.setCallback("fnDeleteConditionCallBack");			
			comAjax.addParam("SOURCE_ID", sourceId);
			comAjax.addParam("TARGET_ID", targetId);			
			comAjax.ajax();
		}
		
		function fnDeleteConditionCallBack(data) {
			if (data.status != 'OK') {
				alert('연결식 저장 중 오류가 발생 하였습니다.');
			}
		}
	</script>
</head>
<body style="min-width:540px;">	
	<div id="contents" style="width:100%; min-width:500px; height:300px;">
		<div class="agent" style="padding-bottom:0px;">	
			<div class="cont_title" style="width:100%;">
			    <h2>Task Graph</h2>
			    <a onclick="fnSave();" href="#" class="btn_color02">Save</a>		    		    		 
			</div>
			<div class="agent_box">
				<div id="divSlotLayer" class="slot_layer agent_in" style="display:none; z-index:1;">
				<a href="#" class="hide divSlotLayer_c" onclick="fnLayerClose();">닫기</a>
				
				<!-- <div id="divSlotLayerCon" class="agent_txt">
					<table>
						<tr>
							<td style="text-indent: 10px;">condition:</td>
							<td><div id="txtCon" onclick="fnOpenPop(this);"></div> --><!-- <textarea id="txtCon" onclick="fnOpenPop(this);"></textarea> <input type="text" /></td>
							<td><a href="#" class="agent_save" onclick="fnSetCondition();">적용</a>	</td>
						</tr>
					</table>
					<input type="hidden" id="hidSourceId" />
					<input type="hidden" id="hidTargetId" />
				</div> -->			
			</div>
				
				<div class="jtk-demo-main">
		            <!-- demo -->
		            <div class="jtk-demo-canvas canvas-wide statemachine-demo jtk-surface jtk-surface-nopan" id="canvas" style="max-height:440px;">	            	
		            	<c:forEach items="${submenu }" var="row">	      			            		
	            		<div class="w" id="${row.SEQ_NO }" style="left:${row.LEFT_POS}px; top:${row.TOP_POS}px;" name="divAgent">${row.AGENT_NAME }
		                    <div class="ep" action="${row.SEQ_NO }"></div>
		                </div>	
					</c:forEach>
		            </div>
		            <!-- /demo -->            
		        </div>
		        <div id="divCon">
			        <div style="margin:0px 45px; line-height:26px;">
			        	<table style="width:100%;">
			        		<colgroup>
			        			<col width="80%" />
			        			<col width="*" />
			        		</colgroup>
			        		<tr>
			        			<td>
			        				<h3 id="hConTitle" style="font-size: 20px; font-weight: normal; line-height: 24px; margin: 0px 0px 10px 0px; display:inline-block;">Condition</h3>
			        			</td>
			        			<td>
				        			<!-- <a onclick="fnSetCondition();" class="class_name_createslot" href="#">적용</a> -->
				        			<a onclick="fnOpenPop(this);" class="class_name_instances" href="#">Script Guide</a>		        										
			        			</td>
			        		</tr>	        		
			        	</table>
			        </div>	        
			        <div class="graph_condition">
			        	<textarea id="txtCon"></textarea>
			        </div>
			        <input type="hidden" id="hidSourceId" />
					<input type="hidden" id="hidTargetId" />
		        </div>
			</div>
		</div>
		<div id="hidgroup">		
		</div>		
	</div>
	<form id="commonForm" name="commonForm">
	</form>	
</body>
</html>