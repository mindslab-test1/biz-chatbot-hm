<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title>함수생성</title>

<link rel="Stylesheet" type="text/css" href="<c:url value='/css/common.css'/>" />
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/common.js'/>"></script>

<script type="text/javascript">
	$(function() {
		$('#txtSearch').keypress(function(e) {
			if(e.keyCode==13)
				fnOpenPop();
		});
		
		$('.popup_function_btn a').click(function(e) {
			if($(this).text() == 'space')
				$('#txtResult').val($('#txtResult').val() + ' ');
			else if($(this).text() == 'delete')
				$('#txtResult').val('');
			else
				$('#txtResult').val($('#txtResult').val() + $(this).text() + ' ');
			fnpreventDefault(e);	
		});
		
		$('#addfn').click(function(e) {
			if(fnValidation())
				fnSetAdd();
			fnpreventDefault(e);
		});
		
		if(opener.$('#${obj}').get(0).tagName == 'DIV')
			$('#txtResult').val(opener.$('#${obj}').text());
		else
			$('#txtResult').val(opener.$('#${obj}').val());
	});
	
	function fnValidation() {
		if($('#txtSearch').val() == '') {
			alert('함수명을 입력하세요.');
			return false;			
		}
		
		var bcheck = true;		
		$('[name=popup_function_text]').each(function(e) {
			if($(this).css('display') != 'none') {
				if($(this).val() == '') {
					alert('파라미터 값을 입력하세요.');
					$(this).focus();
					bcheck = false;
					return false;
				}
			}
		});
		
		return bcheck;
	}
	
	function fnSetAdd() {
 		var addcomma = false;
 		var $txtResult = $('#txtResult');
		var value = $('#txtSearch').val() + '(';
		$('[name=popup_function_text]').each(function(e) {
			if($(this).css('display') != 'none') {
				if(addcomma) {
					 value += ', ';					
				} else
					addcomma = true;
					
				 value += '"' + $(this).val() + '"';
			}
		});
		value += ') ';
		$txtResult.val($txtResult.val() + value);
	}
	
	function fnOpenPop() {
		if($('#txtSearch').val() == 'search functions…')
			$('#txtSearch').val('');
		$('[name=popup_function_text]').val('');
		fnWinPop("<c:url value='/view/openfunctionsearch.do' />" + '?FUNCTION_NAME=' + $("#txtSearch").val(), "Searchpop", 581, 388, 50, 50);
		fnpreventDefault(event);
	}
	
	function fnPramSet(cnt, des) {				
		$txt01 = $('#popup_function_text01');
		$txt02 = $('#popup_function_text02');
		$txt03 = $('#popup_function_text03');			
		
		$('[name=popup_function_text]').hide();
		
		des = des.replace(/[\(\)\"]/g, '');
		var arrdes = des.split(',');
		
		if(cnt == 1) {
			$txt01.show();
			$txt01.val(arrdes[0]);
		} else if(cnt == 2) {
			$txt01.show();
			$txt02.show();
			$txt01.val(arrdes[0]);
			$txt02.val(arrdes[1]);
		} else if(cnt == 3) {
			$txt01.show();
			$txt02.show();
			$txt03.show();
			$txt01.val(arrdes[0]);
			$txt02.val(arrdes[1]);
			$txt03.val(arrdes[2]);
		}
		
		$('[name=popup_function_text]').each(function(e) {
			if ($(this).val() == '슬롯이름' || $(this).val() == '클래스이름') {
				$(this).click(function() {
					fnWinPop("<c:url value='/view/openfliingslotsearch.do' />" + "?obj=" + $(this).attr('id') + "&Type=F", "AgentSearchpop", 430, 550, 0, 0);
				});
			}
		});
	}
	
	function fnSetParent() {
		if($('#txtResult').val() == '') { 
			alert('생성된 값이 없습니다.')
			return false;
		}
		
		if(opener.$('#${obj}').get(0).tagName == 'DIV')
			opener.$('#${obj}').text($('#txtResult').val().trim());
		else
			opener.$('#${obj}').val($('#txtResult').val().trim());
		opener.bWorkChk = true;
		
		if ('${param.Type}' == 'G') {
			opener.bConWork = true;
			opener.$('#${obj}').focus();
		}
		
		fnWinClose();
	}
</script>

</head>
<body style="min-width:0px; overflow:auto;">
	<div class="popup_function">
    	<p>함수 생성</p>
        
        <!--search box start-->
        <div class="search_box">
            <div class="s_word">
                <label for="slot_search">
                    <input type="text" id="txtSearch" value="search functions…" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" id="slot_search" name="slot_search" />
                    <a onclick="fnOpenPop()" href="#" title="검색" class="solt_search"><span class="hide">search</span></a>
                </label>
            </div>
        </div>
        <!--search box end-->
        <div class="popup_function_text">
        	<label for="popup_function_text01">
                    <input type="text" id="popup_function_text01" name="popup_function_text" />
            </label>
        	<label for="popup_function_text02">
                    <input type="text" id="popup_function_text02" name="popup_function_text" />
            </label>
        	<label for="popup_function_text03">
                    <input type="text" id="popup_function_text03" name="popup_function_text" />
            </label>
            <a id="addfn" class="popup_function_add" href="#">적용</a>
        </div>
        <div class="popup_function_btn">        	
        	<a href="#">==</a>
        	<a href="#">~=</a>
        	<a href="#">&lt;</a>
        	<a href="#">&gt;</a>
        	<a href="#">&lt;=</a>
        	<a href="#">&gt;=</a>
        	<a href="#">true</a>
            <a href="#">false</a>        	
        	<a href="#">and</a>
            <a href="#">or</a>
            <a href="#">(</a>
            <a href="#">)</a>                                      
            <a href="#">space</a>
            <!-- <a href="#">delete</a> -->
        </div>
        <div class="popup_function_result">
            <label for="popup_function_text04">
                    <!-- <input type="text" id="txtResult" /> -->
                    <textarea type="text" id="txtResult"></textarea>
            </label>
        </div>
        <div class="layer_box_btn">
            <a onclick="fnSetParent();" href="#">OK</a>
            <a onclick="fnWinClose();" href="#">Cancel</a>
        </div>        
    </div>
</body>
</html>