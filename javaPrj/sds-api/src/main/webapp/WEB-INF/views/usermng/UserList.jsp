<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		window.onload = function() {
			fnSelectList(1);
		}
		
		function fnSelectList(pageNo){
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/selectUserList.do' />");
			comAjax.setCallback("fnSelectListCallBack");
			comAjax.addParam("PAGE_INDEX",$("#PAGE_INDEX").val());
			comAjax.addParam("PAGE_ROW", 10);
			comAjax.ajax();
			fnpreventDefault(event);
		}
		
		function fnSelectListCallBack(data) {
			var total = data.TOTAL;
			var rowcount = data.ROWCNT;
			var body = $("#tbusers > tbody");
			body.empty();
			if(total == 0){
				AddRow(body);
			}
			else{
				var params = {
					divId : "PAGE_NAVI",
					pageIndex : "PAGE_INDEX",
					totalCount : total,
					recordCount : rowcount,
					eventName : "fnSelectList"
					
				};
				gfn_renderPaging(params);
				var iRowColor = true;
				var str = '';
				$.each(data.list, function(key, value){
					if (iRowColor) {
						str += '<tr class="admin_user_list_bg01">';
						iRowColor = false;
					}
					else {
						str += '<tr class="admin_user_list_bg02">';
						iRowColor = true;
					}
											
					str += '<td><a href="#" onclick="fnWrite('+value.SEQ_NO+');" >'+value.USER_ID+'</a></td>';						
					str += '<td>'+value.USER_NAME+'<a href="#" onclick="fnDelete('+value.SEQ_NO+');" class="delete_btn"><span class="hide">삭제</span></a></td>';									
					str +='</tr>'; 
				});
				
				body.append(str);
			}
		}
			
		function fnWrite(seqno) {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/userwrite.do' />");
			comSubmit.addParam("SEQ_NO", seqno);			
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
		function fnDelete(seqno) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/userdelete.do' />");
				comSubmit.addParam("SEQ_NO", seqno);			
				comSubmit.submit();			
				fnpreventDefault(event);	
			}
		}
		
		function fnOpenLua() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/lualist.do' />");					
			comSubmit.submit();			
			fnpreventDefault(event);	
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>
	<!-- 내용 S -->	
	<div id="contents">
		<div class="admin_user">
			<div class="cont_title">
				<h2>Users</h2>
				<a href="#" onclick="fnWrite(0);">Create User</a>
				<a href="#" onclick="fnOpenLua();" style="background:#ff772e;">Edit Lua</a>
			</div>
			<div class="admin_user_con">
				<div class="admin_user_title">
					<table id="tbusers" class="admin_user_list" summary="표시">
						<caption class="hide">정보 표</caption>
						<colgroup>
							<col width="65%" />
							<col width="35%" />
						</colgroup>
						<thead>
							<tr>
								<th>ID</th>
								<th>이름</th>
							</tr>
						</thead>
						<tbody>							
						</tbody>
					</table>
				</div>
				<!--board paging start-->
				<div id="PAGE_NAVI" class="board_paging">								
				</div>
				<!--board paging end-->
				<input type="hidden" id="PAGE_INDEX" name="PAGE_INDEX"/>
			</div>
		</div>
	</div>
	<!-- 내용 E -->
	<%@ include file="/WEB-INF/include/footer.jspf" %>
</body>
</html>