<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		window.onload = function() {
			if ('${USING}' == 'Y') {
				$('#txtUserId').val('${USER_ID}');				
				$('#txtUserPwd').val('${USER_PWD}');
				$('#txtUserName').val('${USER_NAME}');
				$('#selUserType').val('${USER_TYPE}');
				$("#selUserType").trigger("chosen:updated");
				$('#txtRemarks').val('${REMARKS}');
				alert('이미 존재하는 아아디 입니다.');
			}
			else if ('${SEQ_NO}' != '0') {
				$('#txtUserId').attr('readonly', 'readonly');
				$('#selUserType').val('${userinfo.USER_TYPE}');
				$("#selUserType").trigger("chosen:updated");
				$('#txtRemarks').val('${userinfo.REMARKS}');				
			}
		}
		
		function fnDomainList(userno, userid, type) {
			fnWinPop("<c:url value='/view/userdomain.do' />" + "?userno=" + userno + "&userid=" + userid + "&type=" + type, "DomainPop", 565, 420, 0, 0);
			fnpreventDefault(event);
		}
		
		function fnSave() {
			$txtUserId = $('#txtUserId');
			$txtUserPwd = $('#txtUserPwd');
			$txtUserName = $('#txtUserName');
			
			if ($txtUserId.val().trim() == '') {
				alert('아이디를 입력하세요.');
				$txtUserId.focus();
				return;
			}
			
			if ($txtUserPwd.val().trim() == '') {
				alert('비밀번호를 입력하세요.');
				$txtUserPwd.focus();
				return;
			}
			
			if ($txtUserName.val().trim() == '') {
				alert('이름을 입력하세요.');
				$txtUserName.focus();
				return;
			}
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/userinsert.do' />");
			comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
			comSubmit.addParam("USER_ID", $txtUserId.val());
			comSubmit.addParam("USER_PWD", $txtUserPwd.val());
			comSubmit.addParam("USER_NAME", $txtUserName.val());
			comSubmit.addParam("USER_TYPE", $('#selUserType').val());
			comSubmit.addParam("REMARKS", $('#txtRemarks').val());
			comSubmit.submit();
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>
	<div id="contents">
		<div class="admin_user">
			<div class="cont_title">
				<h2>Users</h2>
				<a href="#" onclick="fnSave();">save</a>
			</div>
			<div class="admin_user_con">
				<div class="admin_user_title">
					<table class="admin_user_list admin_user_registration" summary="표시">
						<caption class="hide">정보 표</caption>
						<colgroup>
							<col width="40%" />
							<col width="60%" />
						</colgroup>
						<tr>
							<th class="admin_user_list_bg02">아이디</th>
							<td class="admin_user_list_bg01"><input type="text" id="txtUserId" name="USER_ID" value="${userinfo.USER_ID}" /></td>
						</tr>
						<tr>
							<th class="admin_user_list_bg02">비밀번호</th>
							<td class="admin_user_list_bg01"><input type="password" id="txtUserPwd" name="USER_PWD" value="${userinfo.USER_PWD}" /></td>
						</tr>
						<tr>
							<th class="admin_user_list_bg02">이름</th>
							<td class="admin_user_list_bg01"><input type="text" id="txtUserName" name="USER_NAME" value="${userinfo.USER_NAME}" /></td>
						</tr>
						<tr>
							<th class="admin_user_list_bg02">권한</th>
							<td class="admin_user_list_bg01">
								<div style="margin: 0px 10px;">
									<select class="f_right u_group" id="selUserType" name="USER_TYPE">
										<option value="GU">GU</option>
										<option value="USER">USER</option>
										<option value="SA">SA</option>
									</select>
								</div>
							</td>
						</tr>
						<c:if test="${SEQ_NO ne 0 && userinfo.USER_TYPE ne 'SA'}">
						<tr>
							<th class="admin_user_list_bg02">도메인리스트</th>
							<td class="admin_user_list_bg01">
								<a href="#" class="registration_view" onclick="fnDomainList(${SEQ_NO}, '${userinfo.USER_ID}', 'v');">보기</a>
								<a href="#" class="registration_view" style="background:#ff772e;" onclick="fnDomainList(${SEQ_NO}, '${userinfo.USER_ID}', 'c');">복사</a>
							</td>
						</tr>
						</c:if>
						<tr>
							<th class="admin_user_list_bg02">메모</th>
							<td class="admin_user_list_bg01" style="vertical-align: top;">
								<div class="registration_area">
									<textarea id="txtRemarks" name="REMARKS"></textarea>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/include/footer.jspf" %>
</body>
</html>