<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		window.onload = function() {
			$('#txtParamCount').keyup(function () {
	            $(this).val($(this).val().replace(/[^0-9]/gi, ""));
	        });
		}
		
		function fnSave() {
			$txtFuntionName = $('#txtFuntionName');
			$txtDescription = $('#txtDescription');
			$txtParamCount = $('#txtParamCount');
			
			if ($txtFuntionName.val().trim() == '') {
				alert('함수명을 입력하세요.');
				$txtFuntionName.focus();
				return;
			}
			
			if ($txtDescription.val().trim() == '') {
				alert('인자 값을 입력하세요.');
				$txtDescription.focus();
				return;
			}
			
			if ($txtParamCount.val().trim() == '') {
				alert('인자 수를 입력하세요.');
				$txtParamCount.focus();
				return;
			}					
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/luainsert.do' />");
			comSubmit.addParam("SEQ_NO", '${SEQ_NO}');
			comSubmit.addParam("FUNCTION_NAME", $txtFuntionName.val());
			comSubmit.addParam("DESCRIPTION", $txtDescription.val());
			comSubmit.addParam("PARAM_COUNT", $txtParamCount.val());
			comSubmit.submit();
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>
	<div id="contents">
		<div class="admin_user">
			<div class="cont_title">
				<h2>Users</h2>
				<a href="#" onclick="fnSave();">save</a>
			</div>
			<div class="admin_user_con">
				<div class="admin_user_title">
					<table class="admin_user_list admin_user_registration" summary="표시">
						<caption class="hide">정보 표</caption>
						<colgroup>
							<col width="40%" />
							<col width="60%" />
						</colgroup>
						<tr>
							<th class="admin_user_list_bg02">함수명</th>
							<td class="admin_user_list_bg01"><input type="text" id="txtFuntionName" name="FUNCTION_NAME" value="${luainfo.FUNCTION_NAME}" /></td>
						</tr>
						<tr>
							<th class="admin_user_list_bg02">인자값</th>
							<td class="admin_user_list_bg01"><input type="text" id="txtDescription" name="DESCRIPTION" value='${luainfo.DESCRIPTION}' /></td>
						</tr>
						<tr>
							<th class="admin_user_list_bg02">인자수</th>
							<td class="admin_user_list_bg01"><input type="text" id="txtParamCount" name="PARAM_COUNT" value="${luainfo.PARAM_COUNT}" /></td>
						</tr>						
					</table>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/include/footer.jspf" %>
</body>
</html>