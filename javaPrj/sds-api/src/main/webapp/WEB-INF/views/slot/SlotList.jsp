<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/include/header.jsp"%>
</head>
<body>
	<div class="layer" id="layer">
		<div class="bg"></div>
		<!-- 	<div id="layer2" class="pop-layer">
			<div class="pop-container">
				<div class="pop-conts">
					content //
					<p class="ctxt mb20">Thank you.<br>
						Your registration was submitted successfully.<br>
						Selected invitees will be notified by e-mail on JANUARY 24th.<br><br>
						Hope to see you soon!
					</p>
	
					<div class="btn-r">
						<a href="#" class="cbtn">Close</a>
					</div>
					// content
				</div>
			</div>
		</div> -->

		<!-- Create Class 모달창 -->
		<div id="divCreatClass" class="layer_box create_class_box"
			style="z-index: 1; display: block; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -91px; display: none;">
			<table>
				<col width="30%" />
				<col width="*" />
				<tbody>
					<tr>
						<td><span>Class name</span></td>
						<td><label for="create_class_it"> <input
								id="CLASS_NAME" type="text" id="create_class_it"
								name="create_class_it" />
						</label></td>
					</tr>
					<tr>
						<td>Description</td>
						<td><label for="create_class_description"> <input
								id="DESCRIPTION" type="text" value="" id="class_description"
								name="class_description" />
						</label></td>
					</tr>
				</tbody>
			</table>
			<div class="layer_box_btn">
				<a onclick="fnSaveClass();" href="#">Save</a> <a
					onclick="fnCancelClass();" href="#">Cancel</a>
			</div>

		</div>
		<!-- Create Class 모달창 -->

		<!-- Create slot 모달창 -->
		<div id="divCreatSlot" class="layer_box create_slot_box"
			style="width: 370px; z-index: 1; display: block; position: absolute; top: 50% !important; left: 50% !important; right: 0 !important; margin-left: -171px; margin-top: -270px; display: none; overflow-y: auto; max-height: 500px;">
			<table>
				<colgroup>
					<col width="30%" />
					<col width="*" />
				</colgroup>
				<tbody id="slotTbody">
					<tr>
						<td colspan="2"><span>Slot information</span></td>
					</tr>
					<tr>
						<td>Slot name</td>
						<td><input id="SLOT_NAME" type="text" /></td>
					</tr>
					<tr>
						<td>Slot type</td>
						<td>
							<div class="text_box">
								<div class="s_word">
									<input type="text" id="TYPE_NAME" readonly="readonly" /> <a
										id="aTypeSearch" href="#" title="검색" class="solt_search"><span
										class="hide">search</span></a> 
										<input type="hidden" id="hidslottype" />
										<input type="hidden" id="hidtypeseq" />
										<input type="hidden" id="hidtypesubseq" />
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>Slot description</td>
						<td><input id="SLOT_DESCRIPTION" type="text" /></td>
					</tr>
					<tr id="tr1">
						<td colspan="2"><span>Others</span></td>
					</tr>
					<tr id="tr2">
						<td>Its class</td>
						<td>
							<div class="s_option">
								<label for="user_group"> <select class="f_right u_group"
									id="search_option" name="search_option"
									data-placeholder="검색조건을 선택하세요">
										<option value="">select class</option>
										<c:forEach items="${CLASS_LIST }" var="row">
											<option value="${row.SEQ_NO}">${row.CLASS_NAME }</option>
										</c:forEach>
								</select>
								</label>
							</div>
						</td>
					</tr>
					<tr id="tr3">
						<td>Preceding slots</td>
						<td>
							<div class="text_box">
								<div class="s_word">
									<input type="text" id="PRCE_SLOT_NAME" />
									<a class="close_btn f_right" href="#"></a>
									<a id="aPrceSearch" href="#" title="검색" class="solt_search"><span
										class="hide">search</span></a> 
										<input type="hidden" id="hidPreceSlotSeq" />
										<input type="hidden" id="hidPreceSlotSubSeq" />
								</div>
							</div>
						</td>
					</tr>
					<!-- <tr id="tr4">
						<td>Sense</td>
						<td>
							<div class="text_box">
								<div class="s_word">
									<input type="text" id="SENSE_NAME" readonly="readonly" /> <a
										id="aSenseSearch" href="#" title="검색" class="solt_search"><span
										class="hide">search</span></a> <input type="hidden"
										id="hidSenseSeq" />
								</div>
							</div>
						</td>
					</tr> -->
					<tr id="tr5">
						<td>Instance와 연동</td>
						<td style="text-align:left;">
							<input type="checkbox" id="chkInstance" name="chkInstance" style="width:7%;" onclick="fnCheck(this);"/>
							<label for="chkInstance" style="vertical-align: text-top; line-height:22px;"></label>							
						</td>						
					</tr>
					<tr id="tr6">
						<td colspan="2"><span>Slot value relationship</span>&nbsp;<a
							href="#" onclick="fnAddDefine('', 0, '', '', '');"
							style="color: red;">+add</a></td>
					</tr>
					<tr id="tr7">
						<td id="tdDefine" colspan="2"></td>
					</tr>
					<!-- <tr>
						<td>
							<div class="text_box" style="width:302px;">
								<div class="s_word">
									<input id="txtSortNo" type="text" value="sort no…" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" />
								</div>
							</div>
						</td>
					</tr> -->
				</tbody>
			</table>

			<div class="layer_box_btn">
				<a onclick="fnSaveSlot();" href="#">Save</a> <a
					onclick="fnCancelSlot();" href="#">Cancel</a>
			</div>

		</div>
		<!-- Create slot 모달창 -->
	</div>

	<%@ include file="/WEB-INF/include/left.jspf"%>
	<div class="slot">
		<div class="cont_title">
			<h2>Entities</h2>
			<a onclick="fnOpenCreateClass();" href="#">Create Class</a>
			<!-- <a onclick="fnOpenCreateSlot();" class="btn_color02" href="#">Create slot</a> -->
		</div>

		<div class="solt_contents">
			<!--search box start-->
			<div class="search_box">
				<div class="s_word">
					<label for="slot_search"> <input type="text" id="txtSearch"
						value="search slots…"
						onblur="if (this.value=='') this.value=this.defaultValue"
						onclick="if (this.defaultValue == this.value) this.value = ''"
						id="slot_search" name="slot_search" /><a onclick="fnSearch();"
						href="#" title="검색" class="solt_search"><span class="hide">search</span></a>
					</label>
				</div>
			</div>
			<!--search box end-->

			<c:set var="AgentName" value=""></c:set>
			<c:forEach items="${CLASS_LIST }" var="crow">
				<dl id="dl${crow.SEQ_NO}" class="class_name">
					<c:if test="${null eq param.AGENT_SEQ && AgentName ne crow.AGENT_NAME}">
						<h3
							style="font-size: 20px; font-weight: normal; line-height: 24px; margin: 0px 45px 10px 45px;">${crow.AGENT_NAME}</h3>
						<c:set var="AgentName" value="${crow.AGENT_NAME}"></c:set>
					</c:if>
					<dt>
						<div class="solt_title">
							<a id="aclass${crow.SEQ_NO}" class="slot_down" href="#"
								onclick="fnClassSlide(this);">${crow.CLASS_NAME}</a>
							<!--화살표 반대 class: slot_up-->
							<c:if test="${null ne crow.DESCRIPTION}">
								<span class="explain"><b></b> <span>${crow.DESCRIPTION}</span>
								</span>
							</c:if>
						</div>
						<a onclick="fnClassDelete(${crow.SEQ_NO});" class="class_name_del" href="#">delete</a>
						<a onclick="fnClassEdit(${crow.SEQ_NO}, '${crow.CLASS_NAME}', '${crow.DESCRIPTION}');" class="class_name_del" href="#">edit</a>
						<a onclick="fnClassInstans(${crow.SEQ_NO}, '${crow.CLASS_NAME}', this);" class="class_name_instances" href="#">instances</a>
						<a onclick="fnOpenCreateSlot(${crow.SEQ_NO});" class="class_name_createslot" href="#">Create slot</a>
					</dt>
				</dl>
				<br /><br />
			</c:forEach>
		</div>
		<input type="hidden" id="hidClassSeq" /> <input type="hidden"
			id="hidSlotSeq" />
	</div>
	<%@ include file="/WEB-INF/include/footer.jspf"%>
	<script type="text/javascript">
		var slotdata = jQuery.parseJSON('${SLOT_LIST}');
		var savechk = true;
		var addslotSeq = 0;
		window.onload = function() {			
			$('[id^=dl]').each(function() {
				fnCreateSlotList($(this).attr('id').replace('dl', ''));
			});		
			
			//checkbox
			$('#chkInstance').picker({
				customClass: 'list_check'
			});
			//checkbox_end
		}
		
		$('#CLASS_NAME').keyup(function() {
			var tempval = $(this).val();
			tempval = tempval[0].toLocaleUpperCase() + tempval.substr(1);
			$(this).val(tempval);
		});
		
		$('#txtSortNo').keyup(function () {
            $(this).val($(this).val().replace(/[^0-9]/gi, ""));
        });
		
		function fnCreateSlotList(classSeq) {
			if (slotdata[classSeq] != undefined) {
				var str = '';		
				$.each(slotdata[classSeq], function(key, value){
					var imgurl = 'background: url(../images/user_says_bg01.png) no-repeat -6px center;';
					var himgurl = 'icon_s_img.png';
					str += '<dd id="dd' + classSeq + '_' + value.SEQ_NO + '">';
					str += '<table style="width:100%; table-layout:fixed; padding:10px; color:#585858; width:90%;">';
					if (value.SLOT_TYPE != 'C') {
						str += '<tr onclick="fnSlotDetail(' + value.SEQ_NO + ', \'\', \'' + value.SLOT_NAME + '\', this);" style="cursor: pointer;">';	
					}
					else {
						str += '<tr onclick="fnCreateSubSlotList(' + value.TYPE_SEQ + ', ' + value.SEQ_NO + ', ' + value.CLASS_SEQ_NO + ', this);" style="cursor: pointer;">';
						himgurl = 'icon_s_img01.png';
					}												
					str += '<td style="width:28px; background: url(../images/' + himgurl + ') no-repeat 0px center; height:21px;">&nbsp;</td>';
					str += '<td id="tdslot' + classSeq + '_' + value.SEQ_NO + '" style="width:45%; word-wrap:break-word; vertical-align: middle; font-weight: bold;"><span style="line-height: 18px;">' + value.SLOT_NAME + '</span></td>';
					if (value.SLOT_TYPE == 'C')
						imgurl = 'background: url(../images/icon_c_img02.png) no-repeat 0px center;';
					else if (value.SLOT_TYPE == 'O')
						imgurl = 'background: url(../images/icon_s_img02.png) no-repeat 0px center;';
					str += '<td style="width:28px; ' + imgurl + '">&nbsp;</td>';																															
					str += '<td style="word-wrap:break-word; vertical-align: middle; color: #989898;">';
					str += value.TYPE_NAME
					str += '</td></tr></table>';			
					str += '<input type="hidden" id="hidslotName' + value.SEQ_NO + '" value="' + value.SLOT_NAME + '" />';
					str += '<input type="hidden" id="hidOrgType' + value.SEQ_NO + '" value="' + value.SLOT_TYPE + '" />';
					str += '<input type="hidden" id="hidOrgTypeSeq' + value.SEQ_NO + '" value="' + value.TYPE_SEQ + '" />';
					str += '<a onclick="fnSlotEdit(' + value.SEQ_NO + ', ' + value.CLASS_SEQ_NO + ', \'' + value.SLOT_NAME + '\', \'' + value.SLOT_DESCRIPTION + '\', \'' + value.SLOT_TYPE + '\', ' + value.TYPE_SEQ + ', \'' + value.TYPE_NAME + '\', ' + value.PRCE_SLOT_SEQ + ', \'' + value.PRCE_SLOT_NAME + '\', ' + value.SENSE_SEQ + ', \'' + value.SENSE_NAME + '\', \'' + value.INSTANCE_FLAG + '\', \'' + value.TYPE_SUB_SEQ + '\', \'' + value.PRCE_SLOT_SUB_SEQ + '\');" style="display:none;" href="#" class="modify_btn"><span class="hide">수정</span></a>';
					str += '<a onclick="fnSlotDelete(' + value.SEQ_NO + ', ' + value.CLASS_SEQ_NO + ', \'' + value.SLOT_TYPE + '\', ' + value.TYPE_SEQ + ');" href="#" style="display:none;" class="delete_btn"><span class="hide">삭제</span></a>';
					str += '<a onclick="fnSlotUpandDown(' + value.SEQ_NO + ', \'U\', this);" href="#" style="display:none;" class="up_btn"><span class="hide">up</span></a>';
					str += '<a onclick="fnSlotUpandDown(' + value.SEQ_NO + ', \'D\', this);" href="#" style="display:none;" class="down_btn"><span class="hide">down</span></a>';
					str += '</dd>';
				});
				
				$('#dl' + classSeq).append(str);
			}
		}
		
		function fnCreateSubSlotList(classSeq, slotSeq, parentclassSeq, obj) {			
			var $dd = $(obj).closest('dd');
			var $divSubSlot = $dd.next();
			var Title = $dd.find('tr td[id^=tdslot] span').text();
			var ArrCSeq = new Array();
			
			if($divSubSlot[0] == undefined || $divSubSlot[0].tagName != 'DIV') {
				var str = '<div name="divSubSlot" style="display:block;">';
				$.each(slotdata[classSeq], function(key, value){
					var imgurl = 'background: url(../images/user_says_bg01.png) no-repeat -6px center;';
					var himgurl = 'icon_s_img.png';					
					str += '<dd id="dd' + parentclassSeq + '_' + classSeq + '_' + value.SEQ_NO + '">';
					str += '<table style="width:100%; table-layout:fixed; padding:10px; color:#585858; width:90%;">';
					if (value.SLOT_TYPE != 'C') {
						str += '<tr onclick="fnSlotDetail(' + value.SEQ_NO + ', \'' + Title  + '\', \'' + value.SLOT_NAME + '\', this);" style="cursor: pointer;">';
					}
					else {
						str += '<tr onclick="fnCreateSubSlotList(' + value.TYPE_SEQ + ', ' + value.SEQ_NO + ', ' + value.CLASS_SEQ_NO + ', this);" style="cursor: pointer;">';
						himgurl = 'icon_s_img01.png';
					}			
					
					/* str += '<td style="width:28px; background: url(../images/icon_s_img.png) no-repeat 0px center; height:21px;">&nbsp;</td>'; */
					str += '<td id="tdslot' + parentclassSeq + '_' + classSeq + '_' + value.SEQ_NO + '" colspan="2" style="width:45%; word-wrap:break-word; vertical-align: middle; font-weight: bold; height:21px; padding-left:' + fnAddSpace($(obj).parents().find('[name=divSubSlot]').length) + '">';				
					str += '<span style="line-height: 18px; padding: 3px 0px 3px 28px; display:block; height:100%; background: url(../images/' + himgurl + ') no-repeat 0px center; ">' +  Title  + '.' + value.SLOT_NAME + '</span></td>';
					if (value.SLOT_TYPE == 'C')
						imgurl = 'background: url(../images/icon_c_img02.png) no-repeat 0px center;';
					else if (value.SLOT_TYPE == 'O')
						imgurl = 'background: url(../images/icon_s_img02.png) no-repeat 0px center;';
					str += '<td style="width:28px; ' + imgurl + '">&nbsp;</td>';																															
					str += '<td style="word-wrap:break-word; vertical-align: middle; color: #989898;">';
					str += value.TYPE_NAME
					str += '</td></tr></table>';			
					str += '</dd>';
					ArrCSeq.push(parentclassSeq + '_' + classSeq + '_' + value.SEQ_NO);
				});	
				str += '</div>';
				$dd.after(str);
				
				for (var i = 0; i < ArrCSeq.length; i++) {
					var $tdslot = $('#tdslot' + ArrCSeq[i]);					
					var width = Number($tdslot.css('width').replace('px', '')) + 28;
					$tdslot.css('width', width + 'px');
				}
				
			}
			else {
				if($divSubSlot.css('display') == 'none')
					$divSubSlot.css('display', 'block');
				else 
					$divSubSlot.css('display', 'none');
			}													
		}
		
		function fnAddSpace(count) {
			var retrunValue = 20 * (count + 1);
			
			/* for (var int = 0; int < count + 1; int++) {
				retrunValue += '';
			} */
			return retrunValue+= 'px';
		}
		
		function fnClassSlide(obj) {
			if ($(obj).hasClass('slot_down') == true) {
				$(obj).parent().parent().parent().find('dd').hide();
				$(obj).attr('class', 'slot_up');
			}
			else {
				$(obj).parent().parent().parent().find('dd').show();
				$(obj).attr('class', 'slot_down');
			}
			fnpreventDefault(event);
		}
		
		function fnSlotDetail(seqno, classname, slotname, obj) {
			if(classname != '') classname = '.' + classname;
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/slotwrite.do' />");
			comSubmit.addParam("SLOT_SEQ_NO", seqno);			
			comSubmit.addParam("CLASS_NAME", $('#aclass' + $(obj).closest('dl').attr('id').replace('dl', '')).text() + classname);
			comSubmit.addParam("SLOT_NAME", slotname);
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.submit();
			fnpreventDefault(event);
		}
		
		function fnOpenCreateClass() {
			$('#CLASS_NAME').val('');
			$('#DESCRIPTION').val('');
			$("#divCreatClass").show();
			fnLayerPop('divCreatClass');		
			//$("#divCreatClass").modal();
			fnpreventDefault(event);
		}
		
		function fnOpenCreateSlot(classSeq) {					
			$("#search_option").val(classSeq);
			$("#search_option").trigger("chosen:updated");
			$("#hidslottype").val('S');
			$("#hidtypeseq").val('4');
			$('#hidtypesubseq').val('');
			$("#hidslotname").val('Sys.string');
			$("#TYPE_NAME").val('Sys.string');			
			//$("#txtSortNo").val('');
			$('#SLOT_DESCRIPTION').val('');
			$('#PRCE_SLOT_NAME').val('');
			$('#hidPreceSlotSeq').val('');
			$('#hidPreceSlotSubSeq').val('');
			//$('#SENSE_NAME').val('');
			//$('#hidSenseSeq').val('');
			$('#chkInstance').picker("check");
			
			$("#divCreatSlot").show();
			$('[id^=tr]').hide();
			//fnAddDefine('', 0, '' ,'');
			fnTypeEventBinding(classSeq);
			fnLayerPop('divCreatSlot');
			//$("#modalback").show();
			//$("#divCreatSlot").modal();			
			fnpreventDefault(event);
			$("#SLOT_NAME").val('').focus();
		}
		
		function fnTypeEventBinding(classSeq) {
			$("#aTypeSearch").click(function() {
				fnOpenPop(classSeq, 'T');
			});
			$("#aPrceSearch").click(function() {
				fnOpenPop(classSeq, 'P');
			});
			/* $("#aSenseSearch").click(function() {
				fnOpenPop(classSeq, 'S');
			}); */
			$("#tdDefine").on({
				click: function(e) {
					var hidSeq = $(this).attr('id').replace('aTargetSeq', '');
					fnOpenPop(classSeq, 'D', hidSeq);
				}
			}, '[id^=aTargetSeq]');
		}
		/*
		function fnAddSlot(classSeq) {
			var str = '';
			
			str += '<dd id="dd' + classSeq + '_' + addslotSeq + '">';
			str += '<table style="width:100%; table-layout:fixed; padding:10px; color:#585858; width:90%;">';			
			str += '<tr style="cursor: pointer;">';												
			str += '<td style="width:28px; background: url(../images/icon_s_img.png) no-repeat 0px center; height:21px;">&nbsp;</td>';
			str += '<td id="tdslot' + classSeq + '_' + addslotSeq + '" style="width:45%; word-wrap:break-word; vertical-align: middle; font-weight: bold;">';
			str += '<div id="div' + classSeq + '_' + addslotSeq + '" onclick="fnSlotNaming(this, ' + classSeq + ');" contenteditable="true" style="line-height: 18px; border: 1px solid #BDBDBD; width:90%; min-height:20px;"></div></td>';
			str += '<td style="width:28px; background: url(../images/user_says_bg01.png) no-repeat -6px center;">&nbsp;</td>';																															
			str += '<td style="word-wrap:break-word; vertical-align: middle; color: #989898; cursor: pointer;"><span onclick="fnOpenPop(' + classSeq + ', 1, this);">Sys.String</span>';			
			str += '</td></tr></table>';			
			str += '<input type="hidden" id="hidslotName' + classSeq + '_' + addslotSeq + '" />';
			str += '<input type="hidden" id="hidOrgType' + classSeq + '_' + addslotSeq + '" value="S" />';
			str += '<input type="hidden" id="hidOrgTypeSeq' + classSeq + '_' + addslotSeq + '" value="4" />';
			str += '<a onclick="" style="display:none;" href="#" class="modify_btn"><span class="hide">수정</span></a>';
			str += '<a onclick="" href="#" style="display:none;" class="delete_btn"><span class="hide">삭제</span></a>';
			str += '<a onclick="" href="#" style="display:none;" class="up_btn"><span class="hide">up</span></a>';
			str += '<a onclick="" href="#" style="display:none;" class="down_btn"><span class="hide">down</span></a>';
			str += '</dd>';
			
			$('#dl' + classSeq).append(str);
			addslotSeq ++;
			fnpreventDefault(event);
		}
				
		function fnSlotNaming(obj, classSeq) {
			$obj = $(obj);
			//$obj.attr("contenteditable", "true");
			
			$obj.focusout(function() {
				if($obj.text().trim() != '') {
					$obj.attr("contenteditable", "false");
					$obj.css('border', 'none');
					
					fnSaveSlotAsync(slotname, classSeq, slottype, typeseq, typename);
				}
			});
			fnCursorEnd(obj);
		}
		*/
		
		function fnSaveClass() {
			var className = $('#CLASS_NAME').val().trim();
			if (className == '') {
				alert('클래스명을 입력하세요.');
				fnpreventDefault(event);
				$('#CLASS_NAME').focus();
				return false;
			}
			
			var reg = /[^a-zA-Z0-9_-]/;
			if (reg.test(className)) {
				alert('클래스명에 한글,공백,특수문자를 입력 할 수 없습니다.');
				fnpreventDefault(event);
				$('#CLASS_NAME').focus();
				return false;
			}
			
			var vChk = false;
			if ($('#hidClassSeq').val() == '') {				
				$('[id^=aclass]').each(function(e) {
					if ($(this).text() == className) {
						vChk = true;	
						return;
					}
				});	
			}
			
			if (vChk) {
				alert('이미 존재하는 클래스명 입니다.');
				$('#CLASS_NAME').focus();
				return false;
			}
			
			fnLoading();
			if(savechk) {
				savechk = false;
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/insertslotclass.do' />");
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.addParam("CLASS_NAME", className);
				comSubmit.addParam("DESCRIPTION", $('#DESCRIPTION').val());
				comSubmit.addParam("SEQ_NO", $('#hidClassSeq').val());			
				comSubmit.submit();
				fnpreventDefault(event);				
			}
			else {
				alert('저장중 입니다.');
				fnpreventDefault(event);
				return false;
			}
		}
		
		function fnSaveSlot() {			
			var bSlotSave = false;
			if(savechk) {				
				if ($('#search_option').val() == '') {
					alert('클래스를 선택하세요.');					
					fnpreventDefault(event);
					return false;
				}
				
				if ($('#SLOT_NAME').val().trim() == '') {
					alert('슬롯 이름을 입력하세요.');
					$('#SLOT_NAME').focus();
					fnpreventDefault(event);
					return false;
				}
								
				var reg = /[^a-zA-Z0-9_]/;
				if (reg.test($('#SLOT_NAME').val())) {
					alert('슬롯명에 한글,공백,특수문자를 입력 할 수 없습니다.');
					$('#SLOT_NAME').focus();
					fnpreventDefault(event);
					return false;
				}
				
				if (slotdata[$('#search_option').val()] != undefined && $('#hidSlotSeq').val() == '') {
					$.each(slotdata[$('#search_option').val()], function(key, value){
						if (value.SLOT_NAME == $('#SLOT_NAME').val().trim()) {
							alert('이미 존재하는 슬롯 입니다.');
							$('#SLOT_NAME').focus();
							bSlotSave = true;
							return false;
						}
					});
				}
				
				if (bSlotSave) {
					fnpreventDefault(event);
					return false;
				}
				
				savechk = false;
				
				fnLoading();
				
				var arr = new Object();
				var arrobj = new Array();  
				
				$('#tdDefine table').each(function(idx) {
					var item = new Object();
					$table = $(this);
					item.TARGET_NAME = $table.find('[name=TARGET_NAME]').val();
					if (item.TARGET_NAME.trim() != '') {						
						item.TARGET_SEQ = $table.find('[name=hidTargetSeq]').val();
						if (item.TARGET_SEQ.trim() == '') {
							item.TARGET_SEQ = 0;
						}
						item.CON = $table.find('[name=CONDITION]').val();
						item.ACT = $table.find('[name=ACTION]').val();
						item.TARGET_SUB_SEQ = $table.find('[name=hidTargetSubSeq]').val(); 
						arrobj.push(item);
					}
				});
				
				arr.DEFINE_ITEM = arrobj;
				var jsoobj = JSON.stringify(arr);				
				
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/insertslot.do' />");
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				if ('${AGENT_SEQ}' != '')
					comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				else
					comSubmit.addParam("AGENT_SEQ", 0);
				comSubmit.addParam("SLOT_NAME", $('#SLOT_NAME').val());
				comSubmit.addParam("SLOT_DESCRIPTION", $('#SLOT_DESCRIPTION').val());
				comSubmit.addParam("CLASS_SEQ_NO", $('#search_option').val());
				comSubmit.addParam("SLOT_TYPE", $('#hidslottype').val());
				comSubmit.addParam("TYPE_SEQ", $('#hidtypeseq').val());
				comSubmit.addParam("TYPE_NAME", $('#TYPE_NAME').val());
				comSubmit.addParam("TYPE_SUB_SEQ", $('#hidtypesubseq').val());
				comSubmit.addParam("PRCE_SLOT_SEQ", ($('#hidPreceSlotSeq').val().trim() == '') ? 0 : $('#hidPreceSlotSeq').val());
				comSubmit.addParam("PRCE_SLOT_NAME", $('#PRCE_SLOT_NAME').val());
				comSubmit.addParam("PRCE_SLOT_SUB_SEQ", $('#hidPreceSlotSubSeq').val());
				//comSubmit.addParam("SENSE_SEQ", ($('#hidSenseSeq').val().trim() == '') ? 0 : $('#hidSenseSeq').val());
				//comSubmit.addParam("SENSE_NAME", $('#SENSE_NAME').val());
				comSubmit.addParam("SENSE_SEQ", 0);
				comSubmit.addParam("SENSE_NAME", "");
				
				if ($('#chkInstance').attr('checked') == 'checked')					
					comSubmit.addParam("INSTANCE_FLAG", 'Y');
				else					
					comSubmit.addParam("INSTANCE_FLAG", 'N');
				comSubmit.addParam("DEFINE_ITEM", jsoobj);
				comSubmit.addParam("SEQ_NO", $('#hidSlotSeq').val());
				comSubmit.addParam("SORT_NO", $('#dl' + $('#search_option').val()).children().length);
				//comSubmit.addParam("SORT_NO", ($('#txtSortNo').val().trim() == 'sort no…') ? 0 : $('#txtSortNo').val());
				comSubmit.addParam("OLD_SLOT_TYPE", $('#hidOrgType' + $('#hidSlotSeq').val()).val());
				comSubmit.addParam("OLD_TYPE_SEQ", $('#hidOrgTypeSeq' + $('#hidSlotSeq').val()).val());			
				comSubmit.addParam("OLD_SLOT_NAME", $('#hidslotName' + $('#hidSlotSeq').val()).val());
				comSubmit.addParam("SLOT_LIST", '${SLOT_LIST}');
				comSubmit.submit();			
				fnpreventDefault(event);
			}
			else {
				alert('저장중 입니다.');
				fnpreventDefault(event);
				return false;
			}
		}
				
		function fnSlotDelete(seqno, cseqno, slottype, typeseq) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/deleteslot.do' />");
				comSubmit.addParam("SEQ_NO", seqno);
				comSubmit.addParam("CLASS_SEQ_NO", cseqno);
				comSubmit.addParam("SLOT_TYPE", slottype);
				comSubmit.addParam("TYPE_SEQ", typeseq);
				comSubmit.addParam("OLD_SLOT_NAME", $('#hidslotName' + seqno).val());
				comSubmit.addParam("SLOT_LIST", '${SLOT_LIST}');
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.submit();
				fnpreventDefault(event);
			}
		}
		
		function fnSlotEdit(seqno, classseq, slotname, slotdescript, slottype, typeseq, typename, prceseq, prceslotname, senseseq, sensename, instanceflag, typesubseq, prcesubseq) {
			//$(window).scrollTop(0);
			$('#hidSlotSeq').val(seqno);
			$("#search_option").val(classseq);
			$("#search_option").trigger("chosen:updated");
			$("#SLOT_NAME").val(slotname);
			$("#hidslottype").val(slottype);
			$("#hidtypeseq").val(typeseq);
			$('#hidtypesubseq').val(typesubseq);
			$("#TYPE_NAME").val(typename);
			//$("#txtSortNo").val(sortno);
			$('#SLOT_DESCRIPTION').val(slotdescript);			
			$('#hidPreceSlotSeq').val(prceseq);			
			$('#PRCE_SLOT_NAME').val(prceslotname);
			$('#hidPreceSlotSubSeq').val(prcesubseq);
			//$('#hidSenseSeq').val(senseseq);
			//$('#SENSE_NAME').val(sensename);
			if (instanceflag == 'Y')
				$('#chkInstance').picker('check');
			else
				$('#chkInstance').picker("uncheck");
			
			fnGetDefine(seqno);
			fnTypeEventBinding(classseq);
			$('[id^=tr]').show();
			$("#divCreatSlot").show();
			fnLayerPop('divCreatSlot');
			//$("#modalback").show();
			//$("#divCreatSlot").modal();
			fnpreventDefault(event);
		}
		
		function fnGetDefine(seqno) {
			$('#tdDefine').empty();
			var comAjax = new ComAjax();			
			comAjax.setUrl("<c:url value='/view/getslotdefine.do' />");	
			comAjax.setCallback("fnGetDefineComplete");
			comAjax.addParam("SLOT_SEQ_NO", seqno);
			comAjax.ajax();	
		}
		
		function fnGetDefineComplete(data) {
			if (data.list.length == 0) {
				fnAddDefine('', 0, '', '', '');
			}
			else {
				$.each(data.list, function(key, value){
					fnAddDefine(value.TARGET_NAME, value.TARGET_SEQ, value.CON, value.ACT, value.TARGET_SUB_SEQ);
				});
			}
		}
		
		function fnClassDelete(seqno) {
			if (confirm('해당 항목을 삭제 하시겠습니까?')) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/deleteslotclass.do' />");
				comSubmit.addParam("SEQ_NO", seqno);
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.submit();
				fnpreventDefault(event);
			}
		}
		
		function fnClassEdit(seqno, name, descript) {
			$(window).scrollTop(0);
			$('#hidClassSeq').val(seqno);
			$('#CLASS_NAME').val(name);
			$('#DESCRIPTION').val(descript);
			$('#divCreatClass').show();
			fnLayerPop('divCreatClass');
			//$("#modalback").show();
			//$("#divCreatClass").modal();	
			fnpreventDefault(event);
		}
		
		function fnSearch() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/slotlist.do' />");
			comSubmit.addParam("SLOT_NAME", ($('#txtSearch').val() == 'search slots…') ? '' : $('#txtSearch').val());
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
		function fnCancelClass() {			
			$("#divCreatClass").hide();
			$('#layer').fadeOut();
			$('#hidClassSeq').val('');
			//$("#modalback").hide();
			//$.modal.close();
			fnpreventDefault(event);
		}

		function fnCancelSlot() {			
			$("#divCreatSlot").hide();
			$('#layer').fadeOut();
			$('#hidSlotSeq').val('');
			//$("#modalback").hide();			
			//$.modal.close();
			fnpreventDefault(event);
		}
		
		function fnOpenPop(classSeq, pType, hidSeq) {			
			/* if($('#search_option').val() == '') {
				alert('클래스를 선택하세요.');
				return false;
			} */			
			
			fnWinPop("<c:url value='/view/opentypesearch.do' />" + "?pType=" + pType + "&Type=" + $("#hidslottype").val() + "&Seq=" + $("#hidtypeseq").val() + "&SubSeq=" + $('#hidtypesubseq').val() + "&ClassSeq=" + classSeq + "&hidSeq=" + hidSeq, "Typepop", 430, 550, 0, 0);
			fnpreventDefault(event);
		}
		
		function fnOpenLuaPop(obj) {
			fnWinPop("<c:url value='/view/openfunctionwrite.do' />" + "?obj=" + $(obj).attr('id'), "IntentsPop", 571, 388, 0, 0);	
		}
		
		function fnClassInstans(seqno, classname, obj) {
			if ($(obj).parent().parent().find('dd').length == 0) {
				alert('slot을 생성 후 instance를 작성하세요.');
				return false;
			}
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/slotinstansces.do' />");
			comSubmit.addParam("SEQ_NO", seqno);
			comSubmit.addParam("CLASS_NAME", classname);
			comSubmit.addParam("SLOT_LIST", '${SLOT_LIST}');
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.submit();			
			fnpreventDefault(event);	
		}			
		
		function fnAddDefine(targetname, targetseq, con, act, targetsubseq) {
			var str = '';
			var $tdDefine = $('#tdDefine');
			var defineCnt = $tdDefine.find('table').length;
			
			str += '<table id="tb' + defineCnt + '"><colgroup><col width="30%" /><col width="*" /></colgroup>';
			str += '<tbody><tr><td><span>define</span></td>';
			str += '<td style="text-align: right;">';
			str += '<a onclick="fnDefineDelete(' + defineCnt + ');" href="#" style="margin-left: 15px; font-size: 12px; color: #ffffff; padding: 2px 4px; background: #9d9d9d; border-radius: 3px;">Delete</a>';
			str += '</td></tr><tr><td>Target slot</td><td>';
			str += '<div class="text_box">';
			str += '<div class="s_word">';										
			str += '<input type="text" id="TARGET_NAME' + defineCnt + '" name="TARGET_NAME" value="' + targetname + '" />';
			str += '<a id="aTargetSeq' + defineCnt + '" href="#" title="검색" class="solt_search"><span class="hide">search</span></a>';
			str += '<input type="hidden" id="hidTargetSeq' + defineCnt + '" name="hidTargetSeq" value="' + targetseq + '" />';
			str += '<input type="hidden" id="hidTargetSubSeq' + defineCnt + '" name="hidTargetSubSeq" value="' + targetsubseq + '" />';
			str += '</div></div></td></tr><tr><td>Condition</td><td>';
			str += '<input id="CONDITION' + defineCnt + '" readonly="readonly" onclick="fnOpenLuaPop(this);" name="CONDITION" type="text" value=\'' + con + '\' />';	
			str += '</td></tr><tr><td>Action</td><td>';
			str += '<input id="Action' + defineCnt + '" readonly="readonly" onclick="fnOpenLuaPop(this);" name="ACTION" type="text" value=\'' + act + '\' />';
			str += '</td></tr></tbody></table>';
			
			$tdDefine.append(str);
		}
		
		function fnDefineDelete(seq) {
			$('#tb' + seq).remove();
		}
		
		function fnSlotUpandDown(seqno, type, obj) {
			$dl = $(obj).parent();
			var sindex = $dl.index();
			var eindex;
			var eseqno;
			var bSaveChk = false;
			
			if (type == 'D') {
				eindex = $dl.next().index();
				eseqno = $dl.next().attr('id').substr($dl.attr('id').indexOf('_') + 1);
				$dl.next().after($dl);
				bSaveChk = true;
			}
			else {
				if ($dl.prev().get(0).tagName != 'DT') {
					eindex = $dl.prev().index();
					eseqno = $dl.prev().attr('id').substr($dl.attr('id').indexOf('_') + 1);
					$dl.prev().before($dl);
					bSaveChk = true;
				}				
			}
			
			if (bSaveChk) {
				var comAjax = new ComAjax();			
				comAjax.setUrl("<c:url value='/view/updateslotsort.do' />");			
				comAjax.addParam("S_SEQ_NO", seqno);
				comAjax.addParam("E_SEQ_NO", eseqno);
				comAjax.addParam("SINDEX", sindex);
				comAjax.addParam("EINDEX", eindex);
				comAjax.ajax();
			}
		}
		
		function fnCheck(obj) {
			if ($(obj).attr('checked') == 'checked') {
				$(obj).picker("uncheck");
			}
			else {
				$(obj).picker('check');		
			}
		}
	</script>
</body>
</html>