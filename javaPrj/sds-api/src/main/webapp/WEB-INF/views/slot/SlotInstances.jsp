<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery.simplemodal.js'/>"></script>
	<script type="text/javascript">
		var arrThead;
		var arrChead;
		var strCol = '';
		
		window.onload = function() {
			var iwidth = '100%';						
			if (${fn:length(slotlist)} == 0) {
				
			}
			else {				 
				if(${fn:length(slotlist)} > 5)					
					iwidth = ${fn:length(slotlist)} * 120;										
			}						
			//$('#tbslot').css('width', iwidth);
			
			arrThead = new Array();
			$('#tbslot thead tr [name=thTitle]').each(function() {
				arrThead.push($(this).text());
			});
			
			$('#tbslot').on({				
				'click focusin': function(e) {						
					if($(this).parent().parent().next().length == 0) {
						AddRow($("#tbslot > tbody"));
					}
				},
				focusout: function(e) {
					var $this = $(this);	
					var id = $this.attr('id');					
					
					if(!gfn_isNull(id)) {
						var seq = id.replace('txtObj', '');
						if($('#hidSaveChk' + seq).val() == 'Y') {
							var strupdate = '';
							
							$(this).parent().parent().find('input:text').each(function(idx) {														
								strupdate += arrChead[idx] + ' = "' + $(this).val() + '", ';								
							});							
							
							var comAjax = new ComAjax();		
							comAjax.setCallback("fnAjaxCallBack");
							comAjax.addParam("SEQ_NO", ${SEQ_NO});
							comAjax.addParam("ITEM_SEQ_NO", seq);
							
							if(strupdate != '') {
								strupdate = strupdate.substr(0, strupdate.length - 2);
								comAjax.setUrl("<c:url value='/view/updateslotinstansces.do' />");
								comAjax.addParam("SET_ITEM", strupdate);
							}
							else {
								comAjax.setUrl("<c:url value='/view/deleteslotinstansces.do' />");
								$(this).parent().parent().remove();
							}
							comAjax.ajax();
							$('#hidSaveChk' + seq).val('N');
						}
					}
				},
				keydown: function(e) {					
					if(e.keyCode == 38) {												
						/* $(this).parent().next().children().eq(0).focus();
						fnCursorEnd($(this).parent().next().children().eq(0).get(0));
						fnpreventDefault(e); */
					} else if(e.keyCode == 40) {
						
					} else if(e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 39) {
					
					} else {
						var $this = $(this);
					
						if(!gfn_isNull($this.attr('id'))) 
							$('#hidSaveChk' + $this.attr('id').replace('txtObj', '')).val('Y');
					}
				}			
			}, '[name=OBJECT]');
						
			$('#tbslot').on('click', '[name="imgdel"]', function(e) {
				var $tr = $(this).parent().parent();
				if($tr.find('[id^=hidSeqno]').length > 0) {					
					var comAjax = new ComAjax();
					comAjax.setUrl("<c:url value='/view/deleteslotinstansces.do' />");
					comAjax.setCallback("fnAjaxCallBack");
					comAjax.addParam("SEQ_NO", ${SEQ_NO});
					comAjax.addParam("ITEM_SEQ_NO", $tr.find('[id^=hidSeqno]').val());
					comAjax.ajax();
				}
				$tr.remove();
				fnpreventDefault(e);
			});
			
			strCol = strCol.substring(0, strCol.length - 2);
			arrChead = strCol.split(', ');
			fnSelectList(1);
		}
		
		function fnAjaxCallBack(data) {
			if (data.status != "OK") {
				alert('작업 중 오류가 발생 하였습니다.');
			}
		}
		
		function fnSelectList(pageNo){
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/selectslotinstansces.do' />");
			comAjax.setCallback("fnSelectListCallBack");
			comAjax.addParam("PAGE_INDEX",$("#PAGE_INDEX").val());
			comAjax.addParam("PAGE_ROW", 10);
			comAjax.addParam("SEQ_NO", ${SEQ_NO});
			comAjax.addParam("COL", strCol);
			$txtSearch = $("#txtSearch");
			var strsearch = '';
			if($txtSearch.val().trim() != '' && $txtSearch.val() != 'search…') {
				strsearch = 'WHERE ';
				for (var i in arrThead) {
					strsearch += arrChead[i] + ' = "'+ $txtSearch.val() + '" OR ';
				}
				strsearch = strsearch.substr(0, strsearch.length - 4);
			}
				
			comAjax.addParam("WHERE_SQL", strsearch);
			comAjax.ajax();
			fnpreventDefault(event);
		}
						
		function fnSelectListCallBack(data) {
			var total = data.TOTAL;
			var rowcount = data.ROWCNT;
			var body = $("#tbslot > tbody");
			body.empty();
			if(total == 0){
				AddRow(body);
			}
			else{
				var params = {
					divId : "PAGE_NAVI",
					pageIndex : "PAGE_INDEX",
					totalCount : total,
					recordCount : rowcount,
					eventName : "fnSelectList"
					
				};
				gfn_renderPaging(params);
				
				var str = "";
				var iRowColor = true;											
				
				$.each(data.list, function(key, value){
					if (iRowColor) {
						str += '<tr class="knowledge_th01">';
						iRowColor = false;
					}
					else {
						str += '<tr class="knowledge_th02">';
						iRowColor = true;
					}
						
					str += '<input type="hidden" id="hidSaveChk'+value.SEQ_NO+'" value="N" />' +
						'<input type="hidden" id="hidSeqno'+value.SEQ_NO+'" value="'+value.SEQ_NO+'" />';
						
					for (var i = 0; i < arrChead.length; i++) {
						if (value[arrChead[i]] != undefined) {
							str += '<td><input type="text" class="none_focus" value="'+value[arrChead[i]]+'" id="txtObj'+value.SEQ_NO+'" name="OBJECT" /></td>';	
						}
						else {
							str += '<td><input type="text" class="none_focus" value="" id="txtObj'+value.SEQ_NO+'" name="OBJECT" /></td>';
						}
					}
					str +='<td><img src="../images/delete_btn.png" name="imgdel" /></td>';
					str +='</tr>'; 
				});
				
				body.append(str);
				AddRow(body);
				
				$("a[name='title']").on("click", function(e){ //제목 
					e.preventDefault();
					fn_openBoardDetail($(this));
				});
			}
		}
		
		function AddRow(el) {
			var str = "<tr name='trIn'>";
			
			for (var int = 0; int < ${fn:length(slotlist)}; int++) {
				str += "<td><input type='text' value='' name='OBJECT' /></td>";
			}		
			str +='<td><img src="../images/delete_btn.png" name="imgdel" /></td>';
			str += "</tr>";				
			el.append(str);
		}
		
		function fnSave() {
			var arr = new Object();
			var arrobj = new Array();
			
			var strinsert = 'INSERT INTO slot_class_${SEQ_NO} (';
			
			/* for (var i in arrThead) {
				strinsert += arrThead[i] + ', ';
			} */
			strinsert += strCol + ')';
			//strinsert = strinsert.substr(0, strinsert.length - 2) + ')';
			
			$('#tbslot tbody [name=trIn]').each(function(idx, e) {								
				var item = new Object();				
				var arrobjsub = new Array();				
				var bInserChk = false;							
				var strvalues = 'VALUES(';
				
				$(this).find('input:text').each(function() {
					if($(this).val().trim() != '') {
						bInserChk = true;
					}					
					strvalues += '"' + $(this).val() + '", ';								
				});
				
				if(bInserChk) {
					strvalues = strvalues.substr(0, strvalues.length - 2) + ')';					
					item.ITEM_DETAIL = strvalues;
					arrobj.push(item);
				}
			});
			arr.ITEM = arrobj;
			var jsoobj = JSON.stringify(arr);
			
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/insertslotinstansces.do' />");
			comSubmit.addParam("SEQ_NO", ${SEQ_NO});
			comSubmit.addParam("CLASS_NAME", '${CLASS_NAME}');
			comSubmit.addParam("INSERT_ITEM", strinsert);
			comSubmit.addParam("VALUES_ITEM", jsoobj);
			comSubmit.addParam("SLOT_LIST", '${SLOT_LIST}');
			comSubmit.submit();			
			fnpreventDefault(event);
		}
		
		function fnUpLoadInstances(Type) {
			if(Type == 'O') {
				$("#divExcelUpload").show();
			}
			else {
				var comSubmit = new ComSubmit("ExcelForm");
				comSubmit.setUrl("<c:url value='/view/setslotinstanscesexcel.do' />");
				comSubmit.addParam("SEQ_NO", ${SEQ_NO});
				comSubmit.addParam("CLASS_NAME", '${CLASS_NAME}');
				comSubmit.addParam("HEADER_LIST", arrThead.toString());
				comSubmit.addParam("COL_LIST", arrChead.toString());
				comSubmit.addParam("SLOT_LIST", '${SLOT_LIST}');
				comSubmit.submit();
				fnpreventDefault(event);
			}
		}
		
		function fnLoadInstances() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/getslotinstanscesexcel.do' />");
			comSubmit.addParam("PAGE_INDEX",$("#PAGE_INDEX").val());
			comSubmit.addParam("PAGE_ROW", 10);
			comSubmit.addParam("HEADER_LIST", arrThead.toString());
			comSubmit.addParam("COL_LIST", arrChead.toString());
			comSubmit.addParam("CLASS_NAME", '${CLASS_NAME}');
			comSubmit.addParam("SEQ_NO", ${SEQ_NO});
			comSubmit.submit();
			fnpreventDefault(event);
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>
	<div class="slot">
		<div class="cont_title">
			<h2>Entities</h2>			
			<div style="font-size: 14px; position: absolute; top: 70px; left: 65px; color: #666; line-height: 14px;">
				<!-- <span style="padding:0px 5px;">aaaaa</span><span style="padding:0px 5px;">&gt;</span> -->
				
				<span style="padding:0px 5px; line-height: 16px;height: 16px;">
				<img style="width: 16px; vertical-align: text-top; margin-right: 3px;" src="../images/icon_c_img.png" />${CLASS_NAME }</span><span style="padding:0px 5px;">&gt;</span>
				<span style="padding:0px 5px; line-height: 16px;height: 16px;">Instances</span>
			</div>
			<a href="#" onclick="fnSave();">Save</a>											
		</div>
		
		<div class="solt_contents">		
			<!--search box start-->
			<div class="search_box">
				<div class="s_word">
					<label for="slot_search">
						<input type="text" id="txtSearch" value="search…" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" id="slot_search" name="slot_search" /><a onclick="fnSelectList(1);" href="#" title="검색" class="solt_search"><span class="hide">search</span></a>						
					</label>
				</div>
			</div>
			<!--search box end-->
			
			<dl class="class_name">
				<dt style="margin-right: 17px; margin-left: 17px;">
					<span>${CLASS_NAME}</span>
					<a onclick="fnUpLoadInstances('O');" class="class_name_del" href="#">Upload instances</a>					
					<a onclick="fnLoadInstances();" class="class_name_instances" href="#">Export instances</a>
											
				</dt>
			</dl>
			
			<form id="ExcelForm" name="ExcelForm" enctype="multipart/form-data" method="post">
				<div id="divExcelUpload" class="layer_box w400" style="display:none;">
					<p style="padding-bottom: 10px;"><input type="file" name="excelFile"/></p>
					<div class="layer_box_btn">
						<a onclick="fnUpLoadInstances('S');" href="#">Upload</a>
						<a onclick="javascript:$('#divExcelUpload').hide();" href="#">Cancel</a>			
					</div>		
				</div>
			</form>
		
			<div style="margin:15px; border: 1px solid #e1e1e1; border-bottom: none;overflow: auto;">							
				<table id="tbslot" style="" class="slot_instan_title" summary="TASK_INFORMATION 내용을 보여줍니다.">
					<caption class="hide">Instances 표</caption>					
					<thead>
						<tr>
							<c:forEach items="${slotlist }" var="row">
								<th name="thTitle">${row.VIEW_COL }</th>
								<script>strCol += '${row.QUERY_COL }' + ', ';</script>
							</c:forEach>
							<th style="width:30px;">&nbsp;</th>	
						</tr>						
					</thead>
					<tbody>
						<!-- <tr>
							<td>								
								<input type="text" value="한국전자통신 연구원" id="knowledge_input07" name="knowledge_input01" />								
							</td>
							<td>								
								<input type="text" value="에버트란" id="knowledge_input07" name="knowledge_input02" />								
							</td>
							<td>								
								<input type="text" value="영화표발행" id="knowledge_input08" name="OBJECT" />
							</td>							
						</tr>	 -->					
					</tbody>
					<!-- <tfoot>
						<tr>
							<td colspan="12"><a href="#">Click here to edit row</a></td>
						</tr>
					</tfoot> -->
				</table>			
			</div>
			<!--board paging start-->
			<div id="PAGE_NAVI" class="board_paging">
				<!-- <span class="left">
					<a href="#" class="front"><img src="../images/btn_front.png" /></a>
					<a href="#" class="prev"><img src="../images/btn_prev.png" /></a>
				</span>
				<span class="num_group">
					<a class="on" href="#" title="1번째 페이지 목록 보기">1</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">2</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">3</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">4</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">5</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">6</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">7</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">8</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">9</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">10</a>&nbsp;
				</span>
				<span class="right">
					<a href="#" class="next"><img src="../images/btn_next.png" /></a>
					<a href="#" class="back"><img src="../images/btn_back.png"  /></a>
				</span> -->
			</div>			
			<input type="hidden" id="PAGE_INDEX" name="PAGE_INDEX"/>
			<!--board paging end-->
		</div>
	</div>		
	<%@ include file="/WEB-INF/include/footer.jspf" %>
</body>
</html>