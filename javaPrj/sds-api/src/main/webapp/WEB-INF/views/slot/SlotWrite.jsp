<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript" src="<c:url value='/js/jquery.simplemodal.js'/>"></script>
	<script type="text/javascript">
		var iSeqNo = '';
		var bsaving = false;
		
		window.onload = function() {			
			$('#ObjectList').on({ 
				'focusout': function(e){			
					if(gfn_isNull($(this).text()))
						$(this).text('입력 하세요...');
				},
				click: function(e){			
					if($(this).text() == '입력 하세요...')
						$(this).text('');
				}
			}, '[name=OBJECT_NAME]');
						
			$('#solt_contents').on({				
				'click focusin': function(e) {
					var $this = $(this);
										
					if($this.parent().next().length == 0) {
						$this.parent().parent().append('<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>');
					}
				},
				keydown: function(e) {
					if(e.keyCode == 9) {						
						//$(this).next().focus();
						$(this).parent().next().children().eq(0).focus();
						fnCursorEnd($(this).parent().next().children().eq(0).get(0));
						fnpreventDefault(e);
					}
				}			
			}, '[name=OBJECT_DETAIL_NAME]');
			
			$('#solt_contents').on('click', '.close_btn', function(e) {				
				if($(this).parent().siblings().length > 0)				
					$(this).parent().remove();
			});
			
			if ('${STATUS}' == 'F') {
				alert('엑셀 업로드 중 중복된 값을 발견 했습니다. 제거 후 다시 시도해주세요.');
			}
		}
		
		function fnSlotSave() {
			if (bsaving == true) {
				fnLoading();
				return false;
			}
			else 				
				bsaving = true;
	
			fnLoading();
			
			var arr = new Object();
			var arrobj = new Array();
			var saveChk = true;
			var arrobjChk = new Array();
			Array.prototype.contains = function(elem) {
				for (var i in this) {
					if (this[i] == elem) return true;
				}
				return false;
			}
						
			$('#ObjectList [name=tbOBJ_LIST]').each(function(idx, e) {
				var $this = $(this);
				var item = new Object();
				var arrobjsub = new Array();
				var id = $this.find('[name=OBJECT_NAME]').attr("id");
				var seqno;
				var bObjName = true;
				var iObjDtl = 0;
				
				if(gfn_isNull(id)) {
					item.SEQ_NO = "0";
				} else {
					item.SEQ_NO = id.replace('OBJECT_NAME_', '');					
				}
				
				item.OBJECT_NAME = $this.find('[name=OBJECT_NAME]').text();
				if (item.OBJECT_NAME == '입력 하세요...' || item.OBJECT_NAME.trim() == '') {
					bObjName = false;
				}
				
				if (arrobjChk.contains(item.OBJECT_NAME)) {
					alert('이미 존재하는 값 입니다.');
					$this.find('[name=OBJECT_NAME]').focus();
					saveChk = false;					
					fnLoading();	
					return false;
				}
				
				if (bObjName) {
					$this.find('[name=OBJECT_DETAIL_NAME]').each(function() {
						if(!gfn_isNull($(this).text())) {
							var subitem = new Object();
							subitem.OBJECT_NAME = $(this).text();
							//subitem.PARENT_SEQ_NO = item.SEQ_NO;
							arrobjsub.push(subitem);
							iObjDtl ++;
						}
					});							
									
					arrobjChk.push(item.OBJECT_NAME);
					
					
					//if (iObjDtl > 0) {			
						item.OBJECT_DETAIL = arrobjsub;
						
						arrobj.push(item);
					//}
				}
			});
			if (saveChk) {
				bsaving = false;
				arr.OBJECT_ITEM = arrobj;
				var jsoobj = JSON.stringify(arr);
				
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/insertslotobject.do' />");
				comSubmit.addParam("SLOT_SEQ_NO", ${SLOT_SEQ_NO});
				comSubmit.addParam("OBJECT_ITEM", jsoobj);
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.addParam("CLASS_NAME", '${CLASS_NAME }');
				comSubmit.addParam("SLOT_NAME", '${SLOT_NAME }');
				comSubmit.submit(); 
				/* var comAjax = new ComAjax();			
				comAjax.setUrl("<c:url value='/view/insertslotobject.do' />");	
				comAjax.setCallback("fnSaveComplete");
				comAjax.addParam("SLOT_SEQ_NO", ${SLOT_SEQ_NO});
				comAjax.addParam("OBJECT_ITEM", jsoobj);
				comAjax.addParam("depth1", '${depth1}');
				comAjax.addParam("depth2", '${depth2}');
				comAjax.addParam("depth3", '${depth3}');
				comAjax.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comAjax.addParam("CLASS_NAME", '${CLASS_NAME }');
				comAjax.addParam("SLOT_NAME", '${SLOT_NAME }');
				comAjax.ajax(); */
			}
			fnpreventDefault(event);
		}		
		
		function fnLoadEntry() {
			var comSubmit = new ComSubmit();
			comSubmit.setUrl("<c:url value='/view/getslotobjectexcel.do' />");
			comSubmit.addParam("SLOT_SEQ_NO", ${SLOT_SEQ_NO});			
			comSubmit.addParam("SLOT_NAME", '${SLOT_NAME }');
			comSubmit.addParam("CLASS_NAME", '${CLASS_NAME }');
			comSubmit.addParam("depth1", '${depth1}');
			comSubmit.addParam("depth2", '${depth2}');
			comSubmit.addParam("depth3", '${depth3}');
			comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
			comSubmit.submit();
			fnpreventDefault(event);
		}
		
		function fnUpLoadEntry(Type) {
			if(Type == 'O') {
				$("#divExcelUpload").show();
			}
			else {
				var comSubmit = new ComSubmit("ExcelForm");
				comSubmit.setUrl("<c:url value='/view/setslotobjectexcel.do' />");				
				comSubmit.addParam("SLOT_SEQ_NO", ${SLOT_SEQ_NO});				
				comSubmit.addParam("depth1", '${depth1}');
				comSubmit.addParam("depth2", '${depth2}');
				comSubmit.addParam("depth3", '${depth3}');
				comSubmit.addParam("AGENT_SEQ", '${AGENT_SEQ}');
				comSubmit.addParam("CLASS_NAME", '${CLASS_NAME }');
				comSubmit.addParam("SLOT_NAME", '${SLOT_NAME }');
				comSubmit.submit();
				fnpreventDefault(event);
			}
		}
		
		function fnProgressCancel(type) {
			$.modal.close();
			//$("#divProgress").hide();
			fnpreventDefault(event);
		}
		
		function fnAddRow() {
			$("#ObjectList").append($("#EmptyRow").html());
			fnpreventDefault(event);
		}
		
		function fnObjectDelete(seqno, obj) {
			//if($('#ObjectList [name=tbOBJ_LIST]').length > 1) {
				$(obj).parent().parent().parent().parent().parent().remove();					
			//}
			
			if(seqno != 0) {								
				var comAjax = new ComAjax();
				comAjax.setUrl("<c:url value='/view/deleteslotobject.do' />");				
				comAjax.addParam("SEQ_NO", seqno);			
				comAjax.ajax();	
			}						
			fnpreventDefault(event);
		}
		
		function fnGetObjDetail(seqno) {
			var comAjax = new ComAjax();
			comAjax.setUrl("<c:url value='/view/getslotobjectdetail.do' />");
			comAjax.setCallback("fnCreateObjDetail");
			comAjax.addParam("PARENT_SEQ_NO", seqno);			
			comAjax.ajax();				
		}
		
		function fnCreateObjDetail(data) {
			var iCnt = 0;
			var str = "";			
			$.each(data.DETAIL_LIST, function(key, value){
				str += '<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true">' + value.OBJECT_NAME + '</div><a class="close_btn f_right" href="#"></a></div>';
				iCnt++;
			});
			
			if(iCnt == 0)
				str = '<div class="slot_modify_set02"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>';
			
			$('#OBJECT_NAME_' + data.PARENT_SEQ_NO).parent().next().append(str);
		}		
		
	</script>	
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>	
	<div class="slot">
		<div class="cont_title">
			<h2>Entities</h2>
			<div style="font-size: 14px; position: absolute; top: 70px; left: 65px; color: #666; line-height: 14px;">
				<!-- <span style="padding:0px 5px;">aaaaa</span><span style="padding:0px 5px;">&gt;</span> -->
				
				<span style="padding:0px 5px; line-height: 16px;height: 16px;">
				<img style="width: 16px; vertical-align: text-top; margin-right: 3px;" src="../images/icon_c_img.png" />${CLASS_NAME }</span><span style="padding:0px 5px;">&gt;</span>
				<span style="padding:0px 5px; line-height: 16px;height: 16px;">
				<img style="width: 16px; vertical-align: text-top; margin-right: 3px;" src="../images/icon_s_img.png" />${SLOT_NAME }</span>
			</div>
			<a onclick="fnSlotSave();" href="#">Save</a>
		</div>
		<div id="solt_contents" class="solt_contents">
			<form id="ExcelForm" name="ExcelForm" enctype="multipart/form-data" method="post">
				<div id="divExcelUpload" class="layer_box w400" style="display:none;">
					<p style="padding-bottom: 10px;"><input type="file" name="excelFile"/></p>
					<div class="layer_box_btn">
						<a onclick="fnUpLoadEntry('S');" href="#">Upload</a>
						<a onclick="javascript:$('#divExcelUpload').hide();" href="#">Cancel</a>			
					</div>		
				</div>
			</form>
			
			<dl id="ObjectList" class="class_name mt30">
				<dt style="background: url(../images/icon_s_img.png) no-repeat 0px center; position: relative;">
					<span style="display: inline-block; width: 60%; word-break: break-all;">					
						${CLASS_NAME }.${SLOT_NAME }
					</span>
					<span class="sttable02_title_05">
						<a onclick="fnUpLoadEntry('O');" class="class_name_del" href="#">Upload Entry</a>
						<a onclick="fnLoadEntry();" class="class_name_instances" href="#">Export Entry</a>
					</span>
				</dt>
				
				<c:choose>						
					<c:when test="${fn:length(OBJ_LIST) > 0}">
						<c:forEach items="${OBJ_LIST}" var="row">
							<dd class="solt_dd">
								<table name="tbOBJ_LIST" class="slot_modify">
									<tr>								
										<th>
											<div id="OBJECT_NAME_${row.SEQ_NO}" name="OBJECT_NAME" contenteditable="true">${row.OBJECT_NAME}</div>
										</th>
										<td class="slot_modify01">										
											<script>fnGetObjDetail(${row.SEQ_NO});</script>							
										</td>
										<td class="slot_modify02">
											<a href="#" class="delete_btn" onclick="fnObjectDelete(${row.SEQ_NO}, this);"><span class="hide">삭제</span></a>
										</td>																	
									</tr>
								</table>
							</dd>
						</c:forEach>
					</c:when>					
					<c:otherwise>
						<dd class="solt_dd">
							<table name="tbOBJ_LIST" class="slot_modify">
								<tr>
									<th>
										<div name="OBJECT_NAME" contenteditable="true">입력 하세요...</div>
									</th>
									<td class="slot_modify01">
										<!-- <div name="OBJECT_DETAIL_NAME" class="slot_modify_set01" contenteditable="true">아수라</div>&nbsp; -->										
										<div class="slot_modify_set02 f_left"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>										
									</td>
									<td class="slot_modify02">
										<a href="#" class="delete_btn" onclick="fnObjectDelete(0, this);"><span class="hide">삭제</span></a>
									</td>
								</tr>
							</table>
						</dd>						
					</c:otherwise>
				</c:choose>				
			</dl>
			
			<div class="addrow">
				<a href="#" onclick="fnAddRow();">+Add row</a>
			</div>
		</div>		
	</div>
	<div id="EmptyRow" style="display:none;">
		<dd class="solt_dd">
			<table name="tbOBJ_LIST" class="slot_modify">
				<tr>
					<th>
						<div name="OBJECT_NAME" contenteditable="true">입력 하세요...</div>
					</th>
					<td class="slot_modify01">
						<!-- <div name="OBJECT_DETAIL_NAME" class="slot_modify_set01" contenteditable="true">아수라</div>&nbsp; -->										
						<div class="slot_modify_set02 f_left"><div class="dp_ib" name="OBJECT_DETAIL_NAME" contenteditable="true"></div><a class="close_btn f_right" href="#"></a></div>										
					</td>
					<td class="slot_modify02">
						<a href="#" class="delete_btn" onclick="fnObjectDelete(0, this);"><span class="hide">삭제</span></a>
					</td>
				</tr>
			</table>
		</dd>
	</div>
	<!-- 모달창 -->
	<div id="divProgress" class="layer_box w400" style="display:none;">
		<table>
			<thead>
				<tr>
					<th>&middot; infomation</th>								
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>talker</th>
					<td>
						<div class="s_option">
							<label for="user_group">
								<select class="f_right u_group" id="search_option" name="search_option" data-placeholder="검색조건을 선택하세요">						
									<option value="group_1">system</option>
									<option value="group_2">작성자</option>
								</select>
							</label>
						</div>
					</td>
				</tr>						
				<tr>
					<th>subtype</th>
					<td>
						<div class="s_option">
							<label for="user_group">
								<select class="f_right u_group" id="search_option2" name="search_option" data-placeholder="검색조건을 선택하세요">										
									<option value="group_1">start</option>
									<option value="group_2">restart</option>
								</select>
							</label>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="layer_popup_s">
			<table>
				<thead>
					<tr>
						<th colspan="2">&middot; utterance set</th>
						<td class="layer_popup_btn"><a class="lp_a" href="#"><span class="hide">추가</span></a><a class="lp_d" href="#"><span class="hide">삭제</span></a></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>infomation</th>
						<td class="con_lp_bo">
							<label for="search_option">
								<input type="text" value="" onblur="" onclick="" id="search_option" name="search_option" class="layer_popup_in" />
							</label>
						</td>
						<td class="layer_popup_btn"><a class="lp_a" href="#"><span class="hide">추가</span></a><a class="lp_d" href="#"><span class="hide">삭제</span></a></td>
					</tr>
					<tr>
						<th>talker</th>
						<td class="con_lp_bo">
							<div class="s_option">
								<label for="user_group">
									<select class="f_right u_group" id="search_option" name="search_option" data-placeholder="검색조건을 선택하세요">
										<option value=""></option>
										<option value="group_1">제목</option>
										<option value="group_2">작성자</option>
									</select>
								</label>
							</div>
						</td>
						<td class="layer_popup_btn"><a class="lp_a" href="#"><span class="hide">추가</span></a><a class="lp_d" href="#"><span class="hide">삭제</span></a></td>
					</tr>
					<tr>
						<th>dialog_type</th>
						<td class="con_lp_bo"><p>test 테스트</p></td>
						<td class="layer_popup_btn"><a class="lp_a" href="#"><span class="hide">추가</span></a><a class="lp_d" href="#"><span class="hide">삭제</span></a></td>
					</tr>
				</tbody>
			</table>
		</div>								
		<div class="layer_box_btn">
			<a onclick="fnProgressSave();" href="#">Save</a>
			<a onclick="fnProgressCancel();" href="#">Cancel</a>
		</div>								
	</div>
	<!-- 모달창 -->		
	<%@ include file="/WEB-INF/include/footer.jspf" %>
</body>
</html>