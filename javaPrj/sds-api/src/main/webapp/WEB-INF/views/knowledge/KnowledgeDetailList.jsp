<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>
	<div class="knowledge">
		<div class="cont_title">
			<h2>Knowledge</h2>
			<a href="#">Create</a>
		</div>
		<div class="knowledge_contents">
			<h3 class="ml15">TASK_INFORMATION</h3>
			<!--search box start-->
			<div class="search_box" style="border:none;">
				<div class="s_word">
					<label for="slot_search">
						<input type="text" value="search…" onblur="if (this.value=='') this.value=this.defaultValue" onclick="if (this.defaultValue == this.value) this.value = ''" id="slot_search" name="slot_search" /><a href="#" title="검색" class="solt_search"><span class="hide">search</span></a>
					</label>
				</div>
			</div>
			<!--search box end-->
			<div style="margin:15px; border:1px solid #e1e1e1; solid #e1e1e1;border-bottom: none;">
				<table class="knowledge_title" summary="TASK_INFORMATION 내용을 보여줍니다.">
					<caption class="hide">TASK_INFORMATION 표</caption>
					<colgroup>
						<col width="28%" />
						<col width="24%" />
						<col width="24%" />
						<col width="24%" />
					</colgroup>
					<thead>
						<tr>
							<th>carvatar manufacturer</th>
							<th>carvatar name</th>
							<th>carvatar function</th>
							<th>carvatar method</th>
						</tr>
					</thead>
					<tbody>
						<tr class="knowledge_th01">
							<td>한국전자통신 연구원</td>
							<td>에버트란</td>
							<td>영화표발행</td>
							<td>영화 발행 질문</td>
						</tr>
						<tr class="knowledge_th02">
							<td>한국전자통신 연구원</td>
							<td>에버트란</td>
							<td>영화표발행</td>
							<td>영화 발행 질문</td>
						</tr>
						<tr>
							<td>
								<label for="knowledge_input01">
									<input type="text" value="한국전자통신 연구원" onblur="if (this.value=='') this.value=this.defaultValue" id="knowledge_input01" name="knowledge_input01" />
								</label>
							</td>
							<td>
								<label for="knowledge_input02">
									<input type="text" value="에버트란" onblur="if (this.value=='') this.value=this.defaultValue" id="knowledge_input02" name="knowledge_input02" />
								</label>
							</td>
							<td>
								<label for="knowledge_input03">
									<input type="text" value="영화표발행" onblur="if (this.value=='') this.value=this.defaultValue" id="knowledge_input03" name="knowledge_input03" />
								</label>
							</td>
							<td>
								<label for="knowledge_input04">
									<input type="text" value="영화 발행 질문" onblur="if (this.value=='') this.value=this.defaultValue" id="knowledge_input04" name="knowledge_input04" />
								</label>
							</td>
						</tr>
						<tr>
							<td>
								<label for="knowledge_input01">
									<input type="text" value="" onblur="" onclick="" id="knowledge_input01" name="knowledge_input01" />
								</label>
							</td>
							<td>
								<label for="knowledge_input02">
									<input type="text" value="" onblur="" onclick="" id="knowledge_input02" name="knowledge_input02" />
								</label>
							</td>
							<td>
								<label for="knowledge_input03">
									<input type="text" value="" onblur="" onclick="" id="knowledge_input03" name="knowledge_input03" />
								</label>
							</td>
							<td>
								<label for="knowledge_input04">
									<input type="text" value="" onblur="" onclick="" id="knowledge_input04" name="knowledge_input04" />
								</label>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4"><a href="#">Click here to edit row</a></td>
						</tr>
					</tfoot>
				</table>			
			</div>
			<!--board paging start-->
			<div class="board_paging">
				<span class="left">
					<a href="#" title="처음 페이지 목록 보기" class="front">
						<img src="../images/btn_front.png" alt="처음 페이지 목록 보기" /></a>
					<a href="#" title="이전 페이지 목록 보기" class="prev">
						<img src="../images/btn_prev.png" alt="이전 페이지 목록 보기" /></a>
				</span>
				<span class="num_group">
					<a class="on" href="#" title="1번째 페이지 목록 보기">1</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">2</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">3</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">4</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">5</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">6</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">7</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">8</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">9</a>&nbsp;
					<a href="#" title="1번째 페이지 목록 보기">10</a>&nbsp;
				</span>
				<span class="right">
					<a href="#" title="다음 페이지 목록 보기" class="next">
						<img src="../images/btn_next.png" alt="다음 페이지 목록 보기" /></a>
					<a href="#" title="마지막 페이지 목록 보기" class="back">
						<img src="../images/btn_back.png" alt="마지막 페이지 목록 보기" /></a>
				</span>
			</div>
			<!--board paging end-->
		</div>
	</div>
	<%@ include file="/WEB-INF/include/footer.jspf" %>
</body>
</html>