<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@ include file="/WEB-INF/include/header.jsp" %>
	<script type="text/javascript">
		window.onload = function() {
			$('.class_name').find('.class_name_list').click(function(e) {
				var comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/view/knowledgedetaillist.do' />");
				comSubmit.submit();
			});
		}
	</script>
</head>
<body>
	<%@ include file="/WEB-INF/include/left.jspf" %>
	<div class="knowledge">
		<div class="cont_title">
			<h2>Knowledge</h2>
			<!-- <a href="#">Create</a> -->
		</div>
		<div class="knowledge_contents">            
			<dl class="class_name">
				<dd>
					<a href="#" class="class_name_list">SLOT_MAPPING<span>2 Items</span></a>
				</dd>				
				<dd class="end">
					<a href="#" class="class_name_list">GENERATION_FOMR<span>20 Items</span></a>
				</dd>
			</dl>
		</div>
	</div>
	<%@ include file="/WEB-INF/include/footer.jspf" %>
</body>
</html>