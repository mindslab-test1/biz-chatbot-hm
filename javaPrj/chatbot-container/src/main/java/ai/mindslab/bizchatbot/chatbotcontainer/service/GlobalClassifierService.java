package ai.mindslab.bizchatbot.chatbotcontainer.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotIntentFinderMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.DialogHistoryMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotDomainSimple;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotIntetFinderSdsData;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.IntentFinderDomainRel;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.SdsProjectPortHis;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.SimpleClassifierData;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.ISDSCodes;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.session.data.SdsData;
import ai.mindslab.bizchatbot.commons.session.data.SdsUtterRespData;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.GlobalItfMapper;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfData;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfSdsData;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfSimpleData;
import ai.mindslab.bizchatbot.commons_ex.preitf.dao.data.PreItfData;
import ai.mindslab.bizchatbot.commons_ex.preitf.service.ChatbotServiceEx;
import ai.mindslab.bizchatbot.sds.client.SdsClient;
import ai.mindslab.bizchatbot.sds.client.SdsClientFactory;

@Service
public class GlobalClassifierService implements IChatbotConst{

	private Logger logger = LoggerFactory.getLogger(GlobalClassifierService.class);
	
	private HashMap<String, Pattern> pattern = new HashMap<>();
	
	@Autowired
	private HazelClientCommon hazel;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private ChatbotMapper chatbot;
	
	@Autowired
	private ChatbotIntentFinderMapper itfMapper;
	
	@Autowired
	private DialogHistoryMapper hisMapper;
	
	@Autowired
	private SdsClientFactory sdsFactory;
	
//	@Autowired
//	private ChatbotServiceEx chatbotEx;
	
	@Autowired
	private GlobalItfMapper gItf;
	
	private HashMap<Integer, ArrayList<SdsData>> mapSdsItf = new HashMap<>();
	
	public GlobalClassifierService() {
	}

	public String doSimple(int botId, String userUtter) {
		
		if(StringUtils.isEmpty(userUtter)) {
			return "";
		}
		List<GlobalItfSimpleData> scs = null;
		try {
			scs = getSimpleClassifer(botId);
		}catch(Exception e) {
			logger.warn("doSimple/{}",e.getMessage());
			return "";
		}
		
		String svcTypeCd = "";
		
		Pattern p;
		Matcher m;
		if(scs != null) {
			for(GlobalItfSimpleData sc: scs) {
				p = getPattern(sc.getPattern());
				m = p.matcher(userUtter);
//				if(m.matches()) {
				if(m.find()) {
					svcTypeCd = sc.getSvcTypeCd();
					logger.debug("doSimple/{}/{}",userUtter, svcTypeCd);
					break;
				}
			}
		}
		
		return svcTypeCd;
	}
	
	public List<GlobalItfSimpleData> getSimpleClassifer(int botId) throws Exception{
		//QQQ cache?
		List<GlobalItfSimpleData> scs = null;
		try {
			scs = gItf.getItfSimpleList(botId);
		}catch(Exception e) {
			logger.warn("getSimpleClassifer/{}",e.getMessage());
			throw e;
		}
		return scs;
	}
	
	public Pattern getPattern(String regex) throws PatternSyntaxException{
		
		Pattern p = pattern.get(regex);
		if(p == null) {
			p = Pattern.compile(regex);
			pattern.put(regex, p);
		}
		return p;
	}
	
	public List<GlobalItfSdsData> getIntentFinderSds(int botId) throws Exception{
		logger.info("getIntentFinderSds/{}",botId);
		List<GlobalItfSdsData> list = null;
		try {
			list = gItf.getItfSdsList(botId);
		}catch(Exception e) {
			logger.warn("getIntentFinderSds/{}",e.getMessage());
			throw e;
		}
		return list;
	}
	
	public synchronized boolean hasActiveSdsIntentFinder(int botId) {
		
		//QQQ check 
		ArrayList<SdsData> list = mapSdsItf.get(botId);
		if(list != null) {
			return true;
		}
		return false;
	}
	
	public synchronized void initSdsIntentFinder(int botId) {
		logger.info("initSdsIntentFinder/{}",botId);
		SdsUtterRespData sdsResp = null;
		try {
			List<ChatbotIntetFinderSdsData> sdsIntentFinders = chatbot.getIntentFinderSds(botId);
			if(sdsIntentFinders != null) {
				ArrayList<SdsData> l = new ArrayList<>();
				for(ChatbotIntetFinderSdsData sds: sdsIntentFinders) {
					int nextPrjPort = DialogUtils.getNextProjectPortItf(hazel.getClient(), env);
					logger.info("intent finder SDS Port:{} for {}", nextPrjPort, botId);
					SdsData sData = new SdsData();
					sData.setProjectName(sds.getProjectName());
					sData.setProjectPort(nextPrjPort);
					sData.setDaemonLogFileName("system"+"."+"0000000000");
					sData.setRequest(ISDSCodes.REQUEST_INIT_DIALOG);
					sData.setSdsIp(env.getProperty("sds.ip"));//QQQ clustering case, choose one ip
					sData.setUserName("system");//QQQ with user id?->user name?
					
					SdsClient sdsClient = sdsFactory.makeSdsClient();
					sdsClient.setSdsData(sData);
					sdsResp = sdsClient.initDialog();
					l.add(sData);
					
					//port his
					SdsProjectPortHis hisdata = new SdsProjectPortHis();
					hisdata.setBotId(botId);
					hisdata.setSessionId(sData.getDaemonLogFileName());//set with daemon logfile name
					hisdata.setProjectPort(sData.getProjectPort());
					hisdata.setSdsIp(sData.getSdsIp());
					hisdata.setCloseReasonCd(IChatbotConst.SDS_TERMINATION_BY_SYSTEM);
					hisMapper.insertSdsPortHis(hisdata);
					
					break;//QQQ one domain -> db change
				}
				//store sds intent finder
				mapSdsItf.put(botId, l);
			}
		}catch(Exception e) {
			logger.warn("/initSdsIntentFinder/{}",e.getMessage());
		}
		logger.debug("/initSdsIntentFinder/sdsresp: {}",sdsResp);
	}
	
	public void closeSdsIntentFinders() {
		logger.info("closeSdsIntentFinders/");
		Set<Integer> keys = mapSdsItf.keySet();
		for(Integer key:keys) {
			List<SdsData> list = mapSdsItf.get(key);
			if(list != null) {
				for(SdsData d: list) {
					SdsClient sdsClient = sdsFactory.makeSdsClient();
					sdsClient.setSdsData(d);
					try {
						sdsClient.stopDialog();
					} catch (Exception e) {
						logger.warn("closeSdsIntentFinders/{}",e.getMessage());
					}
				}
			}
		}
	}
	
	public synchronized ArrayList<SdsData> getSdsItfConfig(int botId){
		return mapSdsItf.get(botId);
	}
	
	public IntentFinderDomainRel getIntentFinderInfo(int botId, String intentName){
		//QQQ cache?
		logger.info("getIntentFinderInfo/{},{}", botId, intentName);
		IntentFinderDomainRel info = null;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("botId", botId);
			params.put("intentName", intentName);
			info = itfMapper.getIntentFinderDomainRelation(params);
		}catch(Exception e) {
			logger.warn("getIntentFinderInfo/{}",e.getMessage());
		}
		return info;
	}
	
	public void resetBotSdsItfMap(int botId) {
		mapSdsItf.remove(botId);
	}
}
