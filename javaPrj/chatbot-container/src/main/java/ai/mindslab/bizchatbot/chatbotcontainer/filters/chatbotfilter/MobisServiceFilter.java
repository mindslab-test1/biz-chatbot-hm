package ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.jayway.jsonpath.JsonPath;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.ChatbotOrderedFilter;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiParam;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiResultMappingInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotButton;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichImage;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.SYS_UTTER_TYPE;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.outif.ThirdPartyApiManager;
import ai.mindslab.bizchatbot.commons.outif.ThirdPartyApiManager.ApiResult;
import ai.mindslab.bizchatbot.commons.session.DialogService;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing.Slot;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.ApiUtils;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;
import ai.mindslab.bizchatbot.commons.utils.JsonUtils;
import ai.mindslab.bizchatbot.sds.utils.HttpRequestManager;
import kr.co.mobis.bizchatbot.svc.cafeteria.data.CafeteriaMenuInfo;

public class MobisServiceFilter extends ChatbotOrderedFilter{

	private Logger logger = LoggerFactory.getLogger(MobisServiceFilter.class);

	//Employee service sub task
	enum SUB_TASK{
		userByName,
		userByTask,
		taskByUserId,
		none
	}

	public MobisServiceFilter() {

	}

	public MobisServiceFilter(int order) {
		super(order);
	}


	@Override
	public boolean execBody(DialogSession ses, ChatbotConfig config) throws BizChatbotException {
		try {
				if(ses.getProcessing().getSvcType() == SYS_UTTER_TYPE.EMPLOYEE_INFO) {
					return doEmployeeInfo(ses, config);
			}else if(ses.getProcessing().getSvcType() == SYS_UTTER_TYPE.CAFETERIA_MENU) {
				return doCafeteriaInfo(ses, config);
			}else if(ses.getProcessing().getSvcType() == SYS_UTTER_TYPE.SHUTTLE_BUS) {
				return doBusInfo(ses, config);
			}else if(ses.getProcessing().getSvcType() == SYS_UTTER_TYPE.BUSSTOP_LOC) {
				return doBusStop(ses, config);

			}else if(ses.getProcessing().getSvcType() == SYS_UTTER_TYPE.EMAIL_CHECK) {
				return doEmailCheck(ses, config);

			}else if(ses.getProcessing().getSvcType() == SYS_UTTER_TYPE.APPROVAL_CHECK) {
				return doApprovalCheck(ses, config);

			}else {
				//do next step
				return true;
			}
		} catch (Exception e) {
			logger.warn("execute/{}",e.getMessage());
			DialogUtils.setSessionError(ses, "MobisServiceFilter", e);

			DialogService dialogService = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
			try {
				UtterData utter = DialogUtils.createBotUtterData(ses);
				utter.setSentence("정보조회 오류입니다. (" + ses.getProcessing().getSvcType()+")");
				utter.setCurrStep("MobisServiceFilter");
				utter.setHisDomain(ses.getProcessing().getSvcType().toString());
				long nRet = dialogService.insertBotUtterHistory(utter);
			} catch (Exception e1) {
				logger.warn("execute/{}", e1.getMessage());
			}
		}finally {

//			DialogUtils.resetProcessingData(ses);
//			ses.getProcessing().setSvcType(SYS_UTTER_TYPE.NORMAL);
		}
		return false;
	}


	private boolean doApprovalCheck(DialogSession ses, ChatbotConfig config)  throws BizChatbotException, Exception{

        ThirdPartyApiManager apiManager =  (ThirdPartyApiManager)ApplicationContextProvider.getApplicationContext().getBean(ThirdPartyApiManager.class);
        DialogService dialogService = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
        DialogProcessing processing = ses.getProcessing();

        ChatbotApiInfo apiInfo = null;

        if(processing.getIfId() > 0) {
            try {
                apiInfo = apiManager.getApiInfo(processing.getIfId());
            }catch(Exception e) {
                logger.warn("doApprovalCheck/{}", e.getMessage());
                throw new BizChatbotException(IRestCodes.ERR_CODE_SQL_EXCEPTION, IRestCodes.ERR_MSG_SQL_EXCEPTION);
            }
        }else {
            return true;
        }

        if(apiInfo == null) {
            throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, "doApprovalCheck/api 정보를 찾을 수 없습니다.");
        }

        UtterData utter = DialogUtils.createBotUtterData(ses);
        utter.setCurrStep("MobisServiceFilter");
        utter.setHisDomain(SYS_UTTER_TYPE.APPROVAL_CHECK.toString());

        List<Slot> slots = processing.getExpectedSlots();//request params
        List<ChatbotApiParam> params = apiInfo.getParamList();//required params

		//ApiUtils.setGlobalSlots(params,"1609268", ses.getRemoteAddr(), ses.getDialog().getBotId());
        ApiUtils.setGlobalSlots(params, ses.getUserData().getUserId(), ses.getRemoteAddr(),ses.getDialog().getBotId());


		String resp = "";

		if(IChatbotConst.API_HTTP_METHOD_GET.equals(apiInfo.getMethod())) {
			resp = InterfaceFilter.sendGet(apiInfo);
		}else if(IChatbotConst.API_HTTP_METHOD_POST.equals(apiInfo.getMethod())) {
			resp = InterfaceFilter.sendPost(apiInfo);
		}else {
			throw new Exception("외부 API 설정 오류. method:"+ apiInfo.getMethod());
		}

		int errCode = apiManager.parseCode(resp);
		if(errCode != 0) {
			//api error.
			throw new Exception("외부 API 호출 오류. 에러코드: "+errCode);
		}


		List<ChatbotApiResultMappingInfo> resMappingList = apiManager.getApiResultMappingInfo(apiInfo.getIfId());

		String tmpResp ="";

		if(!StringUtils.isEmpty(resp) && resMappingList != null) {
			try {
				ApiResult apiOut = null;
				ChatbotRichContent richContent = null;
				List<ChatbotButton> buttons = null;
				for(ChatbotApiResultMappingInfo mapping:resMappingList) {
//					if(apiOut == null) {
					try {
						JSONObject obj = new JSONObject(resp);

						//파싱할 데이터가 있을 경우에만...
						if(obj.getJSONObject("data").has("approvals")){
							apiOut = apiManager.parseResult(resp, mapping.getJsonPath());
						}

						if(apiOut != null && apiOut.getResult() instanceof List) {
							if(mapping.getDispName().equals("_result_")) {
								richContent = new ChatbotRichContent();
								richContent.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_BUTTON);
								buttons = apiManager.mappingResultStringToButtons(null, mapping.getDispName(), apiOut.getResult());
								//버튼 문구를 수정해야한다...공통 건들면..문제생길것같아..불편하게한다..
								this.seperateButtonText(buttons, "approvals");
								richContent.setButtons(buttons);
								utter.setRichContent(richContent);
							}
						}else  if( apiOut != null &&  apiOut.getResult() instanceof  Integer){

							String message = "상신받은 결재는 $$CTAG{\"text\":\"_tot_\",\"type\":\"outlink\",\"method\":\"get\",\"url\":\"_url_\",\"params\":{}}CTAG$$건 있습니다.\n";
							tmpResp = apiManager.mappingResultString(message, mapping.getDispName(), apiOut.getResult());
							ApiResult tempApiOut = apiManager.parseResult(resp,"$.data.url");
							tmpResp = apiManager.mappingResultString(tmpResp, "_url_", tempApiOut.getResult());

						}else if (apiOut == null || apiOut.getResult() == null) {
							throw new Exception("상신받은 결재는 0건 있습니다.");
						}
					}catch(Exception e) {
						logger.warn("doApprovalCheck/jsonpath err. {}", e.getMessage());
						tmpResp = apiManager.mappingResultString("상신받은 결재는 0건 있습니다.", mapping.getDispName(), "");
						continue;
					}
					//tmpResp = apiManager.mappingResultString("", mapping.getDispName(), apiOut.getResult());
					logger.debug("doApprovalCheck/{} To. {}", resp, tmpResp);
				}


				//update last sentence
				String outSentence = extractAnswerStringOnly(tmpResp);
				utter.setSentence(outSentence);
				utter.setSysUtterType(ses.getProcessing().getSvcType());
				ses.getDialog().getConversation().add(utter);

				//update history

				long nRet = dialogService.insertBotUtterHistory(utter);
				logger.debug("doApprovalCheck/dialog history updated: {}, count: {}", utter, nRet);

				//reset processing data
				DialogUtils.resetProcessingData(ses);//reset slot
			}catch(Exception ee) {
				logger.warn("doApprovalCheck/SDS Response update error/{}", ee.getMessage());
				throw new Exception("외부 API 결과 맵핑 오류입니다.");
			}
		}

		return false;
	}


	private boolean doEmailCheck(DialogSession ses, ChatbotConfig config) throws BizChatbotException, Exception {

		ThirdPartyApiManager apiManager =  (ThirdPartyApiManager)ApplicationContextProvider.getApplicationContext().getBean(ThirdPartyApiManager.class);
		DialogService dialogService = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		DialogProcessing processing = ses.getProcessing();

		ChatbotApiInfo apiInfo = null;
		if(processing.getIfId() > 0) {
			try {
				apiInfo = apiManager.getApiInfo(processing.getIfId());
			}catch(Exception e) {
				logger.warn("doEmailCheck/{}", e.getMessage());
				throw new BizChatbotException(IRestCodes.ERR_CODE_SQL_EXCEPTION, IRestCodes.ERR_MSG_SQL_EXCEPTION);
			}
		}else {
			return true;
		}

		if(apiInfo == null) {
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, "doEmailCheck/api 정보를 찾을 수 없습니다.");
		}

		UtterData utter = DialogUtils.createBotUtterData(ses);
		utter.setCurrStep("MobisServiceFilter");
		utter.setHisDomain(SYS_UTTER_TYPE.APPROVAL_CHECK.toString());

		List<Slot> slots = processing.getExpectedSlots();//request params
		List<ChatbotApiParam> params = apiInfo.getParamList();//required params

		//ApiUtils.setGlobalSlots(params,"1609268", ses.getRemoteAddr(), ses.getDialog().getBotId());
		//ApiUtils.setGlobalSlots(params,"1110987", ses.getRemoteAddr(), ses.getDialog().getBotId());
		ApiUtils.setGlobalSlots(params, ses.getUserData().getUserId(), ses.getRemoteAddr(), ses.getDialog().getBotId());


		String resp = "";

		if(IChatbotConst.API_HTTP_METHOD_GET.equals(apiInfo.getMethod())) {
			resp = InterfaceFilter.sendGet(apiInfo);
		}else if(IChatbotConst.API_HTTP_METHOD_POST.equals(apiInfo.getMethod())) {
			resp = InterfaceFilter.sendPost(apiInfo);
		}else {
			throw new Exception("외부 API 설정 오류. method:"+ apiInfo.getMethod());
		}
		int errCode = apiManager.parseCode(resp);
		if(errCode != 0) {
			//api error.
			throw new Exception("외부 API 호출 오류. 에러코드: "+errCode);
		}

		List<ChatbotApiResultMappingInfo> resMappingList = apiManager.getApiResultMappingInfo(apiInfo.getIfId());

		String tmpResp =  "";

        if(!StringUtils.isEmpty(resp) && resMappingList != null) {
			try {
				ApiResult apiOut = null;
				ChatbotRichContent richContent = null;
				List<ChatbotButton> buttons = null;

				for (ChatbotApiResultMappingInfo mapping : resMappingList) {
					try {

						JSONObject obj = new JSONObject(resp);

						//파싱할 데이터가 있을 경우에만...
						if(obj.getJSONObject("data").has("emails")){
							apiOut = apiManager.parseResult(resp, mapping.getJsonPath());
						}
						if(apiOut != null && apiOut.getResult() instanceof List) {
							if(mapping.getDispName().equals("_result_")) {
								richContent = new ChatbotRichContent();
								richContent.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_BUTTON);
								buttons = apiManager.mappingResultStringToButtons(null, mapping.getDispName(), apiOut.getResult());

								this.seperateButtonText(buttons, "email");
								richContent.setButtons(buttons);
								utter.setRichContent(richContent);
							}
						}else  if(apiOut != null && apiOut.getResult() instanceof  Integer){
							String message = "읽지 않은 메일은 전체 $$CTAG{\"text\":\"_tot_\",\"type\":\"outlink\",\"method\":\"get\",\"url\":\"_url_\",\"params\":{}}CTAG$$건 있습니다.\n";
							tmpResp = apiManager.mappingResultString(message, mapping.getDispName(), apiOut.getResult());
							ApiResult tempApiOut = apiManager.parseResult(resp,"$.data.url");
							tmpResp = apiManager.mappingResultString(tmpResp, "_url_", tempApiOut.getResult());
						}else if (apiOut == null || apiOut.getResult() == null) {
							throw new Exception("읽지 않은 메일은 전체 0건 있습니다.");
						}

					} catch (Exception e) {
						logger.warn("doEmailCheck/jsonpath err. {}", e.getMessage());
						tmpResp = apiManager.mappingResultString("읽지 않은 메일은 전체 0건 있습니다.", mapping.getDispName(), "");
						continue;
					}

					//tmpResp = apiManager.mappingResultString("", mapping.getDispName(), apiOut.getResult());
					logger.debug("doApprovalCheck/{} To. {}", resp, tmpResp);
				}

				//update last sentence
				String outSentence = extractAnswerStringOnly(tmpResp);
				utter.setSentence(outSentence);
				utter.setSysUtterType(ses.getProcessing().getSvcType());
				ses.getDialog().getConversation().add(utter);

				//update history

				long nRet = dialogService.insertBotUtterHistory(utter);
				logger.debug("doApprovalCheck/dialog history updated: {}, count: {}", utter, nRet);

				//reset processing data
				DialogUtils.resetProcessingData(ses);//reset slot

			}catch(Exception e){
				logger.warn("emailCheck/SDS Response update error/{}", e.getMessage());
				throw new Exception("외부 API 결과 맵핑 오류입니다.");
			}
		}

		return false;
	}

	private boolean doBusStop(DialogSession ses, ChatbotConfig config) {
		//QQQ busstop location info?
		return true;
	}


	private boolean doBusInfo(DialogSession ses, ChatbotConfig config) throws Exception{

		ThirdPartyApiManager apiManager =  (ThirdPartyApiManager)ApplicationContextProvider.getApplicationContext().getBean(ThirdPartyApiManager.class);
		DialogService dialogService = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		DialogProcessing processing = ses.getProcessing();

		ChatbotApiInfo apiInfo = null;
		if(processing.getIfId() > 0) {
			try {
				apiInfo = apiManager.getApiInfo(processing.getIfId());
			}catch(Exception e) {
				logger.warn("doBusInfo/{}", e.getMessage());
				throw new BizChatbotException(IRestCodes.ERR_CODE_SQL_EXCEPTION, IRestCodes.ERR_MSG_SQL_EXCEPTION);
			}
		}else {
			return true;
		}

		if(apiInfo == null) {
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, "doBusInfo/api 정보를 찾을 수 없습니다.");
		}

		UtterData utter = DialogUtils.createBotUtterData(ses);
		utter.setCurrStep("MobisServiceFilter");
		utter.setHisDomain(SYS_UTTER_TYPE.SHUTTLE_BUS.toString());

		List<Slot> slots = processing.getExpectedSlots();//request params
		if(slots.size() < 1) {
			// 필수 파라미터 없음
			return true;
		}
		/*List<Slot> slots = new ArrayList<>();
		Slot slot1 = new Slot();
		Slot slot2 = new Slot();
		slot1.setSlotName("Bus.stopName");
		slot1.setSlotValue("강남");
		slot2.setSlotName("Bus.type");
		slot2.setSlotValue("");
		slots.add(slot1);
		slots.add(slot2);*/


  		List<ChatbotApiParam> params = apiInfo.getParamList();//required params
		JSONObject intentObj = new JSONObject();
		for(ChatbotApiParam p: params) {
			if(DialogUtils.isApiNeedUserInputFilling(p.getParamValue())){
					String fillingType = DialogUtils.extractInputFillingType(p.getParamValue());
				DialogProcessing.Slot slot = DialogUtils.lookupSlot(slots, fillingType);
				//expSlot과 추출된 param type이 같아야만 한다.
				if(slot != null && !StringUtils.isEmpty(slot.getSlotName()) && fillingType.equals(slot.getSlotName())) {
					logger.debug("doBusInfo/input slot filling : {} -> {}", p.getParamValue(), slot.getSlotValue());
					// 앞단에서 api 호출할때 필요한 키워드
					if(slot.getSlotName().indexOf("stopName") > -1){
						intentObj.put("stopName", slot.getSlotValue());
					}else if(slot.getSlotName().indexOf("type") > -1){
						intentObj.put("type", getConvertedTypeParam(slot.getSlotValue()));
					}
					p.setParamValue(slot.getSlotValue());
				}else {

				    /**
                     *타입은 들어오지 않을수도 있지만 정류소는 안됨....
                     * 그래서 정류소가 있으면 타입은 디폴트 처리...
                     */
				    if(intentObj.has("stopName")){
						if(!intentObj.has("type")) {
							intentObj.put("type", "");
						}
                    }else {
						intentObj.put("stopName", "");

						if(!intentObj.has("type")) {
							intentObj.put("type", "");
						}
                      /*  logger.warn("doBusInfo/different slot type, parame: {}", fillingType);
                        DialogUtils.resetProcessingData(ses);//reset slot
                        throw new Exception("API Slot Filling Type Err`or." + slot.getSlotName() + "/" + fillingType);*/
                    }
				}
			}
		}

		logger.info("bus Stop stopName : {}, type: {}", intentObj.get("stopName"), intentObj.get("type"));

		ApiUtils.setGlobalSlots(params, ses.getUserData().getUserId(), ses.getRemoteAddr(), ses.getDialog().getBotId());

		String resp = "";

		if(IChatbotConst.API_HTTP_METHOD_GET.equals(apiInfo.getMethod())) {
			resp = InterfaceFilter.sendGet(apiInfo);
		}else if(IChatbotConst.API_HTTP_METHOD_POST.equals(apiInfo.getMethod())) {
			resp = InterfaceFilter.sendPost(apiInfo);
		}else {
			throw new Exception("외부 API 설정 오류. method:"+ apiInfo.getMethod());
		}
		int errCode = apiManager.parseCode(resp);
		if(errCode != 0) {
			//api error.
			throw new Exception("외부 API 호출 오류. 에러코드: "+errCode);
		}

		try {

			JSONObject obj = new JSONObject(resp);
			String data = "";
			StringBuilder sb = new StringBuilder();

			if(obj.has("data")) {
				data = obj.get("data").toString();

				sb.append("버스 노선 안내해드릴게요.\n");
				sb.append("더 정확한 버스 노선 검색 방법이 궁금하시면 <b>\"버스 노선 찾기\"</b>를 입력해 주세요\n");
				sb.append("마북 버스의 경우 <b>\"승하차 위치\"</b>를 입력하시면 승하차 위치 확인 가능합니다.");

			}else{
				data = "{}";

				sb.append(obj.get("msg").toString());
			}
			intentObj.put("data", data);

			utter.setSentence(sb.toString());
			utter.setIntentJson(intentObj.toString());
			utter.setSysUtterType(ses.getProcessing().getSvcType());
			ses.getDialog().getConversation().add(utter);
			DialogUtils.resetProcessingData(ses);//reset slot

			//update history
			long nRet = dialogService.insertBotUtterHistory(utter);
			logger.debug("doEmployeeInfo/dialog history updated: {}, count: {}", utter, nRet);
			//reset processing data
			DialogUtils.resetProcessingData(ses);//reset slot
		}catch(Exception ee){
			//throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, "Not implemented/BUS INFO.");
			logger.warn("doBusInfo/SDS Response update error/{}", ee.getMessage());
			throw new Exception("외부 API 결과 맵핑 오류입니다.");
		}
		return false;
	}

	private boolean doCafeteriaInfo(DialogSession ses, ChatbotConfig config) throws Exception{

//		throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, "Not implemented/Cafeteria INFO.");
		Environment env = ApplicationContextProvider.getApplicationContext().getEnvironment();
		DialogService dialogService = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		String backendApiUrl = env.getProperty("backend.api.url");

		//do next step
		String remoteAddr = ses.getRemoteAddr();
		String url = backendApiUrl + "/mobis/legacy/cafeteria_menu";
		BasicHeader h = new BasicHeader("Content-Type", ContentType.APPLICATION_FORM_URLENCODED.getMimeType());
		Header[] headers = {h};
		List<BasicNameValuePair> params = new ArrayList<>();

		params.add(new BasicNameValuePair("userId", ses.getUserData().getUserId()));
		logger.info("doCafeteriaInfo user  {}", ses.getUserData().getUserId());

		String resp = "";
		try {
			resp = HttpConnectionManager.getInstance().post(url, params, headers);
		}catch(Exception e) {
			logger.warn("doCafeteriaInfo/interface error/{}", e.getMessage());
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, "Cafeteria menu interface error.");
		}

		BaseResponse<CafeteriaMenuInfo> out = JsonUtils.fromJsonRespUtter(resp, CafeteriaMenuInfo.class);

		UtterData utter = DialogUtils.createBotUtterData(ses);

		if(out == null || out.getData() == null) {
			if( out.getCode() != IRestCodes.ERR_CODE_SUCCESS) {
				logger.warn("doCafeteriaInfo/{}/{}", ses.getUserData().getUserId(), resp);
				throw new BizChatbotException(out.getCode(), out.getMsg());
			}else {
				StringBuilder sb = new StringBuilder();
				sb.append("주말/휴일의 식단 안내는 제공하지 않습니다. \n");
				sb.append("평일에 다시 물어보시면 안내해 드리겠습니다.");
				utter.setSentence(sb.toString());
				utter.setCurrStep("MobisServiceFilter");
				utter.setSysUtterType(ses.getProcessing().getSvcType());
				utter.setHisDomain(SYS_UTTER_TYPE.CAFETERIA_MENU.toString());
				ses.getDialog().getConversation().add(utter);
			}
		}else {
			ChatbotRichContent richContent = new ChatbotRichContent();
			richContent.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_IMAGE);
			ChatbotRichImage img = new ChatbotRichImage();
			img.setImageText("식단 정보 안내");
			img.setImagePath(out.getData().getMenuImage());
			img.setLinkUrl(out.getData().getMenuImage());
			richContent.setImage(img);

			utter.setRichContent(richContent);
			utter.setSentence("");
			utter.setCurrStep("MobisServiceFilter");
			utter.setSysUtterType(ses.getProcessing().getSvcType());
			utter.setHisDomain(SYS_UTTER_TYPE.CAFETERIA_MENU.toString());
			ses.getDialog().getConversation().add(utter);
		}



		//update history
		long nRet = dialogService.insertBotUtterHistory(utter);
		logger.debug("doCafeteriaInfo/dialog history updated: {}, count: {}", utter, nRet);


		return false;
	}

	private boolean doEmployeeInfo(DialogSession ses, ChatbotConfig config) throws BizChatbotException, Exception{
		ThirdPartyApiManager apiManager =  (ThirdPartyApiManager)ApplicationContextProvider.getApplicationContext().getBean(ThirdPartyApiManager.class);
		DialogService dialogService = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);

		DialogProcessing processing = ses.getProcessing();
		ChatbotApiInfo apiInfo = null;
		if(processing.getIfId() > 0) {
			try {
				apiInfo = apiManager.getApiInfo(processing.getIfId());
			}catch(Exception e) {
				logger.warn("doEmployeeInfo/{}", e.getMessage());
				throw new BizChatbotException(IRestCodes.ERR_CODE_SQL_EXCEPTION, IRestCodes.ERR_MSG_SQL_EXCEPTION);
			}
		}else {
			//reset processing data
			DialogUtils.resetProcessingData(ses);//reset slot
			return true;
		}

		if(apiInfo == null) {
			throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, "doEmployeeInfo/api 정보를 찾을 수 없습니다.");
		}

		SUB_TASK subTask = getSubTaskType(apiInfo.getApiName());
		if(subTask == SUB_TASK.taskByUserId || subTask == SUB_TASK.none) {
			//reset processing data
			DialogUtils.resetProcessingData(ses);//reset slot
			//call get employee info from userInfo
			//QQQ goto BQA
			processing.setSvcType(SYS_UTTER_TYPE.EMPLOYEE_INFO);
			return true;
		}

		UtterData utter = DialogUtils.createBotUtterData(ses);
		utter.setCurrStep("MobisServiceFilter");
		utter.setHisDomain(SYS_UTTER_TYPE.EMPLOYEE_INFO.toString());

		List<Slot> slots = processing.getExpectedSlots();//request params
		if(slots.size() < 1) {
			// 필수 파라미터 없음
			//reset processing data
			DialogUtils.resetProcessingData(ses);//reset slot
			return true;
		}
		List<ChatbotApiParam> params = apiInfo.getParamList();//required params

		Iterator<ChatbotApiParam> i = params.iterator();
		while(i.hasNext()) {
			ChatbotApiParam p = i.next();
			if(DialogUtils.isApiNeedUserInputFilling(p.getParamValue())){
				String fillingType = DialogUtils.extractInputFillingType(p.getParamValue());
				DialogProcessing.Slot slot = DialogUtils.lookupSlot(slots, fillingType);
				//expSlot과 추출된 param type이 같아야만 한다.
				if(slot != null && !StringUtils.isEmpty(slot.getSlotName()) && fillingType.equals(slot.getSlotName())) {
					logger.debug("doEmployeeInfo/input slot filling : {} -> {}", p.getParamValue(), slot.getSlotValue());
					p.setParamValue(slot.getSlotValue());
				}else {
					logger.warn("execute/different slot type. expected by sds: {}, param: {}", slot, fillingType);
//					throw new Exception("API Slot Filling Type Error." + slot +"/"+fillingType);

					// replace 되지 않은 slot 값은 parameter에서 제거
					i.remove();
				}
			}
		}
		ApiUtils.setGlobalSlots(params, ses.getUserData().getUserId(), ses.getRemoteAddr(), ses.getDialog().getBotId());

		//api 호출
		String resp = "";
		String baseResp = "_username_님에 관한 정보 안내합니다.";//QQQ
		baseResp = getBaseUtter(subTask);

		//update name
//		baseResp = baseResp.replace("_username_", slots.get(0).getSlotValue());
		if(IChatbotConst.API_HTTP_METHOD_GET.equals(apiInfo.getMethod())) {
			resp = InterfaceFilter.sendGet(apiInfo);
		}else if(IChatbotConst.API_HTTP_METHOD_POST.equals(apiInfo.getMethod())) {
			resp = InterfaceFilter.sendPost(apiInfo);
		}else {
			throw new Exception("외부 API 설정 오류. method:"+ apiInfo.getMethod());
		}

		int errCode = apiManager.parseCode(resp);
		if(errCode != 0) {
			//api error.
			throw new Exception("외부 API 호출 오류. 에러코드: "+errCode);
		}

		JSONObject obj = new JSONObject(resp);
		//파싱할 데이터가 없으면 skip
		if(!obj.has("data")){
			//reset processing data
			DialogUtils.resetProcessingData(ses);//reset slot
			return true;
		}

		List<ChatbotApiResultMappingInfo> resMappingList = apiManager.getApiResultMappingInfo(apiInfo.getIfId());
		if(!StringUtils.isEmpty(resp) && resMappingList != null) {

			try {
				String tmpResp = getButtonString(subTask);
				ApiResult apiOut = null;
				ChatbotRichContent richContent = null;
				List<ChatbotButton> buttons = null;
				for(ChatbotApiResultMappingInfo mapping:resMappingList) {
//					if(apiOut == null) {
					try {
						apiOut = apiManager.parseResult(resp, mapping.getJsonPath());
						if(apiOut != null && apiOut.getResult() instanceof List) {
							if(mapping.getDispName().equals("_result_")) {
								richContent = new ChatbotRichContent();
								richContent.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_BUTTON);
								buttons = apiManager.mappingResultStringToButtons(tmpResp, mapping.getDispName(), apiOut.getResult());
								richContent.setButtons(buttons);
								utter.setRichContent(richContent);
							}
						}else if(apiOut == null || apiOut.getResult() == null) {
							throw new Exception("사용자 정보가 존재하지 않습니다.");
						}
//					}
					}catch(Exception e) {
						logger.warn("doEmployeeInfo/jsonpath err. {}", e.getMessage());
						tmpResp = apiManager.mappingResultString(tmpResp, mapping.getDispName(), "");
						continue;
					}
					tmpResp = apiManager.mappingResultString(tmpResp, mapping.getDispName(), apiOut.getResult());
					logger.debug("doEmployeeInfo/{} To. {}", resp, tmpResp);
				}

				//update last sentence
				String outSentence = extractAnswerStringOnly(tmpResp);
				utter.setSentence(outSentence);
				switch(subTask) {
					case userByName:
					case userByTask:
						utter.setSentence(baseResp);
						break;
				}

				utter.setSysUtterType(ses.getProcessing().getSvcType());
				ses.getDialog().getConversation().add(utter);

				//update history
				long nRet = dialogService.insertBotUtterHistory(utter);
				logger.debug("doEmployeeInfo/dialog history updated: {}, count: {}", utter, nRet);

				//reset processing data
				DialogUtils.resetProcessingData(ses);//reset slot
			}catch(Exception ee) {
				logger.warn("doEmployeeInfo/SDS Response update error/{}", ee.getMessage());
				throw new Exception("외부 API 결과 맵핑 오류입니다.");
			}
		}

		return false;
	}

	private String extractAnswerStringOnly(String sysUtter) {
		if(StringUtils.isEmpty(sysUtter)) {
			return "";
		}

		//String out = sysUtter.replaceAll("_.*_", "");
		//return out;
		return sysUtter;
	}

	private SUB_TASK getSubTaskType(String apiName) {
		if(SUB_TASK.userByName.toString().equals(apiName)) {
			return SUB_TASK.userByName;
		}else if(SUB_TASK.userByTask.toString().equals(apiName)) {
			return SUB_TASK.userByTask;
		}else if(SUB_TASK.taskByUserId.toString().equals(apiName)) {
			return SUB_TASK.taskByUserId;
		}else {
			return SUB_TASK.none;
		}
	}

	private String getBaseUtter(SUB_TASK task) {
		String utter = "";
		switch(task) {
		case userByName:
		case userByTask:
			utter = "찾으시는분이 아래와 같이 확인되었어요.\n버튼을 누르면 연락처, 이메일 등 사우정보를 알려드립니다.";
			break;
		case taskByUserId:
			utter = "담당자: _deptname_ - _username_\n>담당자가 등록한 업무 안내가 있습니다.\n_result_";
			break;
		default:
			utter = "_result_";
		}

		return utter;
	}

	//각 버튼에 들어가는 문구 만들기
	private String getButtonString(SUB_TASK task) {
		String utter = "";
		switch(task) {
		case userByName:
			utter = "_result_";//teamname username
			break;
		case userByTask:
			utter = "_result_";//dispName
			break;
		case taskByUserId:
			utter = "_deptname_ _username_";//teamname + username
			break;
		default:
			utter = "_result_";
		}

		return utter;
	}

	private  void seperateButtonText(List<ChatbotButton> buttons, String type ) {

		if(buttons != null && buttons.size() > 0) {
			for(ChatbotButton button : buttons){
				String[] text = button.getTitle().split("\\$CSS");

				if(type.equals("email")) {
					button.setTitle(emailbtnForm(text[0]));
				}else{
					button.setTitle(approvalFrom(text[0]));
				}
				button.setUserUtter(text[1]);
			}
		}
	}

	private String emailbtnForm(String str) {
		String[] array = str.split(",");
		StringBuilder sb = new StringBuilder();

		if(array.length > 1 ){
			sb.append("제목 : " + array[0].trim() +"\n");
			sb.append("보낸사람 : "  + array[1].trim() +"\n");
			sb.append("받은날짜 : "  + array[2].trim() +"\n");
		}
		return sb.toString();
	}

	private   String  approvalFrom(String str) {
		String[] array = str.split(",");
		StringBuilder sb = new StringBuilder();

		if(array.length > 1 ){
			sb.append("제목 : " + array[0].trim() +"\n");
			sb.append("작성자 : "  + array[1].trim() +"\n");
			sb.append("작성날짜 : "  + array[2].trim() +"\n");
		}
		return sb.toString();
	}


	private String getConvertedTypeParam(String type) {
		String pType = null;
		int num = 0;

		if(type != null) {
			if(StringUtils.isNumeric(type)) {
				num = Integer.parseInt(type);
			}

			if(num == 1 || StringUtils.equals(type.trim(), "출근")) {
				pType = "출근";
			} else if(num == 2 || StringUtils.equals(type.trim(), "퇴근")) {
				pType = "퇴근";
			} else if(num == 3 || StringUtils.equals(type.trim(), "셔틀")) {
				pType = "셔틀";
			} else if(num == 4 || StringUtils.contains(type.trim(), "순환")) {
				pType = "순환";
			}
		}

		return pType;
	}

}
