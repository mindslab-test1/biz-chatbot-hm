package ai.mindslab.bizchatbot.chatbotcontainer.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.bizchatbot.chatbotcontainer.config.AppConfig;
import ai.mindslab.sds.dao.SdsApiMapper;
import ai.mindslab.sds.dao.data.SdsSlotInfo;

@Service
public class SdsApiService {

	private Logger logger = LoggerFactory.getLogger(SdsApiService.class);
	
	@Autowired
	private SdsApiMapper sdsApiMapper;
	
	public SdsApiService() {
	}

	@Transactional(transactionManager=AppConfig.TRANSACTION_MANAGER_SDS, rollbackFor={Exception.class})
	public List<SdsSlotInfo> getSdsSlopInfo(int projectSeq) throws Exception{
		logger.info("getSdsSlopInfo/{}",projectSeq);
		List<SdsSlotInfo> list = null;
		
		try {
			list = sdsApiMapper.getSlotInfo(projectSeq);
		}catch(Exception e) {
			logger.warn("getSdsSlopInfo/{}", e.getMessage());
		}
		
		return list;
	}
	
	@Transactional(transactionManager=AppConfig.TRANSACTION_MANAGER_SDS, rollbackFor={Exception.class})
	public List<SdsSlotInfo> getSdsSlotInfo(String projectName) throws Exception{
		logger.info("getSdsSlopInfo/{}",projectName);
		List<SdsSlotInfo> list = null;
		
		try {
			list = sdsApiMapper.getSlotInfoByName(projectName);
		}catch(Exception e) {
			logger.warn("getSdsSlopInfo/{}", e.getMessage());
		}
		
		return list;
	}
}
