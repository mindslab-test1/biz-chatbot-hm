package ai.mindslab.bizchatbot.chatbotcontainer.filters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;

@Component
@Scope("prototype")
public class FilterChain {
	private Logger logger = LoggerFactory.getLogger(FilterChain.class);
	private ArrayList<ChatbotOrderedFilter> filters;
	public FilterChain() {
		filters = new ArrayList<>();
	}

	public void addFilter(ChatbotOrderedFilter filter) {
		filters.add(filter);
	}
	
	public void execute(DialogSession ses, ChatbotConfig config) throws BizChatbotException{
		
		//QQQ ordering
		Collections.sort(filters, new Comparator<ChatbotOrderedFilter>() {

			@Override
			public int compare(ChatbotOrderedFilter o1, ChatbotOrderedFilter o2) {
				
				return o1.getOrder() > o2.getOrder() ? 1 : (o1.getOrder() == o2.getOrder() ? 0 : -1);
			}
		});
		
		for(ChatbotOrderedFilter filter: filters) {
			if(!filter.execute(ses, config)) {
				logger.debug("execute/last/{}",filter.getClass());
				break;
			}
		}
	}
	
}
