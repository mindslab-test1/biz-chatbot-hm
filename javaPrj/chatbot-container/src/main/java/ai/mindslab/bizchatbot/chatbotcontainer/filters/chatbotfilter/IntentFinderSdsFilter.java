package ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ai.mindslab.bizchatbot.chatbotcontainer.service.BackendApiService;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.ChatbotOrderedFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.service.ClassifierService;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.IntentFinderDomainRel;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.session.DialogService;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.SdsData;
import ai.mindslab.bizchatbot.commons.session.data.SdsUtterRespData;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.sds.client.BaseClient;
import ai.mindslab.bizchatbot.sds.client.SdsClient;
import ai.mindslab.bizchatbot.sds.client.SdsClientFactory;
import ai.mindslab.bizchatbot.sds.client.SdsNgItfClient;

public class IntentFinderSdsFilter extends ChatbotOrderedFilter{

	private Logger logger = LoggerFactory.getLogger(IntentFinderSdsFilter.class);
	
	private static Pattern p;
	static {
		p = Pattern.compile("SLU.*\\#(.+)\\(\\)");
	}
	private String intentFinderDomain;
	private int intentFinderPort;
	
	public IntentFinderSdsFilter() {
	}
	
	public IntentFinderSdsFilter(int order) {
		super(order);
	}
	
	public String getIntentFinderDomain() {
		return intentFinderDomain;
	}

	public void setIntentFinderDomain(String intentFinderDomain) {
		this.intentFinderDomain = intentFinderDomain;
	}

	public int getIntentFinderPort() {
		return intentFinderPort;
	}

	public void setIntentFinderPort(int intentFinderPort) {
		this.intentFinderPort = intentFinderPort;
	}

	@Override
	public boolean execBody(DialogSession ses, ChatbotConfig config) throws BizChatbotException {
		DialogService ds = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		
		//DNN setting : N
		if("N".equals(config.getDnnYn())){
			//skip svm ITF finder -- > skip
			List<String> sdsDomains = ds.getSdsDomainList(ses.getDialog().getBotId());
			
			
			if(!StringUtils.isEmpty(ses.getProcessing().getSdsDomain())) {
				//simple classfier에서 처리한 경우 
				return true;
				
			}else if(sdsDomains.size() == 1) {
				//check if configured single sds domain -> direct routing 
				DialogProcessing processing = ses.getProcessing();
				processing = new DialogProcessing();
				processing.setSdsDomain(sdsDomains.get(0));
//				processing.setCurrBotUtter("");//QQQ leave empty?
				ses.setProcessing(processing);
				logger.debug("execBody/single sds domain {}",sdsDomains.get(0));
				return true;
			 
			}else {
				//multiple?
				logger.warn("botId {} has multiple sds domains. but ITF is not configured. Stop Conversation", ses.getDialog().getBotId());
				try {
					UtterData utter = DialogUtils.createBotUtterData(ses);
					UtterData lastUserUtter = DialogUtils.getLastUserUtter(ses);

					DialogUtils.setUnknownMsg(utter, config);
					this.setNlpUtter(utter, lastUserUtter);

					ses.getDialog().getConversation().add(utter);
					ds.insertBotUtterHistory(utter);
				}catch(Exception e) {
					logger.warn("execBody/{}",e.getMessage());
					DialogUtils.setSessionError(ses, "IntentFinderSdsFilter", new Exception("SDS IntentFinder is not specified."));
				}
				return false;
			}
		}
		
		if(StringUtils.isEmpty(this.intentFinderDomain)) {
			logger.warn("execute/intentFinderDomain is null/STOP");
			return false;
		}
		
		DialogProcessing processing = ses.getProcessing();
		if(processing == null || StringUtils.isEmpty(processing.getSdsDomain())) {
			try {
				UtterData lastUserUtter = DialogUtils.getLastUserUtter(ses);
				String lastSentence = lastUserUtter.getSentence();
				
				//send to sds intent finder.
				String foundDomain = queryIntent(ses, lastSentence);
				if(StringUtils.isEmpty(foundDomain)) {
					UtterData utter = DialogUtils.createBotUtterData(ses);
//					String unknownMsg = DialogUtils.getRandomMsg(config.getUnknownMsg());
//					//String unknownMsg = ds.getChatbotMsgs(ses.getDialog().getBotId(), IChatbotConst.MSG_TYPE_UNKNOWN);
//					utter.setSentence(unknownMsg);
//					utter.setMsgTypeCd(IChatbotConst.MSG_TYPE_UNKNOWN);
					DialogUtils.setUnknownMsg(utter, config);
					this.setNlpUtter(utter, lastUserUtter);

					ses.getDialog().getConversation().add(utter);
					ds.insertBotUtterHistory(utter);
					
					return false;//stop next filter
				}
				
				processing = new DialogProcessing();
				processing.setSdsDomain(foundDomain);
				processing.setCurrBotUtter("");//QQQ leave empty?
				ses.setProcessing(processing);
			} catch (Exception e) {
				logger.warn("IntentFinderSdsFilter/User Utter is null/"+ses.getSessionId(), e);
				DialogUtils.setSessionError(ses, "IntentFinderSdsFilter", e);
				
				return false;
			}
		}
		return true;
	}
	
	
	private String queryIntent(DialogSession ses, String utter) throws Exception{
		logger.info("queryIntent in SDS/{}",utter);
		
		Environment env = (Environment)ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
		
		String clientType = env.getProperty("sds.client.type");
		if(StringUtils.isEmpty(clientType)) {
			clientType = "base";
		}
		
		String sdsDomain = "";
		try {
			if(IChatbotConst.CLIENT_TYPE_BASE.equals(clientType)) {
				sdsDomain = doBaseQuery(ses, utter);
			}else if(IChatbotConst.CLIENT_TYPE_NG.equals(clientType)) {
				sdsDomain = doNgQuery(ses, utter);
			}
		}catch(Exception e) {
			logger.warn("queryIntent/{}",e.getMessage());
		}
		return sdsDomain;
	}
	
	private String doBaseQuery(DialogSession ses, String utter) throws Exception{
		SdsClientFactory sdsFactory = (SdsClientFactory)ApplicationContextProvider.getApplicationContext().getBean(SdsClientFactory.class);
		ClassifierService cfService = (ClassifierService)ApplicationContextProvider.getApplicationContext().getBean(ClassifierService.class);
//		Environment env = (Environment)ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
		
		SdsClient sdsClient = sdsFactory.makeSdsClient();
		if(!cfService.hasActiveSdsIntentFinder(ses.getDialog().getBotId())) {
			cfService.initSdsIntentFinder(ses.getDialog().getBotId());
		}
		
		String intentName = "";
		String sdsDomain = "";
		
		try {
			ArrayList<SdsData> itfData = cfService.getSdsItfConfig(ses.getDialog().getBotId());
			if(itfData != null) {
				for(SdsData d: itfData) {
					//Sds query as a intent finder
					sdsClient.setSdsData(d);
					SdsUtterRespData resp = sdsClient.userUtter(utter);
					String debug = "";
					
					if(resp != null) {
						//QQQ use SLU part for parsing
	//					debug = resp.getDebug();
						debug = resp.getSlu();
						logger.debug("doBaseQuery/{}",debug);
						//QQQ check slu_weight?
						
						intentName = extractIntentName(debug);
						IntentFinderDomainRel sdsDomainName = cfService.getIntentFinderInfo(ses.getDialog().getBotId(), intentName);
						//if(!StringUtils.isEmpty(intentName)) {
						if(sdsDomainName != null) {
							if(!StringUtils.isEmpty(sdsDomainName.getSdsDomain())) {
								//set sds data in curretn session
								ses.getProcessing().setSdsDomain(sdsDomainName.getSdsDomain());
		//						ses.setSdsData(d);
								ses.setItfData(d);//QQQ ses에 저장 필요?
								
								sdsDomain = sdsDomainName.getSdsDomain();
								break;
							}
						}else {
							//QQQ show unknown message
							sdsDomain = null;
						}
					}
				}
			}
		}catch(ConnectException e) {
			logger.warn("can't connect to Intent Finder");
			cfService.resetBotSdsItfMap(ses.getDialog().getBotId());//remove connection info
			throw e;
		}catch(Exception e) {
			throw e;
		}
		
		return sdsDomain;
	}
	
	private String doNgQuery(DialogSession ses, String utter) throws Exception{
		
		String sdsDomain = "";
		SdsNgItfClient client = ApplicationContextProvider.getApplicationContext().getBean(SdsNgItfClient.class);
		try {
			//Sds query as a intent finder
			sdsDomain = client.intentQuery(ses.getDialog().getBotId(), utter);
			if(!StringUtils.isEmpty(sdsDomain)) {
				ses.getProcessing().setSdsDomain(sdsDomain);
			}
		}catch(ConnectException e) {
			logger.warn("can't connect to Intent Finder");
			throw e;
		}catch(Exception e) {
			throw e;
		}
		
		return sdsDomain;
	}

	private String extractIntentName(String debug) {
		logger.debug("extractIntentName/{}",debug);
		
		String foundIntent = "";
//		Pattern p;
		Matcher m;
//		p = Pattern.compile("SLU.*\\#(.+)\\(\\)");
		m = p.matcher(debug);
		if(m.find()) {
			foundIntent = m.group(1);
			logger.debug("extractIntentName/found intent:{}",foundIntent);
		}
		
		return foundIntent;
	}

	// unknown시에 lastUserUtter 형태소 분석해서 명사만 추출하기 - MRC & 검색을 위한 기능
	private void setNlpUtter(UtterData utter, UtterData lastUserUtter) {
		BackendApiService bApiService = (BackendApiService)ApplicationContextProvider.getApplicationContext().getBean(BackendApiService.class);
		BaseResponse<?> resp = bApiService.doNlpAnalyze(lastUserUtter.getSentence());
		if(resp.getCode() == IRestCodes.ERR_CODE_SUCCESS) {
			utter.setNlpUtter((String) resp.getData());
		}
	}
}
