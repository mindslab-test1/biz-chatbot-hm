package ai.mindslab.bizchatbot.chatbotcontainer.controller;

import java.util.Map;
import java.util.concurrent.Callable;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import ai.mindslab.bizchatbot.chatbotcontainer.filters.FilterManager;
import ai.mindslab.bizchatbot.chatbotcontainer.service.ContainerBotService;
import ai.mindslab.bizchatbot.commons.cache.service.ExternalCacheService;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.SdsProjectPortHis;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.ISDSCodes;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.session.DialogService;
import ai.mindslab.bizchatbot.commons.session.data.DialogError;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.SdsData;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DateUtils;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.sds.client.BaseClient;
import ai.mindslab.bizchatbot.sds.client.SdsClient;
import ai.mindslab.bizchatbot.sds.client.SdsClientFactory;
import ai.mindslab.bizchatbot.sds.client.SdsNgClient;
import jhcho.chatbot.processpool.SdsConfProvider;

@RestController
@RequestMapping("/dialog")
public class DialogController implements IChatbotConst, IRestCodes{

	private Logger logger = LoggerFactory.getLogger(DialogController.class);
	
	@Autowired
	private HazelClientCommon hazel;
	
	@Autowired
	private ContainerBotService botService;
	
	@Autowired
	private DialogService dlgService;
	
	@Autowired
	private ExternalCacheService extCacheService;
	
	@Autowired
	private Environment env;
	
	public DialogController() {
	}
	
	@PostConstruct
	public void initNgChatbot() {
		
		String clientType = env.getProperty("sds.client.type");
		if(IChatbotConst.CLIENT_TYPE_NG.equals(clientType)) {
			if(SdsConfProvider.getInstance().needConfig()) {
				SdsConfProvider.getInstance().setEnginePath(env.getProperty("sds.engine.path"));
				SdsConfProvider.getInstance().setDicPath(env.getProperty("sds.dic.path"));
				SdsConfProvider.getInstance().setRuntime("dial");
			}
		}
	}

//	@RequestMapping(value="/sendText",method = {RequestMethod.POST})
//	public BaseResponse<RespDialog> sendText(@RequestBody ReqDialog reqDialog, HttpServletRequest req) throws Exception{
//		logger.info("/dialog/sendText");
//		
//		
//		Map<String,DialogSession> sessionMap = hazel.getClient().getMap(MAP_NAME_SESSION);    // get map from hazel cast
//		
//		if(sessionMap == null) {
//			throw new BizChatbotException(ERR_CODE_INVALID_SESSION_ID, ERR_MSG_INVALID_SESSION_ID);
//		}else {
//			BaseResponse<RespDialog> resp = new BaseResponse<>();
//			RespDialog out = new RespDialog();
//			out.setSessionId(reqDialog.getSessionId());
//			out.setChatbotId(reqDialog.getChatbotId());
//			out.setUserId(reqDialog.getUser().getUserId());
//			
//			DialogSession val = sessionMap.get(reqDialog.getSessionId());
//			//update test
//			UtterData utter = new UtterData();
//			utter.setSpeaker(DIALOG_SPEAKER.CHATBOT);
//			utter.setSentence(reqDialog.getUtter().getSentence());
//			utter.setSessionId(reqDialog.getSessionId());
//			utter.setTimestamp(DateUtils.getNow());
//			utter.setUserId(val.getUserData().getUserId());
//			utter.setDialogSeq(DialogUtils.getNextSeq(val));
//			
//			val.getDialog().getConversation().add(utter);
//			
//			try {
//				ITopic<UtterData> topic = hazel.getClient().getReliableTopic(DialogUtils.getDialogTopic(utter, DIALOG_SPEAKER.CHATBOT));
//				topic.publish(utter);
//				out.setResult(TALK_RESULT.SUCCESS);
//			}catch(TopicOverloadException e) {
//				logger.warn("session:{}/too slow to send :{}",utter.getSessionId(), utter);
//				out.setResult(TALK_RESULT.SYS_FAILURE);
//			}
//			return resp;
//		}
//	}
	
//	@Autowired
//	private SdsClient sdsClient;
	
	@Autowired
	private SdsClientFactory sdsFactory;
	
	@RequestMapping(value="/openDialog", method = {RequestMethod.POST})
	public @ResponseBody Callable<BaseResponse<UtterData>> checkMsg(@RequestParam(value="sessionId")String sessionId, HttpServletRequest req) throws Exception{
		logger.info("/openDialog/{}",sessionId);
		
		Callable<BaseResponse<UtterData>> resp = new Callable<BaseResponse<UtterData>>() {
			@Override
			public BaseResponse<UtterData> call() throws Exception {
				//QQQ !IMPORTANT 반드시 지울것~!!
//				try {
//					extCacheService.clearCacheConfig("botConfig");
//				}catch(Exception e) {
//				}
				
				BaseResponse<UtterData> r = new BaseResponse<>();
				Map<String,DialogSession> sessionMap = hazel.getClient().getMap(MAP_NAME_SESSION);
				DialogSession ses = sessionMap.get(sessionId);
				
				if(ses == null) {
					r.setCode(ERR_CODE_INVALID_SESSION_ID);
					r.setMsg(ERR_MSG_INVALID_SESSION_ID);
					return r;
				}

				//userUtter에서 session 생성하는 것으로 변
				return r;
			}
		};
		return resp;
	}
	
	@RequestMapping(value="/userUtter", method = {RequestMethod.POST})
	public @ResponseBody Callable<BaseResponse<UtterData>> userUtter(@RequestParam(value="sessionId")String sessionId, HttpServletRequest req) throws Exception{
		logger.info("/userUtter/{}",sessionId);
		
		
		Callable<BaseResponse<UtterData>> resp = new Callable<BaseResponse<UtterData>>() {
			@Override
			public BaseResponse<UtterData> call() throws Exception {
				
				int iter = 0;
				while(true) {
					DialogSession ses = null;
					Map<String,DialogSession> sessionMap = null;
					BaseResponse<UtterData> r = new BaseResponse<>();
					try {
						sessionMap = hazel.getClient().getMap(MAP_NAME_SESSION);
						ses = sessionMap.get(sessionId);
						
						//QQQ chatbot container logic here
						//QQQ temp pass to sds
						if(ses == null) {
							r.setCode(ERR_CODE_INVALID_SESSION_ID);
							r.setMsg(ERR_MSG_INVALID_SESSION_ID);
							return r;
						}
						
						//happycall -> STT에서 문장 종료전에 계속 보내는 상
	//					if(ses.getSdsData() != null && StringUtils.isEmpty(ses.getSdsData().getSdsIp()) ) {
	//						//sds session is already opend
	//						
	//					}
						
						DialogUtils.resetSessionError(ses);
					
						//QQQ 이전 종료 domain close
	//					if(ses.getResetSds() != null) {
	//						
	//					}
					
						FilterManager fm = botService.createFilterManager(ses.getDialog().getBotId());
						fm.execute(ses);
					}catch(Exception e) {
						if(ses != null) {
							DialogError err = ses.getDlgError();
							r.setCode(err.getErrCode());
							r.setMsg(err.getErrMsg());
							r.setData(null);
						}else {
							r.setCode(ERR_CODE_FAILURE);
							r.setMsg(e.getMessage());
							r.setData(null);
						}
						return r;
					}
					ses.setLastModified(DateUtils.getNow());
					sessionMap.put(ses.getSessionId(), ses);
					
					DialogError err = ses.getDlgError();
					if(err == null) {
						r.setData(DialogUtils.getLastSystemUtter(ses));
						return r;
					}else if(err.getErrCode() == ERR_CODE_SDS_OPEN_ERROR) {
						String clientType = env.getProperty("sds.client.type");
						if(IChatbotConst.CLIENT_TYPE_BASE.equals(clientType)) {
							if(iter >= 5) {
								r.setCode(err.getErrCode());
								r.setMsg(err.getErrMsg());
								r.setData(null);
								return r;
							}
							logger.warn("sds open error - try again");
							iter++;
							continue;
						}else {
							//NG
							r.setCode(err.getErrCode());
							r.setMsg(err.getErrMsg());
							r.setData(null);
							return r;
						}
					}else {
						r.setCode(err.getErrCode());
						if(StringUtils.isEmpty(err.getErrMsg())) {
							r.setMsg(IRestCodes.ERR_MSG_FAILURE);//default msg
						}else {
							r.setMsg(err.getErrMsg());
						}
						r.setData(null);
						return r;
					}
				}
			}
		};
		return resp;
	}
	
	@RequestMapping(value="/closeDialog", method = {RequestMethod.POST})
	public @ResponseBody Callable<BaseResponse<String>> closeDialog(@RequestParam(value="sessionId")String sessionId, HttpServletRequest req) throws Exception{
		logger.info("/closeDialog/{}",sessionId);
		
		Callable<BaseResponse<String>> resp = new Callable<BaseResponse<String>>() {
			@Override
			public BaseResponse<String> call() throws Exception {
				
				BaseResponse<String> r = new BaseResponse<>();
				try {
					Map<String,DialogSession> sessionMap = hazel.getClient().getMap(MAP_NAME_SESSION);
					DialogSession ses = sessionMap.get(sessionId);
					if(ses == null) {
						r.setCode(ERR_CODE_INVALID_SESSION_ID);
						r.setMsg(ERR_MSG_INVALID_SESSION_ID);
						return r;
					}
					
					//make instance from factory
					BaseClient sdsClient = null;
					String clientType = env.getProperty("sds.client.type");
					if(IChatbotConst.CLIENT_TYPE_BASE.equals(clientType)) {
						sdsClient = sdsFactory.makeSdsClient();
					}else if(IChatbotConst.CLIENT_TYPE_NG.equals(clientType)) {
						sdsClient = sdsFactory.makeSdsNgClient();
						sdsClient.setSessionId(ses.getSessionId());
					}
					
					SdsData sData = ses.getSdsData();
					ses.setSdsData(sData);
					ses.setLastModified(DateUtils.getNow());
					ses.setActive(false);
					sessionMap.put(ses.getSessionId(), ses);
					
					if(sData == null && ses.getResetSds() == null) {
						//no need to close sds session
						return r;
					}
					
					SdsData sdsDataHis = null;
					try {
						if(sData == null) {
							sdsDataHis = ses.getResetSds();
							sdsClient.setSdsData(ses.getResetSds());
//							sdsClient.stopDialog();
							sdsClient.stopDialog();
						}else {
							sdsDataHis = sData;
							logger.debug("/closeDialog/caller/stopDialog ready");
							sData.setRequest(ISDSCodes.REQUEST_STOP_DIALOG);
							sdsClient.setSdsData(sData);
							
							sdsClient.stopDialog();
						}
					}catch(Exception e) {
						logger.warn("sds close exception - continue: {}", e.getMessage());
					}
					
//					long nRet = dlgService.insertSdsPortHis(ses.getDialog().getBotId(), ses.getSessionId(), sData.getProjectPort(), sData.getSdsIp(), true);
					boolean closeByUser = true;
					//SdsProjectPortHis hisData = dlgService.getSdsPortHisLatest(sData.getSdsIp(), sData.getProjectPort());
					SdsProjectPortHis hisData = dlgService.getSdsPortHisLatest(sdsDataHis.getSdsIp(), sdsDataHis.getProjectPort());
					logger.debug("/closeDialog/sdsPortHisData:{}",hisData);
					long nRet = dlgService.updateSdsPortHis(hisData.getHisId(), closeByUser);
					logger.debug("/closeDialog/updateSdsPortHis/Result:{}",nRet);
				}catch(Exception e) {
					logger.warn("/closeDialog/{}",e.getMessage());
					r.setCode(ERR_CODE_SDS_CLOSE_ERROR);
					r.setMsg(ERR_MSG_SDS_CLOSE_ERROR);
				}
				return r;
			}
		};
		return resp;
	}
	
	@RequestMapping(value="/clearCache", method = {RequestMethod.POST})
	public BaseResponse<String> clearCache(@RequestParam(name="cacheName", required=true)String cacheName){
		
		BaseResponse<String> resp = new BaseResponse<>();
		try {
			extCacheService.clearCacheConfig(cacheName);
		}catch(Exception e) {
			resp.setCode(ERR_CODE_FAILURE);
			resp.setMsg(e.getMessage());
		}
		return resp;
	}
}
