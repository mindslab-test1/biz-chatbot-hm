package ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.ChatbotOrderedFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.service.ClassifierService;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.session.DialogService;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.commons_ex.preitf.dao.data.PreItfData;
import ai.mindslab.bizchatbot.commons_ex.preitf.service.ChatbotServiceEx;

public class PreBQAINTFilter extends ChatbotOrderedFilter{

	private Logger logger = LoggerFactory.getLogger(PreBQAINTFilter.class);
			
	private DialogService dialogService;
	private ChatbotServiceEx chatbotServiceEx;
	
	public PreBQAINTFilter() {
	}
	
	public PreBQAINTFilter(int order) {
		super(order);
	}
	
	@Override
	public boolean execBody(DialogSession ses, ChatbotConfig config) throws BizChatbotException {
		chatbotServiceEx = (ChatbotServiceEx)ApplicationContextProvider.getApplicationContext().getBean(ChatbotServiceEx.class);
		
		try {
			List<PreItfData> list = chatbotServiceEx.getPreItfList(config.getBotId());
			if(list == null || list.size() == 0) {
				//skip -> set on process data
//				return false;
				ses.getProcessing().setSkipBqa(false);//set don't skip bqa;
			}else {
				
				ses.getProcessing().setSkipBqa(false); //init setting.
				for(PreItfData itf: list) {
					boolean bMatch = chatbotServiceEx.doMatch(itf.getPattern(), DialogUtils.getLastUserUtter(ses).getSentence());
					if(bMatch) {
						ses.getProcessing().setSkipBqa(true);//set skip;
						break;
					}
				}
			}
		} catch (Exception e) {
			logger.warn("execBody/{}", e.getMessage());
			DialogUtils.setSessionError(ses, "PreBQAINTFilter", e);
			return false;
		}
		
		return true;
	}

}
