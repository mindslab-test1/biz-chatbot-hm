package ai.mindslab.bizchatbot.chatbotcontainer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotApiMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiParam;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiResultMappingInfo;

@Service
public class InterfaceService {

	@Autowired
	private ChatbotApiMapper api;
	
	public InterfaceService() {
	}

	public List<ChatbotApiParam> getParam(int ifId) throws Exception{
		
		List<ChatbotApiParam> params = api.getParams(ifId);
		return params;
	}
	
	public List<ChatbotApiResultMappingInfo> getResultMappingInfo(int ifId) throws Exception{
		
		List<ChatbotApiResultMappingInfo> resultMapping = api.getApiResultMappingInfo(ifId);
		return resultMapping;
	}
	
	public ChatbotApiInfo getApiInfo(int ifId) throws Exception{
		
		ChatbotApiInfo info = api.getApiInfo(ifId);
		return info;
	}
	
}
