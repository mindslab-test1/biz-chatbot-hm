package ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.ChatbotOrderedFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.service.BackendApiService;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotButton;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotDomainSimple;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.chatbot.service.ChatbotService;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.session.DialogService;
import ai.mindslab.bizchatbot.commons.session.data.BasicQaDebugItem;
import ai.mindslab.bizchatbot.commons.session.data.BasicQaResponse;
import ai.mindslab.bizchatbot.commons.session.data.BasicQaResponseWithButtons;
import ai.mindslab.bizchatbot.commons.session.data.BqaProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;

public class BQAFilter extends ChatbotOrderedFilter{

	private Logger logger = LoggerFactory.getLogger(BQAFilter.class);
	
	public BQAFilter() {
		
	}
	
	public BQAFilter(int order) {
		super(order);
	}

	@Override
	public boolean execBody(DialogSession ses, ChatbotConfig config)  throws BizChatbotException{
		
		BackendApiService bApiService = (BackendApiService)ApplicationContextProvider.getApplicationContext().getBean(BackendApiService.class);
		DialogService ds = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		
		//MOBIS
		if(ses.getProcessing().isSkipBqa()) {
			return true;
		}
		
		if(!StringUtils.isEmpty(ses.getProcessing().getSdsDomain())){
			logger.debug("execute/already has sds domain: {} - continue", ses.getProcessing().getSdsDomain());
			return true;
		}
		
		Integer[] domains = null;
		List<ChatbotDomainSimple> domainList = config.getBqaDomains();
		if(domainList != null) {
			domains = new Integer[domainList.size()];
			for(int i=0;i<domainList.size();i++) {
				domains[i] = domainList.get(i).getDomainId();
			}	
		}
		try {
			UtterData lastUserUtter = DialogUtils.getLastUserUtter(ses);
			Object answer = bApiService.doBqaQuery(domains, lastUserUtter.getSentence(), config.getBqaSearchFlowType(), config.getBqaSearchSolrOrMatchRange(), ses.getShowDebug()==null?false:ses.getShowDebug());
			logger.debug("execute/{}/{}/{}", ses.getSessionId(), lastUserUtter.getSentence(), answer);
			if(answer instanceof BasicQaResponse) {
				UtterData botUtter = DialogUtils.createBotUtterData(ses);
				botUtter.setSentence(((BasicQaResponse)answer).getData().getAnswer());
				ses.getDialog().getConversation().add(botUtter);
				ses.getProcessing().setCurrBotUtter(((BasicQaResponse)answer).getData().getAnswer());
				if(ses.getShowDebug()) {
					botUtter.setBqaDebug((BasicQaResponse)answer);
				}
				//fill history data - domain
				botUtter.setHisDomain(((BasicQaResponse)answer).getData().getUsedDomainName());
				botUtter.setCurrStep(getFilterName(this.getClass().getName()));

				//MOBIS : 버스노선 BQA는 SysUtterType 설정
				int usedDomainId = (((BasicQaResponse)answer).getData().getUsedDomainId());
				IChatbotConst.SYS_UTTER_TYPE sysUtterType = IChatbotConst.SYS_UTTER_TYPE.NORMAL;
				// CM_DOMAIN_TB 운영서버 기준 도메인 아이디
				if(usedDomainId == 70) {
					// 버스 노선 BQA
					sysUtterType = IChatbotConst.SYS_UTTER_TYPE.BUSSTOP_LOC;
				} else if (usedDomainId == 74) {
					// 메뉴 안내 BQA
					sysUtterType = IChatbotConst.SYS_UTTER_TYPE.IMOBIS_MENU;
//				} else if (usedDomainId == 134 || usedDomainId == 138) { // DEV
				} else if (usedDomainId == 147 || usedDomainId == 148) {
					// QMS 메뉴 안내 BQA
					sysUtterType = IChatbotConst.SYS_UTTER_TYPE.QMS_MENU;
				}
				botUtter.setSysUtterType(sysUtterType);


				//insert history
				ds.insertBotUtterHistory(botUtter);
				
				//MOBIS
				ses.getProcessing().setBqaProcessing(null);
				//check api resp
				String bqaRespCode = ((BasicQaResponse)answer).getData().getRespCode();
				String bqaContType = "";
				int bqaRcId = 0;
				if(((BasicQaResponse)answer).getData().getRichContent() != null) {
					bqaContType = ((BasicQaResponse)answer).getData().getRichContent().getContentTypeCd();
					bqaRcId = ((BasicQaResponse)answer).getData().getRichContent().getRcId();
				}else if(((BasicQaResponse)answer).getData().getApiInfo() != null){
					bqaContType = IChatbotConst.RICH_CONTENT_TYPE_API;
					bqaRcId = ((BasicQaResponse)answer).getData().getApiInfo().getIfId();
				}
				logger.debug("bqa rich/{},{},{}/",bqaRespCode, bqaContType, bqaRcId);
				if(!StringUtils.isEmpty(bqaRespCode) && IChatbotConst.RICH_CONTENT_TYPE_API.equals(bqaContType)) {
					BqaProcessing bqaProcessing = new BqaProcessing();
					bqaProcessing.setContentTypeCd(bqaContType);
					bqaProcessing.setRcId(bqaRcId);
					bqaProcessing.setRespCode(bqaRespCode);
					//QQQ params?
					
					ses.getProcessing().setBqaProcessing(bqaProcessing);
					ses.getProcessing().setSvcType(sysUtterType);
					return true;//go to next step
					
				}else if(!StringUtils.isEmpty(bqaRespCode) && IChatbotConst.RICH_CONTENT_TYPE_BUTTON.equals(bqaContType)) {
					//MOBIS display richbuttons to bqa answer
//					botUtter.setRichContent(getRichButtons(bqaRcId));
					
					ChatbotRichContent richContent = ((BasicQaResponse)answer).getData().getRichContent();
					botUtter.setRichContent(richContent);
				}else if(!StringUtils.isEmpty(bqaRespCode) && IChatbotConst.RICH_CONTENT_TYPE_IMAGE.equals(bqaContType)) {
					//MOBIS display richimage to bqa answer

					ChatbotRichContent richContent = ((BasicQaResponse)answer).getData().getRichContent();
					botUtter.setRichContent(richContent);
				}
				
				//stop next filter
				return false;
			}else if(answer instanceof BasicQaResponseWithButtons) {
				UtterData botUtter = DialogUtils.createBotUtterData(ses);
				ChatbotRichContent rich = new ChatbotRichContent();
				rich.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_BUTTON);
				//QQQ
				rich.setTitle(config.getBqaReconfirmMsg());
				BasicQaResponseWithButtons ans = (BasicQaResponseWithButtons)answer;
				List<BasicQaDebugItem> list = ans.getData().getList();
				if(list != null) {
					int order = 1;
					for(BasicQaDebugItem item: list) {
						ChatbotButton btn = new ChatbotButton();
						btn.setBtnOrder(order++);
						btn.setTitle(item.getQuestion());
						btn.setUserUtter(item.getQuestion());
						rich.getButtons().add(btn);
					}
				}
				
				botUtter.setRichContent(rich);
				botUtter.setSentence(config.getBqaReconfirmMsg());
				botUtter.setSysUtterType(IChatbotConst.SYS_UTTER_TYPE.BQA_RECOMMEND);

				ses.getDialog().getConversation().add(botUtter);
				ses.getProcessing().setCurrBotUtter(config.getBqaReconfirmMsg());
				if(ses.getShowDebug()) {
					botUtter.setBqaDebugEx((BasicQaResponseWithButtons)answer);
				}
				botUtter.setCurrStep(getFilterName(this.getClass().getName()));
				//insert history
				ds.insertBotUtterHistory(botUtter);
				//stop next filter
				return false;
			}
//			else {
//				
//			}
//			if(!StringUtils.isEmpty(answer)) {
//				UtterData botUtter = DialogUtils.createBotUtterData(ses);
//				botUtter.setSentence(answer);
//				ses.getDialog().getConversation().add(botUtter);
//				
//				ses.getProcessing().setCurrBotUtter(answer);
//				//stop next filter
//				return false;
//			}
		}catch(Exception e) {
			logger.warn("execute/BQAFilter skip with exception/{}/{}",ses.getSessionId(),e.getMessage());
			DialogUtils.setSessionError(ses, "BQAFilter", e);
			return false;
		}
		
		return true;
	}
	
//	private ChatbotRichContent getRichButtons(int rcId) throws Exception{
//		ChatbotService chatbotService = (ChatbotService)ApplicationContextProvider.getApplicationContext().getBean(ChatbotService.class);
//		ChatbotRichContent richContent = null;
//		try {
//			richContent = chatbotService.getRichContent(rcId);
//			
//		} catch (Exception e) {
//			logger.warn("getRichButtons/{}",e.getMessage());
//			throw e;
//		}
//		
//		return richContent;
//	}
	
}
