package ai.mindslab.bizchatbot.chatbotcontainer.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.session.data.BasicQaResponse;
import ai.mindslab.bizchatbot.commons.session.data.BasicQaResponseWithButtons;
import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;
import ai.mindslab.bizchatbot.commons.utils.JsonUtils;

@Service
public class BackendApiService {

	private Logger logger = LoggerFactory.getLogger(BackendApiService.class);
	
	@Value("${backend.api.url}")
	private String _url;
	
	public BackendApiService() {
	}
	
	public BaseResponse<?> doBqaQuery(Integer[] domains, String query, int flowType, int matchRange, boolean debug) {
		logger.info("doBqaQeury/{},{}",domains, query);
		String url = _url;
		
		try {
			if(domains == null || StringUtils.isEmpty(query)) {
				throw new Exception("Invalid parameters for BQA");
			}
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("question", query.trim()));
			params.add(new BasicNameValuePair("domainIds", StringUtils.join(domains, ",")));
			params.add(new BasicNameValuePair("searchFlowType", flowType+""));
			params.add(new BasicNameValuePair("range", matchRange+""));
			params.add(new BasicNameValuePair("debugYn", debug?"Y":"N"));
			
			Header[] headers = HttpConnectionManager.getHeader();
			String resp = HttpConnectionManager.getInstance().post(url+"/bqa/search", params, headers);
			BasicQaResponse out = JsonUtils.fromJson(resp, BasicQaResponse.class);
			logger.debug("doBqaQuery/{}",resp);
			if(out.getCode() == IRestCodes.ERR_CODE_SUCCESS) {
				//show answer
				logger.debug("doBqaQuery/{}/{}",query, out.getData() !=null? out.getData().getAnswer(): "");
				return out;
			}else if(out.getCode() == IRestCodes.ERR_CODE_SEARCH_SOLROR_SUCCESS) {
				//check list
				//re-parsing
				BasicQaResponseWithButtons outEx = JsonUtils.fromJson(resp, BasicQaResponseWithButtons.class);
				logger.debug("doBqaQuery/{}", outEx);
				return outEx; 
			}
//			return out.getData().getAnswer();
		}catch(Exception e) {
			logger.warn("doBqaQuery/{}/{}",query,e.getMessage());
		}
		return null;
	}

	public BaseResponse<?> doNlpAnalyze(String data) {
		logger.info("doNlpTest/{},{}", data);
		String url = _url;

		try {
			if(data == null || StringUtils.isEmpty(data)) {
				throw new Exception("Invalid parameters for NLP Test");
			}
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("data", data.trim()));

			Header[] headers = HttpConnectionManager.getHeader();
			String resp = HttpConnectionManager.getInstance().post(url+"/bqa/nlpAnalyze", params, headers);
			BaseResponse<?> out = JsonUtils.fromJson(resp, BaseResponse.class);
			logger.debug("doNlpTest/{}",resp);
			if(out.getCode() == IRestCodes.ERR_CODE_SUCCESS) {
				//show answer
				logger.debug("doNlpTest/{}/{}", data, out.getData() !=null? out.getData(): "");
				return out;
			}
		}catch(Exception e) {
			logger.warn("doNlpTest/{}/{}", data, e.getMessage());
		}
		return null;
	}

}
