package ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter;


import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.ChatbotOrderedFilter;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.DialogSynonymData;
import ai.mindslab.bizchatbot.commons.chatbot.service.ChatbotService;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;

public class InitialSynonymFilter extends ChatbotOrderedFilter{

	private Logger logger = LoggerFactory.getLogger(InitialSynonymFilter.class);
	
	public InitialSynonymFilter() {
	}
	
	public InitialSynonymFilter(int order) {
		super(order);
	}

	@Override
	public boolean execBody(DialogSession ses, ChatbotConfig config)  throws BizChatbotException{
		
		//Store default domain
//		Environment env = (Environment)ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
		ChatbotService cs = (ChatbotService)ApplicationContextProvider.getApplicationContext().getBean(ChatbotService.class);
		
		List<DialogSynonymData> list = null;
		try {
			list = cs.getSynonymList(ses.getDialog().getBotId());
		}catch(Exception e) {
			logger.warn("execute/continue... /{}",e.getMessage());
			//continue next filter
			DialogUtils.setSessionError(ses, "InitialSynonymFilter", e);
			return true;
		}
		
		String lastUserUtter = "";
		if(list != null) {
			try {
				UtterData lastUtter = DialogUtils.getLastUserUtter(ses);
				lastUserUtter = lastUtter.getSentence();
				
				for(DialogSynonymData syn: list) {
					//do replace here...
					lastUserUtter = replaceUserUtter(lastUserUtter, syn);
				}
				
				//change last user utter.
				lastUtter.setSentence(lastUserUtter);
			}catch(Exception e) {
				logger.warn("execute/continue... /{}", e.getMessage());
				DialogUtils.setSessionError(ses, "InitialSynonymFilter", e);
			}
		}
		
		return true;
	}
	
	private String replaceUserUtter(String utter, DialogSynonymData synonymData) throws Exception{
		if(utter == null || synonymData == null) {
			throw new Exception("InitialSynonymFilter/replaceUserUtter/Invalid arguments");
		}
		
		String userUtter = utter.trim();
		if(StringUtils.isEmpty(userUtter)) {
			return userUtter;
		}else {
			String[] synList = synonymData.getSynonymList().split(",");
			if(synList != null) {
				for(String syn: synList) {
					userUtter = userUtter.replace(syn.trim(), synonymData.getWord());
				}
			}
		}
		logger.debug("replaceUserUtter/{} ==> {}", utter, userUtter);
		return userUtter;
	}
}
