package ai.mindslab.bizchatbot.chatbotcontainer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;

import com.hazelcast.core.IMap;

@Service
public class SesCleanupService implements IChatbotConst{
	
	private Logger logger = LoggerFactory.getLogger(SesCleanupService.class);
	
	@Autowired
	private HazelClientCommon hazelClient;

	public SesCleanupService() {
	}

	public void cleanup(int timeoutMili) {
		logger.info("cleanup/{}",timeoutMili);
		
		//get targets
		IMap<String, DialogSession> dialogs = hazelClient.getMap(MAP_NAME_DIALOG);
		
		
	}
}
