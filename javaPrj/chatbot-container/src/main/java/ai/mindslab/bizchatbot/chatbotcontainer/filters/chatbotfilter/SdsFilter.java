package ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import ai.mindslab.bizchatbot.chatbotcontainer.service.BackendApiService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.ChatbotOrderedFilter;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.SdsProjectPortHis;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.SDS_STATUS;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.codes.ISDSCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.session.DialogService;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.SdsData;
import ai.mindslab.bizchatbot.commons.session.data.SdsUtterRespData;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;
import ai.mindslab.bizchatbot.commons.utils.JsonUtils;
import ai.mindslab.bizchatbot.sds.client.SdsClient;
import ai.mindslab.bizchatbot.sds.client.SdsClientFactory;

public class SdsFilter extends ChatbotOrderedFilter{
	
	private Logger logger = LoggerFactory.getLogger(SdsFilter.class);
	
	private DialogService ds;
	private boolean isDialogEnded = false;
	
	private static ObjectMapper mapper = null;
	
	static{
		mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
		mapper.setTimeZone(TimeZone.getTimeZone("GMT+09:00"));
	}
	
	public SdsFilter() {
	}

	public SdsFilter(int order) {
		super(order);
	}
	
	@Override
	public boolean execBody(DialogSession ses, ChatbotConfig config) throws BizChatbotException {

//		ClassifierService sc = (ClassifierService)ApplicationContextProvider.getApplicationContext().getBean(ClassifierService.class);
//		Environment env = (Environment)ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
		ds = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		
		String sdsDomain = "";
		UtterData lastUserUtter = null;
		try {
			lastUserUtter = DialogUtils.getLastUserUtter(ses);
		}catch(Exception e) {
			logger.warn("execute/{}", e.getMessage());
			DialogUtils.setSessionError(ses, "SdsFilter", "User Utter is not exist");
 			return false;
		}
		
		
		sdsDomain = ses.getProcessing().getSdsDomain();
		
		//QQQ check if user's utter is expected slot type
		String expSlot = ses.getProcessing().getExpectedSlot();
		
//		SdsData sdsData = ses.getSdsData();
		UtterData utter = null;
		try {
			//sds filter에 들어올 필요가 없는 경우 확인.
			if(StringUtils.isEmpty(sdsDomain)) {
//				String unknownMsg = DialogUtils.getRandomMsg(config.getUnknownMsg());
////				String unknownMsg = ds.getChatbotMsgs(ses.getDialog().getBotId(), IChatbotConst.MSG_TYPE_UNKNOWN);
				utter = DialogUtils.createBotUtterData(ses);
//				utter.setSentence(unknownMsg);
//				utter.setMsgTypeCd(IChatbotConst.MSG_TYPE_UNKNOWN);
//				utter.setRichContent(config.getUnknownRichContent());
				DialogUtils.setUnknownMsg(utter, config);
				this.setNlpUtter(utter, lastUserUtter);

				logger.debug("execute/system unknown message due to no sds domain/{}",utter.getSentence());
				ds.insertBotUtterHistory(utter.getBotId(), ses.getUserData().getUserId(), ses.getSessionId(), utter.getSentence(), utter.getDialogSeq(),"", "", IChatbotConst.MSG_TYPE_UNKNOWN, "");
				ses.getProcessing().setCurrBotUtter(utter.getSentence());
				ses.getDialog().getConversation().add(utter);
				return false;
			}
			
			if(!DialogUtils.isOpenedSdsSession(ses)) {
				//initially create sds session
				utter = openSdsSession(ses, sdsDomain);
				//sds greeting -> skip
				//ds.insertUserUtterHistory(ses.getUserData().getUserId(), ses.getSessionId(), utter.getSentence());
				logger.debug("execute open Sds session/{}", utter);
				
				if(utter != null) {
					//user uttr전달
					ses.getProcessing().setCurrBotUtter(utter.getSentence());
					utter = sendSdsTalk(ses, sdsDomain);
				}else {
					logger.warn("sds session open error/{}", ses.getSessionId());
					//QQQ
					throw new BizChatbotException(IRestCodes.ERR_CODE_SDS_OPEN_ERROR, IRestCodes.ERR_MSG_SDS_OPEN_ERROR);
				}
			}else if(!isDialogEnded) {
				utter = sendSdsTalk(ses, sdsDomain);
				logger.debug("execute send Sds talk/{}", utter);
			}
			
			if(utter == null) {
//				String unknownMsg = DialogUtils.getRandomMsg(config.getUnknownMsg());
////				String unknownMsg = ds.getChatbotMsgs(ses.getDialog().getBotId(), IChatbotConst.MSG_TYPE_UNKNOWN);
				utter = DialogUtils.createBotUtterData(ses);
//				utter.setSentence(unknownMsg);
//				utter.setMsgTypeCd(IChatbotConst.MSG_TYPE_UNKNOWN);
//				utter.setRichContent(config.getUnknownRichContent());
				
				DialogUtils.setUnknownMsg(utter, config);
				this.setNlpUtter(utter, lastUserUtter);

				logger.debug("execute/system unknown message/{}",utter.getSentence());
			}
			
			if(DialogUtils.isUnknownSystemUtter(utter)) {
				logger.debug("execute/system unknown message/org: {}",utter.getSentence());
//				String unknownMsg = DialogUtils.getRandomMsg(config.getUnknownMsg());
////				String unknownMsg = ds.getChatbotMsgs(ses.getDialog().getBotId(), IChatbotConst.MSG_TYPE_UNKNOWN);
//				utter.setSentence(unknownMsg);
//				utter.setMsgTypeCd(IChatbotConst.MSG_TYPE_UNKNOWN);
//				utter.setRichContent(config.getUnknownRichContent());
				DialogUtils.setUnknownMsg(utter, config);
				this.setNlpUtter(utter, lastUserUtter);

			}
			
			ds.insertBotUtterHistory(utter.getBotId(), ses.getUserData().getUserId(), ses.getSessionId(), utter.getSentence(), utter.getDialogSeq(),"", ses.getProcessing().getSdsDomain(), "",getFilterName(this.getClass().getName()));
			ses.getProcessing().setCurrBotUtter(utter.getSentence());
			ses.getDialog().getConversation().add(utter);
			
			if(DialogUtils.needIFResultMapping(ses)) {
				//continue & do 3rd parth interface mapping
				return true;
			}else {
				if(isDialogEnded) {
					//QQQ update last status?? -> 확인할것 
					//reset curr stat.
					DialogUtils.resetProcessingData(ses);
				}
			}
		}catch(Exception e) {
			logger.warn("execute/{}/{}",ses.getSessionId(), e.getMessage());
			
			DialogUtils.setSessionError(ses, "SdsFilter", e);
		}
		//stop next filter execution
		return false;
	}
	
	
	//public synchronized UtterData openSdsSession(DialogSession ses, String sdsDomain) {
	public UtterData openSdsSession(DialogSession ses, String sdsDomain) {
		
		UtterData utter = null;
		HazelClientCommon hazel = (HazelClientCommon)ApplicationContextProvider.getApplicationContext().getBean(HazelClientCommon.class);
		Environment env = (Environment)ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
		DialogService ds = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		
		int nextPrjPort  = 0;
		DialogUtils.lock.lock();
		try {
			nextPrjPort = DialogUtils.getNextProjectPort(hazel.getClient(), env);
		logger.debug("nextPrjPort from cache: {}", nextPrjPort);
		//get next port from db
		int maxPort = 59999;
		int minPort = 50100;
		try{
			maxPort = Integer.parseInt(env.getProperty("sds.project.port.max"));
			minPort = Integer.parseInt(env.getProperty("sds.project.port.start"));
		}catch(Exception e) {
		}
//		SdsProjectPortHis portHis = ds.getLastSdsPort(maxPort);//max port < 60000
//		if(portHis != null) {
//			nextPrjPort = portHis.getProjectPort() + 1;
			if(nextPrjPort > maxPort - 20) {
				nextPrjPort = minPort;
			}
//			logger.debug("nextPrjPort from db: {}", nextPrjPort);		
			nextPrjPort = getNextPortFromServer(env, nextPrjPort, nextPrjPort+20);
			logger.debug("nextPrjPort from server: {}", nextPrjPort);
			DialogUtils.setNextProjectPort(hazel.getClient(), nextPrjPort);
//		}
		}finally {
			DialogUtils.lock.unlock();
		}
		
		SdsData sData = new SdsData();
		sData.setProjectName(sdsDomain);
		sData.setProjectPort(nextPrjPort);
		sData.setDaemonLogFileName(ses.getUserData().getUserId()+"."+ses.getSessionId());
		sData.setRequest(ISDSCodes.REQUEST_INIT_DIALOG);
		sData.setSdsIp(env.getProperty("sds.ip"));//QQQ clustering case, choose one ip
		sData.setUserName(ses.getUserData().getUserId());//QQQ with user id?->user name?
		ses.setSdsData(sData);
		
//		SdsClient sdsClient = (SdsClient)ApplicationContextProvider.getApplicationContext().getBean(SdsClient.class);
		SdsClientFactory sdsFactory = (SdsClientFactory)ApplicationContextProvider.getApplicationContext().getBean(SdsClientFactory.class);
		SdsClient sdsClient = sdsFactory.makeSdsClient();
		
		try {
			sdsClient.setSdsData(sData);
			logger.debug("befor initDialog/{}",sData);
			SdsUtterRespData sdsResp = sdsClient.initDialog();
			logger.debug("openSdsSession/sdsresp: {}",sdsResp);
			if(sData.getStatus() == SDS_STATUS.OPEN) {
				ds.insertSdsPortHis(ses.getDialog().getBotId(), ses.getSessionId(), sData.getProjectPort(), sData.getSdsIp());
				
				utter = DialogUtils.createBotUtterData(ses);
				utter.setSentence(DialogUtils.extractSysUtter(sdsResp));
				return utter;
			}else {
				return null;
			}
			
		}catch(Exception e) {
			logger.warn("openSdsSession/{}",e.getMessage());
			
			return null;
		}
	}
	
	public UtterData sendSdsTalk(DialogSession ses, String sdsDomain) {
		
		UtterData utter = null;
//		HazelClientCommon hazel = (HazelClientCommon)ApplicationContextProvider.getApplicationContext().getBean(HazelClientCommon.class);
		Environment env = (Environment)ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
		
		SdsData sData = ses.getSdsData();
		sData.setRequest(ISDSCodes.REQUEST_USER_UTTER);
		ses.setSdsData(sData);
		
//		SdsClient sdsClient = (SdsClient)ApplicationContextProvider.getApplicationContext().getBean(SdsClient.class);
		SdsClientFactory sdsFactory = (SdsClientFactory)ApplicationContextProvider.getApplicationContext().getBean(SdsClientFactory.class);
		SdsClient sdsClient = sdsFactory.makeSdsClient();
		UtterData userUtter = null;
		try {
			userUtter = DialogUtils.getLastUserUtter(ses);
		
			sdsClient.setSdsData(sData);
			SdsUtterRespData sdsResp = sdsClient.userUtter(userUtter.getSentence());
			logger.debug("sendSdsTalk/sdsresp: {}",sdsResp);
			
			utter = DialogUtils.createBotUtterData(ses);
			utter.setSentence(DialogUtils.extractSysUtter(sdsResp));
			try {
				utter.setSluWeight(Float.parseFloat(sdsResp.getSlu_weight()));
			}catch(Exception e) {
			}
			utter.setDa(sdsResp.getDa());
			String nextSlot = DialogUtils.extractDaSlot(sdsResp.getDa());
			ses.getProcessing().setExpectedSlot(nextSlot);
			//multi
			ArrayList<DialogProcessing.Slot> slots = DialogUtils.extratSluSlots(sdsResp.getSlu());
			if(slots != null && slots.size() > 0) {
				DialogUtils.addSlots(ses.getProcessing(), slots);
			}
			
			//set sds debug info.
			if(ses.getShowDebug() != null && ses.getShowDebug()) {
				utter.setDebug(sdsResp);
			}
			
			//QQQ check if sds conv. ended
			if("1".equals(sdsResp.getIsDialogEnd())) {
				isDialogEnded = true;
				
				try {
					sData.setRequest(ISDSCodes.REQUEST_STOP_DIALOG);
					sdsClient.stopDialog();
					String stopResp = sdsClient.readData();
					logger.debug("stopResp/{}",stopResp);
				}catch(IOException e) {
					logger.warn("stopDialog error - continue:{}", e.getMessage());
				}finally {
					removePort(env, sData.getProjectPort());
				}
			}
			
			//insert history
//			ds.insertBotUtterHistory(ses.getUserData().getUserId(), ses.getSessionId(), utter.getSentence());
			
			return utter;
		}catch(Exception e) {
			logger.warn("sendSdsTalk/{}",e.getMessage());
			return null;
		}finally {
			if(utter != null) {
				utter.setSdsDomain(ses.getProcessing().getSdsDomain());
			}
		}
	}
	
	private int getNextPortFromServer(Environment env, int port, int maxport) {
		String url = env.getProperty("sds.project.port.finder.url");
		String portResp = "";
		int newPort = port;
		int newMaxPort = maxport;
		for(int idx = 0;idx<5;idx++) {
			try {
				if(idx > 0) {
					//temp
//					newPort += 1000;
//					newMaxPort += 1000;
					newPort += 20;
					newMaxPort += 20;
				}
				
				portResp = HttpConnectionManager.getInstance().get(url+"?port="+newPort+"&maxport="+newMaxPort, null);
				BaseResponse<Integer> out = null;
				try {
					out = mapper.readValue(portResp, new TypeReference<BaseResponse<Integer>>() {});
					newPort = out.getData();
					break;
				} catch (JsonProcessingException e) {
					logger.warn("getNextPortFromServer/{}", e.getMessage());
					throw e;
				} catch (IOException e) {
					logger.warn("getNextPortFromServer/{}", e.getMessage());
					throw e;
				}
				
			}catch(Exception e) {
				logger.warn("getNextPortFromServer err. use original port {}/{}", newPort,e.getMessage());
			}
		}
		return newPort;
	}
	
	private void removePort(Environment env, int port) {
		String url = env.getProperty("sds.project.port.remover.url");
		String portResp = "";
		int newPort = port;
		try {
			portResp = HttpConnectionManager.getInstance().get(url+"?port="+newPort, null);
			BaseResponse<Integer> out = null;
			try {
				out = mapper.readValue(portResp, new TypeReference<BaseResponse<String>>() {});
			} catch (JsonProcessingException e) {
				logger.warn("removePort/{}", e.getMessage());
				throw e;
			} catch (IOException e) {
				logger.warn("removePort/{}", e.getMessage());
				throw e;
			}
			
		}catch(Exception e) {
			logger.warn("removePort err. original port {}", port);
		}
	}

	// unknown시에 lastUserUtter 형태소 분석해서 명사만 추출하기 - MRC & 검색을 위한 기능
	private void setNlpUtter(UtterData utter, UtterData lastUserUtter) {
		BackendApiService bApiService = (BackendApiService)ApplicationContextProvider.getApplicationContext().getBean(BackendApiService.class);
		BaseResponse<?> resp = bApiService.doNlpAnalyze(lastUserUtter.getSentence());
		if(resp.getCode() == IRestCodes.ERR_CODE_SUCCESS) {
			utter.setNlpUtter((String) resp.getData());
		}
	}
}
