package ai.mindslab.bizchatbot.chatbotcontainer.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@MapperScan(basePackages = { "ai.mindslab.sds.dao" }
, sqlSessionFactoryRef=AppConfig.TRANSACTION_SESSION_FACTORY_SDS
)
public class EtriDialogDatasourceConfig {

	@Autowired
	private MybatisConfigurationSupport mybatisConfigurationSupport;
	
	@Bean(name = AppConfig.DATASOURCE_SDS)
	@ConfigurationProperties(prefix = "sds.datasource")
	public DataSource sdsDataSource() {
		return DataSourceBuilder.create().build();
	}
	
	@Bean(name=AppConfig.TRANSACTION_MANAGER_SDS)
	public PlatformTransactionManager chatbotTransactionManager() {
		return new DataSourceTransactionManager(sdsDataSource());
	}
	
	@Bean(name = AppConfig.TRANSACTION_SESSION_FACTORY_SDS)
    public SqlSessionFactory etlSqlSessionFactory(@Qualifier(AppConfig.DATASOURCE_SDS) DataSource sdsDataSource) throws Exception {
        return mybatisConfigurationSupport.build(sdsDataSource);
    }
}
