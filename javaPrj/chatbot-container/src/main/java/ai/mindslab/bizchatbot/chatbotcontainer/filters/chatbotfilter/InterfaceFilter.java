package ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.ChatbotOrderedFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.service.SdsApiService;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiJsonTemplate;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiParam;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiResultMappingInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotButton;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRespInterfaceRelData;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.chatbot.service.ChatbotService;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.outif.ThirdPartyApiManager;
import ai.mindslab.bizchatbot.commons.outif.ThirdPartyApiManager.ApiResult;
import ai.mindslab.bizchatbot.commons.session.DialogService;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.session.data.UtterReconfirmData;
import ai.mindslab.bizchatbot.commons.utils.ApiUtils;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;
import ai.mindslab.sds.dao.data.SdsSlotInfo;

public class InterfaceFilter extends ChatbotOrderedFilter{

	private Logger logger = LoggerFactory.getLogger(InterfaceFilter.class);
	
	private ThirdPartyApiManager apiManager;
	private DialogService dialogService;
	
	public InterfaceFilter() {
	}
	
	public InterfaceFilter(int order) {
		super(order);
	}

	@Override
	public boolean execBody(DialogSession ses, ChatbotConfig config) throws BizChatbotException {
		
		apiManager =  (ThirdPartyApiManager)ApplicationContextProvider.getApplicationContext().getBean(ThirdPartyApiManager.class);
		dialogService = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		SdsApiService sdsApiService = (SdsApiService)ApplicationContextProvider.getApplicationContext().getBean(SdsApiService.class);
		
		ChatbotRespInterfaceRelData respRel = null;
		ChatbotApiInfo apiInfo = null;
		String resp = "";
		try {
			//replace system in/output with api result.
			
			boolean needRequestMapping = false;
			UtterData utter = DialogUtils.getLastSystemUtter(ses);
			String sysResp = utter.getSentence();
			respRel = apiManager.getChatbotRespInterfaceRel(ses.getDialog().getBotId(), sysResp);
			if(respRel != null) {
				//remove syscode
				sysResp = DialogUtils.removeSystemCode(sysResp);
//				sysResp = respRel.getRespMsg()+DialogUtils.removeSystemCode(sysResp);//it was changed
				
				if(IChatbotConst.RICH_CONTENT_TYPE_API.equals(respRel.getRespTypeCd())) {
					try {
						apiInfo = apiManager.getApiInfo(respRel.getIfId());
						ApiUtils.setGlobalSlots(apiInfo != null? apiInfo.getParamList():null, ses.getUserData().getUserId(), ses.getRemoteAddr(), ses.getDialog().getBotId());

						//QQQ check if user's utter is expected slot type
//						String expSlot = ses.getProcessing().getExpectedSlot();
//						List<SdsSlotInfo> slotInfoList = null;
//						if(expSlot.matches("\\w+\\.\\w+")) {///Aaa.bbb style check
//							slotInfoList = sdsApiService.getSdsSlotInfo(ses.getProcessing().getSdsDomain());
//							//QQQ how to check if it's suitable spoken word
//							UtterData userUtter = DialogUtils.getLastUserUtter(ses);
//							if(userUtter == null) {
//								throw new BizChatbotException("there's no user utter in this session.");
//							}
//							//QQQ 우선 한개의 parameter만 지
//							String userUtterParam = userUtter.getSentence();
//							needRequestMapping = true;
//							if(apiInfo != null) {
//								List<ChatbotApiParam> params = apiInfo.getParamList();
//								for(ChatbotApiParam p: params) {
//									if(DialogUtils.isApiNeedUserInputFilling(p.getParamValue())){
//										String fillingType = DialogUtils.extractInputFillingType(p.getParamValue());
//										//expSlot과 추출된 param type이 같아야만 한다.
//										if(!StringUtils.isEmpty(fillingType) && fillingType.equals(expSlot)) {
//											logger.debug("execute/input slot filling : {} -> {}", p.getParamValue(), userUtterParam);
//											//p.setParamValue(DialogUtils.extractInputFillingType(p.getParamValue()));
//											p.setParamValue(userUtterParam);
//										}else {
//											logger.warn("execute/different slot type. expected by sds: {}, parame: {}", expSlot, fillingType);
//											throw new Exception("API Slot Filling Type Error." + expSlot +"/"+fillingType);
//										}
//									}
//								}
//							}
//						}

						//multi
						ArrayList<DialogProcessing.Slot> slots = ses.getProcessing().getExpectedSlots();
						if(slots != null && slots.size() > 0) {
//							UtterData userUtter = DialogUtils.getLastUserUtter(ses);
							//no need to check user utter
							//just refer to slot values
							if(apiInfo != null) {
								List<ChatbotApiParam> params = apiInfo.getParamList();
								Iterator<ChatbotApiParam> i = params.iterator();
								while(i.hasNext()) {
									ChatbotApiParam p = i.next();
									if(DialogUtils.isApiNeedUserInputFilling(p.getParamValue())){
										String fillingType = DialogUtils.extractInputFillingType(p.getParamValue());
										DialogProcessing.Slot slot = DialogUtils.lookupSlot(slots, fillingType);
										//expSlot과 추출된 param type이 같아야만 한다.
										if(slot != null && !StringUtils.isEmpty(slot.getSlotName()) && fillingType.equals(slot.getSlotName())) {
											logger.debug("execute/input slot filling : {} -> {}", p.getParamValue(), slot.getSlotValue());
											p.setParamValue(slot.getSlotValue());
										}else {
											logger.warn("execute/different slot type. expected by sds: {}, param: {}", slot, fillingType);
//											throw new Exception("API Slot Filling Type Error." + slot +"/"+fillingType);

											// replace 되지 않은 slot 값은 parameter에서 제거
											i.remove();
										}
									}
								}
							}
						}else{
							/*필요없는 코드가 될듯한데.. 혹시 걸리는 케이스가 존재할까해서...주석해놓지 않음. */
							String expSlot = ses.getProcessing().getExpectedSlot();
							List<SdsSlotInfo> slotInfoList = null;
							if(expSlot.matches("\\w+\\.\\w+")) {///Aaa.bbb style check
								slotInfoList = sdsApiService.getSdsSlotInfo(ses.getProcessing().getSdsDomain());
								//QQQ how to check if it's suitable spoken word
								UtterData userUtter = DialogUtils.getLastUserUtter(ses);
								if(userUtter == null) {
									throw new BizChatbotException("there's no user utter in this session.");
								}
								//QQQ 우선 한개의 parameter만 지
								String userUtterParam = userUtter.getSentence();
								needRequestMapping = true;
								if(apiInfo != null) {
									List<ChatbotApiParam> params = apiInfo.getParamList();
									for(ChatbotApiParam p: params) {
										if(DialogUtils.isApiNeedUserInputFilling(p.getParamValue())){
											String fillingType = DialogUtils.extractInputFillingType(p.getParamValue());
											//expSlot과 추출된 param type이 같아야만 한다.
											if(!StringUtils.isEmpty(fillingType) && fillingType.equals(expSlot)) {
												logger.debug("execute/input slot filling : {} -> {}", p.getParamValue(), userUtterParam);
												//p.setParamValue(DialogUtils.extractInputFillingType(p.getParamValue()));
												p.setParamValue(userUtterParam);
											}else {
												logger.warn("execute/different slot type. expected by sds: {}, parame: {}", expSlot, fillingType);
												throw new Exception("API Slot Filling Type Error." + expSlot +"/"+fillingType);
											}
										}
									}
								}
							}
						}
						
						if(apiInfo!=null) {
							if(IChatbotConst.API_HTTP_METHOD_GET.equals(apiInfo.getMethod())) {
								resp = sendGet(apiInfo);
							}else if(IChatbotConst.API_HTTP_METHOD_POST.equals(apiInfo.getMethod())) {
								resp = sendPost(apiInfo);
							}else {
								//error?
								throw new Exception("외부 API 설정 오류. method:"+ apiInfo.getMethod());
							}
							List<ChatbotApiResultMappingInfo> resMappingList = apiManager.getApiResultMappingInfo(apiInfo.getIfId());
							if(!StringUtils.isEmpty(resp) && resMappingList != null) {
								
								try {
									String tmpResp = sysResp;
									ApiResult apiOut = null;
									ChatbotRichContent richContent = null;
									for(ChatbotApiResultMappingInfo mapping:resMappingList) {
//										String result = apiManager.extractValue(resp, mapping.getJsonPath(), String.class);
//										logger.debug("execute/{} To.",resp);
//										tmpResp = tmpResp.replaceAll(mapping.getDispName(), result);
//										ogger.debug("{}", tmpResp);
//										if(apiOut == null) {
										try {
											apiOut = apiManager.parseResult(resp, mapping.getJsonPath());
											if(apiOut != null && apiOut.getResult() instanceof List) {
												if(mapping.getDispName().equals("_result_")) {
													richContent = new ChatbotRichContent();
													richContent.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_BUTTON);
													List<ChatbotButton> buttons = apiManager.mappingResultStringToButtons(tmpResp, mapping.getDispName(), apiOut.getResult());
													this.seperateButtonText(buttons);
													richContent.setButtons(buttons);
													utter.setRichContent(richContent);
												}
											}
//										}
										}catch(Exception e) {
											logger.warn("execute/jsonpath err. {}", e.getMessage());
											tmpResp = apiManager.mappingResultString(tmpResp, mapping.getDispName(), "");
											continue;
										}
										tmpResp = apiManager.mappingResultString(tmpResp, mapping.getDispName(), apiOut.getResult());
										logger.debug("execute/{} To. {}", resp, tmpResp);
									}
									
									//update last sentence
									utter.setSentence(tmpResp);

									try {
										// API에 따른 sysUtterType update
										apiOut = apiManager.parseResult(resp, "$.data.sysUtterTypeCode");
										if (apiOut != null && apiOut.getResult() instanceof String) {
											String sysUtterTypeCode = (String) apiOut.getResult();
											utter.setSysUtterType(IChatbotConst.SYS_UTTER_TYPE.getEnum(sysUtterTypeCode));
										}
									} catch(Exception e) {
										logger.warn("execute/jsonpath err. {}", e.getMessage());
									}
									
									//update history
									long nRet = dialogService.updateUtterHistory(utter);
									logger.debug("dialog history updated: {}, count: {}", utter, nRet);
								}catch(Exception ee) {
									logger.warn("SDS Response update error/{}", ee.getMessage());
									throw new Exception("외부 API 결과 맵핑 오류입니다.");
								}
							}
						}else {
							//error?
							//QQQ return org utter?
							throw new Exception("외부 API정보를 찾을 수 없습니다.");
						}
					}catch(Exception e) {
						//api error
						//QQQ make error system utter.?
						//
						throw e;
					}
				}else {
					//do next filter(rich content)
					return true;
				}
			}else {
				//error
				//QQQ
				throw new Exception("연정보를 찾을 수 없습니다. RespCode Mapping error.");
			}
		}catch(Exception e) {
			logger.warn("execute/{}",e.getMessage());
			DialogUtils.setSessionError(ses, "InterfaceFilter", e);
			try {
				UtterData lastSysUtter = DialogUtils.getLastSystemUtter(ses);
				//lastSysUtter.setSentence(e.getMessage());
				lastSysUtter.setSentence("정보조회 오류입니다.");
			} catch (Exception e1) {
				logger.warn("execute/{}", e1.getMessage());
			}
		}finally {
			try {
				UtterData lastSysUtter = DialogUtils.getLastSystemUtter(ses);
				String isEnded = lastSysUtter.getDebug().getIsDialogEnd();
				if(!StringUtils.isEmpty(isEnded) && "1".equals(isEnded)) {
					DialogUtils.resetProcessingData(ses);
					//QQQ 화재전환?
					String msg = dialogService.getChatbotMsgs(ses.getDialog().getBotId(), IChatbotConst.MSG_TYPE_SDS_RECONFIRM);
					if(!StringUtils.isEmpty(msg)) {
						UtterReconfirmData reconf = new UtterReconfirmData();
						reconf.setMsg(msg);
						//lastSysUtter.setSentence(lastSysUtter.getSentence() + "\n" + DialogUtils.getReconfirmMsg(msg));
						lastSysUtter.setReconfirmMsg(reconf);
						ChatbotRichContent richGreeting =  config.getGreetingRichContent();
						if(richGreeting != null) {
							reconf.setRichContent(richGreeting);
						}
					}
				}
			} catch (Exception e) {
			}
		}
		return false;
	}
	
	
	public static String sendGet(ChatbotApiInfo apiInfo) throws Exception {
		String paramString = HttpConnectionManager.makeUrlParamString(apiInfo.getParamList());
		Header[] headers = HttpConnectionManager.getHeaderParams(apiInfo.getParamList(), apiInfo.getContentType());
		
		String resp = HttpConnectionManager.getInstance().get(apiInfo.getEndpoint()+"?"+paramString, headers);
		return resp;
	}

	public static String sendPost(ChatbotApiInfo apiInfo) throws Exception {
		String resp = "";
		Header[] headers = HttpConnectionManager.getHeaderParams(apiInfo.getParamList(), apiInfo.getContentType());
    		if(apiInfo.getContentType().startsWith(ContentType.APPLICATION_FORM_URLENCODED.getMimeType())) {
			ArrayList<BasicNameValuePair> reqParam = new ArrayList<>();
			List<ChatbotApiParam> params = apiInfo.getParamList();
			if(params != null) {
				for(ChatbotApiParam param: params) {
					if(IChatbotConst.API_HTTP_PARAM_TYPE_HEADER.equals(param.getParamType())) {
						continue;
					}
					reqParam.add(new BasicNameValuePair(param.getParamName(), param.getParamValue()));
				}
			}
			resp = HttpConnectionManager.getInstance().post(apiInfo.getEndpoint(), reqParam, headers);
		}else if(apiInfo.getContentType().equals(ContentType.APPLICATION_JSON.getMimeType())) {
			ChatbotApiJsonTemplate jsonTemplate = apiInfo.getJsonTemplate();
			String jsonString = "";
			if(jsonTemplate != null && jsonTemplate.getJsonString() != null) {
				jsonString = jsonTemplate.getJsonString();
				
				List<ChatbotApiParam> params = apiInfo.getParamList();
				if(params != null) {
					for(ChatbotApiParam param: params) {
						if(IChatbotConst.API_HTTP_PARAM_TYPE_HEADER.equals(param.getParamType())) {
							continue;
						}
						jsonString.replaceAll("{{"+param.getParamName()+"}}", param.getParamValue());
					}
				}
			}
			
			resp = HttpConnectionManager.getInstance().post(apiInfo.getEndpoint(), jsonString, headers);
		}
		
		return resp;
	}

	/**
	 * 팝업 버튼을 만들기 위한 파싱...
	 * @param buttons
	 */
	private  void seperateButtonText(List<ChatbotButton> buttons ) {
		if(buttons != null && buttons.size() > 0) {
			for(ChatbotButton button : buttons){
				if( button.getTitle().indexOf("CSS") > -1) {
					String[] text = button.getTitle().split("\\$CSS");
					button.setTitle(text[0]);
					button.setUserUtter(text[1]);
				}else{
					String text = button.getTitle();
					button.setTitle(text);
				}
			}
		}
	}
	
//	private String removeSystemCode(String respString) {
//		//QQQ pattern -> static
//		String orgMsg = "";
//		Pattern p;
//		Matcher m;
//		p = Pattern.compile(".*("+IChatbotConst.PREFIX_SYSOUT+"[^\\s.+]+)\\s+(.*)");
//		m = p.matcher(respString);
//		if(m.find()) {
//			orgMsg = m.group(2);
//			logger.debug("removeSystemCode/found system code:{}",orgMsg);
//		}
//		return orgMsg;
//	}
	
//	private boolean doButtons(DialogSession ses, ChatbotRespInterfaceRelData respRel) {
//		ChatbotService chatbotService = (ChatbotService)ApplicationContextProvider.getApplicationContext().getBean(ChatbotService.class);
//		
//		ChatbotRichContent richContent = null;
//		try {
//			UtterData utter = DialogUtils.getLastSystemUtter(ses);
//			String sysResp = utter.getSentence();
//			sysResp = DialogUtils.removeSystemCode(sysResp);
//			utter.setSentence(sysResp);
//			
//			richContent = chatbotService.getRichContent(respRel.getIfId());
//			
//			
//		} catch (Exception e) {
//			logger.warn("doButtons/{}",e.getMessage());
//		}
//		
//		return false;
//	}
//	
//	private boolean doImage(DialogSession ses, ChatbotRespInterfaceRelData respRel) {
//		ChatbotService chatbotService = (ChatbotService)ApplicationContextProvider.getApplicationContext().getBean(ChatbotService.class);
//		
//		ChatbotRichContent richContent = null;
//		try {
//			richContent = chatbotService.getRichContent(respRel.getIfId());
//		} catch (Exception e) {
//			logger.warn("doImage/{}",e.getMessage());
//		}
//		
//		return false;
//	}
//	
//	private boolean doCarousel(DialogSession ses,ChatbotRespInterfaceRelData respRel) {
//		ChatbotService chatbotService = (ChatbotService)ApplicationContextProvider.getApplicationContext().getBean(ChatbotService.class);
//		
//		ChatbotRichContent richContent = null;
//		try {
//			richContent = chatbotService.getRichContent(respRel.getIfId());
//		} catch (Exception e) {
//			logger.warn("doCarousel/{}",e.getMessage());
//		}
//		
//		return false;
//	}
}
