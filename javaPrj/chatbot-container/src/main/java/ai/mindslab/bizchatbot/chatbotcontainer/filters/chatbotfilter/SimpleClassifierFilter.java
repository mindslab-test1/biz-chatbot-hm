package ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.ChatbotOrderedFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.service.ClassifierService;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;

public class SimpleClassifierFilter extends ChatbotOrderedFilter{

	private Logger logger = LoggerFactory.getLogger(SimpleClassifierFilter.class);
	
	public SimpleClassifierFilter() {
	}

	public SimpleClassifierFilter(int order) {
		super(order);
	}
	
	@Override
	public boolean execBody(DialogSession ses, ChatbotConfig config) throws BizChatbotException {
		
		if("N".equals(config.getScfYn())) {
			//skip sc. go to next filter
			return true;
		}
		
		ClassifierService sc = (ClassifierService)ApplicationContextProvider.getApplicationContext().getBean(ClassifierService.class);
		Environment env = (Environment)ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
		
		DialogProcessing processing = ses.getProcessing();
		if(!StringUtils.isEmpty(processing.getSdsDomain())) {
			//skip & continue next filters
			return true;
		}
		String sdsDomain = "";
		UtterData lastUserUtter = null;
		try {
			lastUserUtter = DialogUtils.getLastUserUtter(ses);
		}catch(Exception e) {
			logger.warn("execute/{}", e.getMessage());
 			return false;
		}
		
		if(lastUserUtter != null) {
			try {
				sdsDomain = sc.doSimple(config.getBotId(), lastUserUtter.getSentence());
			}catch(Exception e) {
				
			}
			//QQQ use default domain?
//			if(StringUtils.isEmpty(sdsDomain)) {
//				sdsDomain = env.getProperty("sds.default.domain");
//			}
		}
		ses.getProcessing().setSdsDomain(sdsDomain);
		return true;
	}
}
