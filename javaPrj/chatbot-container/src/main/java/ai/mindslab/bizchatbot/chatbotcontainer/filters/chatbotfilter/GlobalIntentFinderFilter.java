package ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.ChatbotOrderedFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.service.ClassifierService;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.IntentFinderDomainRel;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.SYS_UTTER_TYPE;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.session.DialogService;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing.Slot;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.SdsData;
import ai.mindslab.bizchatbot.commons.session.data.SdsUtterRespData;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.commons.utils.JsonUtils;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalIntentInfo;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfData;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfSdsData;
import ai.mindslab.bizchatbot.commons_ex.preitf.service.ChatbotServiceEx;
import ai.mindslab.bizchatbot.sds.client.SdsClient;
import ai.mindslab.bizchatbot.sds.client.SdsClientFactory;
import ai.mindslab.bizchatbot.sds.client.SdsNgItfClient;

public class GlobalIntentFinderFilter extends ChatbotOrderedFilter{

	private Logger logger = LoggerFactory.getLogger(GlobalIntentFinderFilter.class);
	
	private static Pattern p;
	static {
		p = Pattern.compile("SLU.*\\#(.+)\\(\\)");
	}
	
	public GlobalIntentFinderFilter() {
	}
	
	public GlobalIntentFinderFilter(int order) {
		super(order);
	}
	
	@Override
	public boolean execBody(DialogSession ses, ChatbotConfig config) throws BizChatbotException {
		DialogService ds = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		ChatbotServiceEx chatbotServiceEx = (ChatbotServiceEx)ApplicationContextProvider.getApplicationContext().getBean(ChatbotServiceEx.class);
		GlobalItfData gItfData = null;
		SYS_UTTER_TYPE svcType = SYS_UTTER_TYPE.NORMAL;
		
		DialogProcessing processing = ses.getProcessing();
//		if(processing == null || processing.getSvcType() == SYS_UTTER_TYPE.NORMAL) {
			try {
				gItfData = chatbotServiceEx.getGlobalItfData(ses.getDialog().getBotId());
				if(gItfData == null) {
					//continue;
					return true;
				}

				/* global을 타야할  domain 이 아니라면 애초에 skip*/
				if(processing.getSdsDomain()!= null  &&
						 processing.getSdsDomain().length() > 0 && !gItfData.getProjectName().equals(processing.getSdsDomain())){
					return true;
				}
				
				//send to sds intent finder.
				if(gItfData.getSdsList()==null || gItfData.getSdsList().size() == 0) {
					return true;//continue
				}
				
				UtterData lastUserUtter = DialogUtils.getLastUserUtter(ses);
				String lastSentence = lastUserUtter.getSentence();
				
				GlobalIntentInfo info = queryIntent(ses, gItfData, lastSentence);
				if(info == null) {
					return true;//continue
				}
				
				//distinguish svcType
				List<GlobalItfSdsData> gItfList = gItfData.getSdsList();
				for(GlobalItfSdsData sds: gItfList) {
					if(sds.getIntentName().equals(info.getIntentName())) {
						svcType = SYS_UTTER_TYPE.getEnum(sds.getSvcTypeCd());
						break;
					}
				}
				
//				processing = new DialogProcessing();
				//processing.getExpectedSlots().add(new Slot(info.getSlotName(), info.getSlotValue()));

				/**
				 *multi Slot 추가..
				 * 뒷단에서 필요부분을 걸러내기때문에 slot이 중복되어도 상관 없음..
				 * 그냥 넣으면된다.!
				 */
				for(GlobalIntentInfo.Slot  s: info.getSlots()){
					processing.getExpectedSlots().add(new Slot(s.getSlotName(), s.getSlotValue()));
				}

				processing.setSvcType(svcType);
				processing.setIfId(info.getIfId());
				ses.setProcessing(processing);
			} catch (Exception e) {
				logger.warn("GlobalIntentFinderFilter/User Utter is null/"+ses.getSessionId(), e);
				DialogUtils.setSessionError(ses, "GlobalIntentFinderFilter", e);
				
//				return false;
			}
//		}
		
		return true;
	}
	
	private GlobalIntentInfo queryIntent(DialogSession ses, GlobalItfData gItf, String utter) throws Exception{
		logger.info("global queryIntent in SDS/{}",utter);
		
		Environment env = (Environment)ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
		
		String clientType = env.getProperty("sds.client.type");
		if(StringUtils.isEmpty(clientType)) {
			clientType = "base";
		}
		
		GlobalIntentInfo info = null;
		try {
			if(IChatbotConst.CLIENT_TYPE_NG.equals(clientType)) {
				info = doNgQuery(ses, gItf, utter);
			}else {
				throw new Exception("Global Intent finder supports only NG Client.");
			}
		}catch(Exception e) {
			logger.warn("queryIntent/{}",e.getMessage());
		}
		return info;
	}
	
	
	private GlobalIntentInfo doNgQuery(DialogSession ses, GlobalItfData gItf, String utter) throws Exception{
		
		String resp = "";
		GlobalIntentInfo info = null;
		SdsNgItfClient client = ApplicationContextProvider.getApplicationContext().getBean(SdsNgItfClient.class);
		try {
			resp = client.globalIntentQuery(ses.getDialog().getBotId(), utter);
			if(!StringUtils.isEmpty(resp)) {
				info = parseIntentInfo(resp);
			}
		}catch(ConnectException e) {
			logger.warn("can't connect to Intent Finder");
			throw e;
		}catch(Exception e) {
			throw e;
		}
		
		return info;
	}

	
	private GlobalIntentInfo parseIntentInfo(String debug) {
		logger.debug("parseIntentInfo/{}",debug);
		
		BaseResponse<GlobalIntentInfo> out = JsonUtils.fromJsonRespUtter(debug, GlobalIntentInfo.class);
		if(out != null) {
			return out.getData();
		}else {
			return null;
		}
	}

}
