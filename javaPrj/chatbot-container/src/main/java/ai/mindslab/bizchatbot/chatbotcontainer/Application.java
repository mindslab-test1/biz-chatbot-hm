package ai.mindslab.bizchatbot.chatbotcontainer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages={"ai.mindslab.bizchatbot"})
public class Application extends SpringBootServletInitializer {
	private static Logger logger = LoggerFactory.getLogger(Application.class);


//	public static final String USE_HAZELCAST = "false";
	
	/**
	 * main
	 * @param args
	 */
	public static void main(String[] args) {
		logger.info("Bizchatbot Chatbot Container Server newly started.");
		
		SpringApplication.run(Application.class, args);
	}
}
