package ai.mindslab.bizchatbot.chatbotcontainer.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import ai.mindslab.bizchatbot.commons.annotation.BackAuth;

@Component
public class BizChatbotAuthInterceptor extends HandlerInterceptorAdapter {
	
	private Logger logger = LoggerFactory.getLogger(BizChatbotAuthInterceptor.class);

	@Value("${security.allow.ips}")
	private String[] allowedIps;
	
	public BizChatbotAuthInterceptor() {
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
//		return super.preHandle(request, response, handler);
		
		HandlerMethod handlerMethod = (HandlerMethod)handler;
		BackAuth filter = handlerMethod.getMethod().getAnnotation(BackAuth.class);
		if(filter == null) {
			//pass
			return true;
		}
		
		//not configured
		if(allowedIps == null || allowedIps.length == 0) {
			return true;
		}
		
		if(allowedIps != null && allowedIps[0].equals("*")) {
			return true;
		}
		
        for(String ip : allowedIps){
            if(request.getRemoteAddr().matches(ip)) {
                return true;
            }
        }
        
        logger.info("Not allowed IP {} tried to access.", request.getRemoteAddr());
		return false;
	}
	
}
