package ai.mindslab.bizchatbot.chatbotcontainer.filters;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
//import ai.mindslab.bizchatbot.commons.utils.BizChatbotApplicationContextAware;

@Component
@Scope("prototype")
public class FilterManager {

	@Autowired
	private FilterChain filterChain;
	
	private ChatbotConfig chatbotConfig;
	
	public FilterManager() {
//		filterChain = new FilterChain();
	}
	
	public ChatbotConfig getChatbotConfig() {
		return chatbotConfig;
	}

	public void setChatbotConfig(ChatbotConfig chatbotConfig) {
		this.chatbotConfig = chatbotConfig;
	}

	public void addFilter(ChatbotOrderedFilter filter) {
		filterChain.addFilter(filter);
	}
	
	public void execute(DialogSession ses) throws BizChatbotException{
		filterChain.execute(ses, chatbotConfig);
	}
	
}
