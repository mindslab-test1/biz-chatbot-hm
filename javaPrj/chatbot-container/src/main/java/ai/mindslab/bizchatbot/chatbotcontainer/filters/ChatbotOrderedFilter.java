package ai.mindslab.bizchatbot.chatbotcontainer.filters;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;

public class ChatbotOrderedFilter implements ChatbotBaseFilter {

	protected int order = 1;
	public static Pattern p;
	static {
		p = Pattern.compile(".+\\.(\\w+)Filter.*$");
	}
	public ChatbotOrderedFilter() {
	}
	
	public ChatbotOrderedFilter(int order) {
		this.order = order;
	}

	@Override
	public boolean execute(DialogSession ses, ChatbotConfig config) throws BizChatbotException {
		ses.setCurrFilter(this.getClass().getName());
		boolean bRes = execBody(ses, config);
		if(bRes == false) {
			UtterData utter;
			try {
				utter = DialogUtils.getLastSystemUtter(ses);
				utter.setCurrStep(getFilterName(this.getClass().getName()));
			} catch (Exception e) {
			}
		}
		return bRes;
	}

	@Override
	public int getOrder() {
		return order;
	}

	@Override
	public void setOrder(int order) {
		this.order = order;
	}

	public void warn(Logger logger, String loc, DialogSession ses, Exception e) {
		logger.warn("{}/{}/{}/{}",loc, ses.getDialog().getBotId(), ses.getSessionId(), e.getMessage());
	}
	
	public boolean execBody(DialogSession ses, ChatbotConfig config) throws BizChatbotException {
		return false;
	}
	
	protected String getFilterName(String clsName) {
		String out = "";
		try {
//			Pattern p;
			Matcher m;
//			p = Pattern.compile(".+\\.(\\w+)Filter$");
			String tmp = clsName.trim();
			m = p.matcher(tmp);
			if(m.find()) {
				out = m.group(1);
			}
		}catch(Exception e) {
		}
		return out;
	}
}
