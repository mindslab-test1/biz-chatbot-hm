package ai.mindslab.bizchatbot.chatbotcontainer.filters;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;

public interface ChatbotBaseFilter {
	
	/**
	 * If it returns false, stop executing next filters
	 * @param ses
	 * @return
	 */
	public boolean execute(DialogSession ses, ChatbotConfig config) throws BizChatbotException;
	
	public int getOrder();
	public void setOrder(int order);
	
//	public boolean hasError();
//	
//	public String getLastErrorMessage();
}
