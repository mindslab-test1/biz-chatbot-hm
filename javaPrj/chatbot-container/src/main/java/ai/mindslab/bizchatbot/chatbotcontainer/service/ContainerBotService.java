package ai.mindslab.bizchatbot.chatbotcontainer.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.hazelcast.core.IMap;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.FilterManager;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.BQAFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.BQARichContentFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.GlobalIntentFinderFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.InitialSynonymFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.IntentFinderSdsFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.InterfaceFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.MobisServiceFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.PreBQAINTFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.SdsFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.SdsFilterEx;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.SdsRichContentFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.SimpleClassifierFilter;
import ai.mindslab.bizchatbot.commons.cache.service.ExternalCacheService;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotIntetFinderSdsData;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.SimpleClassifierData;
import ai.mindslab.bizchatbot.commons.chatbot.service.ChatbotService;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.session.data.SdsUtterRespData;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;
import ai.mindslab.bizchatbot.commons.utils.JsonUtils;

@Service
public class ContainerBotService implements IChatbotConst{

	private Logger logger = LoggerFactory.getLogger(ContainerBotService.class);
	
	@Autowired
	private HazelClientCommon hazelClient;
	
	@Autowired
	private ChatbotService chatbot;
	
	@Autowired
	private ClassifierService clfservice;
	
	@Autowired
	private ExternalCacheService extCacheService;
	
	private final long ttlMilis = 30*1000;// 2min -> 30sec
	
	@Value("${sds.client.type:base}")
	private String clientType;
	
	@Value("${sds.ng.container.port:8888}")
	private int ngPort;
	
	@Value("${custom.filter.use:true}")
	private boolean useCustomfilter;
	
	public ContainerBotService() {
	}

	
	public ChatbotConfig getBotConfig(int botId) throws Exception{
		
		IMap<String, ChatbotConfig> map = hazelClient.getMap(MAP_NAME_BOTCONFIG);
		ChatbotConfig config = map.get(botId+"");
		if(config == null) {
			config = chatbot.getChatbotConfig(botId);
			map.put(""+botId, config);
		}
		
		return config;
	}
	
	public FilterManager createFilterManager(int botId) throws Exception{
		
		ChatbotConfig botConfig = getBotConfig(botId);
		
		FilterManager fm = (FilterManager)ApplicationContextProvider.getApplicationContext().getBean(FilterManager.class);
//		FilterManager fm = new FilterManager();
		fm.setChatbotConfig(botConfig);
		
		if("Y".equals(botConfig.getSynonymYn())){
			InitialSynonymFilter fInit = new InitialSynonymFilter();
			fm.addFilter(fInit);
		}
		
		int filterOrder = 1;
		
		//Mobis
		if(useCustomfilter) {
			GlobalIntentFinderFilter gItf = new GlobalIntentFinderFilter(filterOrder++);
			fm.addFilter(gItf);
			
			MobisServiceFilter mobFilter = new MobisServiceFilter(filterOrder++);
			fm.addFilter(mobFilter);
		}
		
		PreBQAINTFilter fPreBqa = new PreBQAINTFilter(filterOrder++);
		fm.addFilter(fPreBqa);
		
		BQAFilter fBqa = new BQAFilter(filterOrder++);
		fm.addFilter(fBqa);
		
		BQARichContentFilter fRichBqa = new BQARichContentFilter(filterOrder++);
		fm.addFilter(fRichBqa);
		
		List<SimpleClassifierData> scs = clfservice.getSimpleClassifer(botId);
		if(scs != null && scs.size() > 0) {
			SimpleClassifierFilter fSc = new SimpleClassifierFilter(filterOrder++);
			fm.addFilter(fSc);
		}
		
		List<String> itfDomains = clfservice.getIntentFinderDomain(botId);
		if(itfDomains != null) {
			for(String domain: itfDomains) {
				IntentFinderSdsFilter itf = new IntentFinderSdsFilter(filterOrder++);
				itf.setIntentFinderDomain(domain);
				fm.addFilter(itf);
				break;//QQQ temp, one domain
			}
		}
		
		
		//sds client type
		if(IChatbotConst.CLIENT_TYPE_BASE.equals(clientType)) {
			SdsFilter fSds = new SdsFilter(filterOrder++);
			fm.addFilter(fSds);
		}else if(IChatbotConst.CLIENT_TYPE_NG.equals(clientType)) {
			SdsFilterEx fSds = new SdsFilterEx(filterOrder++);
			fSds.setNgPort(ngPort);
			fm.addFilter(fSds);
		}
		
		InterfaceFilter fInterface = new InterfaceFilter(filterOrder++);
		fm.addFilter(fInterface);
		
		SdsRichContentFilter sdsRichContent = new SdsRichContentFilter(filterOrder++);
		fm.addFilter(sdsRichContent);
		
		return fm;
	}
	
	
	//near cache clear bug in hazelcast.
	@Scheduled(fixedRate = ttlMilis)
	public void doExpiry() {
		String cacheName = "botconfig";
		logger.debug("doExpiry/{}",cacheName);
		extCacheService.clearCacheConfig(cacheName);
	}
	
}
