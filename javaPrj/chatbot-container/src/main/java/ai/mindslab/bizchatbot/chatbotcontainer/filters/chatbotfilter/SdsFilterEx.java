package ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import ai.mindslab.bizchatbot.chatbotcontainer.service.BackendApiService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.ChatbotOrderedFilter;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotDomainSimple;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.SdsProjectPortHis;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.SDS_STATUS;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.codes.ISDSCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.session.DialogService;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.SdsData;
import ai.mindslab.bizchatbot.commons.session.data.SdsUtterRespData;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.session.data.UtterReconfirmData;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;
import ai.mindslab.bizchatbot.sds.client.SdsClientFactory;
import ai.mindslab.bizchatbot.sds.client.SdsNgClient;

public class SdsFilterEx extends ChatbotOrderedFilter{
	
	private Logger logger = LoggerFactory.getLogger(SdsFilterEx.class);
	
	private DialogService ds;
	private boolean isDialogEnded = false;
	private int ngPort;
	
	private static ObjectMapper mapper = null;
	
	static{
		mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
		mapper.setTimeZone(TimeZone.getTimeZone("GMT+09:00"));
	}
	
	public SdsFilterEx() {
	}

	public SdsFilterEx(int order) {
		super(order);
	}
	
	public void setNgPort(int port) {
		this.ngPort = port;
	}
	
	@Override
	public boolean execBody(DialogSession ses, ChatbotConfig config) throws BizChatbotException {

//		ClassifierService sc = (ClassifierService)ApplicationContextProvider.getApplicationContext().getBean(ClassifierService.class);
//		Environment env = (Environment)ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
		ds = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		
		String sdsDomain = "";
		UtterData lastUserUtter = null;
		try {
			lastUserUtter = DialogUtils.getLastUserUtter(ses);
		}catch(Exception e) {
			logger.warn("execute/{}", e.getMessage());
			DialogUtils.setSessionError(ses, "SdsFilter", "User Utter is not exist");
 			return false;
		}
		
		
		sdsDomain = ses.getProcessing().getSdsDomain();
		
		//QQQ check if user's utter is expected slot type
		String expSlot = ses.getProcessing().getExpectedSlot();
		
//		SdsData sdsData = ses.getSdsData();
		UtterData utter = null;
		try {
			//sds filter에 들어올 필요가 없는 경우 확인.
			if(StringUtils.isEmpty(sdsDomain)) {
				utter = DialogUtils.createBotUtterData(ses);
				DialogUtils.setUnknownMsg(utter, config);
				this.setNlpUtter(utter, lastUserUtter);

				logger.debug("execute/system unknown message due to no sds domain/{}",utter.getSentence());
				ds.insertBotUtterHistory(utter.getBotId(), ses.getUserData().getUserId(), ses.getSessionId(), utter.getSentence(), utter.getDialogSeq(),"", "", IChatbotConst.MSG_TYPE_UNKNOWN, "");
				ses.getProcessing().setCurrBotUtter(utter.getSentence());
				ses.getDialog().getConversation().add(utter);
				return false;
			}
			
			if(!DialogUtils.isOpenedSdsSession(ses)) {
				//initially create sds session
				utter = openSdsSession(ses, sdsDomain);
				//sds greeting -> skip
				//ds.insertUserUtterHistory(ses.getUserData().getUserId(), ses.getSessionId(), utter.getSentence());
				logger.debug("execute open Sds session/{}", utter);
				
				//if it's one of multiple domains -> skip sendSdsTalk, show sds greeting
				/*List<ChatbotDomainSimple> sdsDomains = config.getSdsDomains();
				if(sdsDomains != null && sdsDomains.size() > 1) {
					//utter 유지

				}else if(sdsDomains != null && sdsDomains.size() == 1){
					if(utter != null) {
						//user uttr전달
						ses.getProcessing().setCurrBotUtter(utter.getSentence());
						utter = sendSdsTalk(ses, sdsDomain);
					}
				}*/
				if(utter != null) {
					//user uttr전달
					ses.getProcessing().setCurrBotUtter(utter.getSentence());
					utter = sendSdsTalk(ses, sdsDomain);
				}else {
//				if(utter == null) {
					logger.warn("sds session open error/{},{}", ses.getSessionId(), sdsDomain);
					//QQQ
					throw new BizChatbotException(IRestCodes.ERR_CODE_SDS_OPEN_ERROR, IRestCodes.ERR_MSG_SDS_OPEN_ERROR);
				}
			}
//			else if(!isDialogEnded) {
			else {
				utter = sendSdsTalk(ses, sdsDomain);
				logger.debug("execute send Sds talk/{}", utter);
			}
			
			if(utter == null) {
				utter = DialogUtils.createBotUtterData(ses);
				DialogUtils.setUnknownMsg(utter, config);
				this.setNlpUtter(utter, lastUserUtter);
				logger.debug("execute/system unknown message/{}",utter.getSentence());
			}
			
			if(DialogUtils.isUnknownSystemUtter(utter)) {

				String sdsUnknownUtter = utter.getSentence();
				if(StringUtils.isEmpty(sdsUnknownUtter)) {
					// SDS sentence 없을시
					DialogUtils.setUnknownMsg(utter, config);
				} else {
					// unknown 상태값 추가
					utter.setMsgTypeCd(IChatbotConst.MSG_TYPE_UNKNOWN);
				}

				this.setNlpUtter(utter, lastUserUtter);
				logger.debug("execute/system unknown message/org: {}",utter.getSentence());
			}
			
			ds.insertBotUtterHistory(utter.getBotId(), ses.getUserData().getUserId(), ses.getSessionId(), utter.getSentence(), utter.getDialogSeq(),"", ses.getProcessing().getSdsDomain(), "",getFilterName(this.getClass().getName()));
			ses.getProcessing().setCurrBotUtter(utter.getSentence());
			ses.getDialog().getConversation().add(utter);
			
			if(DialogUtils.needIFResultMapping(ses)) {
				//continue & do 3rd parth interface mapping
				return true;
			}else {
				if("1".equals(utter.getIsDialogEnded())) {
					//QQQ update last status?? -> 확인할것 
					//reset curr stat.
					DialogUtils.resetProcessingData(ses);
					//add 화재전환 메세지
					UtterData lastSysUtter = DialogUtils.getLastSystemUtter(ses);
					String msg =ds.getChatbotMsgs(ses.getDialog().getBotId(), IChatbotConst.MSG_TYPE_SDS_RECONFIRM);
					if(!StringUtils.isEmpty(msg)) {
						UtterReconfirmData reconf = new UtterReconfirmData();
						reconf.setMsg(msg);
						//lastSysUtter.setSentence(lastSysUtter.getSentence() + "\n" + DialogUtils.getReconfirmMsg(msg));
						lastSysUtter.setReconfirmMsg(reconf);
						ChatbotRichContent richGreeting =  config.getGreetingRichContent();
						if(richGreeting != null) {
							reconf.setRichContent(richGreeting);
						}
					}
				}
			}
		}catch(BizChatbotException e) {
			logger.warn("execute/{}/{}",ses.getSessionId(), e.getErrMsg());
			
			DialogUtils.setSessionError(ses, "SdsFilter", e);
		}catch(Exception e) {
			logger.warn("execute/{}/{}",ses.getSessionId(), e.getMessage());
			
			DialogUtils.setSessionError(ses, "SdsFilter", e);
		}
		//stop next filter execution
		return false;
	}
	
	
	//public synchronized UtterData openSdsSession(DialogSession ses, String sdsDomain) {
	public UtterData openSdsSession(DialogSession ses, String sdsDomain) {
		
		UtterData utter = null;
		HazelClientCommon hazel = (HazelClientCommon)ApplicationContextProvider.getApplicationContext().getBean(HazelClientCommon.class);
		Environment env = (Environment)ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
		DialogService ds = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		
		SdsData sData = new SdsData();
		sData.setProjectName(sdsDomain);
		sData.setProjectPort(ngPort);
		sData.setDaemonLogFileName(ses.getUserData().getUserId()+"."+ses.getSessionId());//QQQ
		sData.setRequest(ISDSCodes.REQUEST_INIT_DIALOG);
		sData.setSdsIp(env.getProperty("sds.ip"));//QQQ clustering case, choose one ip
		sData.setUserName(ses.getUserData().getUserId());//QQQ with user id?->user name?
		ses.setSdsData(sData);
		
		SdsClientFactory sdsFactory = (SdsClientFactory)ApplicationContextProvider.getApplicationContext().getBean(SdsClientFactory.class);
		SdsNgClient sdsClient = sdsFactory.makeSdsNgClient();
		
		try {
			sdsClient.setSdsData(sData);
			sdsClient.setSessionId(ses.getSessionId());
			logger.debug("befor initDialog/{}",sData);
			SdsUtterRespData sdsResp = sdsClient.initDialog();
			logger.debug("openSdsSession/sdsresp: {}",sdsResp);
			if(sData.getStatus() == SDS_STATUS.OPEN) {
				ds.insertSdsPortHis(ses.getDialog().getBotId(), ses.getSessionId(), sData.getProjectPort(), sData.getSdsIp());
				
				utter = DialogUtils.createBotUtterData(ses);
//				utter.setSentence(sdsResp.getSystem());
				utter.setSentence(DialogUtils.extractSysUtter(sdsResp));
				return utter;
			}else {
				return null;
			}
			
		}catch(Exception e) {
			logger.warn("openSdsSession/{}",e.getMessage());
			
			return null;
		}
	}
	
	public UtterData sendSdsTalk(DialogSession ses, String sdsDomain) {
		
		UtterData utter = null;
//		HazelClientCommon hazel = (HazelClientCommon)ApplicationContextProvider.getApplicationContext().getBean(HazelClientCommon.class);
		Environment env = (Environment)ApplicationContextProvider.getApplicationContext().getBean(Environment.class);
		
		SdsData sData = ses.getSdsData();
		sData.setRequest(ISDSCodes.REQUEST_USER_UTTER);
		ses.setSdsData(sData);
		
//		SdsClient sdsClient = (SdsClient)ApplicationContextProvider.getApplicationContext().getBean(SdsClient.class);
		SdsClientFactory sdsFactory = (SdsClientFactory)ApplicationContextProvider.getApplicationContext().getBean(SdsClientFactory.class);
		SdsNgClient sdsClient = sdsFactory.makeSdsNgClient();
		UtterData userUtter = null;
		try {
			userUtter = DialogUtils.getLastUserUtter(ses);
		
			sdsClient.setSdsData(sData);
			sdsClient.setSessionId(ses.getSessionId());
			SdsUtterRespData sdsResp = sdsClient.userUtter(userUtter.getSentence());
			logger.debug("sendSdsTalk/sdsresp: {}",sdsResp);
			
			utter = DialogUtils.createBotUtterData(ses);
			utter.setSentence(DialogUtils.extractSysUtter(sdsResp).replace("\\n", "<br>"));
			try {
				utter.setSluWeight(Float.parseFloat(sdsResp.getSlu_weight()));
			}catch(Exception e) {
			}
			utter.setIsDialogEnded(sdsResp.getIsDialogEnd());
			utter.setDa(sdsResp.getDa());
			String nextSlot = DialogUtils.extractDaSlot(sdsResp.getDa());
			ses.getProcessing().setExpectedSlot(nextSlot);
			//multi
			ArrayList<DialogProcessing.Slot> slots = DialogUtils.extratSluSlots(sdsResp.getSlu());
			if(slots != null && slots.size() > 0) {
				DialogUtils.addSlots(ses.getProcessing(), slots);
			}
			
			//set sds debug info.
			if(ses.getShowDebug() != null && ses.getShowDebug()) {
				utter.setDebug(sdsResp);
			}
			
			//QQQ check if sds conv. ended
			//new model -> auto exit
			if("1".equals(sdsResp.getIsDialogEnd())) {
				isDialogEnded = true;
//				
//				try {
//					sData.setRequest(ISDSCodes.REQUEST_STOP_DIALOG);
//					sdsClient.stopDialog();
//					String stopResp = sdsClient.readData();
//					logger.debug("stopResp/{}",stopResp);
//				}catch(IOException e) {
//					logger.warn("stopDialog error - continue:{}", e.getMessage());
//				}finally {
//					removePort(env, sData.getProjectPort());
//				}
			}
			
			//insert history
//			ds.insertBotUtterHistory(ses.getUserData().getUserId(), ses.getSessionId(), utter.getSentence());
			
			return utter;
		}catch(Exception e) {
			logger.warn("sendSdsTalk/{}",e.getMessage());
			return null;
		}finally {
			if(utter != null) {
				utter.setSdsDomain(ses.getProcessing().getSdsDomain());
			}
		}
	}
	
	private int getNextPortFromServer(Environment env, int port, int maxport) {
		String url = env.getProperty("sds.project.port.finder.url");
		String portResp = "";
		int newPort = port;
		int newMaxPort = maxport;
		for(int idx = 0;idx<5;idx++) {
			try {
				if(idx > 0) {
					//temp
//					newPort += 1000;
//					newMaxPort += 1000;
					newPort += 20;
					newMaxPort += 20;
				}
				
				portResp = HttpConnectionManager.getInstance().get(url+"?port="+newPort+"&maxport="+newMaxPort, null);
				BaseResponse<Integer> out = null;
				try {
					out = mapper.readValue(portResp, new TypeReference<BaseResponse<Integer>>() {});
					newPort = out.getData();
					break;
				} catch (JsonProcessingException e) {
					logger.warn("getNextPortFromServer/{}", e.getMessage());
					throw e;
				} catch (IOException e) {
					logger.warn("getNextPortFromServer/{}", e.getMessage());
					throw e;
				}
				
			}catch(Exception e) {
				logger.warn("getNextPortFromServer err. use original port {}/{}", newPort,e.getMessage());
			}
		}
		return newPort;
	}
	
	private void removePort(Environment env, int port) {
		String url = env.getProperty("sds.project.port.remover.url");
		String portResp = "";
		int newPort = port;
		try {
			portResp = HttpConnectionManager.getInstance().get(url+"?port="+newPort, null);
			BaseResponse<Integer> out = null;
			try {
				out = mapper.readValue(portResp, new TypeReference<BaseResponse<String>>() {});
			} catch (JsonProcessingException e) {
				logger.warn("removePort/{}", e.getMessage());
				throw e;
			} catch (IOException e) {
				logger.warn("removePort/{}", e.getMessage());
				throw e;
			}
			
		}catch(Exception e) {
			logger.warn("removePort err. original port {}", port);
		}
	}

	// unknown시에 lastUserUtter 형태소 분석해서 명사만 추출하기 - MRC & 검색을 위한 기능
	private void setNlpUtter(UtterData utter, UtterData lastUserUtter) {
		BackendApiService bApiService = (BackendApiService)ApplicationContextProvider.getApplicationContext().getBean(BackendApiService.class);
		BaseResponse<?> resp = bApiService.doNlpAnalyze(lastUserUtter.getSentence());
		if(resp.getCode() == IRestCodes.ERR_CODE_SUCCESS) {
			utter.setNlpUtter((String) resp.getData());
		}
	}
	
}
