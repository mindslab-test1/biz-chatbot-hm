package ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.ChatbotOrderedFilter;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiParam;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiResultMappingInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotButton;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRespInterfaceRelData;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.outif.ThirdPartyApiManager;
import ai.mindslab.bizchatbot.commons.outif.ThirdPartyApiManager.ApiResult;
import ai.mindslab.bizchatbot.commons.session.DialogService;
import ai.mindslab.bizchatbot.commons.session.data.BqaProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.ApiUtils;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.commons_ex.bqa.rich.service.BqaRichServiceSimple;

/**
 * BQARich API에서는 <parma=value/>와 같은 format을 사용한다. 
 * SDS Rich API와는 데이터 저장 방식이 다르므로 이곳에서 별도 구현. 
 */
public class BQARichContentFilter extends ChatbotOrderedFilter{

	private Logger logger = LoggerFactory.getLogger(BQARichContentFilter.class);
			
	public BQARichContentFilter() {
	}
	
	public BQARichContentFilter(int order) {
		super(order);
	}
	
	@Override
	public boolean execBody(DialogSession ses, ChatbotConfig config) throws BizChatbotException {
		ThirdPartyApiManager apiManager =  (ThirdPartyApiManager)ApplicationContextProvider.getApplicationContext().getBean(ThirdPartyApiManager.class);
		DialogService dialogService = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		BqaRichServiceSimple bqaRichService = (BqaRichServiceSimple)ApplicationContextProvider.getApplicationContext().getBean(BqaRichServiceSimple.class);
		
		BqaProcessing bqaProcess = ses.getProcessing().getBqaProcessing();
		if(bqaProcess == null) {
			return true;
		}
		
		String bqaRespType = bqaProcess.getContentTypeCd();
		String bqaRespCode = bqaProcess.getRespCode();
		int bqaRcId = bqaProcess.getRcId();
		
		if(!IChatbotConst.RICH_CONTENT_TYPE_API.equals(bqaRespType)) {
			//skip api filter
			return true;
		}
		
		//API연동만 다룬다. rich => BQAFilter
		ChatbotApiInfo apiInfo = null;
		String resp = "";
		
		try {
			UtterData utter = DialogUtils.getLastSystemUtter(ses);
			String sysResp = utter.getSentence();
			
			apiInfo = bqaRichService.getBqaApiInfo(bqaRespCode);
			ApiUtils.setGlobalSlots(apiInfo != null? apiInfo.getParamList():null, ses.getUserData().getUserId(), ses.getRemoteAddr(), ses.getDialog().getBotId());
			List<DialogProcessing.Slot> slots = getParams(sysResp);
			
			if(slots != null && slots.size() > 0) {
				//no need to check user utter
				//just refer to slot values
				if(apiInfo != null) {
					List<ChatbotApiParam> params = apiInfo.getParamList();
					Iterator<ChatbotApiParam> i = params.iterator();
					while(i.hasNext()) {
						ChatbotApiParam p = i.next();
						if(DialogUtils.isApiNeedUserInputFilling(p.getParamValue())){
							String fillingType = DialogUtils.extractInputFillingType(p.getParamValue());
							DialogProcessing.Slot slot = DialogUtils.lookupSlot(slots, fillingType);
							//expSlot과 추출된 param type이 같아야만 한다.
							if(slot != null && !StringUtils.isEmpty(slot.getSlotName()) && fillingType.equals(slot.getSlotName())) {
								logger.debug("execute/input slot filling : {} -> {}", p.getParamValue(), slot.getSlotValue());
								p.setParamValue(slot.getSlotValue());
							}else {
								logger.warn("execute/different slot type. expected by sds: {}, param: {}", slot, fillingType);
//								throw new Exception("API Slot Filling Type Error." + slot +"/"+fillingType);

								// replace 되지 않은 slot 값은 parameter에서 제거
								i.remove();
							}
						}
					}
				}
			}
			
			if(apiInfo!=null) {
				if(IChatbotConst.API_HTTP_METHOD_GET.equals(apiInfo.getMethod())) {
					resp = InterfaceFilter.sendGet(apiInfo);
				}else if(IChatbotConst.API_HTTP_METHOD_POST.equals(apiInfo.getMethod())) {
					resp = InterfaceFilter.sendPost(apiInfo);
				}else {
					//error?
					throw new Exception("외부 API 설정 오류. method:"+ apiInfo.getMethod());
				}
				List<ChatbotApiResultMappingInfo> resMappingList = apiManager.getApiResultMappingInfo(apiInfo.getIfId());
				if(!StringUtils.isEmpty(resp) && resMappingList != null) {
					
					try {
						String tmpResp = sysResp;
						ApiResult apiOut = null;
						ChatbotRichContent richContent = null;
						for(ChatbotApiResultMappingInfo mapping:resMappingList) {
//							if(apiOut == null) {
							try {
								apiOut = apiManager.parseResult(resp, mapping.getJsonPath());
								if(apiOut != null && apiOut.getResult() instanceof List) {
									if(mapping.getDispName().equals("_result_")) {
										richContent = new ChatbotRichContent();
										richContent.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_BUTTON);
										List<ChatbotButton> buttons = apiManager.mappingResultStringToButtons(tmpResp, mapping.getDispName(), apiOut.getResult());
										this.seperateButtonText(buttons);
										richContent.setButtons(buttons);
										utter.setRichContent(richContent);
									}
								}
//							}
							}catch(Exception e) {
								logger.warn("execute/jsonpath err. {}", e.getMessage());
								tmpResp = apiManager.mappingResultString(tmpResp, mapping.getDispName(), "");
								continue;
							}
							
							tmpResp = apiManager.mappingResultString(tmpResp, mapping.getDispName(), apiOut.getResult());
							logger.debug("execute/{} To. {}", resp, tmpResp);
						}
						
						//update last sentence
						String outSentence = extractAnswerStringOnly(tmpResp);
						utter.setSentence(outSentence);
						utter.setSysUtterType(ses.getProcessing().getSvcType());

						try {
							// API에 따른 sysUtterType update
							apiOut = apiManager.parseResult(resp, "$.data.sysUtterTypeCode");
							if(apiOut != null && apiOut.getResult() instanceof String) {
								String sysUtterTypeCode = (String) apiOut.getResult();
								utter.setSysUtterType(IChatbotConst.SYS_UTTER_TYPE.getEnum(sysUtterTypeCode));
							}
						} catch(Exception e) {
							logger.warn("execute/jsonpath err. {}", e.getMessage());
						}

						//update history
						long nRet = dialogService.updateUtterHistory(utter);
						logger.debug("dialog history updated: {}, count: {}", utter, nRet);
					}catch(Exception ee) {
						logger.warn("SDS Response update error/{}", ee.getMessage());
						throw new Exception("외부 API 결과 맵핑 오류입니다.");
					}
				}
			}else {
				//error?
				//QQQ return org utter?
				throw new Exception("외부 API정보를 찾을 수 없습니다.");
			}
			
		}catch(Exception e) {
			logger.warn("execute/{}",e.getMessage());
			DialogUtils.setSessionError(ses, "BQARichContentFilter", e);
			try {
				UtterData lastSysUtter = DialogUtils.getLastSystemUtter(ses);
				//lastSysUtter.setSentence(e.getMessage());
				lastSysUtter.setSentence("정보조회 오류입니다.");
			} catch (Exception e1) {
				logger.warn("execute/{}", e1.getMessage());
			}
		}finally {
			DialogUtils.resetBqaProcessingData(ses);
			DialogUtils.resetProcessingData(ses);
		}
		return false;
	}
	
	private List<DialogProcessing.Slot> getParams(String sysUtter) {
		if(StringUtils.isEmpty(sysUtter)) {
			return null;
		}
		
		ArrayList<DialogProcessing.Slot> list = new ArrayList<>();
		String slot = "";
		String slotVal = "";
		Pattern p;
		Matcher m;
		p = Pattern.compile("(<([^<^>^]*)=([^<^>]*)/>)");
		m = p.matcher(sysUtter);
		while(m.find()) {
			slot = m.group(2);
			slotVal = m.group(3);
			DialogProcessing.Slot s = new DialogProcessing.Slot(slot, slotVal);
			list.add(s);
		}
		return list;
	}
	
	private String extractAnswerStringOnly(String sysUtter) {
		if(StringUtils.isEmpty(sysUtter)) {
			return "";
		}
		
		String out = sysUtter.replaceAll("<[^<^>]*/>", "");
		return out;
	}
	
	public boolean isBqaApiNeedUserInputFilling(String paramValue) throws Exception{
		if(StringUtils.isEmpty(paramValue)) {
			return false;
		}
		
		if(paramValue.startsWith(IChatbotConst.PREFIX_INPUTPARAM)){
			return true;
		}else {
			return false;
		}
	}


	/**
	 * 팝업 버튼을 만들기 위한 파싱...
	 * @param buttons
	 */
	private  void seperateButtonText(List<ChatbotButton> buttons ) {
		if(buttons != null && buttons.size() > 0) {
			for(ChatbotButton button : buttons){
				if( button.getTitle().indexOf("CSS") > -1) {
					String[] text = button.getTitle().split("\\$CSS");
					button.setTitle(text[0]);
					button.setUserUtter(text[1]);
				}else{
					String text = button.getTitle();
					button.setTitle(text);
				}
			}
		}
	}
}
