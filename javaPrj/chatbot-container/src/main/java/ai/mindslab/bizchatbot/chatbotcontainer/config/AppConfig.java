package ai.mindslab.bizchatbot.chatbotcontainer.config;

import java.util.concurrent.Executor;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;

@Configuration
@MapperScan(basePackages={"ai.mindslab.bizchatbot.commons.chatbot.dao","ai.mindslab.bizchatbot.commons_ex"})
@EnableScheduling
public class AppConfig {

//	Logger logger = LoggerFactory.getLogger(AppConfig.class);
	public static final String DATASOURCE_MAIN = "dataSource1";
	public static final String DATASOURCE_SDS = "sdsDatasource";
	
	public static final String TRANSACTION_MANAGER_MAIN = "transactionManager1";
	public static final String TRANSACTION_MANAGER_SDS = "sdsTransactionManager";
	
	public static final String TRANSACTION_SESSION_FACTORY_MAIN = "sqlSessionFactory1";
	public static final String TRANSACTION_SESSION_FACTORY_SDS = "sdsSqlSessionFactory";
	
	@Value("${hazelcast.server.ip}")
	private String[] hazelIps;
	
	@Value("${hazelcast.group.name}")
	private String groupName;
	
	@Value("${hazelcast.group.password}")
	private String groupPass;
	
	@Autowired
	@Qualifier("hazelClientCommon")
	private HazelClientCommon hazelClient;
	
	
	@Bean
	@Qualifier("hazelCacheManager")
	public CacheManager cacheManager() {
		return new HazelcastCacheManager(hazelcastInstance());
	}
	
	@Bean
    public HazelcastInstance hazelcastInstance() {
		return hazelClient.getClient();
    }
	
	@Bean
    public KeyGenerator keyGenerator() {
        return new SimpleKeyGenerator();
    }
	
	@Bean
	public Jackson2ObjectMapperBuilder objectMapperBuilder() {
	    Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
	    builder.dateFormat(new ISO8601DateFormat());
	    return builder;
	}
	
	@Bean
    public TaskScheduler taskScheduler() {
//        return new ConcurrentTaskScheduler();//single thread
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(10);
        scheduler.setWaitForTasksToCompleteOnShutdown(true);
        return scheduler;
    }

    @Bean
    public Executor taskExecutor() {
        return new SimpleAsyncTaskExecutor();
    }
}
