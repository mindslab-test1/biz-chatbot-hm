package ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ai.mindslab.bizchatbot.chatbotcontainer.filters.ChatbotOrderedFilter;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRespInterfaceRelData;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.chatbot.service.ChatbotService;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.outif.ThirdPartyApiManager;
import ai.mindslab.bizchatbot.commons.session.DialogService;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.session.data.UtterReconfirmData;
import ai.mindslab.bizchatbot.commons.utils.ApplicationContextProvider;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;

public class SdsRichContentFilter extends ChatbotOrderedFilter{

	private Logger logger = LoggerFactory.getLogger(SdsRichContentFilter.class);
			
	private ThirdPartyApiManager apiManager;
	private DialogService dialogService;
	private UtterData utter;
	
	public SdsRichContentFilter() {
	}
	
	public SdsRichContentFilter(int order) {
		super(order);
	}

	@Override
	public boolean execBody(DialogSession ses, ChatbotConfig config) throws BizChatbotException {
		
		apiManager =  (ThirdPartyApiManager)ApplicationContextProvider.getApplicationContext().getBean(ThirdPartyApiManager.class);
		dialogService = (DialogService)ApplicationContextProvider.getApplicationContext().getBean(DialogService.class);
		
		ChatbotRespInterfaceRelData respRel = null;
		ChatbotApiInfo apiInfo = null;
		String resp = "";
		try {
			utter = DialogUtils.getLastSystemUtter(ses);
			String sysResp = utter.getSentence();
			respRel = apiManager.getChatbotRespInterfaceRel(ses.getDialog().getBotId(), sysResp);
			if(respRel != null) {
				//remove syscode
				//sysResp = respRel.getRespMsg()+DialogUtils.removeSystemCode(sysResp);
//				sysResp = respRel.getRespMsg();//MOBIS db에는 slot 값이 들어있지 않음. 사용x
				sysResp = DialogUtils.removeSystemCode(sysResp);
				utter.setSentence(sysResp);
				
				if(IChatbotConst.RICH_CONTENT_TYPE_BUTTON.equals(respRel.getRespTypeCd())) {
					return doButtons(ses, respRel);
				}else if(IChatbotConst.RICH_CONTENT_TYPE_IMAGE.equals(respRel.getRespTypeCd())) {
					return doImage(ses, respRel);
				}else if(IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL.equals(respRel.getRespTypeCd())) {
					return doCarousel(ses, respRel);
				}else {
					throw new BizChatbotException("Invalid Rich Content Type:"+respRel.getRespTypeCd());
				}
			}
			
		}catch(Exception e) {
			logger.warn("execute/{}/{}/{}",ses.getDialog().getBotId(), ses.getSessionId(), e.getMessage());
			DialogUtils.setSessionError(ses, "SdsRichContentFilter", e);
		}finally {
			try {
				UtterData lastSysUtter = DialogUtils.getLastSystemUtter(ses);
				String isEnded = lastSysUtter.getDebug().getIsDialogEnd();
				if(!StringUtils.isEmpty(isEnded) && "1".equals(isEnded)) {
					DialogUtils.resetProcessingData(ses);
					//QQQ 화재전환?
					String msg = dialogService.getChatbotMsgs(ses.getDialog().getBotId(), IChatbotConst.MSG_TYPE_SDS_RECONFIRM);
					if(!StringUtils.isEmpty(msg)) {
						UtterReconfirmData reconf = new UtterReconfirmData();
						reconf.setMsg(msg);
						//lastSysUtter.setSentence(lastSysUtter.getSentence() + "\n" + DialogUtils.getReconfirmMsg(msg));
						lastSysUtter.setReconfirmMsg(reconf);
						ChatbotRichContent richGreeting =  config.getGreetingRichContent();
						if(richGreeting != null) {
							reconf.setRichContent(richGreeting);
						}
					}
				}
			} catch (Exception e) {
			}
		}
		
		return false;
	}
	
	private boolean doButtons(DialogSession ses, ChatbotRespInterfaceRelData respRel) throws Exception{
		ChatbotService chatbotService = (ChatbotService)ApplicationContextProvider.getApplicationContext().getBean(ChatbotService.class);
		
		ChatbotRichContent richContent = null;
		try {
			richContent = chatbotService.getRichContent(respRel.getIfId());
			utter.setRichContent(richContent);
		} catch (Exception e) {
			logger.warn("doButtons/{}/{}/{}",ses.getDialog().getBotId(), ses.getSessionId(), e.getMessage());
			throw e;
		}
		
		return false;
	}
	private boolean doImage(DialogSession ses, ChatbotRespInterfaceRelData respRel) throws Exception{
		ChatbotService chatbotService = (ChatbotService)ApplicationContextProvider.getApplicationContext().getBean(ChatbotService.class);
		
		ChatbotRichContent richContent = null;
		try {
			richContent = chatbotService.getRichContent(respRel.getIfId());
			utter.setRichContent(richContent);
		} catch (Exception e) {
			logger.warn("doImage/{}/{}/{}",ses.getDialog().getBotId(), ses.getSessionId(), e.getMessage());
			throw e;
		}
		
		return false;
	}
	
	private boolean doCarousel(DialogSession ses,ChatbotRespInterfaceRelData respRel) throws Exception{
		ChatbotService chatbotService = (ChatbotService)ApplicationContextProvider.getApplicationContext().getBean(ChatbotService.class);
		
		ChatbotRichContent richContent = null;
		try {
			richContent = chatbotService.getRichContent(respRel.getIfId());
			utter.setRichContent(richContent);
		} catch (Exception e) {
			logger.warn("doCarousel/{}/{}/{}",ses.getDialog().getBotId(), ses.getSessionId(), e.getMessage());
			throw e;
		}
		
		return false;
	}
}
