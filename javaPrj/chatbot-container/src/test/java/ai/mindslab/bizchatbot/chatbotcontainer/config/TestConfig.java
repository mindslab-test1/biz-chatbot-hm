package ai.mindslab.bizchatbot.chatbotcontainer.config;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import org.apache.http.Header;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicHeader;
import org.apache.poi.ss.usermodel.DateUtil;
import org.junit.Before;
import org.junit.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotButton;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.data.RespDialog;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.DateUtils;
import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;


@TestConfiguration
@ComponentScan(basePackages= {"ai.mindslab.bizchatbot.common", "ai.mindslab.bizchatbot.chatbotcontainer"})
@MapperScan(basePackages={"ai.mindslab.bizchatbot.commons.chatbot.dao"})
public class TestConfig {

	private static boolean toFile = true;
//	private static File out = new File("/Users/jaeheoncho/test/chatbot.log");
	private static FileWriter fw;
	
	static {
		try {
			File out = new File("/Users/jaeheoncho/temp/chatbot.log");
			fw = new FileWriter(out);
		}catch(Exception e) {
//			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	private static ObjectMapper mapper = null;
	
	private static Header[] headers;
	static{
		ArrayList<Header> h = new ArrayList<Header>();
		h.add(new BasicHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType()));
		h.add(new BasicHeader("Authentication", "Bearer aaa"));
		headers = h.toArray(new Header[h.size()]);
		
		mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
		mapper.setTimeZone(TimeZone.getTimeZone("GMT+09:00"));
	}
	
//	public static String url = "http://13.209.63.182:8080/front-api/dialog/";
	public static String url = "http://192.168.1.18:8080/front-api/dialog/";
//	public static String url = "http://localhost:8080/front-api/dialog/";
//	public static String url = "http://52.79.244.235:8080/front-api/dialog/";
	
	
	public static String openJson = "{\n" + 
			"	\"chatbotId\": \"__chatbot_id__\",\n" + 
			"	\"accessChannel\": \"WEB_BROWSER\",\n" + 
			"	\"user\": {\n" + 
			"		\"userName\": \"tester\",\n" + 
			"		\"userId\": \"junit\"\n" + 
			"	},\n" + 
			"	\"showDebug\": true\n" + 
			"}";
	
	public static String sendJson = "{\n" + 
			"	\"speaker\": \"USER\",\n" + 
			"	\"userId\": \"1\",\n" + 
			"	\"sessionId\": \"__session_id__\",\n" + 
			"	\"timestamp\": \"__timestamp__\",\n" + 
			"	\"sentence\": \"__utter__\"\n" + 
			"}";
	
	public static String closeJson = "{\n" + 
			"	\"chatbotId\": \"__chatbot_id__\",\n" + 
			"	\"accessChannel\": \"WEB_BROWSER\",\n" + 
			"	\"sessionId\": \"__session_id__\",\n" + 
			"	\"user\": {\n" + 
			"		\"userName\": \"tester\",\n" + 
			"		\"userId\": \"junit\"\n" + 
			"	}\n" + 
			"}";
	
//	@Test
	public void test1() {
		int count = 10;
		//final String []script = {"도시목록", "서울", "인천", "성남", "서울", "인천", "성남", "서울", "인천", "성남", "서울", "인천", "성남"};
		//final String []script = {"반갑습니다","age","여","area","시작하기","사이버대학","부산 거주자만 입학?","처음","모집요강","신입학","지원요건","검정고시","처음","장학금/대출","장학금 종류","이중수혜","학자금대출","처음","학사안내","수강및수업","수강신청학점","처음"};
		//"반갑습니다 신(편)입생 시작하기 사이버대학? (x)
//		final String []script = {"반갑습니다", "재학생", "시작하기", "사이버대학?", "정식 4년제 대학?", "처음", "모집요강", "신입학", "지원요건", "검정고시", "처음", "모집요강", "편입학", "선발기준", "처음", "모집요강", "편입학", "필요서류", "처음", "모집요강", "편입학", "학점인정", "처음", "학사안내", "학과소개", "처음", "학사안내", "성적평가", "1.시험실시및유형", "처음", "장학금/대출", "장학금 종류", "처음",  "장학금/대출", "이중수혜", "처음"};
//		final String []script = {"반갑습니다",
//				"신(편)입생",
//				"시작하기",
//				"★상담예약하기★",
//				"처음으로",
//				"예약진행하기",
//				"동의함",
//				"tel",
//				"subject",
//				"time",
//				"저녁 7시~9시",
//				"처음",
//				"사이버대학?",
//				"정식 4년제 대학?",
//				"처음",
//				"모집요강",
//				"신입학",
//				"지원요건",
//				"외국인",
//				"처음",
//				"모집요강",
//				"편입학",
//				"지원자격",
//				"처음",
//				"모집요강",
//				"입학전형",
//				"전형 종류",
//				"처음",
//				"모집요강",
//				"등록금",
//				"입학전형료",
//				"처음",
//				"모집요강",
//				"자주하는 질문",
//				"신입학 관련",
//				"이중등록",
//				"처음",
//				"학사안내",
//				"학과소개",
//				"처음",
//				"학사안내",
//				"성적평가",
//				"1.시험실시및유형",
//				"처음",
//				"학사안내",
//				"수강및수업",
//				"수강신청학점",
//				"처음",
//				"학사안내",
//				"졸업",
//				"학위취득",
//				"처음",
//				"장학금/대출",
//				"장학금종류"};
		//sds only script
//		final String []script = {"신(편)입생",
//				"시작하기",
//				"★상담예약하기★",
//				"처음으로",
//				"예약진행하기",
//				"동의함",
//				"tel",
//				"subject",
//				"time",
//				"처음",
//				"사이버대학?",
//				"처음",
//				"모집요강",
//				"신입학",
//				"지원요건",
//				"처음",
//				"모집요강",
//				"편입학",
//				"처음",
//				"모집요강",
//				"입학전형",
//				"처음",
//				"모집요강",
//				"등록금",
//				"처음",
//				"모집요강",
//				"자주하는 질문",
//				"신입학 관련",
//				"처음",
//				"학사안내",
//				"학과소개",
//				"처음",
//				"학사안내",
//				"성적평가",
//				"처음",
//				"학사안내",
//				"수강및수업",
//				"처음",
//				"학사안내",
//				"졸업",
//				"처음",
//				"장학금/대출",
//				"장학금종류"};
		final String []script = {
				"피자주문",
				"어떤피자 있어요?",
				"페퍼로니 피자",
				"라지 사이즈",
				"오리지널 도우요",
				"네",
				"콜라",
				"카드요",
				"날씨 알아보려고요.",
				"날씨조회",
				"어떤도시?",
				"날씨조회",
				"어떤도시?",
				"서울",
				"날씨조회",
				"어떤도시?",
				"인천",
				"날씨조회",
				"어떤도시?",
				"대전",
				"날씨조회",
				"어떤도시?",
				"대구",
				"날씨조회",
				"어떤도시?",
				"부산",
				"날씨조회",
				"어떤도시?",
				"성남",
				"날씨조회",
				"어떤도시?",
				"서울",
				"피자주문",
				"페퍼로니피자 라지사이즈 오리지널 도우요",
				"네",
				"어떤음료",
				"콜라요",
				"현금"
		};
//		final String chatbotId = "31";
		final String chatbotId = "37";
		Thread t[] = new Thread[count];
		long st = System.currentTimeMillis();
		for(int i=0; i< count;i++) {
			t[i] = new Thread() {
				
				public void run() {
					
					
					try {
						UtterData open = open(chatbotId);
						String sessionId = open.getSessionId();
						for(String msg:script) {
							UtterData send = send(sessionId, msg);
						}
						close(sessionId,chatbotId);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			t[i].start();
		}
		
		for(int i=0;i<count;i++) {
			try {
				t[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		long tot = (script.length + 1) * count;
		long elapsed = System.currentTimeMillis() - st;
		
		System.out.println("Number of user: " +count);
		System.out.println("tot request: "+ tot);
		System.out.println("total elapsed: "+elapsed + "(msec)");
		System.out.println("avg elapsed: "+(elapsed / tot)+"(msec)");
		System.out.println("tps: " +((float)tot/(float)elapsed*1000f));
		
		
	}
	
	
	
	@Test
	public void testContinious() {
		int iter_count = 100;
		for(int i=0;i<iter_count;i++) {
			test1();
		}
	}
	
	private static BaseResponse<RespDialog> fromJsonOpenRespUtter(String jsonString) {
		
		String l_jsonString = jsonString;
		if(l_jsonString == null){
			l_jsonString = "{}";
		}
		BaseResponse<RespDialog> out = null;
		try {
			out = mapper.readValue(jsonString, new TypeReference<BaseResponse<RespDialog>>() {});
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
		}
		
		return out;
	}
	
	private static BaseResponse<UtterData> fromJsonRespUtter(String jsonString) {
		
		String l_jsonString = jsonString;
		if(l_jsonString == null){
			l_jsonString = "{}";
		}
		BaseResponse<UtterData> out = null;
		try {
			out = mapper.readValue(jsonString, new TypeReference<BaseResponse<UtterData>>() {});
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
		}
		
		return out;
	}
	
	private static UtterData open(String chatbotId) throws Exception{
		String msg = openJson.replaceAll("__chatbot_id__", chatbotId);
//		ArrayList<Header> h = new ArrayList<Header>();
//		h.add(new BasicHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType()));
		try {
			String resp = HttpConnectionManager.getInstance().post(url+"openDialog", msg, headers);
			BaseResponse<RespDialog> utter = fromJsonOpenRespUtter(resp);
			printUtter(utter.getData().getUttrData());
			return utter.getData().getUttrData();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	
	private static UtterData send(String sessionId, String text) throws Exception{
		System.out.println("user: "+ text);
		ISO8601DateFormat fmt = new ISO8601DateFormat();
		String msg = sendJson.replaceAll("__session_id__", sessionId);
		msg = msg.replaceAll("__timestamp__", fmt.format(DateUtils.getNow()));
		msg = msg.replaceAll("__utter__", text);
//		ArrayList<Header> h = new ArrayList<Header>();
//		h.add(new BasicHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType()));
		try {
			String resp = HttpConnectionManager.getInstance().post(url+"sendText", msg, headers);
			BaseResponse<UtterData> utter = fromJsonRespUtter(resp);
			if(utter.getCode() != 0) {
				//printToFile("code:"+utter.getCode()+", msg:"+utter.getMsg());
				printErr(utter);
			}
			printUtter(utter.getData());
			return utter.getData();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	private static void close(String sessionId, String chatbotId) throws Exception{
		
		String msg = closeJson.replaceAll("__session_id__", sessionId);
		msg = msg.replaceAll("__chatbot_id__", chatbotId);
		
//		ArrayList<Header> h = new ArrayList<Header>();
//		h.add(new BasicHeader("Content-Type", ContentType.APPLICATION_JSON.getMimeType()));
		try {
			String resp = HttpConnectionManager.getInstance().post(url+"closeDialog", msg, headers);
			printToFile("close resp:"+resp);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	private static void printUtter(UtterData data) {
		if(data == null) {
			return;
		}
		
		printToFile("system: "+data.getSentence());
		if(data.getRichContent() != null) {
			for(ChatbotButton btn: data.getRichContent().getButtons()) {
				printToFile("\t["+btn.getTitle()+"]");
			}
//			System.out.println(data.getRichContent().toString());
		}
//		if(data.getDebug()!=null) {
//			System.out.println(data.getDebug());
//		}
	}
	
	private static void printErr(BaseResponse<UtterData> utter) {
		Date now = DateUtils.getNow();
		printToFile(format(now)+"\tcode:"+utter.getCode()+", msg:"+utter.getMsg());
		printToFile(utter.toString());
	}
	
	private static void printToFile(String msg) {
		System.out.println(msg);
		if(toFile) {
			try {
				fw.write(msg+"\n");
				fw.flush();
			} catch (IOException e) {
			}
		}
	}
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	private static String format(Date d) {
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		return sdf.format(d);
	}
}
