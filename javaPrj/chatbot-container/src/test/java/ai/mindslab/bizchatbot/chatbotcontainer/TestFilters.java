package ai.mindslab.bizchatbot.chatbotcontainer;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import ai.mindslab.bizchatbot.chatbotcontainer.config.TestConfig;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.FilterManager;
import ai.mindslab.bizchatbot.chatbotcontainer.filters.chatbotfilter.InterfaceFilter;
import ai.mindslab.bizchatbot.chatbotcontainer.service.ContainerBotService;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.DIALOG_SPEAKER;
import ai.mindslab.bizchatbot.commons.session.SessionService;
import ai.mindslab.bizchatbot.commons.session.data.DialogData;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.UserData;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.DateUtils;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestFilters.class})
@Import(TestConfig.class)
public class TestFilters {


//	@Autowired
//	private ContainerBotService botService;
//	@Autowired
//	private SessionService sesService;
//	@Mock
//	private HttpServletRequest request;
	
	@Test
	public void testFilter() {
		System.out.println("====================================");
		System.out.println("testFilter");
		System.out.println("====================================");
		
		int botId = 1;
		try {
			ChatbotConfig botConfig = new ChatbotConfig();
			botConfig.setBotId(botId);
			botConfig.setConfId(1);
			
			UserData user = new UserData();
			user.setUserId("user-test");
			user.setUserName("testuser");
			DialogSession ses = new DialogSession();
			ses.setUserData(user);
			DialogData dialog = new DialogData();
			dialog.setBotId(botId);
			ses.setDialog(dialog);
			
			UtterData utter = new UtterData();
			utter.setDialogSeq(1);
			utter.setSpeaker(DIALOG_SPEAKER.CHATBOT);
			utter.setTimestamp(DateUtils.getNow());
			utter.setSentence("__RESPCODE_"+"TEST");
			ses.getDialog().getConversation().add(utter);
			
			
			InterfaceFilter iff = new InterfaceFilter();
			iff.execute(ses, botConfig);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
