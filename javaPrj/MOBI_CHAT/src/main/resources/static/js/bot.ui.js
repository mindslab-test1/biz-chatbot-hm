/**
 * Popup Window Resize
 */
var openWindow = function openWindow(){

    var gapHeight = window.outerHeight - window.innerHeight,
        gapWidth = window.outerWidth - window.innerWidth;
    var BaseWidth = 440 + gapWidth,
        BaseHeight = (900 <= screen.availHeight)?(900+gapHeight):screen.availHeight;

    console.log("window.screen.availHeight :", window.screen.availHeight);
    console.log("screen.height :", screen.height);
    console.log("availHeight :", screen.availHeight);
    console.log("innerHeight :", window.innerHeight);
    console.log("outerHeight :", window.outerHeight);
    console.log("BaseHeight :", BaseHeight);
    //window.resizeBy(0, changeHeight)
    window.resizeTo(BaseWidth, BaseHeight);
}
var resizeWindow = function resizeWindow(size) {
    window.console && console.log("resizeWindow start...");

    var beforeWinScale = $(document).find("body").attr("data-scale");

    var gapWidth = window.outerWidth - window.innerWidth,
        gapHeight = window.outerHeight - window.innerHeight;

    var minWidth = 1024 + gapWidth,
        minHeight = 768 + gapHeight,
        maxWidth = (1600 <= screen.availWidth)?(1600):screen.availWidth,
        maxHeight = (900 <= screen.availHeight)?(900+gapHeight):screen.availHeight,
        baseWidth = 440 + gapWidth,
        baseHeight = window.outerHeight;

    var w = baseWidth,
        h = baseHeight;

    var scale = (size == "open" || size == "")?beforeWinScale:size;

    if(size){
        switch (scale) {
            case "max":
                w = maxWidth;
                h = maxHeight;
                break;
            default :
                w = minWidth;
                h = minHeight;
                break;
        }
    }

    $(document).find("body").attr('data-scale', scale)
    window.console && console.log("resizeWindow type:", size, " w:", w, " h:", h);

    window.resizeTo(w, h);
    window.focus();
}
/**
 * Layer Popup Open
 */
var popupOpen = function popupOpen(popup) {
    var popupId = popup;
    $(popupId).show().stop().animate({
        'opacity': '1'
    });
}
/**
 * Layer Popup Close
 */
var popupClose = function popupClose(popup) {
    var popupId = popup;
    $(popupId).stop().animate({
        'opacity': '0'
    }, 300, function () {
        $(popupId).attr('style', '').removeClass('full-popup').empty();
    });
}
/**
 * tooltip
 */
var tooltip = $(document).on("mouseenter","[data-tooltip]",function () {
    //console.log($(this).data("tooltip"));
    var positonData = $(this).data("tooltip");
    var myData = positonData.my,
        atData = positonData.at,
        arrowData = positonData.arrow;
    $(this).tooltip({
        position: {
            my: myData,
            at: atData,
            using: function( position, feedback ) {
                //console.log("position : "+position);
                //console.log("feedback : "+feedback.vertical+"/"+feedback.horizontal+"/"+feedback.important);
                $( this ).css( position );
                $( "<div>" )
                    .addClass( "arrow" )
                    .addClass( arrowData )
                    .appendTo( this );
                console.log($( this ));
            }
        }
    });
});

var showPreloader = function () {
    $('.preloader').show().stop().animate({
        "opacity":'1'
    },300);
}
var hidePreloader = function () {
    $('.preloader').stop().animate({
        "opacity":'0'
    },300,function(){
        $(this).hide();
    });
}