/**
 * 봇 API URLS
 */
var BOT_API = {
    BOT: "/MOBI_CHAT/bot",
    BOT_LOGIN: "/MOBI_CHAT/bot/login",
    BOT_GREETING_VIEW: "/MOBI_CHAT/bot/greetingView",
    BOT_QUESTION_VIEW: "/MOBI_CHAT/bot/questionView",
    BOT_CLOSE: "/MOBI_CHAT/bot/close",
    BOT_PINGPONG_VIEW: "/MOBI_CHAT/bot/pingpongView",
    BOT_QUESTION_MRC_VIEW: "/MOBI_CHAT/bot/questionMrcView",
    BOT_LIKE_SAVE: "/MOBI_CHAT/bot/saveLike",

    BOT_CONVERSATION_HISTORY_VIEW: "/MOBI_CHAT/modal/conversationHistoryView",
    BOT_OPINION_VIEW: "/MOBI_CHAT/modal/opinionView",
    BOT_OPINION_SAVE: "/MOBI_CHAT/user/opinion/save",
    BOT_HELP_VIEW: "/MOBI_CHAT/modal/helpView",
    BOT_STTING_VIEW: "/MOBI_CHAT/modal/settingView",
    BOT_MODIFY_STTING: "/MOBI_CHAT/user/setting/modify",
    BOT_SHUTTLE_BUS_VIEW: "/MOBI_CHAT/modal/modalShuttleBusView",
    BOT_CTAG_RPA: "/MOBI_CHAT/bot/ctagRpa",

    SIS_VIEW: "/MOBI_CHAT/search/integrate/rootView",
    SIS_MAIN_VIEW: "/MOBI_CHAT/search/integrate/mainView",
    SIS_LIST_VIEW: "/MOBI_CHAT/search/integrate/listView",
    SIS_AUTOCOMPLETE_LIST: "/MOBI_CHAT/search/integrate/autocomplete",
    SIS_POPULER_LIST: "/MOBI_CHAT/search/integrate/populer/list",
    SIS_PAGE_VIEW: "/MOBI_CHAT/modal/search/pageView",
    SIS_SIMILAR_VIEW: "/MOBI_CHAT/modal/search/similarView",
    SIS_SAME_VIEW: "/MOBI_CHAT/modal/search/sameView",
    SIS_FAVE_DOCUMENT_VIEW: "/MOBI_CHAT/modal/faveDocumentView",
    SIS_FAVE_DOCUMENT_LIST: "/MOBI_CHAT/search/integrate/fave/list",
    SIS_FAVE_DOCUMENT_ADD: "/MOBI_CHAT/search/integrate/fave/add",
    SIS_FAVE_DOCUMENT_REMOVE: "/MOBI_CHAT/search/integrate/fave/remove",
    SIS_CLICK_LOG: "/MOBI_CHAT/search/integrate/clicklog",
    SIS_MRC_PAGE_VIEW: "/MOBI_CHAT/modal/search/mrcPageView",
    SIS_UNKNOWN_MRC_PAGE_VIEW: "/MOBI_CHAT/modal/search/searchUnknownMrcPageView",
    SIS_SECURITY_AGREE_VIEW: "/MOBI_CHAT/modal/search/securityAgreeView",
    SIS_SECURITY_AGREE_SAVE: "/MOBI_CHAT/search/integrate/security/agreement",

    SEARCH_TERMS_LIST_VIEW: "/MOBI_CHAT/search/term/listView",
    SEARCH_TERMS_ITEM_VIEW: "/MOBI_CHAT/search/term/itemView",

    ERROR_UNKNOWN: "/MOBI_CHAT/page/error/unknown",
    ERROR_AUTHENTICATION: "/MOBI_CHAT/page/error/authentication",
    ERROR_SESSION_EXPIRATION: "/MOBI_CHAT/page/error/session_expiration",
}

/**
 * ajax
 *   ( Content-Type : application/json; charset=utf-8 )
 */
var getJSON = function (url) {
    return $.ajax({url: url, type: 'GET', contentType: 'application/json; charset=utf-8'});
};
var postJSON = function (url, data) {
    return $.ajax({url: url, data: JSON.stringify(data), type: 'POST', contentType: 'application/json; charset=utf-8'});
};

/**
 * object type check
 */
var U = {
    isEmpty: function (item) {
        if (item) {
            return false;
        } else {
            return true;
        }
    }
}

/**
 * Page Cursor Scroll Controller
 */
var UScroll = {
    bottom: function ($DomId) {
        $DomId.animate({scrollTop: $DomId.prop('scrollHeight')}, 500);
    }
}

/**
 * Cookie get put
 */
var UCookie = {
    get: function (key) {
        if (typeof key === 'undefined') {
            window.console && console.log('UCookie get key undefined');
            return null;
        }
        if (typeof key !== 'string' || key === "") {
            window.console && console.log('UCookie get key is null');
            return null;
        }

        var value = $.cookie(key);
        if (typeof value === 'undefined') {
            window.console && console.log('UCookie get value undefined');
            return null;
        }

        if (typeof value === 'number' || typeof value === 'string' || typeof value === 'boolean') {
            return atob(value);
        } else if (typeof value === 'object') {
            return value;
        }
        return JSON.parse(atob(value));
    },
    put: function (key, value) {
        if (typeof key === 'undefined') {
            window.console && console.log('UCookie put key undefined');
            return;
        }
        if (typeof value === 'undefined') {
            window.console && console.log('UCookie put value undefined');
            return;
        }

        if (typeof value === 'number' || typeof value === 'string' || typeof value === 'boolean') {
            $.cookie(key, value);
        } else if (typeof value === 'object') {
            $.cookie(key, btoa(JSON.stringify(value)));
        } else {
            window.console && console.log('UCookie put value not type (number, string, boolean, object)');
        }
    }
}

/**
 * Local Storage get put
 */
var ULocalStorage = {
    get: function (key) {
        if (typeof key === 'undefined') {
            window.console && console.log('ULocalStorage get key undefined');
            return null;
        }
        if (typeof key !== 'string' || key === "") {
            window.console && console.log('ULocalStorage get key is null');
            return null;
        }

        var value = localStorage.getItem(key);
        if (typeof value === 'undefined') {
            window.console && console.log('ULocalStorage get value undefined');
            return null;
        }

        try {
            return JSON.parse(value);
        } catch(e) {
            window.console && console.log('ULocalStorage get JSON.parse exception:',e);
            return value;
        }
    },
    put: function (key, value) {
        if (typeof key === 'undefined') {
            window.console && console.log('ULocalStorage put key undefined');
            return;
        }
        if (typeof key !== 'string' || key === "") {
            window.console && console.log('ULocalStorage put key is null');
            return null;
        }
        if (typeof value === 'undefined') {
            window.console && console.log('ULocalStorage put value undefined');
            return;
        }
        var item = localStorage.getItem(key);
        if (item) {
            // window.console && console.log('ULocalStorage put key removeItem');
            localStorage.removeItem(key);
        }

        if (typeof value === 'number' || typeof value === 'string' || typeof value === 'boolean') {
            localStorage.setItem(key, value);
        } else if (typeof value === 'object') {
            // window.console && console.log('ULocalStorage put value: ', value);
            localStorage.setItem(key, JSON.stringify(value));
        } else {
            window.console && console.log('ULocalStorage put value not type (number, string, boolean, object)');
        }
    }
}

var parseURL = function parseURL(url) {
    var parser = document.createElement('a');
    parser.href = url;
    // var searchObject = {};
    // var queries, split, i;

    // Convert query string to object
    // queries = parser.search.replace(/^\?/, '').split('&');
    // for( i = 0; i < queries.length; i++ ) {
    //     split = queries[i].split('=');
    //     searchObject[split[0]] = split[1];
    // }

    // return {
    //     protocol: parser.protocol,
    //     host: parser.host,
    //     hostname: parser.hostname,
    //     port: parser.port,
    //     pathname: parser.pathname,
    //     search: parser.search,
    //     searchObject: searchObject,
    //     hash: parser.hash
    // };
    var _origin = parser.protocol + "//" + parser.host;
    var _pathname = parser.pathname;
    var _search = parser.search;
    if (_pathname && _pathname.charAt(0) != "/") {
        _pathname = "/" + _pathname;
    }
    if (_search && _search.charAt(0) != "?") {
        _search = "?" + _search;
    }

    return {
        origin : _origin,
        pathname: _pathname,
        search: _search
    };
}
