/**
 * 검색 봇
 */

// 검색 문서 Popup Menu
var CONST_POP_MENU_DOCUMENT = "DOCUMENT";
var CONST_POP_MENU_PAGE = "PAGE";
var CONST_POP_MENU_SIMILAR = "SIMILAR";
var CONST_POP_MENU_SAME = "SAME";
var CONST_POP_MENU_FAVE = "FAVE";
var CONST_POP_MENU_FAVE_REMOVE = "FAVE_REMOVE";
var CONST_POP_MENU_COPY_CLIPBOARD = "COPY_CLIPBOARD";


var GIStabs = {
    tabIndex: 0,
    activeTabIndex: 0,
    sTabs: null,
    sTabsBody: null,

    clear: function clear() {
        this.tabIndex = 0;
        this.activeTabIndex = 0;
        sTabs = null;
        sTabsBody = null;
    },
    init: function init() {
        this.tabIndex = 1;
        this.activeTabIndex = this.tabIndex;
        this.sTabs = new Map();
        this.sTabsBody = new Map();

        var tabHeader = {};
        tabHeader.title = '지능검색내용' + this.tabIndex;
        this.sTabs.set(this.tabIndex, tabHeader);
        this.sTabsBody.set(this.tabIndex, null);
    },
    activeTab: function activeTab(tabIndex) {
        if (typeof tabIndex !== 'number') {
            tabIndex = Number(tabIndex);
        }
        this.activeTabIndex = tabIndex;
        return this.getTab(tabIndex);
    },
    newTab: function newTab() {
        this.tabIndex++;
        this.activeTabIndex = this.tabIndex;

        var tabHeader = {};
        tabHeader.title = '지능검색내용' + this.tabIndex;
        this.sTabs.set(this.tabIndex, tabHeader);
        this.sTabsBody.set(this.tabIndex, null);
        return this.getTab(this.tabIndex);
    },
    putTab: function putTab(tabIndex, tabHeader, tabBodyDataNode) {
        if (U.isEmpty(tabIndex) || U.isEmpty(tabHeader)) {
            window.console && console.log("GIStabs.putTab data is null");
            return;
        }
        if (typeof tabIndex !== 'number') {
            tabIndex = Number(tabIndex);
        }
        this.sTabs.set(tabIndex, tabHeader);
        this.sTabsBody.set(tabIndex, tabBodyDataNode);
    },
    putTabBody: function putTabBody(tabIndex, tabTitle, tabBodyDataNode) {
        if (U.isEmpty(tabIndex) || U.isEmpty(tabTitle)) {
            window.console && console.log("GIStabs.putTabBody data is null");
            return;
        }
        if (typeof tabIndex !== 'number') {
            tabIndex = Number(tabIndex);
        }
        var tabHeader = {};
        tabHeader.title = tabTitle;
        this.putTab(tabIndex, tabHeader, tabBodyDataNode);
    },
    removeTab: function removeTab(tabIndex) {
        if (typeof tabIndex !== 'number') {
            tabIndex = Number(tabIndex);
        }
        var autoTabIndex = this.autoActiveTabIndex(tabIndex);
        this.sTabs.delete(tabIndex);
        this.sTabsBody.delete(tabIndex);
        return autoTabIndex;
    },
    getActiveTab: function getActiveTab() {
        if (U.isEmpty(this.activeTabIndex)) {
            window.console && console.log("GIStabs.getActiveTab return is null");
            return null;
        }
        return this.getTab(this.activeTabIndex);
    },
    getTab: function getTab(tabIndex) {
        if (typeof tabIndex !== 'number') {
            tabIndex = Number(tabIndex);
        }

        var tab = {};
        tab.tabIndex = tabIndex;
        tab.tabButtonId = 'btn_tab_' + tabIndex;
        tab.tabId = 'tab_' + tabIndex;
        tab.tabInputId = 'input_tab_' + tabIndex;
        tab.tabPagination = 'search_pagination_' + tabIndex;
        tab.tabPaginationData = 'search_pagination_data_' + tabIndex;
        tab.tabSize = this.sTabs.size;
        tab.tabHeader = this.sTabs.get(tabIndex);
        tab.tabBodyDataNode = this.sTabsBody.get(tabIndex);
        return tab;
    },
    autoActiveTabIndex: function autoActiveTabIndex(tabIndex) {
        var autoTabIndex = 0;
        var smallIndex = 0;
        var bigIndex = 0;

        if (this.activeTabIndex == tabIndex) {
            for (var i = tabIndex - 1; i > 0; i--) {
                if (this.sTabs.has(i)) {
                    smallIndex = i;
                    break;
                }
            }
            for (var i = tabIndex + 1; i <= 5; i++) {
                if (this.sTabs.has(i)) {
                    bigIndex = i;
                    break;
                }
            }
            window.console && console.log("GIStabs.autoActiveTabIndex smallIndex: ", smallIndex, "  bigIndex: ", bigIndex);
            if (smallIndex > 0) {
                autoTabIndex = smallIndex;
            } else if (bigIndex > 0) {
                autoTabIndex = bigIndex;
            } else {
                for (var i = 1; i <= 5; i++) {
                    if (this.sTabs.has(i)) {
                        autoTabIndex = i;
                        break;
                    }
                }
            }
            window.console && console.log("GIStabs.autoActiveTabIndex autoTabIndex: ", autoTabIndex);
        }
        return autoTabIndex;
    }
}


var CONST_QUERY_DEPTH_ZERO = 0;  // 키워드
var CONST_QUERY_DEPTH_ONE = 1;   // 키워드,추천키워드
var CONST_QUERY_DEPTH_TWO = 2;   // 키워드,추천키워드,정렬,내팀/내문서
var CONST_QUERY_DEPTH_THREE = 3; // 키워드,추천키워드,정렬,내팀/내문서, 카테고리
var CONST_QUERY_DEPTH_FOUR = 4;  // 키워드,추천키워드,정렬,내팀/내문서, 카테고리, 상세검색
var CONST_QUERY_DEPTH_FIVE = 5;  // 키워드,추천키워드,정렬,내팀/내문서, 카테고리, 상세검색, page
/**
 * 통합검색 Parameter Model
 */
var GISquery = {
    put: function put(query) {
        ULocalStorage.put("GISquery", query);
    },
    putQ: function putQ(q) {
        this.putQQQQ(CONST_QUERY_DEPTH_ZERO, q, null, null, "", "", null, "", "", "", null, "", "", null, "", "");
    },
    putQQ: function putQQ(q, addq) {
        this.putQQQQ(CONST_QUERY_DEPTH_ONE, q, addq, null, "", "", null, "", "", "", null, "", "", null, "", "");
    },
    putQQQ: function putQQQ(depth, q, addq, sources, myDeptDoc, myDoc, groupIds, ownerName, from, to, extensions, fq, projectName, xdcCategorys, offset, sort) {
        this.putQQQQ(depth, q, addq, sources, myDeptDoc, myDoc, groupIds, ownerName, from, to, extensions, fq, projectName, xdcCategorys, offset, sort);
    },
    putQQQQ: function putQQQQ(depth, q, addq, sources, myDeptDoc, myDoc, groupIds, ownerName, from, to, extensions, fq, projectName, xdcCategorys, offset, sort) {
        var query = {};
        query.depth = depth;                  // (int)    검색 단계(0.키워드, 1.추천키워드, 2.정렬, 팀/My, 3.카테고리 검색, 4.상세검색, 5.page)
        query.q = q;                          // (string) 검색 키워드
        query.addq = addq;                    // (array)  추천키워드로 AND 검색하는 키워드
        query.sources = sources;              // (array)  검색 소스
        query.sessionId = "";                 // (string) 서버에서 Session SET
        query.userId = "";                    // (string) 서버에서 Session SET
        query.userDeptCode = "";              // (string) 서버에서 Session SET
        query.userDivCode = "";               // (string) 서버에서 Session SET
        query.myDeptDoc = myDeptDoc;          // (string) My 부서코드 (필터)
        query.myDoc = myDoc;                  // (string) My 소유자 ID (필터)
        query.groupId = groupIds;             // (array)  그룹 ID
        query.ownerName = ownerName;          // (string) 문서 소유자 이름
        query.from = from;                    // (string) 시작일자 yyyy-MM-dd
        query.to = to;                        // (string) 종료일자 yyyy-MM-dd
        query.extension = extensions;         // (array)  확장자 Array Type
        query.fq = fq;                        // (string) 재검색 할 때 이전 검색 키워드
        query.projectName = projectName       // (string) 프로젝트 검색(있으면 Integrity만 검색)
        query.xdcCategory = xdcCategorys      // (array)  문서유형
        query.offset = offset;                // (int)    페이징 시작 offset
        query.size = 10;                      // (int)    출력 개수
        query.sort = sort;                    // (string) default.정확도순, recent.최근 수정된 문서로 정렬
        query.viewTabIndex = 0;               // (string) View activeTab.tabIndex
        query.viewTabId = "";                 // (string) View activeTab.tabId
        query.viewOwningType = 0;             // (int)    소유자(팀/MY) 0.OWNING_FILTER_ALL | 1.OWNING_FILTER_TEAM | 2.OWNING_FILTER_MY
        query.viewSortType = 0;               // (int)    정렬 0.SORT_DEFAULT | 1.SORT_ACCURACY | 2.SORT_LATEST
        query.viewOptionFixed = 0;            // (int)    상세검색 고정타입 0.close, 1.fixed
        query.viewStartPage = 1;              // (int)    Pagination Start Page
        this.put(query);
    },
    addView: function addView(tabIndex, tabId, owningType, sortType, optionFixed, startPage) {
        var query = this.get();
        query.viewTabIndex = tabIndex;
        query.viewTabId = tabId;
        query.viewOwningType = owningType;
        query.viewSortType = sortType;
        query.viewOptionFixed = optionFixed;
        query.viewStartPage = startPage;
        this.put(query);
    },
    get: function get() {
        var query = ULocalStorage.get("GISquery");
        return query;
    }
}

/**
 * 최초 검색화면 초기화
 */
var openSisView = function () {
    window.console && console.log("openSisView  Start");

    getJSON(BOT_API.SIS_VIEW)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.SIS_VIEW responce success ");
            // window.console && console.log("BOT_API.SIS_VIEW domHtml: ", domHtml);
            appendSearchResultView(domHtml);
        })
        .fail(function () {
            window.console && console.log("BOT_API.SIS_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}

/**
 * 검색 메인화면 요청
 */
var openSisMainView = function (activeTab) {
    window.console && console.log("openSisMainView  Start");

    var viewTabsNode = {};
    viewTabsNode.viewTabIndex = activeTab.tabIndex;
    viewTabsNode.viewTabId = activeTab.tabId;

    postJSON(BOT_API.SIS_MAIN_VIEW, viewTabsNode)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.SIS_MAIN_VIEW responce success ");
            // window.console && console.log("BOT_API.SIS_MAIN_VIEW domHtml: ", domHtml);

            var workingTab = GIStabs.getTab(viewTabsNode.viewTabIndex);
            window.console && console.log("BOT_API.SIS_MAIN_VIEW workingTab: ", workingTab.tabIndex);

            $("#" + workingTab.tabId).find(".row-group").remove();
            $("#" + workingTab.tabId).append(domHtml);
        })
        .fail(function () {
            window.console && console.log("BOT_API.SIS_MAIN_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}

/**
 * TAB 검색 요청
 */
var openSisListView = function (gISquery) {
    window.console && console.log("openSisListView  Start");
    window.console && console.log("openSisListView  gISquery:", gISquery);

    templateLoadingView();
    scrollBottomBotMessages();

    postJSON(BOT_API.SIS_LIST_VIEW, gISquery)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.SIS_LIST_VIEW responce success");
            // window.console && console.log("BOT_API.SIS_LIST_VIEW domHtml: ", domHtml);
            removeTemplateLoadingView();

            var workingTabs = GIStabs.getTab(gISquery.viewTabIndex);
            $("#" + workingTabs.tabId).find(".row-group").remove();
            $("#" + workingTabs.tabId).append(domHtml);
        })
        .fail(function () {
            window.console && console.log("BOT_API.SIS_LIST_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}

/**
 * 검색 문서미리보기
 */
var documentPreview = function (objectItem) {
    window.console && console.log("documentPreview  start...");
    window.console && console.log("documentPreview  objectItem:", objectItem);

    if (U.isEmpty(objectItem.objectUrl)) {
        return;
    }
    if (objectItem.sourceSystemType == "MCLOUD") {
        window.open(objectItem.objectUrl, "_blank");
    } else if (objectItem.sourceSystemType == "APPROVE") {
        window.open(objectItem.objectUrl, "_blank");
    } else if (objectItem.sourceSystemType == "INTEGRITY") {
        window.open(objectItem.objectUrl, "_blank");
    } else if (objectItem.sourceSystemType == "WIKI") {
        if (!U.isEmpty(objectItem.wikiSID)) {
            objectItem.objectUrl = objectItem.objectUrl + "&" + "sid=" + encodeURIComponent(objectItem.wikiSID);
        }
        window.open(objectItem.objectUrl, "_blank");
    } else if (objectItem.sourceSystemType == "QMS") {
        window.open(objectItem.objectUrl, "_blank");
    }
}

/**
 * 검색 페이지별 보기
 */
var modalSisPageView = function (objectItem) {
    window.console && console.log("modalSisPageView  Start");
    // window.console && console.log("modalSisPageView  objectItem: ", JSON.stringify(objectItem));

    var params = {};
    params.objectId = objectItem.objectId;
    params.q = objectItem.q;

    postJSON(BOT_API.SIS_PAGE_VIEW, params)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.SIS_PAGE_VIEW responce success ");
            // window.console && console.log("BOT_API.SIS_MAIN_VIEW domHtml: ", domHtml);

            $("#searchPopup").empty();
            $("#searchPopup").show().animate({
                "opacity": "1"
            }, 300).html(domHtml);
        })
        .fail(function () {
            window.console && console.log("BOT_API.SIS_PAGE_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}

/**
 * 검색 유사문서
 */
var modalSisSimilarView = function (objectItem) {
    window.console && console.log("modalSisSimilarView  Start");
    window.console && console.log("modalSisSimilarView  objectItem: ", objectItem);

    var params = {};
    params.dedupe = objectItem.objectDedupe;
    params.objectId = objectItem.objectId;
    params.sources = objectItem.sourceSystemType;

    postJSON(BOT_API.SIS_SIMILAR_VIEW, params)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.SIS_SIMILAR_VIEW responce success ");
            // window.console && console.log("BOT_API.SIS_SIMILAR_VIEW domHtml: ", domHtml);

            $("#searchPopup").empty();
            $("#searchPopup").show().animate({
                "opacity": "1"
            }, 300).html(domHtml);
        })
        .fail(function () {
            window.console && console.log("BOT_API.SIS_SIMILAR_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}

/**
 * 검색 중복문서
 */
var modalSisSameView = function (objectItem) {
    window.console && console.log("modalSisSameView  Start");
    window.console && console.log("modalSisSameView  objectItem: ", objectItem);

    var params = {};
    params.dedupe = objectItem.objectDedupe;
    params.objectId = objectItem.objectId;

    postJSON(BOT_API.SIS_SAME_VIEW, params)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.SIS_SAME_VIEW responce success ");
            // window.console && console.log("BOT_API.SIS_SAME_VIEW domHtml: ", domHtml);

            $("#searchPopup").empty();
            $("#searchPopup").show().animate({
                "opacity": "1"
            }, 300).html(domHtml);
        })
        .fail(function () {
            window.console && console.log("BOT_API.SIS_SAME_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}

/**
 * 즐겨찾기 문서 오픈
 */
var modalFaveDocument = function () {
    window.console && console.log("modalFaveDocument start...");
    clearSearchPopMenuView();

    getJSON(BOT_API.SIS_FAVE_DOCUMENT_VIEW)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.SIS_FAVE_DOCUMENT_VIEW responce success ");
            // window.console && console.log("BOT_API.SIS_FAVE_DOCUMENT_VIEW domHtml: ", domHtml);
            $("#searchPopup").html(domHtml);
            $('#searchPopup').show().stop().animate({
                'opacity': '1'
            });
        })
        .fail(function () {
            window.console && console.log("BOT_API.SIS_FAVE_DOCUMENT_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}

/**
 * 즐겨찾기 저장
 */
var saveFaveDocument = function (objectItem) {
    window.console && console.log("saveFaveDocuments start...");
    window.console && console.log("saveFaveDocuments objectItem:", objectItem);

    var params = {};
    params.objectId = objectItem.objectId;

    postJSON(BOT_API.SIS_FAVE_DOCUMENT_ADD, params)
        .done(function (responce) {
            window.console && console.log("BOT_API.SIS_FAVE_DOCUMENT_ADD responce success");
            // window.console && console.log("BOT_API.SIS_FAVE_DOCUMENT_ADD responce: ", responce);
        })
        .fail(function () {
            window.console && console.log("BOT_API.SIS_FAVE_DOCUMENT_ADD responce error ");
        });
}

/**
 * 즐겨찾기 삭제
 */
var removeFaveDocument = function (objectItem) {
    window.console && console.log("removeFaveDocument start...");
    window.console && console.log("removeFaveDocument objectItem:", objectItem);

    var params = {};
    params.objectId = objectItem.objectId;

    postJSON(BOT_API.SIS_FAVE_DOCUMENT_REMOVE, params)
        .done(function (responce) {
            window.console && console.log("BOT_API.SIS_FAVE_DOCUMENT_REMOVE responce success");
            window.console && console.log("BOT_API.SIS_FAVE_DOCUMENT_REMOVE responce: ", responce);
            if (objectItem.callback) {
                objectItem.callback(true, objectItem.objectId);
            }
        })
        .fail(function () {
            window.console && console.log("BOT_API.SIS_FAVE_DOCUMENT_REMOVE responce error ");
            if (objectItem.callback) {
                objectItem.callback(false, objectItem.objectId);
            }
        });
}

/**
 * ClickLog 저장
 */
var saveClickLog = function (objectItem) {
    window.console && console.log("saveClickLog start...");
    window.console && console.log("saveClickLog objectItem:", objectItem);

    var params = {};
    params.rObjectId = objectItem.objectId;
    params.orderNo = objectItem.objectOrder;
    params.keyword = objectItem.q;

    postJSON(BOT_API.SIS_CLICK_LOG, params)
        .done(function (responce) {
            window.console && console.log("BOT_API.SIS_CLICK_LOG responce success");
            // window.console && console.log("BOT_API.SIS_CLICK_LOG responce: ", responce);
        })
        .fail(function () {
            window.console && console.log("BOT_API.SIS_CLICK_LOG responce error ");
        });
}

/**
 * MRC Data Parser
 */
// var mrcDataParser = function (tabBodyDataNode) {
//     window.console && console.log("mrcDataParser start...");
//     window.console && console.log("mrcDataParser tabBodyDataNode:", tabBodyDataNode);
//
//     var passageArray = [];
//
//     var loopCount = tabBodyDataNode.length > 10 ? 10 : tabBodyDataNode.length;
//     window.console && console.log("mrcDataParser loopCount:", loopCount);
//     // 상위 20건 문서만 추출대상이며 문서 Max 10개만 passages만 MRC로 전달한다.
//     for (var i = 0; i < loopCount; i++) {
//         // 문서 Max 10개만
//         if (i > 9) {
//             return passageArray;
//         }
//
//         var itemNode = tabBodyDataNode[i];
//         // 소스 시스템 타입
//         var sourceSystemType = itemNode._source && itemNode._source.source_system && itemNode._source.source_system.toUpperCase();
//         if (U.isEmpty(sourceSystemType)) {
//             sourceSystemType = 'null';
//         }
//         // 문서ID
//         var objectId = itemNode._source && itemNode._source.r_object_id;
//         if (U.isEmpty(objectId)) {
//             objectId = '';
//         }
//         // 문서명
//         var objectName = itemNode._source && itemNode._source.r_object_name;
//         if (U.isEmpty(objectName)) {
//             objectName = '';
//         }
//         // 소유자
//         var sourceOwnerName = itemNode._source && itemNode._source.owner_info && itemNode._source.owner_info.user_nm;
//         if (U.isEmpty(sourceOwnerName)) {
//             sourceOwnerName = '';
//         }
//         // 소유자 부서
//         var sourceDeptName = itemNode._source && itemNode._source.owner_info && itemNode._source.owner_info.dept_nm;
//         if (U.isEmpty(sourceDeptName)) {
//             sourceDeptName = '';
//         }
//         // 문서 생성/수정일시
//         var sourceCreationDateTime = itemNode._source.creation_date;
//         var sourceModifyDateTime = itemNode._source.modify_date;
//
//         var hitsArray = itemNode.inner_hits && itemNode.inner_hits.contents && itemNode.inner_hits.contents.hits && itemNode.inner_hits.contents.hits.hits;
//         if (U.isEmpty(hitsArray)) {
//             window.console && console.log("mrcDataParser inner_hits is null  for->continue ");
//             continue;
//         }
//         window.console && console.log("mrcDataParser hitsArray:", hitsArray);
//         var loopPageCount = hitsArray.length > 3 ? 3 : hitsArray.length;
//         for (var j = 0; j < loopPageCount; j++) {
//             var hitsNode = hitsArray[j];
//
//             var pageNumber = hitsNode._source && hitsNode._source.page_number;
//             if (U.isEmpty(pageNumber)) {
//                 pageNumber = "";
//             }
//             var pageContents = hitsNode._source && hitsNode._source.content;
//             if (U.isEmpty(pageContents)) {
//                 pageContents = "";
//             }
//
//             if (!U.isEmpty(pageContents)) {
//                 var passageObj = {};
//                 passageObj.objectOrder = i;
//                 passageObj.objectId = objectId;
//                 passageObj.objectPageNumber = pageNumber;
//                 passageObj.sourceSystemType = sourceSystemType;
//                 passageObj.objectName = objectName;
//                 passageObj.ownerUserName = sourceOwnerName;
//                 passageObj.ownerDeptName = sourceDeptName;
//                 passageObj.creationDate = sourceCreationDateTime;
//                 passageObj.modifyDate = sourceModifyDateTime;
//                 passageObj.passage = pageContents;
//                 window.console && console.log("mrcDataParser passageObj:", passageObj);
//                 passageArray.push(passageObj);
//             }
//         }
//     }
//     return passageArray;
// }
var mrcDataParser = function (tabBodyDataNode) {
    window.console && console.log("mrcDataParser start...");
    window.console && console.log("mrcDataParser tabBodyDataNode:", tabBodyDataNode);

    var objectIds = [];

    var loopCount = tabBodyDataNode.length > 10 ? 10 : tabBodyDataNode.length;
    window.console && console.log("mrcDataParser loopCount:", loopCount);
    // 상위 20건 문서만 추출대상이며 문서 Max 10개만 passages만 MRC로 전달한다.
    for (var i = 0; i < loopCount; i++) {
        var itemNode = tabBodyDataNode[i];
        // 문서ID
        var objectId = itemNode._source && itemNode._source.r_object_id;
        if (U.isEmpty(objectId)) {
            objectId = '';
        }
        if (!U.isEmpty(objectId)) {
            objectIds.push(objectId);
        }
    }
    return objectIds;
}
var searchMrcList = function (message) {
    window.console && console.log("searchMrcList start...");

    var activeTab = GIStabs.getActiveTab();
    window.console && console.log("searchMrcList activeTab:", activeTab);
    if (U.isEmpty(activeTab) || U.isEmpty(activeTab.tabBodyDataNode)) {
        window.console && console.log("searchMrcList activeTab.tabBodyDataNode is null ");
        templateWaringView("검색 결과가 없습니다.");
        scrollBottomBotMessages();
        return;
    }

    var objectIds = mrcDataParser(activeTab.tabBodyDataNode);
    window.console && console.log("searchMrcList objectIds:", objectIds);
    if (U.isEmpty(objectIds) || objectIds.length <= 0) {
        window.console && console.log("searchMrcList objectIds is null ");
        templateWaringView("검색 결과 페이지 내용이 없습니다.");
        scrollBottomBotMessages();
        return;
    }

    var params = {};
    params.question = message;
    params.objectIds = objectIds;

    // 입력 질문 봇에 추가
    templateInputView(message);
    templateLoadingView();
    scrollBottomBotMessages();
    clearBotInputMessage();

    postJSON(BOT_API.SIS_MRC_PAGE_VIEW, params)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.SIS_MRC_PAGE_VIEW responce success");
            // window.console && console.log("BOT_API.SIS_MRC_PAGE_VIEW domHtml: ", domHtml);
            removeTemplateLoadingView();

            $("#searchPopup").empty();
            $("#searchPopup").show().animate({
                "opacity": "1"
            }, 300).html(domHtml);
        })
        .fail(function () {
            window.console && console.log("BOT_API.SIS_MRC_PAGE_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}

/**
 * 봇에서 문서명 버튼 클릭시 MRC 결과 팝업화면
 */
var unknownMrcList = function (mrcNode) {
    window.console && console.log("unknownMrcList start...");

    if (U.isEmpty(mrcNode)) {
        return;
    }

    postJSON(BOT_API.SIS_UNKNOWN_MRC_PAGE_VIEW, mrcNode)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.SIS_UNKNOWN_MRC_PAGE_VIEW responce success");
            // window.console && console.log("BOT_API.SIS_UNKNOWN_MRC_PAGE_VIEW domHtml: ", domHtml);

            $("#bodyPopup").empty();
            $("#bodyPopup").html(domHtml);
            popupOpen('#bodyPopup');
        })
        .fail(function () {
            window.console && console.log("BOT_API.SIS_UNKNOWN_MRC_PAGE_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}