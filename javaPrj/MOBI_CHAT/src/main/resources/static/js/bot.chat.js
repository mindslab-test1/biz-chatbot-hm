/**
 * 챗봇 (질문 전송, 결과 반영)
 */

var CONST_MAIBOT_TYPE = "BK59";
var CONST_BUSINESSBOT_TYPE = "BK56";
var CONST_SEARCHBOT_TYPE = "BK62";
var CONST_PINGPONGBOT_TYPE = "BK99";

var GMaiBot = {
    put: function put(gMaibot) {
        ULocalStorage.put("GMaiBot", gMaibot);
    },
    putOpen: function putOpen(botType) {
        var gMaiBot = {};
        gMaiBot.botType = botType;
        this.put(gMaiBot);
    },
    putSend: function putSend(sentence) {
        var gMaiBot = this.get();
        if (gMaiBot === "undefined" || gMaiBot === null) {
            window.console && console.log('GMaiBot putSend MaiBot is null');
            return;
        }
        if (typeof gMaiBot !== 'object') {
            window.console && console.log('GMaiBot putSend MaiBot is not object');
            return;
        }
        gMaiBot.sentence = sentence;
        this.put(gMaiBot);
    },
    getBotId: function getBotId() {
        var gMaiBot = this.get();
        // window.console && console.log('GMaiBot get gMaiBot: ', gMaiBot);
        if (U.isEmpty(gMaiBot.botType)) {
            return CONST_MAIBOT_TYPE;
        }
        return gMaiBot.botType.substring(2);
    },
    get: function get() {
        var gMaiBot = ULocalStorage.get("GMaiBot");
        // window.console && console.log('GMaiBot get gMaiBot: ', gMaiBot);
        return gMaiBot;
    }
}

/**
 * 봇 최초 진입시 start
 */
var startBot = function (botType) {
    window.console && console.log('startBot start...');
    window.console && console.log('startBot botType: ', botType);

    allClearView();

    var gMaiBot = GMaiBot.get();
    if (gMaiBot && gMaiBot.botType && gMaiBot.botType !== botType) {
        window.console && console.log('startBot botType change closeDialog');
        GIStabs.clear();
        botClose(function(succeed, messages){
            window.console && console.log('startBot callback start...');
            if (succeed) {
                window.console && console.log('startBot callback success!!!');
            } else {
                window.console && console.log('startBot callback failed: ', messages);
            }
            GMaiBot.putOpen(botType);
            botGreeting();
        });
    } else {
        GMaiBot.putOpen(botType);
        botGreeting();
    }
}

var botGreeting = function () {
    window.console && console.log("botGreeting  Start");

    // Bot Messages Area Clear
    clearBotMessagesView();

    if (GMaiBot.get().botType === CONST_PINGPONGBOT_TYPE) {
        var $p_message = $("<p></p>").html("재미봇은 챗봇 스타트업 핑퐁테크놀로지의 기술로 제공되고 있습니다.<br/>바쁜 업무중에 재미있는 대화로 잠깐 머리를 식혀보세요.");
        var $div_comment_box = $("<div class='comment-box'></div>").html($p_message);
        templateDomElementView($div_comment_box);
        // speechBubble(); 재미봇 접속시 말풍선 미노출로 인한 주석처리
    } else {
        templateLoadingView();
        postJSON(BOT_API.BOT_GREETING_VIEW, GMaiBot.get())
            .done(function (domHtml) {
                window.console && console.log("BOT_API.BOT_GREETING_VIEW responce success");
                // window.console && console.log("BOT_API.BOT_GREETING_VIEW domHtml: ", domHtml);
                removeTemplateLoadingView();

                appendBotMessagesView(domHtml);
                scrollBottomBotMessages();
                speechBubble();

                if (GMaiBot.get().botType === CONST_SEARCHBOT_TYPE) {
                    openSisView();
                }
            })
            .fail(function() {
                window.console && console.log("BOT_API.BOT_GREETING_VIEW responce error ");
                removeTemplateLoadingView();
                templateWaringView("서버 연동 원할하지 않습니다. ");
                scrollBottomBotMessages();
            });
    }
}

var botQuestion = function (message) {
    window.console && console.log("botQuestion  Start");

    clearSearchPopMenuView();
    if (U.isEmpty(message)) {
        window.console && console.log("botQuestion  message is null");
        return;
    }

    var msgType = message.substring(0, 1);
    if (GMaiBot.get().botType === CONST_SEARCHBOT_TYPE) {
        if (msgType === "#") { // 용어 검색
            setConverSationTopN(GMaiBot.get().botType, message);
            searchTermList(message.substring(1));
        } else if (msgType === "@") {  // MRC 검색
            setConverSationTopN(GMaiBot.get().botType, message);
            searchMrcList(message.substring(1));
        } else {
            var $p_message = $("<p></p>").html("지능 봇에서는 <b class='color-point'>#용어검색, @인공지능검색</b>만 가능합니다.");
            var $div_comment_box = $("<div class='comment-box'></div>").html($p_message);
            templateDomElementView($div_comment_box);
            scrollBottomBotMessages();
        }
    } else if (GMaiBot.get().botType === CONST_PINGPONGBOT_TYPE) {
        setConverSationTopN(GMaiBot.get().botType, message);
        pingpongQuestion(message);
    } else if (GMaiBot.get().botType === CONST_BUSINESSBOT_TYPE) {
        if (msgType === "@") {  // MRC 검색
            setConverSationTopN(GMaiBot.get().botType, message);
            generalMrcList(message.substring(1));
        } else {
            setConverSationTopN(GMaiBot.get().botType, message);
            GMaiBot.putSend(message);
            // 입력 질문 봇에 추가
            templateInputView(message);
            scrollBottomBotMessages();
            bbotQuestion();
        }
    } else {
        setConverSationTopN(GMaiBot.get().botType, message);
        GMaiBot.putSend(message);
        // 입력 질문 봇에 추가
        templateInputView(message);
        scrollBottomBotMessages();
        bbotQuestion();
    }
}

var bbotQuestion = function () {
    templateLoadingView();
    scrollBottomBotMessages();
    clearBotInputMessage();

    postJSON(BOT_API.BOT_QUESTION_VIEW, GMaiBot.get())
        .done(function (domHtml) {
            window.console && console.log("BOT_API.BOT_QUESTION_VIEW responce success");
            // window.console && console.log("BOT_API.BOT_QUESTION_VIEW domHtml: ", domHtml);
            removeTemplateLoadingView();

            appendBotMessagesView(domHtml);
            scrollBottomBotMessages();
        })
        .fail(function() {
            window.console && console.log("BOT_API.BOT_QUESTION_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}
var botClose = function (callback) {
    window.console && console.log("botClose start...");
    postJSON(BOT_API.BOT_CLOSE, GMaiBot.get())
        .done(function (responce) {
            window.console && console.log("BOT_API.BOT_CLOSE responce success");
            window.console && console.log("BOT_API.BOT_CLOSE responce: ", responce);
            if (callback) {
                callback(true);
            }
        })
        .fail(function() {
            window.console && console.log("BOT_API.BOT_CLOSE responce error ");
            if (callback) {
                callback(false, "봇 종료중 오류가 발생했습니다.");
            }
        });
}
/**
 * MRC 요청
 */
var generalMrcList = function (message) {
    window.console && console.log("generalMrcList start...");

    GMaiBot.putSend(message);

    // 입력 질문 봇에 추가
    templateInputView(message);
    templateLoadingView();
    scrollBottomBotMessages();
    clearBotInputMessage();

    postJSON(BOT_API.BOT_QUESTION_MRC_VIEW, GMaiBot.get())
        .done(function (domHtml) {
            window.console && console.log("BOT_API.BOT_QUESTION_MRC_VIEW responce success");
            // window.console && console.log("BOT_API.BOT_QUESTION_MRC_VIEW domHtml: ", domHtml);
            removeTemplateLoadingView();

            appendBotMessagesView(domHtml);
            scrollBottomBotMessages();
        })
        .fail(function() {
            window.console && console.log("BOT_API.BOT_QUESTION_MRC_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}

/**
 * 봇 핑봉
 */
var pingpongQuestion = function (message) {
    GMaiBot.putSend(message);

    // 입력 질문 봇에 추가
    templateInputView(message);
    scrollBottomBotMessages();
    templateLoadingView();
    scrollBottomBotMessages();
    clearBotInputMessage();

    postJSON(BOT_API.BOT_PINGPONG_VIEW, GMaiBot.get())
        .done(function (domHtml) {
            window.console && console.log("BOT_API.BOT_PINGPONG_VIEW responce success");
            // window.console && console.log("BOT_API.BOT_PINGPONG_VIEW domHtml: ", domHtml);
            removeTemplateLoadingView();

            appendBotMessagesView(domHtml);
            scrollBottomBotMessages();
        })
        .fail(function() {
            window.console && console.log("BOT_API.BOT_PINGPONG_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}

/**
 * 말풍선
 */
var speechBubble = function () {
    if (GMaiBot.get().botType === CONST_SEARCHBOT_TYPE) {
        var $p_message = $("<p></p>").html("지능 봇에서는 <b class='color-point'>#용어검색, @인공지능검색</b>만 가능합니다.<br/>검색창에서는 <b>검색자</b>를 이용한 상세 검색이 가능합니다.<br/>");
        $p_message.append('▶ "키워드" :  필수 포함<br/>');
        $p_message.append('▶  - : 단어 제외<br/>');
        $p_message.append('▶ AND, and : 교집합<br/>');
        $p_message.append('▶ OR, or : 합집합<br/>');
        $p_message.append('▶ () : 검색 우선적용');
        var $div_comment_box = $("<div class='comment-box'></div>").html($p_message);
        templateDomElementView($div_comment_box);
        scrollBottomBotMessages();
    } else if (GMaiBot.get().botType === CONST_PINGPONGBOT_TYPE) {
        var $p_message = $("<p></p>").html("사람과 대화하듯 친근하고 흥미로운 대화를 나눌 수 있어요.<br/>");
        $p_message.append('▶ 안녕? 반가워.<br/>');
        $p_message.append('▶ 넌 무엇을 할 수 있어?');
        var $div_comment_box = $("<div class='comment-box'></div>").html($p_message);
        templateDomElementView($div_comment_box);
        scrollBottomBotMessages();
    } else {
        GMaiBot.putSend("기능안내");
        bbotQuestion();
    }
}

/**
 * 용어 검색
 */
var searchTermList = function (message) {
    window.console && console.log("searchTermList  Start");

    var params = {};
    params.keywords = message;
    // 입력 질문 봇에 추가
    templateInputView(message);
    templateLoadingView();
    scrollBottomBotMessages();
    clearBotInputMessage();

    postJSON(BOT_API.SEARCH_TERMS_LIST_VIEW, params)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.SEARCH_TERMS_LIST_VIEW responce success");
            // window.console && console.log("BOT_API.SEARCH_TERMS_LIST_VIEW domHtml: ", domHtml);
            removeTemplateLoadingView();

            appendBotMessagesView(domHtml);
            scrollBottomBotMessages();
        })
        .fail(function() {
            window.console && console.log("BOT_API.SEARCH_TERMS_LIST_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
            scrollBottomBotMessages();
        });
}

/**
 * 용어 아이템 검색
 */
var searchTermItemView = function (itemNode) {
    window.console && console.log("searchTermItemView  Start");
    window.console && console.log("searchTermItemView  itemNode: ", itemNode);

    var params = {};
    params.korTitle = itemNode.korTitle;
    params.trmsId = itemNode.docId;

    templateLoadingView();
    scrollBottomBotMessages();

    postJSON(BOT_API.SEARCH_TERMS_ITEM_VIEW, params)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.SEARCH_TERMS_ITEM_VIEW responce success");
            // window.console && console.log("BOT_API.SEARCH_TERMS_ITEM_VIEW domHtml: ", domHtml);
            removeTemplateLoadingView();

            appendBotMessagesView(domHtml);
            scrollBottomBotMessages();
        })
        .fail(function() {
            window.console && console.log("BOT_API.SEARCH_TERMS_ITEM_VIEW responce error ");
            removeTemplateLoadingView();
            templateWaringView("서버 연동 원할하지 않습니다. ");
        });
}

/**
 * 봇 대화목록 오픈
 */
var openConversationHistoryView = function () {
    window.console && console.log("openConversationHistoryView start...");
    clearSearchPopMenuView();

    getJSON(BOT_API.BOT_CONVERSATION_HISTORY_VIEW)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.BOT_CONVERSATION_HISTORY_VIEW responce success ");
            // window.console && console.log("BOT_API.BOT_CONVERSATION_HISTORY_VIEW domHtml: ", domHtml);
            $("#bodyPopup").html(domHtml);
            drawConversation_History();
        })
        .fail(function() {
            window.console && console.log("BOT_API.BOT_CONVERSATION_HISTORY_VIEW responce error ");
        });
}

/**
 * 봇 의견등록 오픈
 */
var openOpinionView = function () {
    window.console && console.log("openOpinionView start...");
    clearSearchPopMenuView();

    getJSON(BOT_API.BOT_OPINION_VIEW)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.BOT_OPINION_VIEW responce success ");
            // window.console && console.log("BOT_API.BOT_OPINION_VIEW domHtml: ", domHtml);
            $("#bodyPopup").html(domHtml);
        })
        .fail(function() {
            window.console && console.log("BOT_API.BOT_OPINION_VIEW responce error ");
        });
}

/**
 * 봇 도움말 오픈
 */
var openHelpView = function () {
    window.console && console.log("openHelpView start...");
    clearSearchPopMenuView();

    getJSON(BOT_API.BOT_HELP_VIEW)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.BOT_HELP_VIEW responce success ");
            // window.console && console.log("BOT_API.BOT_HELP_VIEW domHtml: ", domHtml);
            $("#bodyPopup").html(domHtml);
        })
        .fail(function() {
            window.console && console.log("BOT_API.BOT_HELP_VIEW responce error ");
        });
}

/**
 * 봇 설정화면 오픈
 */
var openStting = function () {
    window.console && console.log("openStting start...");
    clearSearchPopMenuView();

    getJSON(BOT_API.BOT_STTING_VIEW)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.BOT_STTING_VIEW responce success ");
            // window.console && console.log("BOT_API.BOT_STTING_VIEW domHtml: ", domHtml);
            $("#bodyPopup").html(domHtml);
        })
        .fail(function() {
            window.console && console.log("BOT_API.BOT_STTING_VIEW responce error ");
        });
}

/**
 * 봇 설정정보 수정
 */
var updateStting = function (botSetting) {
    window.console && console.log("updateStting start...");

    postJSON(BOT_API.BOT_MODIFY_STTING, botSetting)
        .done(function (responce) {
            window.console && console.log("BOT_API.BOT_MODIFY_STTING responce success");
            // window.console && console.log("BOT_API.BOT_MODIFY_STTING responce: ", responce);
            window.location.href = BOT_API.BOT;
        })
        .fail(function() {
            window.console && console.log("BOT_API.BOT_MODIFY_STTING responce error ");
            window.location.href = BOT_API.BOT;
        });
}

/**
 * 봇 좋아요 저장
 */
var saveBotLike = function (params) {
    window.console && console.log("saveBotLike start...");
    window.console && console.log("saveBotLike params:", params);

    postJSON(BOT_API.BOT_LIKE_SAVE, params)
        .done(function (responce) {
            window.console && console.log("BOT_API.BOT_LIKE_SAVE responce success");
            // window.console && console.log("BOT_API.BOT_LIKE_SAVE responce: ", responce);
        })
        .fail(function() {
            window.console && console.log("BOT_API.BOT_LIKE_SAVE responce error ");
        });
}

/**
 * modal 셔틀버스 View
 */
var openShuttleBusView = function (shuttleBusNode) {
    window.console && console.log("openShuttleBusView start...");
    window.console && console.log("openShuttleBusView shuttleBusNode:", shuttleBusNode);

    postJSON(BOT_API.BOT_SHUTTLE_BUS_VIEW, shuttleBusNode)
        .done(function (domHtml) {
            window.console && console.log("BOT_API.BOT_SHUTTLE_BUS_VIEW responce success ");
            // window.console && console.log("BOT_API.BOT_SHUTTLE_BUS_VIEW domHtml: ", domHtml);
            $("#bot_search_results").html(domHtml);
            $('body').attr('data-search','open');
            resizeWindow('max');
        })
        .fail(function() {
            window.console && console.log("BOT_API.BOT_SHUTTLE_BUS_VIEW responce error ");
        });
}