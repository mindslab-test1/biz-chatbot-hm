var Stack = {
    stack_array: [],
    pop: function () {
        if (this.stack_array.length > 0) {
            this.stack_array.shift();
        }
    },

    init: function () {
        this.stack_array = [];
    },

    push: function (value) {
        var data = {
            time: '',
            message: ''
        };
        if (duplicatedWords(this.stack_array, value)) {
            return false;
        } else {
            if (this.stack_array.length == 10) {
                this.pop();
                data.time = getNowTime();
                data.message = value;
                this.stack_array.push(data);
            } else {
                data.time = getNowTime();
                data.message = value;
                this.stack_array.push(data);
            }
        }
    },
    peek: function (index) {
        return this.stack_array[this.stack_array.length - (index + 1)];
    },
    length: function () {
        return this.stack_array.length;
    },

    setArray: function (data) {
        if (data != null && typeof data != 'undefined') {
            this.stack_array = data;
        }
    },
    convertJson: function () {
        if (this.stack_array.length > 0) {
            return JSON.stringify(this.stack_array);
        }
        return '{}';
    },
    getArray: function () {
        // console.log(this.stack_array);
        return this.stack_array;
    }
}

function duplicatedWords(arrayData, value) {
    var is_done = false;
    if (typeof arrayData != "undefined" && arrayData != null) {
        for (var i = 0; i < arrayData.length; i++) {
            var data = arrayData[i];
            if (data.message == value) {
                is_done = true;
                break;
            }
        }
    }
    return is_done;
}


var setConverSationTopN = function (botType, message) {
    console.log('setConverSationTopN botType, {}', botType);

    var s =  ULocalStorage.get(botType);
    if(s != null &&  typeof s != 'undefined' ){
        console.log('setConverSationTopN storage data {}', s);
        Stack.setArray(s);
        Stack.push(message);
        ULocalStorage.put(botType, Stack.getArray());
    }else{
        Stack.init();
        Stack.push(message);
        ULocalStorage.put(botType, Stack.getArray());
    }
}

var getConverSationTopN =  function(botType){
    console.log('getConverSationTopN botType, {}', botType);
    var messageArray= ULocalStorage.get(botType);
    if(messageArray != null &&  typeof messageArray != 'undefined' ){
        // Stack.setArray(messageArray);
        // for(var i = 0; i < Stack.length() ; i++){
        //     console.log("getConverSationTopN object:", Stack.peek(i));
        // }
        return messageArray;
    } else {
        return null;
    }
}

function getNowTime() {
    var d = new Date();

    var s =
        leadingZeros(d.getFullYear(), 4) + '-' +
        leadingZeros(d.getMonth() + 1, 2) + '-' +
        leadingZeros(d.getDate(), 2) + ' ' +

        leadingZeros(d.getHours(), 2) + ':' +
        leadingZeros(d.getMinutes(), 2) + ':' +
        leadingZeros(d.getSeconds(), 2);

    return s;
}

function leadingZeros(n, digits) {
    var zero = '';
    n = n.toString();

    if (n.length < digits) {
        for (i = 0; i < digits - n.length; i++)
            zero += '0';
    }
    return zero + n;
}