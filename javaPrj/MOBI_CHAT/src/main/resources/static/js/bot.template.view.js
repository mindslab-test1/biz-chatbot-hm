/**
 * Thymeleaf Template Views
 */

/******************************************************************************************
 * 여기부터 Bot Template View
 *******************************************************************************************/

var gProgress = false;

var allClearView = function () {
    allClearBotView();
    allClearSearchView();
}

/**
 * 봇 입력데이터 Display
 */
var templateInputView = function (message) {
    var botTime = $.format.date(new Date(), "HH:mm");
    var $li_row = $("\
        <li class='comment-message'>\
            <div class='comment-block'>\
                <div class='comment-box'>\
                    <p>" + message + "</p>\
                </div>\
            </div>\
            <div class='comment-time'>"+botTime+"</div>\
        </li>\
    ");
    appendBotMessagesView($li_row);
}

/**
 * 봇 Dom Element Display
 */
var templateDomElementView = function (domElement) {
    var botTime = $.format.date(new Date(), "HH:mm")
    var $div_comment = $("<div class='comment-block'></div>");
    $div_comment.html(domElement);
    $div_comment.append("<div class='comment-time'>"+botTime+"</div>");

    var $li_row = $("<li class='comment-answer'></li>").html($div_comment);
    appendBotMessagesView($li_row);
}

/**
 * 봇 Message Display
 */
var templateMessageView = function (message) {
    var $p_message = $("<p></p>").html(message);
    var $div_comment_box = $("<div class='comment-box'></div>").html($p_message);
    templateDomElementView($div_comment_box);
}

/**
 * 봇 Waring Display
 */
var templateWaringView = function (message) {
    var $p_message = $("<p></p>").html(message);
    var $div_comment_box = $("<div class='comment-box comment-waring-box'></div>").html($p_message);
    templateDomElementView($div_comment_box);
}

/**
 * 봇 loading
 */
var isGProgress = function () {
    return gProgress;
}
var templateLoadingView = function () {
    gProgress = true;
    var $div_loading = $("\
        <li class='comment-answer bot-loading'>\
            <div class='comment-block'>\
                <div class='comment-box'>\
                    <p class='loader-box'><span class='loader'></span></p>\
                </div>\
            </div>\
        </li>\
    ");
    appendBotMessagesView($div_loading);
    if (GMaiBot.get().botType === CONST_SEARCHBOT_TYPE) {
        showPreloader();
    }
}
var removeTemplateLoadingView = function () {
    $("#bot-messages-area").find(".bot-loading").remove();
    hidePreloader();
    gProgress = false;
}

/**
 * 봇 append message view
 */
var appendBotMessagesView = function (domHtml) {
    $("#bot-messages-area").append(domHtml);
}
var removeBotMessagesEvents = function removeBotMessagesEvents() {
    // 모든 이벤트 제거
    $("#bot-messages-area li").last().find('a').each(function () {
        if ($(this).is('a')) {
            // window.console && console.log("buttonClick this button yes ");
            $(this).prop("onclick", null).off("click");
        }
    });
}

var disabledLikeButtons = function (__this) {
    // 좋아요 버튼 diabled
    $(__this).parent().children().each(function () {
        if ($(this).is('button')) {
            $(this).prop("disabled", "disabled").off("click");
        }
    });
}

var clearBotMessagesView = function () {
    window.console && console.log("clearBotMessagesView start...");
    $("#bot-messages-area").empty();
}
var clearBotInputMessage = function () {
    $("#input-message").val("");
}
var allClearBotView = function () {
    clearBotMessagesView();
    clearBotInputMessage();
}

/**
 * 봇 Messages Area 마지막 라인으로 이동
 */
var scrollBottomBotMessages = function () {
    // window.console && console.log("scrollBottomBotMessages  Start");
    var $DomId = $(".chat-body");
    // UScroll.bottom($DomId);
    $DomId.mCustomScrollbar('update');
    $DomId.mCustomScrollbar("scrollTo", "bottom");
}

/**
 * 식단 정보 Popup window로 보여주기
 */
var cafeteriaMenuWindow = function (imageUrl) {
    var winName = "cafeteria-menu-window";
    window.console && console.log("cafeteriaMenuWindow imageUrl: ", imageUrl);

    var win_width = '604px';
    var win_height = '390px';

    var agent = navigator.userAgent.toLowerCase();
    if ((navigator.appName == 'Netscape' && agent.indexOf('trident') != -1) || (agent.indexOf("msie") != -1)) {
        window.open('about:blank', winName, 'width=' + win_width + ',height=' + win_height);
    } else {
        window.open('about:blank', winName, 'width=' + win_width + ',height=' + win_height);
    }
    var form = document.createElement("form");
    form.method = "POST";
    form.target = winName;
    form.action = "/MOBI_CHAT/page/cafeteriaMenuWindow";
    document.body.appendChild(form);
    var i = document.createElement("input"); // input 엘리멘트 생성
    i.type = "hidden";
    i.name = "imageUrl";
    i.value = imageUrl;
    form.appendChild(i);
    form.submit();
}

/**
 * link Popup window로 보여주기
 */
var postLinkBlank = function (url, params) {
    window.console && console.log("postLinkBlank url: ", url);
    window.console && console.log("postLinkBlank params: ", params);

    // var win_width = '1024px';
    // var win_height = '768px';
    //
    // var agent = navigator.userAgent.toLowerCase();
    // if ((navigator.appName == 'Netscape' && agent.indexOf('trident') != -1) || (agent.indexOf("msie") != -1)) {
    //     window.open('about:blank', winName, 'width=' + win_width + ',height=' + win_height);
    // } else {
    //     window.open('about:blank', winName, 'width=' + win_width + ',height=' + win_height);
    // }

    var pUrl = parseURL(url);
    window.console && console.log("postLinkBlank pUrl: ", pUrl);

    var form = document.createElement("form");
    form.method = "POST";
    form.target = "_blank";

    if (U.isEmpty(params)) {
        return;
    }
    if (typeof params === 'string') {
        params = JSON.parse(params);
    }

    form.action = pUrl.origin + pUrl.pathname;
    document.body.appendChild(form);
    Object.keys(params).map(function(pKey) {
        var pValue = params[pKey];
        window.console && console.log("postLinkBlank key:"+pKey, "|value:"+pValue);
        var i = document.createElement("input"); // input 엘리멘트 생성
        i.type = "hidden";
        i.name = pKey;
        i.value = pValue;
        form.appendChild(i);
    });
    window.console && console.log("postLinkBlank form: ", form);
    form.submit();
}

/**
 * Call Rest Api 요청 --> 결과 Display
 */
var callRestApi = function (url, paramsNode) {
    window.console && console.log("callRestApi url: ", url);
    window.console && console.log("callRestApi paramsNode: ", paramsNode);

    var pUrl = parseURL(url);
    window.console && console.log("callRestApi pUrl: ", pUrl);
    if (U.isEmpty(paramsNode)) {
        return;
    }
    if (typeof paramsNode === 'string') {
        paramsNode = JSON.parse(paramsNode);
    }

    var postRequest = {};
    postRequest.origin = pUrl.origin;
    postRequest.pathname = pUrl.pathname;
    postRequest.params = paramsNode;

    postJSON(BOT_API.BOT_CTAG_RPA, postRequest)
        .done(function (responce) {
            window.console && console.log("BOT_API.BOT_CTAG_RPA responce success");
            window.console && console.log("BOT_API.BOT_CTAG_RPA responce: ", responce);
            if (responce && responce.code == 0) {
                var message = "담당자가 온라인 상태가 되면 모비스온으로 자동 안내 됩니다. RPA Bot이 보내온 모비스온 대화창을 확인하세요.";
                templateMessageView(message);
                scrollBottomBotMessages();
            } else {
                var message = "담당자 모비스온 상태 알림 설정에 실패했습니다.";
                templateMessageView(message);
                scrollBottomBotMessages();
            }
        })
        .fail(function () {
            window.console && console.log("BOT_API.BOT_CTAG_RPA responce error ");
            var message = "담당자 모비스온 상태 알림 설정에 실패했습니다.";
            templateMessageView(message);
            scrollBottomBotMessages();
        });

    // $( __this ).prop("onclick", null).off("click");
}

/**
 * copy 클립보드
 */
var copyToClipboard = function (value) {
    var succeed;
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(value).select();
    succeed = document.execCommand("copy");
    $temp.remove();
    return succeed;
}


/******************************************************************************************
 * 여기부터 Search Template View
 *******************************************************************************************/

/**
 * 검색결과 append view
 */
var appendSearchResultView = function (domHtml) {
    $("#bot_search_results").empty();
    $("#bot_search_results").html(domHtml);
}
var allClearSearchView = function () {
    clearSearchPopMenuView();
    clearSearchResultView();
}
var clearSearchResultView = function () {
    $("#bot_search_results").empty();
}
var clearSearchPopMenuView = function () {
    $("#search_pop_menu_buttons").empty();
    $("#search_pop_menu_buttons").hide();
}

/**
 * 검색 연관키워드 봇 Display
 */
var templateSisRelatedKeywordsView = function (keyword, relatedNode) {
    window.console && console.log("templateSisRelatedKeywordsView start");
    window.console && console.log("templateSisRelatedKeywordsView keyword:", keyword, " relatedNode:",relatedNode);

    var botTime = $.format.date(new Date(), "HH:mm");

    var $li_row = $("<li class='comment-answer'></li>");
    var $div_1 = $("<div class='comment-block'></div>");
    var $div_2 = $("<div class='comment-box'></div>").html("<p><mark>"+keyword+"</mark>의 연관검색어입니다.</p>");
    $p_buttons = $("<p class='buttons buttons-default'></p>");
    $.each(relatedNode.keywords, function (index, relatedKeyword) {
        $a_link = $("<a class='btn btn-outline-primary btn-block' ></a>").html(relatedKeyword).click(function () {
            relatedKeySearch(relatedNode.tabIndex, relatedKeyword);
        });
        $p_buttons.append($a_link);
    });
    $div_2.append($p_buttons);
    $div_1.html($div_2);
    $div_1.append("<div class='comment-time'>"+botTime+"</div>");
    $li_row.html($div_1);

    appendBotMessagesView($li_row);
}
