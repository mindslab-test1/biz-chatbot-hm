package kr.co.mobis.MOBI_CHAT.bot.service;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.bot.domain.LikeEntity;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class LikeService {
    static Logger logger = LoggerFactory.getLogger(LikeService.class);

    @Value("${front.bot.url}")
    private String BOT_URL;

    @Value("${front.bot.like.save.path}")
    private String PATH_LIKE_SAVE;

    private String okHttpClient(String path, RequestBody formBody) {
        HttpUrl httpUrl = HttpUrl.parse(BOT_URL).newBuilder()
                .encodedPath(path)
                .build();
        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/x-www-form-urlencoded")
                .build();
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .postBody(formBody)
                .build();
        return botHttpClient.serverCall();
    }

    /**
     * 좋아요 저장
     */
    public LikeEntity saveLike(String userId, String botId, String currStep, String question, String answer, String countType) throws BotException {
        logger.info("saveBotLike start...");

        RequestBody postBody = new FormBody.Builder()
                .add("userId", userId)
                .add("botId", botId)
                .add("currStep", currStep)
                .add("question", question)
                .add("answer", answer)
                .add("countType", countType)
                .build();
        String responceString = okHttpClient(PATH_LIKE_SAVE, postBody);
        if (UText.isEmpty(responceString)) {
            logger.error("saveBotLike is null");
            throw new BotException("saveBotLike result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        LikeEntity.dataNodeValidationCheck(rootNode);
        return new LikeEntity(rootNode);
    }
}
