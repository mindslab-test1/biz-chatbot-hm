package kr.co.mobis.MOBI_CHAT;

import kr.co.mobis.MOBI_CHAT.bot.domain.MaiBot;
import kr.co.mobis.MOBI_CHAT.bot.service.BotService;
import kr.co.mobis.MOBI_CHAT.common.domain.BotContext;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.utils.UDate;
import kr.co.mobis.MOBI_CHAT.common.utils.UIPAddress;
import kr.co.mobis.MOBI_CHAT.user.model.BotLogin;
import kr.co.mobis.MOBI_CHAT.user.model.BotSessionInfo;
import kr.co.mobis.MOBI_CHAT.user.service.SignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class RootController {
    static Logger logger = LoggerFactory.getLogger(RootController.class);

    @Autowired
    private SignService signService;

    @Autowired
    private BotService botService;

    @GetMapping("/home/bot")
    public String home(Model model) {
        logger.info("home start...");
        return "home";
    }

    @PostMapping("/sign/login")
    public String login(HttpSession session, HttpServletRequest request, BotLogin botLogin, Model model) {
        logger.info("login start...");
        logger.info("login sessionId: " + session.getId());
        logger.info("login botLogin: " + botLogin.toString());
        String userIp = UIPAddress.getClientIp(request);

        logger.info("userIp : {} " , userIp);

        if (BotContext.ONLY_IE) {
            String header = request.getHeader("User-Agent");
            boolean ie = (header.indexOf("MSIE") > -1 || header.indexOf("Trident") > -1);
            logger.info("login ie: " + ie);
            if (!ie) {
                return "page/none_internetExplorer";
            }
        }

        BotSessionInfo botSessionInfo = null;
        try {
            botSessionInfo = signService.login(botLogin.getChannelType(), userIp,  botLogin.getUserId());
        } catch (BotException bte) {
            logger.info("login  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            return MApi.errPage(model, BotContext.ErrorStatus.ERROR_AUTHENTICATION).toPath();
        }
        if (botSessionInfo == null) {
            logger.info("login  no authenticated session information.");
            return MApi.errPage(model, BotContext.ErrorStatus.ERROR_AUTHENTICATION).toPath();
        }

        session.setAttribute("botSessionInfo", botSessionInfo);
        session.setMaxInactiveInterval(BotContext.SESSION_INACTIVE_INTERVAL_SECOND);
        return "redirect:/bot";
    }

    @PostMapping("/quit")
    public void quit(HttpSession session, @RequestBody MaiBot maiBot) {
        logger.info("quit start...");
        logger.info("quit sessionId: " + session.getId());
        logger.info("quit maiBot: " + maiBot.toString());

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("quit  no authenticated session information.");
            return;
        }
        logger.info("quit botSessionInfo:" + botSessionInfo.toString());
        session.invalidate();
        try {
            botService.close(botSessionInfo, maiBot);
        } catch (BotException bte) {
            logger.info("quit  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
        }
    }

    @GetMapping("/page/help")
    public String help(Model model) {
        logger.info("help start...");
        return "/page/help";
    }

    @PostMapping("/page/cafeteriaMenuWindow")
    public String cafeteriaMenuWindow(@RequestParam String imageUrl, Model model) {
        logger.info("cafeteriaMenuWindow start...");
        logger.info("cafeteriaMenuWindow imageUrl: " + imageUrl);

        model.addAttribute("toDay", UDate.nowBotYMD());
        model.addAttribute("imageUrl", imageUrl);
        return "/page/cafeteriaMenuWindow";
    }

    @GetMapping("/page/error/unknown")
    public String errorUnknown(Model model) {
        logger.info("errorUnknown start...");
        return "/page/error_unknown";
    }
    @GetMapping("/page/error/authentication")
    public String errorAuthentication(Model model) {
        logger.info("errorAuthentication start...");
        return "/page/error_authentication";
    }
    @GetMapping("/page/error/session_expiration")
    public String errorSessionExpiration(Model model) {
        logger.info("errorSessionExpiration start...");
        return "/page/error_session_expiration";
    }
}
