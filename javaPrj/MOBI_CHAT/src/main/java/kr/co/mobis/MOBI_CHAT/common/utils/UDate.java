package kr.co.mobis.MOBI_CHAT.common.utils;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class UDate {

	/**
     * Returns Date to milliseconds
     */
    public static Date now() {
        return new Date();
    }
    public static String nowBotTime() {
        return UDate.parseDateISO8601Local(UDate.nowDateISO8601(), DateTimeFormatter.ofPattern("HH:mm"));
    }
    public static String nowBotYMD() {
        return UDate.parseDateISO8601Local(UDate.nowDateISO8601(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    /**
     * Formats the specified date as an ISO 8601 string.
     */
    public static String nowDateISO8601() {
        return ZonedDateTime.now( ZoneOffset.UTC ).format( DateTimeFormatter.ISO_INSTANT );
    }

    /**
     * ZonedDateTime as an ISO 8601 string.
     */
    public static ZonedDateTime parseDateISO8601(String isoDate) {
        return ZonedDateTime.parse(isoDate, DateTimeFormatter.ISO_DATE_TIME);
    }
    public static String parseDateISO8601(String isoDate, DateTimeFormatter formatter) {
        return ZonedDateTime.parse(isoDate, DateTimeFormatter.ISO_DATE_TIME).format(formatter);
    }


    /**
     * UTC ZonedDateTime as an ISO 8601 string.
     */
    public static ZonedDateTime parseDateISO8601UTC(String isoDate) {
        ZonedDateTime result = parseDateISO8601(isoDate);
        return result.withZoneSameInstant(ZoneOffset.UTC);
    }
    public static ZonedDateTime parseDateISO8601Local(String isoDate) {
        ZonedDateTime result = parseDateISO8601(isoDate);
        return result.withZoneSameInstant(ZoneOffset.systemDefault());
    }
    public static String parseDateISO8601Local(String isoDate, DateTimeFormatter formatter) {
        ZonedDateTime result = parseDateISO8601(isoDate);
        return result.withZoneSameInstant(ZoneOffset.systemDefault()).format(formatter);
    }
}

