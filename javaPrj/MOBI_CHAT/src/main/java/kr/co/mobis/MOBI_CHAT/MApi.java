package kr.co.mobis.MOBI_CHAT;

import kr.co.mobis.MOBI_CHAT.common.domain.BotContext;
import kr.co.mobis.MOBI_CHAT.common.utils.UDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;

public class MApi {
    static Logger logger = LoggerFactory.getLogger(MApi.class);

    private static MApi instance;

    public static ErrorPage errPage(Model model, BotContext.ErrorStatus errorStatus) {
        if (instance == null) {
            instance = new MApi();
        }
        return instance.page(model, errorStatus);
    }
    public static ErrorView errView(Model model, BotContext.ErrorStatus errorStatus) {
        if (instance == null) {
            instance = new MApi();
        }
        return instance.view(model, errorStatus);
    }
    public static WaringView waringView(Model model) {
        if (instance == null) {
            instance = new MApi();
        }
        return instance.waring(model);
    }

    private ErrorPage page(Model model, BotContext.ErrorStatus errorStatus) {
        return new ErrorPage(model, errorStatus);
    }
    private ErrorView view(Model model, BotContext.ErrorStatus errorStatus) {
        return new ErrorView(model, errorStatus);
    }
    private WaringView waring(Model model) {
        return new WaringView(model);
    }

    public class ErrorPage {
        private Model model;
        private String pagePath = "redirect:/page/error/unknown";

        public ErrorPage(Model model, BotContext.ErrorStatus errorStatus) {
            this.model = model;
            this.model.addAttribute("errorStatus", errorStatus.name());
            if (errorStatus == BotContext.ErrorStatus.ERROR_AUTHENTICATION) {
                pagePath = "redirect:/page/error/authentication";
            } else if (errorStatus == BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION) {
                pagePath = "redirect:/page/error/session_expiration";
            } else {
                pagePath = "redirect:/page/error/unknown";
            }
        }

        public Model getModel() {
            return model;
        }

        public String toPath() {
            return pagePath;
        }
    }

    public class ErrorView {
        private Model model;
        private String pagePath = "/view/chatting/errorView";

        public ErrorView(Model model, BotContext.ErrorStatus errorStatus) {
            this.model = model;
            this.model.addAttribute("errorStatus", errorStatus.name());
        }

        public Model getModel() {
            return model;
        }

        public String toPath() {
            return pagePath;
        }
    }

    public class WaringView {
        private Model model;
        private String pagePath = "/view/chatting/waringView";

        public WaringView(Model model) {
            this.model = model;
            model.addAttribute("botTime", UDate.nowBotTime());
        }

        public Model getModel() {
            return model;
        }

        public String toPath() {
            return pagePath;
        }
    }
}
