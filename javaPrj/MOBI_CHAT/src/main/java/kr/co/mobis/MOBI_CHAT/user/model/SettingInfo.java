package kr.co.mobis.MOBI_CHAT.user.model;

public class SettingInfo {
    private String type;
    private String name;
    private String fixedYn;
    private String showYn;
    private int order;

    public SettingInfo() {}
    public SettingInfo(String type, String name, String fixedYn, String showYn, int order) {
        this.type = type;
        this.name = name;
        this.fixedYn = fixedYn;
        this.showYn = showYn;
        this.order = order;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFixedYn() {
        return fixedYn;
    }

    public void setFixedYn(String fixedYn) {
        this.fixedYn = fixedYn;
    }

    public String getShowYn() {
        return showYn;
    }

    public void setShowYn(String showYn) {
        this.showYn = showYn;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "SettingInfo{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", fixedYn='" + fixedYn + '\'' +
                ", showYn='" + showYn + '\'' +
                ", order=" + order +
                '}';
    }
}
