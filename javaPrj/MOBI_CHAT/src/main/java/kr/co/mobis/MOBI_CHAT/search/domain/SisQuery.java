package kr.co.mobis.MOBI_CHAT.search.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.user.model.BotSessionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class SisQuery {
    static Logger logger = LoggerFactory.getLogger(SisQuery.class);

    private int depth;            // 검색 단계(0.키워드, 1.추천키워드, 2.정렬, 팀/My, 카테고리 검색, 3.상세검색)
    private String q;             // 검색 키워드
    private String[] addq;        // 추천키워드로 AND 검색하는 키워드
    private String[] sources;     // 검색 소스(MCLOUD,APPROVE,WIKI,INTEGRITY,QMS)
    private String sessionId;     // bot_session_id
    private String userId;        // 사번
    private String userDeptCode;  // 사용자의 팀 코드
    private String userDivCode;   // 사용자의 본부 코드
    private boolean myDeptDoc;    // My 팀문서 여부
    private boolean myDoc;        // My 문서 여부
    private String[] groupId;     // id|name
    private String ownerName;     // 소유자명
    private String from;          // 시작 일자(yyyy-MM-dd)
    private String to;            // 종료 일자(yyyy-MM-dd)
    private String[] extension;   // 파일확장자
    private String fq;            // 재 검색할 키워드
    private String projectName;   // 프로젝트 검색(있으면 Integrity만 검색)
    private String[] xdcCategory; // 문서유형
    private int offset;           // 페이징 시작 offset
    private int size;             // 출력 개수
    private String sort;          // default.정확도순, recent.최근 수정된 문서로 정렬
    private int viewTabIndex;     // View activeTab.tabIndex
    private String viewTabId;     // View activeTab.tabId
    private int viewOwningType;   // View 소유자(팀/MY) 0.OWNING_FILTER_ALL | 1.OWNING_FILTER_TEAM | 2.OWNING_FILTER_MY
    private int viewSortType;     // View 정렬 0.SORT_DEFAULT | 1.SORT_ACCURACY | 2.SORT_LATEST
    private int viewOptionFixed;  // View 상세검색 고정타입 0.close, 1.fixed
    private int viewStartPage;    // View Pagination Start Page

    public SisQuery() {
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public String[] getAddq() {
        return addq;
    }

    public void setAddq(String[] addq) {
        this.addq = addq;
    }

    public String[] getSources() {
        return sources;
    }

    public void setSources(String[] sources) {
        this.sources = sources;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserDeptCode() {
        return userDeptCode;
    }

    public void setUserDeptCode(String userDeptCode) {
        this.userDeptCode = userDeptCode;
    }

    public String getUserDivCode() {
        return userDivCode;
    }

    public void setUserDivCode(String userDivCode) {
        this.userDivCode = userDivCode;
    }

    public boolean isMyDeptDoc() {
        return myDeptDoc;
    }

    public void setMyDeptDoc(boolean myDeptDoc) {
        this.myDeptDoc = myDeptDoc;
    }

    public boolean isMyDoc() {
        return myDoc;
    }

    public void setMyDoc(boolean myDoc) {
        this.myDoc = myDoc;
    }

    public String[] getGroupId() {
        return groupId;
    }

    public void setGroupId(String[] groupId) {
        this.groupId = groupId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String[] getExtension() {
        return extension;
    }

    public void setExtension(String[] extension) {
        this.extension = extension;
    }

    public String getFq() {
        return fq;
    }

    public void setFq(String fq) {
        this.fq = fq;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String[] getXdcCategory() {
        return xdcCategory;
    }

    public void setXdcCategory(String[] xdcCategory) {
        this.xdcCategory = xdcCategory;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public int getViewTabIndex() {
        return viewTabIndex;
    }

    public void setViewTabIndex(int viewTabIndex) {
        this.viewTabIndex = viewTabIndex;
    }

    public String getViewTabId() {
        return viewTabId;
    }

    public void setViewTabId(String viewTabId) {
        this.viewTabId = viewTabId;
    }

    public int getViewOwningType() {
        return viewOwningType;
    }

    public void setViewOwningType(int viewOwningType) {
        this.viewOwningType = viewOwningType;
    }

    public int getViewSortType() {
        return viewSortType;
    }

    public void setViewSortType(int viewSortType) {
        this.viewSortType = viewSortType;
    }

    public int getViewOptionFixed() {
        return viewOptionFixed;
    }

    public void setViewOptionFixed(int viewOptionFixed) {
        this.viewOptionFixed = viewOptionFixed;
    }

    public int getViewStartPage() {
        return viewStartPage;
    }

    public void setViewStartPage(int viewStartPage) {
        this.viewStartPage = viewStartPage;
    }

    public void setUserSessionInfo(BotSessionInfo botSessionInfo) {
        userId = botSessionInfo.getUserId();
        sessionId = botSessionInfo.getBotSessionId();
        userDeptCode = botSessionInfo.getDeptCd();
        userDivCode = botSessionInfo.getUserDivCode();
    }

    public void decodeQuery() {
        String decodeQ = charsetDecoder(StandardCharsets.UTF_8, q);
        logger.info("decodeQuery  decodeQ:" + decodeQ);
        if (!UText.isEmpty(decodeQ)) {
            q = decodeQ;
        }
        String decodeFQ = charsetDecoder(StandardCharsets.UTF_8, fq);
        logger.info("decodeQuery  decodeFQ:" + decodeFQ);
        if (!UText.isEmpty(decodeFQ)) {
            fq = decodeFQ;
        }
    }
    private String charsetEncoder(Charset charset, String value) {
        try {
            ByteBuffer bytes = charset.newEncoder().encode(CharBuffer.wrap(value.toCharArray()));
            return new String(bytes.array());
        } catch (CharacterCodingException e) {
            return null;
        }
    }
    private String charsetDecoder(Charset charset, String value) {
        return charset.decode(ByteBuffer.wrap(value.getBytes(charset))).toString();
    }

    public ObjectNode toQueryJSON() {
        ObjectNode queryNode = new ObjectMapper().createObjectNode();
        queryNode.put("q", q);
        queryNode.put("addq", UText.commaString(addq));
        queryNode.put("sources", UText.commaString(sources));
        queryNode.put("sessionId", sessionId == null ? "" : sessionId);
        queryNode.put("userId", userId == null ? "" : userId);
        queryNode.put("userDeptCode", userDeptCode == null ? "" : userDeptCode);
        queryNode.put("userDivCode", userDivCode == null ? "" : userDivCode);
        queryNode.put("myDeptDoc", myDeptDoc);
        queryNode.put("myDoc", myDoc);
        queryNode.put("groupId", UText.commaString(groupId));
        queryNode.put("ownerName", ownerName == null ? "" : ownerName);
        queryNode.put("from", from == null ? "" : from);
        queryNode.put("to", to == null ? "" : to);
        queryNode.put("extension", UText.commaString(extension));
        queryNode.put("fq", fq == null ? "" : fq);
        queryNode.put("projectName", projectName == null ? "" : projectName);
        queryNode.put("xdcCategory", UText.commaString(xdcCategory));
        queryNode.put("offset", offset);
        queryNode.put("size", size);
        queryNode.put("sort", sort);
        return queryNode;
    }

    public String toQuery() {
        final StringBuffer sb = new StringBuffer();
        sb.append("q=").append(q).append("&");
        sb.append("addq=").append(UText.commaString(addq)).append("&");
        sb.append("sources=").append(UText.commaString(sources)).append("&");
        sb.append("sessionId=").append(sessionId == null ? "" : sessionId).append("&");
        sb.append("userId=").append(userId == null ? "" : userId).append("&");
        sb.append("userDeptCode=").append(userDeptCode == null ? "" : userDeptCode).append("&");
        sb.append("userDivCode=").append(userDivCode == null ? "" : userDivCode).append("&");
        sb.append("myDeptDoc=").append(myDeptDoc).append("&");
        sb.append("myDoc=").append(myDoc).append("&");
        sb.append("groupId=").append(UText.commaString(groupId)).append("&");
        sb.append("ownerName=").append(ownerName == null ? "" : ownerName).append("&");
        sb.append("from=").append(from == null ? "" : from).append("&");
        sb.append("to=").append(to == null ? "" : to).append("&");
        sb.append("extension=").append(UText.commaString(extension)).append("&");
        sb.append("fq=").append(fq == null ? "" : fq).append("&");
        sb.append("projectName=").append(projectName == null ? "" : projectName).append("&");
        sb.append("xdcCategory=").append(UText.commaString(xdcCategory)).append("&");
        sb.append("offset=").append(offset).append("&");
        sb.append("size=").append(size).append("&");
        sb.append("sort=").append(sort == null ? "" : sort);
        return sb.toString();
    }

    @Override
    public String toString() {
        return "SisQuery{" +
                "depth=" + depth +
                ", q='" + q + '\'' +
                ", addq=" + Arrays.toString(addq) +
                ", sources=" + Arrays.toString(sources) +
                ", sessionId='" + sessionId + '\'' +
                ", userId='" + userId + '\'' +
                ", userDeptCode='" + userDeptCode + '\'' +
                ", userDivCode='" + userDivCode + '\'' +
                ", myDeptDoc=" + myDeptDoc +
                ", myDoc=" + myDoc +
                ", groupId=" + Arrays.toString(groupId) +
                ", ownerName='" + ownerName + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", extension=" + Arrays.toString(extension) +
                ", fq='" + fq + '\'' +
                ", projectName='" + projectName + '\'' +
                ", xdcCategory=" + Arrays.toString(xdcCategory) +
                ", offset=" + offset +
                ", size=" + size +
                ", sort='" + sort + '\'' +
                ", viewTabIndex=" + viewTabIndex +
                ", viewTabId='" + viewTabId + '\'' +
                ", viewOwningType=" + viewOwningType +
                ", viewSortType=" + viewSortType +
                ", viewOptionFixed=" + viewOptionFixed +
                ", viewStartPage=" + viewStartPage +
                '}';
    }
}
