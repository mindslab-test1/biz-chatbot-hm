package kr.co.mobis.MOBI_CHAT.user.service;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.user.model.NoticeEntity;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class NoticeService {
    static Logger logger = LoggerFactory.getLogger(NoticeService.class);

    @Value("${front.bot.url}")
    private String BOT_URL;

    @Value("${front.bot.notice.path}")
    private String PATH_NOTICE;


    private String okHttpClient(String path, Headers headers, String query) {
        HttpUrl httpUrl = HttpUrl.parse(BOT_URL).newBuilder()
                .encodedPath(path)
                .encodedQuery(query)
                .build();
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .build();
        return botHttpClient.serverCall();
    }

    /**
     * 봇, 검색 알림정보 조회
     */
    public NoticeEntity botNotice(String botId) throws BotException {
        logger.info("botNotice start...");
        StringBuilder query = new StringBuilder();
        query.append("botId="+botId);

        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/x-www-form-urlencoded")
                .build();
        String responceString = okHttpClient(PATH_NOTICE, headers, query.toString());
        if (UText.isEmpty(responceString)) {
            logger.error("saveOpinion is null");
            throw new BotException("saveOpinion result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        logger.info("botNotice rootNode:"+rootNode.toString());
        NoticeEntity.dataNodeValidationCheck(rootNode);
        return new NoticeEntity(rootNode);
    }

}
