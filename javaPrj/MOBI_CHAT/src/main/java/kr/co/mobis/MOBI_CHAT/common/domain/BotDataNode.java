package kr.co.mobis.MOBI_CHAT.common.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.io.IOException;

public abstract class BotDataNode implements BotResponce<JsonNode> {
    static Logger logger = LoggerFactory.getLogger(BotDataNode.class);

    protected int code;
    @NotNull protected String msg;
    @NotNull protected JsonNode dataNode;

    public static JsonNode toJsonNode(String jonString) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(jonString);
        } catch (IOException e) {
            logger.info("toJsonNode error: " + e.getLocalizedMessage());
            return null;
        }
    }

    public static String toJson(Object obj) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    public BotDataNode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BotDataNode(JsonNode rootNode) {
        this(BotStatusCode.OK.getCode(), BotStatusCode.OK.name());
        dataNode = dataNodeParser(rootNode);
    }

    public abstract JsonNode dataNodeParser(JsonNode rootNode);

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    @Override
    public JsonNode getData() {
        return dataNode;
    }

//    /**
//     * Sub JsonNode 가져온다.
//     */
//    public JsonNode toAsJsonNode(String name) {
//        if (dataNode == null) {
//            return null;
//        }
//        if (dataNode.get(name) == null) {
//            return null;
//        }
//        if (!dataNode.get(name).isContainerNode()) {
//            return null;
//        }
//        return dataNode.get(name);
//    }
//
//    public int toAsInt(String name) {
//        if (dataNode == null) {
//            return 0;
//        }
//        if (dataNode.get(name) == null) {
//            return 0;
//        }
//        if (!dataNode.get(name).isInt()) {
//            return 0;
//        }
//        return dataNode.get(name).asInt(0);
//    }
//
//    public long toAsLong(String name) {
//        if (dataNode == null) {
//            return 0L;
//        }
//        if (dataNode.get(name) == null) {
//            return 0L;
//        }
//        if (!dataNode.get(name).isLong()) {
//            return 0L;
//        }
//        return dataNode.get(name).asLong(0L);
//    }
//
//    public String toAsString(String name) {
//        if (dataNode == null) {
//            return "";
//        }
//        if (dataNode.get(name) == null) {
//            return "";
//        }
//        if (!dataNode.get(name).isTextual()) {
//            return "";
//        }
//        return dataNode.get(name).asText("");
//    }
}
