package kr.co.mobis.MOBI_CHAT;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.encryption.StringEncryptor;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEncryptableProperties
public class MobiChatApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(MobiChatApplication.class, args);
	}

	@Bean(name="encryptorAesBean")
	static public StringEncryptor stringAesEncryptor() {
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		SimpleStringPBEConfig config = new SimpleStringPBEConfig();
		config.setProvider(new BouncyCastleProvider());
		config.setAlgorithm("PBEWITHSHA256AND256BITAES-CBC-BC");
		config.setPassword("mobi_chat_bot_service");
		config.setKeyObtentionIterations("1000");
		config.setPoolSize("1");
		config.setProviderName("BC");
		config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
		config.setIvGeneratorClassName("org.jasypt.salt.NoOpIVGenerator");
		config.setStringOutputType("base64");
		encryptor.setConfig(config);
		return encryptor;
	}

}
