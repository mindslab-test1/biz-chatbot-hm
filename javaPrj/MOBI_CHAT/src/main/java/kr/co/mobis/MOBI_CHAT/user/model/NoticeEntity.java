package kr.co.mobis.MOBI_CHAT.user.model;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NoticeEntity extends BotDataNode {
    static Logger logger = LoggerFactory.getLogger(NoticeEntity.class);

    private JsonNode noticeNode;

    public NoticeEntity(int code, String msg) {
        super(code, msg);
    }
    public NoticeEntity(JsonNode rootNode) {
        super(rootNode);
    }

    @Override
    public JsonNode dataNodeParser(JsonNode rootNode) {
        logger.info("dataNodeParser Start...");
        JsonNode dataNode = rootNode.get("data");
        if (dataNode.get("notices") != null && dataNode.get("notices").isArray()) {
            JsonNode noticesNode = dataNode.get("notices");
            if (noticesNode.size() > 0) {
                noticeNode = noticesNode.get(noticesNode.size()-1);
            }
        }
        return dataNode;
    }

    public static void dataNodeValidationCheck(JsonNode rootNode) throws BotException {
        logger.info("dataNodeValidationCheck start...");

        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("dataNodeValidationCheck rootNode is null or not ContainerNode");
            throw new BotException("dataNodeValidationCheck rootNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }

        // ErrorStatus Check
        if (rootNode.get("code").asInt() != 0) {
            int code = rootNode.get("code").asInt();
            String msg = rootNode.get("msg").asText();
            logger.error("dataNodeValidationCheck dataNode error: (" + code + ")" + msg );
            throw new BotException(msg, code);
        }
        if (rootNode.get("data") == null || !rootNode.get("data").isContainerNode()) {
            logger.error("dataNodeValidationCheck dataNode is null or not ContainerNode");
            throw new BotException("dataNodeValidationCheck dataNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
    }

    public JsonNode getNoticeNode() {
        return noticeNode;
    }

    public void setNoticeNode(JsonNode noticeNode) {
        this.noticeNode = noticeNode;
    }
}
