package kr.co.mobis.MOBI_CHAT.search.domain;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class MrcEntity extends BotDataNode {
    static Logger logger = LoggerFactory.getLogger(MrcEntity.class);

    public MrcEntity(int code, String msg) {
        super(code, msg);
    }
    public MrcEntity(JsonNode rootNode) {
        super(rootNode);
    }

    @Override
    public JsonNode dataNodeParser(JsonNode rootNode) {
        logger.info("dataNodeParser Start...");
        return mrcDataParser(rootNode.get("data"));
    }

    public static void dataNodeValidationCheck(JsonNode rootNode) throws BotException {
        logger.info("dataNodeValidationCheck start...");
        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("dataNodeValidationCheck dataNode is null or not ContainerNode");
            throw new BotException("dataNodeValidationCheck dataNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        // ErrorStatus Check
//        if (rootNode.get("code").asInt() != 0) {
//            int code = rootNode.get("code").asInt();
//            String msg = rootNode.get("msg").asText();
//            logger.error("toJsonParser dataNode error: (" + code + ")" + msg );
//            throw new BotException(msg, code);
//        }
        if (rootNode.get("data") == null || !rootNode.get("data").isArray()) {
            logger.error("dataNodeValidationCheck dataNode is null or not ContainerNode");
            throw new BotException("dataNodeValidationCheck dataNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
    }

    private JsonNode mrcDataParser(JsonNode dateNode) {
        logger.info("mrcDataParser start...");

        ArrayNode arrayNode = new ObjectMapper().createArrayNode();
        Map<String, JsonNode> tempMaps = new HashMap<>();
        for (JsonNode itemNode : dateNode) {
            String objectOrder = itemNode.get("objectOrder").asText();
            if (!tempMaps.containsKey(objectOrder)) {
                tempMaps.put(objectOrder, itemNode);
            }
        }
//        logger.info("mrcDataParser tempMaps:"+tempMaps.toString());
        for (String key : tempMaps.keySet()) {
            arrayNode.add(tempMaps.get(key));
        }
        logger.info("mrcDataParser arrayNode:"+arrayNode.toString());
        return arrayNode;
    }

}
