package kr.co.mobis.MOBI_CHAT.bot.service;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotContext;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriUtils;

public class TagParser {
    static Logger logger = LoggerFactory.getLogger(TagParser.class);

    private StringBuilder original = null;
    private StringBuilder htmlDocument = new StringBuilder();

    public TagParser(String sentence) {
        original = new StringBuilder(sentence);
    }

    public TagParser parseTag() {
        StringBuilder remainder = original;
        // max 20회만 처리
        //  나중 장문에 tag가 20개 이상일때 수정요망.
        for (int i=0; i<20 ; i++) {
            String firstTag = "";
            int firstIndex = remainder.length();
            for (String tag : BotContext.COMPARE_TAGS) {
                int tagIndex = remainder.indexOf(tag);
                if (tagIndex != -1 && firstIndex > tagIndex) {
                    firstIndex = tagIndex;
                    firstTag = tag;
                }
            }
//            logger.info("parseTag firstTag:" + firstTag);
//            logger.info("parseTag firstIndex:" + firstIndex);
            if (!UText.isEmpty(firstTag) && firstIndex > 0) {
                String text = remainder.substring(0, remainder.indexOf(firstTag));
//                logger.info("parseTag text:" + text);
                htmlDocument.append(text);
                remainder.delete(0, remainder.indexOf(firstTag));
//                logger.info("parseTag remainder:" + remainder.toString());
                tagClass(firstTag, remainder);
            } else if (!UText.isEmpty(firstTag) && firstIndex == 0) {
//                logger.info("parseTag remainder:" + remainder.toString());
                tagClass(firstTag, remainder);
            } else {
                htmlDocument.append(remainder.toString());
                break;
            }
        }
//        logger.info("parseTag html:" + htmlDocument.toString());
        return this;
    }

    private void tagClass(String tag, StringBuilder remainder) {
        if (tag.equals(BotContext.TAG_CTAG_START)) {
            String ctagData = remainder.substring(remainder.indexOf(BotContext.TAG_CTAG_START)+6, remainder.indexOf(BotContext.TAG_CTAG_END));
            logger.info("tagClass ctagData: " + ctagData);
            ctag(ctagData);
            remainder.delete(0, remainder.indexOf(BotContext.TAG_CTAG_END)+6);
        } else {
            logger.info("tagClass not tag:" + tag);
            logger.info("tagClass not remainder:" + remainder.toString());
            htmlDocument.append(" **ctag("+tag+") in preparation** ");
        }
    }

    private void ctag(String ctagData) {
        JsonNode ctagNode = BotDataNode.toJsonNode(ctagData);
        if (ctagNode == null) {
            htmlDocument.append(" **outlink data is null** ");
        } else {
            String method = ctagNode.get("method").asText().toUpperCase();
            String type = ctagNode.get("type").asText();
            if (type.equals(BotContext.TAG_CTAG_TYPE_OUTLINK)) {
                if (method.toUpperCase().equals("POST")) {
                    String url = ctagNode.get("url").asText();
                    String text = ctagNode.get("text").asText();
                    JsonNode paramsNode = ctagNode.get("params");
                    StringBuffer aDom = new StringBuffer();
                    aDom.append("<a ");
                    aDom.append("href='#'");
                    aDom.append(" ");
                    aDom.append("onclick=postLinkBlank('"+url+"','"+paramsNode.toString()+"')");
                    aDom.append(">");
                    aDom.append(text);
                    aDom.append("</a>");
//                    logger.info("tagClass aDom:" + aDom.toString());
                    htmlDocument.append(aDom.toString());
                } else {
                    String url = ctagNode.get("url").asText();
                    String text = ctagNode.get("text").asText();
                    StringBuffer aDom = new StringBuffer();
                    aDom.append("<a ");
                    aDom.append("href='" + url + "'");
                    aDom.append(" ");
                    aDom.append("target='_blank'");
                    aDom.append(">");
                    aDom.append(text);
                    aDom.append("</a>");
//                    logger.info("tagClass aDom:" + aDom.toString());
                    htmlDocument.append(aDom.toString());
                }
            } else if (type.equals(BotContext.TAG_CTAG_TYPE_MAILLINK)) {
                String url = ctagNode.get("url").asText();
                String text = ctagNode.get("text").asText();
                String cmd = ctagNode.get("params").get("cmd").asText();
                String userName = ctagNode.get("params").get("userName").asText();
                String email = ctagNode.get("params").get("email").asText();
                String query = "?cmd="+cmd+"&"+"msgTo="+"\"" + userName.trim() + "\" <" + email + ">";
                url += UriUtils.encodeQuery(query,"utf-8");
                logger.info("tagClass url:" + url);

                StringBuffer aDom = new StringBuffer();
                aDom.append("<a ");
                aDom.append("href='" + url + "'");
                aDom.append(" ");
                aDom.append("target='_blank'");
                aDom.append(">");
                aDom.append(text);
                aDom.append("</a>");
//                logger.info("tagClass aDom:" + aDom.toString());
                htmlDocument.append(aDom.toString());
            } else if (type.equals(BotContext.TAG_CTAG_TYPE_RESTAPI_RPA)) {
                String url = ctagNode.get("url").asText();
                String text = ctagNode.get("text").asText();
                JsonNode paramsNode = ctagNode.get("params");

                StringBuffer aDom = new StringBuffer();
                aDom.append("<a class='btn btn-outline-primary btn-label btn-label-window-restore btn-block'");
                aDom.append(" ");
                aDom.append("href='#'");
                aDom.append(" ");
                aDom.append("onclick=callRestApi('"+url+"','"+paramsNode.toString()+"')");
                aDom.append(">");
                aDom.append(text);
                aDom.append("</a>");
                logger.info("tagClass restapi aDom:" + aDom.toString());
                htmlDocument.append(aDom.toString());
            } else {
                htmlDocument.append(" **ctag("+type+") Not supported **");
            }
        }
    }

    public String toHtmlDocument() {
        UText.replaceAll(htmlDocument, "\r\n", "<br/>");
        UText.replaceAll(htmlDocument, "\\r\\n", "<br/>");
        UText.replaceAll(htmlDocument, "\n", "<br/>");
        UText.replaceAll(htmlDocument, "\\n", "<br/>");
        UText.replaceAll(htmlDocument, "\r", "<br/>");
        UText.replaceAll(htmlDocument, "\\r", "<br/>");
        logger.info("toHtmlDocument htmlDocument:" + htmlDocument.toString());
        return htmlDocument.toString();
    }
}
