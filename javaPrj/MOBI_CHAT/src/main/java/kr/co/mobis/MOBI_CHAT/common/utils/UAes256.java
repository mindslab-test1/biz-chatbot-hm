package kr.co.mobis.MOBI_CHAT.common.utils;

import org.jasypt.contrib.org.apache.commons.codec_1_3.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.Key;

public class UAes256 {

	private String iv;
	private byte[] iv16;
	private Key keySpec;

	public UAes256(String key) throws UnsupportedEncodingException {
		this.iv = key.substring(0, 32);
		iv16 = new byte[16];
		for(int i = 0 ; i < iv16.length ; i++) {
			iv16[i] = 0x00;
		}
		byte[] keyBytes = new byte[32];
		byte[] b = key.getBytes("UTF-8");
		int len = b.length;
		if (len > keyBytes.length) {
			len = keyBytes.length;
		}
		System.arraycopy(b, 0, keyBytes, 0, len);
		SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

		this.keySpec = keySpec;
	}

	public String aesEncode(String str) throws Exception {
		Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
		c.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv16));
		byte[] encrypted = c.doFinal(str.getBytes("UTF-8"));
		String enStr = new String(Base64.encodeBase64(encrypted));

		return enStr;
	}

	public String aesDecode(String str) throws Exception {
		Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
		c.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv16));
		byte[] byteStr = Base64.decodeBase64(str.getBytes());
		return new String(c.doFinal(byteStr), "UTF-8");
	}


	public static void main(String[] args) throws Exception {

		UAes256 util = new UAes256("MOBIS_CHAT_BOT_SERVICE_MASTERKEY");
		System.out.println(util.aesEncode("1624991"));    // Q8A4yGZFYQAOpnzq2fNluw==
		System.out.println(util.aesEncode("임형준"));      // fThZ7QkrZ0+7HxefRYFScg==
		System.out.println(util.aesEncode("빅데이터팀"));  // 57Ck6H1bHKuXWRlNGyu5nw==


		System.out.println("---------------------------------------------------------------");
		UAes256 wikiAes256 = new UAes256("M0bisSecurity12#M0bisSecurity12#");
		System.out.println(wikiAes256.aesEncode("1606075"));    // Lz2JRPuM5xLR6Bp/F2jI1Q==

//		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
//		SimpleStringPBEConfig config = new SimpleStringPBEConfig();
//		config.setProvider(new BouncyCastleProvider());
//		config.setAlgorithm("PBEWITHSHA256AND256BITAES-CBC-BC");
//		config.setPassword("mobi_chat_bot_service");
//		config.setKeyObtentionIterations("1000");
//		config.setPoolSize("1");
//		config.setProviderName("BC");
//		config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
//		config.setIvGeneratorClassName("org.jasypt.salt.NoOpIVGenerator");
//		config.setStringOutputType("base64");
//		encryptor.setConfig(config);
//		String masterKey = encryptor.encrypt("M0bisSecurity12#M0bisSecurity12#");
//		System.out.println("wiki masterKey:" + masterKey);
	}
}
