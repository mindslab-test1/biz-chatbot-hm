package kr.co.mobis.MOBI_CHAT.search.domain;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SisPopulerEntity extends BotDataNode {
    static Logger logger = LoggerFactory.getLogger(SisPopulerEntity.class);

    public SisPopulerEntity(int code, String msg) {
        super(code, msg);
    }
    public SisPopulerEntity(JsonNode rootNode) {
        super(rootNode);
    }

    @Override
    public JsonNode dataNodeParser(JsonNode rootNode) {
        logger.info("dataNodeParser Start...");
        ArrayNode arrayNode = new ObjectMapper().createArrayNode();
        int limitCount = rootNode==null ? 0 : (rootNode.size() > 5 ? 5 : rootNode.size());
        for (int i=0; i<limitCount; i++) {
            JsonNode itemNode = rootNode.get(i);
            arrayNode.add(itemNode);
        }
        return limitCount==0?null:arrayNode;
    }

    public static void dataNodeValidationCheck(JsonNode rootNode) throws BotException {
        logger.info("dataNodeValidationCheck start...");
        if (rootNode == null || !rootNode.isArray()) {
            logger.error("dataNodeValidationCheck dataNode is null or not ArrayNode");
            throw new BotException("dataNodeValidationCheck dataNode is null or not ArrayNode", BotStatusCode.Error.getCode());
        }
    }
}
