package kr.co.mobis.MOBI_CHAT.search;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.MApi;
import kr.co.mobis.MOBI_CHAT.common.domain.BotContext;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.utils.UDate;
import kr.co.mobis.MOBI_CHAT.search.domain.SearchTermItemEntity;
import kr.co.mobis.MOBI_CHAT.search.domain.SearchTermListEntity;
import kr.co.mobis.MOBI_CHAT.search.service.SearchTermService;
import kr.co.mobis.MOBI_CHAT.user.model.BotSessionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("search/term")
public class SearchTermController {
    static Logger logger = LoggerFactory.getLogger(SearchTermController.class);

    @Autowired
    private SearchTermService searchTermService;

    @PostMapping("/listView")
    public String listView(HttpSession session, @RequestBody Map<String, String> params, Model model) {
        logger.info("listView  Start...");
        logger.info("listView  params: " + params.toString());

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("listView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("listView botSessionInfo: " + botSessionInfo.toString());
        String keywords = params.get("keywords");

        SearchTermListEntity searchTermListEntity = null;
        try {
            searchTermListEntity = searchTermService.searchList(keywords);
        } catch (BotException bte) {
            logger.info("listView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
//            searchTermListEntity = new SearchTermListEntity(bte.getCode(), bte.getLocalizedMessage());
            return MApi.waringView(model).toPath();
        }

        JsonNode dataNode = searchTermListEntity.getData();
        model.addAttribute("botTime", UDate.nowBotTime());
        model.addAttribute("keywords", keywords);
        model.addAttribute("totalCount", dataNode.get("totalCount").asInt());
        model.addAttribute("listNode", dataNode.get("listNode"));
        return "/view/chatting/searchTermListView";
    }

    @PostMapping("/itemView")
    public String itemView(HttpSession session, @RequestBody Map<String, String> params, Model model) {
        logger.info("itemView  Start...");
        logger.info("itemView  params: " + params.toString());

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("itemView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("itemView botSessionInfo: " + botSessionInfo.toString());

        String korTitle = params.get("korTitle");
        String trmsId = params.get("trmsId");

        SearchTermItemEntity searchTermItemEntity = null;
        try {
            searchTermItemEntity = searchTermService.searchItemView(trmsId);
        } catch (BotException bte) {
            logger.info("itemView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
//            searchTermItemEntity = new SearchTermItemEntity(bte.getCode(), bte.getLocalizedMessage());
            return MApi.waringView(model).toPath();
        }

        JsonNode dataNode = searchTermItemEntity.getData();

        model.addAttribute("botTime", UDate.nowBotTime());
        model.addAttribute("korTitle", korTitle);
        model.addAttribute("dataNode", dataNode);
        return "/view/chatting/searchTermItemView";
    }
}
