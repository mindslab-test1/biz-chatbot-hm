package kr.co.mobis.MOBI_CHAT.common.domain;

public class BotException extends RuntimeException {

    private final int code;

    public BotException(String message, Throwable cause, int code) {
        super(message, cause);
        this.code = code;
    }
    public BotException(String message, int code) {
        super(message);
        this.code = code;
    }
    public BotException(Throwable cause, int code) {
        super(cause);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
