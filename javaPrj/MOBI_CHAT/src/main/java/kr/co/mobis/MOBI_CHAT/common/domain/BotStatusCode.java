package kr.co.mobis.MOBI_CHAT.common.domain;

public enum BotStatusCode {
    OK(0, "Success!!!"),
    Error(1000, "알수 없는 오류가 발생했습니다."),
    Error_Authentication(1010, "세션 종료되었거나, 인증되지 않은 사용자입니다."),
    Error_IO(2000, "서버 통신중 오류가 발생했습니다."),
    Error_IO_Network(2010, "서버 통신중 네트웍 오류가 발생했습니다.");

    private int code;
    private String msg;
    BotStatusCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
