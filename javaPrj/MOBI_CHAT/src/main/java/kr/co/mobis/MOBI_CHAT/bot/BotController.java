package kr.co.mobis.MOBI_CHAT.bot;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import kr.co.mobis.MOBI_CHAT.MApi;
import kr.co.mobis.MOBI_CHAT.bot.domain.*;
import kr.co.mobis.MOBI_CHAT.bot.service.*;
import kr.co.mobis.MOBI_CHAT.common.domain.BotContext;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.utils.UDate;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.search.domain.MrcEntity;
import kr.co.mobis.MOBI_CHAT.search.service.MrcService;
import kr.co.mobis.MOBI_CHAT.user.model.BotSessionInfo;
import kr.co.mobis.MOBI_CHAT.user.model.BotSetting;
import kr.co.mobis.MOBI_CHAT.user.service.SettingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Controller
@RequestMapping("bot")
public class BotController {
    static Logger logger = LoggerFactory.getLogger(BotController.class);

    @Value("${front.bot.image.url}")
    private String BOT_IMAGE_URL;

    @Autowired
    private SettingService settingService;

    @Autowired
    private BotService botService;

    @Autowired
    private PingpongService pingpongService;

    @Autowired
    private LikeService likeService;

    @Autowired
    private MrcService mrcService;

    @Autowired
    private CtagService ctagService;


    @GetMapping("")
    public String bot(HttpSession session, Model model) {
        logger.info("bot  Start...");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("bot  no authenticated session information.");
            return MApi.errPage(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("bot botSessionInfo: " + botSessionInfo.toString());

        BotSetting botSetting = null;
        try {
            botSetting = settingService.setting(botSessionInfo.getUserId());
        } catch (BotException bte) {
            logger.info("bot  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            return MApi.errPage(model, BotContext.ErrorStatus.ERROR_UNKNOWN).toPath();
        }

        model.addAttribute("botSessionInfo", botSessionInfo);
        model.addAttribute("bots", botSetting.getBots());
        model.addAttribute("activeBotType", BotContext.SettingBotType.MAIBOT.getBotType());
        return "/bot";
    }

    @PostMapping("/greetingView")
    public String greetingView(HttpSession session, @RequestBody MaiBot maiBot, Model model) {
        logger.info("greetingView  Start...");
        logger.info("greetingView  maiBot: " + maiBot.toString());

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("greetingView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("greetingView botSessionInfo: " + botSessionInfo.toString());

        maiBot.setUserSessionInfo(botSessionInfo);

        BotGreetingEntity botGreetingEntity = null;
        try {
            botGreetingEntity = botService.greeting(botSessionInfo, maiBot);
        } catch (BotException bte) {
            logger.info("greetingView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
//            botGreetingEntity = new BotGreetingEntity(bte.getCode(), bte.getLocalizedMessage());
            return MApi.waringView(model).toPath();
        }

        JsonNode dataNode = botGreetingEntity.getData();

        String botSessionId = dataNode.get("sessionId").asText();
        logger.info("greetingView botSessionId: " + botSessionId);
        botSessionInfo.setBotSessionId(botSessionId);
        session.setAttribute("botSessionInfo", botSessionInfo);
        // 세션 시간 초기화
        session.setMaxInactiveInterval(BotContext.SESSION_INACTIVE_INTERVAL_SECOND);


        JsonNode uttrData = dataNode.get("uttrData");
        if (uttrData == null) {
            model.addAttribute("botTime", UDate.nowBotTime());
            model.addAttribute("normalMessages", "uttrData 유효하지 않습니다.");
            return "/view/chatting/normalView";
        }

        String richContentType = BotContext.RICH_CONTENT_TYPE_NONE;
        String sentence = uttrData.get("sentence").asText();
        String htmlDocument = new TagParser(sentence).parseTag().toHtmlDocument();
        model.addAttribute("botTime", UDate.nowBotTime());
        model.addAttribute("sentence", htmlDocument);

        JsonNode richContent = uttrData.get("richContent");
        if (richContent == null) {
            model.addAttribute("richContentType", BotContext.RICH_CONTENT_TYPE_NONE);
            model.addAttribute("richContent", null);
        } else {
            if (!UText.isEmpty(richContent.get("contentTypeCd").asText())) {
                if (richContent.get("buttons") != null) {
                    richContentType = richContent.get("contentTypeCd").asText();
                }
            }
            logger.info("greetingView richContentType: " + richContentType);
            model.addAttribute("richContentType", richContentType);
            model.addAttribute("richContent", richContent);
        }

        JsonNode botNotices = (uttrData.get("botNotice") == null ? null : uttrData.get("botNotice").get("notices"));
        model.addAttribute("botNotices", botNotices);
        return "/view/chatting/greetingView";
    }

    @PostMapping("/questionView")
    public String questionView(HttpSession session, @RequestBody MaiBot maiBot, Model model) {
        logger.info("questionView  Start...");
        logger.info("questionView  maiBot: " + maiBot.toString());

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("questionView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        // 세션 시간 초기화
        session.setMaxInactiveInterval(BotContext.SESSION_INACTIVE_INTERVAL_SECOND);
        logger.info("questionView botSessionInfo: " + botSessionInfo.toString());
        maiBot.setUserSessionInfo(botSessionInfo);

        BotQuestionEntity botQuestionEntity = null;
        try {
            botQuestionEntity = botService.question(botSessionInfo, maiBot);
        } catch (BotException bte) {
            logger.info("questionView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
//            botQuestionEntity = new BotQuestionEntity(bte.getCode(), bte.getLocalizedMessage());
            return MApi.waringView(model).toPath();
        }

        //---------------------------------------------------------------------
        // 여기서부터 Component View 처리
        //---------------------------------------------------------------------
        BotContext.BotComponentViewType viewType = BotContext.BotComponentViewType.NORMAL_VIEW;

        JsonNode dataNode = botQuestionEntity.getData();
        String sysUtterType = BotContext.SYS_UTTER_TYPE_NORMAL;
        String richContentType = BotContext.RICH_CONTENT_TYPE_NONE;
        String currStep = "Unknown";
        String sentence = dataNode.get("sentence") == null ? "" : dataNode.get("sentence").asText();

        if (dataNode.get("sysUtterType") != null) {
            sysUtterType = dataNode.get("sysUtterType").asText();
            currStep = dataNode.get("currStep").asText();
            String msgTypeCd = dataNode.get("msgTypeCd") == null ? "" : dataNode.get("msgTypeCd").asText();

            // TODO 봇 분류기 임시로 막음 Lee.s.h 20190516
//            if (msgTypeCd.equals(BotContext.MSG_TYPE_UNKNOWN)) {
//                String unknownSentence = mrcService.xdcRequest(maiBot.getBotId(), maiBot.getSentence());
//                if (!UText.isEmpty(unknownSentence)) {
//                    sysUtterType = BotContext.SYS_UTTER_TYPE_NORMAL;
//                    sentence = unknownSentence;
//                }
//            }
        }

        //---------------------------------------------------------------------
        // View Type
        //---------------------------------------------------------------------
        if (sysUtterType.equals(BotContext.SYS_UTTER_TYPE_SHUTTLE_BUS)) {
            viewType = BotContext.BotComponentViewType.SHUTTLE_BUS_VIEW;
        } else if (sysUtterType.equals(BotContext.SYS_UTTER_TYPE_CAFETERIA_MENU)) {
            viewType = BotContext.BotComponentViewType.CAFETERIA_MENU_VIEW;
        } else {
            if (dataNode.get("richContent") != null && dataNode.get("richContent").isContainerNode()) {
                if (!UText.isEmpty(dataNode.get("richContent").get("contentTypeCd").asText())) {
                    richContentType = dataNode.get("richContent").get("contentTypeCd").asText();
                }
                if (richContentType.equals(BotContext.RICH_CONTENT_TYPE_BUTTON)) {
                    viewType = BotContext.BotComponentViewType.BUTTON_VIEW;
                } else if (richContentType.equals(BotContext.RICH_CONTENT_TYPE_IMAGE)) {
                    viewType = BotContext.BotComponentViewType.POPUP_IMAGE_LINK_VIEW;
                }
            }
        }
        logger.info("questionView  BotComponentViewType: " + viewType.name());

        //---------------------------------------------------------------------
        // View Style
        //---------------------------------------------------------------------
        ObjectNode viewStyleNode = new ObjectMapper().createObjectNode();
        if (sysUtterType.equals(BotContext.SYS_UTTER_TYPE_SHUTTLE_BUS)) {
            viewStyleNode.put("viewStyle", BotContext.BotComponentViewStyle.SHUTTLE_BUS.name());
            viewStyleNode.put("viewTitle", "버스노선 서비스 안내");
        } else if (sysUtterType.equals(BotContext.SYS_UTTER_TYPE_BUSSTOP_LOC)) {
            viewStyleNode.put("viewStyle", BotContext.BotComponentViewStyle.BUSSTOP_LOC.name());
            viewStyleNode.put("viewTitle", "버스노선 서비스 안내");
        } else if (sysUtterType.equals(BotContext.SYS_UTTER_TYPE_CAFETERIA_MENU)) {
            viewStyleNode.put("viewStyle", BotContext.BotComponentViewStyle.CAFETERIA_MENU.name());
            viewStyleNode.put("viewTitle", "식단 안내");
        } else if (sysUtterType.equals(BotContext.SYS_UTTER_TYPE_EMAIL_CHECK)) {
            viewStyleNode.put("viewStyle", BotContext.BotComponentViewStyle.EMAIL.name());
            viewStyleNode.put("viewTitle", "메일 안내");
        } else if (sysUtterType.equals(BotContext.SYS_UTTER_TYPE_APPROVAL_CHECK)) {
            viewStyleNode.put("viewStyle", BotContext.BotComponentViewStyle.APPROVAL.name());
            viewStyleNode.put("viewTitle", "결재 안내");
        } else if (sysUtterType.equals(BotContext.SYS_UTTER_TYPE_EMPLOYEE_INFO)) {
            viewStyleNode.put("viewStyle", BotContext.BotComponentViewStyle.EMPLOYEE.name());
            viewStyleNode.put("viewTitle", "담당자 안내");
        } else if (sysUtterType.equals(BotContext.SYS_UTTER_TYPE_BQA_RECOMMEND)) {
            viewStyleNode.put("viewStyle", BotContext.BotComponentViewStyle.BQA_RECOMMEND.name());
            viewStyleNode.put("viewTitle", "추천 질문");
        } else if (sysUtterType.equals(BotContext.SYS_UTTER_TYPE_WEATHER)) {
            viewStyleNode.put("viewStyle", BotContext.BotComponentViewStyle.WEATHER.name());
            viewStyleNode.put("viewTitle", "날씨 안내");
        } else if (sysUtterType.equals(BotContext.SYS_UTTER_TYPE_IMOBIS_MENU)) {
            viewStyleNode.put("viewStyle", BotContext.BotComponentViewStyle.IMOBIS_MENU.name());
            viewStyleNode.put("viewTitle", "iMobis 메뉴 안내");
        } else if (sysUtterType.equals(BotContext.SYS_UTTER_TYPE_QMS_MENU)) {
            viewStyleNode.put("viewStyle", BotContext.BotComponentViewStyle.QMS_MENU.name());
            viewStyleNode.put("viewTitle", "QMS 메뉴 안내");
        } else {
            viewStyleNode = null;
        }

        ObjectNode likeNode = new ObjectMapper().createObjectNode();
        likeNode.put("question", maiBot.getSentence());
        likeNode.put("answer", sentence);
        likeNode.put("currStep", currStep);

        if (viewType == BotContext.BotComponentViewType.NORMAL_VIEW) {
            String htmlDocument = new TagParser(sentence).parseTag().toHtmlDocument();
            model.addAttribute("botTime", UDate.nowBotTime());
            model.addAttribute("sentence", htmlDocument);
            model.addAttribute("likeNode", likeNode);
            model.addAttribute("viewStyleNode", viewStyleNode);
            return "/view/chatting/normalView";
        } else if (viewType == BotContext.BotComponentViewType.BUTTON_VIEW) {
            String htmlDocument = new TagParser(sentence).parseTag().toHtmlDocument();
//            HtmlEditor.with(dataNode.get("richContent")).toHtmlDocument();

            model.addAttribute("botTime", UDate.nowBotTime());
            model.addAttribute("sentence", htmlDocument);
            model.addAttribute("richContent", dataNode.get("richContent"));
            model.addAttribute("likeNode", likeNode);
            model.addAttribute("viewStyleNode", viewStyleNode);
            return "/view/chatting/buttonsView";
        } else if (viewType == BotContext.BotComponentViewType.POPUP_IMAGE_LINK_VIEW) {
            String htmlDocument = new TagParser(sentence).parseTag().toHtmlDocument();

            JsonNode imageNode = dataNode.get("richContent").get("image");
            String imageText = "";
            String imagePath = "";
            String linkUrl = "";
            if (imageNode != null) {
                imageText = imageNode.get("imageText") == null ? "" : imageNode.get("imageText").asText();
                imagePath = imageNode.get("imagePath") == null ? "" : imageNode.get("imagePath").asText();
                linkUrl = imageNode.get("linkUrl") == null ? "" : imageNode.get("linkUrl").asText();
            }
            if (!UText.isEmpty(imagePath) && !imagePath.startsWith("http")) {
                imagePath = BOT_IMAGE_URL + imagePath;
            }
            if (!UText.isEmpty(linkUrl) && !linkUrl.startsWith("http")) {
                linkUrl = BOT_IMAGE_URL + linkUrl;
            }

            logger.info("questionView  POPUP_IMAGE_LINK_VIEW   imageText:" + imageText);
            logger.info("questionView  POPUP_IMAGE_LINK_VIEW   imagePath:" + imagePath);
            logger.info("questionView  POPUP_IMAGE_LINK_VIEW   linkUrl:" + linkUrl);

            String imageViewType = "OTHER";
            if (!UText.isEmpty(imagePath) && !UText.isEmpty(linkUrl)) {
                imageViewType = "IMAGE_LINK";  // image link
            } else if (UText.isEmpty(imagePath) && !UText.isEmpty(linkUrl)) {
                imageViewType = "BUTTON_LINK"; // button link
            } else if (!UText.isEmpty(imagePath) && UText.isEmpty(linkUrl)) {
                imageViewType = "IMAGE_VIEW";  // image view
            } else if (!UText.isEmpty(imageText)) {
                imageViewType = "TEXT_VIEW";   // text view
            }
            logger.info("questionView  POPUP_IMAGE_LINK_VIEW   imageViewType:" + imageViewType);

            model.addAttribute("botTime", UDate.nowBotTime());
            model.addAttribute("sentence", htmlDocument);
            model.addAttribute("imageViewType", imageViewType);
            model.addAttribute("imageText", imageText);
            model.addAttribute("imagePath", imagePath);
            model.addAttribute("linkUrl", linkUrl);
            model.addAttribute("likeNode", likeNode);
            model.addAttribute("viewStyleNode", viewStyleNode);
            return "/view/chatting/imageLinkView";
        } else if (viewType == BotContext.BotComponentViewType.CAFETERIA_MENU_VIEW) {
            String htmlDocument = new TagParser(sentence).parseTag().toHtmlDocument();
            model.addAttribute("sentence", htmlDocument);
            model.addAttribute("botTime", UDate.nowBotTime());
            model.addAttribute("richContent", dataNode.get("richContent"));
            model.addAttribute("viewStyleNode", viewStyleNode);
            return "/view/chatting/cafeteriaMenuView";
        } else if (viewType == BotContext.BotComponentViewType.SHUTTLE_BUS_VIEW) {
            String htmlDocument = new TagParser(sentence).parseTag().toHtmlDocument();

            JsonNode shuttleBusNode = null;
            JsonNode shuttleBusDataNode = null;
            String stopName = "";
            if (dataNode.get("intentJson") != null) {
                String shuttleBusString = dataNode.get("intentJson").asText();
                if (!UText.isEmpty(shuttleBusString)) {
                    shuttleBusNode = BotDataNode.toJsonNode(shuttleBusString);
                    stopName = shuttleBusNode.get("stopName") == null ? "" : shuttleBusNode.get("stopName").asText();
                }
                if (shuttleBusNode.get("data") != null) {
                    shuttleBusDataNode = BotDataNode.toJsonNode(shuttleBusNode.get("data").asText());
                }
                logger.info("questionView  SHUTTLE_BUS_VIEW   shuttleBusNode:" + shuttleBusNode);
            }

            int shuttleBusCount = shuttleBusDataNode == null ? 0 : shuttleBusDataNode.size();
            ObjectNode shuttleBusObject = new ObjectMapper().createObjectNode();
            shuttleBusObject.put("question", maiBot.getSentence());
            shuttleBusObject.put("stopName", stopName);
            shuttleBusObject.put("shuttleBusCount", shuttleBusCount);
            shuttleBusObject.set("shuttleBus", shuttleBusDataNode);

            model.addAttribute("botTime", UDate.nowBotTime());
            model.addAttribute("sentence", htmlDocument);
            model.addAttribute("shuttleBusNode", shuttleBusObject);
            model.addAttribute("likeNode", likeNode);
            model.addAttribute("viewStyleNode", viewStyleNode);
            return "/view/chatting/shuttleBusView";
        } else {
            return MApi.waringView(model).toPath();
        }
    }

    @PostMapping("/close")
    @ResponseBody
    public ResponseEntity<?> close(HttpSession session, @RequestBody MaiBot maiBot, Model model) {
        logger.info("close  Start...");
        logger.info("close  maiBot: " + maiBot.toString());

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("close  no authenticated session information.");
            return ResponseEntity.noContent().build();
        }
        logger.info("close botSessionInfo: " + botSessionInfo.toString());
        maiBot.setUserSessionInfo(botSessionInfo);

        BotCloseEntity botCloseEntity = null;
        try {
            botCloseEntity = botService.close(botSessionInfo, maiBot);
        } catch (BotException bte) {
            logger.info("close  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            botCloseEntity = new BotCloseEntity(bte.getCode(), bte.getLocalizedMessage());
        }

        return ResponseEntity.ok(botCloseEntity);
    }

    @PostMapping("/saveLike")
    public ResponseEntity<LikeEntity> saveLike(HttpSession session, @RequestBody Map<String, String> params) {
        logger.info("saveLike  Start...");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("saveLike  no authenticated session information.");
            return ResponseEntity.ok(new LikeEntity(BotStatusCode.Error_Authentication.getCode(), BotStatusCode.Error_Authentication.getMsg()));
        }
        logger.info("saveLike botSessionInfo: " + botSessionInfo.toString());
        logger.info("saveLike params: " + params.toString());

        String userId = botSessionInfo.getUserId();
        String botId = params.get("botId");
        String currStep = params.get("currStep");
        String question = params.get("question");
        String answer = params.get("answer");
        String countType = params.get("countType");

        LikeEntity likeEntity = null;
        try {
            likeEntity = likeService.saveLike(userId, botId, currStep, question, answer, countType);
        } catch (BotException bte) {
            logger.info("saveLike  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            likeEntity = new LikeEntity(bte.getCode(), bte.getLocalizedMessage());
        }
        return ResponseEntity.ok(likeEntity);
    }

    @PostMapping("/pingpongView")
    public String pingpongView(HttpSession session, @RequestBody MaiBot maiBot, Model model) {
        logger.info("pingpongView  Start...");
        logger.info("pingpongView  maiBot: " + maiBot.toString());

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("pingpongView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        // 세션 시간 초기화
        session.setMaxInactiveInterval(BotContext.SESSION_INACTIVE_INTERVAL_SECOND);
        logger.info("pingpongView botSessionInfo: " + botSessionInfo.toString());
        maiBot.setUserSessionInfo(botSessionInfo);

        PingpongEntity pingpongEntity = null;
        try {
            pingpongEntity = pingpongService.question(maiBot);
        } catch (BotException bte) {
            logger.info("pingpongView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
//            pingpongEntity = new PingpongEntity(bte.getCode(), bte.getLocalizedMessage());
            return MApi.waringView(model).toPath();
        }

        JsonNode dataNode = pingpongEntity.getData();
        String sentence = dataNode.get("message").asText();

        ObjectNode likeNode = new ObjectMapper().createObjectNode();
        likeNode.put("question", maiBot.getSentence());
        likeNode.put("answer", sentence);
        likeNode.put("currStep", "pingpong");

        String htmlDocument = new TagParser(sentence).parseTag().toHtmlDocument();
        model.addAttribute("botTime", UDate.nowBotTime());
        model.addAttribute("sentence", htmlDocument);
        model.addAttribute("likeNode", likeNode);
        model.addAttribute("viewStyleNode", null);
        return "/view/chatting/normalView";
    }

    @PostMapping("/questionMrcView")
    public String questionMrcView(HttpSession session, @RequestBody MaiBot maiBot, Model model) {
        logger.info("questionMrcView  Start...");
        logger.info("questionMrcView  maiBot: " + maiBot.toString());

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("pingpongView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        // 세션 시간 초기화
        session.setMaxInactiveInterval(BotContext.SESSION_INACTIVE_INTERVAL_SECOND);
        logger.info("pingpongView botSessionInfo: " + botSessionInfo.toString());
        maiBot.setUserSessionInfo(botSessionInfo);

        MrcEntity mrcEntity = null;
        try {
            mrcEntity = mrcService.generalMrcList(maiBot.getSentence());
        } catch (BotException bte) {
            logger.info("mrcPageView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            mrcEntity = new MrcEntity(bte.getCode(), bte.getLocalizedMessage());
        }

        JsonNode mrcNode = mrcEntity == null ? null : mrcEntity.getData();
        JsonNode mrcFirstNode = mrcNode == null ? null : mrcNode.get(0);
        StringBuffer sentenceBuffer = new StringBuffer();
//        StringBuffer commentBuffer = new StringBuffer();
        if (mrcFirstNode == null) {
            sentenceBuffer.append("적절한 답변을 찾지 못했습니다.<br/>");
            sentenceBuffer.append("다시 질문해 주시겠어요?");
        } else {
            String markupSentence = mrcFirstNode.get("markupSentence").asText();
            double prob = mrcFirstNode.get("prob").asDouble();
            String modifyDate = mrcFirstNode.get("modifyDate").asText();
            modifyDate = UDate.parseDateISO8601Local(modifyDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            sentenceBuffer.append("인공지능이 실시간으로 (<mark>" + modifyDate + "</mark>)에 작성된 사내 규정을 읽고 답을 찾아드립니다.");
//            sentenceBuffer.append("정확한 내용은 담당자에게 문의하거나 규정을 참고해주세요.");
            sentenceBuffer.append("<br/>");
            sentenceBuffer.append("&nbsp;&nbsp;<mark>" + markupSentence + "</mark> ");
            sentenceBuffer.append("입니다. <br/>");
            sentenceBuffer.append("정확한 내용은 담당자에게 문의하거나 규정을 참고해주세요.");

//            commentBuffer.append("정확한 내용은 담당자에게 문의하거나 규정을 참고해주세요.");
//            commentBuffer.append("(<mark>" + String.format("%.1f", prob) + "</mark>)");
//            commentBuffer.append("로 규정집에서 찾은 답변입니다.<br/>");
//            commentBuffer.append("해당 정보는 (<mark>" + modifyDate + "</mark>)에 수정되었습니다.");
        }

        ObjectNode viewStyleNode = new ObjectMapper().createObjectNode();
        viewStyleNode.put("viewStyle", BotContext.BotComponentViewStyle.MRC.name());
        viewStyleNode.put("viewTitle", "인공지능 답변: 규정");

        ObjectNode likeNode = new ObjectMapper().createObjectNode();
        likeNode.put("question", maiBot.getSentence());
        likeNode.put("answer", sentenceBuffer.toString());
        likeNode.put("currStep", "mrc");

        model.addAttribute("botTime", UDate.nowBotTime());
        model.addAttribute("sentence", sentenceBuffer.toString());
        model.addAttribute("totalCount", (mrcNode == null ? 0 : mrcNode.size()));
        model.addAttribute("dataFirstNode", mrcNode == null ? null : mrcNode.get(0));
        model.addAttribute("comment", null);
        model.addAttribute("likeNode", likeNode);
        model.addAttribute("viewStyleNode", viewStyleNode);
        return "/view/chatting/mrcView";
    }

    @PostMapping("/ctagRpa")
    public ResponseEntity<CtagEntity> ctagRpa(HttpSession session, @RequestBody JsonNode paramsNode) {
        logger.info("ctagRpa  Start...");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("ctagRpa  no authenticated session information.");
            return ResponseEntity.ok(new CtagEntity(BotStatusCode.Error_Authentication.getCode(), BotStatusCode.Error_Authentication.getMsg()));
        }
        logger.info("ctagRpa botSessionInfo: " + botSessionInfo.toString());
        logger.info("ctagRpa paramsNode: " + paramsNode.toString());

        String origin = paramsNode.get("origin").asText();
        String pathname = paramsNode.get("pathname").asText();
        String postBody = paramsNode.get("params").toString();
        postBody = UText.replaceAll(new StringBuilder(postBody), BotContext.TAG_REFLECT_SESSION_USER_ID, botSessionInfo.getUserId());
        logger.info("ctagRpa postBody: " + postBody);

        CtagEntity ctagEntity = null;
        try {
            ctagEntity = ctagService.ctagRestApiRpa(origin, pathname, postBody);
        } catch (BotException bte) {
            logger.info("ctagRpa  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            ctagEntity = new CtagEntity(bte.getCode(), bte.getLocalizedMessage());
        }
        return ResponseEntity.ok(ctagEntity);
    }
}
