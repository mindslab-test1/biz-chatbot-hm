package kr.co.mobis.MOBI_CHAT.search.domain;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FaveEntity extends BotDataNode {
    static Logger logger = LoggerFactory.getLogger(FaveEntity.class);

    public FaveEntity(int code, String msg) {
        super(code, msg);
    }
    public FaveEntity(JsonNode rootNode) {
        super(rootNode);
    }

    @Override
    public JsonNode dataNodeParser(JsonNode rootNode) {
        logger.info("dataNodeParser Start...");
        return rootNode==null ? null : rootNode.get("hits").get("hits");
    }

    public static void dataNodeValidationCheck(JsonNode rootNode) throws BotException {
        logger.info("dataNodeValidationCheck start...");
        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("dataNodeValidationCheck dataNode is null or not ContainerNode");
            throw new BotException("dataNodeValidationCheck dataNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        if (rootNode.get("hits") == null || !rootNode.get("hits").isContainerNode()) {
            logger.error("dataNodeValidationCheck hitsNode is null or not ContainerNode");
            throw new BotException("dataNodeValidationCheck hitsNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        if (rootNode.get("hits").get("hits") == null || !rootNode.get("hits").get("hits").isArray()) {
            logger.error("dataNodeValidationCheck hitsArrayNode is null or not ContainerNode");
            throw new BotException("dataNodeValidationCheck hitsArrayNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
    }
}
