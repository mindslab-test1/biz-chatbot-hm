package kr.co.mobis.MOBI_CHAT.user.service;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UAes256;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.user.model.BotSessionInfo;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SignService {
    static Logger logger = LoggerFactory.getLogger(SignService.class);

    @Value("${front.bot.url}")
    private String BOT_URL;

    @Value("${front.bot.master.key}")
    private String BOT_AES_MASTER_KEY;

    @Value("${front.wiki.document.master.key}")
    private String WIKI_AES_MASTER_KEY;

    @Value("${front.bot.login.path}")
    private String PATH_LOGIN;

    private String okHttpClient(String path, RequestBody postBody) {
        HttpUrl httpUrl = HttpUrl.parse(BOT_URL).newBuilder()
                .encodedPath(path)
                .build();
        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/x-www-form-urlencoded")
                .build();
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .postBody(postBody)
                .build();
        return botHttpClient.serverCall();
    }

    /**
     * 로그인 인증 체크
     */
    public BotSessionInfo login(String channelType, String userIp,  String userId) throws BotException {
        logger.info("login start...");
        logger.info("login channelType: " + channelType);
        logger.info("login userId: " + userId);
        logger.info("login userIp: " + userIp);
        String originalUserId = null;
        if (userId.length() == 7) {
            originalUserId = userId;
        } else {
            try {
                originalUserId = new UAes256(BOT_AES_MASTER_KEY).aesDecode(userId);
            } catch (Exception e) {
                logger.error("login UAes256.aesDecode error: " + e.getLocalizedMessage());
                throw new BotException("login UAes256.aesDecode error", e, BotStatusCode.Error.getCode());
            }
        }

        logger.info("login originalUserId: " + originalUserId);

        RequestBody formBody = new FormBody.Builder()
                .add("channelType", channelType)
                .add("userId", originalUserId)
                .add("userIp", userIp)
                .build();
        String responceString = okHttpClient(PATH_LOGIN, formBody);
        return toJsonParser(responceString);
    }

    /**
     * 로그인 결과 Parser
     */
    private BotSessionInfo toJsonParser(String jsonString) throws BotException {
        logger.info("login start...");

        if (UText.isEmpty(jsonString)) {
            logger.error("login is null");
            throw new BotException("login legacy user api result is null", BotStatusCode.Error_Authentication.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(jsonString);
        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("login rootNode is null or not ContainerNode");
            throw new BotException("login rootNode is null or not ContainerNode", BotStatusCode.Error_Authentication.getCode());
        }
        // ErrorStatus Check
        if (rootNode.get("code").asInt() != 0) {
            int code = rootNode.get("code").asInt();
            String msg = rootNode.get("msg").asText();
            logger.error("login dataNode error: (" + code + ")" + msg);
            throw new BotException(msg, BotStatusCode.Error_Authentication.getCode());
        }
        if (rootNode.get("data") == null || !rootNode.get("data").isContainerNode()) {
            logger.error("login dataNode is null or not ContainerNode");
            throw new BotException("login dataNode is null or not ContainerNode", BotStatusCode.Error_Authentication.getCode());
        }

        JsonNode dataNode = rootNode.get("data");
        logger.info("login dataNode: " + dataNode.toString());

        BotSessionInfo botSessionInfo = new BotSessionInfo();
        botSessionInfo.setUserId(dataNode.get("userId").asText());
        botSessionInfo.setUserNm(dataNode.get("userName").asText());
        botSessionInfo.setToken(dataNode.get("token").asText());
        botSessionInfo.setRemoteAddress(dataNode.get("remoteAddress").asText());
        botSessionInfo.setDeptCd(dataNode.get("deptCd").asText());
        botSessionInfo.setDeptNm(dataNode.get("deptName").asText());
        botSessionInfo.setUserDivCode(dataNode.get("userDivCode")==null ? "" : dataNode.get("userDivCode").asText());
        botSessionInfo.setUserDivNm(dataNode.get("userDivNm")==null ? "" : dataNode.get("userDivNm").asText());
        botSessionInfo.setConfirmYn(dataNode.get("confirmYn")==null ? "" : dataNode.get("confirmYn").asText());
        botSessionInfo.setWikiSID("");
        try {
            String encryptWikiSid = new UAes256(WIKI_AES_MASTER_KEY).aesEncode(botSessionInfo.getUserId());
            logger.error("login wiki encryptWikiSid:" + encryptWikiSid);
            botSessionInfo.setWikiSID(encryptWikiSid);
        } catch (Exception e) {
            logger.error("login wiki UAes256.aesDecode error: " + e.getLocalizedMessage());
        }
        logger.info("login botSessionInfo: " + botSessionInfo.toString());

        return botSessionInfo;
    }

}
