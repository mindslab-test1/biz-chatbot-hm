package kr.co.mobis.MOBI_CHAT.bot.domain;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PingpongEntity extends BotDataNode {
    static Logger logger = LoggerFactory.getLogger(PingpongEntity.class);

    public PingpongEntity(int code, String msg) {
        super(code, msg);
    }
    public PingpongEntity(JsonNode rootNode) {
        super(rootNode);
    }

    @Override
    public JsonNode dataNodeParser(JsonNode rootNode) {
        logger.info("dataNodeParser Start...");
        this.code = rootNode.get("code").asInt(0);
        this.msg = rootNode.get("msg").asText();

        JsonNode dataNode = null;
        if (rootNode.get("data").isTextual()) {
            dataNode = BotDataNode.toJsonNode(rootNode.get("data").asText());
        } else {
            dataNode = rootNode.get("data");
        }
        logger.info("dataNodeParser End. ");
        return dataNode;
    }

    public static void dataNodeValidationCheck(JsonNode rootNode) throws BotException {
        logger.info("dataNodeValidationCheck start...");

        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("dataNodeValidationCheck rootNode is null or not ContainerNode");
            throw new BotException("dataNodeValidationCheck rootNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }

        // ErrorStatus Check
        if (rootNode.get("code").asInt() != 0) {
            int code = rootNode.get("code").asInt();
            String msg = rootNode.get("msg").asText();
            logger.error("dataNodeValidationCheck error: (" + code + ")" + msg );
            throw new BotException(msg, code);
        }
        if (rootNode.get("data") == null) {
            logger.error("dataNodeValidationCheck dataNode is null");
            throw new BotException("dataNodeValidationCheck dataNode is null", BotStatusCode.Error.getCode());
        }
    }
}