package kr.co.mobis.MOBI_CHAT.common.utils;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UText {

	/**
     * Returns true if the parameter is null or of zero length
     */
    public static boolean isEmpty(final CharSequence s) {
        if (s == null) {
            return true;
        }
        return s.length() == 0;
    }

    /**
     * Returns true if the parameter is null or contains only whitespace
     */
    public static boolean isBlank(final CharSequence s) {
        if (s == null) {
            return true;
        }
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isWhitespace(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

	public static boolean isOneChecked(final String[] textArray, final String value) {
        for (final String text : textArray) {
            if (text.equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }

    public static String commaString(String[] array) {
        if (array == null || array.length == 0)
            return "";
        return Arrays.stream(array)
                .reduce((p1, p2) -> p1 + "," + p2)
                .orElse("");
    }

    public static String replaceAll(StringBuilder original, String from, String to) {
        Pattern pattern = Pattern.compile(from);
        Matcher m = pattern.matcher(original);
        while(m.find()) {
            original.replace(m.start(), m.end(), to);
        }
        return original.toString();
    }
}

