package kr.co.mobis.MOBI_CHAT.search.domain;

import kr.co.mobis.MOBI_CHAT.common.domain.BotContext;

import java.util.ArrayList;
import java.util.List;

public class Aggregations {

    private List<SourceSystemType> sourceSystemTypes = new ArrayList<>();
    private List<CodeName> groupIds = new ArrayList<>();
    private List<String> extensions = new ArrayList<>();
    private List<String> xdcCategorys = new ArrayList<>();

    public Aggregations() {}

    public List<SourceSystemType> getSourceSystemTypes() {
        return sourceSystemTypes;
    }

    public void setSourceSystemTypes(List<SourceSystemType> sourceSystemTypes) {
        this.sourceSystemTypes = sourceSystemTypes;
    }

    public List<CodeName> getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(List<CodeName> groupIds) {
        this.groupIds = groupIds;
    }

    public List<String> getExtensions() {
        return extensions;
    }

    public void setExtensions(List<String> extensions) {
        this.extensions = extensions;
    }

    public List<String> getXdcCategorys() {
        return xdcCategorys;
    }

    public void setXdcCategorys(List<String> xdcCategorys) {
        this.xdcCategorys = xdcCategorys;
    }

    @Override
    public String toString() {
        return "Aggregations{" +
                "sourceSystemTypes=" + sourceSystemTypes +
                ", groupIds=" + groupIds +
                ", extensions=" + extensions +
                ", xdcCategorys=" + xdcCategorys +
                '}';
    }

    public static class SourceSystemType {
        private BotContext.SourceSystemType type;
        private boolean yn;
        private int docCount;

        public SourceSystemType() {}

        public BotContext.SourceSystemType getType() {
            return type;
        }

        public void setType(BotContext.SourceSystemType type) {
            this.type = type;
        }

        public boolean isYn() {
            return yn;
        }

        public void setYn(boolean yn) {
            this.yn = yn;
        }

        public int getDocCount() {
            return docCount;
        }

        public void setDocCount(int docCount) {
            this.docCount = docCount;
        }

        @Override
        public String toString() {
            return "SourceSystemType{" +
                    "type=" + type +
                    ", yn=" + yn +
                    ", docCount=" + docCount +
                    '}';
        }
    }

    public static class CodeName {
        private String code;
        private String name;

        public CodeName() {}

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "CodeName{" +
                    "code='" + code + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
