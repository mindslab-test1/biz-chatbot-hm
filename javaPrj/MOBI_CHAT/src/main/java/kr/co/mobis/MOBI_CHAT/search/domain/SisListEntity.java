package kr.co.mobis.MOBI_CHAT.search.domain;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotContext;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.user.model.SettingInfo;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class SisListEntity extends BotDataNode {
    static Logger logger = LoggerFactory.getLogger(SisListEntity.class);

    private int totCount;
    private int paginationCount;
    private AnalyzeKeywords analyzeKeywords;
    private Aggregations aggregations;

    public SisListEntity(int code, String msg) {
        super(code, msg);
    }
    public SisListEntity(JsonNode rootNode) {
        super(rootNode);
    }

    @Override
    public JsonNode dataNodeParser(JsonNode rootNode) {
        logger.info("dataNodeParser Start...");
        totCount = rootNode.get("totCount").asInt();
        paginationCount = rootNode.get("paginationCount").asInt();
        analyzeKeywordsParser(rootNode.get("analyzeKeywords"));
        aggregationsParser(rootNode.get("aggregations"));
        return rootNode.get("hits");
    }

    public static void oneDataNodeValidationCheck(JsonNode rootNode) throws BotException {
        logger.info("oneDataNodeValidationCheck start...");

        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("oneDataNodeValidationCheck rootNode is null or not ContainerNode");
            throw new BotException("oneDataNodeValidationCheck rootNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        if (rootNode.get("hits") == null || !rootNode.get("hits").isContainerNode()) {
            logger.error("oneDataNodeValidationCheck hitsNode is null or not ContainerNode");
            throw new BotException("oneDataNodeValidationCheck hitsNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        if (rootNode.get("hits").get("hits") == null || !rootNode.get("hits").get("hits").isArray()) {
            logger.error("oneDataNodeValidationCheck hitsArrayNode is null or not ContainerNode");
            throw new BotException("oneDataNodeValidationCheck hitsArrayNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        if (rootNode.get("aggregations") == null || !rootNode.get("aggregations").isContainerNode()) {
            logger.error("oneDataNodeValidationCheck aggregationsNode is null or not ContainerNode");
            throw new BotException("oneDataNodeValidationCheck aggregationsNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        JsonNode aggregationsNode = rootNode.get("aggregations");
        if (aggregationsNode.get("cardinality#total") == null || aggregationsNode.get("cardinality#total").get("value") == null) {
            logger.error("oneDataNodeValidationCheck paginationCount is null");
            throw new BotException("oneDataNodeValidationCheck paginationCount is null", BotStatusCode.Error.getCode());
        }
    }
    public static void twoDataNodeValidationCheck(JsonNode rootNode) throws BotException {
        logger.info("twoDataNodeValidationCheck start...");

        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("twoDataNodeValidationCheck rootNode is null or not ContainerNode");
            throw new BotException("twoDataNodeValidationCheck rootNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
    }

    private void analyzeKeywordsParser(JsonNode wordAnalyzeNode) {
        if (analyzeKeywords == null) {
            analyzeKeywords = new AnalyzeKeywords();
        }

        JsonNode recommendedKeywordsNode = wordAnalyzeNode.get("recommendedKeywords");
        if (recommendedKeywordsNode != null && recommendedKeywordsNode.size() > 0) {
            for (JsonNode keywordNode : recommendedKeywordsNode) {
                analyzeKeywords.getRecommendedKeywords().add(keywordNode.asText());
            }
        }
        JsonNode relatedKeywordsNode = wordAnalyzeNode.get("relatedKeywords");
        if (relatedKeywordsNode != null && relatedKeywordsNode.size() > 0) {
            for (JsonNode keyword : relatedKeywordsNode) {
                analyzeKeywords.getRelatedKeywords().add(keyword.asText());
            }
        }
        logger.info("aggregationsParser analyzeKeywords: " + analyzeKeywords.toString());
    }

    private void aggregationsParser(JsonNode aggregationsNode) {
        if (aggregations == null) {
            aggregations = new Aggregations();
        }

        // 상세검색 소스시스템
        JsonNode sourceSystemNode = aggregationsNode.get("sterms#source_system");
        if (sourceSystemNode != null && sourceSystemNode.get("buckets") != null && sourceSystemNode.get("buckets").size() > 0 ) {
            for (BotContext.SourceSystemType type : BotContext.SourceSystemType.values()) {
                boolean isComparison = false;
                int docCount = 0;
                for (JsonNode systemTypeNode : sourceSystemNode.get("buckets")) {
                    if (systemTypeNode != null && systemTypeNode.get("key") != null) {
                        String sourceSystem = systemTypeNode.get("key").asText();
                        if (type.name().equals(sourceSystem)) {
                            isComparison = true;
                            docCount = systemTypeNode.get("doc_count").asInt();
                        }
                    }
                }
                Aggregations.SourceSystemType ssType = new Aggregations.SourceSystemType();
                ssType.setType(type);
                ssType.setYn(isComparison);
                ssType.setDocCount(docCount);
                aggregations.getSourceSystemTypes().add(ssType);
            }
        }

        // 상세검색 부서
        JsonNode groupIdsNode = aggregationsNode.get("sterms#dept_code_nm");
        if (groupIdsNode != null && groupIdsNode.get("buckets") != null && groupIdsNode.get("buckets").size() > 0 ) {
            JsonNode bucketsNode = groupIdsNode.get("buckets");
            for (int i=0; i<bucketsNode.size(); i++) {
                String key = bucketsNode.get(i).get("key").asText();
                if (!UText.isEmpty(key) &&
                        key.split("\\|").length >= 2 && key.split("\\|")[0] != null && key.split("\\|")[1] != null) {
                    Aggregations.CodeName codeName = new Aggregations.CodeName();
                    codeName.setCode(key.split("\\|")[0]);
                    codeName.setName(key.split("\\|")[1]);
                    aggregations.getGroupIds().add(codeName);
                }
            }
            // abc 가나다 sort
            List<Aggregations.CodeName> sortList = aggregations.getGroupIds()
                    .stream().sorted(Comparator.comparing(Aggregations.CodeName::getName))
                    .collect(Collectors.toList());
            aggregations.setGroupIds(sortList);
        }

        // 상세검색 파일확장명
        JsonNode extensionsNode = aggregationsNode.get("sterms#dos_extension");
        if (extensionsNode != null && extensionsNode.get("buckets") != null && extensionsNode.get("buckets").size() > 0 ) {
            JsonNode bucketsNode = extensionsNode.get("buckets");
            int loopCount = bucketsNode.size();
            for (int i=0; i<loopCount; i++) {
                String key = bucketsNode.get(i).get("key").asText();
                aggregations.getExtensions().add(key);
            }
        }

        // XDC Category
        JsonNode xdcCategoryNode = aggregationsNode.get("sterms#xdc_category");
        if (xdcCategoryNode != null && xdcCategoryNode.get("buckets") != null && xdcCategoryNode.get("buckets").size() > 0 ) {
            JsonNode bucketsNode = xdcCategoryNode.get("buckets");
            int loopCount = bucketsNode.size();
            for (int i=0; i<loopCount; i++) {
                String key = bucketsNode.get(i).get("key").asText();
                aggregations.getXdcCategorys().add(key);
            }
        }

        logger.info("aggregationsParser aggregations: " + aggregations.toString());
    }

    public int getTotCount() {
        return totCount;
    }

    public int getPaginationCount() {
        return paginationCount;
    }

    public AnalyzeKeywords getAnalyzeKeywords() {
        if (analyzeKeywords == null) {
            analyzeKeywords = new AnalyzeKeywords();
        }
        return analyzeKeywords;
    }

    public Aggregations getAggregations() {
        if (aggregations == null) {
            aggregations = new Aggregations();
        }
        return aggregations;
    }
}
