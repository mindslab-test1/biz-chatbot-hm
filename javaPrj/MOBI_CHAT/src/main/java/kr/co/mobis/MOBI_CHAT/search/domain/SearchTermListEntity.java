package kr.co.mobis.MOBI_CHAT.search.domain;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchTermListEntity extends BotDataNode {
    static Logger logger = LoggerFactory.getLogger(SearchTermListEntity.class);

    public SearchTermListEntity(int code, String msg) {
        super(code, msg);
    }
    public SearchTermListEntity(JsonNode rootNode) {
        super(rootNode);
    }

    @Override
    public JsonNode dataNodeParser(JsonNode rootNode) {
        logger.info("dataNodeParser Start...");

        this.code = 0;
        this.msg = "";

        JsonNode listNode = rootNode.get("list");
        logger.info("dataNodeParser listNode: " + listNode.toString());

        if (listNode == null || !listNode.isArray()) {
            return null;
        }

        ObjectNode dataNode = new ObjectMapper().createObjectNode();
        dataNode.put("totalCount", listNode.size());
        ArrayNode newListNode = new ObjectMapper().createArrayNode();
        int loopCount = listNode.size() > 5 ? 5 : listNode.size();
        for (int i=0; i<loopCount; i++) {
            newListNode.add(listNode.get(i));
        }
        dataNode.set("listNode", newListNode);

        logger.info("dataNodeParser End. ");

        return dataNode;
    }
}
