package kr.co.mobis.MOBI_CHAT.bot.domain;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BotGreetingEntity extends BotDataNode {
    static Logger logger = LoggerFactory.getLogger(BotGreetingEntity.class);

    public BotGreetingEntity(int code, String msg) {
        super(code, msg);
    }
    public BotGreetingEntity(JsonNode rootNode) {
        super(rootNode);
    }

    @Override
    public JsonNode dataNodeParser(JsonNode rootNode) {
        logger.info("dataNodeParser Start...");

        this.code = rootNode.get("code").asInt(0);
        this.msg = rootNode.get("msg").asText();

        logger.info("dataNodeParser End. ");
        return rootNode.get("data");
    }

    public static void dataNodeValidationCheck(JsonNode rootNode) throws BotException {
        logger.info("dataNodeValidationCheck start...");

        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("dataNodeValidationCheck rootNode is null or not ContainerNode");
            throw new BotException("dataNodeValidationCheck rootNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }

        // ErrorStatus Check
        if (rootNode.get("code").asInt() != 0) {
            int code = rootNode.get("code").asInt();
            String msg = rootNode.get("msg").asText();
            logger.error("dataNodeValidationCheck error: (" + code + ")" + msg );
            throw new BotException(msg, code);
        }
        if (rootNode.get("data") == null || !rootNode.get("data").isContainerNode()) {
            logger.error("dataNodeValidationCheck dataNode is null or not ContainerNode");
            throw new BotException("dataNodeValidationCheck dataNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
    }
}