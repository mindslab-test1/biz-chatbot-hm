package kr.co.mobis.MOBI_CHAT.user.model;

public class BotSessionInfo {

    private String userId;
    private String userNm;
    private String token;
    private String remoteAddress;
    private String deptCd;
    private String deptNm;
    private String userDivCode;
    private String userDivNm;
    private String confirmYn;
    private String botSessionId;
    private String wikiSID;

    public BotSessionInfo() {}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserNm() {
        return userNm;
    }

    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public String getDeptCd() {
        return deptCd;
    }

    public void setDeptCd(String deptCd) {
        this.deptCd = deptCd;
    }

    public String getDeptNm() {
        return deptNm;
    }

    public void setDeptNm(String deptNm) {
        this.deptNm = deptNm;
    }

    public String getUserDivCode() {
        return userDivCode;
    }

    public void setUserDivCode(String userDivCode) {
        this.userDivCode = userDivCode;
    }

    public String getUserDivNm() {
        return userDivNm;
    }

    public void setUserDivNm(String userDivNm) {
        this.userDivNm = userDivNm;
    }

    public void setConfirmYn(String conFirmYn) { this.confirmYn = conFirmYn;}

    public String getConfirmYn() { return this.confirmYn;};

    public String getBotSessionId() {
        return botSessionId;
    }

    public void setBotSessionId(String botSessionId) {
        this.botSessionId = botSessionId;
    }

    public String getWikiSID() {
        return wikiSID;
    }

    public void setWikiSID(String wikiSID) {
        this.wikiSID = wikiSID;
    }

    @Override
    public String toString() {
        return "BotSessionInfo{" +
                "userId='" + userId + '\'' +
                ", userNm='" + userNm + '\'' +
                ", token='" + token + '\'' +
                ", remoteAddress='" + remoteAddress + '\'' +
                ", deptCd='" + deptCd + '\'' +
                ", deptNm='" + deptNm + '\'' +
                ", userDivCode='" + userDivCode + '\'' +
                ", userDivNm='" + userDivNm + '\'' +
                ", confirmYn='" + confirmYn + '\'' +
                ", botSessionId='" + botSessionId + '\'' +
                ", wikiSID='" + wikiSID + '\'' +
                '}';
    }
}
