package kr.co.mobis.MOBI_CHAT.search.service;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.search.domain.SearchTermItemEntity;
import kr.co.mobis.MOBI_CHAT.search.domain.SearchTermListEntity;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SearchTermService {
    static Logger logger = LoggerFactory.getLogger(SearchTermService.class);

    @Value("${front.search.term.url}")
    private String SEARCH_TERM_URL;

    @Value("${front.search.term.api.key}")
    private String SEARCH_TERM_API_KEY;

    @Value("${front.search.term.path}")
    private String PATH_SEARCH_TERM;

    @Value("${front.search.term.view.path}")
    private String PATH_SEARCH_TERM_VIEW;

    private String okHttpClient(String path, String query) {
        HttpUrl httpUrl = HttpUrl.parse(SEARCH_TERM_URL).newBuilder()
                .encodedPath(path)
                .encodedQuery(query)
                .build();
        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/json; charset=utf-8")
                .build();
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .build();
        return botHttpClient.serverCall();
    }

    /**
     * 용어 검색
     */
    public SearchTermListEntity searchList(String keywords) throws BotException {
        StringBuilder query = new StringBuilder();
        query.append("key=" + SEARCH_TERM_API_KEY);
        query.append("&");
        query.append("keyword=" + keywords);
        query.append("&");
        query.append("display=" + 9999);
        query.append("&");
        query.append("start=" + 1);

        String responceString = okHttpClient(PATH_SEARCH_TERM, query.toString());
        if (UText.isEmpty(responceString)) {
            logger.error("searchList is null");
            throw new BotException("searchList result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("searchList rootNode is null or not ContainerNode");
            throw new BotException("searchList rootNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        if (rootNode.get("list") == null || !rootNode.get("list").isArray()) {
            logger.error("searchList list Node is null or not ArrayNode");
            throw new BotException("searchList list Node is null or not ArrayNode", BotStatusCode.Error.getCode());
        }
        return new SearchTermListEntity(rootNode);
    }


    /**
     * 용어뷰 검색
     */
    public SearchTermItemEntity searchItemView(String trmsId) throws BotException {
        StringBuilder query = new StringBuilder();
        query.append("key=" + SEARCH_TERM_API_KEY);
        query.append("&");
        query.append("langCd=ko");
        query.append("&");
        query.append("trmsId=" + trmsId);

        String responceString = okHttpClient(PATH_SEARCH_TERM_VIEW, query.toString());
        if (UText.isEmpty(responceString)) {
            logger.error("searchItemView is null");
            throw new BotException("searchItemView result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("searchItemView rootNode is null or not ContainerNode");
            throw new BotException("searchItemView rootNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        if (rootNode.get("restTermVO") == null || !rootNode.get("restTermVO").isContainerNode()) {
            logger.error("searchItemView restTermVO Node is null or not ContainerNode");
            throw new BotException("searchItemView restTermVO Node is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        return new SearchTermItemEntity(rootNode);
    }
}
