package kr.co.mobis.MOBI_CHAT.search.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.search.domain.MrcEntity;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MrcService {
    static Logger logger = LoggerFactory.getLogger(MrcService.class);

    @Value("${front.mrc.url}")
    private String MRC_URL;

    @Value("${front.mrc.request.path}")
    private String PATH_MRC_REQUEST;

    @Value("${front.mrc.passage.path}")
    private String PATH_MRC_PASSAGE;

    @Value("${front.xdc.request.path}")
    private String PATH_XDC_REQUEST;

    @Autowired
    private SisService sisService;

    private String okHttpClient(String path, String jsonBody) {
        HttpUrl httpUrl = HttpUrl.parse(MRC_URL).newBuilder()
                .encodedPath(path)
                .build();
        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/json; charset=utf-8")
                .build();
        RequestBody postBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonBody);
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .postBody(postBody)
                .build();
        return botHttpClient.serverCall();
    }

    /**
     * MRC 요청
     */
    private MrcEntity mrcRequest(JsonNode bodyNode) throws BotException {
        logger.info("mrcRequest start...");

        String responceString = okHttpClient(PATH_MRC_REQUEST, bodyNode.toString());
        logger.info("mrcRequest responceString:" + responceString);
        if (UText.isEmpty(responceString)) {
            logger.error("mrcRequest is null");
            throw new BotException("mrcRequest result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        MrcEntity.dataNodeValidationCheck(rootNode);
        return new MrcEntity(rootNode);
    }

    /**
     * 질문으로 MRC 요청
     */
    public MrcEntity generalMrcList(String question) throws BotException {
        logger.info("generalMrcList start...");

        ObjectNode paramsNode = new ObjectMapper().createObjectNode();
        paramsNode.put("question", question);

        String responceString = okHttpClient(PATH_MRC_PASSAGE, paramsNode.toString());
        logger.info("generalMrcList responceString:" + responceString);
        if (UText.isEmpty(responceString)) {
            logger.error("generalMrcList is null");
            throw new BotException("generalMrcList result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        MrcEntity.dataNodeValidationCheck(rootNode);
        return new MrcEntity(rootNode);
    }

    public MrcEntity searchMrcList(String[] ids, String question) throws BotException {
        logger.info("searchMrcList start...");

        JsonNode dataNode = sisService.mrcSearch(ids, question);
        logger.info("searchMrcList dataNode:" + dataNode);
        JsonNode arrayNode = mrcDataParser(dataNode);

        if (arrayNode == null || arrayNode.size() <= 0) {
            logger.error("searchMrcList arrayNode is null");
            throw new BotException("searchMrcList arrayNode is null", BotStatusCode.Error.getCode());
        }

        ObjectNode mrcPassageNode = new ObjectMapper().createObjectNode();
        mrcPassageNode.put("question", question);
        mrcPassageNode.set("passages", arrayNode);

        return mrcRequest(mrcPassageNode);
    }

    private JsonNode mrcDataParser(JsonNode dataNode) {
        logger.info("mrcDataParser start...");

        if (dataNode == null || dataNode.size() <= 0) {
            logger.error("searchMrcList dataNode is null");
            return null;
        }

        ArrayNode arrayNode = new ObjectMapper().createArrayNode();
        for (int i = 0; i < dataNode.size(); i++) {
            JsonNode itemNode = dataNode.get(i);

            String sourceSystemType = "";
            String objectId = "";
            String objectName = "";
            String objectUrl = "";
            String sourceOwnerName = "";
            String sourceDeptName = "";
            String sourceCreationDateTime = "";
            String sourceModifyDateTime = "";

            if (itemNode.get("_source") != null && itemNode.get("_source").get("source_system") != null) {
                sourceSystemType = itemNode.get("_source").get("source_system").asText();
            }
            if (itemNode.get("_source") != null && itemNode.get("_source").get("r_object_id") != null) {
                objectId = itemNode.get("_source").get("r_object_id").asText();
            }
            if (itemNode.get("_source") != null && itemNode.get("_source").get("r_object_name") != null) {
                objectName = itemNode.get("_source").get("r_object_name").asText();
            }
            if (itemNode.get("_source") != null && itemNode.get("_source").get("url") != null) {
                objectUrl = itemNode.get("_source").get("url").asText();
            }
            if (itemNode.get("_source") != null && itemNode.get("_source").get("owner_info") != null) {
                if (itemNode.get("_source").get("owner_info").get("user_nm") != null) {
                    sourceOwnerName = itemNode.get("_source").get("owner_info").get("user_nm").asText();
                }
                if (itemNode.get("_source").get("owner_info").get("dept_nm") != null) {
                    sourceDeptName = itemNode.get("_source").get("owner_info").get("dept_nm").asText();
                }
            }
            if (itemNode.get("_source") != null && itemNode.get("_source").get("creation_date") != null) {
                sourceCreationDateTime = itemNode.get("_source").get("creation_date").asText();
            }
            if (itemNode.get("_source") != null && itemNode.get("_source").get("modify_date") != null) {
                sourceModifyDateTime = itemNode.get("_source").get("modify_date").asText();
            }

            if (itemNode.get("inner_hits") == null || itemNode.get("inner_hits").get("contents") == null ||
                    itemNode.get("inner_hits").get("contents").get("hits") == null ||
                    itemNode.get("inner_hits").get("contents").get("hits").get("hits") == null) {
                continue;
            }
            JsonNode hitsArray = itemNode.get("inner_hits").get("contents").get("hits").get("hits");
            for (int j = 0; j < hitsArray.size(); j++) {
                JsonNode hitsNode = hitsArray.get(j);

                String pageNumber = "";
                String pageContents = "";
                if (hitsNode.get("_source") != null && hitsNode.get("_source").get("page_number") != null) {
                    pageNumber = hitsNode.get("_source").get("page_number").asText();
                }
                if (hitsNode.get("_source") != null && hitsNode.get("_source").get("content") != null) {
                    pageContents = hitsNode.get("_source").get("content").asText();
                }

                ObjectNode passageNode = new ObjectMapper().createObjectNode();
                passageNode.put("objectOrder", i);
                passageNode.put("objectId", objectId);
                passageNode.put("objectPageNumber", pageNumber);
                passageNode.put("sourceSystemType", sourceSystemType);
                passageNode.put("objectName", objectName);
                passageNode.put("ownerUserName", sourceOwnerName);
                passageNode.put("ownerDeptName", sourceDeptName);
                passageNode.put("creationDate", sourceCreationDateTime);
                passageNode.put("modifyDate", sourceModifyDateTime);
                passageNode.put("passage", pageContents);
                passageNode.put("objectUrl", objectUrl);

                arrayNode.add(passageNode);
            }
        }
        return arrayNode;
    }

    private JsonNode xdcRequest(String question) throws BotException {
        logger.info("xdcRequest start...");

        ObjectNode paramsNode = new ObjectMapper().createObjectNode();
        paramsNode.put("question", question);

        String responceString = okHttpClient(PATH_XDC_REQUEST, paramsNode.toString());
        logger.info("xdcRequest responceString:" + responceString);
        if (UText.isEmpty(responceString)) {
            logger.error("xdcRequest is null");
            throw new BotException("xdcRequest result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        if (rootNode == null || rootNode.get("botId") == null) {
            logger.error("xdcRequest result botId null");
            throw new BotException("xdcRequest result botId null", BotStatusCode.Error.getCode());
        }
        return rootNode;
    }

    /**
     * 질문결과 Unknown일때 답변봇 안내
     */
    public String xdcRequest(String botId, String question) {
        logger.info("xdcRequest start...");

        String answerBotId = "";
        String answer = "";
        double prob = 0;
        try {
            JsonNode answerNode = xdcRequest(question);
            answerBotId = answerNode.get("botId").asText();
            answer = answerNode.get("answer").asText();
            prob = answerNode.get("prob").asDouble(0);
        } catch (BotException bte) {
            logger.error("xdcRequest error:" + bte.getLocalizedMessage());
            return "";
        }
        if (UText.isEmpty(answerBotId) || UText.isEmpty(answer)) {
            logger.info("xdcRequest answer is null");
            return "";
        }
        if (botId.equals(answerBotId)) {
            logger.info("xdcRequest Bot is the same");
            return "";
        } else if (prob < 0.5) {
            logger.info("xdcRequest Less than 50% accuracy");
            return "";
        } else {
//            int percentage = (int) (prob * 100);
            StringBuilder sb = new StringBuilder();
            sb.append("정확한 답변을 위하여 Bot 마다 기능을 분류하고 있습니다.<br>");
            sb.append("해당 질문은 <span class='color-point'>\""+answer+"\"</span>에서 답변이 가능해요.<br>");
            sb.append("왼쪽 메뉴에서 <span class='color-point'>\""+answer+"\"</span>을 선택해주세요.");
//            sb.append(" ("+answerBotId+"-" + percentage + "%)");
            return sb.toString();
        }
    }
}
