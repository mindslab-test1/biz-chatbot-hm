package kr.co.mobis.MOBI_CHAT.user.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.user.model.BotSetting;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SettingService {
    static Logger logger = LoggerFactory.getLogger(SettingService.class);

    @Value("${front.bot.url}")
    private String BOT_URL;

    @Value("${front.bot.setting.path}")
    private String PATH_SETTING;

    @Value("${front.bot.setting.modify.path}")
    private String PATH_SETTING_MODIFY;


    private String okHttpClient(String path, Headers headers, RequestBody formBody) {
        HttpUrl httpUrl = HttpUrl.parse(BOT_URL).newBuilder()
                .encodedPath(path)
                .build();
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .postBody(formBody)
                .build();
        return botHttpClient.serverCall();
    }

    /**
     * 설정 조회
     */
    public BotSetting setting(String userId) throws BotException {
        logger.info("setting start...");
        RequestBody postBody = new FormBody.Builder()
                .add("userId", userId)
                .build();
        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/x-www-form-urlencoded")
                .build();
        String responceString = okHttpClient(PATH_SETTING, headers, postBody);
        logger.info("setting responce: " + responceString);
        if (UText.isEmpty(responceString)) {
            logger.error("setting is null");
            throw new BotException("setting api result is null", BotStatusCode.Error.getCode());
        }

        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("setting rootNode is null or not ContainerNode");
            throw new BotException("setting rootNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        // ErrorStatus Check
        if (rootNode.get("code").asInt() != 0) {
            int code = rootNode.get("code").asInt();
            String msg = rootNode.get("msg").asText();
            logger.error("setting dataNode error: (" + code + ")" + msg );
            throw new BotException(msg, code);
        }
        if (rootNode.get("data") == null || !rootNode.get("data").isContainerNode()) {
            logger.error("setting dataNode is null or not ContainerNode");
            throw new BotException("setting dataNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        JsonNode dataNode = rootNode.get("data");
        if (dataNode.get("alarms") == null || !dataNode.get("alarms").isArray()) {
            logger.error("setting alarmsNode is null or not ArrayNode");
            throw new BotException("setting alarmsNode is null or not ArrayNode", BotStatusCode.Error.getCode());
        }
        if (dataNode.get("bots") == null || !dataNode.get("bots").isArray()) {
            logger.error("setting botsNode is null or not ArrayNode");
            throw new BotException("setting botsNode is null or not ArrayNode", BotStatusCode.Error.getCode());
        }
        JsonNode alarmsNode = dataNode.get("alarms");
        JsonNode botsNode = dataNode.get("bots");

        BotSetting botSetting = BotSetting.settingParser(alarmsNode, botsNode);
        logger.info("setting botSetting: " + botSetting.toString());
        return botSetting;
    }

    /**
     * 설정정보 수정
     */
    public void updateSetting(String userId, BotSetting botSetting) throws BotException {
        logger.info("updateSetting start...");

        ObjectNode settingNode = new ObjectMapper().createObjectNode();
        settingNode.put("userId", userId);
        settingNode.set("setting", BotDataNode.toJsonNode(botSetting.toJSON()));

        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/json; charset=utf-8")
                .build();
        RequestBody postBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), settingNode.toString());
        String responceString = okHttpClient(PATH_SETTING_MODIFY, headers, postBody);
        logger.info("updateSetting responceString: " + responceString);
    }
}
