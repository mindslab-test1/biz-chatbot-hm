package kr.co.mobis.MOBI_CHAT.search.domain;

import java.util.ArrayList;
import java.util.List;

public class AnalyzeKeywords {
    private List<String> recommendedKeywords = new ArrayList<>();  // 추천키워드
    private List<String> relatedKeywords = new ArrayList<>();  // 연관키워드

    public AnalyzeKeywords() {}

    public List<String> getRecommendedKeywords() {
        return recommendedKeywords;
    }

    public void setRecommendedKeywords(List<String> recommendedKeywords) {
        this.recommendedKeywords = recommendedKeywords;
    }

    public List<String> getRelatedKeywords() {
        return relatedKeywords;
    }

    public void setRelatedKeywords(List<String> relatedKeywords) {
        this.relatedKeywords = relatedKeywords;
    }

    @Override
    public String toString() {
        return "AnalyzeKeywords{" +
                "recommendedKeywords=" + recommendedKeywords +
                ", relatedKeywords=" + relatedKeywords +
                '}';
    }
}
