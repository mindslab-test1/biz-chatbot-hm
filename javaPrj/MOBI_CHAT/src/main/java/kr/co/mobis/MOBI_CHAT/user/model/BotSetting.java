package kr.co.mobis.MOBI_CHAT.user.model;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotContext;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BotSetting implements BotContext {

    private List<SettingInfo> alarms;
    private List<SettingInfo> bots;

    public BotSetting() {}

    public List<SettingInfo> getAlarms() {
        return alarms;
    }

    public void setAlarms(List<SettingInfo> alarms) {
        this.alarms = alarms;
    }

    public List<SettingInfo> getBots() {
        return bots;
    }

    public void setBots(List<SettingInfo> bots) {
        this.bots = bots;
    }

    public static BotSetting settingParser(JsonNode alarmsNode, JsonNode botsNode) {
        BotSetting botSetting = new BotSetting();

        //-------------------------------------------------------------------------------
        // 3개 알림 구성
        List<SettingInfo> alarms = new ArrayList<>();
        int order = 0;
        for (final JsonNode alarmNode : alarmsNode) {
            SettingAlarmType alarmType = SettingAlarmType.findByAlarmType(alarmNode.get("type").asText());
            if (alarmType == null) {
                continue;
            }

            SettingInfo settingInfo = new SettingInfo();
            settingInfo.setType(alarmType.getAlarmType());
            settingInfo.setName(alarmType.getAlarmName());
            settingInfo.setFixedYn(alarmNode.get("fixedYn").asText());
            settingInfo.setShowYn(alarmNode.get("showYn").asText());
            settingInfo.setOrder(order);

            alarms.add(settingInfo);
            order++;
        }
        botSetting.setAlarms(alarms);

        //-------------------------------------------------------------------------------
        // 4개 봇 구성
        List<SettingInfo> bots = new ArrayList<>();
        for (final JsonNode botNode : botsNode) {
            SettingBotType botType = SettingBotType.findByBotType(botNode.get("type").asText());
            if (botType == null) {
                continue;
            }
            SettingInfo settingInfo = new SettingInfo();
            settingInfo.setType(botType.getBotType());
            settingInfo.setName(botType.getBotName());
            settingInfo.setFixedYn(botNode.get("fixedYn").asText());
            settingInfo.setShowYn(botNode.get("showYn").asText());
            settingInfo.setOrder(BotContext.SettingBotType.findByBotType(botType.getBotType()).getOrder());
            bots.add(settingInfo);
        }
        Collections.sort(bots, new Comparator<SettingInfo>() {
            @Override
            public int compare(SettingInfo o1, SettingInfo o2) {
                return Integer.compare(o1.getOrder(), o2.getOrder());
            }
        });
        botSetting.setBots(bots);
        return botSetting;
    }

    @Override
    public String toString() {
        return "BotSetting{" +
                "alarms=" + alarms +
                ", bots=" + bots +
                '}';
    }

    public String toJSON() {
        return BotDataNode.toJson(this);
    }
}
