package kr.co.mobis.MOBI_CHAT.search.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.search.domain.*;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SissService {
    static Logger logger = LoggerFactory.getLogger(SissService.class);

    @Value("${front.integrate.search.url}")
    private String SIS_URL;

    @Value("${front.integrate.search.similar.path}")
    private String PATH_SIS_SIMILAR;

    @Value("${front.integrate.search.same.path}")
    private String PATH_SIS_SAME;

    @Value("${front.integrate.search.pages.path}")
    private String PATH_SIS_PAGES;

    @Value("${front.integrate.search.popular.path}")
    private String PATH_SIS_POPULAR;

    @Value("${front.integrate.search.bookmark.list.path}")
    private String PATH_FAVE_LIST;

    @Value("${front.integrate.search.bookmark.add.path}")
    private String PATH_FAVE_ADD;

    @Value("${front.integrate.search.bookmark.remove.path}")
    private String PATH_FAVE_REMOVE;

    @Value("${front.integrate.search.autocomplete.path}")
    private String PATH_SIS_AUTOCOMPLETE;

    @Value("${front.integrate.search.clicklog.path}")
    private String PATH_SIS_CLICKLOG;


    private String okHttpClient(String path, String query) {
        HttpUrl httpUrl = HttpUrl.parse(SIS_URL).newBuilder()
                .encodedPath(path)
                .encodedQuery(query)
                .build();
        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/json; charset=utf-8")
                .build();
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .build();
        return botHttpClient.serverCall();
    }

    /**
     * 페이지별 보기
     */
    public SisPagesEntity pages(String userDeptCode, String userDivCode, String objectId, String q) throws BotException {
        logger.info("pages start...");

        StringBuilder query = new StringBuilder();
        query.append("userDeptCode="+userDeptCode).append("&");
        query.append("userDivCode="+userDivCode).append("&");
        query.append("id="+objectId).append("&");
        query.append("q="+q).append("&");
        query.append("size=10");

        String responceString = okHttpClient(PATH_SIS_PAGES, query.toString());
        if (UText.isEmpty(responceString)) {
            logger.error("pages is null");
            throw new BotException("pages result is null", BotStatusCode.Error.getCode());
        }
//        logger.info("pages responceString:"+responceString);
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        SisPagesEntity.dataNodeValidationCheck(rootNode);

        ObjectNode rootObjectNode = new ObjectMapper().createObjectNode();
        rootObjectNode.set("hits", rootNode.get("hits").get("hits"));
        logger.info("pages rootObjectNode: " + rootObjectNode.toString());

        return new SisPagesEntity(rootObjectNode);
    }

    /**
     * 유사문서 보기
     */
    public SisSimilarEntity similar(String userDeptCode, String userDivCode, String dedupe, String objectId, String sources) throws BotException {
        logger.info("similar start...");

        StringBuilder query = new StringBuilder();
        query.append("userDeptCode="+userDeptCode).append("&");
        query.append("userDivCode="+userDivCode).append("&");
        query.append("dedupe="+dedupe).append("&");
        query.append("id="+objectId).append("&");
        query.append("sources="+sources);

        String responceString = okHttpClient(PATH_SIS_SIMILAR, query.toString());
        if (UText.isEmpty(responceString)) {
            logger.error("similar is null");
            throw new BotException("similar result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        logger.info("similar rootNode: " + rootNode.toString());
        SisSimilarEntity.dataNodeValidationCheck(rootNode);
        return new SisSimilarEntity(rootNode);
    }

    /**
     * 중복문서 보기
     */
    public SisSameEntity same(String userDeptCode, String userDivCode, String dedupe, String objectId) throws BotException {
        logger.info("same start...");

        StringBuilder query = new StringBuilder();
        query.append("userDeptCode="+userDeptCode).append("&");
        query.append("userDivCode="+userDivCode).append("&");
        query.append("dedupe="+dedupe).append("&");
        query.append("id="+objectId).append("&");
        query.append("size="+100);

        String responceString = okHttpClient(PATH_SIS_SAME, query.toString());
        if (UText.isEmpty(responceString)) {
            logger.error("same is null");
            throw new BotException("same result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        logger.info("same rootNode: " + rootNode.toString());
        SisSameEntity.dataNodeValidationCheck(rootNode);
        return new SisSameEntity(rootNode);
    }

    /**
     * 인기검색어
     */
    public SisPopulerEntity popular(String group) throws BotException {
        logger.info("popular start...");

        StringBuilder query = new StringBuilder();
        query.append("group="+group);

        String responceString = okHttpClient(PATH_SIS_POPULAR, query.toString());
//        logger.info("popular responceString:"+responceString);
        if (UText.isEmpty(responceString)) {
            logger.error("popular is null");
            throw new BotException("popular result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        logger.info("popular rootNode: " + rootNode.toString());
        SisPopulerEntity.dataNodeValidationCheck(rootNode);
        return new SisPopulerEntity(rootNode);
    }

    /**
     * 즐겨찾기 조회
     */
    public FaveEntity faveList(String userId, int size) throws BotException {
        logger.info("faveList start...");
        StringBuilder query = new StringBuilder();
        query.append("userId="+userId).append("&");
        query.append("size="+size);

        String responceString = okHttpClient(PATH_FAVE_LIST, query.toString());
//        logger.info("faveList responceString:"+responceString);
        if (UText.isEmpty(responceString)) {
            logger.error("faveList is null");
            throw new BotException("faveList result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        logger.info("faveList rootNode:"+rootNode.toString());
        FaveEntity.dataNodeValidationCheck(rootNode);
        return new FaveEntity(rootNode);
    }

    /**
     * 즐겨찾기 저장
     */
    public FaveEntity addFave(String userId, String objectId) throws BotException {
        logger.info("addFave start...");

        StringBuilder query = new StringBuilder();
        query.append("userId="+userId).append("&");
        query.append("rObjectId="+objectId);

        String responceString = okHttpClient(PATH_FAVE_ADD, query.toString());
        if (UText.isEmpty(responceString)) {
            logger.error("addFave is null");
            throw new BotException("addFave result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("addFave rootNode is null or not ContainerNode");
            throw new BotException("addFave rootNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
//        if (!rootNode.get("success").asBoolean()) {
//            logger.error("addFave rootNode is null or not ContainerNode");
//            throw new BotException("addFave rootNode is null or not ContainerNode", BotStatusCode.ErrorStatus.getCode());
//        }
        return new FaveEntity(null);
    }

    /**
     * 즐겨찾기 삭제
     */
    public FaveEntity removeFave(String userId, String objectId) throws BotException {
        logger.error("removeFave start...");
        StringBuilder query = new StringBuilder();
        query.append("userId="+userId).append("&");
        query.append("rObjectId="+objectId);

        String responceString = okHttpClient(PATH_FAVE_REMOVE, query.toString());
        if (UText.isEmpty(responceString)) {
            logger.error("removeFave is null");
            throw new BotException("removeFave result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("removeFave rootNode is null or not ContainerNode");
            throw new BotException("removeFave rootNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
//        FaveEntity.dataNodeValidationCheck(rootNode);
        return new FaveEntity(null);
    }

    /**
     * 자동완성
     */
    public JsonNode autoComplete(String q) throws BotException {
        logger.info("autoComplete start...");

        StringBuilder query = new StringBuilder();
        query.append("q="+q);

        String responceString = okHttpClient(PATH_SIS_AUTOCOMPLETE, query.toString());
//        logger.info("popular responceString:"+responceString);
        if (UText.isEmpty(responceString)) {
            logger.error("autoComplete is null");
            throw new BotException("autoComplete result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        logger.info("autoComplete rootNode: " + rootNode.toString());
        return rootNode==null ? null : (rootNode.get("suggestions")==null ? null : rootNode.get("suggestions"));
    }

    /**
     * Click Log
     */
    public ClickLogEntity clickLog(String rObjectId, int orderNo, String keyword, String userId, String userDeptCode, String userDivCode, String sessionId) throws BotException {
        logger.info("clickLog start...");

        StringBuilder query = new StringBuilder();
        query.append("rObjectId="+rObjectId).append("&");
        query.append("orderNo="+orderNo).append("&");
        query.append("keyword="+keyword).append("&");
        query.append("userId="+userId).append("&");
        query.append("userDeptCode="+userDeptCode).append("&");
        query.append("userDivCode="+userDivCode).append("&");
        query.append("sessionId="+sessionId);

        String responceString = okHttpClient(PATH_SIS_CLICKLOG, query.toString());
//        logger.info("clickLog responceString:"+responceString);
        if (UText.isEmpty(responceString)) {
            logger.error("clickLog is null");
            throw new BotException("clickLog result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        logger.info("clickLog rootNode: " + rootNode.toString());
        return new ClickLogEntity(rootNode);
    }
}
