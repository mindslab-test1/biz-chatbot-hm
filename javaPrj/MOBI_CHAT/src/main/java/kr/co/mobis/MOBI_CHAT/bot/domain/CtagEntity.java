package kr.co.mobis.MOBI_CHAT.bot.domain;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CtagEntity extends BotDataNode {
    static Logger logger = LoggerFactory.getLogger(CtagEntity.class);

    public CtagEntity(int code, String msg) {
        super(code, msg);
    }
    public CtagEntity(JsonNode rootNode) {
        super(rootNode);
    }

    @Override
    public JsonNode dataNodeParser(JsonNode rootNode) {
        logger.info("dataNodeParser Start...");
        return null;
    }

    public static void dataNodeValidationCheck(JsonNode rootNode) throws BotException {
        logger.info("dataNodeValidationCheck start...");

        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("dataNodeValidationCheck rootNode is null or not ContainerNode");
            throw new BotException("dataNodeValidationCheck rootNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }

        // ErrorStatus Check
        if (rootNode.get("code").asInt() != 0) {
            int code = rootNode.get("code").asInt();
            String msg = rootNode.get("msg").asText();
            logger.error("dataNodeValidationCheck dataNode error: (" + code + ") " + msg );
            throw new BotException(msg, code);
        }
    }
}
