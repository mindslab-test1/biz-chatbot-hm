package kr.co.mobis.MOBI_CHAT.bot.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import kr.co.mobis.MOBI_CHAT.user.model.BotSessionInfo;

public class MaiBot {

    private String botType;
    private String accessChannel;
    private String userId;
    private String userName;
    private boolean showDebug;
    private String speaker;
    private String sessionId;
    private String timestamp;
    private String sentence;

    public MaiBot() {
        defaultSetting();
    }

    public String getBotType() {
        return botType;
    }

    public void setBotType(String botType) {
        this.botType = botType;
    }

    public String getBotId() {
        return botType.substring(2);
    }

    public String getAccessChannel() {
        return accessChannel;
    }

    public void setAccessChannel(String accessChannel) {
        this.accessChannel = accessChannel;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isShowDebug() {
        return showDebug;
    }

    public void setShowDebug(boolean showDebug) {
        this.showDebug = showDebug;
    }

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public void defaultSetting() {
        accessChannel = "WEB_BROWSER";
        showDebug = true;
        speaker = "USER";
    }

    public void setUserSessionInfo(BotSessionInfo botSessionInfo) {
        userId = botSessionInfo.getUserId();
        userName = botSessionInfo.getUserNm();
        sessionId = botSessionInfo.getBotSessionId();
    }

    @Override
    public String toString() {
        return "MaiBot{" +
                "botType='" + botType + '\'' +
                ", accessChannel='" + accessChannel + '\'' +
                ", userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", showDebug=" + showDebug +
                ", speaker='" + speaker + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", sentence='" + sentence + '\'' +
                '}';
    }

    public String toOpenJSON() {
        ObjectNode greetingNode = new ObjectMapper().createObjectNode();
        greetingNode.put("chatbotId", getBotId());
        greetingNode.put("accessChannel", accessChannel);
        greetingNode.put("showDebug", showDebug);

        ObjectNode userNode = new ObjectMapper().createObjectNode();
        userNode.put("userId", userId);
        userNode.put("userName", userName);

        greetingNode.set("user", userNode);

        return greetingNode.toString();
    }

    public String toSendJSON() {
        ObjectNode sendNode = new ObjectMapper().createObjectNode();
        sendNode.put("speaker", speaker);
        sendNode.put("userId", userId);
        sendNode.put("sessionId", sessionId);
        sendNode.put("timestamp", timestamp);
        sendNode.put("sentence", sentence);

        return sendNode.toString();
    }

    public String toCloseJSON() {
        ObjectNode closeNode = new ObjectMapper().createObjectNode();
        closeNode.put("chatbotId", getBotId());
        closeNode.put("accessChannel", accessChannel);
        closeNode.put("sessionId", sessionId);

        ObjectNode userNode = new ObjectMapper().createObjectNode();
        userNode.put("userId", userId);
        userNode.put("userName", userName);
        closeNode.set("user", userNode);

        return closeNode.toString();
    }

    public String toPingpongJSON() {
        ObjectNode sendNode = new ObjectMapper().createObjectNode();
        sendNode.put("userId", userId);
        sendNode.put("message", sentence);

        return sendNode.toString();
    }
}
