package kr.co.mobis.MOBI_CHAT.search;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import kr.co.mobis.MOBI_CHAT.MApi;
import kr.co.mobis.MOBI_CHAT.common.domain.BotContext;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.utils.UDate;
import kr.co.mobis.MOBI_CHAT.search.domain.*;
import kr.co.mobis.MOBI_CHAT.search.service.SisService;
import kr.co.mobis.MOBI_CHAT.search.service.SissService;
import kr.co.mobis.MOBI_CHAT.user.model.BotSessionInfo;
import kr.co.mobis.MOBI_CHAT.user.model.NoticeEntity;
import kr.co.mobis.MOBI_CHAT.user.model.SecurityAgreeEntity;
import kr.co.mobis.MOBI_CHAT.user.service.NoticeService;
import kr.co.mobis.MOBI_CHAT.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("search/integrate")
public class SisController {
    static Logger logger = LoggerFactory.getLogger(SisController.class);

    @Autowired
    private SisService integrateSearchService;

    @Autowired
    private SissService sissService;

    @Autowired
    private NoticeService noticeService;

    @Autowired
    private UserService userService;

    @GetMapping("/rootView")
    public String rootView(HttpSession session, Model model) {
        logger.info("rootView  Start...");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("rootView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        // 세션 시간 초기화
        session.setMaxInactiveInterval(BotContext.SESSION_INACTIVE_INTERVAL_SECOND);
        logger.info("rootView botSessionInfo: " + botSessionInfo.toString());

        model.addAttribute("botTime", UDate.nowBotTime());
        return "/view/search/integrateSearchView";
    }

    @PostMapping("/mainView")
    public String mainView(HttpSession session, @RequestBody ObjectNode viewTabsNode, Model model) {
        logger.info("mainView  Start...");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("mainView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        // 세션 시간 초기화
        session.setMaxInactiveInterval(BotContext.SESSION_INACTIVE_INTERVAL_SECOND);
        logger.info("mainView botSessionInfo: " + botSessionInfo.toString());
        logger.info("mainView viewTabsNode: " + viewTabsNode.toString());

        // 인기검색어
        SisPopulerEntity sisPopulerEntity = null;
        try {
            sisPopulerEntity = sissService.popular("");
        } catch (BotException bte) {
            logger.info("mainView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            sisPopulerEntity = new SisPopulerEntity(bte.getCode(), bte.getLocalizedMessage());
        }

        // 즐겨찾기
        FaveEntity faveEntity = null;
        try {
            faveEntity = sissService.faveList(botSessionInfo.getUserId(), 5);
        } catch (BotException bte) {
            logger.info("mainView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            faveEntity = new FaveEntity(bte.getCode(), bte.getLocalizedMessage());
        }

        // 알림(이미지)
        NoticeEntity noticeEntity = null;
        try {
            noticeEntity = noticeService.botNotice(BotContext.SettingBotType.MAIBOT.getBotId());
        } catch (BotException bte) {
            logger.info("mainView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            noticeEntity = new NoticeEntity(bte.getCode(), bte.getLocalizedMessage());
        }

        JsonNode populersNode = sisPopulerEntity.getData() == null ? null : sisPopulerEntity.getData();
        JsonNode favesNode = faveEntity.getData() == null ? null : faveEntity.getData();
        JsonNode noticeNode = noticeEntity.getNoticeNode() == null ? null : noticeEntity.getNoticeNode();
        model.addAttribute("viewTabsNode", viewTabsNode);
        model.addAttribute("populersNode", populersNode);
        model.addAttribute("favesNode", favesNode);
        model.addAttribute("noticeNode", noticeNode);
        return "/view/search/integrateSearchMainView";
    }

    @PostMapping("/listView")
    public String listView(HttpSession session, @RequestBody SisQuery sisQuery, Model model) {
        logger.info("listView Start...");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("listView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        // 세션 시간 초기화
        session.setMaxInactiveInterval(BotContext.SESSION_INACTIVE_INTERVAL_SECOND);
        logger.info("listView botSessionInfo: " + botSessionInfo.toString());
        sisQuery.setUserSessionInfo(botSessionInfo);
        if (sisQuery.getDepth() <= 2) {
            sisQuery.setSources(BotContext.SourceSystemType.toSourceSystemNames());
        }
        logger.info("listView SisQuery: " + sisQuery.toString());

        ObjectNode viewTabsNode = new ObjectMapper().createObjectNode();
        viewTabsNode.put("viewTabIndex", ""+sisQuery.getViewTabIndex());
        viewTabsNode.put("viewTabId", ""+sisQuery.getViewTabId());
        viewTabsNode.put("viewStartPage", ""+sisQuery.getViewStartPage());

        SisListEntity sisListEntity = null;
        try {
            sisListEntity = integrateSearchService.search(sisQuery);
        } catch (BotException bte) {
            logger.info("listView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
//            sisListEntity = new SisListEntity(bte.getCode(), bte.getLocalizedMessage());
            model.addAttribute("keywords", sisQuery.getQ());
            model.addAttribute("viewTabsNode", viewTabsNode);
            model.addAttribute("sisQuery", sisQuery);
            model.addAttribute("totCount", 0);
            model.addAttribute("paginationCount", 0);
            model.addAttribute("recommendedKeywords", null);
            model.addAttribute("relatedKeywords", null);
            model.addAttribute("dataNode", null);
            model.addAttribute("aggregations", null);
            return "/view/search/integrateSearchListView";
        }

        model.addAttribute("keywords", sisQuery.getQ());
        model.addAttribute("viewTabsNode", viewTabsNode);
        model.addAttribute("sisQuery", sisQuery);
        model.addAttribute("totCount", sisListEntity.getTotCount());
        model.addAttribute("paginationCount", sisListEntity.getPaginationCount());
        model.addAttribute("recommendedKeywords", sisListEntity.getAnalyzeKeywords().getRecommendedKeywords());
        model.addAttribute("relatedKeywords", sisListEntity.getAnalyzeKeywords().getRelatedKeywords());
        model.addAttribute("dataNode", sisListEntity.getData());
        model.addAttribute("aggregations", sisListEntity.getAggregations());
        return "/view/search/integrateSearchListView";
    }

    @GetMapping("/populer/list")
    public ResponseEntity<SisPopulerEntity> populerList(HttpSession session) {
        logger.info("populerList  Start...");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("populerList  no authenticated session information.");
            return ResponseEntity.ok(new SisPopulerEntity(BotStatusCode.Error_Authentication.getCode(), BotStatusCode.Error_Authentication.getMsg()));
        }
        logger.info("populerList botSessionInfo: " + botSessionInfo.toString());

        // 인기검색어
        SisPopulerEntity sisPopulerEntity = null;
        try {
            sisPopulerEntity = sissService.popular("");
        } catch (BotException bte) {
            logger.info("populerList  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            sisPopulerEntity = new SisPopulerEntity(bte.getCode(), bte.getLocalizedMessage());
        }

        return ResponseEntity.ok(sisPopulerEntity);
    }

    @GetMapping("/fave/list")
    public ResponseEntity<FaveEntity> faveList(HttpSession session) {
        logger.info("faveList  Start...");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("faveList  no authenticated session information.");
            return ResponseEntity.ok(new FaveEntity(BotStatusCode.Error_Authentication.getCode(), BotStatusCode.Error_Authentication.getMsg()));
        }
        logger.info("faveList botSessionInfo: " + botSessionInfo.toString());

        // 즐겨찾기
        FaveEntity faveEntity = null;
        try {
            faveEntity = sissService.faveList(botSessionInfo.getUserId(), 5);
        } catch (BotException bte) {
            logger.info("faveList  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            faveEntity = new FaveEntity(bte.getCode(), bte.getLocalizedMessage());
        }
        return ResponseEntity.ok(faveEntity);
    }

    @PostMapping("/fave/add")
    public ResponseEntity<FaveEntity> faveAdd(HttpSession session, @RequestBody Map<String, String> params) {
        logger.info("faveAdd  Start...");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("faveAdd  no authenticated session information.");
            return ResponseEntity.ok(new FaveEntity(BotStatusCode.Error_Authentication.getCode(), BotStatusCode.Error_Authentication.getMsg()));
        }
        logger.info("faveAdd botSessionInfo: " + botSessionInfo.toString());

        String objectId = params.get("objectId");

        FaveEntity faveEntity = null;
        try {
            faveEntity = sissService.addFave(botSessionInfo.getUserId(), objectId);
        } catch (BotException bte) {
            logger.info("faveAdd  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            faveEntity = new FaveEntity(bte.getCode(), bte.getLocalizedMessage());
        }

        return ResponseEntity.ok(faveEntity);
    }

    @PostMapping("/fave/remove")
    public ResponseEntity<FaveEntity> faveRemove(HttpSession session, @RequestBody Map<String, String> params) {
        logger.info("faveRemove  Start...");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("faveRemove  no authenticated session information.");
            return ResponseEntity.ok(new FaveEntity(BotStatusCode.Error_Authentication.getCode(), BotStatusCode.Error_Authentication.getMsg()));
        }
        logger.info("faveRemove botSessionInfo: " + botSessionInfo.toString());

        String objectId = params.get("objectId");

        FaveEntity faveEntity = null;
        try {
            faveEntity = sissService.removeFave(botSessionInfo.getUserId(), objectId);
        } catch (BotException bte) {
            logger.info("faveRemove  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            faveEntity = new FaveEntity(bte.getCode(), bte.getLocalizedMessage());
        }

        return ResponseEntity.ok(faveEntity);
    }

    @GetMapping("/autocomplete")
    public ResponseEntity<JsonNode> autoComplete(@RequestParam String q) {
        logger.info("autoComplete  Start...");
        logger.info("autoComplete  q:"+q);
        JsonNode autoCompleteNode = sissService.autoComplete(q);
        return ResponseEntity.ok(autoCompleteNode);
    }

    @PostMapping("/clicklog")
    public ResponseEntity<ClickLogEntity> clicklog(HttpSession session, @RequestBody Map<String, String> params) {
        logger.info("clicklog  Start...");
        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("clicklog  no authenticated session information.");
            return ResponseEntity.ok(new ClickLogEntity(BotStatusCode.Error_Authentication.getCode(), BotStatusCode.Error_Authentication.getMsg()));
        }
        logger.info("clicklog botSessionInfo: " + botSessionInfo.toString());

        String rObjectId = params.get("rObjectId");
        String orderNo = params.get("orderNo");
        String keyword = params.get("keyword");
        String userId = botSessionInfo.getUserId();
        String userDeptCode = botSessionInfo.getDeptCd();
        String userDivCode = botSessionInfo.getUserDivCode();
        String sessionId = botSessionInfo.getBotSessionId();

        ClickLogEntity clickLogEntity = null;
        try {
            clickLogEntity = sissService.clickLog(rObjectId, Integer.valueOf(orderNo), keyword, userId, userDeptCode, userDivCode, sessionId);
        } catch (BotException bte) {
            logger.info("clicklog  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            clickLogEntity = new ClickLogEntity(bte.getCode(), bte.getLocalizedMessage());
        }
        return ResponseEntity.ok(clickLogEntity);
    }

    @PostMapping("/security/agreement")
    public ResponseEntity<SecurityAgreeEntity> securityAgreement(HttpSession session) {
        logger.info("securityAgreement  Start...");
        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("securityAgreement  no authenticated session information.");
            return ResponseEntity.ok(new SecurityAgreeEntity(BotStatusCode.Error_Authentication.getCode(), BotStatusCode.Error_Authentication.getMsg()));
        }
        session.setMaxInactiveInterval(BotContext.SESSION_INACTIVE_INTERVAL_SECOND);
        logger.info("securityAgreement botSessionInfo: " + botSessionInfo.toString());

        SecurityAgreeEntity securityAgreeEntity = null;
        try {
            securityAgreeEntity = userService.securityAgreement(botSessionInfo.getUserId());
            botSessionInfo.setConfirmYn("Y");
            session.setAttribute("botSessionInfo", botSessionInfo);
        } catch (BotException bte) {
            logger.info("securityAgreement  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            securityAgreeEntity = new SecurityAgreeEntity(bte.getCode(), bte.getLocalizedMessage());
        }
        return ResponseEntity.ok(securityAgreeEntity);
    }
}
