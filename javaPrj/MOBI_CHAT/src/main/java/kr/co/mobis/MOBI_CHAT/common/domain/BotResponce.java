package kr.co.mobis.MOBI_CHAT.common.domain;

public interface BotResponce<T> {

    int getCode();
    String getMsg();
    T getData();
}
