package kr.co.mobis.MOBI_CHAT.bot.service;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.bot.domain.CtagEntity;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CtagService {
    static Logger logger = LoggerFactory.getLogger(CtagService.class);

    @Value("${front.bot.url}")
    private String BOT_URL;

    private String okHttpClient(String origin, String path, String jsonBody) {
        HttpUrl httpUrl = HttpUrl.parse(BOT_URL).newBuilder()
                .encodedPath(path)
                .build();
        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/json; charset=utf-8")
                .build();
        RequestBody postBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonBody);
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .postBody(postBody)
                .build();
        return botHttpClient.serverCall();
    }

    /**
     * CTAG RPA
     */
    public CtagEntity ctagRestApiRpa(String origin, String pathname, String postBody) throws BotException {
        logger.info("ctagRestApiRpa start...");

        String responceString = okHttpClient(origin, pathname, postBody);
        logger.info("ctagRestApiRpa responceString: " + responceString);
        if (UText.isEmpty(responceString)) {
            logger.error("ctagRestApiRpa is null");
            throw new BotException("ctagRestApiRpa result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        CtagEntity.dataNodeValidationCheck(rootNode);
        return new CtagEntity(rootNode);
    }
}
