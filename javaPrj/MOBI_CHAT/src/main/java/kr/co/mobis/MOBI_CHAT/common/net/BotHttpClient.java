package kr.co.mobis.MOBI_CHAT.common.net;

import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import okhttp3.*;
import okio.Buffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BotHttpClient {
    static Logger logger = LoggerFactory.getLogger(BotHttpClient.class);

    private HttpUrl httpUrl;
    private Headers headers;
    private RequestBody body;


    public BotHttpClient(Builder builder) {
        this.httpUrl = builder.httpUrl;
        this.headers = builder.headers;
        this.body = builder.body;
    }

    public String serverCall() throws BotException {
        logger.info("serverCall start...");
        logger.info("serverCall httpUrl: " + httpUrl.toString());

        String responceString;
        try {
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(1000L, TimeUnit.MILLISECONDS)
                    .writeTimeout(15000L, TimeUnit.MILLISECONDS)
                    .readTimeout(15000L, TimeUnit.MILLISECONDS)
                    .build();

            Request.Builder requestBuilder = new Request.Builder()
                    .url(httpUrl)
                    .headers(headers);

            if (body != null) {
                logger.info("serverCall body: " + bodyString(body));
                requestBuilder.post(body);
            }

            Request request = requestBuilder.build();

            Response response = client.newCall(request).execute();
            if (response == null || !response.isSuccessful()) {
                logger.error("serverCall response error:" + response.code() + "|" + response.message());
                throw new BotException(response.code() + " " + response.message(), BotStatusCode.Error.getCode());
            } else {
                responceString = response.body().string();
                logger.info("serverCall response success !!!");
//                response.close();
            }
            return responceString;
        } catch (BotException bte) {
            throw bte;
        } catch (RuntimeException rte) {
            logger.error("serverCall okhttp RuntimeException: " + rte.getLocalizedMessage());
            throw new BotException(rte, BotStatusCode.Error_IO.getCode());
        } catch (IOException ioe) {
            logger.error("serverCall okhttp IOException: " + ioe.getLocalizedMessage());
            throw new BotException(ioe, BotStatusCode.Error_IO_Network.getCode());
//            if (ioe instanceof SocketTimeoutException) {
//                throw new BotException(ioe, BotStatusCode.ErrorStatus.getCode());
//            } else {
//                throw new BotException(ioe, BotStatusCode.ErrorStatus.getCode());
//            }
        }
    }

    public static final class Builder {
        HttpUrl httpUrl;
        Headers headers;
        RequestBody body;

        /**
         * Url
         */
        public BotHttpClient.Builder httpUrl(HttpUrl httpUrl) {
            if (httpUrl == null) throw new NullPointerException("builder == null");
            this.httpUrl = httpUrl;
            return this;
        }

        /**
         * Headers
         */
        public BotHttpClient.Builder headers(Headers headers) {
            if (headers == null) throw new NullPointerException("headers == null");
            this.headers = headers;
            return this;
        }

        /**
         * Body
         */
        public BotHttpClient.Builder postBody(RequestBody body) {
            if (body == null) throw new NullPointerException("body == null");

            this.body = body;
            return this;
        }

        public BotHttpClient build() {
            if (httpUrl == null) throw new IllegalStateException("httpUrl == null");
            if (headers == null) throw new IllegalStateException("headers == null");
//            if (headers == null) {
//                Headers headers = new Headers.Builder()
//                        .add("Content-Type", "application/json; charset=utf-8")
//                        .build();
//                this.headers = headers;
//            } else {
//                this.headers = new Headers.Builder()
//                        .addAll(headers).set("Content-Type", "application/json; charset=utf-8")
//                        .build();
//            }
            return new BotHttpClient(this);
        }
    }

    private String bodyString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            copy.writeTo(buffer);
            return buffer.readUtf8();
        } catch (IOException e) {
            return "";
        }
    }
}
