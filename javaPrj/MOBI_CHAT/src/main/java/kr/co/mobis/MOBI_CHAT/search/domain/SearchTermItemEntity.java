package kr.co.mobis.MOBI_CHAT.search.domain;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchTermItemEntity extends BotDataNode {
    static Logger logger = LoggerFactory.getLogger(SearchTermItemEntity.class);

    public SearchTermItemEntity(int code, String msg) {
        super(code, msg);
    }
    public SearchTermItemEntity(JsonNode rootNode) {
        super(rootNode);
    }

    @Override
    public JsonNode dataNodeParser(JsonNode rootNode) {
        logger.info("dataNodeParser Start...");

        this.code = 0;
        this.msg = "";

        JsonNode dataNode = rootNode.get("restTermVO");
        logger.info("dataNodeParser dataNode: " + dataNode.toString());

        if (dataNode == null || !dataNode.isContainerNode()) {
            return null;
        }
        logger.info("dataNodeParser End. ");

        return dataNode;
    }
}
