package kr.co.mobis.MOBI_CHAT.bot.service;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.bot.domain.MaiBot;
import kr.co.mobis.MOBI_CHAT.bot.domain.PingpongEntity;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PingpongService {
    static Logger logger = LoggerFactory.getLogger(PingpongService.class);

    @Value("${front.bot.url}")
    private String BOT_URL;

    @Value("${front.bot.pingpong.path}")
    private String PATH_CHAT_PINGPONG;
    
    private String okHttpClient(String path, RequestBody postBody) {
        HttpUrl httpUrl = HttpUrl.parse(BOT_URL).newBuilder()
                .encodedPath(path)
                .build();
        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/x-www-form-urlencoded")
                .build();
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .postBody(postBody)
                .build();
        return botHttpClient.serverCall();
    }

    public PingpongEntity question(MaiBot maiBot) throws BotException {
        logger.info("question start...");
        RequestBody postBody = new FormBody.Builder()
                .add("userId", maiBot.getUserId())
                .add("message", maiBot.getSentence())
                .build();

        String responceString = okHttpClient(PATH_CHAT_PINGPONG, postBody);
        if (UText.isEmpty(responceString)) {
            logger.error("question responce is null");
            throw new BotException("question responce is null", BotStatusCode.Error.getCode());
        }

        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        PingpongEntity.dataNodeValidationCheck(rootNode);
        logger.info("question rootNode: " + rootNode.toString());
        return new PingpongEntity(rootNode);
    }
}
