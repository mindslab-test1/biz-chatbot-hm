package kr.co.mobis.MOBI_CHAT;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotContext;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.search.domain.*;
import kr.co.mobis.MOBI_CHAT.search.service.MrcService;
import kr.co.mobis.MOBI_CHAT.search.service.SisService;
import kr.co.mobis.MOBI_CHAT.search.service.SissService;
import kr.co.mobis.MOBI_CHAT.user.model.BotSessionInfo;
import kr.co.mobis.MOBI_CHAT.user.model.BotSetting;
import kr.co.mobis.MOBI_CHAT.user.service.SettingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("modal")
public class ModalController {
    static Logger logger = LoggerFactory.getLogger(ModalController.class);

    @Autowired
    private SettingService settingService;

    @Autowired
    private SisService integrateSearchService;

    @Autowired
    private SissService sissService;

    @Autowired
    private MrcService mrcService;

    /**
     * 봇 최근대화목록View
     */
    @GetMapping("/conversationHistoryView")
    public String conversationHistoryView(HttpSession session, Model model) {
        logger.info("conversationHistoryView start");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("conversationHistoryView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("conversationHistoryView botSessionInfo: " + botSessionInfo.toString());

        return "/view/modal/conversationHistoryView";
    }

    /**
     * 봇 의견등록View
     */
    @GetMapping("/opinionView")
    public String opinionView(HttpSession session, Model model) {
        logger.info("opinionView start");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("opinionView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("opinionView botSessionInfo: " + botSessionInfo.toString());

        return "/view/modal/opinionView";
    }

    /**
     * 봇 도움말View
     */
    @GetMapping("/helpView")
    public String helpView(HttpSession session, Model model) {
        logger.info("helpView start");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("helpView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("helpView botSessionInfo: " + botSessionInfo.toString());

        return "/view/modal/helpView";
    }

    /**
     * 사용자설정View
     */
    @GetMapping("/settingView")
    public String settingView(HttpSession session, Model model) {
        logger.info("settingView start");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("settingView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("settingView botSessionInfo: " + botSessionInfo.toString());

        BotSetting botSetting = null;
        try {
            botSetting = settingService.setting(botSessionInfo.getUserId());
        } catch (BotException bte) {
            logger.info("settingView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            model.addAttribute("alarms", null);
            model.addAttribute("bots", null);
            return "/view/modal/settingView";
        }

        model.addAttribute("alarms", botSetting.getAlarms());
        model.addAttribute("bots", botSetting.getBots());
        return "/view/modal/settingView";
    }

    /**
     * 셔틀버스 View
     */
    @PostMapping("/modalShuttleBusView")
    public String modalShuttleBusView(HttpSession session, @RequestBody JsonNode shuttleBusNode, Model model) {
        logger.info("modalShuttleBusView  Start...");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("modalShuttleBusView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("modalShuttleBusView botSessionInfo: " + botSessionInfo.toString());
        logger.info("modalShuttleBusView shuttleBusNode: " + shuttleBusNode.toString());

        model.addAttribute("shuttleBusNode", shuttleBusNode);
        return "/view/modal/modalShuttleBusView";
    }

    /**
     * 즐겨찾기 문서 View
     */
    @GetMapping("/faveDocumentView")
    public String faveDocumentView(HttpSession session, Model model) {
        logger.info("faveDocumentView start");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("faveDocumentView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("faveDocumentView botSessionInfo: " + botSessionInfo.toString());

        FaveEntity faveEntity = null;
        try {
            faveEntity = sissService.faveList(botSessionInfo.getUserId(), 100);
        } catch (BotException bte) {
            logger.info("faveDocumentView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            faveEntity = new FaveEntity(bte.getCode(), bte.getLocalizedMessage());
        }

        JsonNode dataNode = faveEntity.getData();
        model.addAttribute("dataNode", dataNode);
        return "/view/modal/faveDocumentView";
    }

    /**
     * 검색 페이지별보기 View
     */
    @PostMapping("/search/pageView")
    public String pageView(HttpSession session, @RequestBody Map<String, String> params, Model model) {
        logger.info("pageView start");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("pageView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("pageView botSessionInfo: " + botSessionInfo.toString());

        String objectId = params.get("objectId");
        String q = params.get("q");

        SisPagesEntity SisPagesEntity = null;
        try {
            SisPagesEntity = sissService.pages(botSessionInfo.getDeptCd(), botSessionInfo.getUserDivCode(), objectId, q);
        } catch (BotException bte) {
            logger.info("pageView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
//            SisPagesEntity = new SisPagesEntity(bte.getCode(), bte.getLocalizedMessage());
            model.addAttribute("totalCount", 0);
            model.addAttribute("dataNode", null);
            return "/view/modal/searchPageView";
        }

        JsonNode dataNode = SisPagesEntity.getData();
        model.addAttribute("totalCount", (dataNode == null ? 0 : dataNode.size()));
        model.addAttribute("dataNode", dataNode);
        return "/view/modal/searchPageView";
    }

    /**
     * 검색 유사문서 View
     */
    @PostMapping("/search/similarView")
    public String similarView(HttpSession session, @RequestBody Map<String, String> params, Model model) {
        logger.info("similarView start");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("similarView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("similarView botSessionInfo: " + botSessionInfo.toString());

        String dedupe = params.get("dedupe");
        String objectId = params.get("objectId");
        String sources = params.get("sources");

        SisSimilarEntity sisSimilarEntity = null;
        try {
            sisSimilarEntity = sissService.similar(botSessionInfo.getDeptCd(), botSessionInfo.getUserDivCode(), dedupe, objectId, sources);
        } catch (BotException bte) {
            logger.info("similarView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            sisSimilarEntity = new SisSimilarEntity(bte.getCode(), bte.getLocalizedMessage());
        }

        JsonNode dataNode = sisSimilarEntity.getData();
        model.addAttribute("totalCount", (dataNode == null ? 0 : dataNode.size()));
        model.addAttribute("dataNode", dataNode);
        return "/view/modal/searchSimilarView";
    }

    /**
     * 검색 중복문서 View
     */
    @PostMapping("/search/sameView")
    public String sameView(HttpSession session, @RequestBody Map<String, String> params, Model model) {
        logger.info("sameView start");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("sameView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("sameView botSessionInfo: " + botSessionInfo.toString());

        String dedupe = params.get("dedupe");
        String objectId = params.get("objectId");

        SisSameEntity sisSameEntity = null;
        try {
            sisSameEntity = sissService.same(botSessionInfo.getDeptCd(), botSessionInfo.getUserDivCode(), dedupe, objectId);
        } catch (BotException bte) {
            logger.info("sameView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            sisSameEntity = new SisSameEntity(bte.getCode(), bte.getLocalizedMessage());
        }

        JsonNode dataNode = sisSameEntity.getData();
        model.addAttribute("totalCount", (dataNode == null ? 0 : dataNode.size()));
        model.addAttribute("dataNode", dataNode);
        return "/view/modal/searchSameView";
    }

    /**
     * 검색 MRC View
     */
    @PostMapping("/search/mrcPageView")
    public String mrcPageView(HttpSession session, @RequestBody JsonNode paramsNode, Model model) {
        logger.info("mrcPageView start");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("mrcPageView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("mrcPageView botSessionInfo: " + botSessionInfo.toString());
        logger.info("mrcPageView paramsNode: " + paramsNode.toString());
        String question = paramsNode.get("question").asText();
        List<String> objectIds = new ArrayList<>();
        for (JsonNode itemNode : paramsNode.get("objectIds")) {
            objectIds.add(itemNode.asText());
        }

        MrcEntity mrcEntity = null;
        if (UText.isEmpty(question) || objectIds.size() == 0) {
            mrcEntity = new MrcEntity(BotStatusCode.Error.getCode(), BotStatusCode.Error.getMsg());
        } else {
            try {
                mrcEntity = mrcService.searchMrcList(objectIds.stream().toArray(String[]::new), question);
            } catch (BotException bte) {
                logger.info("mrcPageView  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
                mrcEntity = new MrcEntity(bte.getCode(), bte.getLocalizedMessage());
            }
        }

        JsonNode dataNode = mrcEntity.getData();
        model.addAttribute("totalCount", (dataNode == null ? 0 : dataNode.size()));
        model.addAttribute("dataNode", dataNode);
        return "/view/modal/mrcPageView";
    }

    /**
     * 검색 Search MRC View
     */
    @PostMapping("/search/searchUnknownMrcPageView")
    public String searchUnknownMrcPageView(HttpSession session, @RequestBody JsonNode dataNode, Model model) {
        logger.info("searchUnknownMrcPageView start");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("searchUnknownMrcPageView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("searchUnknownMrcPageView botSessionInfo: " + botSessionInfo.toString());
        logger.info("searchUnknownMrcPageView dataNode:"+dataNode.toString());

        model.addAttribute("totalCount", (dataNode == null ? 0 : dataNode.size()));
        model.addAttribute("dataNode", dataNode);
        return "/view/modal/mrcPageView";
    }

    /**
     * 보안 서약 View
     */
    @GetMapping("/search/securityAgreeView")
    public String securityAgreeView(HttpSession session, Model model) {
        logger.info("securityAgreeView start");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("securityAgreeView  no authenticated session information.");
            return MApi.errView(model, BotContext.ErrorStatus.ERROR_SESSION_EXPIRATION).toPath();
        }
        logger.info("securityAgreeView botSessionInfo: " + botSessionInfo.toString());

        return "/view/modal/securityAgreeView";
    }
}
