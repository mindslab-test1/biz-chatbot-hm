package kr.co.mobis.MOBI_CHAT.user.model;

import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;

public class BotUser {

    private String userId;
    private String userNm;
    private String userEmail;
    private String userMobile;
    private String userTelephone;
    private String deptCd;
    private String deptNm;

    public BotUser() {}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserNm() {
        return userNm;
    }

    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserTelephone() {
        return userTelephone;
    }

    public void setUserTelephone(String userTelephone) {
        this.userTelephone = userTelephone;
    }

    public String getDeptCd() {
        return deptCd;
    }

    public void setDeptCd(String deptCd) {
        this.deptCd = deptCd;
    }

    public String getDeptNm() {
        return deptNm;
    }

    public void setDeptNm(String deptNm) {
        this.deptNm = deptNm;
    }

    @Override
    public String toString() {
        return "BotUser{" +
                "userId='" + userId + '\'' +
                ", userNm='" + userNm + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userMobile='" + userMobile + '\'' +
                ", userTelephone='" + userTelephone + '\'' +
                ", deptCd='" + deptCd + '\'' +
                ", deptNm='" + deptNm + '\'' +
                '}';
    }

    public String toJSON() {
        return BotDataNode.toJson(this);
    }

}
