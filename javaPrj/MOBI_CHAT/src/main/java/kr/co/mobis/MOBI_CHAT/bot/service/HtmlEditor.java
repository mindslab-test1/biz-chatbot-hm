package kr.co.mobis.MOBI_CHAT.bot.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HtmlEditor {
    static Logger logger = LoggerFactory.getLogger(HtmlEditor.class);

    private static HtmlEditor instance;

    public static ComponentView with(Object componentNode) {
        if (instance == null) {
            instance = new HtmlEditor();
        }
        if (componentNode instanceof JsonNode) {
            return instance.componentView((JsonNode)componentNode);
        } else {
            return instance.componentView(BotDataNode.toJsonNode((String)componentNode));
        }
    }

    private ComponentView componentView(JsonNode componentNode) {
        return new ComponentView(componentNode);
    }

    public class ComponentView {
        private JsonNode rootNode;
        public ComponentView(JsonNode rootNode) {
            this.rootNode = rootNode;
        }

        public JsonNode toHtmlDocument() {
            if (rootNode == null) {
                return null;
            }
            for (JsonNode buttonNode : rootNode.get("buttons")) {
                String title = buttonNode.get("title").asText();
                StringBuilder sbTitle = new StringBuilder(title);
                UText.replaceAll(sbTitle, "\r\n", "<br/>");
                UText.replaceAll(sbTitle, "\\r\\n", "<br/>");
                UText.replaceAll(sbTitle, "\n", "<br/>");
                UText.replaceAll(sbTitle, "\\n", "<br/>");
                UText.replaceAll(sbTitle, "\r", "<br/>");
                UText.replaceAll(sbTitle, "\\r", "<br/>");
                ((ObjectNode) buttonNode).put("title", sbTitle.toString());
            }
            return rootNode;
        }
    }
}
