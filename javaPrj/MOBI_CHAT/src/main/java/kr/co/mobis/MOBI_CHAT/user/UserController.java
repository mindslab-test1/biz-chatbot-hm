package kr.co.mobis.MOBI_CHAT.user;

import kr.co.mobis.MOBI_CHAT.common.domain.BotContext;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.user.model.BotSessionInfo;
import kr.co.mobis.MOBI_CHAT.user.model.BotSetting;
import kr.co.mobis.MOBI_CHAT.user.model.OpinionEntity;
import kr.co.mobis.MOBI_CHAT.user.service.OpinionService;
import kr.co.mobis.MOBI_CHAT.user.service.SettingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("user")
public class UserController {
    static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private SettingService settingService;

    @Autowired
    private OpinionService opinionService;

    @PostMapping("/setting/modify")
    @ResponseBody
    public ResponseEntity<?> modifySetting(HttpSession session, @RequestBody BotSetting botSetting) {
        logger.info("modifySetting start");
        logger.info("modifySetting botSetting: " + botSetting.toString());

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("modifySetting  no authenticated session information.");
            return ResponseEntity.noContent().build();
        }
        logger.info("modifySetting botSessionInfo: " + botSessionInfo.toString());

        try {
            settingService.updateSetting(botSessionInfo.getUserId(), botSetting);
        } catch (BotException bte) {
            logger.info("modifySetting  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
        }

        session.setMaxInactiveInterval(BotContext.SESSION_INACTIVE_INTERVAL_SECOND);

        return ResponseEntity.noContent().build();
    }

    @PostMapping("/opinion/save")
    @ResponseBody
    public ResponseEntity<OpinionEntity> saveOpinion(HttpSession session, @RequestBody Map<String, String> params) {
        logger.info("saveOpinion  Start...");

        BotSessionInfo botSessionInfo = (BotSessionInfo) session.getAttribute("botSessionInfo");
        if (botSessionInfo == null) {
            logger.info("saveOpinion  no authenticated session information.");
            return ResponseEntity.ok(new OpinionEntity(BotStatusCode.Error_Authentication.getCode(), BotStatusCode.Error_Authentication.getMsg()));
        }
        logger.info("saveOpinion botSessionInfo: " + botSessionInfo.toString());

        String botId = params.get("botId");
        String channel = params.get("channel");
        String title = params.get("title");
        String content = params.get("content");

        OpinionEntity opinionEntity = null;
        try {
            opinionEntity = opinionService.saveOpinion(botSessionInfo.getUserId(), botId, channel, title, content);
        } catch (BotException bte) {
            logger.info("saveOpinion  error: " + bte.getCode() + " msg: " + bte.getLocalizedMessage());
            opinionEntity = new OpinionEntity(bte.getCode(), bte.getLocalizedMessage());
        }

        return ResponseEntity.ok(opinionEntity);
    }

}
