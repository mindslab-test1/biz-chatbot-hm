package kr.co.mobis.MOBI_CHAT.common.domain;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public interface BotContext {

    // TODO 운영 반영시 true
    static final boolean ONLY_IE = false;

    static final int SESSION_INACTIVE_INTERVAL_SECOND = 20*60;

    enum ErrorStatus {
        ERROR_UNKNOWN,
        ERROR_AUTHENTICATION,
        ERROR_SESSION_EXPIRATION,
    }

    enum SettingBotType {
        MAIBOT("BK59", "회사생활봇", 0),
        BUSINESSBOT("BK56", "경영지원봇", 1),
        ITHELPBOT("BK58", "IT지원봇", 2),
        SEARCH("BK62", "문서검색봇", 3),
        PINGPONG("BK99", "재미봇", 4);

        private final String botType;
        private final String botName;
        private final int order;

        SettingBotType(String botType, String botName, int order) {
            this.botType = botType;
            this.botName = botName;
            this.order = order;
        }

        public String getBotType() {
            return botType;
        }

        public String getBotName() {
            return botName;
        }

        public int getOrder() {
            return order;
        }

        public String getBotId() {
            return botType.substring(2);
        }

        public static SettingBotType findByBotType(String botType) {
            return Arrays.stream(SettingBotType.values())
                    .filter(settingBotType -> settingBotType.getBotType().equals(botType))
                    .findFirst()
                    .orElse(null);
        }

        public static List<SettingBotType> orderSort(List<SettingBotType> sortList) {
            SettingBotType[] sortArray = sortList.stream().toArray(SettingBotType[]::new);
            return orderSort(sortArray);
        }
        public static List<SettingBotType> orderSort(SettingBotType[] sortArray) {
            Arrays.sort(sortArray, Comparator.comparing(SettingBotType::getOrder));
            return Arrays.asList(sortArray);
        }
    }

    enum SettingAlarmType {
        NOTICE("US0001", "공지사항"),
        MAIL("US0002", "메일"),
        APPROVAL("US0004", "결재");

        private final String alarmType;
        private final String alarmName;

        SettingAlarmType(String alarmType, String alarmName) {
            this.alarmType = alarmType;
            this.alarmName = alarmName;
        }

        public String getAlarmType() {
            return alarmType;
        }

        public String getAlarmName() {
            return alarmName;
        }

        public static SettingAlarmType findByAlarmType(String alarmType) {
            return Arrays.stream(SettingAlarmType.values())
                    .filter(settingAlarmType -> settingAlarmType.getAlarmType().equals(alarmType))
                    .findFirst()
                    .orElse(null);
        }
    }

    // SYS_UTTER_TYPE
    static final String SYS_UTTER_TYPE_NORMAL = "NORMAL";
    static final String SYS_UTTER_TYPE_GREETING = "GREETING";  // greetingView에서만 사용됨
    static final String SYS_UTTER_TYPE_SHUTTLE_BUS = "SHUTTLE_BUS";
    static final String SYS_UTTER_TYPE_CAFETERIA_MENU = "CAFETERIA_MENU";
    static final String SYS_UTTER_TYPE_EMPLOYEE_INFO = "EMPLOYEE_INFO";
    static final String SYS_UTTER_TYPE_BUSSTOP_LOC = "BUSSTOP_LOC";
    static final String SYS_UTTER_TYPE_EMAIL_CHECK = "EMAIL_CHECK";
    static final String SYS_UTTER_TYPE_APPROVAL_CHECK = "APPROVAL_CHECK";
    static final String SYS_UTTER_TYPE_BQA_RECOMMEND = "BQA_RECOMMEND";
    static final String SYS_UTTER_TYPE_WEATHER = "WEATHER";
    static final String SYS_UTTER_TYPE_IMOBIS_MENU = "IMOBIS_MENU";
    static final String SYS_UTTER_TYPE_QMS_MENU = "QMS_MENU";


    // RICH_CONTENT_TYPE
    static final String RICH_CONTENT_TYPE_BUTTON = "RT0001";
    static final String RICH_CONTENT_TYPE_IMAGE = "RT0002";
    static final String RICH_CONTENT_TYPE_CAROUSEL = "RT0003";
    static final String RICH_CONTENT_TYPE_API = "RT9001";
    static final String RICH_CONTENT_TYPE_NONE = "NONE";

    static final String MSG_TYPE_GREETING = "MT0001";
    static final String MSG_TYPE_UNKNOWN = "MT0002";
    static final String MSG_TYPE_SDS_RECONFIRM = "MT0003";


    // 봇 컴포넌트뷰 타입
    enum BotComponentViewType {
        WARING_VIEW,
        NORMAL_VIEW,
        BUTTON_VIEW,
        POPUP_LINK_VIEW,
        POPUP_IMAGE_LINK_VIEW,
        CAFETERIA_MENU_VIEW,
        SHUTTLE_BUS_VIEW,
        MRC_VIEW;
    }
    // 봇 컴포넌트뷰 스타일
    enum BotComponentViewStyle {
        UNKNOWN,
        EMAIL,
        APPROVAL,
        EMPLOYEE,
        SHUTTLE_BUS,
        BUSSTOP_LOC,
        CAFETERIA_MENU,
        BQA_RECOMMEND,
        WEATHER,
        IMOBIS_MENU,
        QMS_MENU,
        MRC;
    }

    static final String TAG_CTAG_START = "$$CTAG";
    static final String TAG_CTAG_END = "CTAG$$";
    static final String[] COMPARE_TAGS = {TAG_CTAG_START};
    static final String TAG_CTAG_TYPE_OUTLINK = "outlink";
    static final String TAG_CTAG_TYPE_MAILLINK = "maillink";
    static final String TAG_CTAG_TYPE_RESTAPI_RPA = "restapi-rpa";
    static final String TAG_REFLECT_SESSION_USER_ID = "SESSION_USER_ID";



    // 검색 소스시스템구분
    enum SourceSystemType {
        MCLOUD("Mcloud", 1),
        APPROVE("전사문서", 2),
        WIKI("백과사전", 3),
        INTEGRITY("Integrity", 4);
//        QMS("QMS", 5);

        private final String sourceSystemName;
        private final int order;

        SourceSystemType(String sourceSystemName, int order) {
            this.sourceSystemName = sourceSystemName;
            this.order = order;
        }

        public String getSourceSystemName() {
            return sourceSystemName;
        }

        public int getOrder() {
            return order;
        }

        public static String[] toSourceSystemNames() {
            return Arrays.stream(SourceSystemType.values()).map(item -> item.name()).toArray(String[]::new);
        }

        public static List<SourceSystemType> orderSort(List<SourceSystemType> sortList) {
            SourceSystemType[] sortArray = sortList.stream().toArray(SourceSystemType[]::new);
            return orderSort(sortArray);
        }

        public static List<SourceSystemType> orderSort(SourceSystemType[] sortArray) {
            Arrays.sort(sortArray, Comparator.comparing(SourceSystemType::getOrder));
            return Arrays.asList(sortArray);
        }
    }
}
