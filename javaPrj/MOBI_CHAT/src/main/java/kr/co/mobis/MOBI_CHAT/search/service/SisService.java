package kr.co.mobis.MOBI_CHAT.search.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.search.domain.SisListEntity;
import kr.co.mobis.MOBI_CHAT.search.domain.SisQuery;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SisService {
    static Logger logger = LoggerFactory.getLogger(SisService.class);

    @Value("${front.integrate.search.url}")
    private String SIS_URL;

    @Value("${front.integrate.search.search.path}")
    private String PATH_SIS_SEARCH;

    @Value("${front.integrate.search.related.path}")
    private String PATH_SIS_RELATED;

    @Value("${front.integrate.search.mrc.path}")
    private String PATH_SIS_MRC;

    private String okHttpClient(String path, String query) {
        HttpUrl httpUrl = HttpUrl.parse(SIS_URL).newBuilder()
                .encodedPath(path)
                .encodedQuery(query)
                .build();
        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/json; charset=utf-8")
                .build();
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .build();
        return botHttpClient.serverCall();
    }

    public JsonNode generalSearch(SisQuery sisQuery) throws BotException {
        logger.info("search generalSearch...");

        String responceString = okHttpClient(PATH_SIS_SEARCH, sisQuery.toQuery());
        if (UText.isEmpty(responceString)) {
            logger.error("generalSearch is null");
            throw new BotException("generalSearch result is null", BotStatusCode.Error.getCode());
        }
        logger.info("search responceString:"+responceString);
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        JsonNode dataNode = null;
        if (rootNode != null && rootNode.get("hits") != null && rootNode.get("hits").get("hits") != null) {
            dataNode = rootNode.get("hits").get("hits");
        }
        return dataNode;
    }

    /**
     * 키워드 검색
     */
    public SisListEntity search(SisQuery sisQuery) throws BotException {
        logger.info("search start...");

        String responceString = okHttpClient(PATH_SIS_SEARCH, sisQuery.toQuery());
        if (UText.isEmpty(responceString)) {
            logger.error("search is null");
            throw new BotException("search result is null", BotStatusCode.Error.getCode());
        }
        logger.info("search responceString:"+responceString);
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        SisListEntity.oneDataNodeValidationCheck(rootNode);
        // 연관키워드, 추천키워드 가져오기
        JsonNode wordAnalyzeNode = related(sisQuery.getQ(), "");
        SisListEntity.twoDataNodeValidationCheck(wordAnalyzeNode);

        ObjectNode rootObjectNode = new ObjectMapper().createObjectNode();
        rootObjectNode.put("totCount", rootNode.get("hits").get("total").asInt());
        rootObjectNode.put("paginationCount", rootNode.get("aggregations").get("cardinality#total").get("value").asInt());
        rootObjectNode.set("hits", rootNode.get("hits").get("hits"));
        rootObjectNode.set("aggregations", rootNode.get("aggregations"));
        rootObjectNode.set("analyzeKeywords", wordAnalyzeNode);
        logger.info("search rootObjectNode hits count: " + rootNode.get("hits").get("hits").size());
        logger.info("search rootObjectNode: " + rootObjectNode.toString());

        return new SisListEntity(rootObjectNode);
    }

    /**
     * 연관검색어, 추천키워드
     */
    public JsonNode related(String q, String groupId) throws BotException {
        logger.info("related start...");

        StringBuilder query = new StringBuilder();
        query.append("q="+q).append("&");
        query.append("groupId="+groupId);
//        ObjectNode queryNode = new ObjectMapper().createObjectNode();
//        queryNode.put("q", q);
//        queryNode.put("groupId", groupId);

        String responceString = okHttpClient(PATH_SIS_RELATED, query.toString());
        if (UText.isEmpty(responceString)) {
            logger.error("related is null");
            throw new BotException("related result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        logger.info("related rootNode: " + rootNode.toString());

        // 연관키워드, 추천키워드
        ObjectNode wordAnalyzeNode = new ObjectMapper().createObjectNode();
        if (rootNode.get("wordAnalyze") != null && rootNode.get("wordAnalyze").isContainerNode() &&
                rootNode.get("wordAnalyze").get("words") != null && rootNode.get("wordAnalyze").get("words").isArray()) {
            wordAnalyzeNode.set("recommendedKeywords", rootNode.get("wordAnalyze").get("words"));
        } else {
            wordAnalyzeNode.set("recommendedKeywords", null);
        }
        if (rootNode.get("association") != null && rootNode.get("association").isContainerNode() &&
                rootNode.get("association").get("words") != null && rootNode.get("association").get("words").isArray()) {
            wordAnalyzeNode.set("relatedKeywords", rootNode.get("association").get("words"));
        } else {
            wordAnalyzeNode.set("relatedKeywords", null);
        }

        logger.info("related wordAnalyzeNode: " + wordAnalyzeNode.toString());
        return wordAnalyzeNode;
    }

    public JsonNode mrcSearch(String[] ids, String q) throws BotException {
        logger.info("mrcSearch start...");

        final StringBuffer query = new StringBuffer();
        query.append("id=").append(UText.commaString(ids)).append("&");
        query.append("q=").append(q);
//        ObjectNode queryNode = new ObjectMapper().createObjectNode();
//        queryNode.put("ids", UText.commaString(ids));
//        queryNode.put("q", q);

        String responceString = okHttpClient(PATH_SIS_MRC, query.toString());
        if (UText.isEmpty(responceString)) {
            logger.error("generalSearch is null");
            throw new BotException("generalSearch result is null", BotStatusCode.Error.getCode());
        }
        logger.info("search responceString:"+responceString);
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        JsonNode dataNode = rootNode == null ? null : (rootNode.get("hits") == null ? null : (rootNode.get("hits").get("hits") == null ? null : rootNode.get("hits").get("hits")));
        return dataNode;
    }
}
