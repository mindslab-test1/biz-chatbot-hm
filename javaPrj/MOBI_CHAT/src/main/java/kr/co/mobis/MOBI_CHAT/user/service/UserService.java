package kr.co.mobis.MOBI_CHAT.user.service;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.user.model.BotUser;
import kr.co.mobis.MOBI_CHAT.user.model.SecurityAgreeEntity;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    static Logger logger = LoggerFactory.getLogger(UserService.class);

    @Value("${front.bot.url}")
    private String BOT_URL;

    @Value("${front.bot.legacy.user.path}")
    private String PATH_LEGACY_USER;

    @Value("${front.integrate.search.security.agreement.path}")
    private String PATH_SEARCH_SECURITY;

    private String okHttpClient(String path, String query) {
        HttpUrl httpUrl = HttpUrl.parse(BOT_URL).newBuilder()
                .encodedPath(path)
                .encodedQuery(query)
                .build();
        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/x-www-form-urlencoded")
                .build();
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .build();
        return botHttpClient.serverCall();
    }

    /**
     * 봇 사용자정보 요청
     */
    public BotUser legacyUser(String userId) throws BotException {
        logger.info("legacyUser start...");

        StringBuilder query = new StringBuilder();
        query.append("userId="+userId);

        String responceString = okHttpClient(PATH_LEGACY_USER, query.toString());
        if (UText.isEmpty(responceString)) {
            logger.error("toJsonParser is null");
            throw new BotException("legacy user api result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        if (rootNode == null || !rootNode.isContainerNode()) {
            logger.error("toJsonParser rootNode is null or not ContainerNode");
            throw new BotException("toJsonParser rootNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }
        // ErrorStatus Check
        if (rootNode.get("code").asInt() != 0) {
            int code = rootNode.get("code").asInt();
            String msg = rootNode.get("msg").asText();
            logger.error("toJsonParser dataNode error: (" + code + ")" + msg );
            throw new BotException(msg, code);
        }
        if (rootNode.get("data") == null || !rootNode.get("data").isContainerNode()) {
            logger.error("toJsonParser dataNode is null or not ContainerNode");
            throw new BotException("toJsonParser dataNode is null or not ContainerNode", BotStatusCode.Error.getCode());
        }

        JsonNode dataNode = rootNode.get("data");
        logger.info("toJsonParser dataNode: " + dataNode.toString());
        BotUser botUser = new BotUser();
        botUser.setUserId(dataNode.get("userId").asText());
        botUser.setUserNm(dataNode.get("userNm").asText());
        botUser.setUserEmail(dataNode.get("userEmail").asText());
        botUser.setUserMobile(dataNode.get("userMobile").asText());
        botUser.setUserTelephone(dataNode.get("userTelephone").asText());
        botUser.setDeptCd(dataNode.get("deptCd").asText());
        botUser.setDeptNm(dataNode.get("deptNm").asText());
        logger.info("toJsonParser botUser: " + botUser.toString());
        return botUser;
    }


    /**
     * 검색 사용자 문서보안동의
     */
    public SecurityAgreeEntity securityAgreement(String userId) throws BotException {
        logger.info("securityAgreement start...");

        StringBuilder query = new StringBuilder();
        query.append("userId="+userId);

        String responceString = okHttpClient(PATH_SEARCH_SECURITY, query.toString());
        if (UText.isEmpty(responceString)) {
            logger.error("securityAgreement is null");
            throw new BotException("securityAgreement result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        SecurityAgreeEntity.dataNodeValidationCheck(rootNode);
        return new SecurityAgreeEntity(rootNode);
    }
}
