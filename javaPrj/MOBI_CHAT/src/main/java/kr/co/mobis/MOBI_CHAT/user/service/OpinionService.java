package kr.co.mobis.MOBI_CHAT.user.service;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.user.model.OpinionEntity;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class OpinionService {
    static Logger logger = LoggerFactory.getLogger(OpinionService.class);

    @Value("${front.bot.url}")
    private String BOT_URL;

    @Value("${front.bot.opinion.save.path}")
    private String PATH_OPINION_SAVE;

    private String okHttpClient(String path, RequestBody formBody) {
        HttpUrl httpUrl = HttpUrl.parse(BOT_URL).newBuilder()
                .encodedPath(path)
                .build();
        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/x-www-form-urlencoded")
                .build();
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .postBody(formBody)
                .build();
        return botHttpClient.serverCall();
    }

    /**
     * 의견제안 저장
     */
    public OpinionEntity saveOpinion(String userId, String botId, String channel, String title, String content) throws BotException {
        logger.error("saveOpinion start...");
        RequestBody formBody = new FormBody.Builder()
                .add("userId", userId)
                .add("botId", botId)
                .add("channel", channel)
                .add("title", title)
                .add("content", content)
                .build();
        String responceString = okHttpClient(PATH_OPINION_SAVE, formBody);
        if (UText.isEmpty(responceString)) {
            logger.error("saveOpinion is null");
            throw new BotException("saveOpinion result is null", BotStatusCode.Error.getCode());
        }
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        OpinionEntity.dataNodeValidationCheck(rootNode);
        return new OpinionEntity(rootNode);
    }
}
