package kr.co.mobis.MOBI_CHAT.user.model;

public class BotLogin {

    private String channelType;
    private String userId;
    private String userNm;
    private String deptNm;


    public BotLogin() {}

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserNm() {
        return userNm;
    }

    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }

    public String getDeptNm() {
        return deptNm;
    }

    public void setDeptNm(String deptNm) {
        this.deptNm = deptNm;
    }

    @Override
    public String toString() {
        return "BotLogin{" +
                "channelType='" + channelType + '\'' +
                ", userId='" + userId + '\'' +
                ", userNm='" + userNm + '\'' +
                ", deptNm='" + deptNm + '\'' +
                '}';
    }
}
