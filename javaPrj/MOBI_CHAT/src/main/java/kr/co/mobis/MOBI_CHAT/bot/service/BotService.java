package kr.co.mobis.MOBI_CHAT.bot.service;

import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.MOBI_CHAT.bot.domain.BotCloseEntity;
import kr.co.mobis.MOBI_CHAT.bot.domain.BotGreetingEntity;
import kr.co.mobis.MOBI_CHAT.bot.domain.BotQuestionEntity;
import kr.co.mobis.MOBI_CHAT.bot.domain.MaiBot;
import kr.co.mobis.MOBI_CHAT.common.domain.BotDataNode;
import kr.co.mobis.MOBI_CHAT.common.domain.BotException;
import kr.co.mobis.MOBI_CHAT.common.domain.BotStatusCode;
import kr.co.mobis.MOBI_CHAT.common.net.BotHttpClient;
import kr.co.mobis.MOBI_CHAT.common.utils.UText;
import kr.co.mobis.MOBI_CHAT.user.model.BotSessionInfo;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class BotService {
    static Logger logger = LoggerFactory.getLogger(BotService.class);

    @Value("${front.bot.url}")
    private String BOT_URL;

    @Value("${front.bot.chat.open.dialog.path}")
    private String PATH_CHAT_OPEN_DIALOG;

    @Value("${front.bot.chat.send.text.path}")
    private String PATH_CHAT_SEND_TEXT;

    @Value("${front.bot.chat.close.dialog.path}")
    private String PATH_CHAT_CLOSE_DIALOG;
    
    private String okHttpClient(String path, String token, String jsonBody) {
        HttpUrl httpUrl = HttpUrl.parse(BOT_URL).newBuilder()
                .encodedPath(path)
                .build();
        Headers headers = new Headers.Builder()
                .add("Content-Type", "application/json; charset=utf-8")
                .add("Authorization", "Bearer " + token)
                .build();
        RequestBody postBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonBody);
        BotHttpClient botHttpClient = new BotHttpClient.Builder()
                .httpUrl(httpUrl)
                .headers(headers)
                .postBody(postBody)
                .build();
        return botHttpClient.serverCall();
    }

    public BotGreetingEntity greeting(BotSessionInfo botSessionInfo, MaiBot maiBot) throws BotException {
        logger.info("greeting start...");

        String responceString = okHttpClient(PATH_CHAT_OPEN_DIALOG, botSessionInfo.getToken(), maiBot.toOpenJSON());
        if (UText.isEmpty(responceString)) {
            logger.error("greeting responce is null");
            throw new BotException("greeting responce is null", BotStatusCode.Error.getCode());
        }

        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        BotGreetingEntity.dataNodeValidationCheck(rootNode);
        logger.info("greeting rootNode: " + rootNode.toString());
        return new BotGreetingEntity(rootNode);
    }

    public BotQuestionEntity question(BotSessionInfo botSessionInfo, MaiBot maiBot) throws BotException {
        logger.info("question start...");
        String responceString = okHttpClient(PATH_CHAT_SEND_TEXT, botSessionInfo.getToken(), maiBot.toSendJSON());
        if (UText.isEmpty(responceString)) {
            logger.error("question responce is null");
            throw new BotException("question responce is null", BotStatusCode.Error.getCode());
        }

        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        BotQuestionEntity.dataNodeValidationCheck(rootNode);
        logger.info("question rootNode: " + rootNode.toString());
        return new BotQuestionEntity(rootNode);
    }

    public BotCloseEntity close(BotSessionInfo botSessionInfo, MaiBot maiBot) throws BotException {
        logger.info("close start...");
        String responceString = okHttpClient(PATH_CHAT_CLOSE_DIALOG, botSessionInfo.getToken(), maiBot.toCloseJSON());
        JsonNode rootNode = BotDataNode.toJsonNode(responceString);
        BotCloseEntity.dataNodeValidationCheck(rootNode);
        logger.info("close rootNode: " + rootNode.toString());
        return new BotCloseEntity(rootNode);
    }
}
