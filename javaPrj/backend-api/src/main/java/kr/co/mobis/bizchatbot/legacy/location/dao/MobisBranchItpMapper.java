package kr.co.mobis.bizchatbot.legacy.location.dao;


import kr.co.mobis.bizchatbot.legacy.location.dao.data.MobisBranchIpData;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface MobisBranchItpMapper {

    public MobisBranchIpData getBranch(Map<String, Object> param) throws Exception;
}
