package kr.co.mobis.bizchatbot.legacy.controller;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.NoticeInfo;
import ai.mindslab.bizchatbot.commons.chatbot.service.ChatbotService;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.session.data.AccessInfo;
import kr.co.mobis.bizchatbot.IMobisConsts.CAFE_LOC;
import kr.co.mobis.bizchatbot.legacy.anwserpref.dao.data.ChatbotRespData;
import kr.co.mobis.bizchatbot.legacy.anwserpref.service.AnswerPrefService;
import kr.co.mobis.bizchatbot.legacy.approval.MobisApprovalManager;
import kr.co.mobis.bizchatbot.legacy.approval.data.MaibotApproval;
import kr.co.mobis.bizchatbot.legacy.bus.dao.data.MobisBusData;
import kr.co.mobis.bizchatbot.legacy.bus.service.MobisBusService;
import kr.co.mobis.bizchatbot.legacy.cafeteria.dao.data.CafeMenuInfo;
import kr.co.mobis.bizchatbot.legacy.cafeteria.service.MobisCafeteriaService;
import kr.co.mobis.bizchatbot.legacy.controller.data.*;
import kr.co.mobis.bizchatbot.legacy.controller.data.MobisAlarmData.ChatbotNotice;
import kr.co.mobis.bizchatbot.legacy.email.MobisEmailManager;
import kr.co.mobis.bizchatbot.legacy.email.data.Item;
import kr.co.mobis.bizchatbot.legacy.email.data.MaibotEmail;
import kr.co.mobis.bizchatbot.legacy.location.dao.data.MobisBranchIpData;
import kr.co.mobis.bizchatbot.legacy.location.service.MobisBranchIpService;
import kr.co.mobis.bizchatbot.legacy.resource.MobisResourceManager;
import kr.co.mobis.bizchatbot.legacy.resource.data.MaibotResource;
import kr.co.mobis.bizchatbot.legacy.resource.data.MobisResourceData;
import kr.co.mobis.bizchatbot.legacy.resource.service.MobisResourceService;
import kr.co.mobis.bizchatbot.legacy.rpa.dao.data.RpaDataVO;
import kr.co.mobis.bizchatbot.legacy.rpa.service.RpaService;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.BqaQuestionData;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.MobisUserData;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.MobisUserSimpleData;
import kr.co.mobis.bizchatbot.legacy.user.service.BqaUserTaskService;
import kr.co.mobis.bizchatbot.legacy.user.service.MobisUserService;
import kr.co.mobis.bizchatbot.legacy.utils.AES256Cipher;
import kr.co.mobis.bizchatbot.legacy.weather.data.MobisWeatherData;
import kr.co.mobis.bizchatbot.legacy.weather.service.MobisWeatherService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(path = "/mobis/legacy")
public class MobisLegacyController implements IChatbotConst {

	private Logger logger = LoggerFactory.getLogger(MobisLegacyController.class);

	@Autowired
	private MobisUserService user;

	@Autowired
	private MobisCafeteriaService cafe;

	@Autowired
	private BqaUserTaskService userTask;

	@Autowired
    private MobisBusService bus;

	@Autowired
	private MobisBranchIpService branch;

	@Autowired
	AnswerPrefService answerPrefService;

	@Autowired
	private MobisApprovalManager approval;

	@Autowired
	private MobisEmailManager email;

	@Autowired
	private MobisResourceManager resourceManager;

	@Autowired
	private MobisResourceService resourceService;

	@Autowired
	private ChatbotService chatbot;

	@Autowired
	RpaService rpaService;

	@Value("${chatbot.image.url}")
	private String imgUrl;

	@Autowired
	private HazelClientCommon client;

	@Autowired
	private MobisWeatherService weatherService;


	@RequestMapping(value="/user" ,method = {RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getWeather(@RequestParam("userId") String userId) throws Exception{
		logger.info("/user|{}",userId);
		BaseResponse<MobisUserData> out = new BaseResponse<>();
		try {
			MobisUserData data = user.getUserInfo(userId);
			out.setData(data);
		}catch(Exception e) {
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}
		return out;
	}

	@RequestMapping(value="/cafeteria_menu" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getCafeteriaMenu(@RequestParam(name="userId", required=false)String userId,
											 HttpServletRequest request) throws Exception{

		logger.info("/cafeteria_menu|: {}",userId);

		AccessInfo userInfo = null;
		String remoteAddr = null;
		userInfo = (AccessInfo) client.getMap(MAP_NAME_USER).get(userId);

		if(userInfo == null){
			remoteAddr = request.getRemoteAddr();
		}else{
			remoteAddr = userInfo.getRemoteAddress();
		}

		logger.info("/cafeteria_menu|: {}, {}",userId, remoteAddr);
		BaseResponse<CafeMenuInfo> out = new BaseResponse<>();

		try {
			CAFE_LOC loc = cafe.getCafeLocation(remoteAddr);
			CafeMenuInfo menu = cafe.getTodayMenu(loc);
			if(menu == null) {
				out.setData(null);
				out.setMsg("오늘은 메뉴가 없습니다.");
			}else {
				out.setData(menu);
			}
		}catch(Exception e) {
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/userByName" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getUserByName(@RequestParam(name="userName", required=true) String userName
			) throws Exception{
		logger.info("/getUserByName|{}",userName);
		BaseResponse<MobisUserList> out = new BaseResponse<>();

		try {
			MobisUserList userList = new MobisUserList();
			List<MobisUserSimpleData> users = user.getUserInfoByUserNm(userName);
			if(users == null || users.size() == 0) {
				out.setData(null);
				out.setMsg("해당 사용자가 없습니다.");
			}else {
				userList.setUserList(users);
				userList.setSysUtterTypeCode(SYS_UTTER_TYPE.EMPLOYEE_INFO.getCode());

				out.setData(userList);
			}
		}catch(Exception e) {
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/userByNameDeptName" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> userByNameDeptName(@RequestParam(name="userName", required=true) String userName
			,@RequestParam(name="deptName", required=true) String deptName
			) throws Exception{
		logger.info("/userByNameDeptName|{}",userName);
		BaseResponse<MobisUserList> out = new BaseResponse<>();

		try {
			MobisUserList userList = new MobisUserList();
			List<MobisUserSimpleData> users = user.getUserInfoByUserNmDeptNm(userName, deptName);
			if(users == null || users.size() == 0) {
				out.setData(null);
				out.setMsg("해당 사용자가 없습니다.");
			}else {
				userList.setUserList(users);
				userList.setSysUtterTypeCode(SYS_UTTER_TYPE.EMPLOYEE_INFO.getCode());

				out.setData(userList);
			}
		}catch(Exception e) {
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/userByTask" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getUserByTask(
			@RequestParam(name="userTask", required=true) String userTask
			, @RequestParam(name="count", required=true, defaultValue="7") Integer count
	) throws Exception{
		logger.info("/getUserByTask|{},{}", userTask, count);
		BaseResponse<MobisUserList> out = new BaseResponse<>();

		try {
			MobisUserList userList = new MobisUserList();
			List<MobisUserSimpleData> users = user.getUserInfoByUserTask(userTask, count);
			if(users == null || users.size() == 0) {
				out.setData(null);
				out.setMsg("해당 사용자가 없습니다.");
			}else {
				userList.setUserList(users);
				userList.setSysUtterTypeCode(SYS_UTTER_TYPE.EMPLOYEE_INFO.getCode());

				out.setData(userList);
			}
		}catch(Exception e) {
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/taskByUserId" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> taskByUserId(@RequestParam(name="chatbotId", required=true) String chatbotId
			,@RequestParam(name="userId", required=true) String userId
			) throws Exception{
		logger.info("/taskByUserId/{},{}",chatbotId, userId);
		BaseResponse<MobisUserWithTasks> out = new BaseResponse<>();

		try {
			MobisUserWithTasks userInfo = new MobisUserWithTasks();
			MobisUserData uData = user.getUserInfo(userId);
			List<BqaQuestionData> tasks = userTask.getQuestionsByUserId(chatbotId, userId);

			if(uData == null) {
				out.setData(null);
				out.setMsg("해당 사용자가 없습니다.");
			}else {
				String taskSentence = "";
				if(tasks != null && tasks.size() > 0) {
					taskSentence = "담당자가 등록한 업무 안내가 있습니다.";
				}

				userInfo.setUserInfo(uData);
				userInfo.setTaskSentence(taskSentence);
				userInfo.setTasks(tasks);
				userInfo.setSysUtterTypeCode(SYS_UTTER_TYPE.EMPLOYEE_INFO.getCode());

				out.setData(userInfo);
			}

		}catch(Exception e) {
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

    @RequestMapping(value="/busByName" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<?> getBusByName(
            @RequestParam(name="routeName", required=true) String routeName
            , @RequestParam(name="type", required=false) String type
    ) throws Exception{
        logger.info("/getBusByName|{},{}", routeName, type);
        BaseResponse<List<MobisBusData>> out = new BaseResponse<>();

        try {

            List<MobisBusData> buses = bus.getBusInfoByName(routeName, type);
            if(buses == null || buses.size() == 0) {
                out.setData(null);
                out.setMsg("해당 버스 노선이 없습니다.");
            }else {
                out.setData(buses);
            }
        }catch(Exception e) {
            out.setCode(IRestCodes.ERR_CODE_FAILURE);
            out.setMsg(e.getMessage());
        }

        return out;
    }

    @RequestMapping(value="/busByBusstop" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseResponse<?> getBusByBusStop(
            @RequestParam(name="stopName", required=false) String stopName
            , @RequestParam(name="type", required=false) String type
    ) throws Exception{
        logger.info("/getBusByBusStop|{},{}", stopName, type);
        BaseResponse<List<MobisBusData>> out = new BaseResponse<>();

        try {

            List<MobisBusData> buses = bus.getBusInfoByBusStop(stopName, type);
            if(buses == null || buses.size() == 0) {
                out.setData(null);
                out.setMsg("버스노선을 찾지 못하였어요.<br>'버스 노선 찾기'를 입력하시면 버스 노선 확인 꿀팁을 알려 드립니다.");
            }else {
                out.setData(buses);
            }
        }catch(Exception e) {
//            out.setCode(IRestCodes.ERR_CODE_FAILURE);
//            out.setMsg(e.getMessage());
			out.setData(null);
			out.setMsg("버스노선을 찾지 못하였어요.<br>'버스 노선 찾기'를 입력하시면 버스 노선 확인 꿀팁을 알려 드립니다.");
		}

        return out;
    }


	@RequestMapping(value="/location" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getBranchByIP(@RequestParam(name="userId", required=false)String userId,
										 HttpServletRequest request) throws Exception{

		AccessInfo userInfo = null;
		String userIp = null;
		userInfo = (AccessInfo) client.getMap(MAP_NAME_USER).get(userId);

		if(userInfo == null){
			userIp = request.getRemoteAddr();
		}else{
			userIp = userInfo.getRemoteAddress();
		}

		logger.info("/location|{}",userIp);

		BaseResponse<MobisBranchIpData> out = new BaseResponse<>();

		try {
			MobisBranchIpData data = branch.getBranch(userIp);
			if(data == null) {
				out.setData(null);
				out.setMsg("해당 IP에 해당하는 사업장이 없습니다.");
			}else {
				out.setData(data);
			}
		}catch(Exception e) {
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/legacyAlarm" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getLegacyAlarm(@RequestParam(name="userId", required=true) String userId
			,@RequestParam(name="botId", required=true) int botId
            ,HttpServletRequest request) throws Exception{

		logger.info("/legacyAlarm/{},{}",userId, botId);

		BaseResponse<MobisAlarmData> out = new BaseResponse<>();
		MobisAlarmData data = new MobisAlarmData();
		try {

			MaibotApproval appCount = approval.getApprovalCount(userId);
//			MaibotApproval approvalList = approval.getApprovalList(userId, count, userIP);

			MaibotEmail unreadCount = email.getMailUnreadCount(userId);
//			MaibotEmail mails = email.getUnreadMailList(userId, count);
			data.setApprovalCount(appCount.getMobot().size()>0 ? appCount.getMobot().get(0).getCount():0);
			data.setApprovalUrl(appCount.getMobot().size()>0 ? appCount.getMobot().get(0).getUrl():"");
			data.setUnreadEmailCodunt(unreadCount.getMobot().size()>0 ? unreadCount.getMobot().get(0).getCount():0 );
			data.setMailUrl(unreadCount.getMobot().size()>0 ? unreadCount.getMobot().get(0).getUrl():"");

			ChatbotNotice cn = new ChatbotNotice();
			MobisNormalNotice botNotice = new MobisNormalNotice();
			MobisSearchNotice srchNotice = new MobisSearchNotice();
			cn.setBotNotice(botNotice);
			cn.setSearchNotice(srchNotice);
			data.setNotice(cn);

			ChatbotInfo info = chatbot.getChatbotinfo(botId);
			logger.debug("botInfo:{}",info);
			List<NoticeInfo> notices = chatbot.getChatbotNotice(botId);
			if(notices != null) {
				for(NoticeInfo notice:notices) {
					notice.setImagePath(imgUrl+notice.getImagePath());
					if(notice.getNoticePos().equals("4")) {//4번은 search 공지사항
						srchNotice.setNotice(notice);
					}else {
						botNotice.getNotices().add(notice);
					}
				}
			}
			info.setNotices(notices);
			out.setData(data);

		}catch(Exception e) {
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/approvalList" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getApprovalList(@RequestParam(name="userId", required=true) String userId
			,@RequestParam(name="count", required=false, defaultValue="3") int count
            ,HttpServletRequest request) throws Exception{

		AccessInfo userInfo = null;
		String userIP = null;
		userInfo = (AccessInfo) client.getMap(MAP_NAME_USER).get(userId);

		if(userInfo == null){
			userIP = request.getRemoteAddr();
		}else{
			userIP = userInfo.getRemoteAddress();
		}


		logger.info("/approvalList/{},{},{}",userId, count, userIP);

		BaseResponse<MobisApprovalList> out = new BaseResponse<>();
		MobisApprovalList data = new MobisApprovalList();
		try {

			MaibotApproval appCount = approval.getApprovalCount(userId);
			MaibotApproval approvalList = approval.getApprovalList(userId, count, userIP);

			data.setCount(appCount.getMobot().size()>0? appCount.getMobot().get(0).getCount():0);
			data.setUrl(appCount.getMobot().size()>0? appCount.getMobot().get(0).getUrl():"");

			if(approvalList != null && approvalList.getMobot().size() > 0) {
				List<kr.co.mobis.bizchatbot.legacy.approval.data.Item> approvals = new ArrayList<>();

				if(approvalList.getMobot().size() > 5) {

					for(int i =0 ; i< 5; i++){
						approvals.add(approvalList.getMobot().get(i));
					}
				}else{
					approvals.addAll(approvalList.getMobot());
				}
				data.setApprovals(approvals);
			}
			data.setSysUtterTypeCode(SYS_UTTER_TYPE.APPROVAL_CHECK.getCode());
			out.setData(data);
		}catch(Exception e) {
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/unreadEmailList" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getLegacyEmails(@RequestParam(name="userId", required=true) String userId
			,@RequestParam(name="count", required=false, defaultValue="3") int count
            ,HttpServletRequest request) throws Exception{

		logger.info("/unreadEmailList/{},{}",userId, count);

		BaseResponse<MobisEmailUnread> out = new BaseResponse<>();
		MobisEmailUnread data = new MobisEmailUnread();
		try {

			MaibotEmail unreadCount = email.getMailUnreadCount(userId);
			MaibotEmail mails = email.getUnreadMailList(userId, count);
			data.setCount(unreadCount.getMobot().size()>0? unreadCount.getMobot().get(0).getCount():0);
			data.setUrl(unreadCount.getMobot().size()>0? unreadCount.getMobot().get(0).getUrl():"");
			if(mails != null && mails.getMobot().size() > 0) {
				List<Item> emails = new ArrayList<>();
				if(mails.getMobot().size() > 5) {
					for(int i =0 ; i< 5; i++){
						emails.add(mails.getMobot().get(i));
					}
				}else{
					emails.addAll(mails.getMobot());
				}
				data.setEmails(emails);
			}
			data.setSysUtterTypeCode(SYS_UTTER_TYPE.EMAIL_CHECK.getCode());
			out.setData(data);

		}catch(Exception e) {
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/encQmsParams" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> encQmsParams(@RequestParam(name="userId", required=true) String userId
			,@RequestParam(name="menuId", required=true) String menuId
            ,HttpServletRequest request) throws Exception{
		logger.info("/encQmsParams/{},{}",userId, menuId);

		BaseResponse<MobisQmsResp> out = new BaseResponse<>();
		try {
			String encUserId = AES256Cipher.AES_Encode(userId);
			//String encMenuId = AES256Cipher.AES_Encode(menuId);

			MobisQmsResp resp = new MobisQmsResp();
			resp.setP_menuId(menuId);
			resp.setUserid(encUserId);
			out.setData(resp);
		}catch(Exception e) {
			logger.warn("/encQmsParams/{},{}/{}",userId, menuId, e.getMessage());
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/resourceDateButtons" ,method = {RequestMethod.GET,RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getResourceDateButtons(HttpServletRequest request) throws Exception{
		logger.info("/getResourceDateButtons/");

		BaseResponse<List<String>> out = new BaseResponse<>();
		try {

			List<String> list = resourceService.getResourceDateButtons();
			if(list == null || list.size() == 0) {
				out.setData(null);
				out.setMsg("에러 발생! 날짜가 없습니다.");
			}else {
				out.setData(list);
			}
		}catch(Exception e) {
			logger.warn("/getResourceDateButtons/{}", e.getMessage());
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/resourceList" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getResourceList(
			@RequestParam(name="userId", required=true) String userId
			,@RequestParam(name="date", required=true) String date
			,@RequestParam(name="stime", required=true) String stime
			,@RequestParam(name="etime", required=true) String etime
			,@RequestParam(name="pgNo", required=false, defaultValue="1") String pgNoStr
			,HttpServletRequest request) throws Exception{

		logger.info("/getResourceList/{},{},{},{},{}" ,userId ,date ,stime ,etime, pgNoStr);

		BaseResponse<MobisResourceData> out = new BaseResponse<>();
		MobisResourceData data = new MobisResourceData();
		try {

			Map<String, String> paramMap = resourceService.getTimeParams(date, stime, etime);
			String status = paramMap.get("status");

			if(StringUtils.equals(status, "SUCCESS")) {
				List<String> resourceList = resourceManager.getResourceList(userId, paramMap.get("pstime"), paramMap.get("petime"), pgNoStr);

				if(resourceList == null) {
					status = "회의실 시스템에 오류가 발생한듯 합니다.<br>'회의실 예약'을 입력하여 다시 한번 예약을 진행해 주세요.<br>그래도 안되는 경우 '대화 종료'를 입력하여 회의실 대화를 종료해 주세요.";
					data.setList(null);
				} else if (resourceList.size() == 0) {
					status = date+" "+stime+" 부터 "+ etime+"에 예약 가능한 회의실이 없습니다. <br>조회를 처음부터 다시 하시려면 \"처음으로\" 를 입력해주세요.<br>";
					data.setList(null);
				} else {
					status = date+" "+stime+" 부터 "+ etime+"해당 조건에 가능한 회의실 목록은 다음과 같습니다. <br>예약하시려면 버튼을 클릭해주세요. <br>조회를 처음부터 다시 하시려면 \"처음으로\" 를 입력해주세요.";
					data.setList(resourceList);
				}
			}
			data.setStatus(status);

			logger.info("/getResourceList data/{}", data);
			out.setData(data);
		}catch(Exception e) {
			logger.warn("/getResourceList/{}", e.getMessage());
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/resourceSave" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> resourceSave(
			@RequestParam(name="userId", required=true) String userId
			,@RequestParam(name="date", required=true) String date
			,@RequestParam(name="stime", required=true) String stime
			,@RequestParam(name="etime", required=true) String etime
			,@RequestParam(name="importance", required=false, defaultValue="2") String importance
			,@RequestParam(name="roomId", required=true) String roomId
			,@RequestParam(name="subject", required=false, defaultValue="default") String subject
			,HttpServletRequest request) throws Exception{

		logger.info("/resourceSave/{},{},{},{},{},{},{}",userId, date, stime, etime, importance, roomId, subject);

		BaseResponse<MobisResourceData> out = new BaseResponse<>();
		MobisResourceData data = new MobisResourceData();
		try {

			Map<String, String> paramMap = resourceService.getTimeParams(date, stime, etime);
			String status = paramMap.get("status");

			if(StringUtils.equals(status, "SUCCESS")) {
				// 회의명 : (팀명+"업무회의")
				MobisUserData userInfo = user.getUserInfo(userId);
				subject = userInfo.getDeptNm() + " 업무회의";

				status = resourceManager.resourceSave(userId, paramMap.get("pstime"), paramMap.get("petime"), importance, roomId, subject);

				if(StringUtils.isEmpty(status)) {
					status = "상태값이 반환되지 않았습니다.";
					data.setList(null);
				} else {
					List<String> list = new ArrayList<>();
					list.add("회의실 예약 정보 알려줘");
					data.setList(list);
				}
			}
			data.setStatus(status);


			logger.info("/resourceSave data/{}", data);
			out.setData(data);

		}catch(Exception e) {
			logger.warn("/resourceSave/{}", e.getMessage());
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/myResourceList" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getMyResourceList(
			@RequestParam(name="userId", required=true) String userId
			,HttpServletRequest request) throws Exception{

		logger.info("/getMyResourceList/{}", userId);

		BaseResponse<MobisResourceList> out = new BaseResponse<>();
		MobisResourceList data = new MobisResourceList();
		try {

			MaibotResource resourceList = resourceManager.getMyResourceList(userId);

			if(resourceList != null && resourceList.getMobot().size() > 0) {
                data.setCount(resourceList.getMobot().size());
                data.setResources(resourceList.getMobot());
			}
			out.setData(data);
		}catch(Exception e) {
			logger.warn("/getResourceList/{}", e.getMessage());
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}

	@RequestMapping(value="/conversation/pref" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> conversationPref(
			@RequestParam(name="userId", required=true) String userId,
			@RequestParam(name="botId", required=true) Long botId,
			@RequestParam(name="currStep", required=false) String currStep,
			@RequestParam(name="question", required=true) String question,
			@RequestParam(name="answer", required=true) String answer,
			@RequestParam(name="countType", required=true) String countType,  HttpServletRequest request) throws Exception{

		logger.info("/conversation/pref{}", userId);

		long likeCnt  = 0;
		long disLikeCnt = 0;
		int result =  0 ;

		BaseResponse<Map<String, Object>> out = new BaseResponse<>();

		try {

			ChatbotRespData vo = new ChatbotRespData();
			String process = "insert";

			vo.setUserId(userId);
			vo.setDialogType(currStep);
			vo.setBotId(botId);
			vo.setQuestion(question);
			vo.setAnswer(answer);
			vo.setCountType(countType);

			ChatbotRespData data = answerPrefService.getAnswerPref(vo);

			if(data != null) {
				likeCnt = data.getLikeCnt();
				disLikeCnt =  data.getDisLikeCnt();
                process = "update";
			}

			if(countType.equals("dislike")) {
				vo.setDisLikeCnt(likeCnt);
				vo.setLikeCnt(++disLikeCnt);
			}else {
				vo.setDisLikeCnt(++likeCnt);
				vo.setLikeCnt(disLikeCnt);
			}

			if(process.equals("insert")) {
			    result = answerPrefService.insertAnswerPrefHistory(vo);
			}else {
				result = answerPrefService.updateAnswerPrefHistory(vo);
			}

			if(result > 0){
				out.setCode(0);
				out.setMsg("Success");
			} else {
				out.setCode(1);
				out.setCode(IRestCodes.ERR_CODE_FAILURE);
			}

		}catch(Exception e) {
			logger.warn("/conversation/pref{}", e.getMessage());
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}
		return out;
	}


	@RequestMapping(value="/rpa/info" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> conversationPref(@RequestBody Map<String, Object> param, HttpServletRequest request) throws Exception{

		logger.info("/rpa/info, {}", param);

		int result =  0 ;

		BaseResponse<Map<String, Object>> out = new BaseResponse<>();

		try {

			RpaDataVO vo = new RpaDataVO();
			String reqId = param.get("reqId").toString();
			String targetId = param.get("targetId").toString();

			String reqName = rpaService.getRequestUserName(reqId).getUserNm();
			String targetName = rpaService.getRequestUserName(targetId).getUserNm();

			vo.setReqId(reqId);
			vo.setReqName(reqName);
			vo.setTargetId(targetId);
			vo.setTargetName(targetName);

			result = rpaService.insertRpaInfo(vo);

			if(result > 0){
				out.setCode(0);
				out.setMsg("Success");
			} else {
				out.setCode(1);
				out.setCode(IRestCodes.ERR_CODE_FAILURE);
			}

		}catch(Exception e) {
			logger.warn("/conversation/pref{}", e.getMessage());
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}
		return out;
	}
	@RequestMapping(value="/user/secure" ,method = {RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> setUserSecureConfirm(@RequestParam(name="userId", required=true) String userId) throws Exception {

		logger.info("/user/secure, {}, {}", userId);

		int result =  0 ;

		BaseResponse<Map<String, Object>> out = new BaseResponse<>();

		try {

			result = user.setUserSecureConfirm(userId);

			if(result > 0){
				out.setCode(0);
				out.setMsg("Success");

				AccessInfo userInfo = null;
                userInfo = (AccessInfo) client.getMap(MAP_NAME_USER).get(userId);

				if(userInfo != null) {
                    userInfo.setConfirmYn("Y");
					client.getMap(MAP_NAME_USER).put(userId, userInfo);
				}

			} else {
				out.setCode(1);
				out.setCode(IRestCodes.ERR_CODE_FAILURE);
			}


		}catch (Exception e){
			logger.warn("/user/secure {}", e.getMessage());
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}
		return out;
	}

	@RequestMapping(value="/weather" ,method = {RequestMethod.POST,RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getWeather(@RequestParam("city") String city, @RequestParam("country")String country) throws Exception{

		BaseResponse<MobisWeatherData> out = new BaseResponse<>();

		logger.info("[weather] city : " + city +" country :" + country);
		MobisWeatherData data = weatherService.getWeather(city, country, "kr");
		data.setSysUtterTypeCode(SYS_UTTER_TYPE.WEATHER.getCode());
		out.setData(data);

		return out;
	}

	@RequestMapping(value="/teamlist" ,method = {RequestMethod.POST,RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<MobisTeamData>> getMobisTeamList() throws Exception{
		List<MobisTeamData> data = user.getMobisTeamList();
		return new ResponseEntity<>(data, HttpStatus.OK);
	}

	/*
	// globalquery test 용
	@RequestMapping(value="/test" ,method = {RequestMethod.POST}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> test(HttpServletRequest request) throws Exception{

		BaseResponse<String> out = new BaseResponse<>();
		String data = "HI";
		try {
			String debug = "태깅: 가족/가족/NN.용언불가능/친족_조직+들/들/FS/ 입국/입국/NN.용언가능/이동_출입+하/하/VF/+어서/어서/EE.연결/ 병원/병원/NN.용언불가능/조직_건물 이용/이용/NN.용언가능/이용활동\n" +
					"POI:가족/가족/NN.용언불가능/친족 들/들/FS/ 입국/입국/NN.용언가능/이동 하/하/VF/ 어서/어서/EE.연결/ 병원/병원/NN.용언불가능/조직 이용/이용/NN.용언가능/이용활동\n" +
					"SLU1:  <User.type=가족>들 입국해서 병원 이용<TAB> #userByName() (0.120878)\n" +
					"SLU2:  <User.type=가족>들 입국해서 병원 이용<TAB> #userByName(User.type=\"가족\") (0.107308)\n" +
					"SLU3:  <Bus.stopName=가족>들 입국해서 병원 이용<TAB> #userByName(Bus.stopName=\"가족\") (0.107217)";

			Pattern patternIntentInfo = Pattern.compile("(SLU1.*\\#([^\\(^\\)]+)\\(.*\\)\\s+\\((.*)\\))");
			Pattern patternSlots = Pattern.compile("(<([^<>=]*)=([^<>=]*)>)+");

			Matcher m = patternIntentInfo.matcher(debug);

			if(m.find()) {
				String slu1 = m.group(1);
				String intentName = m.group(2);
				String score = m.group(3);
				logger.debug("extractIntentName/found intent:{}, {}", intentName, score);

				boolean isDiscarded = false;
				if(score.matches("[-+]?[0-9]*\\.?[0-9]+")) {
					double dScore = Double.parseDouble(score);
					if(dScore <= 0.4) {
						isDiscarded = true;
						logger.debug("extractIntentName/discard current intent :{}", score);
					}
				}
				if(!isDiscarded) {

					//multiple slots
					Matcher slotMatch = patternSlots.matcher(slu1);
					while(slotMatch.find()) {
						String slotName = slotMatch.group(2);
						String slotValue = slotMatch.group(3);
						logger.debug("extractIntentName-array/found intent:{}, {}", slotName, slotValue);
					}
				}
			}
		}catch(Exception e) {
			logger.warn("/getResourceList/{}", e.getMessage());
			out.setCode(IRestCodes.ERR_CODE_FAILURE);
			out.setMsg(e.getMessage());
		}

		return out;
	}*/
}
