package kr.co.mobis.bizchatbot.legacy.rpa.dao.data;

import java.io.Serializable;

public class RpaDataVO  implements Serializable {

    private String reqId;
    private String targetId;
    private String reqName;
    private String targetName;

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getReqName() {
        return reqName;
    }

    public void setReqName(String reqName) {
        this.reqName = reqName;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }
}
