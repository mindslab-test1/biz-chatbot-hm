package kr.co.mobis.bizchatbot.legacy.controller.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import kr.co.mobis.bizchatbot.legacy.approval.data.Item;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MobisApprovalList implements Serializable{

	protected int count;
	protected String url;
	protected List<Item> approvals;
	protected String sysUtterTypeCode;

	public MobisApprovalList() {
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<Item> getApprovals() {
		if(approvals == null) {
			approvals = new ArrayList<>();
		}
		return approvals;
	}

	public void setApprovals(List<Item> approvals) {
		this.approvals = approvals;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSysUtterTypeCode() {
		return sysUtterTypeCode;
	}

	public void setSysUtterTypeCode(String sysUtterTypeCode) {
		this.sysUtterTypeCode = sysUtterTypeCode;
	}

	@Override
	public String toString() {
		return "MobisApprovalList{" +
				"count=" + count +
				", url='" + url + '\'' +
				", approvals=" + approvals +
				", sysUtterTypeCode='" + sysUtterTypeCode + '\'' +
				'}';
	}
}
