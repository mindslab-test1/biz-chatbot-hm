package kr.co.mobis.bizchatbot.legacy.controller.data;

import java.io.Serializable;

public class MobisQmsResp implements Serializable{

	protected String userid;
	protected String p_menuId;
	protected String url = "https://nqms.mobis.co.kr/qms20/ssoLoginVendor.do";
	
	public MobisQmsResp() {
	}

	public String getUserid() {
		return userid;
	}


	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getP_menuId() {
		return p_menuId;
	}

	public void setP_menuId(String p_menuId) {
		this.p_menuId = p_menuId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "MobisQmsResp [userId=" + userid + ", menuId=" + p_menuId + ", url=" + url + "]";
	}
}
