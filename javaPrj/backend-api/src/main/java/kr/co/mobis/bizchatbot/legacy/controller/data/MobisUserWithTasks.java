package kr.co.mobis.bizchatbot.legacy.controller.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kr.co.mobis.bizchatbot.legacy.user.dao.data.BqaQuestionData;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.MobisUserData;

public class MobisUserWithTasks implements Serializable{

	protected MobisUserData userInfo;
	protected List<BqaQuestionData> tasks;
	protected String taskSentence;
	protected String sysUtterTypeCode;

	public MobisUserWithTasks() {
	}

	public MobisUserData getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(MobisUserData userInfo) {
		this.userInfo = userInfo;
	}

	public List<BqaQuestionData> getTasks() {
		if(tasks == null) {
			tasks = new ArrayList<BqaQuestionData>();
		}
		return tasks;
	}

	public void setTasks(List<BqaQuestionData> tasks) {
		this.tasks = tasks;
	}

	public String getTaskSentence() {
		return taskSentence;
	}

	public void setTaskSentence(String taskSentence) {
		this.taskSentence = taskSentence;
	}

	public String getSysUtterTypeCode() {
		return sysUtterTypeCode;
	}

	public void setSysUtterTypeCode(String sysUtterTypeCode) {
		this.sysUtterTypeCode = sysUtterTypeCode;
	}

	@Override
	public String toString() {
		return "MobisUserWithTasks{" +
				"userInfo=" + userInfo +
				", tasks=" + tasks +
				", taskSentence='" + taskSentence + '\'' +
				", sysUtterTypeCode='" + sysUtterTypeCode + '\'' +
				'}';
	}
}
