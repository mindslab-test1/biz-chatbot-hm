package kr.co.mobis.bizchatbot.legacy.controller.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import kr.co.mobis.bizchatbot.legacy.email.data.Item;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MobisEmailUnread implements Serializable{

	protected int count;
	protected String url;
	protected List<Item> emails;
	protected String sysUtterTypeCode;

	public MobisEmailUnread() {
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<Item> getEmails() {
		if(emails == null) {
			emails = new ArrayList<>();
		}
		return emails;
	}

	public void setEmails(List<Item> emails) {
		this.emails = emails;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSysUtterTypeCode() {
		return sysUtterTypeCode;
	}

	public void setSysUtterTypeCode(String sysUtterTypeCode) {
		this.sysUtterTypeCode = sysUtterTypeCode;
	}

	@Override
	public String toString() {
		return "MobisEmailUnread{" +
				"count=" + count +
				", url='" + url + '\'' +
				", emails=" + emails +
				", sysUtterTypeCode='" + sysUtterTypeCode + '\'' +
				'}';
	}
}
