package kr.co.mobis.bizchatbot.legacy.user.dao.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MobisUserSimpleData implements Serializable{

	protected String userId;
	protected String userNm;
	protected String userEmail;
	protected String userMobile;
	protected String userTelephone;
	protected String deptCd;
	protected String deptNm;
	
	public MobisUserSimpleData() {
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserNm() {
		return userNm;
	}

	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getUserTelephone() {
		return userTelephone;
	}

	public void setUserTelephone(String userTelephone) {
		this.userTelephone = userTelephone;
	}

	public String getDeptCd() {
		return deptCd;
	}

	public void setDeptCd(String deptCd) {
		this.deptCd = deptCd;
	}

	public String getDeptNm() {
		return deptNm;
	}

	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}
	
	public String getDisplayInfo() {
		return this.deptNm + " " + this.userNm;
	}

	@Override
	public String toString() {
		return "MobisUserSimpleData [userId=" + userId + ", userNm=" + userNm + ", userEmail=" + userEmail
				+ ", userMobile=" + userMobile + ", userTelephone=" + userTelephone + ", deptCd=" + deptCd + ", deptNm="
				+ deptNm + "]";
	}
}
