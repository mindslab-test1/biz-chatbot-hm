package kr.co.mobis.bizchatbot.legacy.approval;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;
import kr.co.mobis.bizchatbot.legacy.approval.data.MaibotApproval;

@Service
public class MobisApprovalManager {

	private Logger logger = LoggerFactory.getLogger(MobisApprovalManager.class);
	
	@Value("${legacy.approval.url:}")
	public String approvalUrl;
	
//	@Value("${legacy.email.url:}")
//	public String emaillUrl;
	
	public MobisApprovalManager() {
	}

	public MaibotApproval parseApproval(String xml) throws Exception{
		JAXBContext jc = JAXBContext.newInstance(MaibotApproval.class);
		Unmarshaller unmashaller = jc.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		MaibotApproval out = (MaibotApproval)unmashaller.unmarshal(reader);
		return out;
	}
	
	public MaibotApproval parseApprovalList(String xml) throws Exception{
		JAXBContext jc = JAXBContext.newInstance(MaibotApproval.class);
		Unmarshaller unmashaller = jc.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		MaibotApproval out = (MaibotApproval)unmashaller.unmarshal(reader);
		return out;
	}
	
	public MaibotApproval getApprovalCount(String userId) throws Exception{
		logger.info("getApprovalCount/{}",userId);
		String url = approvalUrl + "/ApprovalCount";
		url = url + "?userID="+userId;
		
		String resp = HttpConnectionManager.getInstance().get(url, null);
		
		//test
//		String resp = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + 
//				"<MAIBOT>\n" + 
//				"    <Item>\n" + 
//				"        <count>52</count>\n" + 
//				"        <url>http://ucdev.imobis.co.kr/Maibot_ApprovalList.aspx?userid=1609268</url>\n" + 
//				"    </Item>\n" + 
//				"</MAIBOT>";
		MaibotApproval out = parseApproval(resp);
//		if(out == null) {
//			return 0;
//		}else {
//			return out.getMobot().size();
//		}
		return out;
	}
	
	public MaibotApproval getApprovalList(String userId, int count, String userIP) throws Exception{
		logger.info("getApprovalList/{},{},{}",userId,count,userIP);
		String url = approvalUrl + "/ApprovalList";
		url = url + "?userID="+userId+"&count="+count+"&userIP="+userIP;
		
		String resp = HttpConnectionManager.getInstance().get(url, null);
		
		//test
//		String resp = ""
//				+ "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + 
//				"<MAIBOT>\n" + 
//				"<Item>\n" + 
//				"<docid />\n" + 
//				"<subject>111111 1559</subject>\n" + 
//				"<url>http://ucdev.imobis.co.kr/myoffice/ezEmail/DLmail_read.aspx?messageid=&lt;095148F0-D771-4599-8769-E2431455C1FB@mobisdev.com&gt;                                                 &amp;saveurl=DLManager@mobisdev.com                            &amp;sendermail=1609268@mobisdev.com                                                                                &amp;partopen=Y</url>\n" + 
//				"<draftnm>ÀÇÈ</draftnm>\n" + 
//				"<draftdt>2019-03-15 ¿ÀÄ3:41:48</draftdt>\n" + 
//				"</Item>\n" + 
//				"<Item>\n" + 
//				"<docid>00000000000000008301</docid>\n" + 
//				"<subject>ÇvÀ °á Å½ºƮ2</subject>\n" + 
//				"<url>http://ucdev.imobis.co.kr/myoffice/ezApproval/ApprovUI/Approvui.aspx?DocID=00000000000000008301&amp;uID=1609268&amp;uName=ÀÇÈ&amp;uDeptID=10001250</url>\n" + 
//				"<draftnm>±è¿µ</draftnm>\n" + 
//				"<draftdt>2019-03-14 ¿Àü9:14</draftdt>\n" + 
//				"</Item>\n" + 
//				"<Item>\n" + 
//				"<docid>00000000000000008300</docid>\n" + 
//				"<subject>°áÅ½ºƮ1</subject>\n" + 
//				"<url>http://ucdev.imobis.co.kr/myoffice/ezApproval/ApprovUI/Approvui.aspx?DocID=00000000000000008300&amp;uID=1609268&amp;uName=ÀÇÈ&amp;uDeptID=10001250</url>\n" + 
//				"<draftnm>±è¿µ</draftnm>\n" + 
//				"<draftdt>2019-03-14 ¿Àü8:25</draftdt>\n" + 
//				"</Item>\n" + 
//				"<Item>\n" + 
//				"<docid>00000000000000008298</docid>\n" + 
//				"<subject>Å½ºƮ1</subject>\n" + 
//				"<url>http://ucdev.imobis.co.kr/myoffice/ezApproval/ApprovUI/Approvui.aspx?DocID=00000000000000008298&amp;uID=1609268&amp;uName=ÀÇÈ&amp;uDeptID=10001250</url>\n" + 
//				"<draftnm>±è¿µ</draftnm>\n" + 
//				"<draftdt>2019-03-08 ¿ÀÄ4:05:19</draftdt>\n" + 
//				"</Item>\n" + 
//				"<Item>\n" + 
//				"<docid />\n" + 
//				"<subject>¼ûv Å½ºƮ1</subject>\n" + 
//				"<url>http://ucdev.imobis.co.kr/myoffice/ezEmail/DLmail_read.aspx?messageid=&lt;A9B02EFC-6370-4C65-B6AD-FD7EE1629151@mobisdev.com&gt;                                                 &amp;saveurl=DLManager@mobisdev.com                            &amp;sendermail=1609268@mobisdev.com                                                                                &amp;partopen=Y</url>\n" + 
//				"<draftnm>ÀÇÈ</draftnm>\n" + 
//				"<draftdt>2018-08-24 ¿ÀÄ4:20:56</draftdt>\n" + 
//				"</Item></MAIBOT>";
		MaibotApproval out = null;
		try {
			out = this.parseApproval(resp);
		} catch(Exception e) {
			return null;
		}
		return out;
	}
}
