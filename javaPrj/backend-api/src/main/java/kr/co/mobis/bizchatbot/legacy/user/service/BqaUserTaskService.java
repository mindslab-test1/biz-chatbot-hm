package kr.co.mobis.bizchatbot.legacy.user.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.mobis.bizchatbot.legacy.user.dao.MobisUserOrgMapper;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.BqaQuestionData;

@Service
public class BqaUserTaskService {
	
	private Logger logger = LoggerFactory.getLogger(BqaUserTaskService.class);

	@Autowired
	private MobisUserOrgMapper mobisMapper;
	
	public BqaUserTaskService() {
	}

	@Transactional
	public List<BqaQuestionData> getQuestionsByUserId(String botId, String userId) throws Exception{
		logger.info("getQuestionsByUserId/{},{}", botId, userId);
		
		List<BqaQuestionData> list = null;
		try {
			HashMap<String,String> params = new HashMap<>();
			params.put("botId", botId);
			params.put("userId", userId);
			list = mobisMapper.questionsByAttr(params);
		}catch(Exception e) {
			logger.warn("getQuestionsByUserId/{}", e.getMessage());
			throw e;
		}
		return list;
	}
}
