package kr.co.mobis.bizchatbot.legacy.cafeteria.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import kr.co.mobis.bizchatbot.legacy.location.dao.MobisBranchItpMapper;
import kr.co.mobis.bizchatbot.legacy.location.dao.data.MobisBranchIpData;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.bizchatbot.commons.utils.DateUtils;
import kr.co.mobis.bizchatbot.IMobisConsts;
import kr.co.mobis.bizchatbot.legacy.cafeteria.dao.CafeMenuMapper;
import kr.co.mobis.bizchatbot.legacy.cafeteria.dao.data.CafeMenuInfo;

@Service
//@Transactional(transactionManager="cafe-transactionmanager")
public class MobisCafeteriaService implements IMobisConsts{

	private Logger logger = LoggerFactory.getLogger(MobisCafeteriaService.class);

	@Autowired
	private CafeMenuMapper cafe;

	@Autowired
	private MobisBranchItpMapper branch;


	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	public MobisCafeteriaService() {
	}

	//
	@Transactional(transactionManager="cafe-transactionmanager", propagation=Propagation.REQUIRES_NEW, readOnly=true, rollbackFor= {Exception.class})
	public CafeMenuInfo getTodayMenu(CAFE_LOC location) throws Exception{
		logger.info("getTodayMenu/{}", location);

		CafeMenuInfo menu = null;
		Date now = DateUtils.getNow();
		String menuDate = sdf.format(now);

		HashMap<String,String> params = new HashMap<>();
		params.put("location", location.toString());
		params.put("date", menuDate);
		menu = cafe.getCafeMenu(params);

		return menu;
	}

	@Transactional(transactionManager="cafe-transactionmanager" , propagation=Propagation.REQUIRES_NEW, readOnly=true, rollbackFor= {Exception.class})
	public CAFE_LOC getCafeLocation(String remoteAddress) throws Exception {

		if(StringUtils.isEmpty(remoteAddress)) {
			return CAFE_LOC.CAFE_LOC_OTHERS;
		}

		Map<String, Object> param = new HashMap<>();
		param.put("userIp", remoteAddress);
		MobisBranchIpData data = branch.getBranch(param);


		if(data == null){
			return CAFE_LOC.CAFE_LOC_OTHERS;
		}else {
			if (data.getSpotName().indexOf("마북") > -1) {
				return CAFE_LOC.CAFE_LOC_RC;
			} else if (data.getSpotName().indexOf("서산") > -1) {
				return CAFE_LOC.CAFE_LOC_SS;
			} else if (data.getSpotName().indexOf("본사") > -1) {
				return CAFE_LOC.CAFE_LOC_HQ;
			} else {
				return CAFE_LOC.CAFE_LOC_OTHERS;
			}
		}

		/*if(remoteAddress.matches(patternLocationHQ)) {
			return CAFE_LOC.CAFE_LOC_HQ;
		}else if(remoteAddress.matches(patternLocationRC)) {
			return CAFE_LOC.CAFE_LOC_RC;
		}else if(remoteAddress.matches(patternLocationSS)) {
			return CAFE_LOC.CAFE_LOC_SS;
		}else {
			return CAFE_LOC.CAFE_LOC_OTHERS;
		}*/
	}
}
