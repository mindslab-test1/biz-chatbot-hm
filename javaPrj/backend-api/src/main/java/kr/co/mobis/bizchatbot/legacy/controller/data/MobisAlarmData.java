package kr.co.mobis.bizchatbot.legacy.controller.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MobisAlarmData extends MobisAlarmSimpleData {

	protected ChatbotNotice notice;
	protected String mailUrl;
	protected String approvalUrl;
	
	public MobisAlarmData() {
	}

	public ChatbotNotice getNotice() {
		return notice;
	}

	public void setNotice(ChatbotNotice notice) {
		this.notice = notice;
	}
	
	public String getMailUrl() {
		return mailUrl;
	}

	public void setMailUrl(String mailUrl) {
		this.mailUrl = mailUrl;
	}

	public String getApprovalUrl() {
		return approvalUrl;
	}

	public void setApprovalUrl(String approvalUrl) {
		this.approvalUrl = approvalUrl;
	}

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public static class ChatbotNotice implements Serializable{
		protected MobisNormalNotice botNotice;
		protected MobisSearchNotice searchNotice;
		
		public ChatbotNotice() {
			
		}

		public MobisNormalNotice getBotNotice() {
			return botNotice;
		}

		public void setBotNotice(MobisNormalNotice botNotice) {
			this.botNotice = botNotice;
		}

		public MobisSearchNotice getSearchNotice() {
			return searchNotice;
		}

		public void setSearchNotice(MobisSearchNotice searchNotice) {
			this.searchNotice = searchNotice;
		}
		
	}
}
