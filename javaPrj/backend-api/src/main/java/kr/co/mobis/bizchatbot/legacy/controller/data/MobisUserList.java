package kr.co.mobis.bizchatbot.legacy.controller.data;

import kr.co.mobis.bizchatbot.legacy.user.dao.data.BqaQuestionData;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.MobisUserData;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.MobisUserSimpleData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MobisUserList implements Serializable{

	protected List<MobisUserSimpleData> userList;
	protected String sysUtterTypeCode;

	public MobisUserList() {
	}

	public List<MobisUserSimpleData> getUserList() {
		return userList;
	}

	public void setUserList(List<MobisUserSimpleData> userList) {
		this.userList = userList;
	}

	public String getSysUtterTypeCode() {
		return sysUtterTypeCode;
	}

	public void setSysUtterTypeCode(String sysUtterTypeCode) {
		this.sysUtterTypeCode = sysUtterTypeCode;
	}

	@Override
	public String toString() {
		return "MobisUserList{" +
				"userList=" + userList +
				", sysUtterTypeCode='" + sysUtterTypeCode + '\'' +
				'}';
	}
}
