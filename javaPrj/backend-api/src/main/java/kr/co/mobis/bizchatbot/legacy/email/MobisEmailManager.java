package kr.co.mobis.bizchatbot.legacy.email;

import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;
import kr.co.mobis.bizchatbot.legacy.email.data.MaibotEmail;

@Service
public class MobisEmailManager {
	
	private Logger logger = LoggerFactory.getLogger(MobisEmailManager.class);

	@Value("${legacy.email.url:}")
	public String emaillUrl;
	
	private Pattern pMailCount = Pattern.compile("<int.*>(\\d*)</int>");
	
	public MobisEmailManager() {
	}

	
	public MaibotEmail parseUnreadCount(String xml) throws Exception{
//		Pattern pMailCount = Pattern.compile("<int.*>(\\d*)</int>");
		
//		Matcher m = pMailCount.matcher(xml);
//		String count = "0";
//		if(m.find()) {
//			count = m.group(1);
//		}
//		return Integer.parseInt(count);
		
		JAXBContext jc = JAXBContext.newInstance(MaibotEmail.class);
		Unmarshaller unmashaller = jc.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		MaibotEmail out = (MaibotEmail)unmashaller.unmarshal(reader);
		return out;
	}
	
	public MaibotEmail parseMailList(String xml) throws Exception{
		JAXBContext jc = JAXBContext.newInstance(MaibotEmail.class);
		Unmarshaller unmashaller = jc.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		MaibotEmail out = (MaibotEmail)unmashaller.unmarshal(reader);
		return out;
	}
	
	public MaibotEmail getMailUnreadCount(String userId) throws Exception{
		logger.info("getMailUnreadCount/{}",userId);
		String url = emaillUrl + "/MailUnreadCount";
		url = url + "?userID="+userId;

		logger.info("emailCount url : " +  url);
		String resp = HttpConnectionManager.getInstance().get(url, null);
		//test
//		String resp = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + 
//				"<int xmlns=\"http://uc.imobis.co.kr/MaiBot/MaiBot.asmx\">4</int>";
		
		//updated test
//		String resp = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + 
//				"<MAIBOT>\n" + 
//				"  <Item>\n" + 
//				"    <count>7</count>\n" + 
//				"    <url>http://ucdev.imobis.co.kr/myoffice/main/index_myoffice.aspx?funCode=1</url>\n" + 
//				"  </Item>\n" + 
//				"</MAIBOT>";
		return parseUnreadCount(resp);
	}
	
	public MaibotEmail getUnreadMailList(String userId, int count) throws Exception{
		logger.info("getUnreadMailList/{},{}",userId, count);
		String url = emaillUrl + "/MailList";
		url = url + "?userID="+userId+"&count="+count;

		logger.info("email url : " +  url);
		String resp = HttpConnectionManager.getInstance().get(url, null);
		//test
//		String resp = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + 
//				"<MAIBOT>\n" + 
//				"  <Item>\n" + 
//				"    <count>4</count>\n" + 
//				"    <subject>111111 김창영, 임혜훈, 최재원</subject>\n" + 
//				"    <receivedt>2019-03-13 17:38:39</receivedt>\n" + 
//				"    <url>http://ucdev.imobis.co.kr/myoffice/ezEmail/mail_read.aspx?URL=AAMkAGI1YWJiYTZlLWJiMGQtNDc3MC05MWExLTk3MGFlZTA2MjUzMQBGAAAAAADOfXEjwdgDTYAZLABHOBJMBwDgQFg/GACqTrWhk3EGOj6MAAAAAAEMAADgQFg/GACqTrWhk3EGOj6MAAJg/YBXAAA=&amp;PNFlag=Y&amp;CONTENTCLASS=IPM.NOTE</url>\n" + 
//				"    <fromname>김창영</fromname>\n" + 
//				"  </Item>\n" + 
//				"  <Item>\n" + 
//				"    <count>4</count>\n" + 
//				"    <subject>수신확인1</subject>\n" + 
//				"    <receivedt>2019-03-13 15:05:01</receivedt>\n" + 
//				"    <url>http://ucdev.imobis.co.kr/myoffice/ezEmail/mail_read.aspx?URL=AAMkAGI1YWJiYTZlLWJiMGQtNDc3MC05MWExLTk3MGFlZTA2MjUzMQBGAAAAAADOfXEjwdgDTYAZLABHOBJMBwDgQFg/GACqTrWhk3EGOj6MAAAAAAEMAADgQFg/GACqTrWhk3EGOj6MAAJg/YBWAAA=&amp;PNFlag=Y&amp;CONTENTCLASS=IPM.NOTE</url>\n" + 
//				"    <fromname>김창영</fromname>\n" + 
//				"  </Item>\n" + 
//				"  <Item>\n" + 
//				"    <count>4</count>\n" + 
//				"    <subject>111</subject>\n" + 
//				"    <receivedt>2019-01-29 10:34:15</receivedt>\n" + 
//				"    <url>http://ucdev.imobis.co.kr/myoffice/ezEmail/mail_read.aspx?URL=AAMkAGI1YWJiYTZlLWJiMGQtNDc3MC05MWExLTk3MGFlZTA2MjUzMQBGAAAAAADOfXEjwdgDTYAZLABHOBJMBwDgQFg/GACqTrWhk3EGOj6MAAAAAAEMAADgQFg/GACqTrWhk3EGOj6MAAJg/YBSAAA=&amp;PNFlag=Y&amp;CONTENTCLASS=IPM.NOTE</url>\n" + 
//				"    <fromname>임혜훈</fromname>\n" + 
//				"  </Item>\n" + 
//				"  <Item>\n" + 
//				"    <count>4</count>\n" + 
//				"    <subject>111</subject>\n" + 
//				"    <receivedt>2019-01-29 10:32:32</receivedt>\n" + 
//				"    <url>http://ucdev.imobis.co.kr/myoffice/ezEmail/mail_read.aspx?URL=AAMkAGI1YWJiYTZlLWJiMGQtNDc3MC05MWExLTk3MGFlZTA2MjUzMQBGAAAAAADOfXEjwdgDTYAZLABHOBJMBwDgQFg/GACqTrWhk3EGOj6MAAAAAAEMAADgQFg/GACqTrWhk3EGOj6MAAJg/YBQAAA=&amp;PNFlag=Y&amp;CONTENTCLASS=IPM.NOTE</url>\n" + 
//				"    <fromname>임혜훈</fromname>\n" + 
//				"  </Item>\n" + 
//				"</MAIBOT>";
		return parseMailList(resp);
	}
	

}
