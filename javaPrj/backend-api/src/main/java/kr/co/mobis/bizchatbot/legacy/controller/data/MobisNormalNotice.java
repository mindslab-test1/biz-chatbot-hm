package kr.co.mobis.bizchatbot.legacy.controller.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.NoticeInfo;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MobisNormalNotice implements Serializable{

	protected List<NoticeInfo> notices;
	
	public MobisNormalNotice() {
	}

	public List<NoticeInfo> getNotices() {
		if(notices == null) {
			notices = new ArrayList<>();
		}
		return notices;
	}

	public void setNotices(List<NoticeInfo> notices) {
		this.notices = notices;
	}

}
