package kr.co.mobis.bizchatbot.legacy.approval.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mobot"
})
@XmlRootElement(name = "MAIBOT")
public class MaibotApproval implements Serializable{

	@XmlElement(name = "Item")
	private List<Item> mobot;
	
	public MaibotApproval() {
	}

	public List<Item> getMobot() {
		if(mobot == null) {
			mobot = new ArrayList<>();
		}
		return mobot;
	}

	public void setMobot(List<Item> mobot) {
		this.mobot = mobot;
	}

	
}
