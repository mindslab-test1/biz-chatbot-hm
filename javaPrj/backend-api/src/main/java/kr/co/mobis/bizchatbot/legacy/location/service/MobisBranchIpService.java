package kr.co.mobis.bizchatbot.legacy.location.service;


import kr.co.mobis.bizchatbot.IMobisConsts;
import kr.co.mobis.bizchatbot.legacy.location.dao.MobisBranchItpMapper;
import kr.co.mobis.bizchatbot.legacy.location.dao.data.MobisBranchIpData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Service
public class MobisBranchIpService implements IMobisConsts {


    @Autowired
    MobisBranchItpMapper mapper;


    @Transactional
    public MobisBranchIpData getBranch(String userIp) throws Exception{

        Map<String, Object> param = new HashMap<>();
        param.put("userIp", userIp);

        MobisBranchIpData data = mapper.getBranch(param);

        if(data !=null){
            setBrachType(data);
        }

        return data;
    }


    public void setBrachType(MobisBranchIpData data){


        if(data.getSpotName().indexOf("마북") > -1 ){
            data.setSpotType(CAFE_LOC.CAFE_LOC_RC.toString());
        }else  if(data.getSpotName().indexOf("서산") >  -1  ){
            data.setSpotType(CAFE_LOC.CAFE_LOC_SS.toString());
        }else  if(data.getSpotName().indexOf("본사") >  -1 ){
            data.setSpotType(CAFE_LOC.CAFE_LOC_HQ.toString());
        }else {
            data.setSpotType(CAFE_LOC.CAFE_LOC_OTHERS.toString());
        }
    }
}
