package kr.co.mobis.bizchatbot.legacy.bus.dao;

import kr.co.mobis.bizchatbot.legacy.bus.dao.data.MobisBusData;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface MobisBusMapper {

	public List<MobisBusData> getBusByName(HashMap<String, Object> params);
	public List<MobisBusData> getBusByBusStop(HashMap<String, Object> params);
}
