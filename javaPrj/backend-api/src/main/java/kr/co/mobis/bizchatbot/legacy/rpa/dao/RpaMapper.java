package kr.co.mobis.bizchatbot.legacy.rpa.dao;

import kr.co.mobis.bizchatbot.legacy.rpa.dao.data.RpaDataVO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RpaMapper {
    int insertRpaInfo(RpaDataVO vo) throws Exception;
}
