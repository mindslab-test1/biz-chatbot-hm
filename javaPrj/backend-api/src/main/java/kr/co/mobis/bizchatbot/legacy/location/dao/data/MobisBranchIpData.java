package kr.co.mobis.bizchatbot.legacy.location.dao.data;

import java.io.Serializable;

public class MobisBranchIpData implements Serializable {
    private int id;
    private String spotName;
    private String spotName2;
    private String spotType;
    private String startIp;
    private String endIp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpotName() {
        return spotName;
    }

    public void setSpotName(String spotName) {
        this.spotName = spotName;
    }

    public String getSpotName2() {
        return spotName2;
    }

    public void setSpotName2(String spotName2) {
        this.spotName2 = spotName2;
    }

    public String getSpotType() {
        return spotType;
    }

    public void setSpotType(String spotType) {
        this.spotType = spotType;
    }



    public String getStartIp() {
        return startIp;
    }

    public void setStartIp(String startIp) {
        this.startIp = startIp;
    }

    public String getEndIp() {
        return endIp;
    }

    public void setEndIp(String endIp) {
        this.endIp = endIp;
    }



    @Override
    public String toString() {
        StringBuilder str =  new StringBuilder();
        str.append("id+ "+ this.getId() +", ");
        str.append("spotName+ "+ this.getSpotName() +", ");
        str.append("spotName2+ "+ this.getSpotName2() +", ");
        str.append("spotType+ "+ this.getSpotType() +", ");
        str.append("startIp+ "+ this.getStartIp() +", ");
        str.append("endIp+ "+ this.getEndIp() +", ");

        return str.toString();
    }
}

