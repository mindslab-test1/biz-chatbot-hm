package kr.co.mobis.bizchatbot.legacy.bus.service;

import kr.co.mobis.bizchatbot.legacy.bus.dao.MobisBusMapper;
import kr.co.mobis.bizchatbot.legacy.bus.dao.data.BusStop;
import kr.co.mobis.bizchatbot.legacy.bus.dao.data.MobisBusData;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Service
public class MobisBusService {

	private Logger logger = LoggerFactory.getLogger(MobisBusService.class);

	@Autowired
	private MobisBusMapper busMapper;

	@Transactional
	public List<MobisBusData> getBusInfoByName(String routeName, String type) throws Exception{
		logger.info("getBusInfoByName/{},{}", routeName, type);
		List<MobisBusData> buses = null;

		try {
			String pRouteName = this.getFullTextSearchParam(routeName);
			String pType = this.getConvertedTypeParam(type);

			logger.info("getBusInfoByName converted params/{},{}", pRouteName, pType);

			HashMap<String, Object> params = new HashMap<>();
			params.put("routeName", pRouteName);
			params.put("type", pType);

			buses = busMapper.getBusByName(params);

			for (MobisBusData bus : buses) {
				List<BusStop> stops = this.getStops(bus);
				bus.setStops(stops);
			}
		}catch(Exception e) {
			logger.warn("getBusInfoByName/{}", e.getMessage());
			throw e;
		}
		return buses;
	}

	@Transactional
	public List<MobisBusData> getBusInfoByBusStop(String stopName, String type) throws Exception{
		logger.info("getBusInfoByBusStop/{},{}", stopName, type);
		List<MobisBusData> buses = null;

		try {
			String pStopName = this.getFullTextSearchParam(stopName);
			String pType = this.getConvertedTypeParam(type);
			String shuttle = null;

			if(StringUtils.equals(pType, "셔틀")) {
				shuttle = pType+"* _"+pType+"*";
			} else if(StringUtils.contains(pType, "순환")) {
				shuttle = pType+"*";
			}

			logger.info("getBusInfoByBusStop  converted params/{},{}", pStopName, pType);

			HashMap<String, Object> params = new HashMap<>();
			params.put("stopName", pStopName);
			params.put("type", pType);
			params.put("shuttle", shuttle);

			buses = busMapper.getBusByBusStop(params);

			for (MobisBusData bus : buses) {
				List<BusStop> stops = this.getStops(bus);
				bus.setStops(stops);

				String kind = this.getKindOfBus(bus);
				bus.setKind(kind);
			}
		}catch(Exception e) {
			logger.warn("getBusInfoByBusStop/{}", e.getMessage());
			throw e;
		}
		return buses;
	}

	private String getFullTextSearchParam(String param) {
		String ftParam = param.trim().replaceAll("[\\(\\)<>]", "");
		if(ftParam.contains(" ")) {
			String oneWord = ftParam.replace(" ", "") + "*";
			String replaceWord = ftParam.replace(" ", "*");
			ftParam = oneWord + " " + replaceWord;
		}
		ftParam = ftParam + "*";
		return ftParam;
	}

	private String getConvertedTypeParam(String type) {
		String pType = null;
		int num = 0;

		if(type != null) {
			if(StringUtils.isNumeric(type)) {
				num = Integer.parseInt(type);
			}

			if(num == 1 || StringUtils.equals(type.trim(), "출근")) {
				pType = "출근";
			} else if(num == 2 || StringUtils.equals(type.trim(), "퇴근")) {
				pType = "퇴근";
			} else if(num == 3 || StringUtils.equals(type.trim(), "셔틀")) {
				pType = "셔틀";
			} else if(num == 4 || StringUtils.contains(type.trim(), "순환")) {
				pType = "순환";
			}
		}

		return pType;
	}

	private List<BusStop> getStops(MobisBusData bus) {

		String[] routeStrArr = bus.getRoute().split("\\|");
		List<BusStop> stops = new ArrayList<>();

		for (String s : routeStrArr) {
			String[] arr = s.split(",");
			int arrLen = arr.length;

			String stopName = (0 < arrLen) ? arr[0] : null;
			String stopTime = (1 < arrLen) ? arr[1] : null;

			if(!(StringUtils.isEmpty(stopName) && StringUtils.isEmpty(stopTime))) {
				BusStop stop = new BusStop();
				stop.setStopName(stopName);
				stop.setStopTime(stopTime);
				stops.add(stop);
			}
		}
		return stops;
	}

	private String getKindOfBus(MobisBusData bus) {
		String opName = bus.getOpName();
		String opKind = bus.getOpKind();

		String kind = null;
		if(opName.contains("순환셔틀")) {
			kind = "순환셔틀";
		} else if(opName.contains("셔틀")) {
			kind = "셔틀";
		} else if(opKind.contains("출근")) {
			kind = "출근";
		} else if(opKind.contains("퇴근")) {
			kind = "퇴근";
		}

		return kind;
	}
}
