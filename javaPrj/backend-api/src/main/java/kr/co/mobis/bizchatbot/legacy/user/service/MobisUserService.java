package kr.co.mobis.bizchatbot.legacy.user.service;

import java.util.HashMap;
import java.util.List;

import kr.co.mobis.bizchatbot.legacy.controller.data.MobisTeamData;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.mobis.bizchatbot.legacy.user.dao.MobisUserOrgMapper;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.MobisUserData;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.MobisUserSimpleData;
import kr.co.mobis.bizchatbot.legacy.utils.AES256Cipher;


@Service
public class MobisUserService {

	private Logger logger = LoggerFactory.getLogger(MobisUserService.class);

	@Autowired
	private MobisUserOrgMapper mobisMapper;

	@Transactional
	public MobisUserData getUserInfo(String userId) throws Exception{
		logger.info("getUserInfo/{}",userId);
		MobisUserData user = null;

		try {
			user = mobisMapper.getUserById(userId);
			logger.debug("user:{}", user);//before decoding
			decodeUserInfo(user);
		}catch(Exception e) {
			logger.warn("getUserInfo/{}", e.getMessage());
			throw e;
		}

		return user;
	}

	@Transactional
	public List<MobisUserSimpleData> getUserInfoByUserNm(String userNm) throws Exception{
		logger.info("getUserInfoByUserNm/{}",userNm);
		List<MobisUserSimpleData> users = null;
		try {
			users = mobisMapper.getUserByUserNm(userNm);
			if(users != null) {
				for(MobisUserSimpleData user:users) {
					decodeUserInfo(user);
				}
			}
		}catch(Exception e) {
			logger.warn("getUserInfoByUserNm/{}", e.getMessage());
			throw e;
		}
		return users;
	}

	@Transactional
	public List<MobisUserSimpleData> getUserInfoByUserNmDeptNm(String userNm, String deptNm) throws Exception{
		logger.info("getUserInfoByUserNmDeptNm/{},{}",userNm, deptNm);
		List<MobisUserSimpleData> users = null;
		try {
			HashMap<String, String> params = new HashMap<>();
			params.put("userNm", userNm);
			params.put("deptNm", deptNm);
			users = mobisMapper.getUserByUserNmDeptNm(params);
			if(users != null) {
				for(MobisUserSimpleData user:users) {
					decodeUserInfo(user);
				}
			}
		}catch(Exception e) {
			logger.warn("getUserInfoByUserNmDeptNm/{}", e.getMessage());
			throw e;
		}
		return users;
	}

	@Transactional
	public List<MobisUserSimpleData> getUserInfoByUserTask(String userTask, int count) throws Exception{
		logger.info("getUserInfoByUserTask/{},{}", userTask, count);
		List<MobisUserSimpleData> users = null;
		String pUserTask = userTask.trim().replaceAll("[\\(\\)<>]", "");

		try {
			if(pUserTask.contains(" ")) {
				String oneWord = pUserTask.replace(" ", "") + "*";
				String replaceWord = pUserTask.replace(" ", "*");
				pUserTask = oneWord + " " + replaceWord;
			}
			pUserTask = pUserTask + "*";

			HashMap<String, Object> params = new HashMap<>();
			params.put("userTask", pUserTask);
			params.put("count", count);

			users = mobisMapper.getUserByUserTask(params);
			if(users != null) {
				for(MobisUserSimpleData user:users) {
					decodeUserInfo(user);
				}
			}
		}catch(Exception e) {
			logger.warn("getUserInfoByUserTask/{}", e.getMessage());
			throw e;
		}
		return users;
	}

	@Transactional
	public int setUserSecureConfirm(String userId)  throws Exception{
		logger.info("setUserSecureConfirm/{}", userId);
		return mobisMapper.setUserSecureConfirm(userId);
	}

	protected void decodeUserInfo(MobisUserSimpleData user) throws Exception {
		if(user != null) {
			if(!StringUtils.isEmpty(user.getUserEmail())) {
				user.setUserEmail(AES256Cipher.AES_Decode(user.getUserEmail()));
			}
			if(!StringUtils.isEmpty(user.getUserMobile())) {
				user.setUserMobile(AES256Cipher.AES_Decode(user.getUserMobile()));
			}
			if(!StringUtils.isEmpty(user.getUserTelephone())) {
				user.setUserTelephone(AES256Cipher.AES_Decode(user.getUserTelephone()));
			}
		}
	}


	public List<MobisTeamData> getMobisTeamList() throws  Exception {
		return mobisMapper.getMobisTeamList();
	}
}
