package kr.co.mobis.bizchatbot.legacy.resource.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MobisResourceData implements Serializable{

	protected String status;
	protected List<String> list;

	public MobisResourceData() {
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getList() {
		if(list == null) {
			list = new ArrayList<>();
		}
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "MobisResourceData{" +
				"status='" + status + '\'' +
				", list=" + list +
				'}';
	}
}
