package kr.co.mobis.bizchatbot.legacy.anwserpref.dao;

import kr.co.mobis.bizchatbot.legacy.anwserpref.dao.data.ChatbotRespData;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AnswerPrefDao {
     ChatbotRespData getAnswerPref(ChatbotRespData vo) throws Exception;
     int insertAnswerPrefHistory(ChatbotRespData vo)  throws  Exception;
     int updateAnswerPrefHistory(ChatbotRespData vo)  throws  Exception;
}
