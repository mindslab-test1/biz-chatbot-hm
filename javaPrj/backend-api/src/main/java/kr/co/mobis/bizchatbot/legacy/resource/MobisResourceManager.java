package kr.co.mobis.bizchatbot.legacy.resource;

import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;
import kr.co.mobis.bizchatbot.legacy.resource.data.Item;
import kr.co.mobis.bizchatbot.legacy.resource.data.MaibotResource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@Service
public class MobisResourceManager {

	private Logger logger = LoggerFactory.getLogger(MobisResourceManager.class);
	
	@Value("${legacy.resource.url:}")
	public String resourceUrl;
	
	public MobisResourceManager() {
	}

	public MaibotResource parseResource(String xml) throws Exception{
		JAXBContext jc = JAXBContext.newInstance(MaibotResource.class);
		Unmarshaller unmashaller = jc.createUnmarshaller();
		StringReader reader = new StringReader(xml);
		MaibotResource out = (MaibotResource)unmashaller.unmarshal(reader);
		return out;
	}

	public List<String> getResourceList(String userId, String stime, String etime, String pgNoStr) throws Exception{
		logger.info("getResourceList/{},{},{}",userId ,stime ,etime);
		String url = resourceUrl + "/ResourceList";
		url = url + "?userID="+userId+"&stime="+stime+"&etime="+etime+"&roomname=";
		
		MaibotResource maibotResource = null;
		try {
			String resp = HttpConnectionManager.getInstance().get(url, null);
			maibotResource = this.parseResource(resp);
		} catch(Exception e) {
			return null;
		}

		List<Item> mobot = maibotResource.getMobot();
		List<String> list = new ArrayList<>();
		int roomId = 0;
		for(Item item : mobot) {
			if (roomId != item.getRoomid()) {
				String roomInfo = item.getRoomInfo().replaceAll("[\\(\\)\\#\\[\\]\\/]", " ");
				list.add(roomInfo);

				roomId = item.getRoomid();
			}
		}

		// 페이징 처리 로직 추가
		List<String> rtnList = new ArrayList<>();
		int roomCounts = list.size();
		int pgNo = 1;
		int LIMIT = 10;
		int lastPg = (roomCounts / LIMIT) + 1;

		// pgNo 계산 처리
		if (StringUtils.isNumeric(pgNoStr)) {
			pgNo = Integer.parseInt(pgNoStr);
		}
		if (pgNo < 1) {
			pgNo = 1;
		}
		if (pgNo > lastPg) {
			pgNo = lastPg;
		}

		// subList 처리 및 이전, 다음 리스트 버튼 추가하기
		if(roomCounts > 0) {
			int start = (pgNo - 1) * LIMIT;
			int end = pgNo * LIMIT;
			if(roomCounts < end) {
				end = roomCounts;
			}
			rtnList = list.subList(start, end);

			if (pgNo > 1) {
				rtnList.add(0, "이전 리스트 " + (pgNo - 1));
			}
			if (pgNo < lastPg) {
				rtnList.add("다음 리스트 " + (pgNo + 1));
			}
		}

		return rtnList;
}

	public String resourceSave(String userId, String stime, String etime, String importance, String roomId, String subject) throws Exception{
		logger.info("ResourceSave/{},{},{},{},{},{}",userId ,stime ,etime, importance, roomId, subject);

		String subjectEncodeUTF8 = URLEncoder.encode(subject, "UTF-8");
		logger.debug("ResourceSave subject utf-8 encoded/{}", subjectEncodeUTF8);

		String url = resourceUrl + "/ResourceSave";
		url = url + "?userID="+userId+"&stime="+stime+"&etime="+etime
				+"&importance="+importance+"&roomid="+roomId+"&subject="+subjectEncodeUTF8;

		//test
//		String resp = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
//				"<string xmlns=\"http://uc.imobis.co.kr/MaiBot/MaiBot.asmx\">4</string>";

		String numStr;
		try {
			String resp = HttpConnectionManager.getInstance().get(url, null);
			numStr = resp.split("<string xmlns=\\\"http://uc.imobis.co.kr/MaiBot/MaiBot.asmx\\\">")[1].split("</string>")[0];
		} catch(Exception e) {
			numStr =  "-1";
		}

		logger.info("ResourceSave numStr/{}", numStr);

		String rtnStr = null;
		switch (numStr) {
			case "1":
				rtnStr = "해당 회의실로 예약 완료 되었어요";
				break;
			case "2":
				rtnStr = "해당 회의실로 신청 완료 되었어요. <br>담당자가 사용 승인을 하면 예약이 완료됩니다.";
				break;
			case "3":
				rtnStr = "회의실 예약이 실패했어요. <br>다른분이 먼저 예약을 했을수 있습니다. <br>회의실 조회를 입력하여 다시한번 회의실을 조회해 보시겠어요";
				break;
			case "-1":
			default:
				rtnStr = "회의실 시스템에서 오류가 발생했어요. <br>담당자에게 문의해주세요";
				break;
		}

		return rtnStr;
	}

	public MaibotResource getMyResourceList(String userId) throws Exception{
		logger.info("MyResourceList/{}",userId);
		String url = resourceUrl + "/MyResourceList";
		url = url + "?userID="+userId;

		String resp = HttpConnectionManager.getInstance().get(url, null);
		MaibotResource maibotResource = null;
		try {
			maibotResource = this.parseResource(resp);
		} catch(Exception e) {
			logger.debug("MyResourceList NO datas");
		}
		return maibotResource;
	}
}
