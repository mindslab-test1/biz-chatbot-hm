package kr.co.mobis.bizchatbot.legacy.rpa.service;

import kr.co.mobis.bizchatbot.legacy.rpa.dao.RpaMapper;
import kr.co.mobis.bizchatbot.legacy.rpa.dao.data.RpaDataVO;
import kr.co.mobis.bizchatbot.legacy.user.dao.MobisUserOrgMapper;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.MobisUserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RpaService {

    @Autowired
    RpaMapper dao;

    @Autowired
    MobisUserOrgMapper userOrgMapper;

    public MobisUserData getRequestUserName(String userId)throws Exception {
        return userOrgMapper.getUserById(userId);
    }

    public int insertRpaInfo(RpaDataVO vo) throws Exception {
        return dao.insertRpaInfo(vo);
    }
}
