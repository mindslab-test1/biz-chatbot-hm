package kr.co.mobis.bizchatbot.legacy.anwserpref.dao.data;

import java.io.Serializable;

public class ChatbotRespData implements Serializable {

    private Long seq;  // 순번
    private String userId; // 사용자  id
    private Long botId;  // 챗봇 명
    private String dialogType;  // 대화타입
    private String question;  // 질문
    private String answer;  // 답변
    private String countType;  //  like or dislike
    private Long likeCnt;
    private Long disLikeCnt;



    public Long getSeq() {
        return seq;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }

    public Long getBotId() {
        return botId;
    }


    public void setBotId(Long botId) {
        this.botId = botId;
    }

    public void setUserId(String userId){
        this.userId = userId;
    }

    public String getUserId() {
        return this.userId;
    }

    public String getDialogType() {
        return dialogType;
    }

    public void setDialogType(String dialogType) {
        this.dialogType = dialogType;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCountType() {
        return countType;
    }

    public void setCountType(String countType) {
        this.countType = countType;
    }

    public Long getLikeCnt() {
        return likeCnt;
    }

    public void setLikeCnt(Long likeCnt) {
        this.likeCnt = likeCnt;
    }



    public Long getDisLikeCnt() {
        return disLikeCnt;
    }

    public void setDisLikeCnt(Long disLikeCnt) {
        this.disLikeCnt = disLikeCnt;
    }
}
