package kr.co.mobis.bizchatbot.legacy.user.dao;

import java.util.HashMap;
import java.util.List;

import kr.co.mobis.bizchatbot.legacy.controller.data.MobisTeamData;
import org.apache.ibatis.annotations.Mapper;

import kr.co.mobis.bizchatbot.legacy.user.dao.data.BqaQuestionData;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.MobisOrgData;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.MobisUserData;
import kr.co.mobis.bizchatbot.legacy.user.dao.data.MobisUserSimpleData;

@Mapper
public interface MobisUserOrgMapper {

	public MobisUserData getUserById(String userId);
	public MobisOrgData getOrgByObjid(String objid);
	public List<MobisUserSimpleData> getUserByUserNm(String userNm);
	public List<MobisUserSimpleData> getUserByUserNmDeptNm(HashMap<String,String> params);
	public List<MobisUserSimpleData> getUserByUserTask(HashMap<String,Object> params);
	public List<BqaQuestionData> questionsByAttr(HashMap<String,String> params);
	public int setUserSecureConfirm(String userId);
	public List<MobisTeamData> getMobisTeamList();
}
