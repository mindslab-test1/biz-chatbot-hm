package kr.co.mobis.bizchatbot.legacy.resource.data;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"roomid",
		"roomname",
		"proomid",
		"stime",
		"etime",
		"approvaeflag",
		"url",
		"approvalid",
		"approvalname",
		"approvaldeptnm",
		"importance"
})
public class Item implements Serializable{

	protected int roomid;
	protected String roomname;
	protected int proomid;
	protected String stime;
	protected String etime;
	protected int approvaeflag;
	protected String url;
	protected String approvalid;
	protected String approvalname;
	protected String approvaldeptnm;
	protected String importance;

	public Item() {
	}

	public int getRoomid() {
		return roomid;
	}

	public void setRoomid(int roomid) {
		this.roomid = roomid;
	}

	public String getRoomname() {
		return roomname;
	}

	public void setRoomname(String roomname) {
		this.roomname = roomname;
	}

	public int getProomid() {
		return proomid;
	}

	public void setProomid(int proomid) {
		this.proomid = proomid;
	}

	public String getStime() {
		return stime;
	}

	public void setStime(String stime) {
		this.stime = stime;
	}

	public String getEtime() {
		return etime;
	}

	public void setEtime(String etime) {
		this.etime = etime;
	}

	public int getApprovaeflag() {
		return approvaeflag;
	}

	public void setApprovaeflag(int approvaeflag) {
		this.approvaeflag = approvaeflag;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getApprovalid() {
		return approvalid;
	}

	public void setApprovalid(String approvalid) {
		this.approvalid = approvalid;
	}

	public String getApprovalname() {
		return approvalname;
	}

	public void setApprovalname(String approvalname) {
		this.approvalname = approvalname;
	}

	public String getApprovaldeptnm() {
		return approvaldeptnm;
	}

	public void setApprovaldeptnm(String approvaldeptnm) {
		this.approvaldeptnm = approvaldeptnm;
	}

	public String getImportance() {
		return importance;
	}

	public void setImportance(String importance) {
		this.importance = importance;
	}

	public String getDisplayInfo() {
//		04/02 15:30 - 17:00
//		대회의실 1703호
		return getDisplayTime(getStime(), getEtime()) + "<br>" + getRoomname() + "$CSS" +  getUrl();
	}

	public String getRoomInfo() {
		return getRoomname() + " " + getRoomid();
	}

	public String getDisplayTime(String stime, String etime) {
		String displayTime = "";
		Pattern p = Pattern.compile("(\\d*)-(\\d*)-(\\d*)\\s(\\W*\\s\\d*:\\d*):(\\d*)");
		Matcher stimeMatch = p.matcher(stime);
		if(stimeMatch.find()) {
			String month = stimeMatch.group(2);
			String day = stimeMatch.group(3);
			String time = stimeMatch.group(4);

			displayTime += month + "/" + day + " " + time;
		}

		Matcher etimeMatch = p.matcher(etime);
		if(etimeMatch.find()) {
			String time = etimeMatch.group(4);

			displayTime += " - " + time;
		}

		return displayTime;
	}

	@Override
	public String toString() {
		return "Item{" +
				"roomid=" + roomid +
				", roomname='" + roomname + '\'' +
				", proomid=" + proomid +
				", stime='" + stime + '\'' +
				", etime='" + etime + '\'' +
				", approvaeflag=" + approvaeflag +
				", url='" + url + '\'' +
				", approvalid='" + approvalid + '\'' +
				", approvalname='" + approvalname + '\'' +
				", approvaldeptnm='" + approvaldeptnm + '\'' +
				", importance='" + importance + '\'' +
				'}';
	}
}
