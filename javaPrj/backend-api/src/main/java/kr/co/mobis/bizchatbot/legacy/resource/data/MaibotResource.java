package kr.co.mobis.bizchatbot.legacy.resource.data;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mobot"
})
@XmlRootElement(name = "MAIBOT")
public class MaibotResource implements Serializable{

	@XmlElement(name = "Item")
	private List<Item> mobot;

	public MaibotResource() {
	}

	public List<Item> getMobot() {
		if (mobot == null) {
			mobot = new ArrayList<>();
        }
		return mobot;
	}

	public void setMobot(List<Item> mobot) {
		this.mobot = mobot;
	}
}
