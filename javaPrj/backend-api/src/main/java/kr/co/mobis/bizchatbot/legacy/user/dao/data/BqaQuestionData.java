package kr.co.mobis.bizchatbot.legacy.user.dao.data;

import java.io.Serializable;

public class BqaQuestionData implements Serializable{

	protected int questionId;
	protected String question;
	protected int domainId;
	protected String useYn;
	protected String attribute1;
	protected String attribute2;
	protected String userId;
	protected String userNm;
	protected String deptCd;
	protected String deptNm;
	
	public BqaQuestionData() {
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public int getDomainId() {
		return domainId;
	}

	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserNm() {
		return userNm;
	}

	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}

	public String getAttribute2() {
		return attribute2;
	}

	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}

	public String getDeptCd() {
		return deptCd;
	}

	public void setDeptCd(String deptCd) {
		this.deptCd = deptCd;
	}

	public String getDeptNm() {
		return deptNm;
	}

	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}

	@Override
	public String toString() {
		return "BqaQuestionData [questionId=" + questionId + ", question=" + question + ", domainId=" + domainId
				+ ", useYn=" + useYn + ", attribute1=" + attribute1 + ", attribute2=" + attribute2 + ", userId="
				+ userId + ", userNm=" + userNm + ", deptCd=" + deptCd + ", deptNm=" + deptNm + "]";
	}
}
