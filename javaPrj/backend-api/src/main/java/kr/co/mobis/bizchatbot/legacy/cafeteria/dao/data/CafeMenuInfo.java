package kr.co.mobis.bizchatbot.legacy.cafeteria.dao.data;

import java.io.Serializable;

public class CafeMenuInfo implements Serializable{

	protected String location;
	//date YYYY-MM-DD
	protected String menuDate;
	protected String menuImage;
	
	public CafeMenuInfo() {
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMenuDate() {
		return menuDate;
	}

	public void setMenuDate(String menuDate) {
		this.menuDate = menuDate;
	}

	public String getMenuImage() {
		return menuImage;
	}

	public void setMenuImage(String menuImage) {
		this.menuImage = menuImage;
	}

	@Override
	public String toString() {
		return "CafeMenuInfo [location=" + location + ", menuDate=" + menuDate + ", menuImage=" + menuImage + "]";
	}
}
