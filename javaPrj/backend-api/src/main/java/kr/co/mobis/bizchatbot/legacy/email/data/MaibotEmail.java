package kr.co.mobis.bizchatbot.legacy.email.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "mobot"
})
@XmlRootElement(name = "MAIBOT")
public class MaibotEmail implements Serializable{

	@XmlElement(name = "Item")
	private List<Item> mobot;
	
	public MaibotEmail() {
	}

	public List<Item> getMobot() {
		if (mobot == null) {
			mobot = new ArrayList<Item>();
        }
		return mobot;
	}

	public void setMobot(List<Item> mobot) {
		this.mobot = mobot;
	}
	
	

}
