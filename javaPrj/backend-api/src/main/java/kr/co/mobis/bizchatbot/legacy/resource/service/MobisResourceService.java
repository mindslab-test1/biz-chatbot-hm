package kr.co.mobis.bizchatbot.legacy.resource.service;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotButton;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
public class MobisResourceService {

	private Logger logger = LoggerFactory.getLogger(MobisResourceService.class);

	public List<String> getResourceDateButtons() throws Exception{
		logger.info("getResourceDateButtons/");
		List<String> list = new ArrayList<>();

		try {
			list = this.getWeekDates();
			list.add("이전");
			list.add("대화종료");

		}catch(Exception e) {
			logger.warn("getResourceDateButtons/{}", e.getMessage());
			throw e;
		}
		return list;
	}

	public Map<String, String> getTimeParams(String date, String stime, String etime) throws Exception {
		logger.info("getTimeParams/{},{},{}", date, stime, etime);
		Map<String, String> respMap = new HashMap<>();

		try {
			respMap.put("status", "SUCCESS");

			String pdate = this.convertDate(date);
			String pstime = this.convertTime(stime);
			String petime = this.convertTime(etime);

			if(StringUtils.contains(pdate, "ERROR")) {
				logger.debug("getTimeParams ERROR/{}", pdate);
				String status = "유효하지 않은 날짜입니다.<br>일주일 이내의 날짜를 입력해 주세요.";
				if(StringUtils.contains(pdate, "WEEKEND")) {
					status = "주말과 휴일에는 회의실 조회가 불가능합니다.<br>일주일 이내의 유효한 날짜를 입력해 주세요 ";
				}
				respMap.put("status", status);
			} else if(StringUtils.contains(pstime, "ERROR")) {
				logger.debug("getTimeParams ERROR/{}", pstime);
				String status = "유효하지 않은 시작 시간입니다.<br>회의실 예약은 7시 ~ 18시 까지 30분 단위로 가능해요.";
				if(StringUtils.contains(pstime, "NOT 30 MOD")) {
					status = "시작 시간을 30분 단위로 입력해주세요.";
				}
				respMap.put("status", status);
			} else if(StringUtils.contains(petime, "ERROR")) {
				logger.debug("getTimeParams ERROR/{}", petime);
				String status = "유효하지 않은 종료 시간입니다.<br>회의실 예약은 7시 ~ 18시 까지 30분 단위로 가능해요.";
				if(StringUtils.contains(pstime, "NOT 30 MOD")) {
					status = "종료 시간을 30분 단위로 입력해주세요.";
				}
				respMap.put("status", status);
			} else {
				// 날짜와 시간 parsing 하여 입력
				respMap.put("pstime", pdate + pstime);
				respMap.put("petime", pdate + petime);
			}

		}catch(Exception e) {
			logger.warn("getTimeParams/{}", e.getMessage());
			throw e;
		}
		return respMap;
	}

	private List<String> getWeekDates() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("M월 d일");
		List<String> dates = new ArrayList<>();

		for(int i=0; i<8; i++) {
			Calendar day = Calendar.getInstance(Locale.KOREA);
			day.add(Calendar.DATE, i);

			int dayNum = day.get(Calendar.DAY_OF_WEEK);
			if(!this.isWeekend(dayNum)) {
				String str = dateFormat.format(day.getTime());
				dates.add(str);
			}
		}
		return dates;
	}

	private boolean isWeekend(int dayNum) {
		boolean isWeekend = false;

		switch(dayNum){
			case 1:
			case 7:
				isWeekend = true;
				break ;
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				isWeekend = false;
				break ;
		}
		return isWeekend;
	}

	private String convertDate(String date) {
		String resp = null;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Calendar today = Calendar.getInstance(Locale.KOREA);

		if(StringUtils.equals(date.trim(), "오늘")) {

			// 요일 체크 (주말인 경우 ERROR)
			resp = this.checkDayOfDate(today, dateFormat);

		} else if(StringUtils.equals(date.trim(), "내일")) {
			today.add(Calendar.DATE, 1); // 하루 더하기

			// 요일 체크 (주말인 경우 ERROR)
			resp = this.checkDayOfDate(today, dateFormat);

		} else { 			// "M월 d일"로 입력되었을시

			// 오늘 년도
			int year = today.get(Calendar.YEAR);

			// 7일후 날짜
			Calendar sevenDaysLater = Calendar.getInstance(Locale.KOREA);
			sevenDaysLater.add(Calendar.DATE,7);

			// 7일후 년도
			int sevenDaysLaterYear = sevenDaysLater.get(Calendar.YEAR);
			if(year + 1 == sevenDaysLaterYear) {
				// 7일후의 날짜가 다음년도 케이스
				// ex) 오늘: 12월 25일 -> 7일후: 1월 1일
				year = sevenDaysLaterYear;
			}

			Pattern p = Pattern.compile("(\\d*)월\\s*(\\d*)일");
			Matcher m = p.matcher(date);

			if(m.find()) {
				Calendar pDay = Calendar.getInstance(Locale.KOREA);

				int pMonth = Integer.parseInt(m.group(1));
				int pDayOfMonth = Integer.parseInt(m.group(2));

				// 입력날짜
				pDay.set(year, pMonth - 1, pDayOfMonth);

				// 입력날짜와 오늘 날짜 비교하기
				int compare = pDay.compareTo(today);
				if(compare < 0) { // 입력날짜가 오늘보다 이전일경우
					resp = "DATE ERROR";

				} else if(compare > 0) {
					int compare2 = pDay.compareTo(sevenDaysLater);
					if(compare2 > 0) { // 입력날짜가 7일후의 날짜보다 큰 경우
						resp = "DATE ERROR";
					}

					if(resp == null) {
						// 요일 체크 (주말인 경우 ERROR)
						resp = this.checkDayOfDate(pDay, dateFormat);
					}
				}
			} else {
				resp = "DATE ERROR";
			}
		}
		return resp;
	}

	private String convertTime(String time) {
		String resp = "";

		String[] strArr = time.trim().split("시");

		// 시간 처리 //-1로 되는지 확인하기
		int hour = Integer.parseInt(strArr[0].replaceAll("[^0-9]", ""));
		if(hour > 18) {
			// 시간 초과 exception 처리
			resp = "HOUR ERROR";
			return resp;
		}
		if(hour <= 6) {
			hour += 12;
		}
		resp += makeTwoDigitsNum(hour);

		// 분 처리
		if(strArr.length > 1) {
			if(strArr[1].contains("반")) {
				resp += "30";
			} else {
				int minute = Integer.parseInt(strArr[1].replaceAll("[^0-9]", ""));
				if(minute > 59) {
					// 분 초과 exception 처리
					resp = "MINUTES ERROR";
					return resp;
				}
				if(minute % 30 != 0) {
					// 30분 단위 아님
					resp = "NOT 30 MOD ERROR";
					return resp;
				}
				resp += makeTwoDigitsNum(minute);
			}
		} else {
			resp += "00";
		}

		return resp;
	}

	private String makeTwoDigitsNum(int num) {
		String str = "";
		if(num < 10) {
			str += "0" + num;
		} else {
			str += num;
		}
		return str;
	}

	private String checkDayOfDate(Calendar day, SimpleDateFormat dateFormat) {
		String resp = null;
		int dayNum = day.get(Calendar.DAY_OF_WEEK);
		if(this.isWeekend(dayNum)) {
			resp = "WEEKEND ERROR";
		}

		if(resp == null) {
			resp = dateFormat.format(day.getTime());
		}
		return resp;
	}
}
