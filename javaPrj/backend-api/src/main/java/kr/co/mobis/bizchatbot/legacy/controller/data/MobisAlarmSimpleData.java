package kr.co.mobis.bizchatbot.legacy.controller.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MobisAlarmSimpleData implements Serializable{

	protected int approvalCount;
	protected int unreadEmailCodunt;
	
	public MobisAlarmSimpleData() {
		
	}

	public int getApprovalCount() {
		return approvalCount;
	}

	public void setApprovalCount(int approvalCount) {
		this.approvalCount = approvalCount;
	}

	public int getUnreadEmailCodunt() {
		return unreadEmailCodunt;
	}

	public void setUnreadEmailCodunt(int unreadEmailCodunt) {
		this.unreadEmailCodunt = unreadEmailCodunt;
	}

	@Override
	public String toString() {
		return "MobisAlarmSimpleData [approvalCount=" + approvalCount + ", unreadEmailCodunt=" + unreadEmailCodunt
				+ "]";
	}
	
}
