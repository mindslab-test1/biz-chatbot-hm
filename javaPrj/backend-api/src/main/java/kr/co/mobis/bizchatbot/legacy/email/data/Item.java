package kr.co.mobis.bizchatbot.legacy.email.data;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "count",
    "subject",
    "receivedt",
    "url",
    "fromname"
})
public class Item implements Serializable{

	protected int count;
    @XmlElement(required = true)
    protected String subject;
    @XmlElement(required = true)
    protected String receivedt;
    @XmlElement(required = true)
    @XmlSchemaType(name = "anyURI")
    protected String url;
    @XmlElement(required = true)
    protected String fromname;
	
	public Item() {
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getReceivedt() {
		return receivedt;
	}

	public void setReceivedt(String receivedt) {
		this.receivedt = receivedt;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFromname() {
		return fromname;
	}

	public void setFromname(String fromname) {
		this.fromname = fromname;
	}
	
	public String getDisplayInfo() {
		return getSubject() + ", " + getFromname() + "," + getReceivedt() + "$CSS" +  getUrl() ;
	}

	@Override
	public String toString() {
		return "Item [count=" + count + ", subject=" + subject + ", receivedt=" + receivedt + ", url=" + url
				+ ", fromname=" + fromname + ", getCount()=" + getCount() + ", getSubject()=" + getSubject()
				+ ", getReceivedt()=" + getReceivedt() + ", getUrl()=" + getUrl() + ", getFromname()=" + getFromname()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
}
