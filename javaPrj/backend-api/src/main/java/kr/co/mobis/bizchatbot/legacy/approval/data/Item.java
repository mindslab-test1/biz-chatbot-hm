package kr.co.mobis.bizchatbot.legacy.approval.data;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "count",
    "url",
    "docid",
    "subject",
    "draftnm",
    "draftdt"
})
public class Item implements Serializable{

	protected int count;
	protected String url;
	protected String docid;
	protected String subject;
	protected String draftnm;
	protected String draftdt;
	
	public Item() {
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDocid() {
		return docid;
	}

	public void setDocid(String docid) {
		this.docid = docid;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDraftnm() {
		return draftnm;
	}

	public void setDraftnm(String draftnm) {
		this.draftnm = draftnm;
	}

	public String getDraftdt() {
		return draftdt;
	}

	public void setDraftdt(String draftdt) {
		this.draftdt = draftdt;
	}
	
	public String getDisplayInfo() {
		return getSubject() + ", " + getDraftnm() + "$CSS"+ getUrl() ;
	}

	@Override
	public String toString() {
		return "Item [count=" + count + ", url=" + url + ", docid=" + docid + ", subject=" + subject + ", draftnm="
				+ draftnm + ", draftdt=" + draftdt + "]";
	}

	
}
