package kr.co.mobis.bizchatbot.legacy.cafeteria.dao;

import java.util.HashMap;

import org.apache.ibatis.annotations.Mapper;

import kr.co.mobis.bizchatbot.legacy.cafeteria.dao.data.CafeMenuInfo;

@Mapper
@MobisCafeteria
public interface CafeMenuMapper {

	public CafeMenuInfo getCafeMenu(HashMap<String, String> params);
}
