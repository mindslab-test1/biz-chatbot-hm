package kr.co.mobis.bizchatbot.legacy.controller.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.NoticeInfo;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MobisSearchNotice implements Serializable{

	protected NoticeInfo notice;
	
	public MobisSearchNotice() {
	}

	public NoticeInfo getNotice() {
		return notice;
	}

	public void setNotice(NoticeInfo notice) {
		this.notice = notice;
	}

}
