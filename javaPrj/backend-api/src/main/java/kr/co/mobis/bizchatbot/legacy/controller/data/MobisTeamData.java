package kr.co.mobis.bizchatbot.legacy.controller.data;

import java.io.Serializable;

public class MobisTeamData implements Serializable {

    private String objId; /*mobis team ID*/
    private String sText; /* team name */
    private String kText; /* team full name*/
    private String parObjId; /* teamGroup ID */


    public String getObjId() {
        return objId;
    }

    public void setObjId(String objId) {
        this.objId = objId;
    }

    public String getsText() {
        return sText;
    }

    public void setsText(String sText) {
        this.sText = sText;
    }

    public String getkText() {
        return kText;
    }

    public void setkText(String kText) {
        this.kText = kText;
    }

    public String getParObjId() {
        return parObjId;
    }

    public void setParObjId(String parObjId) {
        this.parObjId = parObjId;
    }
}
