package kr.co.mobis.bizchatbot.legacy.anwserpref.service;

import kr.co.mobis.bizchatbot.IMobisConsts;
import kr.co.mobis.bizchatbot.legacy.anwserpref.dao.AnswerPrefDao;
import kr.co.mobis.bizchatbot.legacy.anwserpref.dao.data.ChatbotRespData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AnswerPrefService implements IMobisConsts {

    @Autowired
    AnswerPrefDao dao;

    public ChatbotRespData getAnswerPref(ChatbotRespData vo) throws  Exception {
        return dao.getAnswerPref(vo);
    }

    public int updateAnswerPrefHistory(ChatbotRespData vo) throws  Exception {
        return dao.updateAnswerPrefHistory(vo);
    }


    public int insertAnswerPrefHistory(ChatbotRespData vo)  throws  Exception {
        return dao.insertAnswerPrefHistory(vo);
    }
}
