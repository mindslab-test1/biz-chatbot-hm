package kr.co.mobis.bizchatbot.legacy.controller.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.mobis.bizchatbot.legacy.resource.data.Item;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MobisResourceList implements Serializable{

	protected int count;
	protected List<Item> resources;

	public MobisResourceList() {
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<Item> getResources() {
		if(resources == null) {
			resources = new ArrayList<>();
		}
		return resources;
	}

	public void setResources(List<Item> resources) {
		this.resources = resources;
	}

	@Override
	public String toString() {
		return "MobisResourceList [count=" + count + ", resources=" + resources + "]";
	}

}
