package kr.co.mobis.bizchatbot.legacy.bus.dao.data;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BusStop implements Serializable{

	protected String stopName;
	protected String stopTime;

	public BusStop() {
	}

	public String getStopName() {
		return stopName;
	}

	public void setStopName(String stopName) {
		this.stopName = stopName;
	}

	public String getStopTime() {
		return stopTime;
	}

	public void setStopTime(String stopTime) {
		this.stopTime = stopTime;
	}

	@Override
	public String toString() {
		return "BusStop{" +
				"stopName='" + stopName + '\'' +
				", stopTime='" + stopTime + '\'' +
				'}';
	}
}
