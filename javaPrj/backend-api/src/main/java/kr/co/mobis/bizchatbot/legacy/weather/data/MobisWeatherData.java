package kr.co.mobis.bizchatbot.legacy.weather.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MobisWeatherData implements Serializable{

	protected String weatherInfo;
	protected String message;
	protected List<String> list;
    protected String sysUtterTypeCode;

	public MobisWeatherData() {
	}

	public String getWeatherInfo() {
		return weatherInfo;
	}

	public void setWeatherInfo(String weatherInfo) {
		this.weatherInfo = weatherInfo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getList() {
		if(list == null) {
			list = new ArrayList<>();
		}
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

    public String getSysUtterTypeCode() {
        return sysUtterTypeCode;
    }

    public void setSysUtterTypeCode(String sysUtterTypeCode) {
        this.sysUtterTypeCode = sysUtterTypeCode;
    }

    @Override
    public String toString() {
        return "MobisWeatherData{" +
                "weatherInfo='" + weatherInfo + '\'' +
                ", message='" + message + '\'' +
                ", list=" + list +
                ", sysUtterTypeCode='" + sysUtterTypeCode + '\'' +
                '}';
    }
}
