package ai.mindslab.bizchatbot.backendapi.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

//@Component
public class HazelClient {

	@Value("${hazelcast.server.ip}")
	private String[] hazelIps;
	
	@Value("${hazelcast.group.name}")
	private String groupName;
	
	@Value("${hazelcast.group.password}")
	private String groupPassword;
	
	private HazelcastInstance client = null;
	
	public HazelClient() {
		
	}
	
	public HazelClient(String[] ips, String groupName, String password) {
		this.hazelIps = ips;
		this.groupName = groupName;
		this.groupPassword = password;
	}
	
	public HazelcastInstance getClient() {
		if(client == null) {
			ClientConfig clientConfig = new ClientConfig();
			clientConfig.getGroupConfig().setName(groupName).setPassword(groupPassword);
			clientConfig.getNetworkConfig().addAddress(hazelIps);
			
			client = HazelcastClient.newHazelcastClient(clientConfig);
		}
		return client;
	}
	
	public void release() {
		if(this.client != null) {
			this.client.shutdown();
		}
	}
	
	public <K,V> IMap<K, V> getMap(String name){
		
		return this.getClient().getMap(name);
	}
}
