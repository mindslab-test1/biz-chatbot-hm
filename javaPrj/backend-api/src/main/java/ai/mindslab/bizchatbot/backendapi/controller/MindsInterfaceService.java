package ai.mindslab.bizchatbot.backendapi.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.bizchatbot.backendapi.controller.data.IntentFinderResponse;
import ai.mindslab.bizchatbot.backendapi.service.SimpleClassifierService;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.session.data.SdsUtterRespData;


@RestController
@RequestMapping(path = "/admin/api/sc")
public class MindsInterfaceService implements IChatbotConst{

	private Logger logger = LoggerFactory.getLogger(MindsInterfaceService.class);
	
	@Autowired
	@Qualifier("hazelClientCommon")
	private HazelClientCommon client;
	
	@Autowired
	private SimpleClassifierService scSerivce;
	
	@RequestMapping(value="/query", method = {RequestMethod.POST})
	public BaseResponse<?> getScDomain(@RequestParam(value="botId", required=true)int botId
			, @RequestParam(value="utter") String utter, HttpServletRequest req) throws Exception{
		logger.debug("/admin/api/sc/query/{},{}",botId,utter);
		
		BaseResponse<IntentFinderResponse<String>> resp = new BaseResponse<>();
		IntentFinderResponse<String> result = null;
		
		try {
			result = scSerivce.doSimple(botId, utter);
			result.setBotId(botId);
			result.setUserUtter(utter);
			resp.setData(result);
		}catch(Exception e) {
			resp.setCode(IRestCodes.ERR_CODE_FAILURE);
			resp.setMsg(e.getMessage());
		}
		return resp;
	}
	
}
