package ai.mindslab.bizchatbot.backendapi.controller;

import ai.mindslab.bizchatbot.backendapi.service.ThirdpartyProxyService;
import ai.mindslab.bizchatbot.backendapi.service.data.WeatherCity;
import ai.mindslab.bizchatbot.commons.data.BaseListObject;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/3rd")
public class ThirdPartyProxyController {
	
	@Autowired
	private ThirdpartyProxyService svc;

    private Logger logger = LoggerFactory.getLogger(ThirdPartyProxyController.class);
	
	/**
	 * 3rd party api를 아래와 같이 integration하는데 사용가능.
	 * @param city
	 * @param country
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/weather" ,method = {RequestMethod.POST,RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getWeather(@RequestParam("city") String city, @RequestParam("country")String country) throws Exception{
		String resp = "";
		try {
			List<WeatherCity> list = svc.getCities();

            logger.info("[weather] city : " + city +" country :" + country);

//			for(WeatherCity c : list){
//				if(country.toLowerCase().equals("kr")) {
//					if(city.indexOf(c.getName()) > -1) {
//						city = c.getName();
//                        logger.info("[weather commmited] city : " + city +" country :" + country);
//						break;
//					}
//				}else if(country.toLowerCase().equals("en")) {
//                    if(city.indexOf(c.getName()) > -1) {
//						city = c.getEng();
//                        logger.info("[weather commmited] city : " + city +" country :" + country);
//						break;
//					}
//				}
//			}

			resp =  svc.getWeather(city, country, "kr");
		}catch(Exception e) {
//			resp = "{\"code\":1, \"msg\":\""+e.getMessage()+"\"}";
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("code", 1);
			jsonObj.put("msg", e.getMessage());
			resp = jsonObj.toString();
		}
		return resp;
	}

	
	@RequestMapping(value="/weather_city" ,method = {RequestMethod.POST,RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getCities() throws Exception{
		BaseResponse<BaseListObject<WeatherCity>> resp = new BaseResponse<>();
		
		List<WeatherCity> list = svc.getCities();
		BaseListObject<WeatherCity> data = new BaseListObject<>();
		data.setList(list);
		data.setTotalCount(list.size());
		resp.setData(data);
		
		return resp;
	}


	/**
	 * 
	 * @param city
	 * @param country
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/weather_en" ,method = {RequestMethod.POST,RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_VALUE)
	public String getWeatherEn(@RequestParam("city") String city, @RequestParam("country")String country) throws Exception{
        String resp = "";
        try {
            resp = svc.getWeather(city, country, "en");
        }catch(Exception e){
            resp = "{\"code\":1, \"msg\":\""+e.getMessage()+"\"}";
        }
        return resp;
	}

	
	@RequestMapping(value="/weather_city_en" ,method = {RequestMethod.POST,RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_VALUE)
	public BaseResponse<?> getCitiesEn() throws Exception{
		BaseResponse<BaseListObject<WeatherCity>> resp = new BaseResponse<>();
		
		List<WeatherCity> list = svc.getCitiesEn();
		BaseListObject<WeatherCity> data = new BaseListObject<>();
		data.setList(list);
		data.setTotalCount(list.size());
		resp.setData(data);
		
		return resp;
	}
}
