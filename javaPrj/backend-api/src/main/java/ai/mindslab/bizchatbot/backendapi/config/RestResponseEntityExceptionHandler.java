package ai.mindslab.bizchatbot.backendapi.config;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;


@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler implements IRestCodes {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		// TODO Auto-generated method stub
//		return super.handleMethodArgumentNotValid(ex, headers, status, request);
		
		BaseResponse<String> err = new BaseResponse<>(ERR_CODE_PARAMS_INVALID, ERR_MSG_PARAMS_INVALID);
		return new ResponseEntity(err, headers, status) ;
	}

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		// TODO Auto-generated method stub
//		return super.handleMissingServletRequestParameter(ex, headers, status, request);
		
		BaseResponse<String> err = new BaseResponse<>(ERR_CODE_PARAMS_INVALID, ERR_MSG_PARAMS_INVALID);
		return new ResponseEntity(err, headers, status) ;
	}

	//Sample
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		// TODO Auto-generated method stub
//		return super.handleExceptionInternal(ex, body, headers, status, request);
		
		BaseResponse<String> resp = null;
		if(ex instanceof BizChatbotException) {
			resp = new BaseResponse<>(ERR_CODE_FAILURE, ERR_MSG_FAILURE);//test
		}else {
			resp = new BaseResponse<>(ERR_CODE_FAILURE, ERR_MSG_FAILURE);
		}
		
		return new ResponseEntity<Object>(resp, headers, status);
	}
	
}
