package ai.mindslab.bizchatbot.backendapi.controller;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.bizchatbot.backendapi.controller.data.IntentFinderResponse;
import ai.mindslab.bizchatbot.backendapi.service.SdsIntentFinderService;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.IntentFinderDomainRel;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.codes.ISDSCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.data.ReqDialog;
import ai.mindslab.bizchatbot.commons.data.ReqSdsIntentFinder;
import ai.mindslab.bizchatbot.commons.data.RespSdsIntentFinder;
import ai.mindslab.bizchatbot.commons.data.SdsPortDomainPair;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.SdsData;
import ai.mindslab.bizchatbot.commons.session.data.SdsUtterRespData;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalIntentInfo;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfSdsData;
import ai.mindslab.bizchatbot.sds.client.SdsClient;
import ai.mindslab.bizchatbot.sds.client.SdsClientFactory;
import jhcho.chatbot.processpool.SdsIntentFinder;

@RestController
@RequestMapping(path = "/admin/api/itf")
public class SdsIntentFinderController {
	
	private Logger logger = LoggerFactory.getLogger(SdsIntentFinderController.class);
	
	@Autowired
	@Qualifier("hazelClientCommon")
	private HazelClientCommon hazel;
	
	@Autowired
	private SdsIntentFinderService itfService;
	
//	@Autowired
//	private SdsClient sdsClient;
	@Autowired
	private SdsClientFactory sdsFactory;
	
	@Value("${sds.client.type:base}")
	private String sdsClientType;
	
	public SdsIntentFinderController() {
		
	}

	@RequestMapping(value="/query",method = {RequestMethod.POST})
	public BaseResponse<?> querySdsSkill(@RequestBody UtterData utter, HttpServletRequest req) throws Exception{
		logger.info("/itf/query/{}",utter);
		
		BaseResponse<String> resp = null;
		
//		DialogSession ses = itfService.getDialogSession(utter.getSessionId());
		if(!itfService.hasActiveSdsIntentFinder(utter.getBotId())) {
			itfService.initSdsIntentFinder(utter.getBotId());
		}
		
		try {
			String intentName = "";
			String sdsDomain = "";
			ArrayList<SdsData> itfData = itfService.getSdsItfConfig(utter.getBotId());
			if(itfData != null) {
				for(SdsData d: itfData) {
					SdsUtterRespData sdsResp = null;
					String debug = "";
					if(IChatbotConst.CLIENT_TYPE_BASE.equals(sdsClientType)) {
						//Sds query as a intent finder
						SdsClient sdsClient = sdsFactory.makeSdsClient();
						sdsClient.setSdsData(d);
						sdsResp = sdsClient.userUtter(utter.getSentence());
						if(sdsResp != null) {
							//QQQ use SLU part for parsing
		//					debug = resp.getDebug();
							debug = sdsResp.getSlu();
							logger.debug("querySdsSkill/{}",debug);
							intentName = itfService.extractIntentName(debug);
							IntentFinderDomainRel sdsDomainName = itfService.getIntentFinderInfo(utter.getBotId(), intentName);
							if(sdsDomainName != null) {
								if(!StringUtils.isEmpty(sdsDomainName.getSdsDomain())) {
									sdsDomain = sdsDomainName.getSdsDomain();
									break;
								}
							}else {
								//QQQ show unknown message
								sdsDomain = null;
							}
						}
					}else if(IChatbotConst.CLIENT_TYPE_NG.equals(sdsClientType)) {
						debug = itfService.ngQuery(d.getProjectName(), utter.getSentence());
						logger.debug("querySdsSkill/{}",debug);
						intentName = itfService.extractIntentName(debug);
						IntentFinderDomainRel sdsDomainName = itfService.getIntentFinderInfo(utter.getBotId(), intentName);
						if(sdsDomainName != null) {
							if(!StringUtils.isEmpty(sdsDomainName.getSdsDomain())) {
								sdsDomain = sdsDomainName.getSdsDomain();
								break;
							}
						}else {
							//QQQ show unknown message
							sdsDomain = null;
						}
					}
				}
			}
			
			resp = new BaseResponse<>();
			resp.setData(sdsDomain);
		}catch(ConnectException e) {
			logger.warn("/itf/query/{}", e.getMessage());
			itfService.resetBotSdsItfMap(utter.getBotId());
			resp = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}catch(Exception e) {
			logger.warn("/itf/query/{}", e.getMessage());
			resp = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}
		
		return resp;
	}
	
	@RequestMapping(value="/open",method = {RequestMethod.POST})
	public BaseResponse<?> openSdsSkill(@RequestBody ReqSdsIntentFinder itfReq, HttpServletRequest req) throws Exception{
		logger.info("/itf/open/{}",itfReq.toString());
		
		BaseResponse<RespSdsIntentFinder> resp = null;
		if(itfReq.getBotList().size() > 0) {
			resp = new BaseResponse<>();
			
			RespSdsIntentFinder r = new RespSdsIntentFinder();
			for(Integer botId: itfReq.getBotList()) {
				SdsPortDomainPair p = new SdsPortDomainPair();
				if(!itfService.hasActiveSdsIntentFinder(botId)) {
					
					p.setBotId(botId);
					try {
						ArrayList<SdsData> l = itfService.initSdsIntentFinder(botId);
						if(l!=null && l.size() > 0) {
							SdsData sData = l.get(0);
							p.setDomain(sData.getProjectName());
							p.setPort(sData.getProjectPort());
						}else {
							throw new Exception("Sds init. failure in SDS Intent Finder");
						}
						r.getSuccessList().add(p);
					}catch(Exception e) {
						p.setErrMsg(e.getMessage());
						r.getFailureList().add(p);
					}
				}else {
					p.setBotId(botId);
					p.setErrMsg("Already Opened");
					List<String> domains = itfService.getIntentFinderDomain(botId);
					if(domains != null && domains.size() >0) {
						p.setDomain(domains.get(0));
					}
					logger.info("itf/open/Intent finder for {} is already opend", botId);
				}
			}
			
		}else {
			resp = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, "Target Bot List is Empty.");
		}
		
		return resp;
	}
	
	@RequestMapping(value="/close",method = {RequestMethod.POST})
	public BaseResponse<?> closeItf(@RequestParam(value="botId", required=true) int botId, HttpServletRequest req) throws Exception{
		logger.info("/itf/close/{}",botId);
		
		BaseResponse<String> resp = null;
		try {
			itfService.closeSdsIntentFinder(botId);
			resp = new BaseResponse<>();
		}catch(Exception e) {
			resp = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, e.getMessage());
		}
		
		return resp;
	}
	
	@RequestMapping(value="/adminquery",method = {RequestMethod.POST})
	public BaseResponse<?> adminQuery(@RequestParam(value="botId", required=true) int botId, @RequestParam(value="utter", required=true)String utter
			, HttpServletRequest req) throws Exception{
		
		logger.info("/admin/api/itf/adminquery/{},{},{}",botId, utter, sdsClientType);
		BaseResponse<IntentFinderResponse<SdsUtterRespData>> resp = null;
		
		IntentFinderResponse<SdsUtterRespData> result = new IntentFinderResponse<>();
		result.setBotId(botId);
		result.setUserUtter(utter);
		
		if(!itfService.hasActiveSdsIntentFinder(botId)) {
			itfService.initSdsIntentFinder(botId);
		}
		
		try {
			String intentName = "";
			String sdsDomain = "";
			ArrayList<SdsData> itfData = itfService.getSdsItfConfig(botId);
			SdsUtterRespData sdsResp = null;
			String debug = "";
			if(itfData != null) {
				for(SdsData d: itfData) {
					if(IChatbotConst.CLIENT_TYPE_BASE.equals(sdsClientType)) {
						SdsClient sdsClient = sdsFactory.makeSdsClient();
						sdsClient.setSdsData(d);
						sdsResp = sdsClient.userUtter(utter);
						
						if(sdsResp != null) {
							result.setDebug(sdsResp);
							//QQQ use SLU part for parsing
		//					debug = resp.getDebug();
							debug = sdsResp.getSlu();
							logger.debug("/admin/api/itf/adminquery/{}",debug);
							intentName = itfService.extractIntentName(debug);
						}
					}else if(IChatbotConst.CLIENT_TYPE_NG.equals(sdsClientType)) {
						debug = itfService.ngQuery(d.getProjectName(), utter);
						logger.debug("querySdsSkill/{}",debug);
						intentName = itfService.extractIntentName(debug);
						
					}
					
					IntentFinderDomainRel sdsDomainName = itfService.getIntentFinderInfo(botId, intentName);
					if(sdsDomainName != null) {
						if(!StringUtils.isEmpty(sdsDomainName.getSdsDomain())) {
							sdsDomain = sdsDomainName.getSdsDomain();
							break;
						}
					}else {
						//QQQ show unknown message
						sdsDomain = null;
					}
				}
			}
			result.setDomain(sdsDomain);
			
			resp = new BaseResponse<>();
			resp.setData(result);
		}catch(ConnectException e) {
			logger.warn("/admin/api/itf/adminquery/{}", e.getMessage());
			itfService.resetBotSdsItfMap(botId);
			resp = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}catch(Exception e) {
			logger.warn("/admin/api/itf/adminquery/{}", e.getMessage());
			resp = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}
		
		return resp;
	}
	
	@RequestMapping(value="/globalquery",method = {RequestMethod.POST})
	public BaseResponse<?> querySdsSkillGlobal(@RequestBody UtterData utter, HttpServletRequest req) throws Exception{
		logger.info("/itf/globalquery/{}",utter);
		
		BaseResponse<GlobalIntentInfo> resp = null;
		
		try {
			if(!itfService.hasActiveGlobalSdsIntentFinder(utter.getBotId())) {
				itfService.initGlobalSdsIntentFinder(utter.getBotId());
			}
			
			GlobalIntentInfo intentInfo = null;
			ArrayList<SdsData> itfData = itfService.getGlobalSdsItfConfig(utter.getBotId());
			if(itfData != null) {
				for(SdsData d: itfData) {
					SdsUtterRespData sdsResp = null;
					String debug = "";
					if(IChatbotConst.CLIENT_TYPE_BASE.equals(sdsClientType)) {
						//Sds query as a intent finder
						SdsClient sdsClient = sdsFactory.makeSdsClient();
						sdsClient.setSdsData(d);
						sdsResp = sdsClient.userUtter(utter.getSentence());
						if(sdsResp != null) {
							//QQQ use SLU part for parsing
		//					debug = resp.getDebug();
							debug = sdsResp.getSlu();
							logger.debug("querySdsSkillGlobal/{}",debug);
							intentInfo = itfService.extractIntentNameAndSlot(debug);
						}
					}else if(IChatbotConst.CLIENT_TYPE_NG.equals(sdsClientType)) {
						debug = itfService.ngQuery(d.getProjectName(), utter.getSentence());
						logger.debug("querySdsSkillGlobal/{}",debug);
						intentInfo = itfService.extractIntentNameAndSlot(debug);
					}
				}
			}
			if(intentInfo == null) {
				resp = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, "Can't find intent for user utter.");
				return resp;
			}
			resp = new BaseResponse<>();
			GlobalItfSdsData sdsData = itfService.getSvcTypeByInent(utter.getBotId(), intentInfo.getIntentName());
			if(sdsData != null) {
				intentInfo.setSvcTypeCd(sdsData.getSvcTypeCd());
				intentInfo.setIfId(sdsData.getIfId());
			}
			resp.setData(intentInfo);
		}catch(ConnectException e) {
			logger.warn("/itf/globalquery/{}", e.getMessage());
			itfService.resetBotSdsItfMap(utter.getBotId());
			resp = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}catch(Exception e) {
			logger.warn("/itf/globalquery/{}", e.getMessage());
			resp = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}
		
		return resp;
	}
}
