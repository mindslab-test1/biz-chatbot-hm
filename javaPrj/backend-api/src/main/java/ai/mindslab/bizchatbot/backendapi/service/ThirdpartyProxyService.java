package ai.mindslab.bizchatbot.backendapi.service;

import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.PostConstruct;

import com.jayway.jsonpath.JsonPath;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.backendapi.service.data.WeatherCity;
import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;

@Service
public class ThirdpartyProxyService {

	public static String weather_url = "http://api.openweathermap.org/data/2.5/weather?APPID=4923c7a1ae926a29c3f1bef371a0ceb7&q=_city_,_country_&lang=_lang_";
	private Logger logger = LoggerFactory.getLogger(ThirdpartyProxyService.class);
	
	private ArrayList<WeatherCity> list;
	ArrayList<WeatherCity> list_en;
	
	@PostConstruct
	public void init() {
		getCities();
		getCitiesEn();
	}
	
	public String getWeather(String city, String country, String lang) throws Exception{
		logger.info("getWeather params/{},{},{}", city, country, lang);
		String resp = "";

		String eCity = this.translateCityName(city);	// API 도시에 맞게끔 변환

		if(StringUtils.isEmpty(eCity)) {
			throw new Exception("날씨 서비스를 지원하지 않는 도시입니다.");
		}
		if(StringUtils.isEmpty(country)) {
			country = "KR";
		}
		if(StringUtils.isEmpty(lang)) {
			lang = "kr";
		}
		String url = weather_url.replace("_city_", eCity);
		url = url.replace("_country_", country);
		url = url.replace("_lang_", lang);
		Header [] headers = null;
		
		try {
			resp = HttpConnectionManager.getInstance().get(url, headers);
			logger.debug("getWeather/{},{}/{}",eCity,country,resp);

			String condition = JsonPath.read(resp, "$.weather[0].description"); // Fixed value

			JSONObject jsonObj = new JSONObject(resp);
			jsonObj.put("code", 0);
			jsonObj.put("msg", city + " 날씨는 " + condition + " 입니다.");
			resp = jsonObj.toString();
		}catch(Exception e) {
			logger.warn("getWeather/{},{}/{}", eCity, country, e.getMessage());
			//resp = "정보조회 오류입니다.";
			throw new Exception("정보조회 오류입니다.");
		}
		return resp;
	}
	
	public ArrayList<WeatherCity> getCities(){
		
		if(list == null) {
			list = new ArrayList<>();

			list.add(new WeatherCity("서울","Seoul"));
			list.add(new WeatherCity("부산","Busan"));
			list.add(new WeatherCity("인천","Incheon"));
			list.add(new WeatherCity("대구","Daegu"));
			list.add(new WeatherCity("대전","Daejeon"));
			list.add(new WeatherCity("성남","Seongnam"));
			list.add(new WeatherCity("의왕","Anyang"));
			list.add(new WeatherCity("용인","Yongin"));
			list.add(new WeatherCity("마북","Yongin"));
			list.add(new WeatherCity("울산","Ulsan"));
			list.add(new WeatherCity("아산","Asan"));
			list.add(new WeatherCity("광주","Gwangju"));
			list.add(new WeatherCity("서산","Suisan"));
			list.add(new WeatherCity("안양","Anyang"));
			list.add(new WeatherCity("김천","Gimcheon"));
			list.add(new WeatherCity("진천","Chinchon"));
			list.add(new WeatherCity("포승","Pyongtak"));
			list.add(new WeatherCity("창원","Changwon"));
			list.add(new WeatherCity("천안","Tenan"));
			list.add(new WeatherCity("충주","Tsch-hung"));
			list.add(new WeatherCity("강릉","Kang-neung"));
			list.add(new WeatherCity("원주","Wonju"));
			list.add(new WeatherCity("김해","Kimhae"));
			list.add(new WeatherCity("경산","Keizan"));
			list.add(new WeatherCity("화성","Hwaseong"));
			list.add(new WeatherCity("영암","Reigan"));
			list.add(new WeatherCity("양산","Ungsang"));
			list.add(new WeatherCity("포천","Hwangmae"));
			list.add(new WeatherCity("파주","Munemi"));
			list.add(new WeatherCity("순천","Sunchun"));
			list.add(new WeatherCity("안동","Andong"));
			list.add(new WeatherCity("장성","Gwangju"));
			list.add(new WeatherCity("김제","Kimje"));
			list.add(new WeatherCity("제주","Jeju"));
			list.add(new WeatherCity("사천","Chinju"));
			list.add(new WeatherCity("춘천","Chuncheon"));
			list.add(new WeatherCity("오창","Cheongju"));
			list.add(new WeatherCity("경주","Kyonju"));
			list.add(new WeatherCity("수원","Suigen "));
			list.add(new WeatherCity("안성","Anseong "));
			list.add(new WeatherCity("평택","Pyongtak"));
			list.add(new WeatherCity("홍성","Hongsung"));
			list.add(new WeatherCity("예산","Yesan"));
			list.add(new WeatherCity("청주","Cheongju"));
			list.add(new WeatherCity("공주","Kongju"));
			list.add(new WeatherCity("논산","Nonsan"));
			list.add(new WeatherCity("전주","Jeonju"));
			list.add(new WeatherCity("군산","Kunsan"));
			list.add(new WeatherCity("무안","Puan"));
			list.add(new WeatherCity("무주","Muju"));
			list.add(new WeatherCity("남해","Namhae"));
			list.add(new WeatherCity("진주","Chinju"));
			list.add(new WeatherCity("밀양","Miryang"));
			list.add(new WeatherCity("양양","Yangyang"));
			list.add(new WeatherCity("속초","Sogcho"));
			list.add(new WeatherCity("가평","Gapyeong"));
			list.add(new WeatherCity("오산","Osan"));
			list.add(new WeatherCity("여주","Yeoju"));
		}
		
		return list;
	}
	
	public ArrayList<WeatherCity> getCitiesEn(){
		
		if(list_en == null) {
			list_en = new ArrayList<>();
			list_en.add(new WeatherCity("Seoul", "Seoul"));
			list_en.add(new WeatherCity("Busan", "Busan"));
			list_en.add(new WeatherCity("Incheon", "Incheon"));
			list_en.add(new WeatherCity("Daegu", "Daegu"));
			list_en.add(new WeatherCity("Daejeon", "Daejeon"));
			list_en.add(new WeatherCity("Seognam", "Seognam"));
			list_en.add(new WeatherCity("Uiwang", "Uiwang"));
			list_en.add(new WeatherCity("Yongin", "Yongin"));
			list_en.add(new WeatherCity("Yongin", "Yongin"));
			list_en.add(new WeatherCity("Ulsan", "Ulsan"));
			list_en.add(new WeatherCity("Asan", "Asan"));
			list_en.add(new WeatherCity("Gwangju", "Gwangju"));
			list_en.add(new WeatherCity("Seosan", "Seosan"));
			list_en.add(new WeatherCity("Anyang", "Anyang"));
			list_en.add(new WeatherCity("Gimcheon", "Gimcheon"));
			list_en.add(new WeatherCity("Jincheon", "Jincheon"));
			list_en.add(new WeatherCity("Poseung", "Poseung"));
			list_en.add(new WeatherCity("Changwon", "Changwon"));
			list_en.add(new WeatherCity("Cheonan", "Cheonan"));
			list_en.add(new WeatherCity("Chungju", "Chungju"));
			list_en.add(new WeatherCity("Gangneung", "Gangneung"));
			list_en.add(new WeatherCity("Wonju", "Wonju"));
			list_en.add(new WeatherCity("Gimhae", "Gimhae"));
			list_en.add(new WeatherCity("Gyeongsan", "Gyeongsan"));
			list_en.add(new WeatherCity("Hwaseong", "Hwaseong"));
			list_en.add(new WeatherCity("Yeongam", "Yeongam"));
			list_en.add(new WeatherCity("Yangsan", "Yangsan"));
			list_en.add(new WeatherCity("Pocheon", "Pocheon"));
			list_en.add(new WeatherCity("Paju", "Paju"));
			list_en.add(new WeatherCity("Suncheon", "Suncheon"));
			list_en.add(new WeatherCity("Andong", "Andong"));
			list_en.add(new WeatherCity("Jangsung", "Jangsung"));
			list_en.add(new WeatherCity("Kimje", "Kimje"));
			list_en.add(new WeatherCity("Jeju", "Jeju"));
			list_en.add(new WeatherCity("Sacheon", "Sacheon"));
			list_en.add(new WeatherCity("Chuncheon", "Chuncheon"));
			list_en.add(new WeatherCity("Ochang", "Ochang"));
			list_en.add(new WeatherCity("Gyeongju", "Gyeongju"));
		}
		return list_en;
	}

	private static HashMap<String,String> cities = new HashMap<>();

	private String translateCityName(String krName) {

		if(cities.size() == 0) {
			for(WeatherCity city: this.getCities()) {
				cities.put(city.getName(), city.getEng());
			}
		}
		String name = cities.get(krName.trim()) ;

		if(name != null) {
			return name;
		}else {
			return null;
		}
	}
}
