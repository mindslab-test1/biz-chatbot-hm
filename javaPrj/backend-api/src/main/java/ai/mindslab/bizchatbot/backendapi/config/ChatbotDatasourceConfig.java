package ai.mindslab.bizchatbot.backendapi.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@MapperScan(basePackages= {"ai.mindslab.bizchatbot", "kr.co.mobis.bizchatbot.legacy.user", "kr.co.mobis.bizchatbot.legacy.location","kr.co.mobis.bizchatbot.legacy.anwserpref", "kr.co.mobis.bizchatbot.legacy.bus", "kr.co.mobis.bizchatbot.legacy.rpa"}, sqlSessionFactoryRef = AppConfig.TRANSACTION_SESSION_FACTORY_MAIN)
public class ChatbotDatasourceConfig {

	@Autowired
	private MybatisConfigurationSupport mybatisConfigurationSupport;

	@Bean(name=AppConfig.DATASOURCE_MAIN, destroyMethod="close")
	@Primary
	@ConfigurationProperties(prefix="spring.datasource")
	public DataSource dataSource() {
		DataSource ds = DataSourceBuilder.create().build();
		return ds;
	}

	@Bean(name=AppConfig.TRANSACTION_MANAGER_MAIN)
	@Primary
	public PlatformTransactionManager chatbotTransactionManager() {
		return new DataSourceTransactionManager(dataSource());
	}

	@Bean(name=AppConfig.TRANSACTION_SESSION_FACTORY_MAIN)
	@Primary
	public SqlSessionFactory sqlSessionFactory(@Qualifier(AppConfig.DATASOURCE_MAIN) DataSource dataSource) throws Exception{
		return mybatisConfigurationSupport.build(dataSource);
	}

}
