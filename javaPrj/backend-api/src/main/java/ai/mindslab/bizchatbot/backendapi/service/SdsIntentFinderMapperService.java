package ai.mindslab.bizchatbot.backendapi.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



import ai.mindslab.bizchatbot.backendapi.config.AppConfig;
import ai.mindslab.sds.dao.IntentFinderMapper;
import ai.mindslab.sds.dao.data.SdsAgentData;
import ai.mindslab.sds.dao.data.SdsIntentData;
import ai.mindslab.sds.dao.data.SdsIntentUserSay;
import ai.mindslab.sds.dao.data.SdsProjectData;
import ai.mindslab.sds.dao.data.SdsUserData;

@Service
@Qualifier("SdsAdmin")
public class SdsIntentFinderMapperService {

	private Logger logger = LoggerFactory.getLogger(SdsIntentFinderMapperService.class);
	
	@Autowired
	private IntentFinderMapper mapper;
	
	@Value("${sds.engine.path}")
	private String enginePath;
	
	/**
	 * 
	 * @param userId
	 * @param domainName
	 * @return project seq no.
	 * @throws Exception
	 */
	public long prepareItfDomain(String userId, String domainName) throws Exception{
		logger.info("prepareItfDomain/{},{}",userId,domainName);
		SdsUserData user = mapper.getUser(userId);
		
		SdsProjectData project = new SdsProjectData();
		project.setProjectName(domainName);
		project.setDescription("");
		project.setUserSeqNo(user.getSeqNo());
		long nRet = mapper.insertProject(project);
		logger.info("project inserted count:{}", nRet);
		logger.debug("inserted Project:{}", project);
		
		SdsAgentData agent = new SdsAgentData();
		agent.setAgentName("END");
		agent.setLeftPos(0);
		agent.setTopPos(0);
		agent.setResetSlot(0);
		agent.setProjectSeq(project.getSeqNo());
		agent.setTaskType("essential");
		agent.setRelatedSlots("");
		nRet = mapper.insertAgent(agent);
		logger.info("agent inserted count:{}", nRet);
		logger.debug("inserted agent:{}", agent);
		
		agent = new SdsAgentData();
		agent.setAgentName("INIT_"+domainName);
		agent.setLeftPos(0);
		agent.setTopPos(0);
		agent.setResetSlot(0);
		agent.setProjectSeq(project.getSeqNo());
		agent.setTaskType("essential");
		agent.setRelatedSlots("");
		nRet = mapper.insertAgent(agent);
		logger.info("agent inserted count:{}", nRet);
		logger.debug("inserted agent:{}", agent);
		
		//return project seq
		return project.getSeqNo();
		
	}
	
	public void insertIntentUserSay(int projectSeq, String intentName, ArrayList<String> userSays) throws Exception{
		logger.info("createIntentUserSay/{},user say count:{}, projectSeq:{}",intentName,userSays==null?0:userSays.size(),projectSeq);
		
		long intentSeq = 0;
		
		Map<String, Object> map = new HashMap<>();
		map.put("intentName", intentName);
		map.put("projectSeq", projectSeq);
		
		
		SdsIntentData intent = mapper.getIntent(map);
		if(intent != null) {
			intentSeq = intent.getSeqNo();
			logger.info("intent alreay exist. intent seq: {}", intentSeq);
		}else {
			intent = new SdsIntentData();
			intent.setProjectSeq(projectSeq);
			intent.setIntentName(intentName);
			intent.setAgentSeq(0);
			
			long nRet = mapper.insertIntent(intent);
			logger.info("inserted intent count: {}", nRet);
			logger.debug("inserted intent: {}", intent);
			intentSeq = intent.getSeqNo();
		}
		
		int nRet = 0;
		if(userSays != null) {
			SdsIntentUserSay userSay = null;
			for(String say: userSays) {
				userSay = new SdsIntentUserSay();
				userSay.setSay(say);
				userSay.setIntentSeq((int)intentSeq);
				userSay.setSlotSeqNo("");
				userSay.setSayTag(say);
				userSay.setSlotTag(say);
				userSay.setSlotName("");
				userSay.setSlotMap(say);
				userSay.setSlotMapda("");
				nRet += mapper.insertIntentUsersay(userSay);
			}
			logger.info("inserted intent say count: {}", nRet);
		}
	}
 
	@Transactional(transactionManager=AppConfig.TRANSACTION_MANAGER_SDS, rollbackFor={Exception.class})
	public void createItfDomain(String userId, String domainName) throws Exception{
		
		prepareItfDomain(userId, domainName);
		makeProjectFiles(domainName);
	}
	
	@Transactional(transactionManager=AppConfig.TRANSACTION_MANAGER_SDS, rollbackFor={Exception.class})
	public void insertItfCorpus(String userId, String domainName,  String intentName, ArrayList<String> userSays) throws Exception{
		logger.info("insertItfCorpus/{}, {},{},user say count:{}", userId, domainName, intentName, userSays==null?0:userSays.size());
		
		SdsProjectData project = mapper.getProject(domainName);
		
		int projectSeq = 0;
		if(project == null) {
			projectSeq = (int)prepareItfDomain(userId, domainName);
			makeProjectFiles(domainName);
		}else {
			projectSeq = project.getSeqNo();
		}
		
		insertIntentUserSay(projectSeq,intentName, userSays);
	}
	
	@Transactional(transactionManager=AppConfig.TRANSACTION_MANAGER_SDS, rollbackFor= {Exception.class})
	public SdsProjectData getProject(String projectName) throws Exception{
		SdsProjectData data = null;
		data = mapper.getProject(projectName);
		return data;
	}
	
	public void makeProjectFiles(String projectName) {
		logger.info("makeProjectFiles/{}", projectName);
		String emptyPath = "";
		String copyPath = "";
		
		emptyPath = enginePath + "/dialog_domain/empty";
		copyPath = enginePath + "/dialog_domain/" + projectName;
		
		File s = new File(emptyPath);
		File t = new File(copyPath);
		if(!t.exists()) {
			t.mkdir();
		}
		
		Directorycopy(s, t);
		
		File oldsql = new File(copyPath + "/empty_DB.sqlite3");
		File renamesql = new File(copyPath + "/" + projectName + "_DB.sqlite3");
		oldsql.renameTo(renamesql);
		oldsql = new File(copyPath + "/empty.task.txt");
		renamesql = new File(copyPath + "/" + projectName + ".task.txt");
		oldsql.renameTo(renamesql);
		oldsql = new File(copyPath + "/empty.slot.txt");
		renamesql = new File(copyPath + "/" + projectName + ".slot.txt");
		oldsql.renameTo(renamesql);
		oldsql = new File(copyPath + "/empty.dialogLib.txt");
		renamesql = new File(copyPath + "/" + projectName + ".dialogLib.txt");
		oldsql.renameTo(renamesql);
		oldsql = new File(copyPath + "/empty.DAtype.txt");
		renamesql = new File(copyPath + "/" + projectName + ".DAtype.txt");
		oldsql.renameTo(renamesql);
		oldsql = new File(copyPath + "/empty.svm");
		renamesql = new File(copyPath + "/" + projectName + ".svm");
		oldsql.renameTo(renamesql);
	}
	
	private void Directorycopy(File sourceF, File targetF){
		
		File[] ff = sourceF.listFiles();
		for (File file : ff) {
			File temp = new File(targetF.getAbsolutePath() + File.separator + file.getName());
			
			if(file.isDirectory()){
				temp.mkdir();
				Directorycopy(file, temp);
			} else {
				FileInputStream fis = null;
				FileOutputStream fos = null;
				try {
					fis = new FileInputStream(file);
					fos = new FileOutputStream(temp) ;
					byte[] b = new byte[4096];
					int cnt = 0;
					while((cnt=fis.read(b)) != -1){
						fos.write(b, 0, cnt);
					}
				} catch (Exception e) {
					logger.debug(e.getMessage());
				} finally{
					try {
						fis.close();
						fos.close();
					} catch (IOException e) {
					}
				}
			}
		}
	}
	
	
		
	@Transactional(transactionManager=AppConfig.TRANSACTION_MANAGER_SDS, rollbackFor={Exception.class})
	public int deleteIntentUserSays(String userId, String domainName) {
		logger.info("deleteIntentUserSays/userId = {}, domainName= {}", userId, domainName);
		SdsProjectData project = mapper.getProject(domainName);

		int deleteCount = 0;
		if(project != null) {
			List<SdsIntentData> intentList = mapper.getIntentList(project.getSeqNo());
			for(int i = 0 ; i < intentList.size() ; i++) {
				int intentSeq = intentList.get(i).getSeqNo();
				deleteCount += mapper.deleteIntentUsersay(intentSeq);
			}
		}
		return deleteCount;
	}
	
}
