package ai.mindslab.bizchatbot.backendapi.service.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class WeatherCity implements Serializable{

	private String name;
	private String eng;
	
	public WeatherCity() {
		
	}
	
	public WeatherCity(String name, String eng) {
		this.name = name;
		this.eng = eng;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEng() {
		return eng;
	}

	public void setEng(String eng) {
		this.eng = eng;
	}

	@Override
	public String toString() {
		return "WeatherCity [name=" + name + ", eng=" + eng + "]";
	}
}
