package ai.mindslab.bizchatbot.backendapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.hazelcast.core.IMap;

import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotIntentFinderMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.DialogHistoryMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotDomainSimple;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotIntetFinderSdsData;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.IntentFinderDomainRel;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.SdsProjectPortHis;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.ISDSCodes;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing.Slot;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.SdsData;
import ai.mindslab.bizchatbot.commons.session.data.SdsUtterRespData;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.GlobalItfMapper;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalIntentInfo;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfData;
import ai.mindslab.bizchatbot.commons_ex.gitf.dao.data.GlobalItfSdsData;
import ai.mindslab.bizchatbot.sds.client.SdsClient;
import ai.mindslab.bizchatbot.sds.client.SdsClientFactory;
import jhcho.chatbot.processpool.SdsConfProvider;
import jhcho.chatbot.processpool.SdsIntentFinder;

@Service
public class SdsIntentFinderService implements IChatbotConst{

	private Logger logger = LoggerFactory.getLogger(SdsIntentFinderService.class);
	
	@Autowired
	private ChatbotMapper chatbot;
	
	@Autowired
	private DialogHistoryMapper hisMapper;
	
	@Autowired
	private ChatbotIntentFinderMapper itfMapper;
	
	@Autowired
	private HazelClientCommon hazel;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private GlobalItfMapper globalItf;
	
//	@Autowired
//	private SdsClient sdsClient;
	@Autowired
	private SdsClientFactory sdsFactory;
	
	@Value("${sds.client.type:base}")
	private String sdsClientType;
	
	@Value("${sds.itf.poolsize.core:5}")
	private int poolsize_core;
	
	@Value("${sds.itf.poolsize.max:5}")
	private int poolsize_max;
	
	@Value("${sds.itf.queue.size:5}")
	private int queueSize;
	
	@Value("${sds.itf.poolsize.perItf:2}")
	private int poolsize_perItf;
	
	//for ng
	private SdsIntentFinder ITF;
	
	@PostConstruct
	public void initNgItf() {
		String clientType = env.getProperty("sds.client.type");
		if(IChatbotConst.CLIENT_TYPE_NG.equals(clientType)) {
			if(SdsConfProvider.getInstance().needConfig()) {
				SdsConfProvider.getInstance().setEnginePath(env.getProperty("sds.engine.path"));
				SdsConfProvider.getInstance().setDicPath(env.getProperty("sds.dic.path"));
				SdsConfProvider.getInstance().setRuntime("dial");
			}
			
			ITF = new SdsIntentFinder(poolsize_core, poolsize_max, queueSize, poolsize_perItf);
		}
	}
	
	@PreDestroy
	public void destroyNgItf() {
		if(ITF != null) {
//			ITF.closeItfAll();//QQQ 추가하자 ->hot deploy시 누적되는것 방지 
			ITF.shutdown();
			try {
				ITF.awaitTermination();
			} catch (InterruptedException e) {
			}
		}
	}
	
	
	private HashMap<Integer, ArrayList<SdsData>> mapSdsItf = new HashMap<>();
	private HashMap<Integer, ArrayList<SdsData>> mapGlobalSdsItf = new HashMap<>();
	
	public SdsIntentFinderService() {
	}

	public DialogSession getDialogSession(String sessionId) throws Exception{
		IMap<String, DialogSession> mapSession = hazel.getClient().getMap(MAP_NAME_SESSION);
		return mapSession.get(sessionId);
	}
	
	public List<ChatbotIntetFinderSdsData> getIntentFinderSds(int botId) throws Exception{
		logger.info("getIntentFinderSds/{}",botId);
		List<ChatbotIntetFinderSdsData> list = null;
		try {
			list = chatbot.getIntentFinderSds(botId);
		}catch(Exception e) {
			logger.warn("getIntentFinderSds/{}",e.getMessage());
			throw e;
		}
		return list;
	}
	
	public List<String> getIntentFinderDomain(int botId) {
		logger.debug("getIntentFinderSds/{}",botId);
		List<String> tgts = new ArrayList<>();
		try {
			//lookup intent finder conf in intentfinder_sds
			List<ChatbotIntetFinderSdsData> sdsDomains = getIntentFinderSds(botId);
			if(sdsDomains != null && sdsDomains.size() > 0) {
				
				for(ChatbotIntetFinderSdsData sds: sdsDomains) {
					tgts.add(sds.getProjectName());
				}
				return tgts;
			}
			
			//if can't find, lookup sds_domain again.
			ChatbotConfig config = chatbot.getChatbotConfig(botId);
			List<ChatbotDomainSimple> sdsList = chatbot.getSdsDomainList(config.getConfId());
			if(sdsList !=null) {
				for(ChatbotDomainSimple ds: sdsList) {
					tgts.add(ds.getDomainName());
				}
				return tgts;
			}
			
			return tgts;
		}catch(Exception e) {
			logger.warn("getIntentFinderDomain/{}", e.getMessage());
			return tgts;
		}
	}
	
	public List<String> getSdsDomainList(int botId) {
		
		ChatbotConfig config = chatbot.getChatbotConfig(botId);
		ArrayList<String> list = new ArrayList<>();
		if(config != null) {
			List<ChatbotDomainSimple> sdsList = chatbot.getSdsDomainList(config.getConfId());
			if(sdsList != null) {
				for(ChatbotDomainSimple ds: sdsList) {
					list.add(ds.getDomainName());
				}
			}
		}
		return list;
	}
	
	public synchronized boolean hasActiveSdsIntentFinder(int botId) {
		
		//QQQ check 
		ArrayList<SdsData> list = mapSdsItf.get(botId);
		if(list != null) {
			return true;
		}
		return false;
	}
	
	public synchronized ArrayList<SdsData> initSdsIntentFinder(int botId) {
		logger.info("initSdsIntentFinder/{}",botId);
		SdsUtterRespData sdsResp = null;
		try {
			List<ChatbotIntetFinderSdsData> sdsIntentFinders = chatbot.getIntentFinderSds(botId);
			if(sdsIntentFinders != null) {
				ArrayList<SdsData> l = new ArrayList<>();
				for(ChatbotIntetFinderSdsData sds: sdsIntentFinders) {
					
					int nextPrjPort = DialogUtils.getNextProjectPortItf(hazel.getClient(), env);
					logger.info("intent finder SDS Port:{} for {}", nextPrjPort, botId);
					SdsData sData = new SdsData();
					sData.setProjectName(sds.getProjectName());
					sData.setProjectPort(nextPrjPort);
					sData.setDaemonLogFileName("system"+"."+"0000000000");
					sData.setRequest(ISDSCodes.REQUEST_INIT_DIALOG);
					sData.setSdsIp(env.getProperty("sds.ip"));//QQQ clustering case, choose one ip
					sData.setUserName("system");//QQQ with user id?->user name?
					
					if(IChatbotConst.CLIENT_TYPE_NG.equals(sdsClientType)) {
						ITF.query(sds.getProjectName(), "의도분류");//just initialize
					}else {
//						if(IChatbotConst.CLIENT_TYPE_BASE.equals(sdsClientType)) {
						SdsClient sdsClient = sdsFactory.makeSdsClient();
						sdsClient.setSdsData(sData);
						sdsResp = sdsClient.initDialog();
					}
					l.add(sData);
					
					//port his
					SdsProjectPortHis hisdata = new SdsProjectPortHis();
					hisdata.setBotId(botId);
					hisdata.setSessionId(sData.getDaemonLogFileName());//set with daemon logfile name
					hisdata.setProjectPort(sData.getProjectPort());
					hisdata.setSdsIp(sData.getSdsIp());
					hisdata.setCloseReasonCd(IChatbotConst.SDS_TERMINATION_BY_SYSTEM);
					hisMapper.insertSdsPortHis(hisdata);
					
					break;//QQQ one domain -> db change
				}
				//store sds intent finder
				mapSdsItf.put(botId, l);
				return l;
			}
		}catch(Exception e) {
			logger.warn("/initSdsIntentFinder/{}",e.getMessage());
		}
		logger.debug("/initSdsIntentFinder/sdsresp: {}",sdsResp);
		return null;
	}
	
	public void closeSdsIntentFinders() {
		logger.info("closeSdsIntentFinders/");
		
		Set<Integer> keys = mapSdsItf.keySet();
		for(Integer key:keys) {
			List<SdsData> list = mapSdsItf.get(key);
			if(list != null) {
				for(SdsData d: list) {
					if(IChatbotConst.CLIENT_TYPE_BASE.equals(sdsClientType)) {
						SdsClient sdsClient = sdsFactory.makeSdsClient();
						sdsClient.setSdsData(d);
						try {
							sdsClient.stopDialog();
						} catch (Exception e) {
							logger.warn("closeSdsIntentFinders/{}",e.getMessage());
						}
					}else if(IChatbotConst.CLIENT_TYPE_NG.equals(sdsClientType)){
						try {
							ITF.closeItf(d.getProjectName());
						} catch (Exception e) {
						}
					}
				}
			}
		}
	}
	
	public void closeSdsIntentFinder(int botId) {
		logger.info("closeSdsIntentFinder/{}", botId);
//		if(IChatbotConst.CLIENT_TYPE_BASE.equals(sdsClientType)) {
			List<SdsData> list = mapSdsItf.get(botId);
			if(list != null) {
				for(SdsData d: list) {
					if(IChatbotConst.CLIENT_TYPE_BASE.equals(sdsClientType)) {
						SdsClient sdsClient = sdsFactory.makeSdsClient();
						sdsClient.setSdsData(d);
						try {
							sdsClient.stopDialog();
							list = null;
							mapSdsItf.remove(botId);
						} catch (Exception e) {
							logger.warn("closeSdsIntentFinder/{}",e.getMessage());
						}
					}else if(IChatbotConst.CLIENT_TYPE_NG.equals(sdsClientType)){
						try {
							ITF.closeItf(d.getProjectName());
							mapSdsItf.remove(botId);
						} catch (Exception e) {
						}
					}
				}
			}
//		}
	}
	
	public synchronized ArrayList<SdsData> getSdsItfConfig(int botId){
		return mapSdsItf.get(botId);
	}
	
	public IntentFinderDomainRel getIntentFinderInfo(int botId, String intentName){
		//QQQ cache?
		logger.info("getIntentFinderInfo/{},{}", botId, intentName);
		IntentFinderDomainRel info = null;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("botId", botId);
			params.put("intentName", intentName);
			info = itfMapper.getIntentFinderDomainRelation(params);
		}catch(Exception e) {
			logger.warn("getIntentFinderInfo/{}",e.getMessage());
		}
		return info;
	}
	
	public static Pattern patternIntentName;
	static {
		patternIntentName = Pattern.compile("SLU.*\\#(.+)\\(\\)");
	}
	
	public String extractIntentName(String debug) {
		logger.debug("extractIntentName/{}",debug);
		
		String foundIntent = "";
//		Pattern p;
		Matcher m;
//		p = Pattern.compile("SLU.*\\#(.+)\\(\\)");
		m = patternIntentName.matcher(debug);
		if(m.find()) {
			foundIntent = m.group(1);
			logger.debug("extractIntentName/found intent:{}",foundIntent);
		}
		
		return foundIntent;
	}
	
	public void resetBotSdsItfMap(int botId) {
		mapSdsItf.remove(botId);
	}
	
	//return slu
	public String ngQuery(String itfDomain, String utter) throws Exception{
		logger.info("ngQuery/{},{}",itfDomain, utter);
		String resp = ITF.query(itfDomain, utter);
		return resp;
	}
	
	public String getGlobalIntentFinderSds(int botId) throws Exception{
		logger.info("getGlobalIntentFinderSds/{}",botId);
		String domain = "";
		GlobalItfData gData = null;
		try {
			gData = getGlobalIntentFinderSdsInfo(botId);
			if(gData != null) {
				domain = gData.getProjectName() == null ? "": gData.getProjectName();
			}
			
		}catch(Exception e) {
			logger.warn("getGlobalIntentFinderSds/{}",e.getMessage());
			throw e;
		}
		return domain;
	}
	
	public GlobalItfData getGlobalIntentFinderSdsInfo(int botId) throws Exception{
		logger.info("getGlobalIntentFinderSds/{}",botId);
		GlobalItfData gData = null;
		try {
			gData = globalItf.getItfInfo(botId);
		}catch(Exception e) {
			logger.warn("getGlobalIntentFinderSds/{}",e.getMessage());
			throw e;
		}
		return gData;
	}
	
	public synchronized boolean hasActiveGlobalSdsIntentFinder(int botId) {
		
		//QQQ check 
		ArrayList<SdsData> list = mapGlobalSdsItf.get(botId);
		if(list != null) {
			return true;
		}
		return false;
	}
	
	public synchronized ArrayList<SdsData> initGlobalSdsIntentFinder(int botId) {
		logger.info("initGlobalSdsIntentFinder/{}");
		SdsUtterRespData sdsResp = null;
		try {
			GlobalItfData gitf = globalItf.getItfInfo(botId);
			if(gitf == null) {
				return null;
			}
			List<GlobalItfSdsData> sdsList = globalItf.getItfSdsList(botId);
			gitf.setSdsList(sdsList);
			String domain = globalItf.getItfInfo(botId).getProjectName();
			if(!StringUtils.isEmpty(domain)) {
				ArrayList<SdsData> l = new ArrayList<>();
					
				int nextPrjPort = DialogUtils.getNextProjectPortItf(hazel.getClient(), env);
				logger.info("intent finder SDS Port:{} for {}", nextPrjPort, domain);
				SdsData sData = new SdsData();
				sData.setProjectName(domain);
				sData.setProjectPort(nextPrjPort);
				sData.setDaemonLogFileName("system"+"."+"0000000000");
				sData.setRequest(ISDSCodes.REQUEST_INIT_DIALOG);
				sData.setSdsIp(env.getProperty("sds.ip"));//QQQ clustering case, choose one ip
				sData.setUserName("system");//QQQ with user id?->user name?
				
				if(IChatbotConst.CLIENT_TYPE_NG.equals(sdsClientType)) {
					ITF.query(domain, "의도분류");//just initialize
				}else {
//						if(IChatbotConst.CLIENT_TYPE_BASE.equals(sdsClientType)) {
					SdsClient sdsClient = sdsFactory.makeSdsClient();
					sdsClient.setSdsData(sData);
					sdsResp = sdsClient.initDialog();
				}
				l.add(sData);
				
				//port his
				SdsProjectPortHis hisdata = new SdsProjectPortHis();
				hisdata.setBotId(botId);
				hisdata.setSessionId(sData.getDaemonLogFileName());//set with daemon logfile name
				hisdata.setProjectPort(sData.getProjectPort());
				hisdata.setSdsIp(sData.getSdsIp());
				hisdata.setCloseReasonCd(IChatbotConst.SDS_TERMINATION_BY_SYSTEM);
				hisMapper.insertSdsPortHis(hisdata);
					
				//store sds intent finder
				mapGlobalSdsItf.put(botId, l);
				return l;
			}
		}catch(Exception e) {
			logger.warn("/initGlobalSdsIntentFinder/{}",e.getMessage());
		}
		logger.debug("/initGlobalSdsIntentFinder/sdsresp: {}",sdsResp);
		return null;
	}
	
	public synchronized ArrayList<SdsData> getGlobalSdsItfConfig(int botId){
		return mapGlobalSdsItf.get(botId);
	}
	
	public static Pattern patternIntentInfo;
	public static Pattern patternSlots;
	public static double TARGET_SCORE;
	static {
//		patternIntentInfo = Pattern.compile("SLU.*<([^<^>^=]*)=([^<^>^=]*)>.*\\#([^\\(^\\)]+)\\(.*\\)");
		patternIntentInfo = Pattern.compile("(SLU1.*\\#([^\\(^\\)]+)\\(.*\\)\\s+\\((.*)\\))");
//		patternSlots = Pattern.compile("(<([\\w\\.\\d^=^<^>]*)=([^<^>^=\\w\\d]*)>)+");
		patternSlots = Pattern.compile("(<([^<>=]*)=([^<>=]*)>)");
		TARGET_SCORE = 0.4;
	}
	
	public GlobalIntentInfo extractIntentNameAndSlot(String debug) {
		logger.debug("extractIntentNameAndSlot/{}",debug);
		
		GlobalIntentInfo info = new GlobalIntentInfo();
		Matcher m = patternIntentInfo.matcher(debug);
		if(m.find()) {
			String slu1 = m.group(1);
			String intentName = m.group(2);
			String score = m.group(3);
			logger.debug("extractIntentName/found intent:{}, {}", intentName, score);

            boolean isDiscarded = false;
            if(score.matches("[-+]?[0-9]*\\.?[0-9]+")) {
                double dScore = Double.parseDouble(score);
                if(dScore <= TARGET_SCORE) {
                	isDiscarded = true;
					logger.debug("extractIntentName/discard current intent :{}", score);
                }
            }
            if(!isDiscarded) {
				info.setIntentName(intentName);

				//multiple slots
				Matcher m2 = patternSlots.matcher(slu1);
				while(m2.find()) {
					String slotName = m2.group(2);
					String slotValue = m2.group(3);
					GlobalIntentInfo.Slot slot = new GlobalIntentInfo.Slot(slotName, slotValue);
					info.getSlots().add(slot);
					logger.debug("extractIntentName-array/found intent:{}, {}", slotName, slotValue);
				}
			}
		}
		return info;
	}
	
	public GlobalItfSdsData getSvcTypeByInent(int botId, String intentName) throws Exception{
		logger.info("getSvcTypeByInent/{},{}", botId, intentName);
		
		GlobalItfSdsData out = null;
		try {
			List<GlobalItfSdsData> itfSdsList = globalItf.getItfSdsList(botId);
			
			if(itfSdsList != null) {
				for(GlobalItfSdsData d: itfSdsList) {
					if(d.getIntentName().equals(intentName)) {
						out = d;
						break;
					}
				}
			}
		}catch(Exception e) {
			logger.warn("getSvcTypeByInent/{}", e.getMessage());
			throw e;
		}
		return out;
	}
}
