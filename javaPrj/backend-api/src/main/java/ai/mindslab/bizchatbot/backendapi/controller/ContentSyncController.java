package ai.mindslab.bizchatbot.backendapi.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;

@RestController
@RequestMapping(path = "/admin/content")
public class ContentSyncController {
    private Logger logger = LoggerFactory.getLogger(ContentSyncController.class);

    //33번 장비-> 34번 장비로만 실행되도록 설정되어 있음.
    private final String[] cmd_sync_sds_domains = {"rsync", "-avz", "--progress", "--exclude", "log/*"
            , "/home/mobot/EtriDialog/webtool/www/webtool/www_data/dialog_domains/"
            , "mobot@KZCMCWA02P:/home/mobot/EtriDialog/webtool/www/webtool/www_data/dialog_domains"};
    private final String[] cmd_sync_chatbot_images = {"rsync", "-avz", "--progress"
            , "/home/mobot/maum-chatbot/storage/"
            , "mobot@KZCMCWA02P:/home/mobot/maum-chatbot/storage"};

    @RequestMapping(value="/syncDomains" ,method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody Callable<BaseResponse<String>> syncDomains(HttpServletRequest req) throws Exception{
        logger.info("/admin/content/syncDomains");

        Callable<BaseResponse<String>> callable = new Callable<BaseResponse<String>>() {

            @Override
            public BaseResponse<String> call() throws Exception {
                BaseResponse<String> resp = new BaseResponse<>();
                StringBuilder stringBuff = new StringBuilder();
                try {
                    Process process = execCommands(cmd_sync_sds_domains);
                    process.waitFor(30, TimeUnit.SECONDS);
                    InputStream psout = process.getInputStream();

                    copy(psout, stringBuff);
                    resp.setData(stringBuff.toString());
                }catch(Exception e) {
                    logger.warn("SDS Model sync failure./{}", e.getMessage());
                    resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                    resp.setMsg(e.getMessage());
                }

                return resp;
            }
        };
        return callable;
    }

    @RequestMapping(value="/syncImages" ,method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody Callable<BaseResponse<String>> syncImages(HttpServletRequest req) throws Exception{
        logger.info("/admin/content/syncImages");

        Callable<BaseResponse<String>> callable = new Callable<BaseResponse<String>>() {

            @Override
            public BaseResponse<String> call() throws Exception {
                BaseResponse<String> resp = new BaseResponse<>();
                StringBuilder stringBuff = new StringBuilder();
                try {
                    Process process = execCommands(cmd_sync_chatbot_images);
                    process.waitFor(30, TimeUnit.SECONDS);
                    InputStream psout = process.getInputStream();

                    copy(psout, stringBuff);
                    resp.setData(stringBuff.toString());
                }catch(Exception e) {
                    logger.warn("Chatbot Image sync failure./{}", e.getMessage());
                    resp.setCode(IRestCodes.ERR_CODE_FAILURE);
                    resp.setMsg(e.getMessage());
                }

                return resp;
            }
        };
        return callable;
    }


    private static Process execCommands(String[] command) throws Exception {

        ProcessBuilder builder = new ProcessBuilder(command);
        Process process = builder.start();

        return process;
    }

    private static void copy(InputStream input, StringBuilder output) throws IOException {
        byte[] buffer = new byte[1024];
        int n = 0;
        while ((n = input.read(buffer)) != -1) {
            output.append(new String(buffer, 0, n));
        }
    }
}
