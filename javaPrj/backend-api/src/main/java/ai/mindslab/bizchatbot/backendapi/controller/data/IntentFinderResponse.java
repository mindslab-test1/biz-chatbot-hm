package ai.mindslab.bizchatbot.backendapi.controller.data;

import java.io.Serializable;

public class IntentFinderResponse<T> implements Serializable{

	protected int botId;
	protected String userUtter;
	protected String domain;
	protected T debug;
	
	public IntentFinderResponse() {
	}

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public String getUserUtter() {
		return userUtter;
	}

	public void setUserUtter(String userUtter) {
		this.userUtter = userUtter;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public T getDebug() {
		return debug;
	}

	public void setDebug(T debug) {
		this.debug = debug;
	}

	@Override
	public String toString() {
		return "IntentFinderResponse [botId=" + botId + ", userUtter=" + userUtter + ", domain=" + domain + ", debug="
				+ debug + "]";
	}
}
