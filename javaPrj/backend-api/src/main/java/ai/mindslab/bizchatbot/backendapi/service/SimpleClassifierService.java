package ai.mindslab.bizchatbot.backendapi.service;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.backendapi.controller.data.IntentFinderResponse;
import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.SimpleClassifierData;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;

@Service
public class SimpleClassifierService implements IChatbotConst{

	private Logger logger = LoggerFactory.getLogger(SimpleClassifierService.class);
	
	private HashMap<String, Pattern> pattern = new HashMap<>();
	
	//@Value("${sds.default.domain}")
	private String sdsDefaultDomain="";
	
	@Autowired
	private ChatbotMapper chatbot;
	
	public SimpleClassifierService() {
	}
	
	public IntentFinderResponse<String> doSimple(int botId, String userUtter) throws Exception {
		
		
		ChatbotConfig config = chatbot.getChatbotConfig(botId);
		if(config == null) {
			throw new Exception("Chatbot is not exist");
		}
		
		IntentFinderResponse<String> out = new IntentFinderResponse<>();
		if("N".equals(config.getScfYn())) {
			throw new Exception("Simple Classification is not configured.");
		}
		
		if(StringUtils.isEmpty(userUtter)) {
			out.setDomain(sdsDefaultDomain);
			out.setDebug("empty user utter");
		}
		List<SimpleClassifierData> scs = null;
		try {
			scs = getSimpleClassifer(botId);
		}catch(Exception e) {
			logger.warn("doSimple/{}",e.getMessage());
			out.setDomain(sdsDefaultDomain);
			out.setDebug("");
		}
		
		String selDomain = "";
		
		Pattern p;
		Matcher m;
		if(scs != null) {
			for(SimpleClassifierData sc: scs) {
				p = getPattern(sc.getPattern());
				m = p.matcher(userUtter);
				if(m.matches()) {
//				if(m.find()) {
					selDomain = sc.getSdsDomain();
					logger.debug("doSimple/{}/Domain:{}",userUtter, selDomain);
					out.setDebug(String.format("botId:%d, exp:%s", sc.getBotId(), sc.getPattern()));
					out.setDomain(selDomain);
					break;
				}
			}
		}
		
		return out;
	}
	
	public List<SimpleClassifierData> getSimpleClassifer(int botId) throws Exception{
		//QQQ cache?
		List<SimpleClassifierData> scs = null;
		try {
			scs = chatbot.getSimpleClassifer(botId);
		}catch(Exception e) {
			logger.warn("getSimpleClassifer/{}",e.getMessage());
			throw e;
		}
		return scs;
	}
	
	public Pattern getPattern(String regex) throws PatternSyntaxException{
		
		Pattern p = pattern.get(regex);
		if(p == null) {
			p = Pattern.compile(regex);
			pattern.put(regex, p);
		}
		return p;
	}
}
