package ai.mindslab.bizchatbot.backendapi.controller;

import ai.mindslab.bizchatbot.bqa.dao.data.QaExcelVo;
import ai.mindslab.bizchatbot.bqa.dao.data.RichExcelVo;
import ai.mindslab.bizchatbot.bqa.service.BqaRichContentService;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfoDisp;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichApi;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseListObject;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao.data.QaRichVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/bqa/rich")
public class BqaAdminRichController implements IRestCodes {

	private Logger logger = LoggerFactory.getLogger(BqaAdminRichController.class);

	@Autowired
	private BqaRichContentService bqaRichContentService;

	@RequestMapping(value = "/insertBqaRichContent", method = { RequestMethod.POST })
	public BaseResponse<?> insertBqaRichContent(
			@RequestParam(name = "respCode") String respCode
			, @RequestParam(name = "contentTypeCd") String contentTypeCd
			, @RequestParam(name = "userId") String userId
	) throws BizChatbotException {
		logger.info("BqaAdminRichController insertBqaRichContent parameter/{},{},{}", respCode, contentTypeCd, userId);
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		
		try {

			long nRet = bqaRichContentService.insertBqaRichContent(respCode, contentTypeCd, userId);
			logger.debug("inserted Rich Content count {}", nRet);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}

	@RequestMapping(value = "/deleteBqaRichContent", method = { RequestMethod.POST })
	public BaseResponse<?> deleteBqaRichContent(
			@RequestParam(name = "respCode") String respCode
			, @RequestParam(name = "contentTypeCd") String contentTypeCd
	) throws BizChatbotException {
		logger.info("BqaAdminRichController deleteBqaRichContent parameter/{},{}", respCode, contentTypeCd);
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			long nRet = bqaRichContentService.deleteBqaRichContent(respCode, contentTypeCd);
			logger.debug("deleted Rich Content count {}", nRet);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}

	@RequestMapping(value="/getBqaRichContent",  method = {RequestMethod.POST})
	public BaseResponse<?> getBqaRichContent(
			@RequestParam(name = "respCode") String respCode
	) throws BizChatbotException{
		BaseResponse<?> response = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		logger.info("BqaAdminRichController getBqaRichContentByCode parameter/{}", respCode);

		try {
			QaRichVo qaRichVo = bqaRichContentService.getBqaRichInfo(respCode);
			if(qaRichVo != null && !IChatbotConst.RICH_CONTENT_TYPE_API.equals(qaRichVo.getContentTypeCd())) {
				BaseResponse<ChatbotRichContent> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
				ChatbotRichContent rich = bqaRichContentService.getBqaRichContent(respCode);
				resp.setData(rich);

				response = resp;
			}else if(qaRichVo != null && IChatbotConst.RICH_CONTENT_TYPE_API.equals(qaRichVo.getContentTypeCd())) {
				BaseResponse<ChatbotRichApi> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
				ChatbotApiInfo api = bqaRichContentService.getBqaApiInfo(respCode);

				ChatbotRichApi apiData = new ChatbotRichApi();
				apiData.setContentTypeCd(IChatbotConst.RICH_CONTENT_TYPE_API);
				if(api != null && qaRichVo.getRcId() != 0) {
					apiData.setIfId(api.getIfId());
					apiData.setApiData(api);
				}
				resp.setData(apiData);

				response = resp;
			}else {
				throw new BizChatbotException(IRestCodes.ERR_CODE_FAILURE, "Invalid Rich Content Type");
			}
		}catch(BizChatbotException e) {
			response.setCode(e.getErrCode());
			response.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}

		return response;
	}

	@RequestMapping(value = "/getBqaApiList", method = { RequestMethod.POST })
	public BaseResponse<?> getBqaApiList(
			@RequestParam(name = "workspaceId") int workspaceId
	) throws BizChatbotException {
		BaseResponse<BaseListObject<ChatbotApiInfoDisp>> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		logger.info("BqaAdminRichController getBqaApiList parameter/{}", workspaceId);

		try {
			BaseListObject<ChatbotApiInfoDisp> list = bqaRichContentService.getBqaApiList(workspaceId);
			resp.setData(list);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}

	@RequestMapping(value = "/setBqaApi", method = { RequestMethod.POST })
	public BaseResponse<?> setBqaApi(
			@RequestParam(name = "respCode") String respCode
			, @RequestParam(name = "rcId") int rcId
			, @RequestParam(name = "userId") String userId
	) throws BizChatbotException {
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		logger.info("BqaAdminRichController setBqaApi parameter/{},{},{}", respCode, rcId, userId);

		try {
			long nRet = bqaRichContentService.setBqaApi(respCode, rcId, userId);
			logger.debug("updated response relation table count:{}", nRet);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}

	@RequestMapping(value = "/addButton", method = { RequestMethod.POST })
	public BaseResponse<?> addButton(
			@RequestParam(name = "respCode") String respCode
			, @RequestParam(name = "btnOrder") int btnOrder
			, @RequestParam(name = "title") String title
			, @RequestParam(name = "utter") String utter
			, @RequestParam(name = "userId") String userId
	) throws BizChatbotException {
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		logger.info("BqaAdminRichController addButton parameter/{},{},{},{},{}", respCode, btnOrder, title, utter, userId);

		try {
			long nRet = bqaRichContentService.addSingleButton(respCode, btnOrder, title, utter, userId);
//			logger.debug("updated button count:{}", nRet);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}

	@RequestMapping(value = "/delButton", method = { RequestMethod.POST })
	public BaseResponse<?> delButton(
			@RequestParam(name = "respCode") String respCode
			, @RequestParam(name = "btnOrder") int btnOrder
	) throws BizChatbotException {
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		logger.info("BqaAdminRichController delButton parameter/{},{}", respCode, btnOrder);

		try {
			long nRet = bqaRichContentService.deleteSingleButton(respCode, btnOrder);
//			logger.debug("deleted button count:{}", nRet);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}

	@RequestMapping(value = "/addImage", method = { RequestMethod.POST })
	public BaseResponse<?> addImage(
			@RequestParam(name = "respCode") String respCode
			, @RequestParam(name = "imgText", required=false) String imgText
			, @RequestParam(name = "imgLink", required=false) String imgLink
			, @RequestParam(name = "userId") String userId
			, HttpServletRequest request
	) throws BizChatbotException {
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		logger.info("BqaAdminRichController addImage parameter/{},{},{}", respCode, imgText, imgLink, userId);

		try {
			long nRet = bqaRichContentService.addImage(respCode, imgText, imgLink, userId, request);
//			logger.debug("updated image count:{}", nRet);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}

	@RequestMapping(value = "/addCarousel", method = { RequestMethod.POST })
	public BaseResponse<?> addCarousel(
			@RequestParam(name = "respCode") String respCode
			, @RequestParam(name = "imgLink", required=false) String imgLink
			, @RequestParam(name = "userId") String userId
			, HttpServletRequest request
	) throws BizChatbotException {
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		logger.info("BqaAdminRichController addCarousel parameter/{},{},{}", respCode, imgLink, userId);

		try {
			long nRet = bqaRichContentService.addCarousel(respCode, imgLink, userId, request);
//			logger.debug("updated Carousel count:{}", nRet);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}

	@RequestMapping(value = "/deleteCarousel", method = { RequestMethod.POST })
	public BaseResponse<?> deleteCarousel(
			@RequestParam(name = "respCode") String respCode
			, @RequestParam(name = "carouselOrder") int carouselOrder
	) throws BizChatbotException {
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		logger.info("BqaAdminRichController deleteCarousel parameter/{},{}", respCode, carouselOrder);

		try {
			long nRet = bqaRichContentService.deleteCarousel(respCode, carouselOrder);
//			logger.debug("deleted Carousel count:{}", nRet);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}

	@RequestMapping(value = "/insertBqaRichExcelfile", method = { RequestMethod.POST })
	public BaseResponse<?> insertBqaRichExcelfile(
			@RequestParam(required = false, name = "userId", defaultValue="admin") String userId
			, @RequestParam("file") MultipartFile uploadFile
	) throws BizChatbotException {

		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			logger.info("RestController insertBqaRichExcelfile params/{}, {}", userId, uploadFile);

			List<Map<String,Object>> eList = bqaRichContentService.uploadBqaRichExcelData(userId, uploadFile);

			int result = bqaRichContentService.insertBqaRichExcelfile(eList);
			resp.setData(result);

		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}

		return resp;
	}


	/**
	 * multi-part 안쓴버전..
	 * 테스트안해봄...나중을 위해..
	 * 지우지는 않았음...
	 */
/*	@RequestMapping(value = "/insertBqaRichExcelfile", method = { RequestMethod.POST })
	public BaseResponse<?> insertBqaRichExcelfile(@Valid @RequestBody List<RichExcelVo> eList
	) throws BizChatbotException {
		logger.info("RestController insertExcel parameter eList {}",eList);
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {
			int result = bqaRichContentService.insertBqaRichExcelfile(eList);
			resp.setData(result);

		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}

		return resp;
	}*/
}
