package ai.mindslab.bizchatbot.backendapi.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.TimeBase;

@RestController
@RequestMapping(path="/chatbot")
public class ChatbotController implements IRestCodes{
	
	private Logger logger = LoggerFactory.getLogger(ChatbotController.class);

	@Value("${chatbot.image.storage:/}")
	private String chatbotImgStorage;

	@Value("${rich.image.path:/}")
	private String richImgStorage;
	
	@Value("${chatbot.image.url}")
	private String imgUrl;
	
	private HashMap<String, TimeBase<String>> etagmap = new HashMap<>();

	public ChatbotController() {
	}

	@RequestMapping(value="/image/{imgloc}/**", method = {RequestMethod.GET})
	public void getImageAsByteArray(@PathVariable("imgloc") String imgPath, HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		
		final String path = request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE).toString();
	    final String bestMatchingPattern = request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE).toString();
	    String arguments = new AntPathMatcher().extractPathWithinPattern(bestMatchingPattern, path);

		
		logger.info("/image/{}/{}",imgPath,arguments);
		long timeout = 300*60*1000;//5min

		String storage;
		if(imgPath.contains("ws")) {
			storage = chatbotImgStorage;
		} else {
			storage = richImgStorage;
		}
		String fullPath = storage+"/"+imgPath+"/"+arguments;
		File inFile = new File(fullPath);
		if(!inFile.exists() || !inFile.canRead()) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		String lastMod = ""+inFile.lastModified();
		String ifNonMatch = request.getHeader("If-None-Match");
		long curr = System.currentTimeMillis();
		TimeBase<String> cacheMod = etagmap.get(fullPath);
		if( cacheMod != null && lastMod.equals(ifNonMatch) && !cacheMod.isExpired(curr, timeout)) {
			response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
			return;
		}
		//store data
		etagmap.put(fullPath, new TimeBase<String>(lastMod));
		response.setHeader("ETag", ""+lastMod);
		response.setContentType(getImageContentType(imgPath));
//		byte[] buff = FileUtils.readFileToByteArray(new File(imgStorage+"/"+imgPath));
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(inFile));
		IOUtils.copy(in, response.getOutputStream());
		if(in != null) {
			try {
				in.close();
			}catch(Exception e) {
			}
		}
	}
	
	private String getImageContentType(String imgPath) throws Exception{
		
		if( StringUtils.isEmpty(imgPath) ) {
			return "";
		}else if(FilenameUtils.getExtension(imgPath).toLowerCase().equals("png")) {
			return MediaType.IMAGE_PNG_VALUE;
		}else if(FilenameUtils.getExtension(imgPath).toLowerCase().equals("jpg") 
				|| FilenameUtils.getExtension(imgPath).toLowerCase().equals("jpeg") ) {
			return MediaType.IMAGE_JPEG_VALUE;
		}else if(FilenameUtils.getExtension(imgPath).toLowerCase().equals("gif")) {
			return MediaType.IMAGE_GIF_VALUE;
		}else {
			return "";
		}
	}
}
