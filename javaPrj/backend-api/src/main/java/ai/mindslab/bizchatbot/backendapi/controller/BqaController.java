package ai.mindslab.bizchatbot.backendapi.controller;

import ai.mindslab.bizchatbot.bqa.dao.data.NlpAnalysisVo;
import ai.mindslab.bizchatbot.commons.data.BaseListObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.bizchatbot.bqa.dao.data.QaAnswerVo;
import ai.mindslab.bizchatbot.bqa.service.BqaSearchService;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;

import javax.servlet.http.HttpServletRequest;

@Controller
@RestController
@RequestMapping("/bqa")
public class BqaController implements IRestCodes {

	private Logger logger = LoggerFactory.getLogger(BqaController.class);

	@Autowired
	private BqaSearchService bqaSearchService;

	@RequestMapping(value = "/search", method = { RequestMethod.POST })
	public @ResponseBody BaseResponse<?> search(@RequestParam(name = "question", required = true) String question,
			@RequestParam(name = "searchFlowType", defaultValue = "0") int searchFlowType,
			@RequestParam(name = "range", defaultValue = "0") int range,
			@RequestParam(name = "debugYn", defaultValue = "N") String debugYn,
			@RequestParam(name = "domainIds", required = true) int[] domainIds) throws BizChatbotException {
		logger.info("RestController search/{}/{}", question, domainIds);

		BaseResponse<Object> resp = null;
		try {
			if(domainIds == null || domainIds.length == 0) {
				throw new BizChatbotException(ERR_CODE_SEARCH_DOMAIN_PARAMETER_ERROR, ERR_MSG_SEARCH_DOMAIN_PARAMETER_ERROR);	
			}
			if(debugYn.equals("N")) {
				resp = bqaSearchService.search(question,searchFlowType,range,domainIds);
			}else {
				resp = bqaSearchService.searchForDebug(question,searchFlowType,range,domainIds);
			}
			logger.info("search Object/{}/{}", question, resp.getData());
		} catch (BizChatbotException e) {
			resp = new BaseResponse<>(e.getErrCode(), e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}finally {
			return resp;
		}
	}

	@RequestMapping(value = "/nlpAnalyze", method = { RequestMethod.POST })
	public BaseResponse<?> nlpAnalyze(
			HttpServletRequest request
			, @RequestParam(name = "data") String data
	) throws BizChatbotException {
		logger.debug("nlpAnalyze text/{}", data);

		BaseResponse<String> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		String morph;
		try {
			morph = bqaSearchService.nlpAnalyze(data);
			resp.setData(morph);
		} catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}
}
