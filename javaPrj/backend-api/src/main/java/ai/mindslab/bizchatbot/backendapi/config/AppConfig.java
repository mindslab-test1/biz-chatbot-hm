package ai.mindslab.bizchatbot.backendapi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;

@Configuration
public class AppConfig {
	public static final String DATASOURCE_MAIN = "dataSource1";
	public static final String DATASOURCE_SDS = "sdsDatasource";
	
	public static final String TRANSACTION_MANAGER_MAIN = "transactionManager1";
	public static final String TRANSACTION_MANAGER_SDS = "sdsTransactionManager";
	
	public static final String TRANSACTION_SESSION_FACTORY_MAIN = "sqlSessionFactory1";
	public static final String TRANSACTION_SESSION_FACTORY_SDS = "sdsSqlSessionFactory";
	
	@Value("${hazelcast.server.ip}")
	private String[] hazelIps;
	
	@Value("${hazelcast.group.name}")
	private String groupName;
	
	@Value("${hazelcast.group.password}")
	private String groupPass;
	
	@Autowired
	@Qualifier("hazelClientCommon")
	private HazelClientCommon hazelClient;
	
	
	@Bean
	@Qualifier("hazelCacheManager")
	public CacheManager cacheManager() {
		return new HazelcastCacheManager(hazelcastInstance());
	}
	
	@Bean
    public HazelcastInstance hazelcastInstance() {
		return hazelClient.getClient();
    }
	
	@Bean
    public KeyGenerator keyGenerator() {
        return new SimpleKeyGenerator();
    }
}
