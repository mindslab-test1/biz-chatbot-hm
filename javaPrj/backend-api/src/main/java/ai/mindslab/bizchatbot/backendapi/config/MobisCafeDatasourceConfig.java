package ai.mindslab.bizchatbot.backendapi.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import kr.co.mobis.bizchatbot.legacy.cafeteria.dao.MobisCafeteria;

@Configuration
@MapperScan(basePackages= {"kr.co.mobis.bizchatbot.legacy.cafeteria"}, annotationClass=MobisCafeteria.class, sqlSessionFactoryRef="cafe-session-factory")
public class MobisCafeDatasourceConfig {

	@Autowired
	private MybatisConfigurationSupport mybatisConfigurationSupport;
	
	@Bean(name="cafe-datasource", destroyMethod="close")
	@ConfigurationProperties(prefix="cafe.datasource")
	public DataSource cafeDataSource() {
		DataSource ds = DataSourceBuilder.create().build();
		return ds;
	}
	
	@Bean(name="cafe-transactionmanager")
	public PlatformTransactionManager cafeTransactionManager() {
		return new DataSourceTransactionManager(cafeDataSource());
	}
	
	@Bean(name="cafe-session-factory")
	public SqlSessionFactory cafeSqlSessionFactory(@Qualifier("cafe-datasource") DataSource dataSource) throws Exception{
		return mybatisConfigurationSupport.build(dataSource);
	}
	
//	@Bean(name="cafe-session-template")
//    public SqlSessionTemplate cafeSqlSessionTemplate(@Qualifier("cafe-session-factory")SqlSessionFactory sqlSessionFactory) throws Exception{
//    		return new SqlSessionTemplate(sqlSessionFactory);
//    }

}
