package ai.mindslab.bizchatbot.backendapi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.bizchatbot.bqa.nlp.NlpAnalyzeClient;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;

@Controller
@RestController
@RequestMapping("/nlp")
public class NlpController implements IRestCodes {
    
    private Logger logger = LoggerFactory.getLogger(NlpController.class);
    
    @Autowired
    private NlpAnalyzeClient nlpAnalyzeClient;
  
    @RequestMapping(value = "/simpleNlp", method = { RequestMethod.POST })
    public BaseResponse<?> simpleNlp(@RequestParam(name = "question", required = true) String question) throws BizChatbotException {
        logger.info("RestController simpleNlp/{}", question);
        
        BaseResponse<String> resp = null;
        
        try {
            String result = nlpAnalyzeClient.analyze(question);
            resp = new BaseResponse<String>();
            resp.setData(result);
            return resp;
        } catch (Exception e) {
            logger.warn("simpleNlp Error/{}", e);
            throw new BizChatbotException(ERR_CODE_NLP_ANALYSIS_ERROR, ERR_MSG_NLP_ANALYSIS_ERROR);
        }
    }
}
