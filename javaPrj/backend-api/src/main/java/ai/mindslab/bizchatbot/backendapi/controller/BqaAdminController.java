package ai.mindslab.bizchatbot.backendapi.controller;

import ai.mindslab.bizchatbot.bqa.dao.data.*;
import ai.mindslab.bizchatbot.bqa.service.BqaIndexingService;
import ai.mindslab.bizchatbot.bqa.service.BqaInitService;
import ai.mindslab.bizchatbot.bqa.service.BqaManagementService;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseListObject;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/bqa")
public class BqaAdminController implements IRestCodes {

	private Logger logger = LoggerFactory.getLogger(BqaAdminController.class);

	@Autowired
	private BqaManagementService managementService;

	@Autowired
	private BqaIndexingService indexingService;

	@Autowired
	private BqaInitService initService;

		/*@RequestMapping(value = "/createQna", method = { RequestMethod.POST })
	public BaseResponse<?> createQna(@Valid @RequestBody QaQuestionVo qaQuestionVo
			) throws BizChatbotException {
		logger.info("RestController createQna parameter {}",qaQuestionVo);
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			int result = managementService.createQna(qaQuestionVo);
			resp.setData(result);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}*/


	@RequestMapping(value = "/createQna", method = { RequestMethod.POST })
	public BaseResponse<?> createQna(@Valid @RequestBody QaBqaVo qaBqaVo ) throws BizChatbotException {
		logger.info("RestController createQna parameter {}",qaBqaVo);
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			int result = managementService.createQna(qaBqaVo);
			resp.setData(result);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}


	@RequestMapping(value = "/updateQna", method = { RequestMethod.POST })
	public BaseResponse<?> updateQna(@Valid @RequestBody QaBqaVo qaBqaVo
			) throws BizChatbotException {
		logger.info("RestController updateQna parameter {}",qaBqaVo);
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			int result = managementService.updateQna(qaBqaVo);
			resp.setData(result);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}



	/*@RequestMapping(value = "/deleteQna", method = { RequestMethod.POST })
	public BaseResponse<?> deleteQna(@Valid @RequestBody QaQuestionVo qaQuestionVo
			) throws BizChatbotException {
		logger.info("RestController deleteQna parameter{}",qaQuestionVo);
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			int result = managementService.deleteQna(qaQuestionVo);
			resp.setData(result);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}
	*/


	@RequestMapping(value = "/deleteQna", method = { RequestMethod.POST })
	public BaseResponse<?> deleteQna(@Valid @RequestBody QaBqaVo qaBqaVo
	) throws BizChatbotException {
		logger.info("RestController deleteQna parameter{}",qaBqaVo);
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			int result = managementService.deleteQna(qaBqaVo);
			resp.setData(result);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}


	@RequestMapping(value = "/deleteQnaByDomainId", method = { RequestMethod.POST })
	public BaseResponse<?> deleteQnaByDomainId(@RequestParam(name = "domainIds", required=true) int[] domainIds
			) throws BizChatbotException {
		logger.info("RestController deleteQnaByDomainId domainIds{}",domainIds);
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			int result = managementService.deleteQnaByDomainId(domainIds);
			resp.setData(result);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}


	@RequestMapping(value = "/insertExcel", method = { RequestMethod.POST })
	public BaseResponse<?> insertExcel(@Valid @RequestBody List<QaExcelVo> eList
			) throws BizChatbotException {
		logger.info("RestController insertExcel parameter eList {}",eList);
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		try {
			int result = managementService.insertExcel(eList);
			resp.setData(result);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}


	@RequestMapping(value = "/insertExcelfile", method = { RequestMethod.POST })
	public BaseResponse<?> insertExcelfile(@RequestParam(name = "domainId") String domainId,
									   @RequestParam(name = "userId") String userId,
									   @RequestParam("file") MultipartFile uploadFile) throws BizChatbotException {

		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			List<QaExcelVo> eList = managementService.uploadExcelData(userId, domainId, uploadFile);

			logger.info("RestController insertExcel parameter eList {}",eList);

			int result = managementService.insertExcel(eList);
			resp.setData(result);

		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}

		return resp;
	}


	@RequestMapping(value = "/getsExcel", method = { RequestMethod.POST })
	public BaseResponse<?> getsExcel(@RequestParam(name = "domainId", defaultValue = "0") int domainId
			) throws BizChatbotException {
		logger.info("RestController getsExcel domainId/{}",domainId);

		BaseListObject<QaExcelVo> listObj;
		BaseResponse<BaseListObject<QaExcelVo>> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		try {
			listObj = managementService.getsExcel(domainId);
			resp.setData(listObj);
		} catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}

		return resp;
	}

	/*@RequestMapping(value = "/getsQuestion", method = { RequestMethod.POST })
	public BaseResponse<?> getsQuestion(@RequestParam(name = "domainIds", defaultValue = "0") int[] domainIds,
			@RequestParam(name = "pg", defaultValue = "1") int pg,
			@RequestParam(name = "search", defaultValue = "") String search,
			@RequestParam(name = "type", defaultValue = "0") int type,
			@RequestParam(name = "pgSize", defaultValue = "10" ) int pgSize) throws BizChatbotException {
		logger.info("RestController getsQuestion domainIds/{}, pg/{}", domainIds, pg);

		BaseListObject<QaQuestionVo> listObj;
		BaseResponse<BaseListObject<QaQuestionVo>> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		try {
			listObj = managementService.getsQuestion(pg, pgSize, search ,type , domainIds);
			resp.setData(listObj);
		} catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}

		return resp;
	}*/

    @RequestMapping(value = "/getsQuestion", method = { RequestMethod.POST })
    public BaseResponse<?> getsQuestion(@RequestParam(name = "domainIds", defaultValue = "0") int[] domainIds,
                                        @RequestParam(name = "pg", defaultValue = "1") int pg,
                                        @RequestParam(name = "search", defaultValue = "") String search,
                                        @RequestParam(name = "type", defaultValue = "0") int type,
                                        @RequestParam(name = "pgSize", defaultValue = "10" ) int pgSize) throws BizChatbotException {
        logger.info("RestController getsQuestion domainIds/{}, pg/{}", domainIds, pg);

        BaseListObject<QaBqaVo> listObj;
        BaseResponse<BaseListObject<QaBqaVo>> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
        try {
            listObj = managementService.getsQuestion(pg, pgSize, search ,type , domainIds);
            resp.setData(listObj);
        } catch (BizChatbotException e) {
            resp.setCode(e.getErrCode());
            resp.setMsg(e.getErrMsg());
            throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
        }

        return resp;
    }


	@RequestMapping(value = "/nlpTest", method = { RequestMethod.POST })
	public BaseResponse<?> nlpTest(HttpServletRequest request, @RequestParam(name = "data") String data)
			throws BizChatbotException {
		logger.debug("nlpTest text/{}", data);

		BaseListObject<NlpAnalysisVo> listObj;
		BaseResponse<BaseListObject<NlpAnalysisVo>> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		try {
			listObj = managementService.nlpTest(data);
			resp.setData(listObj);
		} catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}

	@RequestMapping(value = "/fullIndexing", method = { RequestMethod.POST })
	public BaseResponse<?> fullIndexing(@RequestParam(name = "creatorId") String creatorId) throws BizChatbotException {
		logger.info("RestController fullIndexing");

		BaseResponse<QaIndexingHistoryVo> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			QaIndexingHistoryVo qaIndexingHistoryVo = indexingService.fullIndexing(creatorId);
			resp.setData(qaIndexingHistoryVo);
		} catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}

	@RequestMapping(value = "/addIndexing", method = { RequestMethod.POST })
	public BaseResponse<?> addIndexing(@RequestParam(name = "creatorId") String creatorId) throws BizChatbotException {
		logger.info("RestController addIndexing");

		BaseResponse<QaIndexingHistoryVo> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			QaIndexingHistoryVo qaIndexingHistoryVo = indexingService.addIndexing(creatorId);
			resp.setData(qaIndexingHistoryVo);
		} catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}

	@RequestMapping(value = "/domainIndexing", method = { RequestMethod.POST })
	public BaseResponse<?> domainIndexing(
			@RequestParam(name = "domainId", required = true) int domainId,
			@RequestParam(name = "creatorId") String creatorId)
			throws BizChatbotException {
		logger.info("RestController domainIndexing domainId = {}",domainId);

		BaseResponse<QaIndexingHistoryVo> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			QaIndexingHistoryVo qaIndexingHistoryVo = indexingService.domainIndexing(domainId, creatorId);
			resp.setData(qaIndexingHistoryVo);
		} catch (BizChatbotException e) {

			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());

			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());

		}
		return resp;
	}

	@RequestMapping(value = "/init", method = { RequestMethod.POST })
	public BaseResponse<?> init() throws BizChatbotException {
		logger.info("RestController init");

		BaseResponse<String> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);

		try {

			String res = initService.init();
			resp.setData(res);
		} catch (Exception e) {
			logger.error("init error", e);
			throw new BizChatbotException(ERR_CODE_FAILURE, e.getMessage());
		}
		return resp;
	}

	@RequestMapping(value = "/insertParaphrase", method = { RequestMethod.POST })
	public BaseResponse<?> insertParaphrase(@RequestBody Map<String, Object> map
			) throws BizChatbotException {
		logger.info("RestController insertParaphrase parameter map {}",map);
		BaseResponse<Integer> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		try {
			int result = managementService.insertParaphrase(map);
			resp.setData(result);
		}catch (BizChatbotException e) {
			resp.setCode(e.getErrCode());
			resp.setMsg(e.getErrMsg());
			throw new BizChatbotException(e.getErrCode(), e.getErrMsg());
		}
		return resp;
	}
}
