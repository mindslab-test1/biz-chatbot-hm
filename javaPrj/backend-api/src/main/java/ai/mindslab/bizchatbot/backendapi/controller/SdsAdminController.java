package ai.mindslab.bizchatbot.backendapi.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ai.mindslab.bizchatbot.backendapi.controller.data.SdsIntentUserSayReq;
import ai.mindslab.bizchatbot.backendapi.service.SdsIntentFinderMapperService;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.sds.dao.data.SdsProjectData;


@RestController
@RequestMapping(path = "/admin/sds")
public class SdsAdminController {
	private Logger logger = LoggerFactory.getLogger(SdsAdminController.class);
	
	@Autowired
	@Qualifier("SdsAdmin")
	private SdsIntentFinderMapperService sdsService;
	
	
	@RequestMapping(value="/insertCorpus" ,method = {RequestMethod.POST})
	public BaseResponse<String> getIntentUserSays(@RequestBody SdsIntentUserSayReq says, HttpServletRequest req) throws Exception{
		logger.info("/admin/sds/insertUserSay/{}",says);
		
		BaseResponse<String> resp = new BaseResponse<>();
		try {
			sdsService.insertItfCorpus(says.getUserId(), says.getDomain(), says.getIntentName(), says.getUserSays());
		}catch(Exception e) {
			resp.setCode(IRestCodes.ERR_CODE_FAILURE);
			resp.setMsg(e.getMessage());
		}
		
		return resp;
	}
	
	@RequestMapping(value = "/getproject",method = {RequestMethod.POST})
	public BaseResponse<SdsProjectData> gettest(@RequestParam(name="projectName", required=true)String projectName, HttpServletRequest req) throws Exception{
		logger.info("/admin/sds/gettest/{}",projectName);
	
		BaseResponse<SdsProjectData> resp = new BaseResponse<SdsProjectData>();
		try {
			SdsProjectData data = sdsService.getProject(projectName);
			resp.setData(data);
		}catch(Exception e) {
			resp.setCode(IRestCodes.ERR_CODE_FAILURE);
			resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
		}
		return resp;
	}
	
	@RequestMapping(value = "/dialoglearing",method = {RequestMethod.POST})
	public BaseResponse<SdsProjectData> dialoglearn(@RequestParam(name="projectName", required=true)String projectName, HttpServletRequest req) throws Exception{
		logger.info("/admin/sds/dialoglearn/{}",projectName);
	
		BaseResponse<SdsProjectData> resp = new BaseResponse<SdsProjectData>();
		try {
			SdsProjectData data = sdsService.getProject(projectName);
			resp.setData(data);
		}catch(Exception e) {
			resp.setCode(IRestCodes.ERR_CODE_FAILURE);
			resp.setMsg(IRestCodes.ERR_MSG_FAILURE);
		}
		return resp;
	}
	
	@RequestMapping(value="/deleteCorpus" ,method = {RequestMethod.POST})
	public BaseResponse<Integer> deleteIntentUserSays(@RequestParam(value="userId", required=true) String userId,@RequestParam(value="domain", required=true) String domain, HttpServletRequest req) throws Exception{
		logger.info("/admin/sds/deleteIntentUserSays/userId={},domain={}",userId, domain);
		
		BaseResponse<Integer> resp = new BaseResponse<>();
		try {
			resp.setData(sdsService.deleteIntentUserSays(userId,domain));
		}catch(Exception e) {
			resp.setCode(IRestCodes.ERR_CODE_FAILURE);
			resp.setMsg(e.getMessage());
		}
		
		return resp;
	}
}
