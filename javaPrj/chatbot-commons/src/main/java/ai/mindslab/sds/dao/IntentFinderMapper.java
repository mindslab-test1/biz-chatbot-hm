package ai.mindslab.sds.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.sds.dao.data.SdsAgentData;
import ai.mindslab.sds.dao.data.SdsIntentData;
import ai.mindslab.sds.dao.data.SdsIntentUserSay;
import ai.mindslab.sds.dao.data.SdsProjectData;
import ai.mindslab.sds.dao.data.SdsUserData;

@Mapper
public interface IntentFinderMapper {

	public SdsUserData getUser(String userId);
	public long insertProject(SdsProjectData data);	
	public long insertIntent(SdsIntentData data);
	public long insertAgent(SdsAgentData data);
	public long insertIntentUsersay(SdsIntentUserSay data);
	public SdsProjectData getProject(String projectName);
	public SdsIntentData getIntent(Map<String, Object> param);
	public List<SdsIntentData> getIntentList(int projectSeq);
	public int deleteIntentUsersay(int intentSeq);
}
