package ai.mindslab.sds.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SdsIntentData implements Serializable{

	protected int seqNo;
	protected String intentName;
	protected int projectSeq;
	protected int agentSeq;
	
	public SdsIntentData() {
	}

	public int getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	public String getIntentName() {
		return intentName;
	}

	public void setIntentName(String intentName) {
		this.intentName = intentName;
	}

	public int getProjectSeq() {
		return projectSeq;
	}

	public void setProjectSeq(int projectSeq) {
		this.projectSeq = projectSeq;
	}

	public int getAgentSeq() {
		return agentSeq;
	}

	public void setAgentSeq(int agentSeq) {
		this.agentSeq = agentSeq;
	}

	@Override
	public String toString() {
		return "SdsIntentData [seqNo=" + seqNo + ", intentName=" + intentName + ", projectSeq=" + projectSeq
				+ ", agentSeq=" + agentSeq + "]";
	}
}
