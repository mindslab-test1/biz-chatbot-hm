package ai.mindslab.sds.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SdsProjectData implements Serializable{

	protected int seqNo;
	protected String projectName;
	protected String description;
	protected int userSeqNo;
	protected String itfYn;
	
	public SdsProjectData() {
	}

	public int getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getUserSeqNo() {
		return userSeqNo;
	}

	public void setUserSeqNo(int userSeqNo) {
		this.userSeqNo = userSeqNo;
	}

	public String getItfYn() {
		return itfYn;
	}

	public void setItfYn(String itfYn) {
		this.itfYn = itfYn;
	}

	@Override
	public String toString() {
		return "SdsProjectData [seqNo=" + seqNo + ", projectName=" + projectName + ", description=" + description
				+ ", userSeqNo=" + userSeqNo + ", itfYn=" + itfYn + "]";
	}
}
