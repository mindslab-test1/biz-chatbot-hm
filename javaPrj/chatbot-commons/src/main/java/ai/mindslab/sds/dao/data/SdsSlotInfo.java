package ai.mindslab.sds.dao.data;

import java.io.Serializable;

public class SdsSlotInfo implements Serializable{

	private static final long serialVersionUID = 1;
	
	protected String fullName;
	protected String className;
	protected String slotName;
	protected String slotType;
	protected String typeName;
	protected int projectSeq;
	protected String projectName;
	
	public SdsSlotInfo() {
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getSlotName() {
		return slotName;
	}

	public void setSlotName(String slotName) {
		this.slotName = slotName;
	}

	public String getSlotType() {
		return slotType;
	}

	public void setSlotType(String slotType) {
		this.slotType = slotType;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public int getProjectSeq() {
		return projectSeq;
	}

	public void setProjectSeq(int projectSeq) {
		this.projectSeq = projectSeq;
	}

	@Override
	public String toString() {
		return "SdsSlotData [fullName=" + fullName + ", className=" + className + ", slotName=" + slotName
				+ ", slotType=" + slotType + ", typeName=" + typeName + ", projectSeq=" + projectSeq + "]";
	}
}
