package ai.mindslab.sds.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SdsIntentUserSay implements Serializable{

	protected int seqNo;
	protected String say;
	protected int intentSeq;
	protected String slotSeqNo;
	protected String sayTag;
	protected String slotTag;
	protected String slotName;
	protected String slotMap;
	protected String slotMapda;
	
	public SdsIntentUserSay() {
	}

	public int getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	public String getSay() {
		return say;
	}

	public void setSay(String say) {
		this.say = say;
	}

	public int getIntentSeq() {
		return intentSeq;
	}

	public void setIntentSeq(int intentSeq) {
		this.intentSeq = intentSeq;
	}

	public String getSlotSeqNo() {
		return slotSeqNo;
	}

	public void setSlotSeqNo(String slotSeqNo) {
		this.slotSeqNo = slotSeqNo;
	}

	public String getSayTag() {
		return sayTag;
	}

	public void setSayTag(String sayTag) {
		this.sayTag = sayTag;
	}

	public String getSlotName() {
		return slotName;
	}

	public void setSlotName(String slotName) {
		this.slotName = slotName;
	}

	public String getSlotMap() {
		return slotMap;
	}

	public void setSlotMap(String slotMap) {
		this.slotMap = slotMap;
	}

	public String getSlotMapda() {
		return slotMapda;
	}

	public void setSlotMapda(String slotMapda) {
		this.slotMapda = slotMapda;
	}

	public String getSlotTag() {
		return slotTag;
	}

	public void setSlotTag(String slotTag) {
		this.slotTag = slotTag;
	}

	@Override
	public String toString() {
		return "SdsIntentUserSay [seqNo=" + seqNo + ", say=" + say + ", intentSeq=" + intentSeq + ", slotSeqNo="
				+ slotSeqNo + ", sayTag=" + sayTag + ", slotTag=" + slotTag + ", slotName=" + slotName + ", slotMap="
				+ slotMap + ", slotMapda=" + slotMapda + "]";
	}
}
