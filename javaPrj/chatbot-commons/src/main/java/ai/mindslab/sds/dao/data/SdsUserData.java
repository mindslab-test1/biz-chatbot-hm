package ai.mindslab.sds.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SdsUserData implements Serializable{

	protected int seqNo;
	protected String userId;
	protected String userPwd;
	protected String userName;
	protected String userType;
	protected String remarks;
	
	public SdsUserData() {
	}

	public int getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "SdsUserData [seqNo=" + seqNo + ", userId=" + userId + ", userPwd=" + userPwd + ", userName=" + userName
				+ ", userType=" + userType + ", remarks=" + remarks + "]";
	}
}
