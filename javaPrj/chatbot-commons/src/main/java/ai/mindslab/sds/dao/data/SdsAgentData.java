package ai.mindslab.sds.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SdsAgentData implements Serializable{

	protected int seqNo;
	protected String agentName;
	protected int resetSlot;
	protected int projectSeq;
	protected int leftPos;
	protected int topPos;
	protected String taskType;
	protected String relatedSlots;
	
	public SdsAgentData() {
	}

	public int getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public int getResetSlot() {
		return resetSlot;
	}

	public void setResetSlot(int resetSlot) {
		this.resetSlot = resetSlot;
	}

	public int getProjectSeq() {
		return projectSeq;
	}

	public void setProjectSeq(int projectSeq) {
		this.projectSeq = projectSeq;
	}

	public int getLeftPos() {
		return leftPos;
	}

	public void setLeftPos(int leftPos) {
		this.leftPos = leftPos;
	}

	public int getTopPos() {
		return topPos;
	}

	public void setTopPos(int topPos) {
		this.topPos = topPos;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public String getRelatedSlots() {
		return relatedSlots;
	}

	public void setRelatedSlots(String relatedSlots) {
		this.relatedSlots = relatedSlots;
	}

	@Override
	public String toString() {
		return "SdsAgentData [seqNo=" + seqNo + ", agentName=" + agentName + ", resetSlot=" + resetSlot
				+ ", projectSeq=" + projectSeq + ", leftPos=" + leftPos + ", topPos=" + topPos + ", taskType="
				+ taskType + ", relatedSlots=" + relatedSlots + "]";
	}
}
