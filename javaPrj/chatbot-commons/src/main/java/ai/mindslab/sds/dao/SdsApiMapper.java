package ai.mindslab.sds.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.sds.dao.data.SdsSlotInfo;

@Mapper
public interface SdsApiMapper {

	public List<SdsSlotInfo> getSlotInfo(int projectSeq);
	public List<SdsSlotInfo> getSlotInfoByName(String projectName);
}
