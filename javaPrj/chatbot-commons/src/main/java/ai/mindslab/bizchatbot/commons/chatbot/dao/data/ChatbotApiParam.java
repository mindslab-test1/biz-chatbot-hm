package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ChatbotApiParam implements Serializable{
	
	protected String paramType;//header or param
	protected String paramName;
	protected String paramValue;
	
	public ChatbotApiParam() {
	}
	
	public ChatbotApiParam(String paramType, String paramName, String paramValue) {
		this.paramType = paramType;
		this.paramName = paramName;
		this.paramValue = paramValue;
	}

	public String getParamType() {
		return paramType;
	}

	public void setParamType(String paramType) {
		this.paramType = paramType;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	@Override
	public String toString() {
		return "ChatbotApiParam [paramType=" + paramType + ", paramName=" + paramName + ", paramValue=" + paramValue
				+ "]";
	}
}
