package ai.mindslab.bizchatbot.commons.session.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichApi;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BasicQaOutWithDebug extends BasicQaOut{

	private static final long serialVersionUID = 1L;
	
	protected List<BasicQaDebugItem> list;
	protected int totalCount;
	protected int morphRange;
	protected String debugYn;
	protected int status;
	protected String result;
	protected int searchFlowType;
	protected String searchFlowTypeName;
	protected int usedDomainId;
	protected String usedDomainName;
	protected String questionMorph;
	//MOBIS
	protected String respCode;
	protected ChatbotRichContent richContent;
	protected ChatbotRichApi apiInfo;

	public BasicQaOutWithDebug() {
	}

	public List<BasicQaDebugItem> getList() {
		return list;
	}

	public void setList(List<BasicQaDebugItem> list) {
		this.list = list;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getMorphRange() {
		return morphRange;
	}

	public void setMorphRange(int morphRange) {
		this.morphRange = morphRange;
	}

	public String getDebugYn() {
		return debugYn;
	}

	public void setDebugYn(String debugYn) {
		this.debugYn = debugYn;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public int getSearchFlowType() {
		return searchFlowType;
	}

	public void setSearchFlowType(int searchFlowType) {
		this.searchFlowType = searchFlowType;
	}

	public String getSearchFlowTypeName() {
		return searchFlowTypeName;
	}

	public void setSearchFlowTypeName(String searchFlowTypeName) {
		this.searchFlowTypeName = searchFlowTypeName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getUsedDomainId() {
		return usedDomainId;
	}

	public void setUsedDomainId(int usedDomainId) {
		this.usedDomainId = usedDomainId;
	}

	public String getUsedDomainName() {
		return usedDomainName;
	}

	public void setUsedDomainName(String usedDomainName) {
		this.usedDomainName = usedDomainName;
	}

	public String getQuestionMorph() {
		return questionMorph;
	}

	public void setQuestionMorph(String questionMorph) {
		this.questionMorph = questionMorph;
	}
	
	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public ChatbotRichContent getRichContent() {
		return richContent;
	}

	public void setRichContent(ChatbotRichContent richContent) {
		this.richContent = richContent;
	}

	public ChatbotRichApi getApiInfo() {
		return apiInfo;
	}

	public void setApiInfo(ChatbotRichApi apiInfo) {
		this.apiInfo = apiInfo;
	}

	@Override
	public String toString() {
		return "BasicQaOutWithDebug [list=" + list + ", totalCount=" + totalCount + ", morphRange=" + morphRange
				+ ", debugYn=" + debugYn + ", status=" + status + ", result=" + result + ", searchFlowType="
				+ searchFlowType + ", searchFlowTypeName=" + searchFlowTypeName + ", usedDomainId=" + usedDomainId
				+ ", usedDomainName=" + usedDomainName + ", questionMorph=" + questionMorph + ", respCode=" + respCode
				+ ", richContent=" + richContent + ", apiInfo=" + apiInfo + "]";
	}
}
