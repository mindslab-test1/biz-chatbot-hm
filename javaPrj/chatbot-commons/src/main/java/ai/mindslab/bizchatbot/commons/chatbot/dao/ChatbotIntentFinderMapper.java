package ai.mindslab.bizchatbot.commons.chatbot.dao;

import java.util.HashMap;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.IntentFinderDomainRel;


@Mapper
public interface ChatbotIntentFinderMapper {

	public IntentFinderDomainRel getIntentFinderDomainRelation(HashMap<String,Object> params);
	
}
	