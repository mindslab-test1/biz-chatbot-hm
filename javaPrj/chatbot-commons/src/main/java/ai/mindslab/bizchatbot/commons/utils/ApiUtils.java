package ai.mindslab.bizchatbot.commons.utils;

import java.util.List;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiParam;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;

public class ApiUtils {

	//Global Entities Definition !!
	public static String global_userId = "#{USER_ID}";
	public static String global_userAddr = "#{USER_ADDR}";
	public static String global_chatbotId = "#{CHATBOT_ID}";

	public static ChatbotApiParam makeParamUserId(String userId) {
		return new ChatbotApiParam(IChatbotConst.API_HTTP_PARAM_TYPE_PARAM, global_userId, userId);
	}
	
	public static ChatbotApiParam makeParamUserAddr(String remoteAddr) {
		return new ChatbotApiParam(IChatbotConst.API_HTTP_PARAM_TYPE_PARAM, global_userAddr, remoteAddr);
	}
	

	public static void setGlobalSlots(List<ChatbotApiParam> params, String userId, String remoteAddr, int chatbotId) {
		if(params == null) {
			return;
		}
//		params.add(makeParamUserId(userId));
//		params.add(makeParamUserAddr(remoteAddr));
		for(ChatbotApiParam param:params) {
			if(param.getParamValue().equals(global_userId)) {
				param.setParamValue(userId);
			} else if(param.getParamValue().equals(global_userAddr)) {
				param.setParamValue(remoteAddr);
			} else if(param.getParamValue().equals(global_chatbotId)) {
				param.setParamValue("" + chatbotId);
			}
		}
	}
}
