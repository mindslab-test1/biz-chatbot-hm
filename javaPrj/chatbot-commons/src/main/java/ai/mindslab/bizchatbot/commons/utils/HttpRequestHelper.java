package ai.mindslab.bizchatbot.commons.utils;

import java.util.HashMap;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;


public class HttpRequestHelper {


	private static HttpRequestHelper instance = null;
	
	//for request cache
	private HashMap<String, HttpGet> mapHttpGet = new HashMap<String, HttpGet>();
	private HashMap<String, HttpPost> mapHttpPost = new HashMap<String, HttpPost>();
	private HashMap<String, HttpPut> mapHttpPut = new HashMap<String, HttpPut>();
	
	public static HttpRequestHelper getInstance(){
		if(instance == null){
			instance = new HttpRequestHelper();
		}
		return instance;
	}
	
	private HttpRequestHelper() {
		
	}
	
	public HttpGet httpGet(String url) {
		
//		HttpGet get = mapHttpGet.get(url);
		HttpGet get = null;
		if(get == null){
			get = new HttpGet(url);
//			mapHttpGet.put(url, get);
		}
		return get;
	}
	
	public HttpPost httpPost(String url) {
		
//		HttpPost post = mapHttpPost.get(url);
		HttpPost post = null;
		if(post == null){
			post = new HttpPost(url);
//			mapHttpPost.put(url, post);
		}
		return post;
	}
	
	public HttpPut httpPut(String url) {
		
//		HttpPut put = mapHttpPut.get(url);
		HttpPut put = null;
		if(put == null){
			put = new HttpPut(url);
//			mapHttpPut.put(url, put);
		}
		return put;
	}

}
