package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserData implements Serializable{

	private static final long serialVersionUID = 1L;
	private String userName;
	private String userId;
	private String token;
	private String userDivCode;
	private String userDivNm;
	private String confirmYn;
	
	public UserData() {};

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserDivCode() {
		return userDivCode;
	}

	public void setUserDivCode(String userDivCode) {
		this.userDivCode = userDivCode;
	}

	public String getUserDivNm() {
		return userDivNm;
	}

	public void setUserDivNm(String userDivNm) {
		this.userDivNm = userDivNm;
	}

	public void setConfirmYn(String confirmYn){ this.confirmYn = confirmYn;};

	public String getConfirmYn() { return this.confirmYn;};
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
