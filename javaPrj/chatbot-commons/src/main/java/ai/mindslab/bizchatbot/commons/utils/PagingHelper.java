package ai.mindslab.bizchatbot.commons.utils;

import java.util.HashMap;

import org.springframework.stereotype.Service;

@Service
public class PagingHelper {
	
	public PageInfo pageInfo(int pg, int pgSize) {
		return new PageInfo(pg, pgSize);
	}
	
	public HashMap<String,Object> setPageInfo(HashMap<String,Object> params, int pg, int pgSize) {
		HashMap<String,Object> p;
		if(params == null) {
			p = new HashMap<>();
		}else {
			p = params;
		}
		PageInfo pi = this.pageInfo(pg, pgSize);
		p.put("startIndex", pi.getStartIndex());
		p.put("pgSize", pgSize);
		return p;
	}

	public static class PageInfo{
		private int startIndex;
		private int pgSize;
		
		public PageInfo() {
		}
		public PageInfo(int pg, int pgSize) {
			this.pgSize = pgSize;
			this.startIndex = (pg - 1) * pgSize;
		}
		public int getStartIndex() {
			return startIndex;
		}
		public void setStartIndex(int startIndex) {
			this.startIndex = startIndex;
		}
		public int getPgSize() {
			return pgSize;
		}
		public void setPgSize(int pgSize) {
			this.pgSize = pgSize;
		}
	}

}
