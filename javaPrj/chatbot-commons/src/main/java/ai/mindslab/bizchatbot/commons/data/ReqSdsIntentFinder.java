package ai.mindslab.bizchatbot.commons.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

@SuppressWarnings("serial")
public class ReqSdsIntentFinder implements Serializable{

	private String caller;
	private List<Integer> botList;
	
	public ReqSdsIntentFinder() {
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public List<Integer> getBotList() {
		if(botList == null) {
			botList = new ArrayList<Integer>();
		}
		return botList;
	}

	public void setBotList(List<Integer> botList) {
		this.botList = botList;
	}

	@Override
	public String toString() {
		return caller + "/"+ StringUtils.join(getBotList().toArray(new Integer[getBotList().size()]),",");
	}
}
