package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.SDS_STATUS;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SdsData implements Serializable{

	private static final long serialVersionUID = 1L;
	protected String request;
	protected String projectName;
	protected String userName;
	protected int projectPort;
	protected String daemonLogFileName;
	protected String dialogSystemEngine = "dial";
	protected String dialogSystemDic = "nlp_dict";
	protected String guideMode = "no";
	protected String uid;
	
	protected SDS_STATUS status = SDS_STATUS.BEFORE_OPEN;
	
	protected String sdsIp;
	
	public SdsData() {
	}
	
	public SdsData(String uid) {
		this.uid = uid;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getProjectPort() {
		return projectPort;
	}

	public void setProjectPort(int projectPort) {
		this.projectPort = projectPort;
	}

	public String getDaemonLogFileName() {
		return daemonLogFileName;
	}

	public void setDaemonLogFileName(String daemonLogFileName) {
		this.daemonLogFileName = daemonLogFileName;
	}

	public String getDialogSystemEngine() {
		return dialogSystemEngine;
	}

	public void setDialogSystemEngine(String dialogSystemEngine) {
		this.dialogSystemEngine = dialogSystemEngine;
	}

	public String getDialogSystemDic() {
		return dialogSystemDic;
	}

	public void setDialogSystemDic(String dialogSystemDic) {
		this.dialogSystemDic = dialogSystemDic;
	}

	public String getGuideMode() {
		return guideMode;
	}

	public void setGuideMode(String guideMode) {
		this.guideMode = guideMode;
	}

	public String getSdsIp() {
		return sdsIp;
	}

	public void setSdsIp(String sdsIp) {
		this.sdsIp = sdsIp;
	}
	
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public SDS_STATUS getStatus() {
		return status;
	}

	public void setStatus(SDS_STATUS status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SdsData [request=" + request + ", projectName=" + projectName + ", userName=" + userName
				+ ", projectPort=" + projectPort + ", daemonLogFileName=" + daemonLogFileName + ", dialogSystemEngine="
				+ dialogSystemEngine + ", dialogSystemDic=" + dialogSystemDic + ", guideMode=" + guideMode + ", uid="
				+ uid + ", status=" + status + ", sdsIp=" + sdsIp + "]";
	}
}
