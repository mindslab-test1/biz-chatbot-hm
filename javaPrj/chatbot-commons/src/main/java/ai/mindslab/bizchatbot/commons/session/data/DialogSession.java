package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DialogSession implements Serializable{

	private static final long serialVersionUID = 2L;
	
	protected String sessionId;
	protected UserData userData;
	protected DialogData dialog;
	protected String remoteAddr;
	protected SdsData sdsData;
	protected SdsData itfData;
	protected DialogError dlgError;
	
	protected DialogProcessing processing;
	protected Boolean showDebug;
	protected String currFilter;
	
	protected SdsData resetSds;
//	protected AccessInfo accessInfo;
	
	@JsonIgnore
	public Date lastModified;
	
	public boolean active = true;
	
	public DialogSession() {}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}
	
	public DialogData getDialog() {
		return dialog;
	}

	public void setDialog(DialogData dialog) {
		this.dialog = dialog;
	}
	
	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getRemoteAddr() {
		return remoteAddr;
	}

	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}

	public SdsData getSdsData() {
		return sdsData;
	}

	public void setSdsData(SdsData sdsData) {
		this.sdsData = sdsData;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public DialogProcessing getProcessing() {
		return processing;
	}

	public void setProcessing(DialogProcessing processing) {
		this.processing = processing;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public SdsData getItfData() {
		return itfData;
	}

	public void setItfData(SdsData itfData) {
		this.itfData = itfData;
	}

	public DialogError getDlgError() {
		return dlgError;
	}

	public void setDlgError(DialogError dlgError) {
		this.dlgError = dlgError;
	}

	public Boolean getShowDebug() {
		return showDebug;
	}

	public void setShowDebug(Boolean showDebug) {
		this.showDebug = showDebug;
	}

	public String getCurrFilter() {
		return currFilter;
	}

	public void setCurrFilter(String currFilter) {
		this.currFilter = currFilter;
	}

	public SdsData getResetSds() {
		return resetSds;
	}

	public void setResetSds(SdsData resetSds) {
		this.resetSds = resetSds;
	}

//	public AccessInfo getAccessInfo() {
//		return accessInfo;
//	}
//
//	public void setAccessInfo(AccessInfo accessInfo) {
//		this.accessInfo = accessInfo;
//	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
