package ai.mindslab.bizchatbot.commons.session.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ai.mindslab.bizchatbot.commons.data.BaseResponse;

@SuppressWarnings("serial")
public class BasicQaResponse extends BaseResponse<BasicQaOutWithDebug>{

	public BasicQaResponse() {
	}

	@Override
	public String toString() {
		return "BasicQaResponse ["+ getData()!=null?getData().toString():"" +"]";
	}

	
}
