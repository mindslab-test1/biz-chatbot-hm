package ai.mindslab.bizchatbot.commons.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import ai.mindslab.bizchatbot.commons.data.BaseResponse;

public class JsonUtils {

//	private static Gson gson = null;
	private static ObjectMapper mapper = null;
	
	static{
		//gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
		mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
		mapper.setTimeZone(TimeZone.getTimeZone("GMT+09:00"));
	}
	
	public static <T> T fromJson(String jsonString, Class<T> clazz) {
		
		String l_jsonString = jsonString;
		if(l_jsonString == null){
			l_jsonString = "{}";
		}
//		T out = gson.fromJson(l_jsonString, clazz);
		T out = null;
		try {
			out = mapper.readerFor(clazz).readValue(jsonString);
		} catch (JsonProcessingException e) {
		} catch (IOException e) {
		}
		
		return out;
	}
	
	public static String toJson(Object obj) {
		if(obj == null){
			return "{}";
		}
		//return gson.toJson(obj);
		String out = "";
		try {
			out = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
		}
		return out;
	}
	
	public static <T> BaseResponse<T> fromJsonRespUtter(String jsonString, Class<T> clazz) {
		
		String l_jsonString = jsonString;
		if(l_jsonString == null){
			l_jsonString = "{}";
		}
		BaseResponse<T> out = null;
		try {
//			out = mapper.readValue(jsonString, new TypeReference<BaseResponse<T>>() {});
			JavaType type = mapper.getTypeFactory().constructParametricType(BaseResponse.class, clazz);
			out =  mapper.readValue(jsonString, type);
		} catch (JsonProcessingException e) {
		} catch (IOException e) {
		}
		
		return out;
	}

}
