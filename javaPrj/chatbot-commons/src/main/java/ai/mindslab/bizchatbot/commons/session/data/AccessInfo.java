package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

public class AccessInfo extends UserData{

	protected String remoteAddress;
	protected String deptCd;
	protected String deptName;
	
	public AccessInfo() {
	}

	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}

	public String getDeptCd() {
		return deptCd;
	}

	public void setDeptCd(String deptCd) {
		this.deptCd = deptCd;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	@Override
	public String toString() {
		return "AccessInfo [remoteAddress=" + remoteAddress + ", deptCd=" + deptCd + ", deptName=" + deptName
				+ ", toString()=" + super.toString() + "]";
	}
}
