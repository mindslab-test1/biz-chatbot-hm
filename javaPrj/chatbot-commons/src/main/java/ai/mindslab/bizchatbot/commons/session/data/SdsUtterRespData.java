package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SdsUtterRespData implements Serializable{

	protected String system;
	protected String slu;
	protected String da;
	protected String isDialogEnd;
	protected String slu_weight;
	protected String slu_similar_str;
	protected String debug;
	protected String err_msg;
	protected String rx;
	protected String time;
	protected String deamon_log_file_name;
	protected String cmd;
	
	@JsonProperty("_DIALOG_SYSTEM_ENGINE_")
	protected String engine;
	@JsonProperty("_DIALOG_SYSTEM_SERV_PORT_")
	protected String servPort;
	
	public SdsUtterRespData() {
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getSlu() {
		return slu;
	}

	public void setSlu(String slu) {
		this.slu = slu;
	}

	public String getDa() {
		return da;
	}

	public void setDa(String da) {
		this.da = da;
	}

	public String getIsDialogEnd() {
		return isDialogEnd;
	}

	public void setIsDialogEnd(String isDialogEnd) {
		this.isDialogEnd = isDialogEnd;
	}

	public String getSlu_weight() {
		return slu_weight;
	}

	public void setSlu_weight(String slu_weight) {
		this.slu_weight = slu_weight;
	}

	public String getSlu_similar_str() {
		return slu_similar_str;
	}

	public void setSlu_similar_str(String slu_similar_str) {
		this.slu_similar_str = slu_similar_str;
	}

	public String getDebug() {
		return debug;
	}

	public void setDebug(String debug) {
		this.debug = debug;
	}

	public String getErr_msg() {
		return err_msg;
	}

	public void setErr_msg(String err_msg) {
		this.err_msg = err_msg;
	}

	public String getRx() {
		return rx;
	}

	public void setRx(String rx) {
		this.rx = rx;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDeamon_log_file_name() {
		return deamon_log_file_name;
	}

	public void setDeamon_log_file_name(String deamon_log_file_name) {
		this.deamon_log_file_name = deamon_log_file_name;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public String getServPort() {
		return servPort;
	}

	public void setServPort(String servPort) {
		this.servPort = servPort;
	}

	@Override
	public String toString() {
		return "SdsUtterRespData [system=" + system + ", slu=" + slu + ", da=" + da + ", isDialogEnd=" + isDialogEnd
				+ ", slu_weight=" + slu_weight + ", slu_similar_str=" + slu_similar_str + ", debug=" + debug
				+ ", err_msg=" + err_msg + ", rx=" + rx + ", time=" + time + ", deamon_log_file_name="
				+ deamon_log_file_name + ", cmd=" + cmd + ", engine=" + engine + ", servPort=" + servPort + "]";
	}
}
