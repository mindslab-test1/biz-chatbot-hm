package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("serial")
@JsonIgnoreProperties({"createdDtm", "updatedDtm", "creatorId", "updatorId", "rcId"})
public class ChatbotButton implements Serializable{

	private int rcId;
	private int btnOrder;
	private String title;
	private String userUtter;
	
	private Date createdDtm;
	private Date updatedDtm;
	private String creatorId;
	private String updatorId;
	
	public ChatbotButton() {
	}

	public int getRcId() {
		return rcId;
	}

	public void setRcId(int rcId) {
		this.rcId = rcId;
	}

	public int getBtnOrder() {
		return btnOrder;
	}

	public void setBtnOrder(int btnOrder) {
		this.btnOrder = btnOrder;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUserUtter() {
		return userUtter;
	}

	public void setUserUtter(String userUtter) {
		this.userUtter = userUtter;
	}

	public Date getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(Date createdDtm) {
		this.createdDtm = createdDtm;
	}

	public Date getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(Date updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	@Override
	public String toString() {
		return "ChatbotButton [rcId=" + rcId + ", btnOrder=" + btnOrder + ", title=" + title + ", userUtter="
				+ userUtter + ", createdDtm=" + createdDtm + ", updatedDtm=" + updatedDtm + ", creatorId=" + creatorId
				+ ", updatorId=" + updatorId + "]";
	}
}
