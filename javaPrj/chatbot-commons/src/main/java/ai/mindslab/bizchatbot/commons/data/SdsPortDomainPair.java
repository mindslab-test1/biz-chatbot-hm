package ai.mindslab.bizchatbot.commons.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SdsPortDomainPair implements Serializable{

	private int port;
	private String domain;
	private int botId;
	private String errMsg;
	
	public SdsPortDomainPair() {
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	@Override
	public String toString() {
		return "SdsPortDomainPair [port=" + port + ", domain=" + domain + ", botId=" + botId + ", errMsg=" + errMsg
				+ "]";
	}
}
