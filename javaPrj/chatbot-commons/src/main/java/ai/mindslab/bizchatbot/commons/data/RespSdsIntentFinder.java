package ai.mindslab.bizchatbot.commons.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class RespSdsIntentFinder implements Serializable{

	private List<SdsPortDomainPair> successList;
	private List<SdsPortDomainPair> failureList;
	
	public RespSdsIntentFinder() {
	}

	public List<SdsPortDomainPair> getSuccessList() {
		if(successList == null) {
			successList = new ArrayList<SdsPortDomainPair>();
		}
		return successList;
	}

	public void setSuccessList(List<SdsPortDomainPair> successList) {
		this.successList = successList;
	}

	public List<SdsPortDomainPair> getFailureList() {
		if(failureList == null) {
			failureList = new ArrayList<SdsPortDomainPair>();
		}
		return failureList;
	}

	public void setFailureList(List<SdsPortDomainPair> failureList) {
		this.failureList = failureList;
	}

	@Override
	public String toString() {
		return "RespSdsIntentFinder [successList=" + successList + ", failureList=" + failureList + "]";
	}
	
}
