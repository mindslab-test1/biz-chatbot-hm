package ai.mindslab.bizchatbot.commons.codes;

/**
 * Created by jaeheoncho on 2018. 4. 20..
 */
public interface IChatbotConst {
	
	public static final String MAP_NAME_SESSION = "sessionss";
	public static final String MAP_NAME_DIALOG = "dialogs";
	public static final String MAP_NAME_CONFIG = "objcache";//general values
	public static final String MAP_NAME_BOTCONFIG = "botconfig";//bot config
	public static final String MAP_NAME_USER = "user";//bot config
	
	public static final String TOPIC_DIALOG_PREFIX = "^dialog$";
	
	public static final String FILTER_STEP_INIT = "InitGreeting";
	
	public static final int ERR_CODE_INVALID_SESSION_ID = 800000;
    public static final String ERR_MSG_INVALID_SESSION_ID = "INVALID SESSION ID";
    public static final int ERR_CODE_INVALID_CHATBOT = 800001;
    public static final String ERR_MSG_INVALID_CHATBOT = "INVALID CHATBOT";
    
    public static final String KEY_PRJ_PORT = "project_port";
    public static final String KEY_PRJ_PORT_ITF = "project_port_itf";
    
    public static final String CLIENT_TYPE_BASE = "base";
    public static final String CLIENT_TYPE_NG = "ng";
	
	public enum ACCESS_CHANNEL{
		WEB_BROWSER,
		ANDROID,
		IOS
	}
	
	public enum DIALOG_SPEAKER{
		CHATBOT,
		USER
	}
	
	//여기서 확장 
	public enum TALK_RESULT{
		SUCCESS,
		SYS_FAILURE,
		SDS_FAILURE,
		UNKNOWN
	}
	
	public enum SDS_STATUS{
		BEFORE_OPEN,
		OPEN,
		CLOSED
	}
	
	//mobis specific feature -> common code확인. (database)
	public enum SYS_UTTER_TYPE{
		NORMAL("SC0001"),
		GREETING("SC0002"),
		SHUTTLE_BUS("SC0003"),
		CAFETERIA_MENU("SC0004"),
		EMPLOYEE_INFO("SC0005"),
		BUSSTOP_LOC("SC0006"),
		EMAIL_CHECK("SC0007"),
		APPROVAL_CHECK("SC0008"),
		BQA_RECOMMEND("SC0009"),
		WEATHER("SC0010"),
		IMOBIS_MENU("SC0011"),
		QMS_MENU("SC0012");

		private String code;
		SYS_UTTER_TYPE(String code){
			this.code = code;
		}
		
		public String getCode() {
			return this.code;
		}
		
		public static SYS_UTTER_TYPE getEnum(String code) {
			for(SYS_UTTER_TYPE val:SYS_UTTER_TYPE.values()) {
				if(val.getCode().equals(code)) {
					return val;
				}
			}
			return NORMAL;
		}
	}
	
	public static final String API_HTTP_METHOD_GET = "GET";
	public static final String API_HTTP_METHOD_POST = "POST";
	
	public static final String API_HTTP_PARAM_TYPE_HEADER = "PT0002";
	public static final String API_HTTP_PARAM_TYPE_PARAM = "PT0001";
	
	public static final String RICH_CONTENT_TYPE_BUTTON = 	"RT0001";
	public static final String RICH_CONTENT_TYPE_IMAGE = 		"RT0002";
	public static final String RICH_CONTENT_TYPE_CAROUSEL = 	"RT0003";
	public static final String RICH_CONTENT_TYPE_API = 		"RT9001";
	public static final String RICH_CONTENT_TYPE_NONE = 		"NONE";
	
	public static final String MSG_TYPE_GREETING = "MT0001";
	public static final String MSG_TYPE_UNKNOWN = "MT0002";
	public static final String MSG_TYPE_SDS_RECONFIRM = "MT0003";
	
	public static final String SDS_TERMINATION_BY_USER = "TR0001";
	public static final String SDS_TERMINATION_BY_SYSTEM = "TR0002";
	
	public static final String PREFIX_SDS_SYSTEM = "system: ";
	public static final String PREFIX_SYSOUT = "__RESCODE_";
	public static final String PREFIX_INPUTPARAM = "#{";
	
	public static final String DIR_RICH_IMG = "image";
	public static final String DIR_RICH_CAROUSEL = "carousel";
	
	public static final String SCENARIO_CHANGE_TAG_ST = "<RECONFIRM>";
	public static final String SCENARIO_CHANGE_TAG_ED = "</RECONFIRM>";
}
