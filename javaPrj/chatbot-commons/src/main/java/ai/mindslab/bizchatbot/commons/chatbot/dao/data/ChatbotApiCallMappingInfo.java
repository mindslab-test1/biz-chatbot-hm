package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

@SuppressWarnings("serial")
public class ChatbotApiCallMappingInfo implements Serializable{

	protected int mappingId;
	protected int botId;
	protected String sdsResultCd;
	protected int ifId;
	protected List<ChatbotApiResultMappingInfo> resultMappingList;
	
	@JsonIgnore
	protected String createdDtm;
	@JsonIgnore
	protected String updatedDtm;
	@JsonIgnore
	protected String creatorId;
	@JsonIgnore
	protected String updatorId;
	
	public ChatbotApiCallMappingInfo() {
	}

	public int getMappingId() {
		return mappingId;
	}

	public void setMappingId(int mappingId) {
		this.mappingId = mappingId;
	}

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public String getSdsResultCd() {
		return sdsResultCd;
	}

	public void setSdsResultCd(String sdsResultCd) {
		this.sdsResultCd = sdsResultCd;
	}

	public int getIfId() {
		return ifId;
	}

	public void setIfId(int ifId) {
		this.ifId = ifId;
	}

	public List<ChatbotApiResultMappingInfo> getResultMappingList() {
		if(resultMappingList == null) {
			resultMappingList = new ArrayList<>();
		}
		return resultMappingList;
	}

	public void setResultMappingList(List<ChatbotApiResultMappingInfo> resultMappingList) {
		this.resultMappingList = resultMappingList;
	}

	public String getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	@Override
	public String toString() {
		return "ChatbotApiCallMappingInfo [mappingId=" + mappingId + ", botId=" + botId + ", sdsResultCd=" + sdsResultCd
				+ ", ifId=" + ifId + ", resultMappingList=" + resultMappingList + ", createdDtm=" + createdDtm
				+ ", updatedDtm=" + updatedDtm + ", creatorId=" + creatorId + ", updatorId=" + updatorId + "]";
	}

}
