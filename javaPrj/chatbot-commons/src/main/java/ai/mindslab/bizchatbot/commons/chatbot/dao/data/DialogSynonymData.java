package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class DialogSynonymData implements Serializable{

	private int synonymId;
	private int workspaceId;
	private String word;
	private String synonymList;
	
	public DialogSynonymData() {
	}

	public int getSynonymId() {
		return synonymId;
	}

	public void setSynonymId(int synonymId) {
		this.synonymId = synonymId;
	}

	public int getWorkspaceId() {
		return workspaceId;
	}

	public void setWorkspaceId(int workspaceId) {
		this.workspaceId = workspaceId;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getSynonymList() {
		return synonymList;
	}

	public void setSynonymList(String synonymList) {
		this.synonymList = synonymList;
	}

	@Override
	public String toString() {
		return "DialogSynonymData [synonymId=" + synonymId + ", workspaceId=" + workspaceId + ", word=" + word
				+ ", synonymList=" + synonymList + "]";
	}

}
