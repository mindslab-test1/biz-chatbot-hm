package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties({"createdDtm", "updatedDtm", "creatorId", "updatorId", "rcId"})
public class ChatbotRichCarousel implements Serializable{

	protected int rcId;
	protected int carouselOrder;
	protected String imagePath;
	protected String title;
	protected String subTitle;
	protected String utterTypeCd;
	protected String userUtter;
	
//	@JsonIgnore
	protected Date createdDtm;
//	@JsonIgnore
	protected Date updatedDtm;
//	@JsonIgnore
	protected String creatorId;
//	@JsonIgnore
	protected String updatorId;
	
	public ChatbotRichCarousel() {
	}

	public int getRcId() {
		return rcId;
	}

	public void setRcId(int rcId) {
		this.rcId = rcId;
	}

	public int getCarouselOrder() {
		return carouselOrder;
	}

	public void setCarouselOrder(int carouselOrder) {
		this.carouselOrder = carouselOrder;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getUtterTypeCd() {
		return utterTypeCd;
	}

	public void setUtterTypeCd(String utterTypeCd) {
		this.utterTypeCd = utterTypeCd;
	}

	public String getUserUtter() {
		return userUtter;
	}

	public void setUserUtter(String userUtter) {
		this.userUtter = userUtter;
	}

	public Date getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(Date createdDtm) {
		this.createdDtm = createdDtm;
	}

	public Date getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(Date updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	@Override
	public String toString() {
		return "ChatbotRichCarousel [rcId=" + rcId + ", carouselOrder=" + carouselOrder + ", imagePath=" + imagePath
				+ ", title=" + title + ", subTitle=" + subTitle + ", utterTypeCd=" + utterTypeCd + ", userUtter="
				+ userUtter + ", createdDtm=" + createdDtm + ", updatedDtm=" + updatedDtm + ", creatorId=" + creatorId
				+ ", updatorId=" + updatorId + "]";
	}
}
