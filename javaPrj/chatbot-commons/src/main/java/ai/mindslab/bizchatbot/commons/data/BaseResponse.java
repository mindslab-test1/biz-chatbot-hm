package ai.mindslab.bizchatbot.commons.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import ai.mindslab.bizchatbot.commons.codes.IRestCodes;

/**
 * Created by jaeheoncho on 2018. 4. 23..
 */
@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BaseResponse<T> implements Serializable{
    private int code;
    private String msg;
    private T data;

    public BaseResponse(){
    		this.code = IRestCodes.ERR_CODE_SUCCESS;
    		this.msg = IRestCodes.ERR_MSG_SUCCESS;
    }
    public BaseResponse(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
