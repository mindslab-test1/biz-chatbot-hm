package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BasicQaOut implements Serializable{

	protected int answerId;
	protected String answer;
	protected int questionId;
	protected String question;
	protected String useYn;
	protected String createdDtm;
	protected String updatedDtm;
	protected String creatorId;
	protected String updatorId;
	
	public BasicQaOut() {
	}

	public int getAnswerId() {
		return answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	@Override
	public String toString() {
		return "BasicQaOut [answerId=" + answerId + ", answer=" + answer + ", questionId=" + questionId + ", question="
				+ question + ", useYn=" + useYn + ", createdDtm=" + createdDtm + ", updatedDtm=" + updatedDtm
				+ ", creatorId=" + creatorId + ", updatorId=" + updatorId + "]";
	}
	
}
