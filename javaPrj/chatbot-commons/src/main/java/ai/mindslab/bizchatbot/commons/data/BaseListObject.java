package ai.mindslab.bizchatbot.commons.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@SuppressWarnings("serial")
public class BaseListObject<T> implements Serializable{

	protected long totalCount = 0;
	protected List<T> list;
	
	public BaseListObject() {
	}
	
	public BaseListObject(int tot, List<T> list) {
		this.totalCount = tot;
		this.list = list;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public List<T> getList() {
		if(list == null) {
			list = new ArrayList<>();
		}
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "BaseListObject [totalCount=" + totalCount + ", list=" + list + "]";
	}
}
