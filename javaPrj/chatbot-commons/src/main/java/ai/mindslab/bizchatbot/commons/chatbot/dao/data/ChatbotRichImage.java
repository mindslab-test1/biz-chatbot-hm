package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties({"createdDtm", "updatedDtm", "creatorId", "updatorId", "rcId"})
public class ChatbotRichImage implements Serializable{

	protected int rcId;
	protected String imagePath;
	protected String imageText;
	protected String linkUrl;

//	@JsonIgnore
	protected Date createdDtm;
//	@JsonIgnore
	protected Date updatedDtm;
//	@JsonIgnore
	protected String creatorId;
//	@JsonIgnore
	protected String updatorId;
	
	public ChatbotRichImage() {
	}

	public int getRcId() {
		return rcId;
	}

	public void setRcId(int rcId) {
		this.rcId = rcId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getImageText() {
		return imageText;
	}

	public void setImageText(String imageText) {
		this.imageText = imageText;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public Date getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(Date createdDtm) {
		this.createdDtm = createdDtm;
	}

	public Date getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(Date updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	@Override
	public String toString() {
		return "ChatbotRichImage [rcId=" + rcId + ", imagePath=" + imagePath + ", imageText=" + imageText
				+ ", linkUrl=" + linkUrl + ", createdDtm=" + createdDtm + ", updatedDtm=" + updatedDtm
				+ ", creatorId=" + creatorId + ", updatorId=" + updatorId + "]";
	}
}
