package ai.mindslab.bizchatbot.commons.data;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.ACCESS_CHANNEL;
import ai.mindslab.bizchatbot.commons.session.data.UserData;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;


@SuppressWarnings("serial")
public class ReqDialog implements Serializable{

	protected int chatbotId;
	protected String sessionId;
	protected ACCESS_CHANNEL accessChannel;
	protected UserData user;
	protected UtterData utter;
	protected Boolean showDebug = false;;
	
	public ReqDialog() {
		// TODO Auto-generated constructor stub
	}

	public int getChatbotId() {
		return chatbotId;
	}

	public void setChatbotId(int chatbotId) {
		this.chatbotId = chatbotId;
	}

	public ACCESS_CHANNEL getAccessChannel() {
		return accessChannel;
	}

	public void setAccessChannel(ACCESS_CHANNEL accessChannel) {
		this.accessChannel = accessChannel;
	}
	
	public UserData getUser() {
		return user;
	}

	public void setUser(UserData user) {
		this.user = user;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public UtterData getUtter() {
		return utter;
	}

	public void setUtter(UtterData utter) {
		this.utter = utter;
	}

	public Boolean getShowDebug() {
		return showDebug;
	}

	public void setShowDebug(Boolean showDebug) {
		this.showDebug = showDebug;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
