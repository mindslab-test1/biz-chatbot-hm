package ai.mindslab.bizchatbot.commons.session;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.hazelcast.core.ITopic;

import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotRichContentMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.DialogHistoryMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotDomainSimple;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotMsg;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.SdsProjectPortHis;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.interfaces.IChatbotInterface;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.UserData;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.session.listener.DialogReplyListenerCallable;
import ai.mindslab.bizchatbot.commons.utils.DateUtils;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.commons.utils.JsonUtils;

@Service
public class DialogService implements IChatbotConst, IRestCodes{

	private Logger logger = LoggerFactory.getLogger(DialogService.class);
	
	@Autowired
	private SessionService session;
	
	@Autowired
	@Qualifier("hazelClientCommon")
	private HazelClientCommon hazel;
	
	@Autowired
	private DialogHistoryMapper his;
	
	@Autowired
	private ChatbotMapper chatbot;
	
	public DialogService() {
		// TODO Auto-generated constructor stub
	}

	public Callable<BaseResponse<UtterData>> sendText(UtterData utter, IChatbotInterface chatbotIf) throws Exception{
		logger.info("sendText/START/{}",utter);
		
		try {
			if(utter == null) {
				return new Callable<BaseResponse<UtterData>>(){

					@Override
					public BaseResponse<UtterData> call() throws Exception {
						throw new BizChatbotException(ERR_CODE_PARAMS_INVALID, "User Utter is Empty");
					}
				};
			}
			
			Map<String,DialogSession> map = hazel.getClient().getMap(MAP_NAME_SESSION);
			DialogSession dialog = map.get(utter.getSessionId());
			if(dialog == null) {
				return new Callable<BaseResponse<UtterData>>(){
					@Override
					public BaseResponse<UtterData> call() throws Exception {
						throw new BizChatbotException(ERR_CODE_INVALID_SESSION_ID, ERR_MSG_INVALID_SESSION_ID);
					}
				};
			}
			
			//QQQ container's logic?
			
			//store utter history
			//int lastSeq = dialog.getDialog().getConversation().size()==0?1:dialog.getDialog().getConversation().get(dialog.getDialog().getConversation().size()-1).getDialogSeq();
			utter.setDialogSeq(DialogUtils.getNextSeq(dialog));
			utter.setTimestamp(DateUtils.getNow());
			utter.setBotId(dialog.getDialog().getBotId());
			dialog.getDialog().getConversation().add(utter);
			map.put(utter.getSessionId(), dialog);
			
			insertHistory(utter.getBotId(), DIALOG_SPEAKER.USER, utter.getUserId(), utter.getSessionId(), utter.getSentence(), utter.getDialogSeq(), "", "", "", "");
			
			//response
//			DialogReplyListenerCallable callable = new DialogReplyListenerCallable();
//			ITopic<UtterData> topic = hazel.getClient().getReliableTopic(DialogUtils.getDialogTopic(utter, DIALOG_SPEAKER.USER));
//			topic.addMessageListener(callable);
			Callable<BaseResponse<UtterData>> callable = new Callable<BaseResponse<UtterData>>() {

				@Override
				public BaseResponse<UtterData> call() throws Exception {
					//QQQ move all logic here
					
					//send to chatbot container
					BaseResponse<UtterData> ifResp = chatbotIf.sendUserUtter(utter.getSessionId());
					return ifResp;
				}
				
			};
			return callable;
		}finally {
			logger.info("sendText/END");
		}
	}
	
	
	public long insertHistory(UtterData utter) throws Exception{
		if(utter != null && utter.getRichContent() != null) {
			utter.setContentJson(JsonUtils.toJson(utter.getRichContent()));
		}
		long nRet = his.insertDialog(utter);
		return nRet;
	}
	
	public long insertHistory(int botId, DIALOG_SPEAKER speaker, String speakerId, String sessionId, String text, int dialogSeq, String contentJson, String hisDomain, String msgTypeCd, String dlgStep) throws Exception {
		logger.info("inserHistory/START/{},{},{},{},{},{},{},{},{}",botId, speaker, speakerId, sessionId, text, contentJson, hisDomain, msgTypeCd, dlgStep);
		long nRet = 0;
		try {
			UtterData utter = new UtterData();
			utter.setBotId(botId);
			utter.setUserId(speakerId);
			utter.setSessionId(sessionId);
			utter.setSpeaker(speaker);
			utter.setSentence(text);
//			utter.setTimestamp(DateUtils.getNow());
			utter.setDialogSeq(dialogSeq);
			utter.setContentJson(contentJson);
			utter.setHisDomain(hisDomain);
			utter.setMsgTypeCd(msgTypeCd);
			utter.setCurrStep(dlgStep);
			nRet = his.insertDialog(utter);
		}catch(Exception e) {
			logger.warn("inserHistory/{}",e.getMessage());
			throw e;
		}finally {
			logger.info("insertHistory/END");	
		}
		return nRet;
	}
	
	public long insertBotUtterHistory(int botId, String userId, String sessionId, String text, int dialogSeq, String contentJson, String hisDomain, String msgTypeCd, String dlgStep) throws Exception {
		return insertHistory(botId, DIALOG_SPEAKER.CHATBOT, userId, sessionId, text, dialogSeq, contentJson, hisDomain, msgTypeCd, dlgStep);
	}
	
	public long insertBotUtterHistory(UtterData utter) throws Exception {
		if(utter == null) {
			return 0;
		}
		utter.setSpeaker(DIALOG_SPEAKER.CHATBOT);
		return insertHistory(utter);
	}
	
	public long insertUserUtterHistory(int botId, String userId, String sessionId, String text, int dialogSeq) throws Exception {
		return insertHistory(botId, DIALOG_SPEAKER.USER, userId, sessionId, text, dialogSeq, "", "", "", "");
	}
	
	public long insertUserUtterHistory(UtterData utter) throws Exception {
		if(utter == null) {
			return 0;
		}
		utter.setSpeaker(DIALOG_SPEAKER.USER);
		return insertHistory(utter);
	}
	
	public long updateUtterHistory(UtterData utter) throws Exception {
		return his.updateDialog(utter);
	}
	
//	public List<ChatbotMsg> getChatbotMsgs(int chatbotId, String msgTypeCd) throws Exception{
	public String getChatbotMsgs(int chatbotId, String msgTypeCd) throws Exception{
		logger.info("getChatbotMsgs/START/{},{}",chatbotId, msgTypeCd);
		List<ChatbotMsg> list = null;
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("chatbotId", chatbotId);
			params.put("msgTypeCd", msgTypeCd);
			list = chatbot.getChatbotMessages(params); //QQQ cache?
			
			if(list != null && list.size() > 0) {
				Random r = new Random();
				int idx = r.nextInt(list.size());
				ChatbotMsg m = list.get(idx);
				return m.getMsg();
			}else {
				return "";
			}
		}finally {
			logger.info("getChatbotMsgs/END");
		}
//		return list;
	}
	
	public long insertSdsPortHis(int botId, String sessionId, int projectPort, String sdsIp) throws Exception{
		logger.info("insertSdsPortHis/{},{},{},{},{}", botId, sessionId, projectPort, sdsIp);
		
		SdsProjectPortHis hisdata = new SdsProjectPortHis();
		hisdata.setBotId(botId);
		hisdata.setSessionId(sessionId);
		hisdata.setProjectPort(projectPort);
		hisdata.setSdsIp(sdsIp);
//		hisdata.setCloseReasonCd(byUser? IChatbotConst.SDS_TERMINATION_BY_USER:IChatbotConst.SDS_TERMINATION_BY_SYSTEM);
		long nRet = 0;
		try {
			nRet = his.insertSdsPortHis(hisdata);
		}catch(Exception e) {
			logger.warn("insertSdsPortHis/{}", e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	public long updateSdsPortHis(int hisId, boolean closeByUser) throws Exception{
		logger.info("updateSdsPortHis/{},{}", hisId, closeByUser);
		
		HashMap<String, Object> params = new HashMap<>();
		params.put("hisId", hisId);
		params.put("closeReasonCd", closeByUser? IChatbotConst.SDS_TERMINATION_BY_USER: IChatbotConst.SDS_TERMINATION_BY_SYSTEM);
		
		long nRet = 0;
		try {
			nRet = his.updateSdsPortHis(params);
		}catch(Exception e) {
			logger.warn("updateSdsPortHis/{}", e.getMessage());
			throw e;
		}
		return nRet;
	}
	
	public SdsProjectPortHis getSdsPortHisLatest(String sdsIp, int projectPort) {
		logger.info("getSdsPortHisLatest/{},{}", projectPort, sdsIp);
		
		SdsProjectPortHis hisData = null;
		HashMap<String, Object> params = new HashMap<>();
		params.put("sdsIp", sdsIp);
		params.put("projectPort", projectPort);
		
		try {
			hisData = his.getSdsPortHisLatest(params);
		}catch(Exception e) {
			logger.debug("getSdsPortHisLatest/{}",e.getMessage());
		}
		return hisData;
	}
	
	public synchronized SdsProjectPortHis getLastSdsPort(int maxPort) {
		logger.info("getLastSdsPort/{}", maxPort);
		SdsProjectPortHis hisData = null;
		try {
			hisData = his.getLastSdsPort(maxPort);
		}catch(Exception e) {
			logger.debug("getLastSdsPort/{}",e.getMessage());
		}
		return hisData;
	}
	
	/**
	 * get configured sds domain list for a bot
	 * @param botId
	 * @return
	 */
	public List<String> getSdsDomainList(int botId) {
		
		ChatbotConfig config = chatbot.getChatbotConfig(botId);
		ArrayList<String> list = new ArrayList<>();
		if(config != null) {
			List<ChatbotDomainSimple> sdsList = chatbot.getSdsDomainList(config.getConfId());
			if(sdsList != null) {
				for(ChatbotDomainSimple ds: sdsList) {
					list.add(ds.getDomainName());
				}
			}
		}
		return list;
	}
}
