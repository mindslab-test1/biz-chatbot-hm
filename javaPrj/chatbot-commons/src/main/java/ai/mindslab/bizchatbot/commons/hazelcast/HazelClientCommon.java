package ai.mindslab.bizchatbot.commons.hazelcast;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.config.InMemoryFormat;
import com.hazelcast.config.NearCacheConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

//import com.hazelcast.client.config.ClientConfig;

@Component
@Qualifier("hazelClientCommon")
//@ConditionalOnClass(name="com.hazelcast.core.HazelcastInstance")
public class HazelClientCommon {

//	private static HazelClient instance = null;
	
	@Value("${hazelcast.server.ip}")
	private String[] hazelIps;
	
	@Value("${hazelcast.group.name}")
	private String groupName;
	
	@Value("${hazelcast.group.password}")
	private String groupPassword;
	
	private HazelcastInstance client = null;
	
	@PreDestroy
	private void destroy() {
		if(client != null) {
			client.shutdown();
		}
	}
	
	public HazelClientCommon() {
		
	}
	
	public HazelClientCommon(String[] ips, String groupName, String password) {
		this.hazelIps = ips;
		this.groupName = groupName;
		this.groupPassword = password;
	}
	
//	public static HazelClient getInstance() {
//		if(instance == null) {
//			instance = new HazelClient();
//		}
//		return instance;
//	}
	
	public HazelcastInstance getClient() {
		if(client == null || !client.getLifecycleService().isRunning()) {
			ClientConfig clientConfig = new ClientConfig();
			clientConfig.getGroupConfig().setName(groupName).setPassword(groupPassword);
			clientConfig.getNetworkConfig().setSmartRouting(true);
			clientConfig.getNetworkConfig().addAddress(hazelIps);
			clientConfig.getNetworkConfig().setConnectionAttemptLimit(1);
			clientConfig.getNetworkConfig().setConnectionAttemptPeriod(1000);
			
			NearCacheConfig nearCacheConfig = getNearCacheConfig("chatbot-objects");
			clientConfig.addNearCacheConfig(nearCacheConfig);
			nearCacheConfig = getNearCacheConfig("botconfig");
			clientConfig.addNearCacheConfig(nearCacheConfig);
			
			
			client = HazelcastClient.newHazelcastClient(clientConfig);
		}
		return client;
	}
	
	private NearCacheConfig getNearCacheConfig(String cacheName) {
		NearCacheConfig nearCacheConfig = new NearCacheConfig(cacheName);
		nearCacheConfig.setMaxSize(1000);
		nearCacheConfig.setInvalidateOnChange(true);
		nearCacheConfig.setTimeToLiveSeconds(300);
		nearCacheConfig.setMaxIdleSeconds(300);
		nearCacheConfig.setInMemoryFormat(InMemoryFormat.OBJECT);
		nearCacheConfig.setEvictionPolicy("LRU");
		return  nearCacheConfig;
	}
	
	public HazelcastInstance reconnect() {
		if(client != null && client.getLifecycleService().isRunning()) {
			client.getLifecycleService().shutdown();
		}
		
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.getGroupConfig().setName(groupName).setPassword(groupPassword);
		clientConfig.getNetworkConfig().addAddress(hazelIps);
		clientConfig.getNetworkConfig().setConnectionAttemptLimit(1);
		clientConfig.getNetworkConfig().setConnectionAttemptPeriod(1000);
		
		client = HazelcastClient.newHazelcastClient(clientConfig);
		return client;
	}
	
	public <K,V> IMap<K, V> getMap(String name){
		
		return this.getClient().getMap(name);
	}
}
