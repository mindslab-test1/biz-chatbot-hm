package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

public class DialogCommonConfig implements Serializable{

	private static final long serialVersionUID = 1L;
	protected String key;
	protected String value;
	
	public DialogCommonConfig() {
	}
	
	public DialogCommonConfig(String key, String val) {
		this.key = key;
		this.value = val;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "DialogCommonConfig [key=" + key + ", value=" + value + "]";
	}
}
