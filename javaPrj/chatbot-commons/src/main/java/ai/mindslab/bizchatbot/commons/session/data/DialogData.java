package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DialogData implements Serializable{

	private static final long serialVersionUID = 1L;
	private int botId;
	private String botName;
	private ArrayList<UtterData> conversation;
	
	public DialogData(){ }

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public String getBotName() {
		return botName;
	}

	public void setBotName(String botName) {
		this.botName = botName;
	}

	public ArrayList<UtterData> getConversation() {
		if(conversation == null) {
			conversation = new ArrayList<UtterData>(20);
		}
		return conversation;
	}

	public void setConversation(ArrayList<UtterData> conversation) {
		this.conversation = conversation;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
