package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

public class DialogError implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String errFilter;
	private String errMsg;
	private int errCode;
	
	public DialogError() {
	}

	public String getErrFilter() {
		return errFilter;
	}

	public void setErrFilter(String errFilter) {
		this.errFilter = errFilter;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public int getErrCode() {
		return errCode;
	}

	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}

	@Override
	public String toString() {
		return "DialogError [errFilter=" + errFilter + ", errMsg=" + errMsg + ", errCode=" + errCode + "]";
	}
}
