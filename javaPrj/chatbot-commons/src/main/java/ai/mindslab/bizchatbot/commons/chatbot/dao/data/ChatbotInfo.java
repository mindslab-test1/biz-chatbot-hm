package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties({"createdDtm", "updatedDtm", "creatorId", "updatorId"})
@SuppressWarnings("serial")
public class ChatbotInfo implements Serializable {

	protected String botId;
	protected int teamId;
	protected String botName;
	protected String description;
	protected String imgPath;
	protected String useYn;
	protected String createdDtm;
	protected String updatedDtm;
	protected String creatorId;
	protected String updatorId;
	protected ChatbotConfig chatbotConfig;
	protected List<NoticeInfo> notices; 
	
	public ChatbotInfo() {
	}
  
	public String getBotId() {
		return botId;
	}

	public void setBotId(String botId) {
		this.botId = botId;
	}

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getBotName() {
		return botName;
	}

	public void setBotName(String botName) {
		this.botName = botName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ChatbotConfig getChatbotConfig() {
		return chatbotConfig;
	}

	public void setChatbotConfig(ChatbotConfig chatbotConfig) {
		this.chatbotConfig = chatbotConfig;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	public List<NoticeInfo> getNotices() {
		return notices;
	}

	public void setNotices(List<NoticeInfo> notices) {
		this.notices = notices;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
