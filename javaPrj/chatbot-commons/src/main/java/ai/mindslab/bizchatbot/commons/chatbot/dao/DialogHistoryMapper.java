package ai.mindslab.bizchatbot.commons.chatbot.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.SdsProjectPortHis;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;

@Mapper
public interface DialogHistoryMapper {

	public List<UtterData> getDialogHistory(HashMap<String,Object> params);
	public long getDialogHistoryCount(HashMap<String,Object> params);
	public UtterData getDialog(int utterId);
	public long insertDialog(UtterData utter);
	public long updateDialog(UtterData utter);
	
	public long insertSdsPortHis(SdsProjectPortHis his);
	public long updateSdsPortHis(HashMap<String, Object> params);
	public List<SdsProjectPortHis> getSdsPortHis(HashMap<String, Object> params);
	public long getSdsPortHisCount(HashMap<String, Object> params);
	public SdsProjectPortHis getSdsPortHisLatest(HashMap<String, Object> params);
	
	public SdsProjectPortHis getLastSdsPort(int projectPort);
	
}
