package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ChatbotConfig implements Serializable{

	protected int confId;
	protected int botId;
	protected int greetingMsgBtnId;
	protected int unknownMsgBtnId;
	protected String synonymYn;
	protected String scfYn;
	protected String dnnYn;
	protected String bqaReconfirmMsg;
	protected int bqaSearchFlowType;
	protected int bqaSearchSolrOrMatchRange;
	protected String createdDtm;
	protected String creatorId;
	protected String updatedDtm;
	protected String updatorId;
	
	protected List<ChatbotDomainSimple> bqaDomains;
	protected List<ChatbotDomainSimple> sdsDomains;
	protected List<ChatbotMsg> greetingMsg;
	protected List<ChatbotMsg> unknownMsg;
//	protected List<ChatbotButton> greetingButtons;
//	protected List<ChatbotButton> unknownButtons;
	
	protected ChatbotRichContent greetingRichContent;
	protected ChatbotRichContent unknownRichContent;
	
	protected float sdsSluWeightCriteria = 0.f;
	protected String changeDomainYn;
	
	public ChatbotConfig() {
	}

	public int getConfId() {
		return confId;
	}

	public void setConfId(int confId) {
		this.confId = confId;
	}

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public List<ChatbotDomainSimple> getBqaDomains() {
		return bqaDomains;
	}

	public void setBqaDomains(List<ChatbotDomainSimple> bqaDomains) {
		this.bqaDomains = bqaDomains;
	}

	public List<ChatbotDomainSimple> getSdsDomains() {
		return sdsDomains;
	}

	public void setSdsDomains(List<ChatbotDomainSimple> sdsDomains) {
		this.sdsDomains = sdsDomains;
	}

	public List<ChatbotMsg> getGreetingMsg() {
		return greetingMsg;
	}

	public void setGreetingMsg(List<ChatbotMsg> greetingMsg) {
		this.greetingMsg = greetingMsg;
	}

	public List<ChatbotMsg> getUnknownMsg() {
		return unknownMsg;
	}

	public void setUnknownMsg(List<ChatbotMsg> unknownMsg) {
		this.unknownMsg = unknownMsg;
	}

	public int getUnknownMsgBtnId() {
		return unknownMsgBtnId;
	}

	public void setUnknownMsgBtnId(int unknownMsgBtnId) {
		this.unknownMsgBtnId = unknownMsgBtnId;
	}

	public String getSynonymYn() {
		return synonymYn;
	}

	public void setSynonymYn(String synonymYn) {
		this.synonymYn = synonymYn;
	}

	public String getScfYn() {
		return scfYn;
	}

	public void setScfYn(String scfYn) {
		this.scfYn = scfYn;
	}

	public String getDnnYn() {
		return dnnYn;
	}

	public void setDnnYn(String dnnYn) {
		this.dnnYn = dnnYn;
	}

	public String getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

//	public List<ChatbotButton> getUnknownButtons() {
//		if(unknownButtons == null) {
//			unknownButtons = new ArrayList<>();
//		}
//		return unknownButtons;
//	}
//
//	public void setUnknownButtons(List<ChatbotButton> unknownButtons) {
//		this.unknownButtons = unknownButtons;
//	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	public int getGreetingMsgBtnId() {
		return greetingMsgBtnId;
	}

	public void setGreetingMsgBtnId(int greetingMsgBtnId) {
		this.greetingMsgBtnId = greetingMsgBtnId;
	}

//	public List<ChatbotButton> getGreetingButtons() {
//		if(greetingButtons == null) {
//			greetingButtons = new ArrayList<>();
//		}
//		return greetingButtons;
//	}
//
//	public void setGreetingButtons(List<ChatbotButton> greetingButtons) {
//		this.greetingButtons = greetingButtons;
//	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public ChatbotRichContent getGreetingRichContent() {
		return greetingRichContent;
	}

	public void setGreetingRichContent(ChatbotRichContent greetingRichContent) {
		this.greetingRichContent = greetingRichContent;
	}

	public ChatbotRichContent getUnknownRichContent() {
		return unknownRichContent;
	}

	public void setUnknownRichContent(ChatbotRichContent unknownRichContent) {
		this.unknownRichContent = unknownRichContent;
	}

	public String getBqaReconfirmMsg() {
		return bqaReconfirmMsg;
	}

	public void setBqaReconfirmMsg(String bqaReconfirmMsg) {
		this.bqaReconfirmMsg = bqaReconfirmMsg;
	}

	public int getBqaSearchFlowType() {
		return bqaSearchFlowType;
	}

	public void setBqaSearchFlowType(int bqaSearchFlowType) {
		this.bqaSearchFlowType = bqaSearchFlowType;
	}

	public int getBqaSearchSolrOrMatchRange() {
		return bqaSearchSolrOrMatchRange;
	}

	public void setBqaSearchSolrOrMatchRange(int bqaSearchSolrOrMatchRange) {
		this.bqaSearchSolrOrMatchRange = bqaSearchSolrOrMatchRange;
	}

	public float getSdsSluWeightCriteria() {
		return sdsSluWeightCriteria;
	}

	public void setSdsSluWeightCriteria(float sdsSluWeightCriteria) {
		this.sdsSluWeightCriteria = sdsSluWeightCriteria;
	}

	public String getChangeDomainYn() {
		return changeDomainYn;
	}

	public void setChangeDomainYn(String changeDomainYn) {
		this.changeDomainYn = changeDomainYn;
	}

}
