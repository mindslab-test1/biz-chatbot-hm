package ai.mindslab.bizchatbot.commons.outif;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotApiMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiJsonTemplate;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiParam;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiResultMappingInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotButton;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRespInterfaceRelData;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;

@Service
public class ThirdPartyApiManager {

	private Logger logger = LoggerFactory.getLogger(ThirdPartyApiManager.class);

	@Autowired
	private ChatbotApiMapper api;
	
	public ThirdPartyApiManager() {
	}
	
	@Transactional
	public ChatbotApiInfo getApiInfo(int ifId) throws Exception{
		logger.info("getApiInfo/{}",ifId);
		ChatbotApiInfo info = null;
		try {
			info = api.getApiInfo(ifId);
			if(info != null) {
				List<ChatbotApiParam> list = api.getParams(ifId);
				if(list != null) {
					info.getParamList().addAll(list);
				}
				ChatbotApiJsonTemplate jsonTemplate = api.getJsonTemplate(ifId);
				info.setJsonTemplate(jsonTemplate);
			}
		}catch(Exception e) {
			logger.warn("getApiInfo/{}",e.getMessage());
		}
		return info;
	}
	
	public List<ChatbotApiParam> getApiParams(int ifId) throws Exception{
		logger.info("getApiParams/{}",ifId);
		List<ChatbotApiParam> list = null;
		try {
			list = api.getParams(ifId);
		}catch(Exception e) {
			logger.warn("getApiParams/{}", e.getMessage());
		}
		return list;
	}

	public List<ChatbotApiResultMappingInfo> getApiResultMappingInfo(int ifId) throws Exception{
		logger.info("getApiMappingInfo/{}",ifId);
		List<ChatbotApiResultMappingInfo> list = null;
		try {
			list = api.getApiResultMappingInfo(ifId);
		}catch(Exception e) {
			logger.warn("getApiResultMappingInfo/{}", e.getMessage());
		}
		return list;
	}
	
	public <T> T extractValue(String jsonString, String selector, Class<T> clazz) throws Exception{
		T value;
		
		DocumentContext context = JsonPath.parse(jsonString);
		value = context.read(selector, clazz);
		
		return value;
	}
	
	public Object extractValue(String jsonString, String selector) throws Exception{
		Object out = null;
		
		out = JsonPath.read(jsonString, selector);
		
		return out;
	}
	
	@Transactional
	public ChatbotRespInterfaceRelData getChatbotRespInterfaceRel(int botId, String respString) throws Exception{
		logger.info("getChatbotRespInterfaceRel/{}/{}",botId, respString);
		ChatbotRespInterfaceRelData rel = null;
		try {
			String syscode = extractRespSystemCode(respString);
			if(StringUtils.isEmpty(syscode)) {
				return null;
			}
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("botId", botId);
			params.put("respCode", syscode);
			
			rel = api.getResponseInterfaceRel(params);
			if(rel != null) {
				List<ChatbotApiResultMappingInfo> list = api.getApiResultMappingInfo(rel.getIfId());//like search
				if(list != null) {
					rel.getResultMappingList().addAll(list);
				}
			}
		}catch(Exception e) {
			logger.warn("getChatbotRespInterfaceRel/{}", e.getMessage());
		}
		return rel;
	}
	
	public ChatbotApiJsonTemplate getJsonTemplate(int ifId) throws Exception{
		logger.info("getChatbotRespInterfaceRel/{}",ifId);
		ChatbotApiJsonTemplate template = null;
		
		try {
			template = api.getJsonTemplate(ifId);
		}catch(Exception e) {
			logger.warn("getJsonTemplate/{}", e.getMessage());
		}
		
		return template;
	}
	
	public String extractRespSystemCode(String respString) {
		//QQQ pattern -> static
		String foundSystemCode = "";
		Pattern p;
		Matcher m;
		//p = Pattern.compile(".*("+IChatbotConst.PREFIX_SYSOUT+"[^\\s.+]+)\\s+(.*)");
		p = Pattern.compile(IChatbotConst.PREFIX_SYSOUT+"([^\\s.+]+)\\s+(.*)");
		m = p.matcher(respString);
		if(m.find()) {
			foundSystemCode = m.group(1);
			logger.debug("extractRespSystemCode/found system code:{}",foundSystemCode);
		}
		return foundSystemCode;
	}
	
	public String removeSystemCode(String respString) {
		//QQQ pattern -> static
		String orgMsg = "";
		Pattern p;
		Matcher m;
		p = Pattern.compile(".*("+IChatbotConst.PREFIX_SYSOUT+"[^\\s.+]+)\\s+(.*)");
		m = p.matcher(respString);
		if(m.find()) {
			orgMsg = m.group(2);
			logger.debug("removeSystemCode/found system code:{}",orgMsg);
		}
		return orgMsg;
	}
	
	public String mappingResultString(String sysOut, String target, Object result) throws Exception{
		if(result == null) {
			return sysOut;
		}
		if(result instanceof String || result instanceof Integer || result instanceof Long || result instanceof Float) {
			
			return sysOut.replaceAll(target, ""+result);
		}else if(result instanceof List) {
			//List<String>만 지원.
			//QQQ make button list?
			try {
				List<Object> list = (List<Object>) result;
				StringBuilder buff = new StringBuilder();
				if(list != null) {
					int idx = 0;
					String tmpStr = "";
					for(Object str: list) {
						if(str instanceof Map) {
							tmpStr = mappingResultString("_", "_", str);
						}else if(str instanceof String) {
							tmpStr = (String)str;
						}
						if(idx !=0) {
							buff.append(", ").append(tmpStr);
						}else {
							buff.append(tmpStr);
						}
						idx++;
					}
				}
				return sysOut.replaceAll(target, buff.toString());
			}catch(Exception e) {
				Object ch = ((List)result).get(0);
				throw new Exception("Api result output is not applicable for List of " + ch.getClass().getSimpleName());
			}
		}else if(result instanceof Map) {
			//result: {key:value}
			Map out = (Map)result;
			Set<String> keys = out.keySet();
			StringBuilder buff = new StringBuilder();
			int idx = 0;
			for(String key: keys) {
				if(idx ==0) {
					buff.append(key).append(": ").append(out.get(key));
				}else {
					buff.append(", ").append(key).append(": ").append(out.get(key));
				}
				idx++;
			}
			String tmp = buff.toString();
			return sysOut.replaceAll(target, tmp);
		}else {
			throw new Exception("Api result output is not applicable." + result.getClass().getSimpleName());
		}
		
	}
	
	public List<ChatbotButton> mappingResultStringToButtons(String sysOut, String target, Object result) throws Exception{
		if(result == null || !(result instanceof List)) {
			return null;
		}
		List<ChatbotButton> buttons = new ArrayList<ChatbotButton>();
		
		try {
			List<Object> list = (List<Object>) result;
			StringBuilder buff = new StringBuilder();
			if(list != null) {
				int btnIdx = 1;
				String tmpStr = "";
				for(Object str: list) {
					if(str instanceof Map) {
						Map map = (Map)str;
						Set<String> keys = map.keySet();
						for(String key:keys) {
							ChatbotButton btn = new ChatbotButton();
							btn.setBtnOrder(btnIdx++);
							btn.setTitle((String)map.get(key));
							btn.setUserUtter((String)map.get(key));
//							btn.setTitle(sysOut.replaceAll(target, (String)map.get(key)));
//							btn.setUserUtter(sysOut.replaceAll(target, (String)map.get(key)));
							buttons.add(btn);
						}
					}else if(str instanceof String || str instanceof Integer || str instanceof Long || str instanceof Float) {
						ChatbotButton btn = new ChatbotButton();
						btn.setBtnOrder(btnIdx++);
						btn.setTitle(str+"");
						btn.setUserUtter(str+"");
//						btn.setTitle(sysOut.replaceAll(target, (String)str));
//						btn.setUserUtter(sysOut.replaceAll(target, (String)str));
						buttons.add(btn);
					}
				}
			}
			return buttons;
		}catch(Exception e) {
			Object ch = ((List)result).get(0);
			throw new Exception("Api result output is not applicable for List of " + ch.getClass().getSimpleName());
		}
	}
	
	public ApiResult parseResult(String jsonString, String selector) throws Exception{
		
		Object out = extractValue(jsonString, selector);
		ApiResult result = new ApiResult();
		result.setResult(out);
		result.setType(out.getClass());
		
		return result;
	}
	
	public int parseCode(String jsonString) throws Exception{
		Integer out = (Integer)extractValue(jsonString, "$.code");
		return out;
	}

	public Object parseData(String jsonString ) throws Exception{
		Object out = (Integer)extractValue(jsonString, "$.data");
		return out;
	}
	
	public static class ApiResult {
		protected Class type;
		protected Object result;
		
		public ApiResult() {
			
		}

		public Class getType() {
			return type;
		}

		public void setType(Class type) {
			this.type = type;
		}

		public Object getResult() {
			return result;
		}

		public void setResult(Object result) {
			this.result = result;
		}

		@Override
		public String toString() {
			return "ApiResult [type=" + type.getName() + ", result=" + result + "]";
		}
		
	}
}
