package ai.mindslab.bizchatbot.commons.chatbot.service;

import java.util.HashMap;
import java.util.List;

import javax.cache.annotation.CacheResult;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotRichContentMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.DialogSynonymMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotButton;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotDomainSimple;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotMsg;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichCarousel;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichImage;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.DialogSynonymData;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.NoticeInfo;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.data.BaseListObject;
import ai.mindslab.bizchatbot.commons.utils.PagingHelper;

@Service
public class ChatbotService implements IChatbotConst{
	private Logger logger = LoggerFactory.getLogger(ChatbotService.class);
	
	@Autowired
	protected ChatbotMapper chatbot;
	
	@Autowired
	protected ChatbotRichContentMapper rich;
	
	@Autowired
	protected PagingHelper pgHelper;
	
	@Autowired
	protected DialogSynonymMapper synonym; 
	
//	@Autowired
//	protected CacheManager cacheManager;
	
	public ChatbotService() {
		// TODO Auto-generated constructor stub
	}
	
	
	public List<ChatbotInfo> getChatbotList(String useYn, int pg, int pgSize) throws Exception {
		logger.info("getChatbotList/START/{},{},{}", useYn, pg, pgSize);
		
		HashMap<String,Object> params = new HashMap<>();
		
//		params.put("startIndex", 0);
//		params.put("pgSize", ""+pgSize);
		pgHelper.setPageInfo(params, pg, pgSize);
		if(!StringUtils.isEmpty(useYn)) {
			params.put("useYn", useYn);
		}
		
		List<ChatbotInfo> list;
		try {
			list = chatbot.getChatbotInfoList(params);
		}catch(Exception e) {
			logger.warn("getChatbotList/{}",e.getMessage());
			throw e;
		}finally {
			logger.info("getChatbotList/END/");
		}
		return list;
	}
	
	public long getChatbotinfoListCount(String useYn) throws Exception{
		logger.info("getChatbotinfoListCount/START/{}", useYn);
		
		HashMap<String,Object> params = new HashMap<>();
		if(!StringUtils.isEmpty(useYn)) {
			params.put("useYn", useYn);
		}
		
		long count = 0;
		try {
			count = chatbot.getChatbotInfoListCount(params);
			logger.debug("count:{}",count);
		}catch(Exception e) {
			logger.warn("getChatbotinfoListCount/{}",e.getMessage());
			throw e;
		}finally {
			logger.info("getChatbotinfoListCount/END/");
		}
		return count;
	}
	
	public BaseListObject<ChatbotInfo> getChatbotListObj(String useYn, int pg, int pgSize) throws Exception {
		List<ChatbotInfo> list = null;
		int tot = (int)getChatbotinfoListCount(useYn);
		if(tot > 0) {
			list = getChatbotList(useYn, pg, pgSize);
		}
		BaseListObject<ChatbotInfo> obj = new BaseListObject<ChatbotInfo>(tot, list);
		return obj;
	}
	
	@Cacheable(key= "#botId", cacheNames="chatbot-objects", cacheManager="hazelCacheManager")
	public ChatbotInfo getChatbotinfo(int botId) throws Exception{
		logger.info("getChatbotinfo/START/{}", botId);
		
		ChatbotInfo info;
		try {
			info = chatbot.getChatbotInfo(botId);
			logger.debug("chatbot:{}",info);
		}catch(Exception e) {
			logger.warn("getChatbotinfo/{}",e.getMessage());
			throw e;
		}finally {
			logger.info("getChatbotinfo/END/");
		}
		return info;
	}
	
	@Cacheable(key="#botId", cacheNames="botconfig", cacheManager="hazelCacheManager")
	public ChatbotConfig getChatbotConfig(int botId) throws Exception{
		logger.info("getChatbotConfig/START/{}", botId);
		//QQQ cache?
		
		ChatbotConfig chatbotConfig = null;
		try {
			chatbotConfig = chatbot.getChatbotConfig(botId);
			if(chatbotConfig != null) {
				if(chatbotConfig.getUnknownMsgBtnId()>0) {
//					List<ChatbotButton> buttons = rich.getButtonList(chatbotConfig.getUnknownMsgBtnId());
//					chatbotConfig.setUnknownButtons(buttons);
					ChatbotRichContent cont = getRichContent(chatbotConfig.getUnknownMsgBtnId());
					chatbotConfig.setUnknownRichContent(cont);
				}
				if(chatbotConfig.getGreetingMsgBtnId()>0) {
//					List<ChatbotButton> buttons = rich.getButtonList(chatbotConfig.getGreetingMsgBtnId());
//					chatbotConfig.setGreetingButtons(buttons);
					ChatbotRichContent cont = getRichContent(chatbotConfig.getGreetingMsgBtnId());
					chatbotConfig.setGreetingRichContent(cont);
				}
				List<ChatbotDomainSimple> bqaDomains = chatbot.getBqaDomainList(chatbotConfig.getConfId());
				chatbotConfig.setBqaDomains(bqaDomains);
				List<ChatbotDomainSimple> sdsDomains = chatbot.getSdsDomainList(chatbotConfig.getConfId());
				chatbotConfig.setSdsDomains(sdsDomains);
				
				//greeting msgs
				List<ChatbotMsg> gMsgs = getChatbotMsg(botId, MSG_TYPE_GREETING);
				chatbotConfig.setGreetingMsg(gMsgs);
				List<ChatbotMsg> uMsgs = getChatbotMsg(botId, MSG_TYPE_UNKNOWN);
				chatbotConfig.setUnknownMsg(uMsgs);
			}
		}catch(Exception e) {
			logger.warn("getChatbotConfig/{}",e.getMessage());
			throw e;
		}finally {
			logger.info("getChatbotConfig/END/");
		}
		return chatbotConfig;
	}
	
	@Cacheable(key="#richContentId", cacheNames="chatbot-objects", cacheManager="hazelCacheManager")
	public ChatbotRichContent getRichContent(int richConentId) throws Exception{
		logger.info("getRichContent/START/{}", richConentId);
		//QQQ cache?
		ChatbotRichContent content = null;
		try {
			content = rich.getRichConent(richConentId);
			if(content != null) {
				if(IChatbotConst.RICH_CONTENT_TYPE_BUTTON.equals(content.getContentTypeCd())) {
					List<ChatbotButton> buttons = rich.getButtonList(richConentId);
					content.setButtons(buttons);
				}else if(IChatbotConst.RICH_CONTENT_TYPE_IMAGE.equals(content.getContentTypeCd())) {
					ChatbotRichImage image = rich.getRichImage(richConentId);
					content.setImage(image);
				}else if(IChatbotConst.RICH_CONTENT_TYPE_CAROUSEL.equals(content.getContentTypeCd())) {
					List<ChatbotRichCarousel> carousels = rich.getRichCarousel(richConentId);
					content.setCarousels(carousels);
				}else {
					
				}
			}
		}catch(Exception e) {
			logger.warn("getRichContent/{}",e.getMessage());
			throw e;
		}finally {
			logger.info("getRichContent/END/");
		}
		return content;
	}
	
//	@Cacheable(key= "{#botId,#msgTypeCode}", cacheNames="chatbot-objects", cacheManager="hazelCacheManager")
	public List<ChatbotMsg> getChatbotMsg(int botId, String msgTypeCode) throws Exception{
		logger.info("getChatbotMsg/START/{}, {}", botId, msgTypeCode);
		//QQQ cache?
		List<ChatbotMsg> msgs = null;
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("chatbotId", botId);
			params.put("msgTypeCd", msgTypeCode);
			
			msgs = chatbot.getChatbotMessages(params);
		}catch(Exception e) {
			logger.warn("getChatbotMsg/{}",e.getMessage());
			throw e;
		}finally {
			logger.info("getChatbotMsg/END/");
		}
		return msgs;
	}
	
	public List<DialogSynonymData> getSynonymList(int botId) throws Exception{
		logger.info("getSynonymList/START/{}", botId);
		List<DialogSynonymData> list = null;
		
		try {
			Integer wsId = synonym.getWorkspaceId(botId);
			if(wsId != null) {
				list = synonym.getSynonymList(wsId);
			}
		}catch(Exception e) {
			logger.warn("getSynonymList/{}",e.getMessage());
		}
		return list;
	}
	
	public List<NoticeInfo> getChatbotNotice(int botId) throws Exception{
		logger.info("getChatbotNotice/START/{}", botId);
		List<NoticeInfo> list = null;
		try {
			list = chatbot.getChatbotNotice(botId);
		}catch(Exception e) {
			logger.warn("getChatbotNotice/{}",e.getMessage());
		}
		return list;
	}
	
}
