package ai.mindslab.bizchatbot.commons.codes;

public interface IBasicQAConst {

	public static final int SEARCH_TYPE_DB = 2;
	public static final int SEARCH_TYPE_SOLRAND = 3;
	public static final int SEARCH_TYPE_DB_SOLRAND = 1;
	public static final int SEARCH_TYPE_DB_SOLRAND_SOLROR = 4;
	
	public final static int SEARCH_RESULT_DB_FAIL = -1;
	public final static int SEARCH_RESULT_DB_SUCCESS = 1;
	public final static int SEARCH_RESULT_SOLRAND_FAIL = -2;
	public final static int SEARCH_RESULT_SOLRAND_SUCCESS = 2;
	public final static int SEARCH_RESULT_SOLROR_FAIL = -3;
	public final static int SEARCH_RESULT_SOLROR_SUCCESS = 3;

	public static final int ADMIN_SEARCH_TYPE_NOTHING = 0;
	public static final int ADMIN_SEARCH_TYPE_QUESTION = 1;
	public static final int ADMIN_SEARCH_TYPE_ANSWER = 2;
}
