package ai.mindslab.bizchatbot.commons.session.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.map.listener.EntryUpdatedListener;

import ai.mindslab.bizchatbot.commons.session.data.DialogSession;

public class SessionUpdatedListener implements EntryUpdatedListener<String, DialogSession> {
	private Logger logger = LoggerFactory.getLogger(SessionUpdatedListener.class);
	
	private String sessionId;
	
	public SessionUpdatedListener(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public void entryUpdated(EntryEvent<String, DialogSession> event) {
		logger.debug("session updated/ses:{}/botid:{}/{}",sessionId, event.getValue().getDialog().getBotId(), event.getValue().getDialog().getConversation().get(event.getValue().getDialog().getConversation().size()-1));
		//QQQ
	}
}
