package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

@SuppressWarnings("serial")
public class ChatbotApiJsonTemplate implements Serializable{

	protected int templId;
	protected int ifId;
	protected String jsonString;
	
	@JsonIgnore
	protected String createdDtm;
	@JsonIgnore
	protected String updatedDtm;
	@JsonIgnore
	protected String creatorId;
	@JsonIgnore
	protected String updatorId;
	
	public ChatbotApiJsonTemplate() {
	}

	public int getTemplId() {
		return templId;
	}

	public void setTemplId(int templId) {
		this.templId = templId;
	}

	public int getIfId() {
		return ifId;
	}

	public void setIfId(int ifId) {
		this.ifId = ifId;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public String getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	@Override
	public String toString() {
		return "ChatbotApiJsonTemplate [templId=" + templId + ", ifId=" + ifId + ", jsonString=" + jsonString
				+ ", createdDtm=" + createdDtm + ", updatedDtm=" + updatedDtm + ", creatorId=" + creatorId
				+ ", updatorId=" + updatorId + "]";
	}

}
