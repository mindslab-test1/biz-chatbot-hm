package ai.mindslab.bizchatbot.commons.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;

public class StrUtils {
	
	private static Pattern CODE_PATTERN;
	static {
		CODE_PATTERN = Pattern.compile("("+IChatbotConst.PREFIX_SYSOUT+"[^\\s.+]+)?\\s+(.*)");
	}

	public static int getNum(String val, int defaultVal) {
		String tgtVal = val;
		try {
			if(StringUtils.isNumeric(tgtVal)) {
				return Integer.parseInt(tgtVal);
			}else {
				return defaultVal;
			}
		}catch(Exception e) {
			return defaultVal;
		}
	}
	
	public static String getStr(String val, String defaultVal) {
		if(StringUtils.isEmpty(val)) {
			return defaultVal;
		}else {
			return val;
		}
	}
	
	public static String extractRespSystemCode(String respString) {
		//QQQ pattern -> static
		String foundSystemCode = "";
//		Pattern p;
		Matcher m;
//		p = Pattern.compile(".*("+IChatbotConst.PREFIX_SYSOUT+"[^\\s.+]+)\\s+(.*)");
		m = CODE_PATTERN.matcher(respString);
		if(m.find()) {
			foundSystemCode = m.group(1);
		}
		return foundSystemCode;
	}
	
	public static String removeSystemCode(String respString) {
		//QQQ pattern -> static
		String orgMsg = "";
//		Pattern p;
		Matcher m;
//		p = Pattern.compile(".*("+IChatbotConst.PREFIX_SYSOUT+"[^\\s.+]+)\\s+(.*)");
		m = CODE_PATTERN.matcher(respString);
		if(m.find()) {
			orgMsg = m.group(2);
		}
		return orgMsg;
	}
	
	public static String removePrefix(String code) {
		if(StringUtils.isEmpty(code)) {
			return "";
		}
		if(code.startsWith(IChatbotConst.PREFIX_SYSOUT)) {
			return code.substring(IChatbotConst.PREFIX_SYSOUT.length());
		}else {
			return code;
		}
	}
}
