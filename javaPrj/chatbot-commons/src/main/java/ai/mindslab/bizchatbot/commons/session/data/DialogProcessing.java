package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;
import java.util.ArrayList;

import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.SYS_UTTER_TYPE;

public class DialogProcessing implements Serializable{

	protected String currBotUtter;
	protected String sdsDomain;
	protected String expectedSlot;
	protected ArrayList<Slot> expectedSlots;
	protected boolean skipBqa = false;//MOBIS
	protected BqaProcessing bqaProcessing; //MOBIS
	protected SYS_UTTER_TYPE svcType = SYS_UTTER_TYPE.NORMAL;
	protected int ifId;
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public DialogProcessing() {
	}

	public String getCurrBotUtter() {
		return currBotUtter;
	}

	public void setCurrBotUtter(String currBotUtter) {
		this.currBotUtter = currBotUtter;
	}

	public String getSdsDomain() {
		return sdsDomain;
	}

	public void setSdsDomain(String sdsDomain) {
		this.sdsDomain = sdsDomain;
	}
	
	public String getExpectedSlot() {
		return expectedSlot;
	}

	public void setExpectedSlot(String expectedSlot) {
		this.expectedSlot = expectedSlot;
	}
	
	public boolean isSkipBqa() {
		return skipBqa;
	}

	public void setSkipBqa(boolean skipBqa) {
		this.skipBqa = skipBqa;
	}

	public BqaProcessing getBqaProcessing() {
		return bqaProcessing;
	}

	public void setBqaProcessing(BqaProcessing bqaProcessing) {
		this.bqaProcessing = bqaProcessing;
	}

	public ArrayList<Slot> getExpectedSlots() {
		if(expectedSlots == null) {
			expectedSlots = new ArrayList<>();
		}
		return expectedSlots;
	}

	public void setExpectedSlots(ArrayList<Slot> expectedSlots) {
		this.expectedSlots = expectedSlots;
	}

	public SYS_UTTER_TYPE getSvcType() {
		return svcType;
	}

	public void setSvcType(SYS_UTTER_TYPE svcType) {
		this.svcType = svcType;
	}

	public int getIfId() {
		return ifId;
	}

	public void setIfId(int ifId) {
		this.ifId = ifId;
	}

	@Override
	public String toString() {
		return "DialogProcessing [currBotUtter=" + currBotUtter + ", sdsDomain=" + sdsDomain + ", expectedSlot="
				+ expectedSlot + ", expectedSlots=" + expectedSlots + ", skipBqa=" + skipBqa + ", bqaProcessing="
				+ bqaProcessing + ", svcType=" + svcType + ", ifId=" + ifId + "]";
	}



	@SuppressWarnings("serial")
	public static class Slot implements Serializable{
		protected String slotName;
		protected String slotValue;
		public Slot() {
			
		}
		public Slot(String name, String value) {
			this.slotName = name;
			this.slotValue = value;
		}
		public String getSlotName() {
			return slotName;
		}
		public void setSlotName(String slotName) {
			this.slotName = slotName;
		}
		public String getSlotValue() {
			return slotValue;
		}
		public void setSlotValue(String slotValue) {
			this.slotValue = slotValue;
		}
		@Override
		public String toString() {
			return "Slot [slotName=" + slotName + ", slotValue=" + slotValue + "]";
		}
		
	}
}
