package ai.mindslab.bizchatbot.commons.session.listener;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.core.Message;
import com.hazelcast.core.MessageListener;

import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;

public class DialogReplyListenerCallable implements Callable<BaseResponse<UtterData>>, MessageListener<UtterData>{

	private Logger logger = LoggerFactory.getLogger(DialogReplyListenerCallable.class);
	
	private UtterData utterReply;
	private static final long DIALOG_TIMEOUT = 5000;
//	private Lock reentrantLock = new ReentrantLock();
	private AtomicBoolean isComp = new AtomicBoolean(false);
	
	public DialogReplyListenerCallable() {
//		reentrantLock.lock();
	}

	@Override
	public void onMessage(Message<UtterData> message) {
		logger.debug("onMessage/{}/{}/{}",Thread.currentThread().getName(),this.toString(),message.getMessageObject());
		try {
			utterReply = message.getMessageObject();
			isComp.set(true);
		}finally {
//			logger.debug("onMessage/unlock-before");
//			try {
//				reentrantLock.unlock();
//			}catch(Throwable t) {
//				reentrantLock.unlock();
//			}
//			logger.debug("onMessage/unlock");
		}
	}

	@Override
	public BaseResponse<UtterData> call() throws Exception {
		BaseResponse<UtterData> resp;
		logger.debug("call/{}/{}/{}", Thread.currentThread().getName(),this.toString(), isComp.get()?"READY":"WAITING");
//		if(utterReply == null) {
//		
//			if(reentrantLock.tryLock(DIALOG_TIMEOUT, TimeUnit.MILLISECONDS)) {
//				logger.debug("call/{}/{}/{} {}", Thread.currentThread().getName(), this.toString(), "got Message", utterReply.toString());
//				try {
//					resp = new BaseResponse<UtterData>();
//					resp.setData(utterReply);
//					return resp;
//				}finally {
//					reentrantLock.unlock();
//				}
//			}else {
//				logger.debug("call/{}/{}/{}", Thread.currentThread().getName(),this.toString(), "timeout");
//				return new BaseResponse<UtterData>(IRestCodes.ERR_CODE_DIALOG_TIMEOUT, IRestCodes.ERR_MSG_DIALOG_TIMEOUT);
//			}
//		}else {
//			resp = new BaseResponse<UtterData>();
//			resp.setData(utterReply);
//			return resp;
//		}
		long st = System.currentTimeMillis();
		while(!isComp.get() && (System.currentTimeMillis() - st < DIALOG_TIMEOUT)) {
			try {
				Thread.sleep(300);
			}catch(InterruptedException e) {
			}
		}
		
		if(isComp.get()) {
			resp = new BaseResponse<UtterData>();
			resp.setData(utterReply);
			return resp;
		}else {
			logger.debug("call/{}/{}/{}", Thread.currentThread().getName(),this.toString(), "timeout");
			return new BaseResponse<UtterData>(IRestCodes.ERR_CODE_DIALOG_TIMEOUT, IRestCodes.ERR_MSG_DIALOG_TIMEOUT);
		}
	}
}
