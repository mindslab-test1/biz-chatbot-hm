package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

public class BasicQaAnswerSimple implements Serializable {

	protected int answerId;
	protected String answer;
	protected int questionId;
	protected String useYn;
	
	public BasicQaAnswerSimple() {
		
	}

	public int getAnswerId() {
		return answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	@Override
	public String toString() {
		return "BasicQaAnswerSimple [answerId=" + answerId + ", answer=" + answer + ", questionId=" + questionId
				+ ", useYn=" + useYn + "]";
	}
}
