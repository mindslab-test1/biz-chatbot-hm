package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ChatbotRichApi implements Serializable{

	public int ifId;
	public String contentTypeCd;
	public ChatbotApiInfo apiData;
	
	public ChatbotRichApi() {
	}

	public int getIfId() {
		return ifId;
	}

	public void setIfId(int ifId) {
		this.ifId = ifId;
	}

	public String getContentTypeCd() {
		return contentTypeCd;
	}

	public void setContentTypeCd(String contentTypeCd) {
		this.contentTypeCd = contentTypeCd;
	}

	public ChatbotApiInfo getApiData() {
		return apiData;
	}

	public void setApiData(ChatbotApiInfo apiData) {
		this.apiData = apiData;
	}

	@Override
	public String toString() {
		return "ChatbotRichApi [ifId=" + ifId + ", contentTypeCd=" + contentTypeCd + ", apiData=" + apiData + "]";
	}

}
