package ai.mindslab.bizchatbot.commons.interfaces;

import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;

public interface IChatbotInterface {

	public BaseResponse<UtterData> sendOpenDialog(String sessionId);
	public BaseResponse<UtterData> sendUserUtter(String sessionId);
	public BaseResponse<UtterData> closeDialog(String sessionId);
}
