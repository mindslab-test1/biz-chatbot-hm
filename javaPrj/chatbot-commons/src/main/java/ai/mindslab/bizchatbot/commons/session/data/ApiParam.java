package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ApiParam implements Serializable{

	protected String key;
	protected String value;
	protected String mapping;
	
	public ApiParam() {
		
	}
	
	public ApiParam(String key, String value, String mapping) {
		this.key = key;
		this.value = value;
		this.mapping = mapping;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getMapping() {
		return mapping;
	}

	public void setMapping(String mapping) {
		this.mapping = mapping;
	}

	@Override
	public String toString() {
		return "ApiParam [key=" + key + ", value=" + value + ", mapping=" + mapping + "]";
	}
}
