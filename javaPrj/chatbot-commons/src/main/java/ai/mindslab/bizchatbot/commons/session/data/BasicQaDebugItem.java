package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;
import java.util.List;

public class BasicQaDebugItem implements Serializable{

	protected String question;
	protected String questionMorph;
	protected int morphCount;
	protected int domainId;
	protected int res;
	protected float score;
	protected String mainYn;
	protected List<BasicQaAnswerSimple> answerList;
	
	public BasicQaDebugItem() {
	}
	
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getQuestionMorph() {
		return questionMorph;
	}

	public void setQuestionMorph(String questionMorph) {
		this.questionMorph = questionMorph;
	}

	public int getMorphCount() {
		return morphCount;
	}

	public void setMorphCount(int morphCount) {
		this.morphCount = morphCount;
	}

	public int getDomainId() {
		return domainId;
	}

	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}

	public int getRes() {
		return res;
	}

	public void setRes(int res) {
		this.res = res;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public String getMainYn() {
		return mainYn;
	}

	public void setMainYn(String mainYn) {
		this.mainYn = mainYn;
	}

	public List<BasicQaAnswerSimple> getAnswerList() {
		return answerList;
	}

	public void setAnswerList(List<BasicQaAnswerSimple> answerList) {
		this.answerList = answerList;
	}

	@Override
	public String toString() {
		return "BasicQaOutItem [question=" + question + ", questionMorph=" + questionMorph + ", morphCount="
				+ morphCount + ", domainId=" + domainId + ", res=" + res + ", score=" + score + ", mainYn=" + mainYn
				+ ", answerList=" + answerList + "]";
	}

}
