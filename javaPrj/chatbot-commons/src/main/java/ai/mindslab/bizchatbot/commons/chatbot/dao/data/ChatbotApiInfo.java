package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ChatbotApiInfo implements Serializable{

	protected int ifId;
	protected String apiName;
	protected String endpoint;
	protected String method;
	protected String contentType;
	protected List<ChatbotApiParam> paramList;
	protected ChatbotApiJsonTemplate jsonTemplate;
	
	@JsonIgnore
	protected String createdDtm;
	@JsonIgnore
	protected String updatedDtm;
	@JsonIgnore
	protected String creatorId;
	@JsonIgnore
	protected String updatorId;
	
	public ChatbotApiInfo() {
	}

	public int getIfId() {
		return ifId;
	}

	public void setIfId(int ifId) {
		this.ifId = ifId;
	}

	public String getApiName() {
		return this.apiName;
	}
	
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}
	
	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	public List<ChatbotApiParam> getParamList() {
		if(paramList == null) {
			paramList = new ArrayList<>();
		}
		return paramList;
	}

	public void setParamList(List<ChatbotApiParam> paramList) {
		this.paramList = paramList;
	}

	public ChatbotApiJsonTemplate getJsonTemplate() {
		return jsonTemplate;
	}

	public void setJsonTemplate(ChatbotApiJsonTemplate jsonTemplate) {
		this.jsonTemplate = jsonTemplate;
	}

	@Override
	public String toString() {
		return "ChatbotApiInfo [ifId=" + ifId + ", endpoint=" + endpoint + ", method=" + method + ", contentType="
				+ contentType + ", paramList=" + paramList + ", jsonTemplate=" + jsonTemplate + ", createdDtm="
				+ createdDtm + ", updatedDtm=" + updatedDtm + ", creatorId=" + creatorId + ", updatorId=" + updatorId
				+ "]";
	}

}
