package ai.mindslab.bizchatbot.commons.chatbot.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotDomainSimple;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotIntetFinderSdsData;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotMsg;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.NoticeInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.SimpleClassifierData;

@Mapper
public interface ChatbotMapper {

	public List<ChatbotInfo> getChatbotInfoList(HashMap<String,Object> params);
	public long getChatbotInfoListCount(HashMap<String,Object> params);
	public ChatbotInfo getChatbotInfo(int chatbotId);
	public List<ChatbotMsg> getChatbotMessages(HashMap<String,Object> params);
	
	public ChatbotConfig getChatbotConfig(int botId);
	public List<ChatbotDomainSimple> getBqaDomainList(int confId);
	public List<ChatbotDomainSimple> getSdsDomainList(int confId);
	
	public List<SimpleClassifierData> getSimpleClassifer(int botId);
	public List<ChatbotIntetFinderSdsData> getIntentFinderSds (int botId);
	
	public List<NoticeInfo> getChatbotNotice(int botId);
}
	