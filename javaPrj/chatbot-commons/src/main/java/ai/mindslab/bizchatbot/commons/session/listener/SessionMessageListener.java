package ai.mindslab.bizchatbot.commons.session.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.core.Message;
import com.hazelcast.core.MessageListener;

import ai.mindslab.bizchatbot.commons.session.data.UtterData;

public class SessionMessageListener implements MessageListener<UtterData>{

	private Logger logger = LoggerFactory.getLogger(SessionMessageListener.class);
	
	private String topic;
	
	public SessionMessageListener(String topic) {
		this.topic = topic;
	}

	@Override
	public void onMessage(Message<UtterData> message) {
		logger.info("onMessage/topic:{}/{}",topic,message.getMessageObject());
		//QQQ
		
		
	}
	
}
