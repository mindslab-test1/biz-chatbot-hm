package ai.mindslab.bizchatbot.commons.chatbot.dao;


import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotButton;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichCarousel;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichImage;


@Mapper
public interface ChatbotRichContentMapper {

	/**
	 * 
	 * @param rcId rich content Id
	 * @return
	 */
	public ChatbotRichContent getRichConent(int rcId);
	/**
	 * 
	 * @param rcId rich content id
	 * @return
	 */
	public List<ChatbotButton> getButtonList(int rcId);
	public ChatbotRichImage getRichImage(int rcId);
	public List<ChatbotRichCarousel> getRichCarousel(int rcId);
}
