package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SdsProjectPortHis implements Serializable{

	private int hisId;
	private int botId;
	private String sessionId;
	private int projectPort;
	private String sdsIp;
	private String openDtm;
	private String closeDtm;
	private String closeReasonCd;
	
	public SdsProjectPortHis() {
	}

	public int getHisId() {
		return hisId;
	}

	public void setHisId(int hisId) {
		this.hisId = hisId;
	}

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public int getProjectPort() {
		return projectPort;
	}

	public void setProjectPort(int projectPort) {
		this.projectPort = projectPort;
	}

	public String getSdsIp() {
		return sdsIp;
	}

	public void setSdsIp(String sdsIp) {
		this.sdsIp = sdsIp;
	}

	public String getOpenDtm() {
		return openDtm;
	}

	public void setOpenDtm(String openDtm) {
		this.openDtm = openDtm;
	}

	public String getCloseDtm() {
		return closeDtm;
	}

	public void setCloseDtm(String closeDtm) {
		this.closeDtm = closeDtm;
	}

	public String getCloseReasonCd() {
		return closeReasonCd;
	}

	public void setCloseReasonCd(String closeReasonCd) {
		this.closeReasonCd = closeReasonCd;
	}

	@Override
	public String toString() {
		return "SdsProjectPortHis [hisId=" + hisId + ", botId=" + botId + ", sessionId=" + sessionId + ", projectPort="
				+ projectPort + ", sdsIp=" + sdsIp + ", openDtm=" + openDtm + ", closeDtm=" + closeDtm
				+ ", closeReasonCd=" + closeReasonCd + "]";
	}
}
