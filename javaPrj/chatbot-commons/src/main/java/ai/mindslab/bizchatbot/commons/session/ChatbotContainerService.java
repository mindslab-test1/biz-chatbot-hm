package ai.mindslab.bizchatbot.commons.session;

import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.commons.codes.ISDSCodes;
import ai.mindslab.bizchatbot.commons.session.data.ChatbotResponse;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;

@Service
public class ChatbotContainerService implements ISDSCodes{

	public ChatbotContainerService() {
		// TODO Auto-generated constructor stub
	}

	public ChatbotResponse sendTextToChatbot(UtterData utter) throws Exception{
		
		//QQQ implement
		//get bot utter
		ChatbotResponse resp = new ChatbotResponse();
		if(isValidSDSCode(resp.getSdsResp())) {
			return doSysCodeResp(resp);
		}
		
		// bypassing resp to user
		
		return resp;
	}
	
	
	private boolean isValidSDSCode(String respCode) {
		
		return respCode.startsWith(SDS_CODE_PREFIX);
	}
	
	private ChatbotResponse doSysCodeResp(ChatbotResponse resp) {
		
		UtterData orgUtter = resp.getOrgUtter();
		//QQQ processing
		//get data & send to user
		//make bot utter
		UtterData botUtter = new UtterData();
		resp.setBotUtter(botUtter);
		
		return resp;
	}
}
