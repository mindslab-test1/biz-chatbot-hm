package ai.mindslab.bizchatbot.commons.exceptions;

public class BizChatbotHttpException extends BizChatbotException{

	public BizChatbotHttpException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BizChatbotHttpException(int code, String msg, String message) {
		super(code, msg, message);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotHttpException(int code, String msg) {
		super(code, msg);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotHttpException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotHttpException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotHttpException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotHttpException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
