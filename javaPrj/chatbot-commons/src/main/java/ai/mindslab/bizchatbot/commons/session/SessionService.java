package ai.mindslab.bizchatbot.commons.session;

import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.hazelcast.core.IMap;
import com.hazelcast.core.ITopic;
import com.hazelcast.core.Message;
import com.hazelcast.core.MessageListener;

import ai.mindslab.bizchatbot.commons.chatbot.dao.ChatbotMapper;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotInfo;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.session.data.DialogData;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.UserData;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.session.listener.SessionLifecycleListener;
import ai.mindslab.bizchatbot.commons.session.listener.SessionMessageListener;
import ai.mindslab.bizchatbot.commons.session.listener.SessionUpdatedListener;

@Service
public class SessionService implements IChatbotConst{

	private Logger logger = LoggerFactory.getLogger(SessionService.class);
	
	@Autowired
	private ChatbotMapper chatbot;
	
	@Autowired
	@Qualifier("hazelClientCommon")
	private HazelClientCommon hazel;
	
	@PostConstruct
	private void postConstruct() {
		//just init client on post constructor
		try {
			hazel.getClient().getMap(MAP_NAME_SESSION);
			hazel.getClient().addDistributedObjectListener(new SessionLifecycleListener());
		}catch(Exception e) {
			logger.error("hazelcast client init error/{}",e.getMessage());
		}
	}
	
	public String generateSessionId() {
		//QQQ other algorithm
		String uuid = UUID.randomUUID().toString();
        return uuid;
	}
	
	
	public DialogSession openNewSession(UserData user, int chatbotId, HttpServletRequest req, boolean bStore) throws Exception{
		logger.info("openNewSession/START/{},{}",user, chatbotId);
		
		DialogSession ses = new DialogSession();
		ses.setUserData(user);
		DialogData dialog = new DialogData();
		dialog.setBotId(chatbotId);
		String remoteAddr = req.getHeader("X-FORWARDED-FOR");
        if (remoteAddr == null) {
			remoteAddr = req.getRemoteAddr();
		}

		if (remoteAddr.substring(0, 7).equals("::ffff:")) {
			remoteAddr = remoteAddr.substring(7);
		}

		ses.setRemoteAddr(remoteAddr);
		logger.info("openNewSession  remoteAddr  : {}", remoteAddr);
		try {
			ChatbotInfo cb = getChatbotInfo(chatbotId);
			dialog.setBotName(cb.getBotName());
			
			ses.setDialog(dialog);
			ses.setSessionId(generateSessionId());
			
			//set processing data
			DialogProcessing processing = new DialogProcessing();
			ses.setProcessing(processing);
			
			//store to hazelcast
			if(bStore) {
				hazel.getClient().getMap(MAP_NAME_SESSION).put(ses.getSessionId(), ses);
			}
			
			//test
			//hazel.getClient().getMap(MAP_NAME_SESSION).addEntryListener(new SessionUpdatedListener(ses.getSessionId()) , true);
//			ITopic<UtterData> topic = hazel.getClient().getReliableTopic(TOPIC_DIALOG_PREFIX+ses.getSessionId());
//			topic.addMessageListener(new SessionMessageListener(TOPIC_DIALOG_PREFIX+ses.getSessionId()));
			
			return ses;
		}catch(Exception e) {
			logger.warn("openNewSession/{}",e.getMessage());
			throw e;
		}finally {
			logger.info("openNewSession/END");
		}
	}
	
	@Cacheable(key= "#botId", cacheNames="chatbot-objects", cacheManager="hazelCacheManager")
	public ChatbotInfo getChatbotInfo(int chatbotId) throws Exception{
		ChatbotInfo info = null;
		try {
			info = chatbot.getChatbotInfo(chatbotId);
		}catch(Exception e) {
			logger.warn("getChatbotInfo/{}", e.getMessage());
			throw e;
		}
		return info;
	}
	
	public void storeDialogSession(DialogSession session) throws Exception{
		logger.info("storeDialogSession/{}",session);
		try {
			hazel.getClient().getMap(MAP_NAME_SESSION).put(session.getSessionId(), session);
		}catch(Exception e) {
			logger.warn("storeDialogSession/{}",e.getMessage());
			throw e;
		}
	}
	
	public void closeSession(UserData user, int chatbotId, String sessionId) throws Exception{
		logger.info("closeSession/START/clear session from hazel {},{},{}",user, chatbotId, sessionId);
		
		try {
			//store to hazelcast
			hazel.getClient().getMap(MAP_NAME_SESSION).remove(sessionId);
			//QQQ database?
		}catch(Exception e) {
			throw e;
		}finally {
			logger.info("closeSession/END");
		}
	}
	
	public boolean isValidSession(String sessionId) {
		boolean bValid = false;
//		logger.info("isValidSession/START/{}", sessionId);
		try {
			//store to hazelcast
			IMap<String, DialogSession> s = hazel.getClient().getMap(MAP_NAME_SESSION);
			if(s != null) {
				bValid = true;
			}
		}catch(Exception e) {
			bValid = false;
//			throw e;
		}finally {
//			logger.info("isValidSession/END");
		}
		return bValid;
	}
}
