package ai.mindslab.bizchatbot.commons.exceptions;

@SuppressWarnings("serial")
public class BizChatbotAuthException extends BizChatbotException {

	public BizChatbotAuthException() {
		// TODO Auto-generated constructor stub
	}

	public BizChatbotAuthException(int code, String msg) {
		super(code, msg);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotAuthException(int code, String msg, String message) {
		super(code, msg, message);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotAuthException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotAuthException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotAuthException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotAuthException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
