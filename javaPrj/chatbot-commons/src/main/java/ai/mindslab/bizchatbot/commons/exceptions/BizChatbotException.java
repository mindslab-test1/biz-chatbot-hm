package ai.mindslab.bizchatbot.commons.exceptions;

public class BizChatbotException extends Exception {

	private int errCode;
	private String errMsg;
	
	public BizChatbotException() {
		// TODO Auto-generated constructor stub
	}
	
	public BizChatbotException(int code, String msg) {
		this.errCode = code;
		this.errMsg = msg;
	}

	public BizChatbotException(int code, String msg, String message) {
		super(message);
		this.errCode = code;
		this.errMsg = msg;
	}
	
	public BizChatbotException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BizChatbotException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}
	
	public int getErrCode() {
		return errCode;
	}
	
	public String getErrMsg() {
		return errMsg;
	}

}
