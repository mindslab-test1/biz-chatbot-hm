package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ChatbotApiInfoDisp implements Serializable{

	protected int ifId;
	protected int workspaceId;
	protected String apiName;
	protected String description;
	protected String endpoint;
	protected String method;
	protected String contentType;
	
	@JsonIgnore
	protected String createdDtm;
	@JsonIgnore
	protected String updatedDtm;
	@JsonIgnore
	protected String creatorId;
	@JsonIgnore
	protected String updatorId;
	
	public ChatbotApiInfoDisp() {
	}

	public int getIfId() {
		return ifId;
	}

	public void setIfId(int ifId) {
		this.ifId = ifId;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	public int getWorkspaceId() {
		return workspaceId;
	}

	public void setWorkspaceId(int workspaceId) {
		this.workspaceId = workspaceId;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "ChatbotApiInfoDisp [ifId=" + ifId + ", workspaceId=" + workspaceId + ", apiName=" + apiName
				+ ", description=" + description + ", endpoint=" + endpoint + ", method=" + method + ", contentType="
				+ contentType + ", createdDtm=" + createdDtm + ", updatedDtm=" + updatedDtm + ", creatorId=" + creatorId
				+ ", updatorId=" + updatorId + "]";
	}

}
