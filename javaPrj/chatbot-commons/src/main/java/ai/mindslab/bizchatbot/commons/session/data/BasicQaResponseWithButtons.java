package ai.mindslab.bizchatbot.commons.session.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ai.mindslab.bizchatbot.commons.data.BaseResponse;

@JsonIgnoreProperties({"code", "msg"})
public class BasicQaResponseWithButtons extends BaseResponse<BasicQaOutWithButtons>{

	public BasicQaResponseWithButtons() {
	}

}
