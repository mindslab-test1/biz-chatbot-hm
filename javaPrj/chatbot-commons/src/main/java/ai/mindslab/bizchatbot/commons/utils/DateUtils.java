package ai.mindslab.bizchatbot.commons.utils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

	private static DateTimeFormatter dateForm  = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


	public DateUtils() {
	}
	
	public static Date getNow() {
		Calendar cal = Calendar.getInstance(Locale.KOREA);//QQQ locale
		return cal.getTime();
	}
	
	public static Date getAfterMin(int diff) {
		Calendar cal = Calendar.getInstance(Locale.KOREA);
		cal.add(Calendar.MINUTE, diff);
		return cal.getTime();
	}

	
	public static long diffSec(Date oldDate) {
		if(oldDate == null) {
			return -1L;
		}
		Date now = getNow();
		long deiffMilli =  now.getTime() - oldDate.getTime();
		return deiffMilli / 1000;
	}

	public static String convertUtcToLocalDate(String utcTime){
		ZoneId zoneId = ZoneId.systemDefault();
		Instant in = Instant.parse(utcTime);
		ZonedDateTime localTime = in.atZone(zoneId);

		return localTime.format(dateForm);
	}

}
