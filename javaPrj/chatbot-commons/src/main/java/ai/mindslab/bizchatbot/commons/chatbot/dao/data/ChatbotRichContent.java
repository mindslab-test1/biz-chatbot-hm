package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties({"createdDtm", "updatedDtm", "creatorId", "updatorId", "rcId"})
public class ChatbotRichContent implements Serializable{

	private int rcId;
	private String contentTypeCd;
	private String title;
	private String content;
	private String imgPath;
	private String linkPath;
	
//	@JsonIgnore
	private Date createdDtm;
//	@JsonIgnore
	private Date updatedDtm;
//	@JsonIgnore
	private String creatorId;
//	@JsonIgnore
	private String updatorId;
	
	private List<ChatbotButton> buttons;
	private ChatbotRichImage image;
	private List<ChatbotRichCarousel> carousels;
	
	public ChatbotRichContent() {
	}

	public int getRcId() {
		return rcId;
	}

	public void setRcId(int rcId) {
		this.rcId = rcId;
	}

	public String getContentTypeCd() {
		return contentTypeCd;
	}

	public void setContentTypeCd(String contentTypeCd) {
		this.contentTypeCd = contentTypeCd;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getLinkPath() {
		return linkPath;
	}

	public void setLinkPath(String linkPath) {
		this.linkPath = linkPath;
	}

	public Date getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(Date createdDtm) {
		this.createdDtm = createdDtm;
	}

	public Date getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(Date updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}
	
	public List<ChatbotButton> getButtons() {
		if(buttons == null) {
			buttons = new ArrayList<>();
		}
		return buttons;
	}

	public void setButtons(List<ChatbotButton> buttons) {
		this.buttons = buttons;
	}

	public ChatbotRichImage getImage() {
		return image;
	}

	public void setImage(ChatbotRichImage image) {
		this.image = image;
	}

	public List<ChatbotRichCarousel> getCarousels() {
		if(carousels == null) {
			carousels = new ArrayList<>();
		}
		return carousels;
	}

	public void setCarousels(List<ChatbotRichCarousel> carousels) {
		this.carousels = carousels;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
