package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@SuppressWarnings("serial")
public class SdsDebugRespData implements Serializable{

	protected String debug;
	protected String time;
	protected String err_msg;
	
	protected int max_cnt;
	protected String msg;
	protected String result;
	
	public SdsDebugRespData() {
	}

	public String getDebug() {
		return debug;
	}

	public void setDebug(String debug) {
		this.debug = debug;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getErr_msg() {
		return err_msg;
	}

	public void setErr_msg(String err_msg) {
		this.err_msg = err_msg;
	}

	public int getMax_cnt() {
		return max_cnt;
	}

	public void setMax_cnt(int max_cnt) {
		this.max_cnt = max_cnt;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "SdsDebugRespData [debug=" + debug + ", time=" + time + ", err_msg=" + err_msg + ", max_cnt=" + max_cnt
				+ ", msg=" + msg + ", result=" + result + "]";
	}
}
