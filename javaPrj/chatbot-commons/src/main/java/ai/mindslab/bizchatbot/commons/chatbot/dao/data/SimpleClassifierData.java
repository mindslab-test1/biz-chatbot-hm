package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;

public class SimpleClassifierData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1;
	
	
	protected int scfId;
	protected int botId;
	protected String pattern;
	protected String sdsDomain;
	
	public SimpleClassifierData() {
	}

	public int getScfId() {
		return scfId;
	}

	public void setScfId(int scfId) {
		this.scfId = scfId;
	}

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getSdsDomain() {
		return sdsDomain;
	}

	public void setSdsDomain(String sdsDomain) {
		this.sdsDomain = sdsDomain;
	}

	@Override
	public String toString() {
		return "SimpleClassifierData [scfId=" + scfId + ", botId=" + botId + ", pattern=" + pattern + ", sdsDomain="
				+ sdsDomain + "]";
	}
	
}
