package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;

import ai.mindslab.bizchatbot.commons.codes.ISessionCodes.SESSION_STATUS;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SessionInfo implements Serializable {
	protected SESSION_STATUS status;
	protected String sessionId;
	
	public SessionInfo() {
	}

	public SESSION_STATUS getStatus() {
		return status;
	}

	public void setStatus(SESSION_STATUS status) {
		this.status = status;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
