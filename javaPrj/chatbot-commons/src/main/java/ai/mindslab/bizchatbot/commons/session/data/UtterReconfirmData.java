package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UtterReconfirmData implements Serializable{

	protected String msg;
	protected ChatbotRichContent richContent;
	
	public UtterReconfirmData() {
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public ChatbotRichContent getRichContent() {
		return richContent;
	}

	public void setRichContent(ChatbotRichContent richContent) {
		this.richContent = richContent;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
