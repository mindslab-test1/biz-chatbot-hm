package ai.mindslab.bizchatbot.commons.chatbot.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.DialogSynonymData;

@Mapper
public interface DialogSynonymMapper {

	public List<DialogSynonymData> getSynonymList(int workspaceId);
	public Integer getWorkspaceId(int botId);
}
