package ai.mindslab.bizchatbot.commons.utils;

import java.util.Vector;

public class StopWatch {

	protected long st = 0L;
	protected long lap = 0L;
	protected long end = 0L;
	protected Vector<Long> heap = new Vector<>(30);
	
	public static StopWatch getInstance() {
		return new StopWatch();
	}
	
	private StopWatch() {
		st = System.currentTimeMillis();
		lap = st;
	}
	
	
	public long getElapsed() {
		long tmp = System.currentTimeMillis();
		long out = tmp - lap;
		lap = tmp;
		heap.add(out);
		return out;	
	}
	
	public long getTotalElapsed() {
		
		end = System.currentTimeMillis();
		return end - st;
	}
	
	public void reset() {
		st = System.currentTimeMillis();
		lap = 0L;
		end = 0L;
		heap.clear();
	}
	
	public float getAvg() {
		long sum = 0L;
		for(long l:heap) {
			sum += l;
		}
		return (float)sum/(float)heap.size();
	}

}
