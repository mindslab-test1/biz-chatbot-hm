package ai.mindslab.bizchatbot.commons.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.TALK_RESULT;
import ai.mindslab.bizchatbot.commons.session.data.SdsUtterRespData;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;

@SuppressWarnings("serial")
public class RespDialog implements Serializable{
	
	protected String sessionId;
	protected int chatbotId;
	protected String userId;
	protected TALK_RESULT result;
//	protected List<String> greetings;
	protected UtterData uttrData;
	
	public RespDialog() {
	}
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public int getChatbotId() {
		return chatbotId;
	}

	public void setChatbotId(int chatbotId) {
		this.chatbotId = chatbotId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public TALK_RESULT getResult() {
		return result;
	}

	public void setResult(TALK_RESULT result) {
		this.result = result;
	}
	
//	public List<String> getGreetings() {
//		if(greetings == null) {
//			greetings = new ArrayList<String>();
//		}
//		return greetings;
//	}
//
//	public void setGreetings(List<String> greetings) {
//		this.greetings = greetings;
//	}
	
	

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public UtterData getUttrData() {
		return uttrData;
	}

	public void setUttrData(UtterData uttrData) {
		this.uttrData = uttrData;
	}
}
