package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ChatbotDomainSimple implements Serializable{

	protected int confId;
	protected int domainId;
	protected String domainName;
	
	public ChatbotDomainSimple() {
	}

	public int getConfId() {
		return confId;
	}

	public void setConfId(int confId) {
		this.confId = confId;
	}

	public int getDomainId() {
		return domainId;
	}

	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	@Override
	public String toString() {
		return "ChatbotDomainSimple [confId=" + confId + ", domainId=" + domainId + ", domainName=" + domainName + "]";
	}
}
