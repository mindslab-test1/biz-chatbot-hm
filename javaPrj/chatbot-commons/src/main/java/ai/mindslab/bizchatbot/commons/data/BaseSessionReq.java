package ai.mindslab.bizchatbot.commons.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class BaseSessionReq implements Serializable{

	protected String sessionId;
	
	public BaseSessionReq() {
		// TODO Auto-generated constructor stub
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "BaseSessionReq [sessionId=" + sessionId + "]";
	}

}
