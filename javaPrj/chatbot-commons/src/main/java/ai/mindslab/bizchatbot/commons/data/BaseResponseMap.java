package ai.mindslab.bizchatbot.commons.data;

import java.util.HashMap;

@SuppressWarnings("serial")
public class BaseResponseMap extends BaseResponse<HashMap>{
    public BaseResponseMap() {
        
    }
    
    @Override
    public String toString() {
        return "MapBaseResponse [" + getData() != null ? getData().toString() : "" + "]";
    }
}
