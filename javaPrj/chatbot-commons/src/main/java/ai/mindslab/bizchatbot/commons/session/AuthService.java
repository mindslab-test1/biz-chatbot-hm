package ai.mindslab.bizchatbot.commons.session;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotAuthException;

@Component
public class AuthService implements IRestCodes{
	private Logger logger = LoggerFactory.getLogger(AuthService.class);
	
	@Autowired
	private Environment env;
	
	private JWTVerifier verifier = null;//reuse
	
	public String createToken(String uid) throws Exception{
		String token = "";
		String issuer;
		String claim_uid = "uid";
		try {
			issuer = env.getProperty("bizchatbot.jwt.issuer");
		    Algorithm algorithm = Algorithm.HMAC256( getSecret() );
		    token = JWT.create()
		        .withIssuer(issuer)
		        .withExpiresAt( getExpireDate() )
		        .withClaim(claim_uid, uid)
		        .sign(algorithm);
		} catch (UnsupportedEncodingException e){
		    //UTF-8 encoding not supported
			logger.warn("createToken/{}",e.getMessage());
			throw e;
		} catch (JWTCreationException e){
		    //Invalid Signing configuration / Couldn't convert Claims.
			logger.warn("createToken/{}",e.getMessage());
			//throw e;
			throw new BizChatbotAuthException(ERR_CODE_AUTH_ERROR, "Auth token creation error.");
		}

		return token;
	}
	
	private String getSecret() {
		String secret = env.getProperty("bizchatbot.jwt.secret");
		return secret;
	}
	
	public Date getExpireDate(){
		int expDate = 10;
		try {
			expDate = Integer.parseInt(env.getProperty("bizchatbot.jwt.expiry"));//days
		}catch(Exception e) {
			expDate = 10;
			logger.error("jwt expiry parsing error. {}", env.getProperty("bizchatbot.jwt.expiry"));
		}
		Calendar cal = Calendar.getInstance(Locale.KOREAN);
		cal.add(Calendar.DATE, expDate);
		Date exp = cal.getTime();
		return exp;
	}
	
	public DecodedJWT verifyToken(String token) throws BizChatbotAuthException, Exception{
		DecodedJWT jwt = null;
		try {
		    JWTVerifier vf = getVerifier();
		    jwt = vf.verify(token);
		    return jwt;
		} catch (UnsupportedEncodingException e){
		    //UTF-8 encoding not supported
			logger.warn("verifyToken/{}",e.getMessage());
			throw new BizChatbotAuthException(ERR_CODE_AUTH_ERROR, "Auth token encoding error");
		} catch (JWTVerificationException e){
		    //Invalid signature/claims
			logger.warn("verifyToken/{}",e.getMessage());
			throw new BizChatbotAuthException(ERR_CODE_AUTH_ERROR, "Auth token verification error.");
		} catch (Exception e){
			logger.warn("verifyToken/{}",e.getMessage());
			throw new BizChatbotAuthException(ERR_CODE_AUTH_ERROR, "Auth token verification error.");
		}
	}
	
	public DecodedJWT verifyToken(HttpServletRequest req) throws BizChatbotAuthException, Exception{
		String auth = req.getHeader("Authorization");
		logger.debug("Authrization: {}",auth);
		if(StringUtils.isEmpty(auth)){
			throw new BizChatbotAuthException(ERR_CODE_AUTH_ERROR, "auth token is empty");
		}
		auth = auth.substring(7);//Bearer
		return verifyToken(auth);
	}
	
	private JWTVerifier getVerifier() throws Exception{
		if(verifier == null){
			String issuer = env.getProperty("bizchatbot.jwt.issuer");
			
			Algorithm algorithm = Algorithm.HMAC256( getSecret() );
		    verifier = JWT.require(algorithm)
			        .withIssuer(issuer)
			        .acceptExpiresAt(0)
			        .build();
		}
		return verifier;
	} 
}
