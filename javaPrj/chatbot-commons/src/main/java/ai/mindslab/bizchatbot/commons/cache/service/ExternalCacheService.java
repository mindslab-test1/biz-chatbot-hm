package ai.mindslab.bizchatbot.commons.cache.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;

@Component
@ConditionalOnClass(HazelClientCommon.class)
public class ExternalCacheService {
	private Logger logger = LoggerFactory.getLogger(ExternalCacheService.class);
	
	@Autowired
	private CacheManager cacheManager;

	public void clearCacheConfig(String cacheName) {
		logger.info("clearCacheConfig/{}",cacheName);
		if(cacheManager != null) {
			Cache cache = cacheManager.getCache(cacheName);
			if(cache != null) {
				cache.clear();
			}
		}
	}

}
