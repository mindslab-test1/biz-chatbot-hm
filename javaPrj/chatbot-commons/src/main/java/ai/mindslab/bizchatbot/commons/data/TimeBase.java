package ai.mindslab.bizchatbot.commons.data;

public class TimeBase<T> {

	private long timeStamp;
	private T data;
	
	public TimeBase() {
		timeStamp = System.currentTimeMillis();
	}
	
	public TimeBase(T data) {
		this.data = data;
		timeStamp = System.currentTimeMillis();
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	public boolean isExpired(long currTimestamp, long timeout) {
		return currTimestamp - timeStamp > timeout;
	}
	
	public boolean isExpired(long timeout) {
		return System.currentTimeMillis() - timeStamp > timeout;
	}

	@Override
	public String toString() {
		return "TimeBase [timeStamp=" + timeStamp + ", data=" + data + "]";
	}
}
