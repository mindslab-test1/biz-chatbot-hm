package ai.mindslab.bizchatbot.commons.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.commons.session.data.UserData;

@Service
public class UserService {

	private Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	private AuthService auth;
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}
	
	public UserData login(String userId) throws Exception{
		logger.info("login/START/{}",userId);
		UserData user = null;
		try {
			user = new UserData();
			user.setUserId(userId);
			user.setUserName("no name");//QQQ
			user.setToken(auth.createToken(userId));
			//QQQ user info? which database?
			logger.debug("user:{}",user);
			return user;
		}catch(Exception e){
			logger.warn("login/{}",e.getMessage());
			throw e;
		}finally {
			logger.info("login/END");
		}
	}

}
