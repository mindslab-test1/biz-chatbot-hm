package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
public class BqaProcessing implements Serializable{

	protected String contentTypeCd;
	protected String respCode;
	protected int rcId;
	protected List<ApiParam> params;
	
	public BqaProcessing() {
	}

	public String getContentTypeCd() {
		return contentTypeCd;
	}

	public void setContentTypeCd(String contentTypeCd) {
		this.contentTypeCd = contentTypeCd;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public int getRcId() {
		return rcId;
	}

	public void setRcId(int rcId) {
		this.rcId = rcId;
	}

	public List<ApiParam> getParams() {
		if(params == null) {
			params = new ArrayList<>();
		}
		return params;
	}

	public void setParams(List<ApiParam> params) {
		this.params = params;
	}
}
