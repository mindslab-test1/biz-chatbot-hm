package ai.mindslab.bizchatbot.commons.codes;

public interface ISDSCodes {

	public static String SDS_CODE_PREFIX = "__RESP_SDS__";
	
	public static String REQUEST_INIT_DIALOG = "init_dialog_system";
	public static String REQUEST_USER_UTTER = "dialog_system_user_utter";
	public static String REQUEST_STOP_DIALOG = "stop_dialog_system";
	public static String REQUEST_DEBUG_DIALOG = "tail_log_learn_slu";
	public static String REQUEST_LEARN_SLU = "learn_slu";
	public static String REQUEST_TAIL_LEARN_SLU = "tail_log_learn_slu";
	
	
	public static int MAX_LINE = 4096;
	public static int MAX_PACKET = MAX_LINE / 2;
	
	
//	public static String CMD_KEY_SSH_PASS = "sshpass";
//	public static String CMD_KEY_SSH_NAME = "sshname";
	public static String CMD_KEY_USERNAME = "user_name";
	public static String CMD_KEY_DOMAIN = "domain";
	public static String CMD_KEY_PROJECT_PORT = "project_port";
	public static String CMD_KEY_LOG_FILENAME = "log_filename";
	
}
