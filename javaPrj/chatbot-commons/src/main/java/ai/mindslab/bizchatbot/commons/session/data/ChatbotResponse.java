package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

@SuppressWarnings("serial")
public class ChatbotResponse implements Serializable {

	protected UtterData orgUtter;
	protected UtterData botUtter;
	protected String sdsResp;
	
	public ChatbotResponse() {
	}
	
	public String getSdsResp() {
		return sdsResp;
	}

	public void setSdsResp(String sdsResp) {
		this.sdsResp = sdsResp;
	}
	
	public UtterData getOrgUtter() {
		return orgUtter;
	}

	public void setOrgUtter(UtterData orgUtter) {
		this.orgUtter = orgUtter;
	}
	
	public UtterData getBotUtter() {
		return botUtter;
	}

	public void setBotUtter(UtterData botUtter) {
		this.botUtter = botUtter;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
