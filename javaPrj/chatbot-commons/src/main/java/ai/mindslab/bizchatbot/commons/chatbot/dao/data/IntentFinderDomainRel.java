package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;

public class IntentFinderDomainRel implements Serializable{

	protected int relId;
	protected int botId;
	protected String intentName;
	protected String sdsDomain;
	protected String createdDtm;
	protected String updatedDtm;
	protected String creatorId;
	protected String updatorId;
	
	public IntentFinderDomainRel() {
	}

	public int getRelId() {
		return relId;
	}

	public void setRelId(int relId) {
		this.relId = relId;
	}

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public String getIntentName() {
		return intentName;
	}

	public void setIntentName(String intentName) {
		this.intentName = intentName;
	}

	public String getSdsDomain() {
		return sdsDomain;
	}

	public void setSdsDomain(String sdsDomain) {
		this.sdsDomain = sdsDomain;
	}

	public String getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	@Override
	public String toString() {
		return "IntentFinderDomainRel [relId=" + relId + ", botId=" + botId + ", intentName=" + intentName
				+ ", sdsDomain=" + sdsDomain + ", createdDtm=" + createdDtm + ", updatedDtm=" + updatedDtm
				+ ", creatorId=" + creatorId + ", updatorId=" + updatorId + "]";
	}
}
