package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRichContent;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.DIALOG_SPEAKER;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.SYS_UTTER_TYPE;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties({"contentJson", "utter_id"})
@JsonPropertyOrder({"speaker", "sysUtterType" })
public class UtterData implements Serializable{

	protected static final long serialVersionUID = 2L;
	protected DIALOG_SPEAKER speaker;
	protected int utter_id;
	protected int botId;
	protected String userId;
	protected String sessionId;
	protected Date timestamp;
	protected String sentence;
	protected int dialogSeq;
	protected ChatbotRichContent richContent;
	protected SdsUtterRespData debug;
	protected BasicQaResponse bqaDebug;
	protected BasicQaResponseWithButtons bqaDebugEx;
	protected float sluWeight = 0.f;
	protected String da = "";
	protected String currStep = "";
	protected String sdsDomain = "";
	protected String hisDomain = "";//domain for dialog history -> bqa or sds domain will be filled
	protected String msgTypeCd = "";//unknown msg code
	protected String isDialogEnded = "0";
	protected UtterReconfirmData reconfirmMsg;
	protected SYS_UTTER_TYPE sysUtterType = SYS_UTTER_TYPE.NORMAL;
	protected Object botNotice = null;
	protected String nlpUtter;

//	@JsonIgnore
	protected String contentJson = "";

	protected String intentJson ="";

	public UtterData() {
		
	}

	public DIALOG_SPEAKER getSpeaker() {
		return speaker;
	}

	public void setSpeaker(DIALOG_SPEAKER speaker) {
		this.speaker = speaker;
	}

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getSentence() {
		return sentence;
	}

	public void setSentence(String sentence) {
		this.sentence = sentence;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getUtter_id() {
		return utter_id;
	}

	public void setUtter_id(int utter_id) {
		this.utter_id = utter_id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public int getDialogSeq() {
		return dialogSeq;
	}

	public void setDialogSeq(int dialogSeq) {
		this.dialogSeq = dialogSeq;
	}
	
	public ChatbotRichContent getRichContent() {
		return richContent;
	}

	public void setRichContent(ChatbotRichContent richContent) {
		this.richContent = richContent;
	}

	public String getContentJson() {
		return contentJson;
	}

	public void setContentJson(String contentJson) {
		this.contentJson = contentJson;
	}

	public SdsUtterRespData getDebug() {
		return debug;
	}

	public void setDebug(SdsUtterRespData debug) {
		this.debug = debug;
	}

	public float getSluWeight() {
		return sluWeight;
	}

	public void setSluWeight(float sluWeight) {
		this.sluWeight = sluWeight;
	}

	public String getDa() {
		return da;
	}

	public void setDa(String da) {
		this.da = da;
	}

	public BasicQaResponse getBqaDebug() {
		return bqaDebug;
	}

	public void setBqaDebug(BasicQaResponse bqaDebug) {
		this.bqaDebug = bqaDebug;
	}

	public BasicQaResponseWithButtons getBqaDebugEx() {
		return bqaDebugEx;
	}

	public void setBqaDebugEx(BasicQaResponseWithButtons bqaDebugEx) {
		this.bqaDebugEx = bqaDebugEx;
	}

	public String getCurrStep() {
		return currStep;
	}

	public void setCurrStep(String currStep) {
		this.currStep = currStep;
	}

	public String getSdsDomain() {
		return sdsDomain;
	}

	public void setSdsDomain(String sdsDomain) {
		this.sdsDomain = sdsDomain;
	}

	public String getHisDomain() {
		return hisDomain;
	}

	public void setHisDomain(String hisDomain) {
		this.hisDomain = hisDomain;
	}

	public String getMsgTypeCd() {
		return msgTypeCd;
	}

	public void setMsgTypeCd(String msgTypeCd) {
		this.msgTypeCd = msgTypeCd;
	}
	
	public String getIsDialogEnded() {
		return isDialogEnded;
	}

	public void setIsDialogEnded(String isDialogEnded) {
		this.isDialogEnded = isDialogEnded;
	}

	public UtterReconfirmData getReconfirmMsg() {
		return reconfirmMsg;
	}

	public void setReconfirmMsg(UtterReconfirmData reconfirmMsg) {
		this.reconfirmMsg = reconfirmMsg;
	}

	public SYS_UTTER_TYPE getSysUtterType() {
		return sysUtterType;
	}

	public void setSysUtterType(SYS_UTTER_TYPE sysUtterType) {
		this.sysUtterType = sysUtterType;
	}

	public String getIntentJson() {
		return intentJson;
	}

	public void setIntentJson(String intentJson) {
		this.intentJson = intentJson;
	}

	public Object getBotNotice() {
        return botNotice;
    }

    public void setBotNotice(Object botNotice) {
        this.botNotice = botNotice;
    }

	public String getNlpUtter() {
		return nlpUtter;
	}

	public void setNlpUtter(String nlpUtter) {
		this.nlpUtter = nlpUtter;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
