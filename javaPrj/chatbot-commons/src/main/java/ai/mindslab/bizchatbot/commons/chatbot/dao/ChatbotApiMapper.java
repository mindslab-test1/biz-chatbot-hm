package ai.mindslab.bizchatbot.commons.chatbot.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiInfoDisp;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiJsonTemplate;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiParam;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotApiResultMappingInfo;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotRespInterfaceRelData;

@Mapper
public interface ChatbotApiMapper {

	public List<ChatbotApiParam> getParams(int ifId);
	public ChatbotApiInfo getApiInfo(int ifId);
	public List<ChatbotApiInfoDisp> getApiList(HashMap<String, Object> params);
	public long getApiListCount(HashMap<String, Object> params);
	public List<ChatbotApiResultMappingInfo> getApiResultMappingInfo(int ifId);
	public ChatbotRespInterfaceRelData getResponseInterfaceRel(HashMap<String, Object> params);
	public ChatbotApiJsonTemplate getJsonTemplate(int ifId);
	public ChatbotRespInterfaceRelData getResponseInterfaceRelWithSdsInfo(HashMap<String, Object> params);
	public ChatbotRespInterfaceRelData getResponseInterfaceRelWithRespCode(HashMap<String, Object> params);
	public ChatbotRespInterfaceRelData getResponseInterfaceRelById(HashMap<String, Object> params);
	
	public long addSingleButton(HashMap<String, Object> params);
	public long deleteSingleButton(HashMap<String, Object> params);
	public long updateSingleButton(HashMap<String, Object> params);
	
	public long insertRichContent(HashMap<String,Object> params);
	public long deleteRichContent(int rcId);
	public long insertApiInfo(HashMap<String,Object> params);
	
	public ChatbotRespInterfaceRelData getResponseInterfaceRelWithPos(HashMap<String, Object> params);
	public long insertResponseInterfaceRelWithRespCode(ChatbotRespInterfaceRelData data);
	public long updateResponseInterfaceRelWithRespCode(ChatbotRespInterfaceRelData data);//very limited update. see the query
	public long updateResponseInterfaceRelWithIfidPos(ChatbotRespInterfaceRelData data);//update only ifId
	public long updateResponseInterfaceRelWithIfidIntent(ChatbotRespInterfaceRelData data);//update only ifId
	public long deleteResponseInterfaceRelWithRespCode(ChatbotRespInterfaceRelData data);
	public ChatbotRespInterfaceRelData getResponseInterfaceRelWithIntent(HashMap<String, Object> params);
	
	public long deleteButtonById(Integer rcId);
	public long deleteImageById(Integer rcId);
	public long deleteCarouselById(Integer rcId);
	public long deleteCarouselByPos(HashMap<String, Object> params);
	
	public long insertRichImage(HashMap<String, Object> params);
	public long insertRichCarousel(HashMap<String, Object> params);
	public long updateSingleCarousel(HashMap<String, Object> params);
	public long getCarouselCount(Integer rcId);
}
	