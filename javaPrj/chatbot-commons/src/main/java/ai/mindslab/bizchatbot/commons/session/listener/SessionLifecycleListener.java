package ai.mindslab.bizchatbot.commons.session.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hazelcast.core.DistributedObject;
import com.hazelcast.core.DistributedObjectEvent;
import com.hazelcast.core.DistributedObjectListener;

public class SessionLifecycleListener implements DistributedObjectListener{

	private Logger logger = LoggerFactory.getLogger(SessionLifecycleListener.class);
			
	public SessionLifecycleListener() {

	}

	@Override
	public void distributedObjectCreated(DistributedObjectEvent event) {
		
	}

	@Override
	public void distributedObjectDestroyed(DistributedObjectEvent event) {
		
		//remove other listener
		DistributedObject obj = event.getDistributedObject();
		logger.debug("distributedObjectDestroyed/{}",obj.getName());
	}
	
}
