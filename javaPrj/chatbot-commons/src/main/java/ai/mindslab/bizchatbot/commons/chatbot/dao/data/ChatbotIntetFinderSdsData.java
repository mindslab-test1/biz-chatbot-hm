package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ChatbotIntetFinderSdsData implements Serializable{

	private	int itfId;
	private int botId;
	private String projectName;
	private int itfOrder;
	private String createdDtm;
	private String creatorId;
	private String updatedDtm;
	private String updatedId;
	
	public ChatbotIntetFinderSdsData() {
	}

	public int getItfId() {
		return itfId;
	}

	public void setItfId(int itfId) {
		this.itfId = itfId;
	}

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public int getItfOrder() {
		return itfOrder;
	}

	public void setItfOrder(int itfOrder) {
		this.itfOrder = itfOrder;
	}

	public String getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getUpdatedId() {
		return updatedId;
	}

	public void setUpdatedId(String updatedId) {
		this.updatedId = updatedId;
	}

	@Override
	public String toString() {
		return "ChatbotIntetFinderSdsData [itfId=" + itfId + ", botId=" + botId + ", projectName=" + projectName
				+ ", itfOrder=" + itfOrder + ", createdDtm=" + createdDtm + ", creatorId=" + creatorId + ", updatedDtm="
				+ updatedDtm + ", updatedId=" + updatedId + "]";
	}
}
