package ai.mindslab.bizchatbot.commons.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotMsg;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.ISDSCodes;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.DIALOG_SPEAKER;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.SDS_STATUS;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.SYS_UTTER_TYPE;
import ai.mindslab.bizchatbot.commons.codes.ISessionCodes;
import ai.mindslab.bizchatbot.commons.codes.ISessionCodes.SESSION_STATUS;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import ai.mindslab.bizchatbot.commons.session.data.ApiParam;
import ai.mindslab.bizchatbot.commons.session.data.BqaProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogCommonConfig;
import ai.mindslab.bizchatbot.commons.session.data.DialogError;
import ai.mindslab.bizchatbot.commons.session.data.DialogProcessing;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.SdsData;
import ai.mindslab.bizchatbot.commons.session.data.SdsUtterRespData;
import ai.mindslab.bizchatbot.commons.session.data.SessionInfo;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;

public class DialogUtils {
	
	public static ReentrantLock lock = new ReentrantLock();
	public static Condition portCodn = lock.newCondition();

	public DialogUtils() {
	}
	
	public static int getNextSeq(DialogSession dialog) throws Exception{
		if(dialog == null || dialog.getDialog() == null) {
			throw new Exception("Inavlid Dialog");
		}
//		int lastSeq = dialog.getDialog().getConversation().size()==0?1:dialog.getDialog().getConversation().get(dialog.getDialog().getConversation().size()-1).getDialogSeq();
		int lastSeq = dialog.getDialog().getConversation().size();
		
		return lastSeq + 1;
	}

	public static String getDialogTopic(UtterData utter, DIALOG_SPEAKER speaker) throws Exception{
		
		if(utter == null) {
			throw new Exception("Invalid Utter");
		}
		int seq = speaker == DIALOG_SPEAKER.USER.USER ? (utter.getDialogSeq()+1) : utter.getDialogSeq(); 
		return IChatbotConst.TOPIC_DIALOG_PREFIX+utter.getSessionId()+"|"+seq;
	}
	
	public static SessionInfo getSessionStatus(DialogSession dialog){
		SessionInfo ses = new SessionInfo();
		if(dialog == null) {
			ses.setStatus(SESSION_STATUS.NOT_FOUND);
		}else if(DateUtils.diffSec(dialog.getLastModified()) > ISessionCodes.SESSION_EXPIRY) {
			ses.setStatus(SESSION_STATUS.EXPIRED);
		}else {
			ses.setStatus(SESSION_STATUS.VALID);
		}
		
		
		return ses;
	}
	
	//sds project port
	public static int getNextProjectPort(HazelcastInstance hazel, Environment prop) {
		
		int prjPort = 0;
			IMap<String, DialogCommonConfig> map = hazel.getMap(IChatbotConst.MAP_NAME_CONFIG);
			DialogCommonConfig port = map.get(IChatbotConst.KEY_PRJ_PORT);
			
			int maxPort = 59999;
			int initPort = 50100;
			int tmpPort = 50100;
			
			if(port == null) {
				try {
					initPort = Integer.parseInt(prop.getProperty("sds.project.port.start"));
				}catch(Exception e) {
				}
				
				prjPort = initPort;
				port = new DialogCommonConfig(IChatbotConst.KEY_PRJ_PORT, prjPort+"");
			}else {
				
				try {
					prjPort = Integer.parseInt(port.getValue());
					initPort = Integer.parseInt(prop.getProperty("sds.project.port.start"));
					maxPort = Integer.parseInt(prop.getProperty("sds.project.port.max"));
					tmpPort = prjPort+1;
					if(tmpPort >= maxPort) {
						tmpPort = initPort;
					}
				}catch(Exception e) {
					tmpPort = initPort;
				}
				prjPort = tmpPort;
				port.setValue(prjPort+"");
			}
			
			//map.put(IChatbotConst.KEY_PRJ_PORT, port, 0, TimeUnit.SECONDS);//set unlimitted
			
//			}catch(InterruptedException e) {
				
		return prjPort;
	}
	
	public static void setNextProjectPort(HazelcastInstance hazel, int usedport) {
		IMap<String, DialogCommonConfig> map = hazel.getMap(IChatbotConst.MAP_NAME_CONFIG);
		
		DialogCommonConfig port = map.get(IChatbotConst.KEY_PRJ_PORT);
		if(port == null) {
			port = new DialogCommonConfig(IChatbotConst.KEY_PRJ_PORT, usedport+"");
		}else {
			port.setValue(""+usedport);
		}
		
		map.put(IChatbotConst.KEY_PRJ_PORT, port, 0, TimeUnit.SECONDS);//set unlimitted
	}
	
	//sds intent finder project port
	public static synchronized int getNextProjectPortItf(HazelcastInstance hazel, Environment prop) {
		int prjPort = 0;
		Map<String, DialogCommonConfig> map = hazel.getMap(IChatbotConst.MAP_NAME_CONFIG);
		DialogCommonConfig port = map.get(IChatbotConst.KEY_PRJ_PORT_ITF);
		
		int maxPort = 60999;
		int initPort = 60000;
		int tmpPort = 60000;
		
		if(port == null) {
			try {
				initPort = Integer.parseInt(prop.getProperty("sds.project.port.itf.start"));
			}catch(Exception e) {
			}
			
			prjPort = initPort;
			port = new DialogCommonConfig(IChatbotConst.KEY_PRJ_PORT_ITF, prjPort+"");
		}else {
			
			try {
				prjPort = Integer.parseInt(port.getValue());
				initPort = Integer.parseInt(prop.getProperty("sds.project.port.itf.start"));
				maxPort = Integer.parseInt(prop.getProperty("sds.project.port.itf.max"));
				tmpPort = prjPort+1;
				if(tmpPort >= maxPort) {
					tmpPort = initPort;
				}
			}catch(Exception e) {
				tmpPort = initPort;
			}
			prjPort = tmpPort;
			port.setValue(prjPort+"");
		}
		
		map.put(IChatbotConst.KEY_PRJ_PORT_ITF, port);
		
		return prjPort;
	}
	
	public static String extractSysUtter(SdsUtterRespData utterResp) {
		if(utterResp == null || utterResp.getSystem() == null) {
			return "";
		}
		try {
			int pos = utterResp.getSystem().indexOf("<BR>");
			if(pos < 0) {
				pos = utterResp.getSystem().indexOf("\n");
			}
			String tmp = utterResp.getSystem();
			if(pos > -1) {
				return tmp.substring(IChatbotConst.PREFIX_SDS_SYSTEM.length(), pos);
			}else {
				return tmp.substring(IChatbotConst.PREFIX_SDS_SYSTEM.length());
			}
//			return utterResp.getSystem().substring("system: ".length(), utterResp.getSystem().indexOf("<BR>"));
		}catch(Exception e) {
			return utterResp.getSystem();
		}
	}
	
	public static UtterData createBotUtterData(DialogSession ses) throws Exception{
		
		int nextSeq = DialogUtils.getNextSeq(ses);
		UtterData utter = new UtterData();
		utter.setBotId(ses.getDialog().getBotId());
		utter.setDialogSeq(nextSeq);
		utter.setSessionId(ses.getSessionId());
		utter.setSpeaker(DIALOG_SPEAKER.CHATBOT);
		utter.setTimestamp(DateUtils.getNow());
		utter.setUserId(ses.getUserData().getUserId());
		
		return utter;
	}
	
	public static UtterData getLastUserUtter(DialogSession ses) throws Exception{
		
		if(ses != null && ses.getDialog() != null && ses.getDialog().getConversation() != null) {
			List<UtterData> list = ses.getDialog().getConversation();
			UtterData utter = list.get(list.size() - 1);
			if(utter.getSpeaker() == DIALOG_SPEAKER.USER) {
				return utter;
			}else if(list.size() - 2 >= 0){
				utter = list.get(list.size() - 2);
				return utter;
			}
		}
		
		return null;
	}
	
	public static UtterData getLastSystemUtter(DialogSession ses) throws Exception{
		
		if(ses != null && ses.getDialog() != null && ses.getDialog().getConversation() != null) {
			List<UtterData> list = ses.getDialog().getConversation();
			UtterData utter = list.get(list.size() - 1);
			if(utter.getSpeaker() == DIALOG_SPEAKER.CHATBOT) {
				return utter;
			}
		}
		
		//QQQ if can'f find bot's response
		UtterData utter = createBotUtterData(ses);
		utter.setSentence("대화오류.. 다시 시도해 주세요.(변경 예정");
		return null;
	}
	
	public static boolean isOpenedSdsSession(DialogSession ses) throws Exception{
		if(ses == null) {
			throw new Exception("Invalid Session");
		}
		if(ses.getSdsData() == null) {
			return false;
		}
		return ses.getSdsData().getStatus() == SDS_STATUS.OPEN;
	}
	
	public static void setSessionError(DialogSession ses, String location, Exception e) {
		if(ses == null) {
			return;
		}
		
		DialogError err = new DialogError();
		if(e instanceof BizChatbotException) {
			err.setErrCode(((BizChatbotException) e).getErrCode());
			err.setErrMsg(((BizChatbotException) e).getErrMsg());
		}else {
			err.setErrCode(1);//QQQ code?
			err.setErrMsg(e.getMessage());
		}
		err.setErrFilter(location);
		ses.setDlgError(err);
	}
	
	public static void setSessionError(DialogSession ses, String location, String errMsg) {
		if(ses == null) {
			return;
		}
		
		DialogError err = new DialogError();
		err.setErrCode(1);//QQQ code?
		err.setErrFilter(location);
		err.setErrMsg(errMsg);
		ses.setDlgError(err);
	}
	
	public static void resetSessionError(DialogSession ses) {
		if(ses == null) {
			return;
		}
		ses.setDlgError(null);
		ses.setCurrFilter("");
	}
	
	
	public static boolean needIFResultMapping(DialogSession ses) throws Exception{
		if(ses == null) {
			return false;
		}
		UtterData utter = getLastSystemUtter(ses);
		if(utter != null) {
			//if utter start with '__RESCODE_', it's system code response
			//if(utter.getSentence().startsWith(IChatbotConst.PREFIX_SYSOUT)) {
			//QQQ
			if(utter.getSentence().contains(IChatbotConst.PREFIX_SYSOUT)) {
				return true;
			}
		}
		return false;
	}
	
	public static String removeSystemCode(String respString) {
		//QQQ pattern -> static
		String orgMsg = "";
		Pattern p;
		Matcher m;
		p = Pattern.compile(".*("+IChatbotConst.PREFIX_SYSOUT+"[^\\s.+]+)\\s+(.*)");
		m = p.matcher(respString);
		if(m.find()) {
			orgMsg = m.group(2);
		}
		return orgMsg;
	}
	
	public static String getRandomMsg(List<ChatbotMsg> list) {
		
		if(list != null && list.size() > 0) {
			Random r = new Random();
			int idx = r.nextInt(list.size());
			ChatbotMsg m = list.get(idx);
			return m.getMsg();
		}else {
			return "";
		}
		
	}
	
	public static boolean isApiNeedUserInputFilling(String paramValue) throws Exception{
		if(StringUtils.isEmpty(paramValue)) {
			return false;
		}
		
		if(paramValue.startsWith(IChatbotConst.PREFIX_INPUTPARAM)){
			return true;
		}else {
			return false;
		}
	}
	
	public static String extractInputFillingType(String paramValue) throws Exception{
		if(StringUtils.isEmpty(paramValue)) {
			return "";
		}
		String slot = "";
		Pattern p;
		Matcher m;
		//p = Pattern.compile(IChatbotConst.PREFIX_INPUTPARAM+"(\\w+\\.\\w+)\\}$");
		p = Pattern.compile("\\#\\{(\\w+\\.?\\w+)\\}");
		String tmp = paramValue.trim();
		m = p.matcher(tmp);
		if(m.find()) {
			slot = m.group(1);
		}
		return slot;
	}
	
	public static String extractDaSlot(String da) {
		if(StringUtils.isEmpty(da)) {
			return "";
		}
		String slot = "";
		Pattern p;
		Matcher m;
		//p = Pattern.compile(".+\\((.+)\\)$");
		//p = Pattern.compile(".+request\\((.+)\\)$");
		p = Pattern.compile(".+\\((.+)=.*");
		String tmp = da.trim();
		m = p.matcher(tmp);
		if(m.find()) {
			slot = m.group(1);
		}
		return slot;
	}
	
	public static ArrayList<DialogProcessing.Slot> extratSluSlots(String sluOrg) {
		if(StringUtils.isEmpty(sluOrg)) {
			return null;
		}
		
		String slu = extractSlu1(sluOrg);
		ArrayList<DialogProcessing.Slot> list = new ArrayList<>();
		String slot = "";
		String slotVal = "";
		Pattern p;
		Matcher m;
//		p = Pattern.compile("(<([\\w\\.\\d^=^<^>]*)=([^<^>^=\\w\\d]*)>)+");
		p = Pattern.compile("(<([^<>=]*)=([^<>=]*)>)+");
		String tmp = slu.trim();
		m = p.matcher(tmp);
		while(m.find()) {
			slot = m.group(2);
			slotVal = m.group(3);
			DialogProcessing.Slot s = new DialogProcessing.Slot(slot, slotVal);
			list.add(s);
		}
		return list;
	}
	
	public static DialogProcessing.Slot lookupSlot(List<DialogProcessing.Slot> slots, String slotType) {
		
		DialogProcessing.Slot out = null;
		if(slots == null || StringUtils.isEmpty(slotType)) {
			return out;
		}
		
		for(DialogProcessing.Slot s: slots) {
			if(s.getSlotName()!= null
					      && s.getSlotName().equals(slotType)) {
				out = s;
				break;
			}
		}
		return out;
	}
	
	public static String extractSlu1(String slu) {
		if(StringUtils.isEmpty(slu)) {
			return "";
		}
		String slu1 = "";
		Pattern p;
		Matcher m;
		p = Pattern.compile("SLU1:\\s(.*)\n.*");
		String tmp = "";
		if(slu.indexOf("\n") > -1) {
		    int in = slu.indexOf("SLU1");
		    int out = slu.length();

		    if(in > -1 ){
                tmp = slu.substring(slu.indexOf("SLU1"), out).trim() +"\n";
            }

		}else {
			tmp = slu;
		}
		m = p.matcher(tmp);
		if(m.find()) {
			slu1 = m.group(1);
		}
		return slu1;
	}
	
	public static void resetProcessingData(DialogSession dialog) {
		if(dialog == null) {
			return;
		}
		
		//QQQ for closing sds
//		SdsData sData = dialog.getSdsData();
//		if(sData != null) {
//			SdsData backup = new SdsData();
//			backup.setSdsIp(sData.getSdsIp());
//			backup.setProjectPort(sData.getProjectPort());
//			backup.setRequest(ISDSCodes.REQUEST_STOP_DIALOG);
//			
//			dialog.setResetSds(backup);
//		}
		
		dialog.getProcessing().setCurrBotUtter("");
		dialog.getProcessing().setExpectedSlot("");
		dialog.getProcessing().setSdsDomain("");
		
		ArrayList<DialogProcessing.Slot> slots = dialog.getProcessing().getExpectedSlots();
		dialog.getProcessing().setExpectedSlots(null);
		slots = null;
		dialog.getProcessing().setIfId(0);
		dialog.getProcessing().setSvcType(SYS_UTTER_TYPE.NORMAL);
		
		dialog.setSdsData(null);
	}
	
	public static void resetBqaProcessingData(DialogSession dialog) {
		if(dialog == null) {
			return;
		}
		
		BqaProcessing bqaProcessing = dialog.getProcessing().getBqaProcessing();
		if(bqaProcessing != null) {
			List<ApiParam> params = bqaProcessing.getParams();
			if(params != null) {
				for(ApiParam param:params) {
					param = null;
				}
				params = null;
				bqaProcessing.setParams(null);
			}
			bqaProcessing = null;
			dialog.getProcessing().setBqaProcessing(null);;
		}
	}
	
	public static boolean isUnknownSystemUtter(UtterData utter) {
		if(utter == null) {
			return false;
		}
		
		SdsUtterRespData sdsresp = utter.getDebug();
		if(sdsresp == null) {
			return false;
		}
		
		String systemUtter = sdsresp.getSlu();
		if(StringUtils.isEmpty(systemUtter)) {
			return true;
		}

		if(systemUtter.indexOf("#unknown()") > -1) {
			return true;
		}

		return false;
	}
	
	public static void setUnknownMsg(UtterData utter, ChatbotConfig config) {
		if(utter == null || config == null) {
			return;
		}
		String unknownMsg = getRandomMsg(config.getUnknownMsg());
		utter.setSentence(unknownMsg);
		utter.setMsgTypeCd(IChatbotConst.MSG_TYPE_UNKNOWN);
		utter.setRichContent(config.getUnknownRichContent());
	}
	
	public static String getReconfirmMsg(String msg) {
		return IChatbotConst.SCENARIO_CHANGE_TAG_ST+msg+IChatbotConst.SCENARIO_CHANGE_TAG_ED;
	}
	
	public static void addSlot(DialogProcessing processing, String slotName, String slotValue) {
		if(processing == null) {
			return;
		}
		
		for(DialogProcessing.Slot s:processing.getExpectedSlots()) {
			if(s.getSlotName().equals(slotName)) {
				s.setSlotValue(slotValue);
				return;
			}
		}
		DialogProcessing.Slot slot = new DialogProcessing.Slot(slotName,slotValue);
		processing.getExpectedSlots().add(slot);
	}
	
	public static void addSlot(DialogProcessing processing, DialogProcessing.Slot slot) {
		if(processing == null || slot == null) {
			return;
		}
		
		for(DialogProcessing.Slot s:processing.getExpectedSlots()) {

			if(s.getSlotName()!= null
					     && s.getSlotName().equals(slot.getSlotName())) {

				s.setSlotValue(slot.getSlotValue());
				return;
			}
		}
		processing.getExpectedSlots().add(slot);
	}
	
	public static void addSlots(DialogProcessing processing, ArrayList<DialogProcessing.Slot> slots) {
		if(processing == null || slots == null) {
			return;
		}
		
		for(DialogProcessing.Slot s:slots) {
			addSlot(processing, s);
		}
	}
}

