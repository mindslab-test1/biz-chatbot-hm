package ai.mindslab.bizchatbot.commons.session.data;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class BasicQaOutWithButtons implements Serializable{
	
	protected String question;
	protected int totalCount;
	protected int morphRange;
	protected String debugYn;
	protected int status;
	protected String result;
	protected int searchFlowType;
	protected String searchFlowTypeName;
	protected List<BasicQaDebugItem> list;
	
	public BasicQaOutWithButtons() {
	}
	
	public String getQuestion() {
		return question;
	}
	
	public void setQuestion(String question) {
		this.question = question;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getMorphRange() {
		return morphRange;
	}

	public void setMorphRange(int morphRange) {
		this.morphRange = morphRange;
	}

	public String getDebugYn() {
		return debugYn;
	}

	public void setDebugYn(String debugYn) {
		this.debugYn = debugYn;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public int getSearchFlowType() {
		return searchFlowType;
	}

	public void setSearchFlowType(int searchFlowType) {
		this.searchFlowType = searchFlowType;
	}

	public String getSearchFlowTypeName() {
		return searchFlowTypeName;
	}

	public void setSearchFlowTypeName(String searchFlowTypeName) {
		this.searchFlowTypeName = searchFlowTypeName;
	}

	public List<BasicQaDebugItem> getList() {
		return list;
	}

	public void setList(List<BasicQaDebugItem> list) {
		this.list = list;
	}

//	public static class BasicQaOutItem implements Serializable{
//		
//		protected String question;
//		protected String questionMorph;
//		protected int morphCount;
//		protected int domainId;
//		protected int res;
//		protected float score;
//		protected String mainYn;
//		protected List<BasicQaAnswerSimple> answerList;
//		
//		public BasicQaOutItem() {
//			
//		}
//
//		public String getQuestion() {
//			return question;
//		}
//
//		public void setQuestion(String question) {
//			this.question = question;
//		}
//
//		public String getQuestionMorph() {
//			return questionMorph;
//		}
//
//		public void setQuestionMorph(String questionMorph) {
//			this.questionMorph = questionMorph;
//		}
//
//		public int getMorphCount() {
//			return morphCount;
//		}
//
//		public void setMorphCount(int morphCount) {
//			this.morphCount = morphCount;
//		}
//
//		public int getDomainId() {
//			return domainId;
//		}
//
//		public void setDomainId(int domainId) {
//			this.domainId = domainId;
//		}
//
//		public int getRes() {
//			return res;
//		}
//
//		public void setRes(int res) {
//			this.res = res;
//		}
//
//		public float getScore() {
//			return score;
//		}
//
//		public void setScore(float score) {
//			this.score = score;
//		}
//
//		public String getMainYn() {
//			return mainYn;
//		}
//
//		public void setMainYn(String mainYn) {
//			this.mainYn = mainYn;
//		}
//
//		public List<BasicQaAnswerSimple> getAnswerList() {
//			return answerList;
//		}
//
//		public void setAnswerList(List<BasicQaAnswerSimple> answerList) {
//			this.answerList = answerList;
//		}
//
//		@Override
//		public String toString() {
//			return "BasicQaOutItem [question=" + question + ", questionMorph=" + questionMorph + ", morphCount="
//					+ morphCount + ", domainId=" + domainId + ", res=" + res + ", score=" + score + ", mainYn=" + mainYn
//					+ ", answerList=" + answerList + "]";
//		}
//	}
}
