package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@SuppressWarnings("serial")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties({"createdDtm", "updatedDtm", "creatorId", "updatorId", "rcId"})
public class ChatbotRespInterfaceRelData implements Serializable{

	protected int relId;
	protected int botId;
	protected String domain;
	protected String respCode="";
	protected String respMsg="";
	protected String respTypeCd="";
	protected int ifId;
	protected List<ChatbotApiResultMappingInfo> resultMappingList;
	protected int intentSeq;
	protected int agentSeq;
	protected String uiPos="";
	protected int uiPosVal;
	
	@JsonIgnore
	protected String createdDtm;
	@JsonIgnore
	protected String updatedDtm;
	@JsonIgnore
	protected String creatorId;
	@JsonIgnore
	protected String updatorId;
	
	public ChatbotRespInterfaceRelData() {
	}

	public int getRelId() {
		return relId;
	}

	public void setRelId(int relId) {
		this.relId = relId;
	}

	public int getBotId() {
		return botId;
	}

	public void setBotId(int botId) {
		this.botId = botId;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getRespMsg() {
		return respMsg;
	}

	public void setRespMsg(String respMsg) {
		this.respMsg = respMsg;
	}

	public int getIfId() {
		return ifId;
	}

	public void setIfId(int ifId) {
		this.ifId = ifId;
	}

	public String getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public List<ChatbotApiResultMappingInfo> getResultMappingList() {
		if(resultMappingList == null) {
			resultMappingList = new ArrayList<>();
		}
		return resultMappingList;
	}

	public void setResultMappingList(List<ChatbotApiResultMappingInfo> resultMappingList) {
		this.resultMappingList = resultMappingList;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	public String getRespTypeCd() {
		return respTypeCd;
	}

	public void setRespTypeCd(String respTypeCd) {
		this.respTypeCd = respTypeCd;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getIntentSeq() {
		return intentSeq;
	}

	public void setIntentSeq(int intentSeq) {
		this.intentSeq = intentSeq;
	}

	public int getAgentSeq() {
		return agentSeq;
	}

	public void setAgentSeq(int agentSeq) {
		this.agentSeq = agentSeq;
	}

	public String getUiPos() {
		return uiPos;
	}

	public void setUiPos(String uiPos) {
		this.uiPos = uiPos;
	}

	public int getUiPosVal() {
		return uiPosVal;
	}

	public void setUiPosVal(int uiPosVal) {
		this.uiPosVal = uiPosVal;
	}

	@Override
	public String toString() {
		return "ChatbotRespInterfaceRelData [relId=" + relId + ", botId=" + botId + ", domain=" + domain + ", respCode="
				+ respCode + ", respMsg=" + respMsg + ", respTypeCd=" + respTypeCd + ", ifId=" + ifId
				+ ", resultMappingList=" + resultMappingList + ", intentSeq=" + intentSeq + ", agentSeq=" + agentSeq
				+ ", uiPos=" + uiPos + ", uiPosVal=" + uiPosVal + ", createdDtm=" + createdDtm + ", updatedDtm="
				+ updatedDtm + ", creatorId=" + creatorId + ", updatorId=" + updatorId + "]";
	}
}
