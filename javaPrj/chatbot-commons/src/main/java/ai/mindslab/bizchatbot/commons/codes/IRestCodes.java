package ai.mindslab.bizchatbot.commons.codes;

/**
 * Created by jaeheoncho on 2018. 4. 20..
 */
public interface IRestCodes {

	public static final int ERR_CODE_SUCCESS = 0;
	public static final String ERR_MSG_SUCCESS = "SUCCESS";
	
	public static final int ERR_CODE_FAILURE = 1;
	public static final String ERR_MSG_FAILURE = "FAILURE";
	
	
    public static final int ERR_CODE_PARAMS_INVALID = 100001;
    public static final String ERR_MSG_PARAMS_INVALID = "INVALID PARAMETERS";
    
    public static final int ERR_CODE_AUTH_ERROR = 400000;
    public static final String ERR_MSG_AUTH_ERROR = "AUTHENTICATION ERROR";
    
    public static final int ERR_CODE_HTTP_INVALID_METHOD = 500001;
    public static final String ERR_MSG_HTTP_INVALID_METHOD = "INVALID HTTP METHOD";
    
    public static final int ERR_CODE_SQL_EXCEPTION = 500000;
    public static final String ERR_MSG_SQL_EXCEPTION = "DATABASE EXCEPTION";
    
    public static final int ERR_CODE_DIALOG_TIMEOUT = 60000;
    public static final String ERR_MSG_DIALOG_TIMEOUT = "CHATBOT RESPONSE TIMEOUT";
    public static final int ERR_CODE_CHATBOT_INIT_ERROR = 61001;
    public static final String ERR_MSG_CHATBOT_INIT_ERROR = "CHATBOT DIALOG INIT. ERROR";
    public static final int ERR_CODE_SDS_OPEN_ERROR = 61002;
    public static final String ERR_MSG_SDS_OPEN_ERROR = "SDS DIALOG OPEN ERROR";
    public static final int ERR_CODE_SDS_CLOSE_ERROR = 61003;
    public static final String ERR_MSG_SDS_CLOSE_ERROR = "SDS DIALOG CLOSE ERROR";
    
    
	// BQA SERVICE
	public static final int ERR_CODE_SEARCH_FAILURE = 900000;
	public static final String ERR_MSG_SEARCH_FAILURE = "Can't find the word";;
	public static final int ERR_CODE_SEARCH_DB_FAILURE = 900001;
	public static final String ERR_MSG_SEARCH_DB_FAILURE = "Can't find the Data in Database";;
	public static final int ERR_CODE_SEARCH_SOLR_FAILURE = 900002;
	public static final String ERR_MSG_SEARCH_SOLR_FAILURE = "Can't find the Data in Solr";;
	public static final int ERR_CODE_SEARCH_DB_SOLR_FAILURE = 900003;
	public static final String ERR_MSG_SEARCH_DB_SOLR_FAILURE = "Can't find the Data in Solr & DB";;
	public static final int ERR_CODE_SEARCH_SOLR_IO_ERROR = 900004;
	public static final String ERR_MSG_SEARCH_SOLR_IO_ERROR = "Error Solr Server IO";;
	public static final int ERR_CODE_SEARCH_NO_ANSWER = 900005;
	public static final String ERR_MSG_SEARCH_NO_ANSWER = "Can't find the Answer";;
	public static final int ERR_CODE_SEARCH_SOLR_TOO_MANY_RESULT = 900006;
	public static final String ERR_MSG_SEARCH_SOLR_TOO_MANY_RESULT = "Error Too Many Solr Result";;
	public static final int ERR_CODE_SEARCH_SOLR_SYSTEM_ERROR = 900007;
	public static final String ERR_MSG_SEARCH_SOLR_SYSTEM_ERROR = "Error Solr Server";;
	public static final int ERR_CODE_SEARCH_SOLROR_NOTHING = 900009;
	public static final String ERR_MSG_SEARCH_SOLROR_NOTHING = "Can't find results by Solr Or query";
	public static final int ERR_CODE_SEARCH_SOLROR_RANGE_SHOT = 900008;
	public static final String ERR_MSG_SEARCH_SOLROR_RANGE_SHOT = "SolrOr range is less than question";
	public static final int ERR_CODE_SEARCH_NLP_RESULT_NOTHING_ERROR = 900009;
	public static final String ERR_MSG_SEARCH_NLP_RESULT_NOTHING_ERROR = "Error NLP Result is nothing";
	public static final int ERR_CODE_SEARCH_DOMAIN_PARAMETER_ERROR = 900010;
	public static final String ERR_MSG_SEARCH_DOMAIN_PARAMETER_ERROR = "Error Domain parameter is nothing";
	public static final int ERR_CODE_INVALID_DATA_QNA = 990000;
	public static final String ERR_MSG_INVALID_DATA_QNA = "Invalid Data Q&A";
	public static final int ERR_CODE_SEARCH_SOLROR_SUCCESS = 999000;
	public static final String ERR_MSG_SEARCH_SOLROR_SUCCESS = "Return to list of similarity questions"; 
	public static final int ERR_CODE_SEARCH_SOLROR_FAILURE = 999001;
	public static final String ERR_MSG_SEARCH_SOLROR_FAILURE = "Can't find the Data in Solr OR";

	// MGMT
	public static final int ERR_CODE_MGMT_UPDATE_ERROR_Q = 910000;
	public static final String ERR_MSG_MGMT_UPDATE_ERROR_Q = "Fail Insert or Update  Question table";
	public static final int ERR_CODE_MGMT_UPDATE_FAIL_Q = 910001;
	public static final String ERR_MSG_MGMT_UPDATE_FAIL_Q = "Fail Insert or Update  Question table, Count 0";
	public static final int ERR_CODE_MGMT_UPDATE_ERROR_A = 910002;
	public static final String ERR_MSG_MGMT_UPDATE_ERROR_A = "Fail Insert or Update  Answer table";
	public static final int ERR_CODE_MGMT_UPDATE_FAIL_A = 910003;
	public static final String ERR_MSG_MGMT_UPDATE_FAIL_A = "Fail Insert or Update  Answer table, Count 0";
	public static final int ERR_CODE_MGMT_UPDATE_FAIL_Q_INDEX = 910004;
	public static final String ERR_MSG_MGMT_UPDATE_FAIL_Q_INDEX = "Fail Insert or Update  Q&A Index table, Count 0";
	public static final int ERR_CODE_MGMT_DELETE_ERROR_Q = 911000;
	public static final String ERR_MSG_MGMT_DELETE_ERROR_Q = "Fail DELETE  Q&A table";
	public static final int ERR_CODE_MGMT_DELETE_FAIL_Q = 911001;
	public static final String ERR_MSG_MGMT_DELETE_FAIL_Q = "Fail DELETE  Q&A table, Count 0";
	public static final int ERR_CODE_MGMT_DELETE_ERROR_A = 911000;
	public static final String ERR_MSG_MGMT_DELETE_ERROR_A = "Fail DELETE  Q&A table";
	public static final int ERR_CODE_MGMT_DELETE_FAIL_A = 911001;
	public static final String ERR_MSG_MGMT_DELETE_FAIL_A = "Fail DELETE  Q&A table, Count 0";
	public static final int ERR_CODE_MGMT_DELETE_FAIL_Q_INDEX = 911002;
	public static final String ERR_MSG_MGMT_DELETE_FAIL_Q_INDEX = "Fail DELETE  Q&A Index table, Count 0";
	public static final int ERR_CODE_MGMT_SELECT_ERROR_Q = 911003;
	public static final String ERR_MSG_MGMT_SELECT_ERROR_Q = "Fail SELECT  Q&A table";
	
	// INDEX
	public static final int ERR_CODE_INDEX_SOLR_SYSTEM_ERROR = 920000;
	public static final String ERR_MSG_INDEX_SOLR_SYSTEM_ERROR = "Error Indexing Solr Server";;
	public static final int ERR_CODE_INDEX_DOMAIN_NOTHING = 920001;
	public static final String ERR_MSG_INDEX_DOMAIN_NOTHING = "Error Indexing data is nothing";;
	// NLP
	public static final int ERR_CODE_NLP_ANALYSIS_ERROR = 930000;
	public static final String ERR_MSG_NLP_ANALYSIS_ERROR = "Error analysis grpc nlp";;
	// EXCEL
	public static final int ERR_CODE_EXCEL_CREATE_ERROR = 940001;
	public static final String ERR_MSG_EXCEL_CREATE_ERROR = "Fail To Create Q&A Excel Document";
	public static final int ERR_CODE_EXCEL_UPLOAD_ERROR = 940002;
	public static final String ERR_MSG_EXCEL_UPLOAD_ERROR = "Fail To Upload Q&A Excel Document";
	public static final int ERR_CODE_EXCEL_INSERT_ERROR_IO = 941001;
	public static final String ERR_MSG_EXCEL_INSERT_ERROR_IO = "Fail To Insert Q&A Excel Document Because of IO";
	public static final int ERR_CODE_EXCEL_INSERT_ERROR_FORMAT = 941002;
	public static final String ERR_MSG_EXCEL_INSERT_ERROR_FORMAT = "Fail To Insert Q&A Excel Document Because of FORMAT";
	
	//Paraphrase
	public static final int ERR_CODE_PARAPHRASE_INSERT_ERROR = 950001;
	public static final String ERR_MSG_PARAPHRASE_INSERT_ERROR = "Fail INSERT PARAPHRASE Table";
}
