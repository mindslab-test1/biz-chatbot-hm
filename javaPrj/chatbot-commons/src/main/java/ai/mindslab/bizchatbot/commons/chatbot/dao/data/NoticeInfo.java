package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties({"createdDtm", "updatedDtm", "creatorId", "updatorId", "rcId"})
@SuppressWarnings("serial")
public class NoticeInfo extends ChatbotRichImage{

	protected String noticePos;
	protected String useYn = "Y";
	
	public NoticeInfo() {
	}

	public String getUseYn() {
		return useYn;
	}

	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}

	public String getNoticePos() {
		return noticePos;
	}

	public void setNoticePos(String noticePos) {
		this.noticePos = noticePos;
	}

	@Override
	public String toString() {
		return "NoticeInfo [noticePos=" + noticePos + ", useYn=" + useYn + ", rcId=" + rcId + ", imagePath=" + imagePath
				+ ", linkUrl=" + linkUrl + ", imageText=" + imageText + ", createdDtm=" + createdDtm + ", updatedDtm="
				+ updatedDtm + ", creatorId=" + creatorId + ", updatorId=" + updatorId + "]";
	}


}
