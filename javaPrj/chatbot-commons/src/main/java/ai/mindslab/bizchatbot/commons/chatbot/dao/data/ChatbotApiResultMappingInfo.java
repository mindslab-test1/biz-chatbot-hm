package ai.mindslab.bizchatbot.commons.chatbot.dao.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

@SuppressWarnings("serial")
public class ChatbotApiResultMappingInfo implements Serializable{

	protected int mappingId;
	protected int ifId;
	protected String jsonPath;
	protected String dispName;
	
	@JsonIgnore
	protected String createdDtm;
	@JsonIgnore
	protected String updatedDtm;
	@JsonIgnore
	protected String creatorId;
	@JsonIgnore
	protected String updatorId;
	
	public ChatbotApiResultMappingInfo() {
	}

	public int getMappingId() {
		return mappingId;
	}

	public void setMappingId(int mappingId) {
		this.mappingId = mappingId;
	}

	public int getIfId() {
		return ifId;
	}

	public void setIfId(int ifId) {
		this.ifId = ifId;
	}

	public String getJsonPath() {
		return jsonPath;
	}

	public void setJsonPath(String jsonPath) {
		this.jsonPath = jsonPath;
	}

	public String getDispName() {
		return dispName;
	}

	public void setDispName(String dispName) {
		this.dispName = dispName;
	}

	public String getCreatedDtm() {
		return createdDtm;
	}

	public void setCreatedDtm(String createdDtm) {
		this.createdDtm = createdDtm;
	}

	public String getUpdatedDtm() {
		return updatedDtm;
	}

	public void setUpdatedDtm(String updatedDtm) {
		this.updatedDtm = updatedDtm;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	@Override
	public String toString() {
		return "ChatbotApiResultMappingInfo [mappingId=" + mappingId + ", ifId=" + ifId + ", jsonPath=" + jsonPath
				+ ", dispName=" + dispName + ", createdDtm=" + createdDtm + ", updatedDtm=" + updatedDtm
				+ ", creatorId=" + creatorId + ", updatorId=" + updatorId + "]";
	}

}
