package ai.mindslab.bizchatbot.commons.data;

import java.io.Serializable;

public class SdsPortUsed implements Serializable{
 
	private static final long serialVersionUID = 1L;
	private long timestamp = 0L;
	private int port;
	private String ip;
	public SdsPortUsed() {
		this.timestamp = System.currentTimeMillis();
	}
	public SdsPortUsed(String ip, int port) {
		this.ip = ip;
		this.port = port;
		this.timestamp = System.currentTimeMillis();
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	@Override
	public String toString() {
		return "SdsPortUsed [timestamp=" + timestamp + ", port=" + port + ", ip=" + ip + "]";
	}
	

}
