/**
 * maum.ai M2U 플랫폼, 라우터 서비스의 구조
 *
 * M2U Router는 MAP에서 인터페이스된 결과물을 서버로 전송하고 이를 MAP으로 내려보내는 구조를
 * 서비스를 가지고 있습니다.
 *
 * DialogRouter는 다음과 같은 역할을 수행해야 합니다.
 *
 * (1) 대화를 처리할 수 있는 세션 정보를 획득합니다.
 * (2) 대화를 처리하는 chatbot을 지정합니다., 챗봇이 없을 경우에는 default chatbot
 * (3) 챗봇에 대응하는 Intent Finder를 사용하여 (skill/intent)를 찾아옵니다.
 * (4) Skill을 찾아와서 일치하는 Dialog Agent를 호출합니다.
 * (5) 스킬의 전이에 따라서 필요한 경우 DialogAgent를 호출할 수 있습니다.
 * (6) 응답을 받아서 MAP에게 최종 응답을 전달합니다.
 *
 * namespace: maum.m2u.common
 */
syntax = "proto3";

import "google/protobuf/struct.proto";
import "google/protobuf/empty.proto";
import "maum/m2u/common/dialog.proto";
import "maum/m2u/common/card.proto";
import "maum/m2u/common/event.proto";
import "maum/m2u/common/directive.proto";

package maum.m2u.router.v3;


// 대화를 처리하도록 Router에 요청을 넘깁니다.
// 대화는 논리적으로 하나의 발화에 대해서 여러개의 응답이 가능한 구조를 갖추고 있습니다.
// 2가지 경우에 그러합니다.
// TRIGGER를 통해서 하나의 발화를 태우고 다른 발롸로 넘어간 경우에 하나 이상이 가능합니다.
// OPEN의 경우에 여러 개의 응압이 가능할 수 있습니다.
service TalkRouter {
  // 새로은 세션을 성생하도록 요청합니다.
  // map의 Dialog/Open에 대응합니다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc OpenRoute (OpenRouteRequest) returns (stream TalkRouteResponse);
  // 대화 요청을 보내고, 응답을 받습니다.
  // map의 Dialog/*Talk에 대응합니다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc TalkRoute (TalkRouteRequest) returns (stream TalkRouteResponse);
  // 이벤트를 보내고, 응답을 받습니다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc EventRoute (EventRouteRequest) returns (stream TalkRouteResponse);
  // 명시적으로 대화 종료 요청을 응답을 받습니다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc CloseRoute (CloseRouteRequest) returns (CloseRouteResponse);
  // 대화에 대한 피드백을 보냅니다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc Feedback (FeedbackRequest) returns (google.protobuf.Empty);
}


/**
 * 새로운 챗봇을 생성할 때 요청이 됩니다.
 */
message OpenRouteRequest {
  // 사용자 발화 정보
  maum.m2u.common.Utter utter = 1;

  // 모든 시스템 컨텍스트
  maum.m2u.common.SystemContext system_context = 2;

  // 챗봇 (aka service group)
  // 세션을 생성할 때
  string chatbot = 11;
}


/**
 * 대화를 넘겨서 응답을 받도록 하는 요청
 */
message TalkRouteRequest {
  // 사용자 발화 정보
  maum.m2u.common.Utter utter = 1;

  // 모든 시스템 컨텍스트
  maum.m2u.common.SystemContext system_context = 2;

  // 챗봇 (aka service group)
  // 세션을 생성할 때
  string chatbot = 11 [deprecated=true];

  // SKILL: 명시적으로 SKILL을 지정하여 들어오는 경우
  // FIXME: 이 것은 여전히 말이 안되는 SPEC 입니다.
  string skill = 12 [deprecated=true];
}

/**
 * 대화 요청에 대한 응답
 */
message TalkRouteResponse {
  oneof test_res {
    // 대화 응답
    TalkRouteSpeechResponse response = 1;
    // 위임형 터리
    TalkRouteDialogDelegate delegate = 3;
  }
}

/**
 * TalkRoute를 통해서 대화 호출한 결과, 정상적인 응답
 */
message TalkRouteSpeechResponse {
  // 단말의 출력 장치에 내보낼 텍스트를 지정한다.
  maum.m2u.common.Speech speech = 1;

  // 단말이 출력할 카드를 내려보낸다.
  repeated maum.m2u.common.Card cards = 2;

  // DEVICE가 수행할 지시를 내려보낸다.
  repeated maum.m2u.common.Directive directives = 3;

  // 대화의 결과를 DEVICE까지 내보낼 데이터가 있을 경우에 지정해준다.
  google.protobuf.Struct meta = 4;
}

/**
 * TalkRoute를 경유하여 대화 호출한 결과 중, 단말 요청 사항
 */
message TalkRouteDialogDelegate {
  // 내려보낼 디렉티브
  // 이때 내려갈 Directive는 반드시 몇가지 제약된
  maum.m2u.common.DelegateDirective directive = 1;

  // 대화의 결과를 DEVICE까지 내보낼 데이터가 있을 경우에 지정해준다.
  google.protobuf.Struct meta = 31;
}


/**
 * EventRoute를 통해서 대화를 요청하는 경우
 */
message EventRouteRequest {
  // 이벤트
  maum.m2u.common.Event event = 1;

  // 시스템 컨텍스트
  maum.m2u.common.SystemContext system_context = 2;
}

/**
 * CloseRoute를 통해서 명시적인 세션 종료 호출
 */
message CloseRouteRequest {
  // 시스템 컨텍스트
  maum.m2u.common.SystemContext system_context = 1;
}

//
// CloseRoute를 통해서 명시적인 세션 종료 호출
message CloseRouteResponse {
  // 응답으로 내려보낼 메시지
  string message = 1;
}


/**
 * 피드백 호출
 */
message FeedbackRequest {
  // 시스템 컨텍스트
  maum.m2u.common.SystemContext system_context = 1;

  // 대화 일련 번호
  int32 talk_seq = 2;
  // 만족도 숫자
  int32 satisfaction = 3;
  // 추가적인 매타 데이터
  google.protobuf.Struct meta = 8;
}
