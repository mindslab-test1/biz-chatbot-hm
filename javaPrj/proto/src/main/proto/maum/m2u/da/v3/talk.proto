syntax = "proto3";


import "google/protobuf/struct.proto";
import "google/protobuf/empty.proto";
import "maum/common/lang.proto";
import "maum/m2u/common/dialog.proto";
import "maum/m2u/common/card.proto";
import "maum/m2u/common/event.proto";
import "maum/m2u/common/directive.proto";
import "maum/m2u/router/v3/intentfinder.proto";
import "maum/brain/nlp/nlp.proto";

import "maum/m2u/da/provider.proto";

package maum.m2u.da.v3;

/**
 * 대화를 수행하는 Diaog Agent 서비스
 *
 * 이중에서 대화를 수행하는 API는 다음과 같습니다.
 * Open: 새로운 대화를 새로이 시작합니다.
 */
service DialogAgentProvider {
  // LIFE CYCLE MANAGEMENT

  /**
   * Web console을 통해 DA를 Dialog Agent Instance 의 형태로 실행할 때,
   * 관련 설정값을 이용하여 Instnace를 초기화하도록 실행되도록 하는 함수
   * 입력된 사항에 대해 Talk() 함수에서 사용할 수 있도록
   * InitParameter의 정보를 변수에 저장하는 과정 수행
   */
  rpc Init (InitParameter) returns (DialogAgentProviderParam);

  /**
   * 현재 Dialog Agent Instance 의 상태를 확인할 수 있는 함수
   * Dialog Agent Instance 동작에 따라 상태 변경 필요
   *
   * 생성자에서 DIAG_STATE_IDLE 상태로 초기화 해줘야 합니다. (준비 상태)
   * Init() 함수에서 DIAG_STATE_INITIALIZING로 상태를 변경해주고 (초기화중)
   * Init() 이 성공적으로 되면 DIAG_STATE_RUNNING 상태로 변경해줘야 합니다. (동작중)
   * Talk()는 DIAG_STATE_RUNNING인 경우만 할 수 있습니다.
   * Terminate() 에서는 상태를 DIAG_STATE_TERMINATED로 변경해줘야 합니다.
   */
  rpc IsReady (google.protobuf.Empty) returns (DialogAgentStatus);

  /**
   * 동작중인 DA Instance을 종료시키는 함수
   */
  rpc Terminate (google.protobuf.Empty) returns (google.protobuf.Empty);

  // APPLICATION MANAGEMENT

  /**
   * 실제로 DA Instnace가 돌아가기 위해 필요한 paramerters를 정의
   */
  rpc GetRuntimeParameters (google.protobuf.Empty) returns (RuntimeParameterList);

  /**
   * User가 사용자 확장 속성을 정의하고자 할 때 사용됨.
   * DA의 생성자에서 정의된 DA의 User Attribute List를 반환
   * FIXME: 이 함수는 더 이상 호출할 필요가 없습니다.
   */
  rpc GetUserAttributes (google.protobuf.Empty) returns (google.protobuf.Empty);

  // CHATBOT SERVICE MANAGEMENT

  /**
   * DA가 동작하는 데 필요한 여라가지 옵션 값을 처리하는 DA Provider Parameter를 반환
   */
  rpc GetProviderParameter (google.protobuf.Empty) returns (DialogAgentProviderParam);

  // TALK API

  // 세션이 새로이 시작했을 때에 메시지를 전달 받고, 응답을 줄 수 있습니다.
  // 이 호출은 세션 전체를 시작할 때 들어오는 요청이고 모든 DA가 이 요청을 받지는 않습니다.
  //
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc OpenSession (OpenSessionRequest) returns (TalkResponse);

  // 해당하는 스킬을 최초로 호출 할 때 호출된다.
  // 싱글턴 일 경우에는 호출되지 않는다.
  // 대화를 시작하기 전에 초기화해야 하는 작업이 있을 경우에 호출한다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc OpenSkill (OpenSkillRequest) returns (OpenSkillResponse);

  // 대화를 주고 응답을 생성하도록 한다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc Talk (TalkRequest) returns (TalkResponse);

  // DA에서 내려보낸 디렉티브에 대한 실행 결과를 받아와서 처리할 수 있도록 한다.
  // 다음과 같은 동작이 예가 될 수 있다.
  // 로봇이 특정한 위치로 이동하라고 명명하고 수행이 완료되었을 경우에 내보낸다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc Event (EventRequest) returns (TalkResponse);

  // 현재 SKILL이 종료되었음을 알려준다.
  // SKILL이 정상적으로 스스로 종료할 경우에는 호출하지 않으므로
  // 매우 주의해야 한다.
  // header: x-operation-sync-id : 모든 이벤트 호출에 대한 헤더로 사용되도록 함.
  rpc CloseSkill (CloseSkillRequest) returns (CloseSkillResponse);
}


/**
 * 새로운 대화 세션이 열럴 때, 이에 대한 응답을 할 수 있는 서비스를 지정합니다.
 * 이 경우에는 INTENT FINDER를 타지는 않고 바로 들어오게 됩니다.
 * SKILL은 CHATBOT의 설정에서 세션 OPEN 시점에 호출해야할 SKILL을 지정하도록 하고 있고
 * 이를 통해서 지정된 스킬 들이 호출되도록 되어 잇습니다.
 * 이 스킬은 하나 이상이 될 수 있습니다.
 */
message OpenSessionRequest {
  // 시스템 컨텍스트
  maum.m2u.common.SystemContext system_context = 2;
  // 세션 및 컨텍스트
  maum.m2u.common.Session session = 3;

  // 챗봇의 이름
  string chatbot = 11;
  // 호출되는 스킬의 이름
  string skill = 12;
  // 언어
  maum.common.Lang lang = 13;
}

/**
 * 이 스킬이 시작되었음을 알려줍니다.
 * 새로운 세션으로 스킬이 시작될 때, 해당 세션에 관련된 초기화 작업을 수행할 수 있습니다.
 * Router는 해당 세션에서 스킬을 새로이 시작할 때 OPEN을 호출해서 정보를 알려줘야 합니다.
 */
message OpenSkillRequest {
  // 시스템 컨텍스트를 모두 보냅니다.
  maum.m2u.common.SystemContext system_context = 1;
  // 현재의 세션 정보를 보냅니다.
  maum.m2u.common.Session session = 2;
}

/**
 * 새로운 대화를 시작할 때, 해당 스킬에 대한 초기화 작업을 수행할 수 있습니다.
 *
 * 새로운 세션을 호출할 때 초기화할 일이 있으면 필요한 작업을 수행할 기회를 제공합니다.
 * 응답에서는 session에 대한 업데이트를 제공할 수 있습니다.
 */
message OpenSkillResponse {
  // 최종 응답에 포함될 메타를 지정합니다.
  google.protobuf.Struct meta = 1;
  // 이 세션에서 연 SKILL을 열 때 세션에 업데이트 정보를 지정해준다.
  maum.m2u.common.Session session_update = 2;
}

/**
 * 라우터로부터 받는 대화 요청
 *
 * 대화 요청에는 대화, 시스템 컨텍스트, 세션 정보가 포함되어 있습니다.
 */
message TalkRequest {
  // 사용자 발화 및 메타데이터
  maum.m2u.common.Utter utter = 1;
  // 시스템 컨텍스트
  maum.m2u.common.SystemContext system_context = 2;
  // 세션 및 컨텍스트
  maum.m2u.common.Session session = 3;

  // 챗봇의 이름
  string chatbot = 11;
  // 스킬의 이름
  string skill = 12;
  // 의도의 이름 *optional* 대부분 비어 있습니다.
  // 경우에 따라서 채워져서 오는 수도 있습니다.
  string intent = 13;

  // INTENT FINDER를 수행하고 난 결과 발견된 의도들을 모아 놓은 곳입니다.
  maum.m2u.router.v3.FoundIntent found_intent = 21;
  // NLU RESULT가 정상적으로 동작해야 합니다.
  maum.brain.nlp.Document nlu_result = 22;
}

/**
 * 대화에 대한 응답은 크게 세 가지 이다.
 * 1. SpeechReponse: 정상적인 대화의 응답
 * 2. SkillTransition: SKILL에 대한 요청을 처리할 수 없어서 다른 SKILL로 전이하는 경우
 * 3. DialogDelegate: DEVICE의 추가적인 응답을 요청하고 이를 재처리하는 경우
 */
message TalkResponse {
  oneof test_res {
    // 대화 응답
    SpeechResponse response = 1;
    // 스킬 전이
    SkillTransition transit = 2;
    // 위임형 터리
    DialogDelegate delegate = 3;
  }
}


/**
 * 정상적인 대화 응답
 */
message SpeechResponse {
  // 단말의 출력 장치에 내보낼 텍스트를 지정한다.
  maum.m2u.common.Speech speech = 1;

  // 단말이 출력할 카드를 내려보낸다.
  repeated maum.m2u.common.Card cards = 2;

  // DEVICE가 수행할 지시를 내려보낸다.
  repeated maum.m2u.common.Directive directives = 3;

  // 대화의 결과를 DEVICE까지 내보낼 데이터가 있을 경우에 지정해준다.
  google.protobuf.Struct meta = 4;

  // 대화를 수행한 결과 중 세션 컨텍스트에 정보를 업데이트 해준다.
  maum.m2u.common.Session session_update = 11;

  // closeSkill이 TRUE이면 라우터는 명시적으로 Close()를 호출해야 한다.
  // 라우터는 현재의 세션에서 current skill 정보를 삭제해야 한다.
  bool close_skill = 100;
}

/**
 * 새로운 스킬로 전이한다.
 *
 * 라우터는 SkillTransition이 반복적으로 처리되는 것을 막야야 한다.
 * 하나의 대화에서 SkillTranstion이 한번 이상 발생해서는 안된다.
 */
message SkillTransition {
  enum TransitionType {
    SKILL_TRANS_UNKNOWN = 0;
    SKILL_TRANS_GOTO = 11;
    SKILL_TRANS_GOTO_ONCE = 12;
    SKILL_TRANS_GOTO_DEFAULT = 13;
    SKILL_TRANS_DISCARD = 21;
    SKILL_TRANS_CANNOT_UNDERSTAND = 22;
    CHATBOT_TRANS_GOTO = 31;
  }
  // 응답 타입을 지정한다.
  TransitionType transition_type = 1;
  // GOTO, GOTO_ONCE의 경우 직접 이동해야할 SKILL을 지정한다.
  string skill = 11;
  // GOTO, GODO_ONCE의 경우 명시적으로 INTENT를 지정한다.
  string intent = 12;
  // 챗봇을 이동하도록 한다.
  // 지정된 챗봇, 지정된 skill로 이동하도록 한다.
  string chatbot = 21;

  // 대화의 결과를 DEVICE까지 내보낼 데이터가 있을 경우에 지정해준다.
  google.protobuf.Struct meta = 31;

  // 대화를 수행한 결과 중 세션 컨텍스트에 정보를 업데이트 해준다.
  maum.m2u.common.Session session_update = 32;
}


/**
 * 사용자의 디바이스에서 처리해야할 추가적인 이벤트를 전송하고 이를 처리하도록 한다.
 * 이 경우에는 발화가 나가지 않고 클라이언트의 응답에 따라서 대화의 내용이 달라지게 된다.
 *
 * 다음 대화 시나리오에서 처리되도록 한다.
 * "아빠에게 전화 걸어줘" --> 단말에 "아빠" 전화번호를 전달하도록 한다.
 * "내 적금에 얼마 들어 있어" --> 단말에 "인증을 수행하여" 사용자 정보를 포함하여 전송하도록 한다.
 *
 * map은 포함된 Directive를 그대로 단말에 전송하게 되며, 단말이 바로 응답을 해야 한다.
 * 응답할 때에는 Dialog.Event를 통해서 전송하게 되며, 이 Event 데이터는 의도분류가 없이
 * 바로 해당 SKILL에 전달되어야 한다.
 *
 * 대화 위임이 발생한 경우, 챗봇 및 SKILL, INTENT, 세션 정보를 전달해준다.
 *
 */
message DialogDelegate {
  // 내려보낼 디렉티브
  // 이때 내려갈 Directive는 반드시 몇가지 제약된
  maum.m2u.common.DelegateDirective directive = 1;

  // 대화의 결과를 DEVICE까지 내보낼 데이터가 있을 경우에 지정해준다.
  google.protobuf.Struct meta = 31;

  // 대화를 수행한 결과 중 세션 컨텍스트에 정보를 업데이트 해준다.
  maum.m2u.common.Session session_update = 32;
}

/**
 * 이벤트 요청
 */
message EventRequest{
  // 이벤트
  maum.m2u.common.Event event = 1;

  // 시스템 컨텍스트
  maum.m2u.common.SystemContext system_context = 2;

  // 세션 및 컨텍스트
  maum.m2u.common.Session session = 3;
}


/**
 * 닫기 시점에 처리해야할 요청
 */
message CloseSkillRequest {
  // 시스템 컨텍스트
  maum.m2u.common.SystemContext system_context = 1;
  // 세션 및 컨텍스트
  maum.m2u.common.Session session = 2;

  // 챗봇
  string chatbot = 11;
  // 스킬
  string skill = 12;
  // 언어
  maum.common.Lang lang = 13;

  // 닫는 이유
  enum CloseReason {
    // 잘 모름
    CLOSED_REASON_UNKNOWN = 0;
    // MAP으로 부터 명시적으로 닫기 호출
    USER_INITIATED = 1;
    // 세션 만료
    EXPIRED = 2;
    // 명시적인 스킬 DA 종료
    DIALOG_ENDED = 3;
    // 라우터 오류
    ROUTER_ERROR = 4;
    // 시스템 오류
    SYSTEM_ERROR = 5;
    // 대화 전이 횟수 초과
    EXCEED_TRANSITION = 6;
  }
  CloseReason reason = 21;
  // 닫는 메시지
  string message = 22;
}

// 딛힌 스킬에 대한 응답 처리
message CloseSkillResponse {
  // 세션 및 컨텍스트 업데이트
  maum.m2u.common.Session update_session = 1;
}