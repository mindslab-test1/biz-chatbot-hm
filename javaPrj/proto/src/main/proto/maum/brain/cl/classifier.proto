syntax = "proto3";

import "google/protobuf/empty.proto";
import "google/protobuf/timestamp.proto";
import "maum/common/lang.proto";
import "maum/common/types.proto";

package maum.brain.cl;

// Query for Classifier Resolver
message Model {
  string model = 1;
  maum.common.LangCode lang = 3;
}

// Classifier Model List
message ModelList {
  repeated Model models = 1;
}

// Classifier Server State
enum ServerState {
  SERVER_STATE_NONE = 0;            // not running
  SERVER_STATE_STARTING = 1;        // parent process
  SERVER_STATE_INITIALIZING = 2;    // sub process
  SERVER_STATE_RUNNING = 3;         // sub process
}

message ServerStatus {
  string model = 1;
  maum.common.LangCode lang = 2;
  bool running = 10;
  ServerState state = 11;
  string server_address = 20;
  string invoked_by = 21;
}

message ServerStatusList {
  repeated ServerStatus servers = 1;
}

enum SetModelResult {
  CL_SETMODEL_SUCCESS = 0;
  CL_SETMODEL_FAILED = 1;
  CL_SETMODEL_SUCCESS_RESTART = 2;
  CL_SETMODEL_SUCCESS_NOTSTARTED = 3;
}

message SetModelResponse {
  string model = 1;
  maum.common.LangCode lang = 2;
  SetModelResult result = 10;
  string error = 100;
}

message ModelCategories {
  string model = 1;
  maum.common.LangCode lang = 2;
  repeated string categories = 11;
  google.protobuf.Timestamp updated_at = 21;
}

// Classifier Resolver Service
service ClassifierResolver {
  rpc Find (Model) returns (ServerStatus);
  rpc GetModels (google.protobuf.Empty) returns (ModelList);
  rpc GetServers (google.protobuf.Empty) returns (ServerStatusList);
  rpc Stop (Model) returns (ServerStatus);
  rpc Restart (Model) returns (ServerStatus);
  rpc Ping (Model) returns (ServerStatus);

  // in.filename = BASE64 encoded string
  // in.lang = (kor, eng)
  // in.model = default or weather baseline and so on
  rpc SetModel (stream maum.common.FilePart) returns (SetModelResponse);
  rpc DeleteModel (Model) returns (ServerStatus);

  // get model categories
  rpc GetModelCategories (Model) returns (ModelCategories);
}

// Classifier Input Text
message ClassInputText {
  string text = 1;
  string model = 2;
  maum.common.LangCode lang = 3;
}

// Classified Item
message Classified {
  string sa = 1;
  float probability = 2;
}

message ClassSentence {
  string text = 1;
  repeated Classified cls = 2;
}

// Classified ITEM
message ClassifiedSummary {
  string c1_top = 1;
  float probability_top = 2;
  repeated ClassSentence sentences = 3;
}

// Proxy Classifier
service ClassifierService {
  rpc GetClass (ClassInputText) returns (ClassifiedSummary);
  rpc GetClassMultiple (stream ClassInputText) returns (stream ClassifiedSummary);
}

// Real Classifier
service Classifier {
  rpc Ping(Model) returns (ServerStatus);
  rpc GetClass (ClassInputText) returns (ClassifiedSummary);
  rpc GetClassMultiple (stream ClassInputText) returns (stream ClassifiedSummary);
}
