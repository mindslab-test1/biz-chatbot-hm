package ai.mindslab.bizchatbot.frontapi.controller;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mysql.fabric.xmlrpc.base.Array;

import ai.mindslab.bizchatbot.commons.annotation.JwtAuth;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotConfig;
import ai.mindslab.bizchatbot.commons.chatbot.dao.data.ChatbotMsg;
import ai.mindslab.bizchatbot.commons.chatbot.service.ChatbotService;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IChatbotConst.SYS_UTTER_TYPE;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.data.BaseResponseMap;
import ai.mindslab.bizchatbot.commons.session.AuthService;
import ai.mindslab.bizchatbot.commons.session.DialogService;
import ai.mindslab.bizchatbot.commons.session.SessionService;
import ai.mindslab.bizchatbot.commons.session.data.DialogSession;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.DialogUtils;
import ai.mindslab.bizchatbot.commons.utils.JsonUtils;
import ai.mindslab.bizchatbot.frontapi.service.BackendApiService;
import ai.mindslab.bizchatbot.frontapi.service.ChatbotContainerInterfaceService;
import kr.co.mobis.bizchatbot.IMobisConsts;
import kr.co.mobis.bizchatbot.personal.controller.data.UserSettingsAll;
import kr.co.mobis.bizchatbot.personal.controller.data.UserSettingsAll.SettingInfo;
import kr.co.mobis.bizchatbot.personal.service.PersonalService;
import ai.mindslab.bizchatbot.commons.data.ReqDialog;
import ai.mindslab.bizchatbot.commons.data.RespDialog;

@RestController
@RequestMapping(path="/dialog")
public class DialogController implements IRestCodes{

	
	private Logger logger  = LoggerFactory.getLogger(DialogController.class);
	
	@Autowired
	private AuthService auth;
	
	@Autowired
	private SessionService sesService;
	
	@Autowired
//	@Qualifier("ChatbotServiceEx")
	private ChatbotService bot;
	
	@Autowired
	private DialogService dialog;
	
	@Autowired
	private ChatbotContainerInterfaceService chatbotIf;
	
    @Autowired
    private PersonalService personal;
	
	@Autowired
	private BackendApiService backendApiService;
	
	final private String CTAG_TEXT = "__TEXT__";
	final private String CTAG_URL  = "__URL__";
	final private String CTAG_GET_LINK = "$$CTAG{\"text\": \"__TEXT__\",\"type\": \"outlink\",\"method\":\"get\",\"url\":\"__URL__\"}CTAG$$";
	
	public DialogController() {
		
	}
	
//	@JwtAuth
	@RequestMapping(value="/openDialog", method = {RequestMethod.POST})
	public @ResponseBody Callable<BaseResponse<RespDialog>> openDialog(@RequestBody ReqDialog reqDialog, HttpServletRequest req) throws Exception{
		logger.info("/openDialog/{}",reqDialog);
		
		Callable<BaseResponse<RespDialog>> callable = new Callable<BaseResponse<RespDialog>>() {

			@Override
			public BaseResponse<RespDialog> call() throws Exception {
				BaseResponse<RespDialog> resp = new BaseResponse<>();
				
				try {
					DialogSession ds = sesService.openNewSession(reqDialog.getUser(), reqDialog.getChatbotId(), req, false);
					ds.setShowDebug(reqDialog.getShowDebug()==null?false:reqDialog.getShowDebug());
					RespDialog respDialog = new RespDialog();
					
					
					ChatbotConfig botConfig = bot.getChatbotConfig(reqDialog.getChatbotId());
					
					respDialog.setChatbotId(reqDialog.getChatbotId());
					respDialog.setSessionId(ds.getSessionId());
					respDialog.setUserId(ds.getUserData().getUserId());
					
					String gmsg = DialogUtils.getRandomMsg(botConfig.getGreetingMsg());
					Object botNotice = null;
					int maibot_id = Integer.parseInt(IMobisConsts.BOT_TYPE_MAIBOT.substring(2));
					
					if(maibot_id == reqDialog.getChatbotId()) {
					    boolean dispEmailYn = false;
					    boolean dispApprovalYn = false;
					    
					    try {
    					    UserSettingsAll allSettings = personal.getUserSettings(ds.getUserData().getUserId());
    					    List<SettingInfo> alarms = allSettings.getAlarms();
    					    
    					    for(SettingInfo item : alarms) {
                                if("US0002".equalsIgnoreCase(item.getType())) {
                                    // email
                                    if("Y".equalsIgnoreCase(item.getEnabledYn()))
                                        dispEmailYn = true;
                                }
                                else if("US0004".equalsIgnoreCase(item.getType())) {
                                    // approval
                                    if("Y".equalsIgnoreCase(item.getEnabledYn()))
                                        dispApprovalYn = true;
                                }
                            };
					    }
					    catch(Exception e) {
					        dispEmailYn = false;
					        dispApprovalYn = false;
					    }

					    BaseResponseMap legacyNotice = backendApiService.doLegacyAlarmQuery(ds.getUserData().getUserId(), Integer.toString(reqDialog.getChatbotId()));
					    if(dispEmailYn || dispApprovalYn) {
    					    if(legacyNotice != null) {
    					        logger.debug("legacyNotice:" + legacyNotice.toString());
    					        //make notice string
    					        String noticeStr = "";
    					        noticeStr += "<div class=\"comment-box\">";
    					        noticeStr += "<p>";
    					        noticeStr += "[마이크로 서비스]";
    					        noticeStr += "\n\n";
    					        noticeStr += "오늘의 주요 알림 안내입니다.";
    					        noticeStr += "\n";
    					        if(dispEmailYn) {
    					            noticeStr += "- 읽지 않은 메일 " + 
    					                    CTAG_GET_LINK
    					                    .replace(CTAG_TEXT, Integer.toString((int)legacyNotice.getData().get("unreadEmailCodunt")) + "건")
    					            .replace(CTAG_URL, (String)legacyNotice.getData().get("mailUrl")) + "\n";
    					            
    					        }
    					        if(dispApprovalYn) {
    					            noticeStr += "- 상신 받은 결재 " +
    					                    CTAG_GET_LINK
    					                    .replace(CTAG_TEXT, Integer.toString((int)legacyNotice.getData().get("approvalCount")) + "건")
    					            .replace(CTAG_URL, (String)legacyNotice.getData().get("approvalUrl")) + "\n";
    					            
    					        }
    					        noticeStr += "\n";
    					        noticeStr += "imobis 로그인 상태에서만 연결됩니다.";
    					        noticeStr += "</p>";
    					        noticeStr += "</div>";
    					        gmsg += noticeStr;
    					        
    					    }
					    }
					    //bot notice
					    try {
					        botNotice = ((HashMap)legacyNotice.getData().get("notice")).get("botNotice");
					    } catch(Exception e) {
					        botNotice = null;
					    }
					}
					resp.setData(respDialog);

					UtterData ud = DialogUtils.createBotUtterData(ds);
					ud.setSysUtterType(SYS_UTTER_TYPE.GREETING);
					ud.setSentence(gmsg);
					ud.setBotNotice(botNotice);
					ud.setRichContent(botConfig.getGreetingRichContent());
					ds.getDialog().getConversation().add(ud);
					ud.setContentJson(JsonUtils.toJson(ud.getRichContent()));
					ud.setCurrStep(IChatbotConst.FILTER_STEP_INIT);
					//insert history
					dialog.insertBotUtterHistory(ud.getBotId(), reqDialog.getUser().getUserId(), ds.getSessionId(), ud.getSentence(), 1, ud.getContentJson(), "", "", ud.getCurrStep());
					
					//set to response
					respDialog.setUttrData(ud);
					//store to session
					sesService.storeDialogSession(ds);
					
					BaseResponse<UtterData> r = chatbotIf.sendOpenDialog(ds.getSessionId());
					//QQQ ignore sds greeting.
//					respDialog.setUttrData(r.getData());
					//QQQ err code checking?
					if(r.getCode() != ERR_CODE_SUCCESS) {
						resp.setData(null);
						resp.setCode(r.getCode());
						resp.setMsg(r.getMsg());
//						respDialog.setUttrData(r.getData());
						resp.setData(respDialog);
					}
				}catch(Exception e) {
					logger.warn("/dialog/openDialog/{}",e.getMessage());
					resp.setCode(ERR_CODE_CHATBOT_INIT_ERROR);
					resp.setMsg(ERR_MSG_CHATBOT_INIT_ERROR);
					resp.setData(null);
				}
				return resp;
			}
		};
		
		return callable;
	}
	
	
//	@JwtAuth
	@RequestMapping(value="/sendText", method = {RequestMethod.POST})
	public @ResponseBody Callable<BaseResponse<UtterData>> sendText(@RequestBody UtterData reqDialog, HttpServletRequest req) throws Exception{
		logger.info("/sendText/{}",reqDialog);
		
		Callable<BaseResponse<UtterData>> sdsResp = dialog.sendText(reqDialog, chatbotIf);
//		BaseResponse<UtterData> r = chatbotIf.sendUserUtter(reqDialog.getSessionId());
//		if(r.getCode() == ERR_CODE_SUCCESS) {
//			return sdsResp;
//		}else {
//			return new Callable<BaseResponse<UtterData>>(){
//
//				@Override
//				public BaseResponse<UtterData> call() throws Exception {
//					BaseResponse<UtterData> err = new BaseResponse<>(ERR_CODE_SDS_OPEN_ERROR, ERR_MSG_SDS_OPEN_ERROR);
//					return err;
//				}
//			};
//		}
		return sdsResp;
	}
	
//	@JwtAuth
	@RequestMapping(value="/closeDialog", method = {RequestMethod.POST})
	public BaseResponse<?> closeDialog(@RequestBody ReqDialog reqDialog, HttpServletRequest req) throws Exception{
		logger.info("/closeDialog/{}",reqDialog);
		
		if(!sesService.isValidSession(reqDialog.getSessionId())) {
			return new BaseResponse<>(IChatbotConst.ERR_CODE_INVALID_SESSION_ID, IChatbotConst.ERR_MSG_INVALID_SESSION_ID);
		}
		
		BaseResponse<UtterData> resp = null;
		try {
			resp = chatbotIf.closeDialog(reqDialog.getSessionId());
			//QQQ directly remove?
			//sesService.closeSession(reqDialog.getUser(), reqDialog.getChatbotId(), reqDialog.getSessionId());
		}catch(Exception e) {
			logger.warn("/closeDialog/{}", e.getMessage());
			resp = new BaseResponse<>(ERR_CODE_FAILURE, ERR_MSG_FAILURE);
		}
		
		return resp;
	}
}
