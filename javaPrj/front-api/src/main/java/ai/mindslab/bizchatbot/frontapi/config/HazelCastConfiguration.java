package ai.mindslab.bizchatbot.frontapi.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import com.hazelcast.config.ReplicatedMapConfig;

import ai.mindslab.bizchatbot.commons.codes.IEnv;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jaeheoncho on 2018. 4. 20..
 */
//@Configuration
public class HazelCastConfiguration implements IEnv{

    @Bean
    public Config hazelCastConfig(){
        return new Config()
                .setInstanceName(ENV_HAZELCAST_INSTANCE)
                .addMapConfig(
                        new MapConfig()
                                .setName("configuration")
                                .setBackupCount(2)
                                .setTimeToLiveSeconds(600)
                                .setMaxSizeConfig(new MaxSizeConfig(10000, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
                                .setEvictionPolicy(EvictionPolicy.LRU)
                                .setTimeToLiveSeconds(300))
                					;
    }
}
