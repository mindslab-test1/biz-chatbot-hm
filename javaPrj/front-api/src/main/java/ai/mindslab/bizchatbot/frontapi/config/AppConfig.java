package ai.mindslab.bizchatbot.frontapi.config;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;

@Configuration
@EnableCaching
@MapperScan(basePackages={"ai.mindslab.bizchatbot.commons.chatbot.dao"
		,"ai.mindslab.bizchatbot.commons_ex.bqa.rich.dao"
		,"ai.mindslab.bizchatbot.commons_ex.gitf.dao"
		,"ai.mindslab.bizchatbot.frontapi.dao"
})
public class AppConfig {

	Logger logger = LoggerFactory.getLogger(AppConfig.class);
	
	@Value("${hazelcast.server.ip}")
	private String[] hazelIps;
	
	@Value("${hazelcast.group.name}")
	private String groupName;
	
	@Value("${hazelcast.group.password}")
	private String groupPass;
	
	@Autowired
	@Qualifier("hazelClientCommon")
	private HazelClientCommon hazelClient;
	
//	@Bean
//    public CacheManager cacheManager() {
//        
//        ClientConfig clientConfig = new ClientConfig();
//		clientConfig.getGroupConfig().setName(groupName).setPassword(groupPass);
//		clientConfig.getNetworkConfig().addAddress(hazelIps);
//		clientConfig.getNetworkConfig().setConnectionAttemptLimit(5);
//		clientConfig.getNetworkConfig().setConnectionAttemptPeriod(5000);
//		
//		HazelcastInstance client = HazelcastClient.newHazelcastClient(clientConfig);
//
//        return new HazelcastCacheManager(client);
//    }
	@Bean
	@Qualifier("hazelCacheManager")
	public CacheManager cacheManager() {
		return new HazelcastCacheManager(hazelcastInstance());
	}
	
	@Bean
//    @Profile("client")
    public HazelcastInstance hazelcastInstance() {
		return hazelClient.getClient();
    }
	
	@Bean
    public KeyGenerator keyGenerator() {
        return new SimpleKeyGenerator();
    }
	
	@Bean
	public Jackson2ObjectMapperBuilder objectMapperBuilder() {
	    Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
//	    builder.serializationInclusion(JsonInclude.Include.NON_NULL);
	    builder.dateFormat(new ISO8601DateFormat());
	    return builder;
	}
}
