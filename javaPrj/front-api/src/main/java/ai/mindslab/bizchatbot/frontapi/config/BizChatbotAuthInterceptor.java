package ai.mindslab.bizchatbot.frontapi.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.WebAsyncManager;
import org.springframework.web.context.request.async.WebAsyncUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.auth0.jwt.interfaces.DecodedJWT;

import ai.mindslab.bizchatbot.commons.annotation.JwtAuth;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotAuthException;
import ai.mindslab.bizchatbot.commons.session.AuthService;
import ai.mindslab.bizchatbot.frontapi.controller.DialogController;

@Component
public class BizChatbotAuthInterceptor extends HandlerInterceptorAdapter {
	
	private Logger logger = LoggerFactory.getLogger(BizChatbotAuthInterceptor.class);
	private static final Object CALLABLE_INTERCEPTOR_KEY = new Object();

	@Autowired
	private AuthService auth;
	
	public BizChatbotAuthInterceptor() {
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
//		return super.preHandle(request, response, handler);
//		if(handler instanceof DialogController) {
			
			HandlerMethod handlerMethod = (HandlerMethod)handler;
//			JwtAuth filter = handlerMethod.getMethod().getAnnotation(JwtAuth.class);
			JwtAuth filter = handlerMethod.getMethod().getAnnotation(JwtAuth.class);
			if(filter != null) {
				String token = request.getHeader("Authentication");
				if(token != null) {
					token=token.substring(7);
					DecodedJWT jwt = auth.verifyToken(token);
					logger.debug("auth info:{}", jwt);
				}else {
					logger.debug("token is null");
					return false;
				}
			}
//		}
		
		return true;
	}

//	@Override
//	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
//			ModelAndView modelAndView) throws Exception {
//		// TODO Auto-generated method stub
////		super.postHandle(request, response, handler, modelAndView);
//	}

//	@Override
//	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler)
//			throws Exception {
////		super.afterConcurrentHandlingStarted(request, response, handler);
//		logger.warn("async interceptor");
//		throw new BizChatbotAuthException(IRestCodes.ERR_CODE_DIALOG_TIMEOUT, IRestCodes.ERR_MSG_DIALOG_TIMEOUT);
//	}
	
	
	
}
