package ai.mindslab.bizchatbot.frontapi.config;

import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Created by jaeheoncho on 2018. 4. 20..
 */
@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler implements IRestCodes{

	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		// TODO Auto-generated method stub
//		return super.handleMethodArgumentNotValid(ex, headers, status, request);
		
		BaseResponse<String> err = new BaseResponse<>(ERR_CODE_PARAMS_INVALID, ERR_MSG_PARAMS_INVALID);
		return new ResponseEntity(err, headers, status) ;
	}

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		// TODO Auto-generated method stub
//		return super.handleMissingServletRequestParameter(ex, headers, status, request);
		
		BaseResponse<String> err = new BaseResponse<>(ERR_CODE_PARAMS_INVALID, ERR_MSG_PARAMS_INVALID);
		return new ResponseEntity(err, headers, status) ;
	}

	//Sample
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		// TODO Auto-generated method stub
//		return super.handleExceptionInternal(ex, body, headers, status, request);
		
		BaseResponse<String> resp = null;
		if(ex instanceof BizChatbotException) {
			resp = new BaseResponse<>(ERR_CODE_FAILURE, ERR_MSG_FAILURE);//test
		}else if(ex instanceof HttpMessageNotReadableException){//no parameer
			resp = new BaseResponse<>(ERR_CODE_PARAMS_INVALID, ERR_MSG_PARAMS_INVALID);
		}else {
			resp = new BaseResponse<>(ERR_CODE_FAILURE, ERR_MSG_FAILURE);
		}
		
		return new ResponseEntity<Object>(resp, headers, status);
	}
	
	//async exception handler
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<BaseResponse<String>> handleAllExceptions(Exception ex, WebRequest request) {
		BaseResponse<String> errorDetails = null;
		if(ex instanceof BizChatbotException) {
			BizChatbotException ee = (BizChatbotException) ex;
			errorDetails = new BaseResponse<String>(ee.getErrCode(), ee.getErrMsg());
			
		}else {
			errorDetails = new BaseResponse<String>(ERR_CODE_FAILURE, ERR_MSG_FAILURE);
		}
		return new ResponseEntity<>(errorDetails, HttpStatus.OK);
	}
	

	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		// TODO Auto-generated method stub
//		return super.handleHttpRequestMethodNotSupported(ex, headers, status, request);
		BaseResponse<Object> err = new BaseResponse<>(ERR_CODE_HTTP_INVALID_METHOD, ERR_MSG_HTTP_INVALID_METHOD);
		return new ResponseEntity<>(err, headers, status) ;
	}
	
}
