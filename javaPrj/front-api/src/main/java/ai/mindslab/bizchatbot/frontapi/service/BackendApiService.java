package ai.mindslab.bizchatbot.frontapi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.data.BaseResponseMap;
import ai.mindslab.bizchatbot.commons.session.data.BasicQaResponse;
import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;
import ai.mindslab.bizchatbot.commons.utils.JsonUtils;

@Service
public class BackendApiService {
    private Logger logger = LoggerFactory.getLogger(BackendApiService.class);
    
    @Value("${backend.api.url}")
    private String _url;
    
    public BackendApiService() {
        
    }
    
    public BaseResponseMap doLegacyAlarmQuery(String userId, String botId) {
        logger.info("doLegacyAlarmQuery/{},{}", userId, botId);
        String url = _url;
        try {
            if(userId == null || StringUtils.isEmpty(userId) || 
               botId == null  || StringUtils.isEmpty(botId)) {
                throw new Exception("Ivalid paramters for doLegacyAlarmQuery");
            }
            
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("userId", userId.trim()));
            params.add(new BasicNameValuePair("botId", botId.trim()));
            
            Header[] headers = HttpConnectionManager.getHeader();
            String resp = HttpConnectionManager.getInstance().post(url+"/mobis/legacy/legacyAlarm", params, headers);
            BaseResponseMap out = JsonUtils.fromJson(resp, BaseResponseMap.class);
            logger.debug("doBqaQuery/{}",resp);
            if(out.getCode() == IRestCodes.ERR_CODE_SUCCESS) {
                logger.debug("doLegacyAlarmQuery resp/{}", out.getData() != null ? out.getData().toString(): "");
                return out;
            } else {
                return null;
            }
        }
        catch(Exception e) {
            logger.warn("doLegacyAlarmQuery/{}", e.getMessage());
        }
        return null;
    }
}
