package ai.mindslab.bizchatbot.frontapi.config;

import java.util.concurrent.TimeUnit;

import javax.cache.CacheManager;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.Duration;
import javax.cache.expiry.TouchedExpiryPolicy;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.stereotype.Component;

@Component
public class CacheConfig implements JCacheManagerCustomizer{

	public CacheConfig() {
	}

	@Override
	public void customize(CacheManager cacheManager) {
		cacheManager.createCache("chatbot-objects",
				new MutableConfiguration<>()
						.setExpiryPolicyFactory(TouchedExpiryPolicy.factoryOf(new Duration(TimeUnit.SECONDS, 300)))
						.setStoreByValue(false).setStatisticsEnabled(true));
	}

}
