package ai.mindslab.bizchatbot.frontapi.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import org.apache.http.Header;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.interfaces.IChatbotInterface;
import ai.mindslab.bizchatbot.commons.session.data.UtterData;
import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;
import ai.mindslab.bizchatbot.commons.utils.JsonUtils;

@Service
public class ChatbotContainerInterfaceService implements IChatbotInterface{
	private Logger logger = LoggerFactory.getLogger(ChatbotContainerInterfaceService.class);
	
	private final String _url = "http://__host__:__port__/chatbot-container/dialog/";
	
	@Value("${bizchatbot.container.ips}")
	private String[] ips;
	
	@Value("${bizchatbot.container.port}")
	private int port;
	
	public ChatbotContainerInterfaceService() {
		// TODO Auto-generated constructor stub
	}

	public BaseResponse<UtterData> sendOpenDialog(String sessionId) {
		logger.info("sendOpenDialog/{}",sessionId);
		
		//QQQ loadbalancing algorithm?
		BaseResponse<UtterData> sdsResp = null;
		
		String ip = ips[0];
		String url = _url.replaceAll("__host__", ip).replaceAll("__port__", port+"")+"openDialog";
		ArrayList<BasicNameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("sessionId", sessionId));
		
		try {
			Header[] headers = HttpConnectionManager.getHeader();
			String resp = HttpConnectionManager.getInstance().post(url, params, headers);
			sdsResp = fromJsonRespUtter(resp);
		} catch (Exception e) {
			logger.warn("sendOpenDialog/{}/{}",sessionId,e.getMessage());
			sdsResp = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
		}
		
		return sdsResp;
	}
	
	private static ObjectMapper mapper = null;
	
	static{
		mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
//		mapper.setDateFormat(new ISO8601DateFormat());
		mapper.setTimeZone(TimeZone.getTimeZone("GMT+09:00"));
	}
	
	private static BaseResponse<UtterData> fromJsonRespUtter(String jsonString) {
		
		String l_jsonString = jsonString;
		if(l_jsonString == null){
			l_jsonString = "{}";
		}
		BaseResponse<UtterData> out = null;
		try {
			out = mapper.readValue(jsonString, new TypeReference<BaseResponse<UtterData>>() {});
		} catch (JsonProcessingException e) {
		} catch (IOException e) {
		}
		
		return out;
	}
	
	public BaseResponse<UtterData> sendUserUtter(String sessionId) {
		logger.info("sendUserUtter/{}",sessionId);
		
		//QQQ loadbalancing algorithm?
		BaseResponse<UtterData> sdsResp = null;
		
		String ip = ips[0];
		String url = _url.replaceAll("__host__", ip).replaceAll("__port__", port+"")+"userUtter";
		ArrayList<BasicNameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("sessionId", sessionId));
		
		try {
			Header[] headers = HttpConnectionManager.getHeader();
			String resp = HttpConnectionManager.getInstance().post(url, params, headers);
			sdsResp = fromJsonRespUtter(resp);
		} catch (Exception e) {
			sdsResp = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
			logger.warn("sendUserUtter/{}/{}",sessionId,e.getMessage());
		}
		
		return sdsResp;
	}
	
	public BaseResponse<UtterData> closeDialog(String sessionId) {
		logger.info("closeDialog/{}",sessionId);
		
		//QQQ loadbalancing algorithm?
		BaseResponse<UtterData> sdsResp = null;
		
		String ip = ips[0];
		String url = _url.replaceAll("__host__", ip).replaceAll("__port__", port+"")+"closeDialog";
		ArrayList<BasicNameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("sessionId", sessionId));
		
		try {
			Header[] headers = HttpConnectionManager.getHeader();
			String resp = HttpConnectionManager.getInstance().post(url, params, headers);
			sdsResp = fromJsonRespUtter(resp);
		} catch (Exception e) {
			sdsResp = new BaseResponse<>(IRestCodes.ERR_CODE_FAILURE, IRestCodes.ERR_MSG_FAILURE);
			logger.warn("closeDialog/{}/{}",sessionId,e.getMessage());
		}
		
		return sdsResp;
	}
}
