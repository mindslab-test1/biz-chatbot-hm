package ai.mindslab.bizchatbot.frontapi.service;

import ai.mindslab.bizchatbot.commons.session.data.UserInfoData;
import ai.mindslab.bizchatbot.frontapi.dao.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoService {

    private Logger logger = LoggerFactory.getLogger(UserInfoService.class);

    @Autowired
    private UserDao userDao;

    public UserInfoData getUserData(String userId) throws Exception{
        return userDao.getUserData(userId);
    }

    public int insertUserAccessHistory(UserInfoData  vo) throws Exception {
        return userDao.insertUserAccessHistory(vo);
    }
}
