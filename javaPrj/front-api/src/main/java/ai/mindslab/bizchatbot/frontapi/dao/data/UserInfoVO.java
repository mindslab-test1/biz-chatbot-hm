package ai.mindslab.bizchatbot.frontapi.dao.data;

import java.io.Serializable;

public class UserInfoVO implements Serializable {

    private String userId;
    private String userName;
    private String channelType;
    private String team;

}
