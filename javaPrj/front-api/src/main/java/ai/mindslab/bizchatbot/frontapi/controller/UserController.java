package ai.mindslab.bizchatbot.frontapi.controller;

import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotAuthException;
import ai.mindslab.bizchatbot.commons.hazelcast.HazelClientCommon;
import ai.mindslab.bizchatbot.commons.session.AuthService;
import ai.mindslab.bizchatbot.commons.session.SessionService;
import ai.mindslab.bizchatbot.commons.session.UserService;
import ai.mindslab.bizchatbot.commons.session.data.AccessInfo;
import ai.mindslab.bizchatbot.commons.session.data.UserData;
import ai.mindslab.bizchatbot.commons.session.data.UserInfoData;
import ai.mindslab.bizchatbot.commons.utils.HazelClient;
import ai.mindslab.bizchatbot.frontapi.dao.data.UserInfoVO;
import ai.mindslab.bizchatbot.frontapi.service.UserInfoService;
import kr.co.mobis.bizchatbot.personal.dao.data.UserInfoSimple;
import kr.co.mobis.bizchatbot.personal.service.PersonalService;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by jaeheoncho on 2018. 4. 20..
 */
@RestController
public class UserController implements IRestCodes, IChatbotConst{

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private HazelClientCommon client;
    
    @Autowired
    private SessionService session;
    
    @Autowired
    private UserService userService;

    @Autowired
	private UserInfoService userInfoService;
    
    @Autowired
    private PersonalService personal;
    
    @Autowired
    private AuthService auth;

//    @RequestMapping(value="/putdata",method = {RequestMethod.GET})
//    public BaseResponse<?> putData (@RequestParam(value="key",required = true, defaultValue = "aa") String key, @RequestParam("value") String value) {
//        logger.info("putdata/{},{}",key,value);
//        java.util.Map<String,String> stringStringMap = client.getMap(MAP_NAME_SESSION);
//        
//
//        try {
////            Cache c = cacheManager.getCache(MAP_NAME_SESSION);
//        	stringStringMap.put("key", value);
//
//            return new BaseResponse<>(0,"success");
//        }catch(Exception e){
//            logger.error("putdata/{}/{}",key,value);
//            return new BaseResponse<>(1,"err/"+e.getMessage());
//        }
//    }
//
//    @GetMapping("/getdata")
//    public BaseResponse<?> getData(@RequestParam(value="key",required = true, defaultValue = "aa")String key){
//        logger.info("getdata/{}",key);
//        java.util.Map<String,String> stringStringMap = client.getMap(MAP_NAME_SESSION);
//        
//        try {
//        	
//        		String val = stringStringMap.get(key);
//        	
//        	
//        		logger.debug("getdata,{}",val);
//        		return new BaseResponse<>(0, val);
//        }catch(Exception e){
//
//            logger.error("putdata/{}",key);
//            return new BaseResponse<String>(1,e.getMessage());
//        }
//    }
    
    @PostMapping("/login")
    public BaseResponse<?> login(@RequestParam(value="userId",required = true)String userId,
							     @RequestParam(value="channelType",required = false)String channelType,
								 @RequestParam(value="userIp",required = false)String userIp,
								 HttpServletRequest req) throws BizChatbotAuthException, Exception{

    	logger.info("/login Test/ {}", userIp);

		if (userIp.substring(0, 7).equals("::ffff:")) {
			userIp = userIp.substring(7);
		}
    	
		AccessInfo user = null;
		user = (AccessInfo) client.getMap(MAP_NAME_USER).get(userId);
    	
		try {
			if(user == null) {
				//retrieve user data.
				UserData userData = userService.login(userId);//including token
				UserInfoSimple userInfoDb = null;
				if(userData != null) {
					userInfoDb = personal.getUserInfo(userId);
					if(userInfoDb == null) {
						throw new Exception("User "+  userId+ " is not exist.");
					}
					user = new AccessInfo();
					user.setDeptCd(userInfoDb.getDeptCd());
					user.setDeptName(userInfoDb.getDeptNm());
					user.setToken(userData.getToken());
					user.setUserId(userId);
					user.setUserName(userInfoDb.getUserNm());
					user.setUserDivCode(userInfoDb.getUserDivCode());
					user.setUserDivNm(userInfoDb.getUserDivNm());
					user.setConfirmYn(userInfoDb.getConfirmYn());
					user.setRemoteAddress(userIp);
        	    		
					client.getMap(MAP_NAME_USER).put(userId, user);
				}else {
					throw new Exception("Token issuing error.");
				}
			}else {
				UserInfoSimple userInfoDb = personal.getUserInfo(userId);
				user.setConfirmYn(userInfoDb.getConfirmYn());
				client.getMap(MAP_NAME_USER).put(userId, user);
			}
    			
		} catch (Exception e) {
			logger.warn("login/{}", e.getMessage());
			throw new BizChatbotAuthException(ERR_CODE_AUTH_ERROR, ERR_MSG_AUTH_ERROR);
		}

		if(!StringUtils.isEmpty(userIp)
				  && !userIp.equals(user.getRemoteAddress())) {
			user.setRemoteAddress(userIp);
			client.getMap(MAP_NAME_USER).put(userId, user);
		}
		
		BaseResponse<UserData> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		resp.setData(user);

		UserInfoData vo = userInfoService.getUserData(userId);
		int result  = 0;
		if(vo != null){
			logger.info("insert user History {}" + vo);
			vo.setChannelType(channelType == null ? "none" : channelType);
			result = userInfoService.insertUserAccessHistory(vo);
		}

		return resp;
    }


	@PostMapping("/out")
	public BaseResponse<?> out(@RequestParam(value="userId",required = true)String userId,
								HttpServletRequest req) throws BizChatbotAuthException, Exception{

		AccessInfo user = null;
		user = (AccessInfo) client.getMap(MAP_NAME_USER).get(userId);

		try {
			if(user != null) {
				client.getMap(MAP_NAME_USER).remove(userId);
			}

		} catch (Exception e) {
			logger.warn("login/{}", e.getMessage());
			throw new BizChatbotAuthException(ERR_CODE_AUTH_ERROR, ERR_MSG_AUTH_ERROR);
		}
		BaseResponse<UserData> resp = new BaseResponse<>(ERR_CODE_SUCCESS, ERR_MSG_SUCCESS);
		return resp;
	}


	public  String getClientIp(HttpServletRequest request) {
		String ip = request.getHeader("X-FORWARDED-FOR");

		/*if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = request.getHeader("HTTP_X_FORWARDED");
		}
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = request.getHeader("HTTP_X_CLUSTER_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = request.getHeader("HTTP_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = request.getHeader("HTTP_FORWARDED");
		}
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = request.getHeader("HTTP_VIA");
		}
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = request.getHeader("REMOTE_ADDR");
		}
		if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
			ip = request.getRemoteAddr();
			System.out.println("IP ADDRESS : " + ip);
		}*/
		return ip;
	}

}
