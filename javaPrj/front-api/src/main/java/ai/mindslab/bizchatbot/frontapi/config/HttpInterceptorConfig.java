package ai.mindslab.bizchatbot.frontapi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class HttpInterceptorConfig extends WebMvcConfigurerAdapter{

	@Autowired
	private BizChatbotAuthInterceptor auth;
	
	@Autowired
	private BizChatbotAsyncInterceptor async;
	
	public HttpInterceptorConfig() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		
//		super.addInterceptors(registry);
 		registry.addInterceptor(auth)
 			.addPathPatterns("/**"); //exclude?
// 		registry.addInterceptor(async)
// 			.addPathPatterns("/**");
	}
}
