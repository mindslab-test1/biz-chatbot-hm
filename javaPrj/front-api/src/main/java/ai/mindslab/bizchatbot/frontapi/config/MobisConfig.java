package ai.mindslab.bizchatbot.frontapi.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan(basePackages={"kr.co.mobis.bizchatbot.personal.dao"
		,"kr.co.mobis.bizchatbot.legacy.user.dao"
		,"ai.mindslab.bizchatbot.commons_ex.preitf.dao"})
@ComponentScan(basePackages= {"kr.co.mobis.bizchatbot"})
public class MobisConfig {

}
