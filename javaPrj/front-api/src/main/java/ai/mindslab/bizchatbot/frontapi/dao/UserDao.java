package ai.mindslab.bizchatbot.frontapi.dao;

import ai.mindslab.bizchatbot.commons.session.data.UserInfoData;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao  {
    public UserInfoData getUserData(String userId)  throws Exception;
    public int insertUserAccessHistory(UserInfoData vo)  throws Exception;
}
