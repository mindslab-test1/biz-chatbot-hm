package kr.co.mobis.bizchatbot.personal.controller.data;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("serial")
public class UserSettingsAll implements Serializable{

	protected List<SettingInfo> alarms;
	protected List<SettingInfo> bots;
	
	public UserSettingsAll() {
	}
	
	public List<SettingInfo> getAlarms() {
		return alarms;
	}

	public void setAlarms(List<SettingInfo> alarms) {
		this.alarms = alarms;
	}

	public List<SettingInfo> getBots() {
		return bots;
	}

	public void setBots(List<SettingInfo> bots) {
		this.bots = bots;
	}

	@Override
	public String toString() {
		return "UserSettingsAll{" +
				"alarms=" + alarms +
				", bots=" + bots +
				'}';
	}

	public static class SettingInfo implements Serializable{
		
		protected String type;

		@JsonProperty("fixedYn")
		protected String fixedYn;
		
		@JsonProperty("showYn")
		protected String enabledYn;
		
		public SettingInfo() {
		}
		
		public SettingInfo(String type, String settingValue) {
			this.type = type;
			String[] values = settingValue.split(",");
			if (values == null || values.length != 2) {
				this.fixedYn = "Y";
				this.enabledYn = "Y";
			} else {
				this.fixedYn = values[0];
				this.enabledYn = values[1];
			}
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getFixedYn() {
			return fixedYn;
		}

		public void setFixedYn(String fixedYn) {
			this.fixedYn = fixedYn;
		}

		public String getEnabledYn() {
			return enabledYn;
		}

		public void setEnabledYn(String enabledYn) {
			this.enabledYn = enabledYn;
		}

		@Override
		public String toString() {
			return "SettingInfo{" +
					"type='" + type + '\'' +
					", fixedYn='" + fixedYn + '\'' +
					", enabledYn='" + enabledYn + '\'' +
					'}';
		}
	}
}
