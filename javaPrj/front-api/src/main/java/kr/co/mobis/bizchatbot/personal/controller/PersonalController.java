package kr.co.mobis.bizchatbot.personal.controller;

import ai.mindslab.bizchatbot.commons.codes.IRestCodes;
import ai.mindslab.bizchatbot.commons.data.BaseResponse;
import ai.mindslab.bizchatbot.commons.exceptions.BizChatbotException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import kr.co.mobis.bizchatbot.IMobisConsts;
import kr.co.mobis.bizchatbot.personal.controller.data.UserSettingsAll;
import kr.co.mobis.bizchatbot.personal.dao.data.PingPongData;
import kr.co.mobis.bizchatbot.personal.dao.data.UserBookmark;
import kr.co.mobis.bizchatbot.personal.dao.data.UserComment;
import kr.co.mobis.bizchatbot.personal.service.PersonalService;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path="/mobis/personal")
public class PersonalController implements IRestCodes, IMobisConsts{
	
	private Logger logger = LoggerFactory.getLogger(PersonalController.class);
	
	@Autowired
	private PersonalService personal;
	
	@RequestMapping(value="/settings", method = {RequestMethod.POST})
	public BaseResponse<?> settings(@RequestParam(name="userId", required=true)String userId
			,HttpServletRequest request) throws Exception{
		
		logger.info("settings/{}",userId);
		BaseResponse<UserSettingsAll> resp = null;
		try {
			UserSettingsAll allSettings = personal.getUserSettings(userId);
			resp = new BaseResponse<>();
			resp.setData(allSettings);
		}catch(Exception e) {
			throw new BizChatbotException(ERR_CODE_SQL_EXCEPTION, ERR_MSG_SQL_EXCEPTION);
		}
		
		return resp;
	}

	@RequestMapping(value="/updateAllSettings", method = {RequestMethod.POST})
	public BaseResponse<?> updateAllSettings(@RequestBody JsonNode dataNode
			,HttpServletRequest request) throws Exception{

		logger.info("updateAllSettings/{}",dataNode);
		BaseResponse<String> resp = null;
		try {
			long nRet = personal.updateAllSettings(dataNode.get("userId").asText(), dataNode.get("setting"));
			resp = new BaseResponse<>();
			if(nRet < 1) {
				resp.setCode(ERR_CODE_FAILURE);
				resp.setMsg(ERR_MSG_FAILURE);
			}
		}catch(Exception e) {
			throw new BizChatbotException(ERR_CODE_SQL_EXCEPTION, ERR_MSG_SQL_EXCEPTION);
		}
		return resp;
	}
	
	@RequestMapping(value="/bookmark/list", method = {RequestMethod.POST})
	public BaseResponse<?> getUserBookmark(@RequestParam(name="userId", required=true)String userId
	        ,@RequestParam(name="cnt", required=true)int cnt
            ,HttpServletRequest request)  throws Exception {
	    logger.info("getUserBookmark/{},{}",userId, cnt);
	    BaseResponse<List<UserBookmark>> resp = null;
	    try {
	        List<UserBookmark> list = personal.getUserBookmark(userId, cnt);
	        resp = new BaseResponse<>();
	        resp.setData(list);
	    } catch(Exception e) {
	        throw new BizChatbotException(ERR_CODE_SQL_EXCEPTION, ERR_MSG_SQL_EXCEPTION);
	    }
	    return resp;
	}
	
	@RequestMapping(value="/bookmark/insert", method = {RequestMethod.POST})
	public BaseResponse<?> insertUserBookmark(
	        @RequestParam(name="userId", required=true)String userId,
	        @RequestParam(name="rObjectId", required=true)String rObjectId,
	        @RequestParam(name="rObjectName", required=true)String rObjectName,
	        @RequestParam(name="botId", required=true)int botId
	        ) throws Exception {
	    logger.info("insertUserBookmark/{},{},{},{}",userId, rObjectId, rObjectName, botId);
	    BaseResponse<String> resp = null;
	    try {
	        UserBookmark ub = new UserBookmark();
	        ub.setUserId(userId);
	        ub.setrObjectId(rObjectId);
	        ub.setrObjectName(rObjectName);
	        ub.setBotId(botId);
	        long nRet = personal.insertUserBookmark(ub);
	        resp = new BaseResponse<>();
	        if(nRet < 1) {
	            resp.setCode(ERR_CODE_FAILURE);
	            resp.setMsg(ERR_MSG_FAILURE);
	        }
        }catch(Exception e) {
            throw new BizChatbotException(ERR_CODE_SQL_EXCEPTION, ERR_MSG_SQL_EXCEPTION);
        }
        return resp;
	}
	
	@RequestMapping(value="/bookmark/delete", method = {RequestMethod.POST})
	public BaseResponse<?> deleteUserBookmark(
	        @RequestParam(name="userId", required=true)String userId,
            @RequestParam(name="rObjectId", required=true)String rObjectId
	        ) throws Exception {
	    logger.info("insertUserBookmark/{},{}",userId, rObjectId);
	    BaseResponse<String> resp = null;
        try {
            UserBookmark ub = new UserBookmark();
            ub.setUserId(userId);
            ub.setrObjectId(rObjectId);

            long nRet = personal.deleteUserBookmark(ub);
            resp = new BaseResponse<>();
            if(nRet < 1) {
                resp.setCode(ERR_CODE_FAILURE);
                resp.setMsg(ERR_MSG_FAILURE);
            }
        }catch(Exception e) {
            throw new BizChatbotException(ERR_CODE_SQL_EXCEPTION, ERR_MSG_SQL_EXCEPTION);
        }
        return resp;
	}
	
    @RequestMapping(value="/comment/insert", method = {RequestMethod.POST})
    public BaseResponse<?> insertUserComment(
            @RequestParam(name="userId", required=true)String userId,
            @RequestParam(name="botId", required=true)int botId,
            @RequestParam(name="channel", required=true)String channel,
            @RequestParam(name="title", required=true)String title,
            @RequestParam(name="content", required=true)String content
            ) throws Exception {
        logger.info("insertUserComment/{},{},{},{}",userId, botId, channel, title);
        BaseResponse<String> resp = null;
        try {
            UserComment uc = new UserComment();
            uc.setUserId(userId);
            uc.setBotId(botId);
            uc.setChannel(channel);
            uc.setTitle(title);
            uc.setContent(content);
            long nRet = personal.insertUserComment(uc);
            resp = new BaseResponse<>();
            if(nRet < 1) {
                resp.setCode(ERR_CODE_FAILURE);
                resp.setMsg(ERR_MSG_FAILURE);
            }
        }catch(Exception e) {
            throw new BizChatbotException(ERR_CODE_SQL_EXCEPTION, ERR_MSG_SQL_EXCEPTION);
        }
        return resp;
    }


	@RequestMapping(value="/pingpong/send", method = {RequestMethod.POST})
	public BaseResponse<?> insertUserComment( @RequestParam(name="message", required=true) String message,
											  @RequestParam(name="userId", required=true)String userId){

		String pingPongUrl  ="https://api.pingpong.us/v0.1/reaction/custom/mobis?query=_message_&from=mobis";



		BaseResponse<String> resultMessage = new BaseResponse<>();
		JSONObject json = new JSONObject();
	//	String test ="[{\"model\": \"10k/지시행위\", \"model_score\": 1.8657574653625488, \"message\": \"맞춰봐요\"}, {\"model\": \"10k/단순반응\", \"model_score\": 1.3970614671707153, \"message\": \"비밀이에요\"}, {\"model\": \"10k/YES/NO 질문\", \"model_score\": 1.3920546770095825, \"message\": \"궁금해요?\"}]";
		try {
			//모든방법을 동원한 공백제거...
			// 트림제거, 띄어쓰기..html 표현제거...정규식으로 앞뒤 공백제거..
			String modify_message = message.trim().replaceAll(" ", "").replaceAll("\\p{Z}", "").replaceAll("(^\\p{Z}+|\\p{Z}+$)", "");

            logger.info("pinpong send, {}, {},", userId, modify_message);

			String resp =  personal.sendPingPongMessage(pingPongUrl, modify_message);
			JsonParser jsonParser = new JsonParser();
			JsonObject obj = null;

			if(resp != null && resp.length()  > 0)  {
				JsonArray jsonArray = (JsonArray) jsonParser.parse(resp);
				obj = jsonArray.get(0).getAsJsonObject();
				PingPongData data = new PingPongData();

				data.setUserId(userId);
				data.setQuestion(modify_message);
				data.setAnswerMessage(obj.get("message").getAsString());
				data.setAnswerScore(obj.get("model_score").getAsDouble());

			   int result = personal.insertPingPongMessage(data);

			   if(result > 0 ){
				   resultMessage.setCode(0);
				   resultMessage.setMsg("Success");
				   json.put("message", data.getAnswerMessage());
				   resultMessage.setData(json.toJSONString());
			   }else{
                   resultMessage.setCode(0);
                   resultMessage.setMsg("1일 제한량 2,000회를 초과하였습니다.");
				   json.put("message",  "1일 제한량 2,000회를 초과하였습니다");
				   resultMessage.setData(json.toJSONString());
			   }
			}

		}catch(Exception e) {
			resultMessage.setCode(0);
			resultMessage.setMsg("1일 제한량 2,000회를 초과하였습니다.");
			json.put("message", "1일 제한량 2,000회를 초과하였습니다");
			resultMessage.setData(json.toJSONString());
		}
		return resultMessage;
	}
    
//	@RequestMapping(value="/updateAlarm", method = {RequestMethod.POST})
//	public BaseResponse<?> getChatbotList(@RequestParam(name="userId", required=true)String userId
//			,@RequestParam(name="type", required=true)String type
//			,@RequestParam(name="showYn", required=true, defaultValue="Y")String showYn
//			,HttpServletRequest request) throws Exception{
//
//		logger.info("updateAlarm/{},{},{}",userId, type, showYn);
//		BaseResponse<String> resp = null;
//		try {
//			long nRet = personal.updateAlaram(userId, type, "Y".equals(showYn));
//			resp = new BaseResponse<>();
//			if(nRet < 1) {
//				resp.setCode(ERR_CODE_FAILURE);
//				resp.setMsg(ERR_MSG_FAILURE);
//			}
//		}catch(Exception e) {
//			throw new BizChatbotException(ERR_CODE_SQL_EXCEPTION, ERR_MSG_SQL_EXCEPTION);
//		}
//		return resp;
//	}
//
//	@RequestMapping(value="/updateBotOrder", method = {RequestMethod.POST})
//	public BaseResponse<?> getChatbotList(@RequestParam(name="userId", required=true)String userId
//			,@RequestParam(name="botorder", required=true)String botorder
//			,HttpServletRequest request) throws Exception{
//
//		logger.info("updateBotOrder/{},{}",userId, botorder);
//		BaseResponse<String> resp = null;
//		try {
//			String bots[] = botorder.split(BOT_SEPERATOR);
//			if(bots.length < BOT_COUNT) {
//				throw new BizChatbotException(ERR_CODE_PARAMS_INVALID, "Invalid botorder." + botorder);
//			}
//			List<String> botList = Arrays.asList(bots);
//			botList = botList.stream()
//					.distinct()
//					.filter(item -> item.matches(BOT_VALIDATION))
//					.collect(Collectors.toList());
//			if(botList.size() < BOT_COUNT) {
//				throw new BizChatbotException(ERR_CODE_PARAMS_INVALID, "Invalid botorder." + botorder);
//			}
//			long nRet = personal.updateBotOrder(userId, botList);
//			resp = new BaseResponse<>();
//			if(nRet < 1) {
//				resp.setCode(ERR_CODE_FAILURE);
//				resp.setMsg(ERR_MSG_FAILURE);
//			}
//		}catch(BizChatbotException e) {
//			throw e;
//		}catch(Exception e) {
//			throw new BizChatbotException(ERR_CODE_SQL_EXCEPTION, ERR_MSG_SQL_EXCEPTION);
//		}
//		return resp;
//	}
}
