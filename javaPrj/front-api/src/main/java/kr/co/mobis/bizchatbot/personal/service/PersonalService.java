package kr.co.mobis.bizchatbot.personal.service;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import ai.mindslab.bizchatbot.commons.utils.HttpConnectionManager;
import com.fasterxml.jackson.databind.JsonNode;
import kr.co.mobis.bizchatbot.personal.dao.*;
import kr.co.mobis.bizchatbot.personal.dao.data.*;
import org.apache.http.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ai.mindslab.bizchatbot.commons.codes.IChatbotConst;
import kr.co.mobis.bizchatbot.IMobisConsts;
import kr.co.mobis.bizchatbot.personal.controller.data.UserSettingsAll;

@Service
public class PersonalService implements IChatbotConst, IMobisConsts {

    private Logger logger = LoggerFactory.getLogger(PersonalService.class);

    @Autowired
    private UserSettingMapper settings;

    @Autowired
    private UserInfoMapper user;

    @Autowired
    private UserBookmarkMapper userBookmark;
    
    @Autowired
    private UserCommentMapper userComment;

    @Autowired
    PingPongDao pingPongDao;
    
    @Transactional
    public UserSettingsAll getUserSettings(String userId) throws Exception {
        logger.info("getUserSettings/START/{}", userId);

        try {
            List<UserSettingItem> list = settings.getUserSetting(userId);
            UserSettingsAll all = new UserSettingsAll();
            if (list == null || list.size() == 0) {
                insertInitUserSettings(userId);
                list = settings.getUserSetting(userId);
            }
            List<UserSettingsAll.SettingInfo> alarmsOut = list.stream()
                    .filter(item -> item.getSettingType().startsWith(ALARAM_TYPE))
                    .map(item -> new UserSettingsAll.SettingInfo(item.getSettingType(), item.getSettingValue()))
                    .collect(Collectors.toList());
            List<UserSettingsAll.SettingInfo> botsOut = list.stream().filter(item -> item.getSettingType().startsWith(BOT_TYPE))
                    .map(item -> new UserSettingsAll.SettingInfo(item.getSettingType(), item.getSettingValue()))
                    .collect(Collectors.toList());
            all.setAlarms(alarmsOut);
            all.setBots(botsOut);
            return all;
        } catch (Exception e) {
            logger.warn("getUserSettings/{}", e.getMessage());
            throw e;
        } finally {
            logger.info("getUserSettings/END");
        }
    }

    @Transactional
    public List<UserSettingItem> getUserBotSettings(String userId) throws Exception {
        logger.info("getUserBotSettings/START/{}", userId);
        try {
            List<UserSettingItem> list = settings.getUserSetting(userId);
            List<UserSettingItem> listOut = null;
            if (list != null) {
                listOut = list.stream()
                        .filter(item -> item.getSettingType().startsWith(BOT_TYPE))
                        .collect(Collectors.toList());
            }
            return listOut;
        } catch (Exception e) {
            logger.warn("getUserBotSettings/{}", e.getMessage());
            throw e;
        } finally {
            logger.info("getUserBotSettings/END/");
        }
    }

    @Transactional
    public List<UserSettingItem> getUserAlarmSettings(String userId) throws Exception {
        logger.info("getUserAlarmSettings/START/{}", userId);

        try {
            List<UserSettingItem> list = settings.getUserSetting(userId);
            List<UserSettingItem> listOut = null;
            if (list != null) {
                listOut = list.stream()
                        .filter(item -> item.getSettingType().startsWith(ALARAM_TYPE))
                        .collect(Collectors.toList());
            }
            return listOut;
        } catch (Exception e) {
            logger.warn("getUserAlarmSettings/{}", e.getMessage());
            throw e;
        } finally {
            logger.info("getUserAlarmSettings/END/");
        }
    }

    @Transactional
    public long insertInitUserSettings(String userId) throws Exception {
        logger.info("insertInitUserSettings/START/{}", userId);
        long nRet = 0;
        try {
            nRet = settings.insertUserSetting(userId);
            logger.debug("inserted user setting count: {}", nRet);
            return nRet;
        } catch (Exception e) {
            logger.warn("insertInitUserSettings/{}", e.getMessage());
            throw e;
        } finally {
            logger.info("insertInitUserSettings/END/");
        }
    }

    @Transactional
    public long updateUserSetting(String userId, String settingType, String settingValue) throws Exception {

        logger.info("updateUserSetting/{},{},{}", userId, settingType, settingValue);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("settingType", settingType);
        params.put("settingValue", settingValue);
        params.put("userId", userId);
        long nRet = settings.updateUserSetting(params);
        logger.debug("updateUserSetting/{}", nRet);
        return nRet;
    }

    @Transactional
    public long updateSetting(String userId, String type, boolean fixed, boolean on) throws Exception {
        logger.info("updateSetting/START/{},{},{}, {}", userId, type, fixed, on);
        try {
            String settingValue = (fixed ? "Y" : "N") + "," + (on ? "Y" : "N");
            long nRet = updateUserSetting(userId, type, settingValue);
            return nRet;
        } catch (Exception e) {
            logger.warn("updateSetting/{}", e.getMessage());
            throw e;
        } finally {
            logger.info("updateSetting/END");
        }
    }

    @Transactional
    public long updateAllSettings(String userId, JsonNode settingNode) throws Exception {
        logger.info("updateAllSettings/START/{},{}", userId, settingNode);
        try {
            long updateCount = 0L;
            for (JsonNode alarmsNode : settingNode.get("alarms")) {
                String type = alarmsNode.get("type").asText();
                String settingValue = alarmsNode.get("fixedYn").asText() + "," + alarmsNode.get("showYn").asText();
                updateUserSetting(userId, type, settingValue);
                updateCount++;
            }
            for (JsonNode botsNode : settingNode.get("bots")) {
                String type = botsNode.get("type").asText();
                String settingValue = botsNode.get("fixedYn").asText() + "," + botsNode.get("showYn").asText();
                updateUserSetting(userId, type, settingValue);
                updateCount++;
            }
            return updateCount;
        } catch (Exception e) {
            logger.warn("updateAllSettings/{}", e.getMessage());
            throw e;
        } finally {
            logger.info("updateAllSettings/END");
        }
    }

    @Transactional
    public List<UserBookmark> getUserBookmark(String userId, int cnt)  throws Exception {
        logger.info("getUserBookmark/START/{}", userId);
        try {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("userId", userId);
            map.put("cnt", cnt);
            
            List<UserBookmark> list = userBookmark.list(map);
            return list;
        }catch(Exception e) {
            logger.warn("getUserBookmark/{}", e.getMessage());
            throw e;
        } finally {
            logger.info("getUserBookmark/END/{}", userId);
        }
    }

    @Transactional
    public long insertUserBookmark(UserBookmark ub)  throws Exception {
        logger.info("insertUserBookmark/START/{}", ub);
        try {
            long nRet = userBookmark.insert(ub);
            return nRet;
        }catch(Exception e) {
            logger.warn("insertUserBookmark/{}", e.getMessage());
            throw e;
        } finally {
            logger.info("insertUserBookmark/END/{}", ub);
        }
    }   
 
    @Transactional
    public long deleteUserBookmark(UserBookmark ub)  throws Exception {
        logger.info("deleteUserBookmark/START/{}", ub);
        try {
            long nRet = userBookmark.delete(ub);
            return nRet;
        }catch(Exception e) {
            logger.warn("deleteUserBookmark/{}", e.getMessage());
            throw e;
        } finally {
            logger.info("deleteUserBookmark/END/{}", ub);
        }
    } 
    
    @Transactional
    public long insertUserComment(UserComment uc) throws Exception {
        logger.info("insertUserComment/START/{}", uc);
        try {
            long nRet = userComment.insert(uc);
            return nRet;
        }catch(Exception e) {
            logger.warn("insertUserComment/{}", e.getMessage());
            throw e;
        } finally {
            logger.info("insertUserComment/END/{}", uc);
        }    
    }
    
//	@Transactional
//	public long updateBotOrder(String userId, List<String> botList) throws Exception{
//		if(botList == null || botList.size() != BOT_COUNT) {
//			logger.warn("Invalid BOT INFO. " + userId);
//			return 0;
//		}
//		String strBotList = botList.stream().collect(Collectors.joining(";"));
//		logger.info("updateBotOrder/START/{},{}",userId, strBotList);
//
//		try {
//			HashMap<String,String> params = new HashMap<String,String>();
//			params.put("userId", userId);
//			params.put("settingType", BOT_ORDER);
//			params.put("settingValue", strBotList);
//			long nRet = settings.updateUserSetting(params);
//			return nRet;
//		}catch(Exception e) {
//			logger.warn("updateBotOrder/{}", e.getMessage());
//			throw e;
//		}finally {
//			logger.info("updateBotOrder/END");
//		}
//	}

    public UserInfoSimple getUserInfo(String userId) throws Exception {
        logger.info("getUserInfo/{}", userId);
        UserInfoSimple userInfo = null;
        userInfo = user.getUserById(userId);
        if (userInfo == null) {
            logger.info("user {} is not exist.", userId);
        }
        return userInfo;
    }

    public String sendPingPongMessage(String url, String message) throws Exception {
        String resp = "";
        Header[] headers = null;
        try {
            url = url.replace("_message_", message);
            resp = HttpConnectionManager.getInstance().get(url, headers);

            logger.debug("responsce pingpong/{}",resp);
        }catch(Exception e) {
            logger.debug("error pingpong/{}",resp);
            //resp = "정보조회 오류입니다.";
            throw new Exception("정보조회 오류입니다.");
        }
        return resp;
    }

    public int insertPingPongMessage(PingPongData vo) throws Exception{
        return pingPongDao.insertPingPongMessage(vo);
    }
}
