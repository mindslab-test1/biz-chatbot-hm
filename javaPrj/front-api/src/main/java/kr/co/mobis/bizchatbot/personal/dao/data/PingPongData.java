package kr.co.mobis.bizchatbot.personal.dao.data;

public class PingPongData {

    private String userId;
    private String question;
    private Double answerScore;
    private String answerMessage;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Double getAnswerScore() {
        return answerScore;
    }

    public void setAnswerScore(Double answerScore) {
        this.answerScore = answerScore;
    }

    public String getAnswerMessage() {
        return answerMessage;
    }

    public void setAnswerMessage(String answerMessage) {
        this.answerMessage = answerMessage;
    }
}
