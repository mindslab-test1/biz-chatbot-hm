package kr.co.mobis.bizchatbot.personal.dao;

import kr.co.mobis.bizchatbot.personal.dao.data.PingPongData;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PingPongDao {
    int insertPingPongMessage(PingPongData vo) throws Exception;
}
