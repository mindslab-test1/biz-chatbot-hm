#!/usr/bin/bash

PYTHON=python3.6
SERVER=~/maum-chatbot/nlp/src/server.py
PID=`ps -ef | grep $SERVER | grep -v grep | awk '{print $2}'`
if [[ "" !=  "$PID" ]]; then
  echo "Server is already Running"
  kill -9 $PID
  echo "Restarting Server"
else
  echo "Starting Server"
fi
nohup $PYTHON $SERVER > /dev/null 2>&1 &
