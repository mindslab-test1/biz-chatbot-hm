#!/usr/bin/bash

SERVER=~/maum-chatbot/nlp/src/server.py
PID=`ps -ef | grep $SERVER | grep -v grep | awk '{print $2}'`
if [[ "" !=  "$PID" ]]; then
  echo "killing $PID"
  kill -9 $PID
else
  echo "Server Not Running"
fi

