#!/usr/bin/bash

PYTHON=python3.6
SERVER=~/maum-chatbot/nlp/src/server.py
RELOAD=~/maum-chatbot/nlp/src/dic_reload_client.py
PID=`ps -ef | grep $SERVER | grep -v grep | awk '{print $2}'`
if [[ "" !=  "$PID" ]]; then
  echo "Server is Running, call Reload"
  $PYTHON $RELOAD
else
  echo "Server is NOT Running, call Reload FAILED"
fi

