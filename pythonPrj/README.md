MeCab-nlp grpc python 코드
===================================

## Requirements

1.python3.6 설치 필요  
2.pip 설치 필요

3.grpcio 파이썬버전 설치 필요 (pip 사용해서 설치)  
4.protobuf 파이썬버전 설치 필요 (기본적으로 grpcio 설치시 같이 설치됨)  

    % python -m pip install grpcio
    % python -m pip install protobuf

>참고 URL : https://grpc.io/docs/quickstart/python.html

5.mecab-ko, mecab-ko-dic 설치 필요
>https://bitbucket.org/eunjeon/mecab-ko  
>https://bitbucket.org/eunjeon/mecab-ko-dic

## Usage

python3.6으로 server.py 실행  
기본 포트 9811로 고정

## Test

client_test.py 참조
