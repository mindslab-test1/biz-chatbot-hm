# -*- coding: utf-8 -*-
import time

from google.protobuf import empty_pb2
import grpc

from maum.brain.nlp import nlp_pb2
from maum.brain.nlp import nlp_pb2_grpc
from maum.brain.nlp import nlp_operation_pb2_grpc

# number of multiplying messages
_MESSAGE_REPEAT_NUM = 200

# number of calling stub
_ROUTINE_NUM = 1


def print_res(document):
    for sentence in document.sentences:
        print('sentence '+sentence.text)
        for word in sentence.words:
            print('word '+str(word.text))
        for morp in sentence.morps:
            print('morp '+str(morp.lemma))
        for nes in sentence.nes:
            print('nes ' + str(nes.text))
        for chunk in sentence.chunks:
            print('chunk '+str(chunk.text))

    for keyword_frequency in document.keyword_frequencies:
        print('keyword '+keyword_frequency.keyword)

    print("EOL")
    print(document)

    if not document:
        print("Server returned incomplete feature")
        return


def req_analyze(stub, text):
    input_text = make_input_text(text)

    document = stub.Analyze(input_text)
    print_res(document)


def analyze(stub):
    text = "헤어질 시간이야"
    req_analyze(stub, text)


def make_input_text(text):
    return nlp_pb2.InputText(text=text)


def generate_messages():
    messages = [
        make_input_text("시작 메세지니까 가볍게"),
        make_input_text("동물 좋아하는거 뭐있어. 안녕하세요"),
        make_input_text("세종대왕은 누구야?"),
        make_input_text("거의 다 왔다"),
        make_input_text("마지막 문장은 마지막 문장답게"),
    ]

    total_messages = []
    for i in range(0, _MESSAGE_REPEAT_NUM):
        total_messages.extend(messages)

    return total_messages


def yield_messages(messages):
    for msg in messages:
        yield msg


def analyze_multiple(stub):
    responses = stub.AnalyzeMultiple(yield_messages(generate_messages()))
    for response in responses:
        print_res(response)


def perform_test(stub):

    total_messages = generate_messages()
    success_cnt = 0

    analyze_start_time = time.time()
    for i in range(0, _ROUTINE_NUM):
        responses = stub.AnalyzeMultiple(yield_messages(total_messages))
        for response in responses:
            # print_res(response)
            if response:
                success_cnt += 1
    analyze_end_time = time.time()
     
    total_cnt = len(total_messages) * _ROUTINE_NUM
    print("TOTAL COUNT: " + str(total_cnt))
    print("SUCCESS COUNT: " + str(success_cnt))

    analyze_time = analyze_end_time - analyze_start_time
    print("테스트 시간: " + str(analyze_time))

def dic_reload(stub):
    text = stub.reload(empty_pb2.Empty())
    print(text)

def run():
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    with grpc.insecure_channel('localhost:9811') as channel:
        nlp_stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(channel)
        # print("-------------- Analyze --------------")
        # analyze(stub)
        # print("-------------- Analyze AnalyzeMultiple--------------")
        # analyze_multiple(stub)
        # print("-------------- Performance Test --------------")
        # perform_test(stub)

        dic_stub = nlp_operation_pb2_grpc.DictionaryReloadServiceStub(channel)
        print("-------------- DIC_RELOAD --------------")
        dic_reload(dic_stub)

if __name__ == '__main__':
    run()
