# -*- coding: utf-8 -*-

import logger

from maum.brain.nlp import nlp_pb2
from maum.brain.nlp import nlp_operation_pb2_grpc
import nlp_analyzer

LOGGER = logger.get_logger('dic_reload')


def reload_mecab_tagger():

    result = nlp_analyzer.load_tagger()
    if result:
        text = "DIC Loading SUCCESS"
    else:
        text = "DIC Loading FAILED"    

    LOGGER.info(text)
    res = nlp_pb2.Text()
    res.text = text

    return res


class DictionaryReloadServiceServicer(nlp_operation_pb2_grpc.DictionaryReloadServiceServicer):

    def reload(self, request, context):
        return reload_mecab_tagger()
