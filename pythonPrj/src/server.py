# -*- coding: utf-8 -*-

from concurrent import futures
import time
import grpc

import logger
from maum.brain.nlp import nlp_pb2_grpc
from maum.brain.nlp import nlp_operation_pb2_grpc
from nlp_analyzer import NaturalLanguageProcessingServiceServicer
from dic_reload import DictionaryReloadServiceServicer


_ONE_DAY_IN_SECONDS = 60 * 60 * 24
LOGGER = logger.get_logger('server')


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    nlp_pb2_grpc.add_NaturalLanguageProcessingServiceServicer_to_server(
        NaturalLanguageProcessingServiceServicer(), server)

    nlp_operation_pb2_grpc.add_DictionaryReloadServiceServicer_to_server(
        DictionaryReloadServiceServicer(), server)

    # server.add_insecure_port('[::]:50051')
    server.add_insecure_port('[::]:9811')
    server.start()
    LOGGER.info("SERVER STARTED")
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
