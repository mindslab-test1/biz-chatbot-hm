# -*- coding: utf-8 -*-

import re
import os

import logger
from maum.brain.nlp import nlp_pb2
from maum.brain.nlp import nlp_pb2_grpc
import MeCab

_MECAB_TAGS_ARR = ['NNG', 'NNP', 'NNB', 'NNBC', 'NR', 'NP', 'VV', 'VA', 'VX', 'VCP', 'VCN', 'MM', 'MAG', 'MAJ', 'IC'
                    , 'JKS', 'JKC', 'JKG', 'JKO', 'JKB', 'JKV', 'JKQ', 'JX', 'JC'
                    , 'EP', 'EF', 'EC', 'ETN', 'ETM', 'XPN', 'XSN', 'XSV', 'XSA'
                    , 'XR', 'SF', 'SE', 'SSO', 'SSC', 'SC', 'SY', 'SL', 'SH', 'SN', 'UNKNOWN'
                 ]
_NLP_TAGS_ARR = ['NC', 'NR', 'NB', 'NB', 'NN', 'NP', 'PV', 'PA', 'PX', 'CO', 'CO', 'MM', 'MAG', 'MAJ', 'II'
                    , 'JC', 'JC', 'JM', 'JC', 'JC', 'JC', 'JC', 'JX', 'JJ'
                    , 'EP', 'EF', 'EC', 'ETN', 'ETM', 'XP', 'XSN', 'XSV', 'XSM'
                    , 'UK', 'S', 'S', 'S', 'S', 'S', 'S', 'NC', 'S', 'S', 'UK'
                 ]
LOGGER = logger.get_logger('nlp_analyzer')


# def replace_word(word):
#     new_word = word
#     if '"' in word:
#         new_word = word.replace('"', '')
#     return new_word


def switch_tag(mecab_morp_type, analyze_level):
    nlp_morp_type = ''
    if '+' not in mecab_morp_type:
        index = _MECAB_TAGS_ARR.index(mecab_morp_type)
        nlp_morp_type += _NLP_TAGS_ARR[index]
    else:
        mecab_morp_type = mecab_morp_type.split('+')

        # analyze level ALL 시에만 복합형태소 반환
        if analyze_level == nlp_pb2.NLP_ANALYSIS_ALL:
            for i in range(len(mecab_morp_type)):
                if i != 0:
                    nlp_morp_type += '+'
                index = _MECAB_TAGS_ARR.index(mecab_morp_type[i])
                nlp_morp_type += _NLP_TAGS_ARR[index]
        else:
            # analyze_level == nlp_pb2.NLP_ANALYSIS_MORPHEME
            # 복합 형태소의 첫 형태소만 변경 후 반환
            index = _MECAB_TAGS_ARR.index(mecab_morp_type[0])
            nlp_morp_type += _NLP_TAGS_ARR[index]

    return nlp_morp_type.lower()


def get_analyze(request):

    full_text = str(request.text)
    analyze_level = request.level
    sentence_texts = re.split("\. |\! |\? |\.|\!|\?", full_text)

    document = nlp_pb2.Document()
    try:
        for i in range(len(sentence_texts)):
            sentence = document.sentences.add()
            sentence.seq = i
            # 영문자는 전부 대문자로 변환
            # 기존 시스템에서 전부 대문자로 해서 compile함
            sentence.text = sentence_texts[i].upper()

            m = tagger.parseToNode(sentence.text)

            j = 0
            while m:
                arr = m.feature.split(",")

                morp_type = arr[0]
                word = m.surface

                if 'BOS/EOS' not in morp_type:
                    morpheme = sentence.morps.add()
                    morpheme.seq = j
                    morpheme.lemma = word
                    # morpheme.lemma = replace_word(word)
                    # morpheme.type = morp_type
                    morpheme.type = switch_tag(morp_type, analyze_level)
                    morpheme.position = j
                    j = j + 1

                m = m.next
        LOGGER.info("SUCCESS " + full_text)

    except Exception as e:
        LOGGER.error("Analyze FAILED")
        LOGGER.error(str(e))

    return document


def load_tagger():
    global tagger

    dic_path = '-d /usr/local/lib/mecab/dic/mecab-ko-dic'
    if os.name == 'nt':
        dic_path = ''

    try:
        result = True
        tagger = MeCab.Tagger(dic_path)

        # 최초 로드시에 0xff Decode 에러 방지하기 위해서
        tagger.parseToNode('')
        LOGGER.info("Mecab Tagger loading SUCCESS")
    except Exception as e:
        result = False
        LOGGER.error("Mecab Tagger loading FAILED")
        LOGGER.error(str(e))

    return result


class NaturalLanguageProcessingServiceServicer(nlp_pb2_grpc.NaturalLanguageProcessingServiceServicer):

    def __init__(self):
        load_tagger()
        return

    def Analyze(self, request, context):
        return get_analyze(request)

    def AnalyzeMultiple(self, request_iterator, context):
        for request in request_iterator:
            yield get_analyze(request)
