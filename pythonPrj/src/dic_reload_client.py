# -*- coding: utf-8 -*-
from google.protobuf import empty_pb2
import grpc

from maum.brain.nlp import nlp_operation_pb2_grpc


def dic_reload(stub):
    text = stub.reload(empty_pb2.Empty())
    print(text)

def run():
    with grpc.insecure_channel('localhost:9811') as channel:
        dic_stub = nlp_operation_pb2_grpc.DictionaryReloadServiceStub(channel)
        print("-------------- DIC_RELOAD --------------")
        dic_reload(dic_stub)

if __name__ == '__main__':
    run()
