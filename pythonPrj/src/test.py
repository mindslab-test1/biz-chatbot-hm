#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import MeCab

sentence = "동물 좋아하는거 뭐있어"

try:
    # print(MeCab.VERSION)
    dic_path = '-d /usr/local/lib/mecab/dic/mecab-ko-dic'
    if os.name == 'nt':
        dic_path = ''

    t = MeCab.Tagger(dic_path)
    # t = MeCab.Tagger()
    # print(t.parse(sentence))

    m = t.parseToNode(sentence)
    while m:
        print(m.surface, "\t", m.feature)
        m = m.next
    print("EOS")

    # lattice = MeCab.Lattice()
    # lattice.set_sentence(sentence)
    # t.parse(lattice)
    # len = lattice.size()
    # for i in range(len + 1):
    #     b = lattice.begin_nodes(i)
    #     e = lattice.end_nodes(i)
    #     while b:
    #         print("B[%d] %s\t%s" % (i, b.surface, b.feature))
    #         b = b.bnext
    #     while e:
    #         print("E[%d] %s\t%s" % (i, e.surface, e.feature))
    #         e = e.bnext
    # print("EOS")

    # d = t.dictionary_info()
    # while d:
    #     print("filename: %s" % d.filename)
    #     print("charset: %s" %  d.charset)
    #     print("size: %d" %  d.size)
    #     print("type: %d" %  d.type)
    #     print("lsize: %d" %  d.lsize)
    #     print("rsize: %d" %  d.rsize)
    #     print("version: %d" %  d.version)
    #     d = d.next

except RuntimeError as e:
    print("RuntimeError:", e)
