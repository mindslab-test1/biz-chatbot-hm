# -*- coding: utf-8 -*-

import os
import logging
import logging.handlers

PROJECT_HOME = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
LOG_FILE = os.path.join(PROJECT_HOME, 'logs', 'mecab_nlp.log')
LOG_LEVEL = "INFO"


def get_logger(name):
    handler = logging.handlers.WatchedFileHandler(os.environ.get("LOGFILE", LOG_FILE))
    formatter = logging.Formatter('[%(levelname)s] %(asctime)s [%(threadName)s] %(name)s - %(message)s')
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(os.environ.get("LOGLEVEL", LOG_LEVEL))
    logger.addHandler(handler)

    return logger
