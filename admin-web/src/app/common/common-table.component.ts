import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../environments/environment';
import { MenuItem } from 'primeng/primeng';
import { CommonUtilService } from '../service';
//import { TemplateService } from '../service/index';

const _ = require('lodash');
interface OptionList {
  name: string;
  code: string;
}
declare var $: any;
@Component({
  selector: 'common-table',
  templateUrl: './common-table.component.html'
})

export class CommonTableComponent implements OnInit {
  public _environment = environment;
  public _option: any = {
    limit: _.cloneDeep(this._environment.table.LIMIT)
  };
  public items: MenuItem[];
  public optionList: OptionList [];
  public selectLists: OptionList;
  public optionList2: OptionList [];
  public selectLists2: OptionList;
  public Math = Math;
  public totalItems = 100;
  public examples: any;
  public currentPage = 1;


  constructor(
    private cm: CommonUtilService
  ) {
    // this.optionList = [
    //   {name: 'select', code: ''},
    //   {name: 'option1', code: 'op1'},
    //   {name: 'option2', code: 'op2'},
    //   {name: 'option3', code: 'op3'},
    //   {name: 'option4', code: 'op4'},
    //   {name: 'option5', code: 'op5'},
    //   {name: 'option6', code: 'op6'},
    //   {name: 'option7', code: 'op7'},
    //   {name: 'option8', code: 'op8'},
    //   {name: 'option9', code: 'op9'}
    // ];
    // this.optionList2 = [
    //   {name: '숫자', code: ''},
    //   {name: '5', code: '5'},
    //   {name: '10', code: '10'},
    //   {name: '15', code: '15'},
    //   {name: '20', code: '20'},
    //   {name: '30', code: '30'},
    //   {name: '50', code: '50'},
    //   {name: '100', code: '100'}
    // ];
  }
  private _defaultStyle = {};
  _style = this._defaultStyle;
  get style() {
    return this._style;
  }
  @Input() set style(style) {
    this._style = this.cm.prop(this._style,style);
  }

  private _defaultBodyStyle = {};
  _bodyStyle = this._defaultBodyStyle;
  get bodyStyle() {
    return this._bodyStyle;
  }
  @Input() set bodyStyle(bodyStyle) {
    this._bodyStyle = this.cm.prop(this._bodyStyle,bodyStyle);
  }
  

  ngOnInit() {
    // this.items = [
    //   { label: 'tap1' },
    //   { label: 'tap2' },
    //   { label: 'tap3' },
    //   { label: 'tap4' },
    //   { label: 'tap5' }
    // ];
    // this.templateService.getExampleData().subscribe(examples => {
    //   this.examples = examples.data;
    // });
  }

  public pageChanged(event, page): boolean {
    if ($(event.currentTarget).hasClass('disabled')) {
      return false;
    }
    this.currentPage = page;
    return true;
  }

}
