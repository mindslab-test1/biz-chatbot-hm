import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Option } from '../../model/dom-models';

@Component({
  selector: 'app-page-size-sellector',
  templateUrl: './page-size-sellector.component.html',
  styleUrls: ['./page-size-sellector.component.scss']
})
export class PageSizeSellectorComponent implements OnInit {

  constructor(
  ) { }

  ngOnInit() {
    this.initOptions();
  }
  @Output() pageSize = new EventEmitter(false);
  DEFAULT_PAGE_SIZE = 10;
  options: Option [];
  selectOption: string;
  initOptions(){
    this.options = [
      { label: '5', value: '5' },
      { label: '10', value: '10' },
      { label: '15', value: '15' },
      { label: '20', value: '20' },
      { label: '30', value: '30' },
      { label: '50', value: '50' },
      { label: '100', value: '100' }
    ];
    this.selectOption = this.options.find(option=>option.value==this.DEFAULT_PAGE_SIZE.toString()).value;
    this.notify();
  }
  onOptionListSelect(value) {
    // console.log('value',value);
    // this.selectOption = value;
    this.notify();
  }
  notify() {
    const pageSize = this.selectOption;
    this.pageSize.emit({selectOption: pageSize});
  }

}
