import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { CommonUtilService } from '../../service';

/**
 * TODO paginatorComponent 에서 Paginator 클래스 분리하기.
 */
export interface PaginatorInterface {
  currentPage?: number;
  pageIndex?: number;
  totalCount?: number;
  totalPages?: number;
  pageSize?: number;
}

export class Paginator implements PaginatorInterface {
  
  DEFAULT_CURR_PAGE = 1;
  DEFAULT_PAGE_SIZE = 10;
  DEFAULT_TOTAL_COUNT = 0;

  currentPage: number;
  pageIndex: number;
  totalCount: number;
  totalPages: number;
  pageSize: number;

  constructor() {
    this.currentPage = this.DEFAULT_CURR_PAGE;
    this.pageIndex = this.DEFAULT_CURR_PAGE - 1;
    this.totalCount = this.DEFAULT_TOTAL_COUNT;
    this.totalPages = 1;
    this.pageSize = this.DEFAULT_PAGE_SIZE;
  }
  getTotalPage = () => this.totalCount == 0 ? 1 : Math.ceil(this.totalCount/this.pageSize);
  getCurrPage = () => this.currentPage > this.getTotalPage() ? this.DEFAULT_CURR_PAGE : this.currentPage;
}
@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {

  constructor(
    private cm: CommonUtilService
  ) { }

  ngOnInit() {
    this.initPaginator();
  }
  initPaginator() {
    // this.cm.isDev() && console.log('paginator init');
    this.page.emit({paginator:this._paginator});
  }

  @Output() page = new EventEmitter(false);

  _paginator = new Paginator();
  _currPage = this._paginator.currentPage;
  _totalCount = this._paginator.totalCount;
  _pageSize = this._paginator.pageSize;

  // private _getTotalPage = () => Math.ceil(this._totalCount/this._pageSize);
  // private _getCurrPage = () => this._currPage > this._getTotalPage() ? this._paginator.DEFAULT_CURR_PAGE : this._currPage;


  get currPage() {
    return this._currPage;
  }
  @Input() set currPage(currPage){
    const tempCurrPage = this._paginator.currentPage;
    if (tempCurrPage != currPage) {

      //set
      this._currPage = currPage;
      this._paginator.currentPage = currPage;
      this._paginator.pageIndex = currPage - 1;

      //notify
      const paginator: PaginatorInterface = {};
      paginator.currentPage = this._paginator.currentPage;
      paginator.pageIndex = this._paginator.pageIndex;
      this.page.emit({paginator});
    }
  }

  get pageSize() {
    return this._pageSize;
  }
  @Input() set pageSize(pageSize){
    const tempPageSize = this._paginator.pageSize;
    if (tempPageSize != pageSize) {

      // set
      this._pageSize = pageSize;
      this._paginator.pageSize = pageSize;
  
      // set & notify
      this.setAndNotifyTotalPage();
    }
  }


  _totalCountChange = false;
  get totalCount() {
    return this._totalCount;
  }
  @Input() set totalCount(totalCount) {
    const tempTotalCount = this._paginator.totalCount;
    if (tempTotalCount != totalCount) {

      //set
      this._totalCount = totalCount;
      this._paginator.totalCount = totalCount;
      this._totalCountChange = true;
  
      // set & notify
      this.setAndNotifyTotalPage();
    }
  }

  private _defaultStyle = {};
  _style = this._defaultStyle;
  get style() {
    return this._style;
  }
  @Input() set style(style) {
    this._style = this.cm.prop(this._style,style);
  }

  setAndNotifyTotalPage(){
    //set
    const tempTotalPage = this._paginator.totalPages;
    const _totalPage = this._paginator.getTotalPage();
    if (tempTotalPage != _totalPage) {
      this._paginator.totalPages = _totalPage;
    }

    const tempCurrPage = this._paginator.currentPage;
    const _currPage = this._paginator.getCurrPage();
    if (tempCurrPage != _currPage) {
      this._currPage = _currPage;
      this._paginator.currentPage = _currPage;
      this._paginator.pageIndex = this._currPage - 1;
    }

    //notify
    // const paginator: PaginatorInterface = this._paginator;
    const paginator: PaginatorInterface = {};
    if (tempTotalPage != _totalPage) {
      paginator.totalPages = this._paginator.totalPages;
    }
    if (tempCurrPage != _currPage) {
      paginator.currentPage = this._paginator.currentPage;
      paginator.pageIndex = this._paginator.pageIndex;
    }
    if (this._totalCountChange) {
      paginator.totalCount = this._paginator.totalCount;
      this._totalCountChange = false;
    }
    if(this.cm.isNotEmpty(paginator)){
      this.page.emit({paginator});
    }
  }

  get paginator(){
    return this._paginator;
  }


  setCurrPage(currPage = this._paginator.DEFAULT_CURR_PAGE){
    this.currPage = currPage;
  }
  onPageChanged(event, page): boolean {
    // console.log('debug pageChange event.currentTarget',event.currentTarget);
    if(event.currentTarget.className.split(" ").includes('disabled')){
      return false;
    }
    // console.log('debug paginator emit change', event, page);
    this.setCurrPage(page);


    return true;
  }
}
