import {Component, ElementRef, OnInit, ViewChild, AfterViewChecked, Input} from '@angular/core';
import { CommonUtilService } from '../service';

// declare const $: any;
@Component({
  selector: 'common-modal',
  templateUrl: './common-modal.component.html'
})
export class CommonModalComponent implements OnInit, AfterViewChecked {
  public dummy: any = null;
  constructor(
    private elementRef: ElementRef,
    private cm: CommonUtilService
  ) {
  }
  ngOnInit() {
    
  }
  ngAfterViewChecked() {
    this.popupCenter();
  }
  @ViewChild('commonModalContainer')
  commonModalContainer: ElementRef;
  @ViewChild('commonModalSection')
  commonModalSection: ElementRef;
  
  public confirmCallback: any = null;
  private callback(event, ...args) {
    // console.log('dev test : event =>',event,...args);
    this.confirmCallback && this.confirmCallback(event,...args);
  }
  isOpen:boolean = false;
  public modalOpen() {
    // $(this.elementRef.nativeElement.querySelector('div')).addClass('modal-section');
    // this.openCommonModal();
    this.isOpen = true;
  }
  public modalClose() {
    // $(this.elementRef.nativeElement.querySelector('div')).removeClass('modal-section');
    // this.closeCommonModal();
    this.isOpen = false;
  }

  isChange:boolean = false;
  public popupCenter() {
    // const modalHeight = $('.modal-section .common-modal-container').height() + 30;
    // const modalMargin = - ( modalHeight / 2 );
    // $('.common-modal-container').css({
    //   'top': '50%',
    //   'marginTop': modalMargin + 'px'
    // });
    // $('.modal-section').css('min-height', modalHeight);
    
    const isClose = this.commonModalSection.nativeElement.style.display == 'none';
    if(isClose){
      this.isChange = false;
      return false;
    }
    if(!this.isChange) {
      const modalHeight = (this.isOpen ? this.commonModalContainer.nativeElement.offsetHeight : 0) + 30;
      const modalMargin = - ( modalHeight / 2 );
      if(modalHeight != 30) {
        this.isChange = true;
        this.cm.prop(
            this.commonModalContainer.nativeElement.style
            , {
              top: '50%',
              marginTop: modalMargin + 'px'
            }
        );
        this.cm.prop(
            this.commonModalSection.nativeElement.style
            , {
              minHeight: modalHeight + 'px'
            }
        );
      }

      // console.log('debug commonModalSection',this.commonModalSection.nativeElement);
      // console.log('debug commonModalContainer',this.commonModalContainer.nativeElement);
      // console.log('debug commonModalContainer.offsetHeight',this.commonModalContainer.nativeElement.offsetHeight);
      // console.log('debug modalHeight',modalHeight);
    }
  }

  public closeAllModals() {
    // $('.common-modal-section').removeClass('modal-section');
    // this.closeCommonModal();
    this.isOpen = false;
  }
  // DISPLAY_CLASS_NAME = 'modal-section';
  // private openCommonModal(){
  //   this.cm.addClass(this.commonModalSection, this.DISPLAY_CLASS_NAME);
  //   this.popupCenter();
  // }
  // private closeCommonModal(){
  //   this.cm.removeClass(this.commonModalSection, this.DISPLAY_CLASS_NAME);
  //   this.popupCenter();
  // }

  private _defaultSectionStyle = {};
  _sectionStyle = this._defaultSectionStyle;
  get sectionStyle() {
    return this._sectionStyle;
  }
  @Input() set sectionStyle(sectionStyle) {
    this._sectionStyle = this.cm.prop(this._sectionStyle,sectionStyle);
  }
  resetSectionStyle() {
    this._sectionStyle = this._defaultSectionStyle;
  }

  private _defaultStyle = {};
  _style = this._defaultStyle;
  get style() {
    return this._style;
  }
  @Input() set style(style) {
    this._style = this.cm.prop(this._style,style);
  }
  public setModalStyle(width?:number,height?:number,style?:object) {
    if(width) {
      this.commonModalContainer.nativeElement.style.width = `${width}px`;
    }
    if(height) {
      this.commonModalContainer.nativeElement.style.height = `${height}px`;
    }
    if(style) {
      this.cm.prop(this.commonModalContainer.nativeElement.style, style);
    }
  }
}
