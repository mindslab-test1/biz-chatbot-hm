import { Injectable, Inject } from '@angular/core';
import { ApiHelperService } from './api-helper.service';
import { Http, RequestOptionsArgs, Headers, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { CommonUtilService } from '.';
import { environment as env } from '../environments/environment';
import { BaseResponse } from '../model/base-response';
import { Observable } from 'rxjs';

@Injectable()
export class SdsApiHelperService extends ApiHelperService {

  constructor(
      @Inject(Http) protected http: Http
    , @Inject(HttpClient) protected httpClient: HttpClient
    , @Inject(CommonUtilService) protected cm: CommonUtilService
  ) { 
    super(http, httpClient, cm);
    this.url = env.sdsApiUrl;
  }

  api = env.SDS_API_PATH;
  
  postBody(path: string, body: string): Observable<any> {
    // this.cm.isDev() && console.log('debug ',`postBody path: ${path}, params: ${JSON.stringify(params)}, headers: ${JSON.stringify(headers)}`);
    const url = `${this.url}${path}`;
    const options: RequestOptionsArgs = new RequestOptions({
      headers: new Headers({})
    });
    console.log('debug temp',JSON.stringify(options));
    return this.http.post(url,body, options);
  }
  parseRes(res): BaseResponse {
    if(!res || !this.cm.isFunction(res.json)) {
      console.log('Failed to parse. Invalid res', res);
      return res;
    }
    const resp = res.json();
    return resp;

  }

}
