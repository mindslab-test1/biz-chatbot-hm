import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as go from 'gojs';
import { CommonUtilService } from './commonutil.service';

const XLSX = require('xlsx');

@Injectable()
export class UtilService {
  constructor(private cm: CommonUtilService) {
  }

  /*
  [입력 데이터 예시]
  sheetData ->
  [['header1', 'header2'], ['data1', 'data2'], ['data3', 'data4']]
  excel file ->
  header1 | header2
  data1   | data2
  data3   | data4
   */
  exportExcel(fileName: string, sheetData: any[][]) {
    /* generate worksheet */
    const ws = XLSX.utils.aoa_to_sheet(sheetData);

    /* generate workbook and add the worksheet */
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, fileName + '.xlsx');
  }
  exportExcelEx(fileName: string, totSheetData: any[][], sheetPerMax, maxSheetCount = 3) {
    this.cm.isDev() && console.log('exportExcelEx ... [s]');

    if(totSheetData.length <= sheetPerMax) {
      this.cm.isDev() && console.log('exportExcelEx ==> exportExcel');
      this.exportExcel(fileName, totSheetData);
      return;
    }
    /* generate workbook and add the worksheet */
    const wb = XLSX.utils.book_new();

    const getFileName = (_fileNumber, _fileName)=>(_fileNumber == 1 ? _fileName : `${_fileName} (${_fileNumber})`);

    const header = totSheetData.shift();
    const bodyMaxIdx = totSheetData.length-1;
    let fileNumber = 1;
    let sheetNumber = 1;
    totSheetData.reduce((sheetData: any[], v, i: number)=>{
      sheetData.push(v);
      if((i+1)%(sheetPerMax) == 0 || i == bodyMaxIdx) {
        sheetData.unshift(header);
        /* generate worksheet */
        const ws = XLSX.utils.aoa_to_sheet(sheetData);
        XLSX.utils.book_append_sheet(wb, ws, `Sheet${sheetNumber}`);
        sheetData = [];
        sheetNumber++;
      }
      if(sheetNumber == (maxSheetCount + 1) || i == bodyMaxIdx) {
        /* save to file */
        XLSX.writeFile(wb, getFileName(fileNumber, fileName) + '.xlsx');
        sheetNumber = 1;
        fileNumber++;
      }
      return sheetData;
    },[]);
    this.cm.isDev() && console.log('exportExcelEx ... [e]');

  }

  /*
  [사용 방법]
  html file ->
    <input type="file" (change)="onFileChange($event)" multiple="false" />
  ts file ->
    public onFileChange(event: any) {
      this.util.importExcel(event).subscribe(data => {
        console.log(data);
      });
    }
  [응답 결과 예시]
  2차원 배열로 excel 데이터 전달
  -> excel file
  header1 | header2
  data1   | data2
  data3   | data4
  -> response
  [['header1', 'header2'], ['data1', 'data2'], ['data3', 'data4']]
   */
  importExcel(event: any): Observable<any> {
    return Observable.create(observer => {
      /* wire up file reader */
      const target: DataTransfer = <DataTransfer>(event.target);
      const reader: FileReader = new FileReader();

        /* IE11 version */
        if (!FileReader.prototype.readAsBinaryString) {
          let binary = "";
          reader.onload = function (e) {
            const bytes = new Uint8Array(e.target.result);
            const length = bytes.byteLength;
            for (let i = 0; i < length; i++) {
              binary += String.fromCharCode(bytes[i]);
            }
            const wb = XLSX.read(binary, {type: 'binary'});

            /* grab first sheet */
            const wsname: string = wb.SheetNames[0];
            const ws = wb.Sheets[wsname];

            /* save data */
            observer.next(XLSX.utils.sheet_to_json(ws, {header: 1}));
            observer.complete();
          };
          reader.readAsArrayBuffer(target.files[0]);
        } else {
          /*Chrome version */
          reader.onload = (e: any) => {
            /* read workbook */
            const bstr: string = e.target.result;
            const wb = XLSX.read(bstr, {type: 'binary'});

            /* grab first sheet */
            const wsname: string = wb.SheetNames[0];
            const ws = wb.Sheets[wsname];

            /* save data */
            observer.next(XLSX.utils.sheet_to_json(ws, {header: 1}));
            observer.complete();
          };
          reader.readAsBinaryString(target.files[0]);
        }
    });
  }

  initGoJSDiagram() {
    function mouseEnter(e, obj) {
      const shape = obj.findObject('SHAPE');
      shape.fill = HighlightedNodeBG;
      const text = obj.findObject('TEXT');
      text.stroke = HighlightedTextColor;
    }

    function mouseLeave(e, obj) {
      if (obj.isSelected === true ) {
        return;
      }
      const shape = obj.findObject('SHAPE');
      shape.fill = DefaultNodeBG;
      const text = obj.findObject('TEXT');
      text.stroke = DefaultTextColor;
    }

    const DefaultNodeBG = '#CECECE';
    const HighlightedNodeBG = '#4688E8';
    const DefaultTextColor = '#4C4C4C';
    const HighlightedTextColor = '#FFFFFF';

    /* Init GoJS Object*/
    go.Shape.defineFigureGenerator('TaskIcon', function(shape, w, h) {
      const geo = new go.Geometry();
      const p1 = h / 2;
      geo.add(new go.PathFigure(0, p1)
        .add(new go.PathSegment(go.PathSegment.Arc, 180, 90, p1, p1, p1, p1))
        .add(new go.PathSegment(go.PathSegment.Arc, 270, 90, w - p1, p1, p1, p1))
        .add(new go.PathSegment(go.PathSegment.Arc, 0, 90, w - p1, h - p1, p1, p1))
        .add(new go.PathSegment(go.PathSegment.Arc, 90, 90, p1, h - p1, p1, p1).close()));
      geo.spot1 = new go.Spot(0, 0, 0.3 * p1, 0.3 * p1);
      geo.spot2 = new go.Spot(1, 1, -0.3 * p1, 0);
      return geo;
    });

    const goObject = go.GraphObject.make;

    const diagram = new go.Diagram();
    diagram.initialContentAlignment = go.Spot.LeftCenter;
    diagram.allowDrop = true;
    diagram.undoManager.isEnabled = true;
    diagram.nodeTemplate =
      goObject(go.Node, 'Auto',
        {
          selectionAdorned: false,
          mouseEnter: mouseEnter,
          mouseLeave: mouseLeave
        },
        new go.Binding('location', 'loc', go.Point.parse).makeTwoWay(go.Point.stringify),
        goObject(go.Shape,
          {
            // desiredSize: new go.Size(100, 30),
            name: 'SHAPE',
            height: 30,
            figure: 'TaskIcon',
            fill: DefaultNodeBG,
            strokeWidth: 0,
            portId: '',
            cursor: 'pointer',
            fromLinkable: true,
            toLinkable: true,
            fromLinkableSelfNode: true,
            toLinkableSelfNode: true,
            fromLinkableDuplicates: false,
            toLinkableDuplicates: false
          }, new go.Binding('fill', 'isSelected', function(sel) {
            if (sel) {
              return HighlightedNodeBG;
            } else {
              return DefaultNodeBG;
            }
          }).ofObject('')),
        goObject(go.TextBlock,
          {
            name: 'TEXT',
            margin: new go.Margin(5, 10, 5, 10) ,
            editable: false,
            stroke: DefaultTextColor},
          new go.Binding('text', 'key').makeTwoWay(),
          new go.Binding('stroke', 'isSelected', function(sel) {
            if (sel) {
              return HighlightedTextColor;
            } else {
              return DefaultTextColor;
            }
          }).ofObject(''))
      );
    diagram.linkTemplate =
      goObject(go.Link,
        {
          relinkableFrom: true,
          relinkableTo: true,
          selectionAdorned: false,
          routing: go.Link.AvoidsNodes,
          corner: 10
        },
        goObject(go.Shape,
          {
            strokeWidth: 2,
            stroke: DefaultTextColor
          }),
        goObject(go.Shape,
          {
            toArrow: 'Standard',
            fill: DefaultTextColor,
            stroke: null
          })
      );

    const tempLink = goObject(go.Link,
      {
        layerName: 'Tool'
      },
      goObject(go.Shape,
        {
          stroke: HighlightedNodeBG,
          strokeWidth: 2,
          strokeDashArray: [4, 2]
        })
    );

    const tempFromNode =
      goObject(go.Node,
        {
          layerName: 'Tool'
        },
        goObject(go.Shape, 'TaskIcon',
          {
            stroke: HighlightedNodeBG,
            fill: null,
            portId: '',
            width: 1,
            height: 1
          })
      );

    const tempToNode =
      goObject(go.Node,
        {
          layerName: 'Tool'
        },
        goObject(go.Shape, 'TaskIcon',
          {
            stroke: HighlightedNodeBG,
            strokeWidth: 3,
            fill: null,
            portId: '',
            width: 1,
            height: 1
          })
      );

    diagram.toolManager.linkingTool.temporaryLink = tempLink;

    diagram.toolManager.linkingTool.temporaryFromNode = tempFromNode;
    diagram.toolManager.linkingTool.temporaryFromPort = tempFromNode.port;

    diagram.toolManager.linkingTool.temporaryToNode = tempToNode;
    diagram.toolManager.linkingTool.temporaryToPort = tempToNode.port;

    diagram.toolManager.relinkingTool.temporaryLink = tempLink;

    diagram.toolManager.relinkingTool.temporaryFromNode = tempFromNode;
    diagram.toolManager.relinkingTool.temporaryFromPort = tempFromNode.port;

    diagram.toolManager.relinkingTool.temporaryToNode = tempToNode;
    diagram.toolManager.relinkingTool.temporaryToPort = tempToNode.port;

    return diagram;
  }

  initGoJSDiagramReadOnly() {
    const DefaultNodeBG = '#CECECE';
    const HighlightedNodeBG = '#4688E8';
    const DefaultTextColor = '#4C4C4C';
    const HighlightedTextColor = '#FFFFFF';

    /* Init GoJS Object*/
    go.Shape.defineFigureGenerator('TaskIcon', function(shape, w, h) {
      const geo = new go.Geometry();
      const p1 = h / 2;
      geo.add(new go.PathFigure(0, p1)
        .add(new go.PathSegment(go.PathSegment.Arc, 180, 90, p1, p1, p1, p1))
        .add(new go.PathSegment(go.PathSegment.Arc, 270, 90, w - p1, p1, p1, p1))
        .add(new go.PathSegment(go.PathSegment.Arc, 0, 90, w - p1, h - p1, p1, p1))
        .add(new go.PathSegment(go.PathSegment.Arc, 90, 90, p1, h - p1, p1, p1).close()));
      geo.spot1 = new go.Spot(0, 0, 0.3 * p1, 0.3 * p1);
      geo.spot2 = new go.Spot(1, 1, -0.3 * p1, 0);
      return geo;
    });

    const goObject = go.GraphObject.make;

    const diagram = new go.Diagram();
    diagram.initialContentAlignment = go.Spot.Center;
    diagram.allowDrop = true;
    diagram.undoManager.isEnabled = true;
    diagram.nodeTemplate =
      goObject(go.Node, 'Auto',
        {
          selectionAdorned: false,
          selectable: false
        },
        new go.Binding('location', 'loc', go.Point.parse).makeTwoWay(go.Point.stringify),
        goObject(go.Shape,
          {
            // desiredSize: new go.Size(100, 30),
            name: 'SHAPE',
            height: 30,
            figure: 'TaskIcon',
            fill: DefaultNodeBG,
            strokeWidth: 0,
            portId: '',
            cursor: 'pointer',
            fromLinkable: false,
            toLinkable: false,
            fromLinkableSelfNode: false,
            toLinkableSelfNode: false,
            fromLinkableDuplicates: false,
            toLinkableDuplicates: false
          }, new go.Binding('fill', 'isHighlighted', function(sel) {
            if (sel) {
              return HighlightedNodeBG;
            } else {
              return DefaultNodeBG;
            }
          }).ofObject('')),
        goObject(go.TextBlock,
          {
            name: 'TEXT',
            margin: new go.Margin(5, 10, 5, 10) ,
            editable: false,
            stroke: DefaultTextColor},
          new go.Binding('text', 'key').makeTwoWay(),
          new go.Binding('stroke', 'isHighlighted', function(sel) {
            if (sel) {
              return HighlightedTextColor;
            } else {
              return DefaultTextColor;
            }
          }).ofObject(''))
      );
    diagram.linkTemplate =
      goObject(go.Link,
        {
          relinkableFrom: false,
          relinkableTo: false,
          selectionAdorned: false,
          routing: go.Link.AvoidsNodes,
          corner: 10
        },
        goObject(go.Shape,
          {
            strokeWidth: 2,
            stroke: DefaultTextColor
          }),
        goObject(go.Shape,
          {
            toArrow: 'Standard',
            fill: DefaultTextColor,
            stroke: null
          })
      );

    const tempLink = goObject(go.Link,
      {
        layerName: 'Tool'
      },
      goObject(go.Shape,
        {
          stroke: HighlightedNodeBG,
          strokeWidth: 2,
          strokeDashArray: [4, 2]
        })
    );

    const tempFromNode =
      goObject(go.Node,
        {
          layerName: 'Tool'
        },
        goObject(go.Shape, 'TaskIcon',
          {
            stroke: HighlightedNodeBG,
            fill: null,
            portId: '',
            width: 1,
            height: 1
          })
      );

    const tempToNode =
      goObject(go.Node,
        {
          layerName: 'Tool'
        },
        goObject(go.Shape, 'TaskIcon',
          {
            stroke: HighlightedNodeBG,
            strokeWidth: 3,
            fill: null,
            portId: '',
            width: 1,
            height: 1
          })
      );

    diagram.toolManager.linkingTool.temporaryLink = tempLink;

    diagram.toolManager.linkingTool.temporaryFromNode = tempFromNode;
    diagram.toolManager.linkingTool.temporaryFromPort = tempFromNode.port;

    diagram.toolManager.linkingTool.temporaryToNode = tempToNode;
    diagram.toolManager.linkingTool.temporaryToPort = tempToNode.port;

    diagram.toolManager.relinkingTool.temporaryLink = tempLink;

    diagram.toolManager.relinkingTool.temporaryFromNode = tempFromNode;
    diagram.toolManager.relinkingTool.temporaryFromPort = tempFromNode.port;

    diagram.toolManager.relinkingTool.temporaryToNode = tempToNode;
    diagram.toolManager.relinkingTool.temporaryToPort = tempToNode.port;

    return diagram;
  }
}
