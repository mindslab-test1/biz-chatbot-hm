import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { UserService } from './user.service';

export interface DataInterface {
  cmd: string;
  data: any;
}
@Injectable()
export class DataService {

  constructor(
    private userService: UserService,
  ) { }
  private subject = new Subject<DataInterface>();



  CMD = {
     TITLE: 'title'
    ,SAVE: 'save'
    ,NOTI_EXIT: 'noti_exit'
    ,EXIT: 'exit'
    ,GET: 'get'
    ,IS_SAVE: 'isSave'
    ,INSERT: 'insert'
    ,IS_DEBUG: 'isDebug'
    ,CHAT_START: 'chatStart'
    ,CHAT_END: 'chatEnd'
    ,CHAT_CLEAR: 'chatClear'
    ,SET_DEBUG_TARGET: 'setDebugTarget'
    ,CHATBOT_TEST_AVAILABLE: 'chatbotTestAvailable'
    ,PREVIEW_MESSAGE: 'previewMessage'
    ,PREVIEW_QNA: 'previewQna'
    ,PREVIEW_SCENARIO: 'previewScenario'
    ,IS_QNA: 'isQna'
    ,IS_SCENARIO: 'isScenario'
    ,CHAT_CURR_MENU: 'chatCurrMenu'
    ,IS_WORKING: 'isWorking'
    
    // ,NOTI_EXIT: 'notifyExit'
  };

  data: DataInterface;

  sendDataEx(message: DataInterface) {
      this.subject.next(message);
  }
  sendData(cmd: string, data: Object) {
    if(cmd == 'isSave'){
    
      if(data['isSave'] != undefined){
        this.userService.changeIsSaveFlag(data['isSave']);
      }
    }
    
    const message = {cmd, data};
    this.subject.next(message);
  }

  clearData() {
      this.subject.next();
  }

  getData(): Observable<DataInterface> {
      return this.subject.asObservable();
  }

}
