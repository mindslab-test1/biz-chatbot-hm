/**
 * Created by zepirox on 2018. 2. 22..
 */
import {Injectable, ElementRef} from '@angular/core';
import {environment} from '../environments/environment';

import 'rxjs/add/operator/map';

@Injectable()
export class CommonUtilService {
  private url: string;
  private previousUrl: string;

  constructor() {
    this.url = environment.url;
  }

  public getPreviousUrl() {
    return this.previousUrl;
  }
  public setPreviousUrl(v) {
    this.previousUrl = v;
  }

  public getDummyData() {
    return environment.dummyData;
  }

  public removeElementFromArray(array, element) {
    const index = array.indexOf(element);
    if (index !== -1) {
      array.splice(index, 1);
    }
  }

  public checkDuplicateElementFromArray(array, element): boolean {
    const index = array.indexOf(element);

    if (index !== -1) {
      return true;
    } else {
      return false;
    }
  }

  public arrayToSelectItems(array: Array<string>) {
    const retObj = [];
    for (let i = 0; i < array.length; i++) {
      retObj.push({
        label: array[i],
        value: array[i]
      });
    }
    return retObj;
  }

  public objectArrayToSelectItemWithLabelKey(array: Array<any>, label) {
    const retObj = [];
    for (let i = 0; i < array.length; i++) {
      retObj.push({
        label: array[i][label],
        value: array[i][label]
      });
    }
    return retObj;
  }

  public objectArrayToSelectedItemWithLabelKeyAndMergeLabelKey(array: Array<any>, labelKey, mergeKey) {
    const retObj = [];
    for (let i = 0; i < array.length; i++) {
      retObj.push({
        label: array[i][labelKey] + ' - ' + array[i][mergeKey],
        value: array[i][labelKey]
      });
    }
    return retObj;
  }

  public objectArrayToSelectItemWithLabelKeyAndValue(array: Array<any>, label, value) {
    const retObj = [];
    for (let i = 0; i < array.length; i++) {
      retObj.push({
        label: array[i][label],
        value: array[i][value]
      });
    }
    return retObj;
  }

  public getObjectFromArrayWithKeyAndValue(array, key, value) {
    const foundArray = array.filter((val, idx) => (val['key'] === value));
    if (foundArray.length > 0) {
      return foundArray[0];
    }
    return null;
  }

  public getLabelFromSelectItemsWithValue(array, value) {
    const foundArray = array.filter((val, idx) => (val['value'] === value));
    if (foundArray.length > 0) {
      return foundArray[0].label;
    }
    return value;
  }

  public getFileNameArray(fileArray) {
    const retObj = [];
    for (let i = 0; i < fileArray.length; i++) {
      retObj.push(fileArray[i].name);
    }
    return retObj;
  }

  private stringToString(str, len) {
    let s = '', i = 0;
    while (i++ < len) {
      s += str;
    }
    return s;
  }

  private stringZf(str, len) {
    return this.stringToString('0', len - str.length) + str;
  }

  public convertDateFormat(date, format) {
    if (date === null || !date.valueOf()) {
      return '';
    }
    const weekName = ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'];
    const d = date;
    const self = this;
    return format.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function ($1) {
      switch ($1) {
        case 'yyyy': {
          return d.getFullYear();
        }
        case 'yy': {
          return self.stringZf('' + (d.getFullYear() % 1000), 2);
        }
        case 'MM': {
          return self.stringZf('' + (d.getMonth() + 1), 2);
        }
        case 'dd': {
          return self.stringZf('' + (d.getDate()), 2);
        }
        case 'E': {
          return weekName[d.getDay()];
        }
        case 'HH': {
          return self.stringZf('' + d.getHours(), 2);
        }
        case 'hh': {
          let h;
          return self.stringZf('' + ((h = d.getHours() % 12) ? h : 12), 2);
        }
        case 'mm': {
          return self.stringZf('' + d.getMinutes(), 2);
        }
        case 'ss': {
          return self.stringZf('' + d.getSeconds(), 2);
        }
        case 'a/p': {
          return d.getHours() < 12 ? '오전' : '오후';
        }
        default: {
          return $1;
        }
      }
    });
  }

  public getLocalStorageItem(item) {
      return localStorage.getItem(item) || '';
  }
  public setLocalStorageItem(item, value) {
      localStorage.setItem(item, value);
  }
  public clearLocalStorageItem(item) {
      localStorage.removeItem(item);
  }
  public clearLocalStorage() {
      localStorage.clear();
  }

  sel = (v, el = document) => el.querySelector(v);
  el = (tag, ...attr) => {
    const el = typeof tag === 'string' ? document.createElement(tag) : tag;
    for (let i = 0; i < attr.length;) {
      const k = attr[i++], v = attr[i++];
      if (this.isFunction(el[k])) { el[k](...(Array.isArray(v) ? v : [v])); } else if (k[0] === '@') { el.style[k.substr(1)] = v; } else { el[k] = v; }
    }
    return el;
  };
  err = (v = 'invalid') => { throw v; };
  override = () => {
  //  this.err('override');
  };

  prop = (t, p) => Object.assign(t, p);
  is = (t, p) => t instanceof p;
  isNull = (v) => {
    if (v == null || v === undefined) {
      return true;
    }
  };
  isNotNull = (v) => !this.isNull(v);
  isEmpty = (v) => {
    if (this.isNull(v) || !v) {
      return true;
    }
    return (Object.keys(v).length === 0 && v.constructor === Object)
     || (v.length === 0 && v.constructor === Array);
  };
  isNotEmpty = (v) => {
    return !this.isEmpty(v);
  };
  isJsonString = (v) => {
    try {
      JSON.parse(v);
      return true;
    } catch (err) {
      return false;
    }
  };
  isFunction = (v) => (this.isNotEmpty(v) && (typeof v === 'function'));
  equalObj = (v, v2) => {
    return JSON.stringify(v) === JSON.stringify(v2);
  };
  notEqualObj = (v, v2) => {
    return !this.equalObj(v, v2);
  };
  equalFunction = (v: Function, v2: Function) => {
    const f1 = v.toString();
    const f2 = v2.toString();
    return f1 === f2;
  };
  find = (v, key, expect) => {
    const res = v.find((item) => item[key] === expect);
    return res;
  };
  filter = (v, key, expect) => {
    const res = v.filter((item) => item[key] === expect);
    return res;
  };
  reduce = (v, key, expect) => {
    const res = v.filter((item) => item[key] !== expect);
    return res;
  };
  getKeys = (json: Array<any>, key: string): Array<any> => {
    const result = json.reduce((res: Array<any>, v) => {
      res.push(v[key]);
      return res;
    }, []);
    return result;
  };
  parseObjToArr = (item: any, parser: any= (key, item, res) => {
    res.push(item[key]);
    return res;
  }): any[] => {
    let res = [];
    for (const key in item) {
      res = parser(key, item, res);
    }
    return res;
  };
  parseObjToArrEx = (item: any, parser: any= (key, item, res) => {
    res.push({
      key: key
     , value: item[key]
    });
    return res;
  }): any[] => {
    return this.parseObjToArr(item, parser);
  };

  /**
   * @argument before 'train'
   * @argument expect 'proper'
   * @argument beforeDelim '_'
   * @argument afterDelim ' '
   */
  convertCase = (target: string, before = 'train', expect = 'proper', beforeDelim = '_', afterDelim = ' ') => {
    const upper = (v: string) => v.toUpperCase();
    const lower = (v: string) => v.toLowerCase();
    const first = upper, second = lower;

    return target.split(beforeDelim).reduce((res, item) => {
      item = item.split('').map((v, i) => {
        if (i === 0) {
          v = first(v);
        } else {
          v = second(v);
        }
        return v;
      }).join('');
      res.push(item);
      return res;
    }, []).join(afterDelim);
  };




  // for angular ==========================================

  // ElementRef
  private DELIM = ' ';
  getClassList = (ele: ElementRef) => ele.nativeElement.className.split(this.DELIM);
  addClass = (ele: ElementRef, className) => {
    const classList = this.getClassList(ele);
    if (!classList.includes(className)) {
      classList.push(className);
      ele.nativeElement.className = classList.join(this.DELIM);
    }
  };
  removeClass = (ele: ElementRef, className) => {
    const classList = this.getClassList(ele);
    if (classList.includes(className)) {
      ele.nativeElement.className = classList.filter(classNameZ => classNameZ !== className).join(this.DELIM);
    }
  };
  getClassListEx = (ele) => ele.className.split(this.DELIM);
  addClassEx = (ele, className) => {
    const classList = this.getClassListEx(ele);
    if (!classList.includes(className)) {
      classList.push(className);
      ele.className = classList.join(this.DELIM);
    }
  };
  removeClassEx = (ele, className) => {
    const classList = this.getClassListEx(ele);
    if (classList.includes(className)) {
      ele.className = classList.filter(classNameZ => classNameZ !== className).join(this.DELIM);
    }
  };

  isDev = () => (!environment.production);


  // for web ==============================================

  isKey = (event, keyCode, key?) => {
    if (!(event && (event.keyCode || event.key || event.code))) {
      return false;
    }
    if ( event.keyCode === keyCode || event.key === key || event.code === key ) {
      return true;
    }
    return false;
  };
  isEnter = (event) => this.isKey(event, 13, 'Enter');
  isArrowUp = (event) => this.isKey(event, 38, 'ArrowUp');
  isArrowDown = (event) => this.isKey(event, 40, 'ArrowDown');
  isKeyup = (event) => event.type === 'keyup';

  isIe = () => {
    const res = (
        navigator.appName === 'Netscape' && navigator.userAgent.search('Trident') !== -1
      ) || (
        navigator.userAgent.toLowerCase().indexOf('msie') !== -1
      );
    return res;
  };

  devLog = (message?: any, ...optionalParams: any[]) => {
    if (this.isDev()) {
      if (this.isEmpty(optionalParams)) {
        console.log(message);
      } else {
        console.log(message, optionalParams);
      }
    }
  };

  public replaceAll(str, searchStr, replaceStr) {
    return str.split(searchStr).join(replaceStr);
  }

  public decimalAdjust(type, value, exp) {
    // If the exp is undefined or zero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }


  // UI
  scrollDownEle(ele: ElementRef) {
    if (this.isEmpty(ele)) {
      return false;
    }
    // ele.nativeElement.scrollIntoViewIfNeeded();
    ele.nativeElement.scrollTo({top: ele.nativeElement.scrollHeight, behavior: 'smooth'});
  }
}

