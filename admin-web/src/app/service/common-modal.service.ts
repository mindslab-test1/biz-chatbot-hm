import {EventEmitter, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { MessageExService as msgService } from './message.service';
import { Msg, lang } from '../model/message';
import { CommonUtilService } from './commonutil.service';

export enum ModalType {
  DEFAULT = <any>'DEFAULT',
  DIALOG_MODEL_TRAINING_MODAL = <any>'DIALOG_MODEL_TRAINING_MODAL',
  ALERT_MESSAGE_MODAL = <any>'ALERT_MESSAGE'
}

export class CommonModalEvent {
  modalType: ModalType;
  modalTitle: string;
  modalTextMessage: string;
  modalHasCancelButton: boolean;
  modalHasUserAction: boolean = true;
  modalSectionStyle: any = {};
  isOpen: boolean;
  contentsIsHtml: boolean;
  eventEmitter: EventEmitter<any>;
  modalExtraData: any;

  constructor(
  ) {
    this.modalType = ModalType.DEFAULT;
    this.modalTextMessage = '';
    this.modalTitle = Msg.com[lang].titleAlert;
    this.isOpen = true;
    this.contentsIsHtml = false;
    this.eventEmitter = null;
    this.modalExtraData = null;
  }
}

export class DialogModelTrainingEvent {
  indriThreshold: number;
  svmThreshold: number;
  isOk: boolean;
  constructor() {
    this.indriThreshold = 0.5;
    this.svmThreshold = 0;
    this.isOk = true;
  }
}

export class AlertModalEvent {
  isOk: boolean;
  constructor() {
    this.isOk = false;
  }
}

@Injectable()
export class CommonModalService {
  private modalHandlerObserver: any;
  public modalHandler: Observable<CommonModalEvent>;
  constructor(
    private cm: CommonUtilService
  ) {
    this.modalHandlerObserver = null;
    this.modalHandler = Observable.create(observer => {
      this.modalHandlerObserver = observer;
    });
  }

  _modalNotReady(){
    return this.modalHandlerObserver == null || this.modalHandlerObserver == undefined;
  }

  public openDialogModelTrainingModal(modelId: number, modelName: string) {
    const evt = new CommonModalEvent();
    evt.modalType = ModalType.DIALOG_MODEL_TRAINING_MODAL;
    evt.eventEmitter = null;
    evt.isOpen = true;
    evt.modalExtraData = {};
    evt.modalExtraData['modelId'] = modelId;
    evt.modalExtraData['modelName'] = modelName;
    this.modalHandlerObserver.next(evt);
  }

  public openCommonDialogOnlyMsg(modalTitle: string, modalHtml: string, hasCancelbutton: boolean = false, hasFooter: boolean = false, eventEmitter?: EventEmitter<AlertModalEvent>) {
    this.openCommonDialog(modalTitle, modalHtml, hasCancelbutton, hasFooter, { background: 'none'}, eventEmitter);
  }

  public openCommonDialog(modalTitle: string, modalHtml: string, hasCancelbutton: boolean, hasFooter: boolean = true, modalSectionStyle: any = {}, eventEmitter?: EventEmitter<AlertModalEvent>) {
    return this.openDialogBase(modalTitle, modalHtml, true, hasCancelbutton, hasFooter, modalSectionStyle, eventEmitter);
  }

  public openAlertDialog(modalTitle: string = Msg.com[lang].titleError, modalText: any, hasCancelbutton: boolean = false, eventEmitter?: EventEmitter<AlertModalEvent>) {
    const msg = typeof modalText == 'object' ? msgService.parseErrorMessage(modalText) : modalText;
    return this.openDialogBase(modalTitle, msg, false, hasCancelbutton, true, {}, eventEmitter);
  }

  public openSuccessAlert(modalText: any, modalTitle: string = Msg.com[lang].titleSuccess) {
    return this.openAlertDialog(modalTitle, modalText, false, null);
  }

  public openErrorAlert(modalText: any, modalTitle: string = Msg.com[lang].titleError) {
    return this.openAlertDialog(modalTitle, modalText, false, null);
  }

  public openConfirmDialog(modalContent: any, okCallback: Function = ()=>{ this.cm.override(); }
      , contentsIsHtml: boolean = false
      , modalTitle: string = Msg.com[lang].titleConfirm
      , hasCancelbutton: boolean = true, cancelCallback?: Function
  ) {
    const msg = typeof modalContent == 'object' ? msgService.parseErrorMessage(modalContent) : modalContent;
    let eventEmitter = new EventEmitter<AlertModalEvent>();
    eventEmitter.subscribe(res => {
      if(res.isOk){
        okCallback();
        return true;
      }
      this.cm.isFunction(cancelCallback) && cancelCallback();
      return false;
    }, err=>{
      console.log('confirm err',msgService.parseErrorMessage(err));
    });
    return this.openDialogBase(modalTitle, msg, contentsIsHtml, hasCancelbutton, true, {}, eventEmitter);
  }

  public openDialogBase(modalTitle: string, modalText: string, contentsIsHtml: boolean = false, hasCancelbutton: boolean, hasFooter: boolean = true, sectionStyle: any = {}, eventEmitter?: EventEmitter<AlertModalEvent>) {
    if (this._modalNotReady()) {
      return false;
    }
    const evt = new CommonModalEvent();
    evt.modalType = ModalType.ALERT_MESSAGE_MODAL;
    evt.eventEmitter = eventEmitter;
    evt.modalTitle = modalTitle;
    evt.modalTextMessage = modalText;
    evt.modalHasCancelButton = hasCancelbutton;
    evt.modalHasUserAction = hasFooter;
    evt.modalSectionStyle = sectionStyle;
    evt.isOpen = true;
    evt.contentsIsHtml = contentsIsHtml;
    this.modalHandlerObserver.next(evt);
    return true;
  }

  public closeAlertDialog() {
    const evt = new CommonModalEvent();
    evt.modalType = ModalType.ALERT_MESSAGE_MODAL;
    evt.isOpen = false;
    this.modalHandlerObserver.next(evt);
  }

}
