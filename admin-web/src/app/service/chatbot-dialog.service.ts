import { Injectable, OnDestroy } from '@angular/core';
import { UtterData, RespDialog, ChatbotButton } from '../model/response-dialog';
import { IChatbotConst } from '../model/chatbot-const';
import { CommonUtilService } from '.';
import { MessageType } from '../model/chatbot-message';
import { DataService } from './data.service';
import { chatbotStepGroups, chatbotStepGroupNames, chatbotStepInvalid } from '../model/chatbot-step';
import { Chatbot_info, Chatbot_msg, Chatbot_buttons } from '../adminsdk';
import { DateService } from './date.service';

const _ = require('lodash');

@Injectable()
export class ChatbotDialogService implements OnDestroy {
  constructor(
      private cm: CommonUtilService
    , private ds: DataService
    , private dateService: DateService
  ) { }

  ngOnDestroy() {
    this.ds.clearData();
  }

  c = IChatbotConst;
  default = {
      sessionId: ''
    , chatInfo: null
    , uttrData: null
    , chatbotStep: chatbotStepInvalid[0]
  };

  getContentType(uttrData: UtterData) {
    const defaultContentType = MessageType.TEXT;
    let res = defaultContentType;
    if( this.cm.isEmpty(uttrData)
    ) {
      console.log('no uttrData.',uttrData);
      return res;
    }
    if( this.cm.isEmpty(uttrData.richContent)
      || this.cm.isEmpty(uttrData.richContent.contentTypeCd)
    ) {
      console.log('no richContent.',uttrData);
      return res;
    }
    res = this.getMessageRichContentType(uttrData.richContent.contentTypeCd);
    return res;
  }

  getMessageRichContentType(contentTypeCd: string) {
    const defaultContentType = MessageType.TEXT;
    let res;
    switch(contentTypeCd){
      case this.c.RICH_CONTENT_TYPE_BUTTON:
        res = MessageType.BUTTON;
        break;
      case this.c.RICH_CONTENT_TYPE_IMAGE:
        res = MessageType.IMAGE;
        break;
      case this.c.RICH_CONTENT_TYPE_CAROUSEL:
        res = MessageType.CAROUSEL;
        break;
      case this.c.RICH_CONTENT_TYPE_API:
        res = MessageType.API;
        break;
      default:
        res = defaultContentType;
    }
    return res;
  }

  parseUttrToRespDialog(uttrData: UtterData, res: RespDialog) {
    res.uttrData = uttrData;
    return res;
  }

  parseChatbotInfoToRespDialog(isGreeting, data: Chatbot_info) {
    if(
        this.cm.isEmpty(data)
        ||this.cm.isEmpty(data.bot_id)
        ||this.cm.isEmpty(data.chatbotConfigs)
    ){
      return false;
    }

    const richContent = isGreeting
        ? data.chatbotConfigs[0].richContentGreeting
        : data.chatbotConfigs[0].richContentUnknown;

    if(this.cm.isEmpty(richContent)) {
      return false;
    }

    const parseButton = (_buttons: Chatbot_buttons[]): ChatbotButton[] =>{
      const buttons: ChatbotButton[] = _buttons.reduce((res,item)=>{
        let v: ChatbotButton = {
            rcId: item.rc_id
          , btnOrder: item.btn_order
          , title: item.title
          , userUtter: item.user_utter
          , createdDtm: item.created_dtm
          , updatedDtm: item.updated_dtm
          , creatorId: item.creator_id
          , updatorId: item.updator_id
        };
        res.push(v);
        return res;
      },[]);
      return buttons;
    };
    const _buttons: Chatbot_buttons[] = richContent.buttons;
    const contentTypeCd = richContent.content_type_cd;
    const buttons = parseButton(_buttons);
    // const carousels = parseCarouseles();
    const res = this.parseRespDialog(data.bot_id, contentTypeCd, buttons);
    return res;
  }
  parseRespDialog(bot_id, contentTypeCd, details) {
    const now = this.dateService.getNowDate();
    let res: RespDialog = {
      sessionId: ''
      , chatbotId: bot_id
      , result: null
      , userId: ''
      , uttrData: {
        // speaker: DIALOG_SPEAKER;
          speaker: null
        , utter_id: -1
        , userId: ''
        , sessionId: ''
        , timestamp: now
        , sentence: ''
        , dialogSeq: -1
        , currStep: ''
        , sluWeight: -1
        , richContent: {
            rcId: -1
          , contentTypeCd
          , title: ''
          , content: ''
          , imgPath: ''
          , linkPath: ''
          , createdDtm: now
          , updatedDtm: now
          , creatorId: ''
          , updatorId: ''
          , buttons: []
          , image: null
          , carousels: []
        }
         ,isDialogEnded: 0
         ,reconfirmMsg:{
             msg:''
            ,richContent: {
                  rcId: -1
                , contentTypeCd
                , title: ''
                , content: ''
                , imgPath: ''
                , linkPath: ''
                , createdDtm: now
                , updatedDtm: now
                , creatorId: ''
                , updatorId: ''
                , buttons: []
                , image: null
                , carousels: []
              }
        }
      }
    };
    if(this.cm.isNotEmpty(details)) {
      if(contentTypeCd == this.c.RICH_CONTENT_TYPE_BUTTON) {
        res.uttrData.richContent.buttons = details;
      }
      if(contentTypeCd == this.c.RICH_CONTENT_TYPE_CAROUSEL) {
        res.uttrData.richContent.carousels = details;
      }
    }
    return res;
  }
  parseDialogsToButton = (list: any[]): ChatbotButton[]=>{
    const res = list.reduce((resList: ChatbotButton[],v: any, i: number)=>{
      let button: ChatbotButton = {
         rcId: -1
        ,btnOrder: i
        ,title: v.title
        ,userUtter: v.userUtter
        ,createdDtm: new Date()
        ,updatedDtm: new Date()
        ,creatorId: 'ADMIN'
        ,updatorId: 'ADMIN'
      };
      resList.push(button);
      return resList;
    },[]);
    return res;
  };

  changeDebug(entity: RespDialog) {
    this.ds.sendData(this.ds.CMD.SET_DEBUG_TARGET,entity);
  }

  private names = chatbotStepGroupNames;
  chatbotStepGroupIs(groupName: string, v: string){
    return _.includes(chatbotStepGroups[groupName].member, v);
  }
  chatbotStepGroupNameGet(v: string): string{
    const names = this.names;
    for (const name in names) {
      if(this.chatbotStepGroupIs(name, v)) {
        return name;
      }
    }
    return '';
  }
  chatbotStepGroupIsInvalid(v: string){
    return this.chatbotStepGroupIs(this.names.invalid, v);
  }
  chatbotStepGroupIsInit(v: string){
    return this.chatbotStepGroupIs(this.names.init, v);
  }
  chatbotStepGroupIsBqa(v: string){
    return this.chatbotStepGroupIs(this.names.bqa, v);
  }
  chatbotStepGroupIsScf(v: string){
    return this.chatbotStepGroupIs(this.names.scf, v);
  }
  chatbotStepGroupIsSds(v: string){
    return this.chatbotStepGroupIs(this.names.sds, v);
  }

  getChatbotGreetingMessage(data:Chatbot_info): Chatbot_msg {
    return this.getChatbotMessage(data, this.c.MSG_TYPE_GREETING);
  }

  getChatbotUnknownMessage(data:Chatbot_info): Chatbot_msg {
    return this.getChatbotMessage(data, this.c.MSG_TYPE_UNKNOWN);
  }

  private getChatbotMessage(data:Chatbot_info, target): Chatbot_msg {
    const msgs = data.chatbotMsgs.filter(v=>v.msg_type_cd == target);
    const max = msgs.length;
    const min = 0;
    const res = msgs[this.getRandomInteger(min, max)];
    return res;
  }
  /**
   * 균등 분포.
   */
  private getRandomInteger=(min, max)=>{
    return Math.floor(Math.random() * (max - min)) + min;
  }
}
