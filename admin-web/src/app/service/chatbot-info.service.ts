import { Injectable } from '@angular/core';
import { FileService } from './file.service';

@Injectable()
export class ChatbotInfoService {

  constructor(
    private fileService: FileService
  ) { }

  default = {
    botId: -1,
    img_path: './assets/images/chatbot-default.png',
    title: ''
  };

  set = {
    container_chatbot_img: 'chatbot-img',
    max_size: 512,
    max_size_view: 128,
    chat_max_size_view: 110,
    ext_thumb: 'thumb'
  };

  getImgPath(img_path){
    return this.fileService.getDownloadPath(this.set.container_chatbot_img, img_path);
  }
  getThumbImgPath(img_path){
    return `${this.getImgPath(img_path)}.${this.set.ext_thumb}`;
  }

  isNotRequireResize(_width,_height, max){
    if(_width < max && _height < max){
      return true;
    }
    return false;
  }
  /**
   *  resize
   */
  calcResizeWH(_width,_height, max){
    let width, height;
    if(_width > _height){
      width = max;
      height = (_height * max) / _width;
    } else {
      width = (_width * max) / _height;
      height = max;
    }
    return {
      width, height
    }
  }
}
