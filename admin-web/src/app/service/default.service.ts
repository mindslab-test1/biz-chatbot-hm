import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../environments/environment';

import 'rxjs/add/operator/map';

@Injectable()
export class DefaultService {
  private url: string;

  constructor(private http: Http) {
    this.url = environment.api;
  }

  public getDefault(): Observable<any> {
    return this.http.get(this.url)
      .map((response: Response) => {
         return response.json();
      });
  }
  public editDefault(param): Observable<any> {
    return this.http.put(this.url, JSON.stringify(param))
      .map((response: Response) => {
          return response.json();
      });
  }
  public deleteDefault(): Observable<any> {
    return this.http.delete(this.url)
      .map((response: Response) => {
          return response.json();
      });
  }
  public addDefault(param): Observable<any> {
    return this.http.post(this.url, JSON.stringify(param))
      .map((response: Response) => {
          return response.json();
      });
  }
}
