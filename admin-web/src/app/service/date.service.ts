import { Injectable } from '@angular/core';

const moment = require('moment');
@Injectable()
export class DateService {

  constructor(
  ) {}

  /** moment isSame */
  isSame=(v, v2)=>moment(v).isSame(v2);
  /** moment isNotSame */
  isNotSame=(v, v2)=>!this.isSame(v, v2);
  /** moment isAfter */
  isAfter=(s, e)=>moment(s).isAfter(e);
  getAddDate=(addDate: number, v: Date)=>{
    let date = new Date(v);
    date.setDate(date.getDate() + addDate);
    return date;
  };
  getAddHour=(addHour: number, v: Date)=>{
    let date = new Date(v);
    date.setHours(date.getHours() + addHour);
    return date;
  };

  /**
   * date: Date obj
   * format: default YYYY-MM-DD HH:mm:ss
   */
  format=(date = new Date(), format: string = 'YYYY-MM-DD HH:mm:ss')=>moment(date).format(format);

  formatDay=(date = new Date(), format: string = 'YYYY-MM-DD')=>moment(date).format(format);

  /**
   * via common.es5.js
   */
  isDate=(obj)=>{
    return obj instanceof Date && !isNaN(obj.valueOf());
  };

  /**
   * via common.es5.js
   */
  isDateStr=(value: string)=>{
    const date = new Date(value);
    if (this.isDate(date)) {
      return true;
    }
    const ISO8601_DATE_REGEX = /^(\d{4})-?(\d\d)-?(\d\d)(?:T(\d\d)(?::?(\d\d)(?::?(\d\d)(?:\.(\d+))?)?)?(Z|([+-])(\d\d):?(\d\d))?)?$/;
    let /** @type {?} */ match = void 0;
    if ((typeof value === 'string') && (match = value.match(ISO8601_DATE_REGEX))) {
      return true;
    }
    return false;
  };

  getDefaultStartDateBeforeOneMonth(now: Date = new Date(), interval = 1): Date{
    let res = new Date(now.toISOString());
    res.setMonth(res.getMonth() - interval);
    res = this.setDefaultStartTime(res);
    return res;
  }


  getDefaultStartDateBeforeOneWeek(now: Date = new Date(), interval = 7): Date {
    let res = new Date(now.toISOString());
    res.setDate(res.getDate() - interval);
    res = this.setDefaultStartTime(res);
    return res;
  }

  getDefaultStartDateBeforeOneDay(now: Date = new Date(), interval = 1): Date {
    let res = new Date(now.toISOString());
    res.setDate(res.getDate() - interval);
    res = this.setDefaultStartTime(res);
    return res;
  }


  getDefaultStartDate(now: Date = new Date()): Date{
    let res = new Date(now.toISOString());
    res = this.setDefaultStartTime(res);
    return res;
  }
  getDefaultEndDate(now: Date = new Date()): Date{
    let res = new Date(now.toISOString());
    res = this.setDefaultEndTime(res);
    return res;
  }
  getNowDate(now: Date = new Date()): Date{
    let res = new Date(now.toISOString());
    return res;
  }

  /**
   * 인자로 받은 date 객체를 00:00:00 '000 로 세팅한다.
   * @param res 시간을 셋할 date 객체
   */
  setDefaultStartTime(res: Date): Date{
    res.setHours(0, 0, 0, 0);
    return res;
  }
  /**
   * 인자로 받은 date 객체를 23:59:59 '999 로 세팅한다.
   * @param res 시간을 셋할 date 객체
   */
  setDefaultEndTime(res: Date): Date{
    res.setHours(23, 59, 59, 999);
    return res;
  }
  delim = {
     T:'T'
    ,Z:'Z'
  };
  getDateStrEx=(date: Date):string=>{
    // return new Date(date.getTime() - date.getTimezoneOffset()*60*1000).toISOString().replace('Z','');
    let temp: string[] = new Date(date.getTime() - date.getTimezoneOffset()*60*1000).toISOString().replace(this.delim.Z,'').split(this.delim.T);
    const dateStr = temp[0];
    const timeStr = temp[1];
    return `${dateStr} ${timeStr}`;
  };

  getMinDateStr=(date: Date)=>{
    return this.getDateStr(date, this.minTimeV);
  };
  getMinDate=(_date: Date)=>{
    let date = new Date(_date);
    return this.setTime(date, this.minTimeV);
  };
  getMinDateEx=(_date: Date)=>{
    let date = new Date(_date);
    date.setDate(date.getDate() + 1);
    return this.setTime(date, this.minTimeV);
  };
  getMinDateStrEx=(_date: Date)=>{
    let date = new Date(_date);
    date.setDate(date.getDate() + 1);
    return this.getDateStr(date, this.minTimeV);
  };

  getMaxDateStr=(date: Date)=>{
    return this.getDateStr(date, this.maxTimeV);
  };

  getDateStr=(date: Date,time: string)=>{
   const x = new Date();
   let currentTimeZoneOffsetInHours = x.getTimezoneOffset()/60;

   const setting = this.getAddHour(currentTimeZoneOffsetInHours, date);



   return this.setTime(date, time).toISOString();
  };

  setTime=(date: Date,time: string): Date=>{
    const parseHours = v => {
      if (!v.includes(':') || v.split(':').length < 2) {
        console.log(':를 포함한 시간값으로 쓸 것.');
        return {hours:0,minutes:0,seconds:0,ms:0};
      }
      const tarr = v.split('.');
      const arr = tarr[0].split(':');
      return {
         hours:Number(arr[0])
        ,minutes:Number(arr[1])
        ,seconds:Number(arr[2])
        ,ms:Number(tarr[1])
      };
    };

    const {hours, minutes, seconds, ms} = parseHours(time);
    date.setHours(hours, minutes, seconds, ms);

    // return new Date(date.getTime() - date.getTimezoneOffset()*60*1000).toISOString();
    return date;
    // return date.toString();
  };

  minTimeV: string = '00:00:00.000';
  maxTimeV: string = '23:59:59.999';

  // parseLoopbackDateStrToDate=(v): Date=>{
  //   if(this.isDateStr(v)){
  //     let res = new Date(v);
  //     res.setHours(res.getHours() - 9);
  //     return res;
  //   } else {
  //     console.log('invalid date str', v);
  //     return null;
  //   }
  // }

  /** (1000*60*60) */
  hourMs = 3600000;
  parseHour = (ms: number)=>{
    return ms/this.hourMs;
  };
  getMiddleDate = (s: Date,e: Date)=>{
    if(this.isAfter(s,e)) {
      throw 'start date 가  end date 보다 큼.';
    }
    const halfHours = this.parseHour((e.getTime()-s.getTime())/2);
    let m = new Date(s);
    m.setHours(m.getHours() + halfHours);
    return m;
  }
}
