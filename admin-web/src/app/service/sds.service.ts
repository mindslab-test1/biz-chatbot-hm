import { Injectable, Inject } from '@angular/core';
import { environment } from '../environments/environment';

import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { UserService } from './user.service';
import { Itf_sdsApi } from '../adminsdk/services/custom/Itf_sds';
import { Itf_corpus_categoryApi } from '../adminsdk/services/custom/Itf_corpus_category';
import { Itf_sds } from '../adminsdk/models/Itf_sds';
import { Itf_corpus_category } from '../adminsdk/models/Itf_corpus_category';
import { CommonModalService } from './common-modal.service';
import { CommonUtilService } from './commonutil.service';
import { Msg, lang } from '../model/message';
import { MessageExService } from './message.service';
import { ValidatorService } from './validator.service';
import { BaseResponse } from '../model/base-response';

export class SdsVo {
  domain: string;
  userId: string;
  intentName: string;
  userSays: Array<string>;
}
@Injectable()
export class SdsService {
  private url: string;
  userId: string;
  limit = 30;
  constructor(
    private httpClient: HttpClient,
    private userSerivce: UserService,
    private itf_sdsApi: Itf_sdsApi,
    private itf_corpus_categoryApi: Itf_corpus_categoryApi,
    @Inject(CommonModalService) private commonModalService: CommonModalService,
    private cm: CommonUtilService,
    private validator: ValidatorService
  ) {

    this.url = environment.backendApiUrl;
    // console.log("sds url", this.url);
    this.userId = this.userSerivce.getCurrentUserId();
  }

  prepareLearning(bot_id, callback = (domain)=>{ this.cm.override(); }) {

    let domain;
    this.itf_sdsApi.findOne({
      where: { and: [{ bot_id }, { project_name: "INTENT_FINDER_" + bot_id }] }
    }).subscribe((res: Itf_sds) => {
      console.log("itf_sdsApi", res);
      if (res) {
        domain = res.project_name;
      } else {
        console.log('no sds');
        return false;
      }
      if(this.cm.isEmpty(domain)) {
        this.commonModalService.openErrorAlert('학습할 도메인이 없습니다.', "Error Intent finder");
        return false;
      }
      if(!this.cm.isFunction(callback)) {
        console.log("invalid callback learning", domain);
        return false;
      }
      this._prepareLearning(bot_id, domain, (domain)=>{
        console.log('before last action');
        callback(domain);
      });
    }, err => {
      this.commonModalService.openErrorAlert("Error Intent finder", err);
      console.error(err);
    }, () => {
    });
  }

  private _prepareLearning(bot_id, domain, callback = (domain)=>{ this.cm.override(); }) {
      const _getCount = (_callback = ()=>{ this.cm.override(); })=>{
        this.itf_corpus_categoryApi.count({ bot_id }).subscribe(res =>{
          if(res && res.count > 0){
            _callback();
          }else{
            this.commonModalService.openErrorAlert("학습데이터를 넣어주세요");
          }
        });
      };
      _getCount(()=>{
        let arrayCorpus: Itf_corpus_category[];
        this.itf_corpus_categoryApi.find({ where: { bot_id } }).subscribe((res: Itf_corpus_category[]) => {
          console.log("itf_corpus_categoryApi", res);
          if (this.cm.isEmpty(res)) {
            this.commonModalService.openErrorAlert('학습할 문장이 없습니다.');
            return false;
          }
          arrayCorpus = res;
  
          let sdsVoArray = new Array<SdsVo>();
          let limit = this.limit;
          let intentName;
          if (domain && arrayCorpus) {
            intentName = arrayCorpus[0].intent_name;
          } else {
            return false;
          }
  
          let sdsVo = new SdsVo();
          sdsVo.domain = domain;
          sdsVo.intentName = intentName;
          sdsVo.userId = this.userId;
          sdsVo.userSays = new Array<string>();
          for (let x = 0; x < arrayCorpus.length; x++) {
            if (sdsVo.userSays.length >= limit || sdsVo.intentName != arrayCorpus[x].intent_name) {
              
              sdsVoArray.push(sdsVo);
              sdsVo = new SdsVo();
              sdsVo.domain = domain;
              sdsVo.intentName = arrayCorpus[x].intent_name;
              sdsVo.userId = this.userId;
              sdsVo.userSays = new Array<string>();
              sdsVo.userSays.push(arrayCorpus[x].question);
            } else {
              sdsVo.userSays.push(arrayCorpus[x].question);
            }
          }
          sdsVoArray.push(sdsVo);

          console.log("corpus 삭제 시작 sdsVoArray", sdsVoArray);
          this.deleteCorpus(this.userId, domain).subscribe((_res: any) => {
            if (_res.code == 0) {
              console.log("corpus 추가 시작 sdsVoArray", sdsVoArray);
              this.arrayInsert(sdsVoArray, ()=>{
                callback(domain);
              });
            } else {
              this.commonModalService.openErrorAlert(Msg.err[lang].failDeleteData);
            }
          }, _err => {
            console.log(MessageExService.parseErrorMessage(_err));
          })
  
          // this.commonModalService.openAlertDialog("Success", "추가 성공", false, null);
        }, err => {
          console.log(MessageExService.parseErrorMessage(err));
          return false;
        });
      });
  }
  arrayInsert(array: Array<SdsVo>, callback = ()=>{ this.cm.override(); }) {
    if (array.length <= 0) {
      console.log("corpus 추가 성공");
      callback();
      return true;
    }

    let data = array.pop();
    this.insertCorpus(data).subscribe((res: BaseResponse) => {
      console.log(res);
      if(this.validator.isInvalidResponse(res)) {
        this.commonModalService.openErrorAlert('학습 데이터 사전 준비에 실패했습니다.');
        return false;
      }
      this.arrayInsert(array, callback);
    }, err=>{
      console.log(MessageExService.parseErrorMessage(err));
    }, () => {
    });
  }
  insertCorpus(sdsVo) {
    console.log(sdsVo.toString());

    return this.httpClient.post(`${this.url}/admin/sds/insertCorpus`, sdsVo, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
    });
  }

  deleteCorpus(userId, domain) {

    const body = new HttpParams()
      .set('userId', userId)
      .set('domain', domain);

    return this.httpClient.post(`${this.url}/admin/sds/deleteCorpus`, body.toString(), {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    }
    );

  }
}
