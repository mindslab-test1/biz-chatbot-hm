import {Injectable} from '@angular/core';
import {DateService} from './date.service';
import {ResponseDialogResult, ResponseTps} from '../view/container/dashboard';
import {ResponseCpuUsedResult, ResponseMemUsedResult, ResponseDiskUsedResult} from '../view/container/dashboard/serverStatus.component';

import {CommonUtilService} from '.';
import {OptionEx} from '../model/dom-models';

@Injectable()
export class DashboardService {
  constructor(
    private dateService: DateService
    , private cm: CommonUtilService
  ) { }

  SET = {
    FIXED_POINT_PART_SIZE: 3,
    INTERVAL: 2,
    CHART: {
      MIN_VALUE: 0,
      X_STEP_SIZE_MIN: 1,
      X_DATA_COUNT_EXPECT: 12,
      X_STEP_SIZE_EXPECT: 2,
      MIN_TICKS_LIMIT_EXPECT: 5,
      MAX_TICKS_LIMIT_EXPECT: 8
    }
  };
  ds = this.SET;
  sc = this.ds.CHART;

  default: any = {
    statistics: {
      sessionCount: 0,
      dialogCount: 0,
      tpsAverage: 0,
      reliabilityAverage: 0
    },
    chart: {
      options: {
        scaleShowVerticalLines: false,
        responsive: true,
        legend: {
          position: 'top'
        },
        scales: {
          xAxes: [{
            type: 'time',
            time: {
              unit: 'hour',
              stepSize: this.sc.X_STEP_SIZE_EXPECT,
              displayFormats: {
                hour: 'HH:mm'
              }
            }
          }],
          yAxes: [{
            ticks: {
              max: 1,
              minTicksLimit: this.sc.MIN_TICKS_LIMIT_EXPECT,
              maxTicksLimit: this.sc.MAX_TICKS_LIMIT_EXPECT
              // ,beginAtZero: true
            }
          }]
        }
      }
    }
  };
  dd = this.default;
  dds = this.dd.statistics;
  ddc = this.default.chart;

  statistics: OptionEx[] = [
    {
      label: '세션 수'
      ,value: this.dds.sessionCount
      ,unit: ''
    },
    {
      label: '대화 수'
      ,value: this.dds.dialogCount
      ,unit: ''
    },
    {
      label: '평균 TPS'
      ,value: this.dds.tpsAverage.toFixed(this.ds.FIXED_POINT_PART_SIZE)
      ,unit: ''
    },
    {
      // label: '평균 신뢰도'
      label: '답변 성공률'
      ,value: this.dds.reliabilityAverage.toFixed(this.ds.FIXED_POINT_PART_SIZE)
      ,unit: '%'
    }
  ];
  rangeOption = {
     time: 'time'
    ,day: 'day'
  };

  parseStatTotal = (baseData)=>{
    const {sessionCount, dialogCount, tpsAverage, reliabilityAverage} = baseData[0];
    const parseData = this.statistics;
 //   console.log('psd  = ' + parseData);
    parseData[0].value = sessionCount;
    parseData[1].value = dialogCount;
    parseData[2].value = tpsAverage.toFixed(this.ds.FIXED_POINT_PART_SIZE);
    parseData[3].value = reliabilityAverage.toFixed(this.ds.FIXED_POINT_PART_SIZE);
    return parseData;
  };

  // 차트 값 보정 [s] ======================================================
  parseStatTps = (baseData, startDate: Date, endDate: Date, selectedRange: string)=>{
    let res = {
      datas: [
        {
          data: [],
          label: 'TPS'
        }
      ], period: []
    };
    if(this.cm.isEmpty(baseData)) {
      // 맨 앞 데이터 삽입
      res.period.push(this.parsePeriod(this.getViewStartDateStr(startDate), selectedRange));
      res.datas[0].data.push(this.sc.MIN_VALUE.toFixed(this.ds.FIXED_POINT_PART_SIZE));

      // 맨 끝 데이터 삽입
      res.period.push(this.parsePeriod(this.getViewEndDateStr(endDate, selectedRange), selectedRange));
      res.datas[0].data.push(this.sc.MIN_VALUE.toFixed(this.ds.FIXED_POINT_PART_SIZE));
      return res;
    }

    const lastIdx = baseData.length - 1;
    baseData.reduce((resList, v: ResponseTps, idx)=>{
      // 맨 앞 데이터 보정
      /*if(idx == 0) {
        res.period.push(this.parsePeriod(this.getViewBeforeStartDateStr(startDate, selectedRange), selectedRange));
        res.datas[0].data.push(this.sc.MIN_VALUE);


         if (this.rangeByTime(selectedRange)) {
           if (this.dateService.isNotSame(v.period, this.getViewStartDateStr(startDate))) {
             res.period.push(this.parsePeriod(this.getViewStartDateStr(startDate), selectedRange));
             res.datas[0].data.push(this.sc.MIN_VALUE.toFixed(this.ds.FIXED_POINT_PART_SIZE));
           }
         }
      }*/

      res.period.push(this.parsePeriod(v.period, selectedRange));
      res.datas[0].data.push(v.tps.toFixed(this.ds.FIXED_POINT_PART_SIZE));

      // 맨 끝 데이터 보정
      if(idx == lastIdx) {

        // this.cm.isDev() && console.log('date compare end','\n',new Date(v.period),'\n',endDate,'\n',this.dateService.isNotSame(v.period, endDate));
        if (this.rangeByTime(selectedRange)) {
            if (this.dateService.isNotSame(v.period, this.getViewEndDateStr(endDate, selectedRange))) {

                if (v.tps > 0) {
                    res.period.push(this.parsePeriod(v.period, selectedRange));
                    res.datas[0].data.push(this.sc.MIN_VALUE);
                }

                res.period.push(this.parsePeriod(this.getViewEndDateStr(endDate, selectedRange), selectedRange));
                res.datas[0].data.push(this.sc.MIN_VALUE.toFixed(this.ds.FIXED_POINT_PART_SIZE));
            }
        }
        res.period.push(this.parsePeriod(this.getViewAfterEndDateStr(endDate, selectedRange), selectedRange));
        res.datas[0].data.push(this.sc.MIN_VALUE);
      }

      // resList.push(v);
      // return resList;
    },[]);

   // console.log(res);

    return res;
  };
  parseStatDialogResult = (baseData, startDate: Date, endDate: Date, selectedRange: string)=>{
    let res = {
      datas: [
        {
          data: [],
          label: '정상 답변'
        },
        {
          data: [],
          label: '답변 못함'
        },
        // {
        //   data: [],
        //   label: '오류'
        // }
      ], period: []
    };
    if(this.cm.isEmpty(baseData)) {

      // 맨 앞 데이터 삽입
      res.period.push(this.parsePeriod(this.getViewStartDateStr(startDate), selectedRange));
      res.datas[0].data.push(this.sc.MIN_VALUE);
      res.datas[1].data.push(this.sc.MIN_VALUE);
      // res.datas[2].data.push(this.sc.MIN_VALUE);

      // 중간 데이터 삽입
      res.period.push(this.parsePeriod(this.getViewMiddleDateStr(startDate, endDate), selectedRange));
      res.datas[0].data.push(this.sc.MIN_VALUE);
      res.datas[1].data.push(this.sc.MIN_VALUE);
      // res.datas[2].data.push(this.sc.MIN_VALUE);

      // 맨 끝 데이터 삽입
      res.period.push(this.parsePeriod(this.getViewEndDateStr(endDate, selectedRange), selectedRange));
      res.datas[0].data.push(this.sc.MIN_VALUE);
      res.datas[1].data.push(this.sc.MIN_VALUE);
      // res.datas[2].data.push(this.sc.MIN_VALUE);
      return res;
    }

    const lastIdx = baseData.length - 1;


    baseData.reduce((resList, v: ResponseDialogResult, idx)=>{
      // 맨 앞 데이터 보정
      if(idx == 0) {
        res.period.push(this.parsePeriod(this.getViewBeforeStartDateStr(startDate, selectedRange), selectedRange));
        res.datas[0].data.push(this.sc.MIN_VALUE);
        res.datas[1].data.push(this.sc.MIN_VALUE);
        // res.datas[2].data.push(this.sc.MIN_VALUE);
        // this.cm.isDev() && console.log('date compare start','\n',new Date(v.period),'\n',startDate,'\n',this.dateService.isNotSame(v.period, startDate));
       if (this.rangeByTime(selectedRange)) {
           if (this.dateService.isNotSame(v.period, this.getViewStartDateStr(startDate))) {
               res.period.push(this.parsePeriod(this.getViewStartDateStr(startDate), selectedRange));
               res.datas[0].data.push(this.sc.MIN_VALUE);
               res.datas[1].data.push(this.sc.MIN_VALUE);
               // res.datas[2].data.push(this.sc.MIN_VALUE);
           }
       }
      }

      res.period.push(this.parsePeriod(v.period, selectedRange));
      res.datas[0].data.push(v.success);
      res.datas[1].data.push(v.unknown);
      // res.datas[2].data.push(v.fail);


      // 맨 끝 데이터 보정
      if(idx == lastIdx) {
        // this.cm.isDev() && console.log('date compare end','\n',new Date(v.period),'\n',endDate,'\n',this.dateService.isNotSame(v.period, endDate));
        if (this.rangeByTime(selectedRange)) {
            if (this.dateService.isNotSame(v.period, this.getViewEndDateStr(endDate, selectedRange))) {
                res.period.push(this.parsePeriod(this.getViewEndDateStr(endDate, selectedRange), selectedRange));
                res.datas[0].data.push(this.sc.MIN_VALUE);
                res.datas[1].data.push(this.sc.MIN_VALUE);
                // res.datas[2].data.push(this.sc.MIN_VALUE);
            }
        }

        res.period.push(this.parsePeriod(this.getViewAfterEndDateStr(endDate, selectedRange), selectedRange));
        res.datas[0].data.push(this.sc.MIN_VALUE);
        res.datas[1].data.push(this.sc.MIN_VALUE);
        // res.datas[2].data.push(this.sc.MIN_VALUE);
      }

      // resList.push(v);
      // return resList;
    },[]);
    return res;
  };

    parseCpuUsedResult = (serverNames, baseData, startDate: Date, endDate: Date, selectedRange: string) => {

        const res = {
            datas: []
            , period: []
        };


        const value = {
            label : serverNames,
            data : []
        };
        res.datas.push(value);



        if(this.cm.isEmpty(baseData)) {

            // 맨 앞 데이터 삽입
            res.period.push(this.parsePeriod(this.getViewStartDateStr(startDate), selectedRange));
            res.datas[0].data.push(this.sc.MIN_VALUE);

            // 중간 데이터 삽입
            res.period.push(this.parsePeriod(this.getViewMiddleDateStr(startDate, endDate), selectedRange));
            res.datas[0].data.push(this.sc.MIN_VALUE);

            // 맨 끝 데이터 삽입
            res.period.push(this.parsePeriod(this.getViewEndDateStr(endDate, selectedRange), selectedRange));
            res.datas[0].data.push(this.sc.MIN_VALUE);
            return res;
        }


        if (selectedRange !== 'time') {

            const dayInterval = this.getDateRange(startDate, endDate);

          //  console.log(dayInterval);

            let isExist = false;

            for (const time of dayInterval) {
                isExist = false;
                for (let i = 0; i < baseData.length; i++) {
                    const v: ResponseCpuUsedResult = baseData[i];
                    const p = this.parsePeriod(v.period, selectedRange);
                    if (time === p) {
                     //   console.log(v.period);
                        res.period.push(p);
                        res.datas[0].data.push(v.cpuUsed);
                        isExist = true;
                        break;
                    }
                }

                if (!isExist) {
                    res.period.push(this.parsePeriod(time, selectedRange));
                    res.datas[0].data.push(this.sc.MIN_VALUE);
                }
            }
        } else {

            // 시간검색 일때는 start와 end가 같아서.....갭을 구하기 위해서는 종료시간으로 변경해줘야한다.
            endDate = this.dateService.getDefaultEndDate(endDate);

            const timeInterval = this.getTimeRange(startDate, endDate);

            let isExist = false;

            for (const time of timeInterval) {
                isExist = false;
                for (let i = 0; i < baseData.length; i++) {
                    const v: ResponseCpuUsedResult = baseData[i];
                    const p = this.parsePeriod(v.period, selectedRange);
                    if (time === p) {
                     //   console.log(v.period);
                        res.period.push(p);
                        res.datas[0].data.push(v.cpuUsed);
                        isExist = true;
                        break;
                    }
                }

                if (!isExist) {
                    res.period.push(this.parsePeriod(time, selectedRange));
                    res.datas[0].data.push(this.sc.MIN_VALUE);
                }
            }
        }

        res.period = res.period.filter(function(item, pos) {
            return res.period.indexOf(item) === pos;
        });

        return res;
    };


    parseMemUsedResult = (serverNames, baseData, startDate: Date, endDate: Date, selectedRange: string) => {

        const res = {
            datas: []
            , period: []
        };

        const value = {
            label : serverNames,
            data : []
        };
        res.datas.push(value);


        if (this.cm.isEmpty(baseData)) {

            // 맨 앞 데이터 삽입
            res.period.push(this.parsePeriod(this.getViewStartDateStr(startDate), selectedRange));
            res.datas[0].data.push(this.sc.MIN_VALUE);

            // 중간 데이터 삽입
            res.period.push(this.parsePeriod(this.getViewMiddleDateStr(startDate, endDate), selectedRange));
            res.datas[0].data.push(this.sc.MIN_VALUE);

            // 맨 끝 데이터 삽입
            res.period.push(this.parsePeriod(this.getViewEndDateStr(endDate, selectedRange), selectedRange));
            res.datas[0].data.push(this.sc.MIN_VALUE);
            return res;
        }

        if (selectedRange !== 'time') {

            const dayInterval = this.getDateRange(startDate, endDate);

         //   console.log(dayInterval);

            let isExist = false;

            for (const time of dayInterval) {
                isExist = false;
                for (let i = 0; i < baseData.length; i++) {
                    const v: ResponseMemUsedResult = baseData[i];
                    const p = this.parsePeriod(v.period, selectedRange);
                    if (time === p) {
                    //    console.log(v.period);
                        res.period.push(p);
                        res.datas[0].data.push(this.unitValue(v.memUsage));
                        isExist = true;
                        break;
                    }
                }

                if (!isExist) {
                    res.period.push(this.parsePeriod(time, selectedRange));
                    res.datas[0].data.push(this.sc.MIN_VALUE);
                }
            }
        } else {

            // 시간검색 일때는 start와 end가 같아서.....갭을 구하기 위해서는 종료시간으로 변경해줘야한다.
            endDate = this.dateService.getDefaultEndDate(endDate);
            const timeInterval = this.getTimeRange(startDate, endDate);

            let isExist = false;

            for (const time of timeInterval) {
                isExist = false;
                for (let i = 0; i < baseData.length; i++) {
                    const v: ResponseMemUsedResult = baseData[i];
                    const p = this.parsePeriod(v.period, selectedRange);
                    if (time === p) {
                     //   console.log(v.period);
                        res.period.push(p);
                        res.datas[0].data.push(this.unitValue(v.memUsage));
                        isExist = true;
                        break;
                    }
                }

                if (!isExist) {
                    res.period.push(this.parsePeriod(time, selectedRange));
                    res.datas[0].data.push(this.sc.MIN_VALUE);
                }
            }
        }

        res.period = res.period.filter(function(item, pos) {
            return res.period.indexOf(item) === pos;
        });


        res.period = res.period.filter(function(item, pos) {
            return res.period.indexOf(item) === pos;
        });

        return res;
    };


    parseDiskUsedResult = (serverNames, baseData, startDate: Date, endDate: Date, selectedRange: string) => {

        const res = {
            datas: []
            , period: []
        };

        const value = {
            label : serverNames,
            data : []
        };
        res.datas.push(value);


        if(this.cm.isEmpty(baseData)) {

            // 맨 앞 데이터 삽입
            res.period.push(this.parsePeriod(this.getViewStartDateStr(startDate), selectedRange));
            res.datas[0].data.push(this.sc.MIN_VALUE);

            // 중간 데이터 삽입
            res.period.push(this.parsePeriod(this.getViewMiddleDateStr(startDate, endDate), selectedRange));
            res.datas[0].data.push(this.sc.MIN_VALUE);

            // 맨 끝 데이터 삽입
            res.period.push(this.parsePeriod(this.getViewEndDateStr(endDate, selectedRange), selectedRange));
            res.datas[0].data.push(this.sc.MIN_VALUE);
            return res;
        }


        if (selectedRange !== 'time') {

            const test = this.getDateRange(startDate, endDate);

           //  console.log(test);

            let isExist = false;

            for (const time of test) {
                isExist = false;
                for (let i = 0; i < baseData.length; i++) {
                    const v: ResponseDiskUsedResult = baseData[i];
                    const p = this.parsePeriod(v.period, selectedRange);
                    if (time === p) {
                       // console.log(v.period);
                        res.period.push(p);
                        res.datas[0].data.push(this.unitValue(v.diskUsage));
                        isExist = true;
                        break;
                    }
                }

                if (!isExist) {
                    res.period.push(this.parsePeriod(time, selectedRange));
                    res.datas[0].data.push(this.sc.MIN_VALUE);
                }
            }
        } else {

            // 시간검색 일때는 start와 end가 같아서.....갭을 구하기 위해서는 종료시간으로 변경해줘야한다.
            endDate = this.dateService.getDefaultEndDate(endDate);
            const timeInterval = this.getTimeRange(startDate, endDate);

            let isExist = false;

            for (const time of timeInterval) {
                isExist = false;
                for (let i = 0; i < baseData.length; i++) {
                    const v: ResponseDiskUsedResult = baseData[i];
                    const p = this.parsePeriod(v.period, selectedRange);
                    if (time === p) {
                       // console.log(v.period);
                        res.period.push(p);
                        res.datas[0].data.push(this.unitValue(v.diskUsage));
                        isExist = true;
                        break;
                    }
                }

                if (!isExist) {
                    res.period.push(this.parsePeriod(time, selectedRange));
                    res.datas[0].data.push(this.sc.MIN_VALUE);
                }
            }
        }

        res.period = res.period.filter(function(item, pos) {
            return res.period.indexOf(item) === pos;
        });

        return res;
    };




    // 차트 상세 값 보정 [s] ======================================================
  getViewBeforeStartDateStr = (_startDate, selectedRange): string=>{
    let res;
    if (this.rangeByTime(selectedRange)) {
      const s = this.dateService.getMinDate(_startDate);
      res = this.dateService.getAddHour(-1, s);

    } else {
      const s = this.dateService.getMinDate(_startDate);
      res = this.dateService.getAddDate(-1, s);
    }
    return this.dateService.format(res);
  };


  getViewNextDateStr = (_startDate, selectedRange): string=>{
        let res;
        let dateFormat = '';
        if (this.rangeByTime(selectedRange)) {
            dateFormat = this.dateService.format(_startDate);
            const s = new Date(dateFormat);
            res = this.dateService.getAddHour(1, s);

        } else {
            dateFormat = this.dateService.formatDay(_startDate);
            const s = this.dateService.getMinDate(new Date(dateFormat));
            res = this.dateService.getAddDate(1, s);
        }
        return this.dateService.format(res);
    };

  getViewStartDateStr = (_startDate): string=>{
    let res = this.dateService.getMinDate(_startDate);
    return this.dateService.format(res);
  };
  getViewMiddleDateStr = (_startDate, _endDate): string=>{
    const _middleDate = this.dateService.getMiddleDate(_startDate, _endDate);
    return this.dateService.format(_middleDate);
  };
  getViewEndDateStr = (_endDate, selectedRange): string=>{
    let res;
    if (this.rangeByTime(selectedRange)) {
      res = this.dateService.getMinDateEx(_endDate);
    } else {
      res = this.dateService.getMinDate(_endDate);
    }
    return this.dateService.format(res);
  };
  getViewAfterEndDateStr = (_endDate, selectedRange): string=>{
    let res;
    if (this.rangeByTime(selectedRange)) {
      const e = this.dateService.getMinDateEx(_endDate);
      res = this.dateService.getAddHour(1, e);
    } else {
      const e = this.dateService.getMinDate(_endDate);
      res = this.dateService.getAddDate(1, e);
    }
    return this.dateService.format(res);
  };
  parsePeriod = (period, selectedRange): string=>{
    let dateFormat: string ='';

     if (this.rangeByTime(selectedRange)) {
       dateFormat = this.dateService.format(period);
     }else{
       dateFormat = this.dateService.formatDay(period);
     }
    return dateFormat;
  };

  // 차트 날짜 보정 [s] ======================================================
  parseChartOptions = (max, stepSize, unit, format): any=>{
    let chartOptions = this.ddc.options;

    chartOptions.scales.yAxes[0].ticks.max = max;

    chartOptions.scales.xAxes[0].time.stepSize= stepSize;
    chartOptions.scales.xAxes[0].time.unit = unit;
    chartOptions.scales.xAxes[0].time.displayFormats = {};
    chartOptions.scales.xAxes[0].time.displayFormats[unit] = format;
    return chartOptions;
  };
  // 차트 날짜 보정 [e] ======================================================

  // 차트 옵션 값 계산 [s] =====================================================
  rangeByTime = (selectedRange): boolean=>selectedRange == this.rangeOption.time;
  rangeByDay = (selectedRange): boolean=>selectedRange == this.rangeOption.day;

  getUnit = (selectedRange): string=>{
    return this.rangeByTime(selectedRange) ? 'hour' : 'day';
  };
  getYAxesMaxTps = (baseData: ResponseTps[]): number=>{
    let max = this.sc.MIN_TICKS_LIMIT_EXPECT;
    baseData.forEach((v: ResponseTps, idx: number)=>{
      if(idx == 0) {
        max = v.tps;
      }
      if(max < v.tps) {
        max = v.tps;
      }
    },[]);
    // const calcMax = Math.ceil(max/this.sc.MAX_TICKS_LIMIT_EXPECT * 10) || this.sc.MIN_TICKS_LIMIT_EXPECT;
    const calcMax = this.cm.decimalAdjust('ceil', max/this.sc.MAX_TICKS_LIMIT_EXPECT * 10, -3) || this.sc.MIN_TICKS_LIMIT_EXPECT;
    return calcMax;
  };
  getYAxesMaxDialogResult = (baseData: ResponseDialogResult[]): number=>{
    const getMax = (v: ResponseDialogResult) => {
      let res = v.success;
      if(v.success < v.unknown) {
        res = v.unknown;
      }
      // if(res < v.fail) {
      //   res = v.fail;
      // }
      return res;
    };
    let max = this.sc.MIN_TICKS_LIMIT_EXPECT;
    baseData.forEach((v: ResponseDialogResult, idx: number)=>{
      const rowMax = getMax(v);
      if(idx == 0) {
        max = rowMax;
      }
      if(max < rowMax) {
        max = rowMax;
      }
    },[]);
    const calcMax = this.getMaxNum(Math.ceil(max/this.sc.MAX_TICKS_LIMIT_EXPECT * 10)) || this.sc.MIN_TICKS_LIMIT_EXPECT;
    return calcMax;
  };


  getYAxesMaxCpuUsedResult = (baseData: ResponseCpuUsedResult[]): number => {
        const getMax = (v: ResponseCpuUsedResult) => {
          let max = 0;

          //for (const vo of v) {
            if ( max < v.cpuUsed) {
              max = v.cpuUsed;
            }
          //}

            /**
             * max 값이 10단위로 떨어지지 않을 경우.
             * 그래프가 치솟는 케이스 발견
             * 그래서 10단위가 넘을 경우 max 치를 10 단위로 맞춰주도록함.
             */
            if (max >= 10 && max % 10  !== 0) {
                const t = max - (max % 10)  + 10;
                max = t;
            }
            return max;
        };

        let max = this.sc.MIN_TICKS_LIMIT_EXPECT;
        baseData.forEach((v: any, idx: number) => {
            const rowMax = getMax(v);
            if (idx === 0) {
                max = rowMax;
            }
            if (max < rowMax) {
                max = rowMax;
            }
        }, []);

        const calcMax = this.getMaxNum(Math.ceil(max / this.sc.MAX_TICKS_LIMIT_EXPECT * 10)) || this.sc.MIN_TICKS_LIMIT_EXPECT;
        return calcMax;
    };

     unitValue = (value) => {

         while (true) {
             value = value / 1024;
             if (value < 1024) {
                 break;
             }
         }

        value = Math.floor(value * 100) / 100;

        return value;
    };


    getYAxesMaxMemUsedResult = (baseData: ResponseMemUsedResult[]): number => {

        const getMax = (v: ResponseMemUsedResult) => {
            let max = 0;

            // for (const vo of v) {
            if ( max < v.memUsage) {
                max = v.memUsage;
            }
            //}
            return this.unitValue(max);
        };

        let max = this.sc.MIN_TICKS_LIMIT_EXPECT;
        baseData.forEach((v: any, idx: number) => {
            const rowMax = getMax(v);
            if (idx === 0) {
                max = rowMax;
            }
            if (max < rowMax) {
                max = rowMax;
            }
        }, []);

        const calcMax = this.getMaxNum(Math.ceil(max / this.sc.MAX_TICKS_LIMIT_EXPECT * 10)) || this.sc.MIN_TICKS_LIMIT_EXPECT;
        return calcMax;
    };



    getYAxesMaxDiskUsedResult = (baseData: ResponseDiskUsedResult[]): number => {

        const getMax = (v: ResponseDiskUsedResult) => {
            let max = 0;

            // for (const vo of v) {
            if ( max < v.diskUsage) {
                max = v.diskUsage;
            }
            // }
            return this.unitValue(max);
        };

        let max = this.sc.MIN_TICKS_LIMIT_EXPECT;
        baseData.forEach((v: any, idx: number) => {
            const rowMax = getMax(v);
            if (idx === 0) {
                max = rowMax;
            }
            if (max < rowMax) {
                max = rowMax;
            }
        }, []);

        const calcMax = this.getMaxNum(Math.ceil(max / this.sc.MAX_TICKS_LIMIT_EXPECT * 10)) || this.sc.MIN_TICKS_LIMIT_EXPECT;
        return calcMax;
    };


  getStepSize = (baseData: any[], selectedRange: string): number=>{
    let stepSize;
    if(this.rangeByTime(selectedRange)) {
      stepSize = this.sc.X_STEP_SIZE_EXPECT;
      return stepSize;
    }
    if(baseData.length <= this.sc.X_DATA_COUNT_EXPECT * 2) {
      stepSize = this.sc.X_STEP_SIZE_EXPECT;
      return stepSize;
    }
    if(baseData.length <= this.sc.X_DATA_COUNT_EXPECT * 3) {
      stepSize = 3;
      return stepSize;
    }
    stepSize = Math.floor((baseData.length)/this.sc.X_DATA_COUNT_EXPECT);
    return stepSize;
  };
  getFormat = (selectedRange): string=>{
    return this.rangeByTime(selectedRange) ? 'HH:mm' : 'MM/DD';
  };

  /**
   * 가장 큰 자리만 남기고 0으로 치환된 숫자를 반환.
   */
  getMaxNum = (num: number): number => {
    const e = num.toPrecision().split('');
    let remainIdx = 0;
    if(e.length > 3) {
      remainIdx = 1;
    }
    const res = e.map((v,i)=>i<=remainIdx?v:'0').join('');
    return parseInt(res);
  };
  // 차트 옵션 값 계산 [e] =====================================================


  getDateRange(startDate, endDate) {
      const dayInterval = 1000 * 60 * 60 * 24; // 1 day
      const duration = endDate - startDate;
      const steps = duration / dayInterval;
      return Array.from({length: steps+1}, (v,i) => this.dateService.formatDay(new Date(startDate.valueOf() + (dayInterval * i))));
  }

    getTimeRange(startDate, endDate) {
        const dayInterval = 1000 * 60 * 60; // 1 day
        const duration = endDate - startDate;
        const steps = duration / dayInterval;
        return Array.from({length: steps+1}, (v,i) => this.dateService.format(new Date(startDate.valueOf() + (dayInterval * i))));
    }
}
