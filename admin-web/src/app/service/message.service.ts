import { Injectable } from '@angular/core';
import { Cm_cd_detail } from '../adminsdk';
import { BaseResponse } from '../model/base-response';
import { getComponentViewDefinitionFactory } from '@angular/core/src/view';
import { Msg, lang } from '../model/message';

@Injectable()
export class MessageExService {
  constructor() {}
  private static getCode=error=>{
    let code = error.statusCode || error.code || '';
    if (code == '') {
      code = (error.hasOwnProperty('stack') ? 'ERROR' : 'UNKNOWN');
    }
    return code;
  };
  private static getMessage=error=>{
    let message = error.message || error.msg || '';
    return message;
  };
  static parseErrorMessage=error=>{
    // loopback
    // return (error ? `[${error.statusCode || (error.hasOwnProperty('stack') ? 'ERROR' : 'UNKNOWN')}] ${error.message}` : Msg.err[lang].failLoadData);
    // frontApi
    // return (error ? `[${error.code}] ${error.msg}` : 'Failed to get data.');
    if(!error) {
      return Msg.err[lang].failLoadData;
    }
    const code = MessageExService.getCode(error);
    const message = MessageExService.getMessage(error);
    return `[${code}] ${message}`;
  };
  
}
