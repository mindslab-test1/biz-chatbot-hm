import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import { CommonUtilService } from '.';
export interface HttpHeadersEx {
    contentType: string
  , authorization?: string
}

@Injectable()
export class ApiHelperService {
  constructor(
      @Inject(Http) protected http: Http
    , @Inject(HttpClient) protected httpClient: HttpClient
    , @Inject(CommonUtilService) protected cm: CommonUtilService
  ) {
    this.url = '';
  }

  url: string;

  HEADER_CONTENT_TYPE_JSON= 'application/json';
  HEADER_CONTENT_TYPE_DEFAULT = 'application/x-www-form-urlencoded';
  postJson(path: string, params: any, headers: HttpHeadersEx = {contentType: this.HEADER_CONTENT_TYPE_JSON}): Observable<any> {
    // this.cm.isDev() && console.log('debug ', `post json path: ${path}`);
    headers.contentType = this.HEADER_CONTENT_TYPE_JSON;
    return this._post(path, JSON.stringify(params), headers);
  }
  post(path: string, params: any, headers: HttpHeadersEx = {contentType: this.HEADER_CONTENT_TYPE_DEFAULT}): Observable<any> {
    // this.cm.isDev() && console.log('debug ',`post path: ${path}`);
    headers.contentType = this.HEADER_CONTENT_TYPE_DEFAULT;
    return this._post(path, this.parseHttpParams(params).toString(), headers);
  }
  protected _post(path: string, params: string, headers?: HttpHeadersEx): Observable<any> {
    if(this.cm.isEmpty(this.url)) {
      this.cm.isDev() && console.log('set host first');
      this.cm.override();
    }
    // this.cm.isDev() && console.log('debug ',`params: ${JSON.stringify(params)}, headers: ${JSON.stringify(headers)}`);
    return this.httpClient.post(
        `${this.url}${path}`
      , params
      , {
        headers: this.parseHttpHeader(headers)
      }
    );
  }

  /**
   * 
   * 기본은 post 요청을 위한 헤더이나 임의의 contentType 설정 가능.
   * @param contentType
   */
  parseHttpHeader(headers?: HttpHeadersEx): HttpHeaders {
    let header = new HttpHeaders();
    for(const param in headers){
      header = header.set(this._getKey(param), headers[param]);
    }
    return header;
  }
  protected _getKey = (param) =>{
    const headerKeys: HttpHeadersEx = {
        contentType: 'Content-Type'
      , authorization: 'Authorization'
    };
    const key = headerKeys[param] || param;
    return key;
  };

  /**
   * 
   * @param params object literal 로부터 HttpParams Return
   */
  parseHttpParams(params: any): HttpParams {
    let body = new HttpParams();
    for(const param in params){
      body = body.set(param, params[param]);
    }
    return body;
  }


  /**
   * apiHelper를 활용한 구현 예시..
   * @param pg 
   * @param pageSize 
   * @param domainIds 
   */
  protected example(pg, pageSize, domainIds): Observable<any> {
    // constructor 에 추가
    // private apiH: ApiHelperService

    // api host를 default 가 아닌 다른 곳으로 해서 쓰고 싶은 경우..
    // this.apiH.host = 'http://another-host.com';
      
    const body = {
        pg: pg
      , pageSize: pageSize
      , domainIds: domainIds
    };
    // return this.apiH.post('/admin/bqa/getsQuestion', body, 'application/x-www-form-urlencoded');
    return this.post('your api path', body, {contentType: 'application/x-www-form-urlencoded'});
  }

}
