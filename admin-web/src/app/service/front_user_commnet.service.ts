import {Injectable} from '@angular/core';
import {CommonUtilService} from './commonutil.service';
import {ChatbotDialogService} from './chatbot-dialog.service';
import {Observable} from 'rxjs';
import {DialogErrorChat} from '../view/container/dialogs/dialog-logs/dialog-error.component';
import {Front_user_commentApi} from '../adminsdk/services/custom';
import {SuggestChat} from '../view/container/add-ons/sug-op/suggestionOpinion.component';

@Injectable()
export class FrontUserCommentService {

    constructor(
        private cm: CommonUtilService
        , private dialogService: ChatbotDialogService
        , private api: Front_user_commentApi
    ) { }

    get SET_SEARCH_OPTIONS() {
        return {
              botName: { key: 'botName', label: '봇이름',  direction: 'asc'}
            , userName: { key: 'userName',  label: '사용자이름' ,  direction: 'asc'}
            , title: { key: 'title', label: '제목',  direction: 'asc'}
            , content :  { key: 'content',  label: '내용' ,  direction: 'asc'}
            , chType :  { key: 'chType',  label: '접속타입' ,  direction: 'asc'}
        };
    }

    getCountObs = (startDateStr, endDateStr, searchConditionJson, workspace_id): Observable<any> => {
        return this.api.getCommentCount(startDateStr, endDateStr, searchConditionJson, workspace_id);
    }

    getListObs = (pg, pgSize, startDateStr, endDateStr, searchConditionJson, workspace_id): Observable<SuggestChat[]> => {
        console.log(pgSize);
        return this.api.getCommentList(pg, pgSize, startDateStr, endDateStr, searchConditionJson, workspace_id);
    }

    getSearchConditionsJsonStr = (selectOption, searchStr) => {
        return JSON.stringify(this.getSearchConditions(selectOption, searchStr));
    }


    getSearchConditions = (selectOption, searchStr) => {
        const searchConditions = {};
        if (this.cm.isEmpty(selectOption)) {
            return searchConditions;
        }

        searchConditions[selectOption] = searchStr;
        return searchConditions;
    }

}
