import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Cm_userApi, Cm_workspace_team, Cm_team, Cm_user, Cm_workspace} from '../adminsdk';
import { CommonUtilService } from './commonutil.service';
import { environment as env } from '../environments/environment';
import {Subject} from '../../../node_modules/rxjs';


@Injectable()
export class UserService {
  public isSaveFlag: Boolean = true;
  private ROLE = {
    // ADMIN: 'ADMIN',
    // CS_ADMIN: 'CS_ADMIN',
    // CS_AGENT: 'CS_AGENT',
    // OPERATE_ADMIN: 'OPERATE_ADMIN',
    // OPERATE_TEMP: 'OPERATE_TEMP',
    // SERVICE_ADMIN: 'SERVICE_ADMIN',
    // SERVICE_TRAINER: 'SERVICE_TRAINER'
    ADMIN: 'ADMIN',
    CS_AGENT: 'CS_AGENT',
    OPERATOR: 'OPERATOR',
    SERVICE_ADMIN: 'SERVICE_ADMIN'
   };

  isLoadHeaderEvent: Subject<number> = new Subject<number>();
  isLoadTeamEvent: Subject<boolean> = new Subject<boolean>();

  constructor(
      private userApi: Cm_userApi
    , private cm: CommonUtilService
  ) {
  }

  findUser(): Observable<any> {
    return this.userApi.findById(this.getCurrentUserId());
  }
  getCurrentUserId(): any {
    return this.userApi.getCurrentId();
  }
  isAuthUser(): any {
    return this.userApi.isAuthenticated();
  }
  getUser(): any {
    return this.userApi.getCachedCurrent();
  }
  getCachedUserInfo() {
    const res = this.userApi.isAuthenticated()  && this.userApi.getCachedCurrent()  || null;
    return  res;
    // return this.loginApi.isAuthenticated() && this.loginApi.getCachedCurrent() || null;
  }

  getTeam() {
    const user: Cm_user = this.getCachedUserInfo();
    if (this.cm.isEmpty(user)) {
      return new Cm_team();
    }
    return user.team;
  }

  getWorkSpace() {
      const team: Cm_team = this.getTeam();
      if (this.cm.isEmpty(team)) {
          return [];
      }
      return team.workspaceRel.reduce((res, item: Cm_workspace_team) => {
          res.push(item.workspace_id);
          return res;
      }, []);
  }


  SUPERVISOR_TEAM_ID = 0;
  getTeamIdForSupervisor() {
    const res = this.getCachedUserInfo().role_id == this.ROLE.ADMIN;
    if (res) {
      return this.SUPERVISOR_TEAM_ID;
    } else {
      return this._getTeamId();
    }
  }

  getCurrentUserTeamId() {
    return this._getTeamId();
  }


  private _getTeamId = () => {
    const team: Cm_team = this.getTeam();
    if (this.cm.isEmpty(team)) {
      return -1;
    }
    return team.team_id;
  };
  getTeamId() {

    //TODO Supervisor 인 경우 헤더 셀렉트 박스의 값을 가져오도록 수정.
    const res = this.getCachedUserInfo().role_id == this.ROLE.ADMIN;
    if (res) {
      return parseInt(this.teamId);
    } else {
      return this._getTeamId();
    }
    // this.roleApi.isSupervisor(this.getCachedUserInfo().role_id).subscribe(res=>{
    //   if(res) {
    //     return [this.userService.teamId];
    //   } else {
    //     return _getId();
    //   }
    // }, err=>{
    //   console.log(MessageExService.parseErrorMessage(err));
    // });
  }
  getWorkspaceIds() {

    //TODO Supervisor 인 경우 헤더 셀렉트 박스의 값을 가져오도록 수정.
    const res = this.getCachedUserInfo().role_id == this.ROLE.ADMIN;
    if (res) {
      return [parseInt(this.workspaceId)];
    } else {
      return  this.getWorkSpace();
    }
    // this.roleApi.isSupervisor(this.getCachedUserInfo().role_id).subscribe(res=>{
    // if(res) {
    //   return [this.userService.workspaceId];
    // } else {
    //   return _getId();
    // }
    // }, err=>{
    //   console.log(MessageExService.parseErrorMessage(err));
    // });
  }
  public changeIsSaveFlag(isSave) {
    this.isSaveFlag = isSave;
  }

  private isAlreadyLogout() {
    return this.cm.isEmpty(this.userApi.getCurrentToken()) || this.cm.isEmpty(this.userApi.getCurrentToken().id);
  }
  logout(callback: () => void = () => {
     // this.cm.override();

  }, errorCallback: (err: any) => void = () => {
    //  this.cm.override();
    }): void {
    if (this.isAlreadyLogout()) {
      this.cm.isFunction(callback) && callback();
      return;
    }
      this.userApi.logout().subscribe(res => {
        this.cm.isFunction(callback) && callback();
      }, err => {

        this.cm.isFunction(errorCallback) && errorCallback(err);
      });
  }
  // logout(): Observable<any> {
  //   return this.loginApi.logout();
  // }

  // 화면 진입시 1회 호출 필요
  // login(username,password): Observable<any> {
  // }
  //for debug
  // login(username = 'admin',password = '1234'): Observable<any> {
  // }
  login(credentials): Observable<any> {
    credentials.ttl = env.ttl;
    return this.userApi.login(credentials);
  }

  checkConnectedUser(userId): Observable<any> {
    return this.userApi.checkConnectedUser(userId);
  }

  getLastLoginInfo(userId, workspace_id, team_id): Observable<any> {
    return this.userApi.lastLoginInfo(userId, workspace_id, team_id);
  }

  deleteCurrentUserToken(userId): Observable<any> {
    return this.userApi.deleteAccessTokens(userId);
  }

  get exclude() {
    return {
        teamId: 0
      , workspaceId: 0
    };
  }

  private localStorageKeyTeamId = 'teamId';
  private localStorageKeyWorkspaceId = 'workspaceId';

  get initTeamId() {
     return localStorage.getItem(this.localStorageKeyTeamId) || (this.getCurrentUserTeamId() + '');
  }

  get teamId() {
    return localStorage.getItem(this.localStorageKeyTeamId) || '';
  }
  set teamId(v) {
    //console.log('teamId Test :' + v);
    localStorage.setItem(this.localStorageKeyTeamId, v);
  }
  get workspaceId() {
    return localStorage.getItem(this.localStorageKeyWorkspaceId) || '';
  }
  set workspaceId(v) {
    localStorage.setItem(this.localStorageKeyWorkspaceId, v);
  }

  private clearTeamId(): void {
     localStorage.removeItem(this.localStorageKeyTeamId);
  }

  private clearWorkspaceId(): void {
    localStorage.removeItem(this.localStorageKeyWorkspaceId);
  }

  clearLocalUserInfoStoraage(): void {
    this.clearTeamId();
    this.clearWorkspaceId();
  }

  isLoaedHeaderEvent(num): void {
     this.isLoadHeaderEvent.next(num);
  }


  loadTeamEvent(): void {
    this.isLoadTeamEvent.next(true);
  }

  get getRoles() {
    return this.ROLE;
  }

  getUserRoleId() {
    const user: Cm_user = this.getCachedUserInfo();
    return user.role_id;
  }

}
