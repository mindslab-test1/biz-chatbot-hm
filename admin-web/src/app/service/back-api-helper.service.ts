import { Injectable, Inject } from '@angular/core';
import { environment as env } from '../environments/environment';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { UserService } from './user.service';
import { CommonUtilService } from '.';
import { ApiHelperService } from './api-helper.service';

@Injectable()
export class BackApiHelperService extends ApiHelperService {
  constructor(
      @Inject(Http) protected http: Http
    , @Inject(HttpClient) protected httpClient: HttpClient
    , @Inject(CommonUtilService) protected cm: CommonUtilService
    , @Inject(UserService) protected userSerivce: UserService
  ) {
    super(http, httpClient, cm);
    this.url = env.backendApiUrl;
  }

  api = env.BACK_API_PATH;

  addUserId(data, create:boolean){
    let userId = this.userSerivce.getCurrentUserId();
    if(create){
      data.createId = userId;
    }
    data.updateId = userId;
    return data;
  }



}
