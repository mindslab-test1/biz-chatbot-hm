import { Injectable } from '@angular/core';
import { Chatbot_sds_port_his, Chatbot_sds_port_hisApi, Dlg_history, Dlg_historyApi } from '../adminsdk';
import { Chatbot_sds_port_his_ex } from '../view/container/dialogs/dialog-logs/dialog-logs.component';
import { CommonUtilService } from '.';
import { ChatbotDialogService } from './chatbot-dialog.service';
import { DateService } from './date.service';
import { Speaker } from '../model/chatbot-message';
import { Observable } from 'rxjs';
import { Dlg_history_ex } from '../view/container/dialogs/dialog-logs/dialog-logs-detail.component';

@Injectable()
export class DialogLogService {

  constructor(
     private cm: CommonUtilService
    , private dialogService: ChatbotDialogService
    // , private _api: Chatbot_sds_port_hisApi
    , private api: Dlg_historyApi
    , private dateService: DateService
  ) { }

  get searchKey(){
    return 'search';
  }

  get teamIdKey(){
    return 'team_id';
  }

  get SET_SEARCH_OPTIONS(){
    return {
       session_id   : { key: 'session_id',    label: 'Session Id'   , direction: 'desc' }
      ,bot_name     : { key: 'bot_name',      label: 'Bot Name'     , direction: 'asc'  }
      ,user_id      : { key: 'user_id',       label: 'User Id'      , direction: 'asc'  }
      ,domain_count : { key: 'domain_count',  label: 'Domain Count' , direction: 'desc' }
      ,dialog_count : { key: 'dialog_count',  label: 'Dialog Count' , direction: 'desc' }
      ,unknown_count: { key: 'unknown_count', label: 'Unknown Count', direction: 'desc' }
      // ,sentence:      { key: 'sentence',      label: 'Sentence'      }
      // ,speaker:       { key: 'speaker',       label: 'Speaker'       }
    };
  }

  getCountObs=(startDateStr, endDateStr, searchConditionJson, teamId): Observable<any>=>{
  return this.api.getSessionCount(startDateStr, endDateStr, searchConditionJson, teamId);
  };
  getListObs=(pg, pgSize, startDateStr, endDateStr, searchConditionJson, teamId): Observable<Chatbot_sds_port_his_ex[]>=>{
    return this.api.getSessionList(pg, pgSize, startDateStr, endDateStr, searchConditionJson, teamId);
  };
  getItemObs=(startDateStr, endDateStr, selectOption, searchStr, session_id, team_id): Observable<Chatbot_sds_port_his_ex[]>=>{
    return this.api.getSessionList(1, 1,
      startDateStr, endDateStr,
      JSON.stringify(
        this.cm.prop(this.getSearchConditions(selectOption, searchStr), {session_id})
      ), team_id
    );
  };
  isUser = (item: Dlg_history)=> item.speaker == Speaker.USER;
  isChatbot = (item: Dlg_history)=> item.speaker == Speaker.CHATBOT;

  parseSearch=(v)=>{
    let startDate: Date, endDate: Date, selectOption: string, searchStr: string;

    let searchs = JSON.parse(v);

    if(this.cm.isNotEmpty(searchs['startDate'])){
      // startDate = new Date(searchs['startDate']);
      startDate = new Date(searchs['startDate'].replace(' ', this.dateService.delim.T));
    }
    if(this.cm.isNotEmpty(searchs['endDate'])){
      // endDate = new Date(searchs['endDate']);
      endDate = new Date(searchs['endDate'].replace(' ', this.dateService.delim.T));
    }
    for (const search in searchs) {
      if(search == 'startDate' || search == 'endDate'){
        continue;
      }
      if(this.cm.isNotNull(searchs[search])){
        selectOption = search;
        searchStr = searchs[search];
        break;
      }
    }
    return {
       startDate
      ,endDate
      ,selectOption
      ,searchStr
    };
  };
  getSearchConditions=(selectOption, searchStr)=>{
    let searchConditions = {};
    if(this.cm.isEmpty(selectOption)) {
      return searchConditions;
    }

    searchConditions[selectOption] = searchStr;

    // column order 서버단에서 정함.
    // this.orderDirection = this.SET_SEARCH_OPTIONS[this.selectOption].direction;
    return searchConditions;
  };
  getSearchConditionsJsonStr=(selectOption, searchStr)=>{
    return JSON.stringify(this.getSearchConditions(selectOption, searchStr));
  };
  getSearchJsonStr=(selectOption, searchStr, startDate, endDate)=>{
    return JSON.stringify(this.cm.prop(this.getSearchConditions(selectOption, searchStr),this.getDates(startDate, endDate)));
  };
  getDates=(startDate, endDate)=>{
    const dates = {
        startDate: this.dateService.getDateStrEx(startDate)
      , endDate: this.dateService.getDateStrEx(endDate)
    };
    return dates;
  };
  parseSearchConditions = (searchConditions): any[]=>{
    const parsedSearchConditions = [];
    for(const key in searchConditions) {
      const item = this._parseItem(searchConditions,key);
      //TODO key에 따라서 다른 식으로 넣게..
      let searchCondition = {};
      searchCondition[key] = item;
      parsedSearchConditions.push(searchCondition);
    }
    return parsedSearchConditions;
  };
  private _parseItem(searchConditions,key){
    switch(key){
    case this.SET_SEARCH_OPTIONS.session_id.key:
    case this.SET_SEARCH_OPTIONS.user_id.key:
      return searchConditions[key];
    case this.SET_SEARCH_OPTIONS.domain_count.key:
    case this.SET_SEARCH_OPTIONS.dialog_count.key:
    case this.SET_SEARCH_OPTIONS.unknown_count.key:
      return { gte: searchConditions[key] };
    case this.SET_SEARCH_OPTIONS.bot_name.key:
      return searchConditions[key];
    default:
      return searchConditions[key];
    }
  }

  parseChatbotSdsToVm=(result: Chatbot_sds_port_his[]): Chatbot_sds_port_his_ex[]=>{
    const resList: Chatbot_sds_port_his_ex[] = result.reduce((res,item: Chatbot_sds_port_his)=>{
      const botName = this._getBotName(item);
      const {domain_count, dialog_count, unknown_count} = this._getSessionCnts(item);
      const userId = this._getUserId(item);
      const v: Chatbot_sds_port_his_ex = {
         session_id: item.session_id
        ,bot_name: botName
        ,open_dtm: item.open_dtm
        ,close_dtm: item.close_dtm
        ,domain_count
        ,dialog_count
        ,unknown_count
        ,user_id: userId
      };
      res.push(v);
      return res;
    },[]);
    return resList;
  };
  private _getBotName=(item: Chatbot_sds_port_his)=>{
    if(this.cm.isEmpty(item)
        || this.cm.isEmpty(item.chatbot)
    ) {
      return '';
    }
    return item.chatbot.bot_name;
  };
  private _getUserId=(item: Chatbot_sds_port_his)=>{
    if(this.cm.isEmpty(item)
        || this.cm.isEmpty(item.chatSession)
        || this.cm.isEmpty(item.chatSession.dlgHistorys)
    ) {
      return '';
    }
    return item.chatSession.dlgHistorys[0].user_id;
  };
  c = this.dialogService.c;
  private _getSessionCnts=(item: Chatbot_sds_port_his)=>{
    let domain_count = 0, dialog_count = 0, unknown_count = 0;
    if(this.cm.isEmpty(item)
        || this.cm.isEmpty(item.chatSession)
        || this.cm.isEmpty(item.chatSession.dlgHistorys)
    ) {
      return {
        domain_count, dialog_count, unknown_count
      };
    }
    let domainNames = [];
    item.chatSession.dlgHistorys.map(dlg=>{
      if(this.cm.isNotEmpty(dlg.msg_type_cd)
          && dlg.msg_type_cd == this.c.MSG_TYPE_UNKNOWN
      ) {
        unknown_count++;
      }
      if(this.cm.isNotEmpty(dlg.domain_name)
          && (
              !domainNames.includes(dlg.domain_name)
          )
      ) {
        domainNames.push(dlg.domain_name);
        domain_count++;
      }
      dialog_count++;
    });
    return {
      domain_count, dialog_count, unknown_count
    };
  };

  setItems = (result: Dlg_history[], sessionInfos?)=>{
    const _getPrevItem = (idx): Dlg_history=>{
      return _getItem(idx-1);
    };
    const _getNextItem = (idx): Dlg_history=>{
      return _getItem(idx+1);
    };
    const _getItem = (idx): Dlg_history=>{
      const item: Dlg_history = result[idx];
      if(this.cm.isEmpty(item)) {
        return null;
      }
      return item;
    };
    const _getQuestion = (idx): string=>{
      const item: Dlg_history = _getPrevItem(idx);
      if(this.cm.isEmpty(item)) {
        return null;
      }
      return this.isUser(item) ? item.sentence : null;
    };
    const _getAnswer = (idx): string=>{
      const item: Dlg_history = _getNextItem(idx);
      if(this.cm.isEmpty(item)) {
        return null;
      }
      return this.isChatbot(item) ? item.sentence : null;
    };
    // const parseDLgHistEx = (item): Dlg_history_ex=>{
    //   const { bot_name, utter_id, session_id, timestamp, user_id, dialog_seq, question, answer, dlg_step, domain_name, msg_type_cd, content_json } = item;
    //   const v: Dlg_history_ex = {
    //       bot_name
    //     , utter_id
    //     , session_id
    //     , timestamp
    //     , user_id
    //     , dialog_seq
    //     , question
    //     , answer
    //     , dlg_step
    //     , domain_name
    //     , msg_type_cd
    //     , content_json
    //   };
    //   return v;
    // }
    const resList = result.reduce((res, item: Dlg_history, idx)=>{
      const bot_id = item.bot_id;
      const bot_name = this.cm.isEmpty(sessionInfos) ? '' : sessionInfos.find(_item=>_item.session_id = item.session_id).bot_name;
      if(this.isUser(item)){
        const answer = _getAnswer(idx);
        if(this.cm.isNull(answer)) {
          let {
              utter_id
            , session_id
            , timestamp
            , user_id
            , dialog_seq
            , dlg_step
            , domain_name
            , content_json
          } = item;
          // timestamp = this.dateService.parseLoopbackDateStrToDate(timestamp);
          const v: Dlg_history_ex = {
              bot_id
            , bot_name
            , utter_id
            , session_id
            , timestamp
            , user_id
            , dialog_seq
            , question: item.sentence
            , answer
            , dlg_step
            , domain_name
            , msg_type_cd: '답변 없음.'
            , content_json
          };

          res.push(v);
        }
      }
      if(this.isChatbot(item)){
        const question = _getQuestion(idx);
        const answer = item.sentence;
        let prevItem;
        if(this.cm.isNotNull(question)) {
          prevItem = _getPrevItem(idx);
          let {
              utter_id
            , session_id
            , timestamp
            , user_id
            , dialog_seq
          } = prevItem;
          const {
              dlg_step
            , domain_name
            , msg_type_cd
            , content_json
          } = item;
          // timestamp = this.dateService.parseLoopbackDateStrToDate(timestamp);
          const v: Dlg_history_ex = {
              bot_id
            , bot_name
            , utter_id
            , session_id
            , timestamp
            , user_id
            , dialog_seq
            , question
            , answer
            , dlg_step
            , domain_name
            , msg_type_cd
            , content_json
          };
          res.push(v);
        } else {
          let {
              utter_id
            , session_id
            , timestamp
            , user_id
            , dialog_seq
            , content_json
            , dlg_step
            , domain_name
            , msg_type_cd
          } = item;
          // timestamp = this.dateService.parseLoopbackDateStrToDate(timestamp);
          const v: Dlg_history_ex = {
              bot_id
            , bot_name
            , utter_id
            , session_id
            , timestamp
            , user_id
            , dialog_seq
            , question: ''
            , answer
            , dlg_step
            , domain_name
            , msg_type_cd
            , content_json
          };
          res.push(v);
        }
      }
      return res;
    },[]);
    return resList;
  };
}
