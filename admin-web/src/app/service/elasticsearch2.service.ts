import { Injectable } from '@angular/core';

import { Client } from 'elasticsearch';

@Injectable()
export class ElasticsearchService2 {

    private client: Client;


    constructor() {
        if (!this.client) {
            this.connect();
        }
    }

    private connect() {
        this.client = new Client({
            host: 'http://10.240.252.32:9200',
            log: 'trace'
        });

    }

    createIndex(name): any {
        return this.client.indices.create(name);
    }

    isAvailable(): any {
        return this.client.ping({
            requestTimeout: Infinity,
            body: 'Connect Test!'
        });
    }

    addToIndex(value): any {
        return this.client.create(value);
    }

    getAllCount(_index, query): any {
        return this.client.search({
            index: _index,
            //type: _type,
            body: query,
            filterPath: ['aggregations']
        });
    }

    getAllDocuments(_index, query, from, size): any {
        return this.client.search({
            index: _index,
            from: from,
            size: size,
            //type: _type,
            body: query,
            filterPath: ['hits.hits._source']
        });
    }

    getAllaggs(_index, query, from, size): any {
        return this.client.search({
            index: _index,
            from: from,
            size: size,
            //type: _type,
            body: query,
            filterPath: ['aggregations']
        });
    }
}
