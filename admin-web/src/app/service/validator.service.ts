import { Injectable } from '@angular/core';
import { CommonModalService} from './common-modal.service';
import { CommonUtilService } from './commonutil.service';
import { BaseResponse } from '../model/base-response';

@Injectable()
export class ValidatorService {

  constructor(
      private modal: CommonModalService
    , private cm: CommonUtilService
  ) { }

  validate=(data, Validator, extendFunc = ()=>{return true;})=>{
    // this.cm.isDev() &&
    // console.log('debug validate data, validator, ext', data, Validator, extendFunc);

    for(const id in Validator){
      const item = Validator[id];
      if (this.cm.isEmpty(data[id])) {
        if (item.required) {
          this.modal.openErrorAlert(`${id}를 입력해주십시오.`);
          return false;
        } else {
          continue;
        }
      }
      if (
           this.cm.isNotEmpty(item.minLength)
        && this.cm.isNotEmpty(item.maxLength)
        && item.minLength == item.maxLength
        && data[id].length != item.minLength
      ) {
        this.modal.openErrorAlert(`${id}를 ${item.minLength} 자리로 정확하게 입력해주십시오.`);
        return false;
      }
      if (this.cm.isNotEmpty(item.minLength) && data[id].length < item.minLength) {
        this.modal.openErrorAlert(`${id}를 ${item.minLength} 자리 이상 입력해주십시오.`);
        return false;
      }
      if (this.cm.isNotEmpty(item.maxLength) && data[id].length > item.maxLength) {
        this.modal.openErrorAlert(`${id}를 ${item.maxLength} 자리 이하 입력해주십시오.`);
        return false;
      }
    }

    if(!extendFunc()){
      return false;
    }

    return true;
  };
  isInvalid=(data, Validator, extendFunc = ()=>{return true;})=>{
    return !this.validate(data, Validator, extendFunc);
  };

  isInvalidResponse = (res: BaseResponse)=>{
    if(this.cm.isEmpty(res)
        || this.isInvalidCd(res.code)) {
      return true;
    }
    return false;
  };
  hasNoDataResponse = (res: BaseResponse)=>{
    if(this.cm.isEmpty(res.data)) {
      return true;
    }
    return false;
  };
  isInvalidCd = (code: number)=>code==1;
}
