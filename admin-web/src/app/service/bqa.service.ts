import {Injectable} from '@angular/core';
import {Http, ResponseContentType} from '@angular/http';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs/Observable';

import * as FileSaver from 'file-saver';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { UserService } from './user.service';


export class QaExcelVo {
  domainId: number;
  questionId: number;
  question: string;
  attribute1: string;
  attribute2: string;
  answerId: number;
  answer: string;
  userId: string;
}
export class RicahExcelVo {
  userId: string;
  questionId: number;
  type: string;
  btnOrder: string;
  btnTitle: string;
  btnUtter: number;
  imgText: string;
  imgLinkUrl: string;
  apiId: number;
}



@Injectable()
export class BasicQAService {
    private url: string;
    userId: string;
    constants = {
        CONTENT_TYPE: {
            BUTTON: 'RT0001',
            IMAGE: 'RT0002',
            CAROUSEL: 'RT0003',
            API: 'RT9001',
            NONE: 'NONE'
        },
        IMG_OPTION: {
            CONTAINER: 'sds-img',
            DEFAULT_IMG: '/assets/images/noimage.png'
        },
    };

    constructor(private http: Http,
                private httpClient: HttpClient,
                private userSerivce: UserService,
    ) {

        this.url = environment.backendApiUrl;
        this.userId = this.userSerivce.getCurrentUserId();
    }

    getUserId() {
        return this.userId;
    }

    public addUserId(data, create: boolean) {
        const userId = this.userSerivce.getCurrentUserId();
        if (create) {
            data.creatorId = userId;
        }
        data.updatorId = userId;
        return data;
    }

    public gets(pg, pageSize, ids, domainIds, type, search): Observable<any> {

        let idArray: any;

        if (domainIds === 0) {
            idArray = ids;
        } else {
            idArray = domainIds;
        }


        const body = new HttpParams()
            .set('pg', pg)
            .set('pgSize', pageSize)
            .set('type', type)
            .set('search', search)
            .set('domainIds', idArray);

        return this.httpClient.post(`${this.url}/admin/bqa/getsQuestion`, body.toString(), {
                headers: new HttpHeaders()
                    .set('Content-Type', 'application/x-www-form-urlencoded')
            }
        );
    }

    public getsExcel(domainId): Observable<any> {

        const body = new HttpParams()
            .set('domainId', domainId);

        return this.httpClient.post(`${this.url}/admin/bqa/getsExcel`, body.toString(), {
                headers: new HttpHeaders()
                    .set('Content-Type', 'application/x-www-form-urlencoded')
            }
        );
    }

    public insertExcel(data): Observable<any> {

        console.log('insertExcel', data);
        data = this.addUserId(data, false);
        return this.httpClient.post(`${this.url}/admin/bqa/insertExcel`, data, {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
        });
    }

    /*public insertBqaRichExcelfile(data): Observable<any> {
      console.log('insertBqaRichExcelfile', data);
      data = this.addUserId(data, false);
      return this.httpClient.post(`${this.url}/admin/bqa/rich/insertBqaRichExcelfile`, data, {
        headers: new HttpHeaders()
            .set('Content-Type', 'application/json')
      });
    }
  */

    public insertBqaRichExcelfile(data: FormData): Observable<any> {

        console.log('insertExcel', data);
        data = this.addUserId(data, false);

        const headers = new HttpHeaders();

        headers.append('enctype', 'multipart/form-data');
        headers.append('Access-Control-Allow-Headers', 'Authorization');

        return this.httpClient.post(`${this.url}/admin/bqa/rich/insertBqaRichExcelfile`, data, {
            headers: headers
        });
    }

    public deleteQna(data): Observable<any> {
        data = this.addUserId(data, false);
        return this.httpClient.post(`${this.url}/admin/bqa/deleteQna`, data, {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
        });
    }

    public deleteQnaDomainId(domainIds): Observable<any> {

        const body = new HttpParams()
            .set('domainIds', domainIds)
            .set('updatorId', this.userId);

        return this.httpClient.post(`${this.url}/admin/bqa/deleteQnaDomainId`, body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    public createQna(data): Observable<any> {
        data = this.addUserId(data, true);
        return this.httpClient.post(`${this.url}/admin/bqa/createQna`, data, {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
        });
    }

    public updateQna(data): Observable<any> {

        data = this.addUserId(data, false);
        return this.httpClient.post(`${this.url}/admin/bqa/updateQna`, data, {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
        });
    }

    public nlp(data): Observable<any> {

        const body = new HttpParams()
            .set('data', data);

        return this.httpClient.post(`${this.url}/admin/bqa/nlpTest`, body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    public uploadExcel(formData: FormData): Observable<any> {

        return this.httpClient.post(`${this.url}/excel/upload`, formData);
    }

    public downloadExcel(domainId): Observable<any> {

        return Observable.create(observer => {
            this.http.get(`${this.url}/excel/download?domainId=` + domainId, {responseType: ResponseContentType.Blob})
                .subscribe(response => {
                    const fileName = 'QnAList.xlsx';
                    FileSaver.saveAs(response.blob(), fileName);
                    observer.next(true);
                    observer.complete();
                }, error => {
                    observer.error(error);
                    observer.complete();
                });
        });
    }


    public domainIndexing(domainId): Observable<any> {

        const body = new HttpParams()
            .set('creatorId', this.userId)
            .set('domainId', domainId);

        return this.httpClient.post(`${this.url}/admin/bqa/domainIndexing`, body.toString(), {
                headers: new HttpHeaders()
                    .set('Content-Type', 'application/x-www-form-urlencoded')
            }
        );
    }

    public fullIndexing(): Observable<any> {

        const body = new HttpParams()
            .set('creatorId', this.userId);


        return this.httpClient.post(`${this.url}/admin/bqa/fullIndexing`, body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    public addIndexing(): Observable<any> {

        const body = new HttpParams()
            .set('creatorId', this.userId);

        return this.httpClient.post(`${this.url}/admin/bqa/addIndexing`, body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    public search(question, domainIds): Observable<any> {

        const body = new HttpParams()
            .set('question', question)
            .set('domainIds', domainIds);

        return this.httpClient.post(`${this.url}/bqa/search`, body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    public insertBqaRichContent(respCode, contentTypeCd, userId): Observable<any> {

        const body = new HttpParams()
            .set('respCode', respCode)
            .set('contentTypeCd', contentTypeCd)
            .set('userId', userId);

        return this.httpClient.post(`${this.url}/admin/bqa/rich/insertBqaRichContent`, body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    public deleteBqaRichContent(respCode, contentTypeCd): Observable<any> {

        const body = new HttpParams()
            .set('respCode', respCode)
            .set('contentTypeCd', contentTypeCd);

        return this.httpClient.post(`${this.url}/admin/bqa/rich/deleteBqaRichContent`, body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    public getBqaRichContent(respCode): Observable<any> {

        const body = new HttpParams()
            .set('respCode', respCode);

        return this.httpClient.post(`${this.url}/admin/bqa/rich/getBqaRichContent`, body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    public addButton(respCode, btnOrder, title, utter, userId): Observable<any> {

        const body = new HttpParams()
            .set('respCode', respCode)
            .set('btnOrder', btnOrder)
            .set('title', title)
            .set('utter', utter)
            .set('userId', userId);

        return this.httpClient.post(`${this.url}/admin/bqa/rich/addButton`, body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    public delButton(respCode, btnOrder): Observable<any> {

        const body = new HttpParams()
            .set('respCode', respCode)
            .set('btnOrder', btnOrder);

        return this.httpClient.post(`${this.url}/admin/bqa/rich/delButton`, body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    public addImage(respCode, imgText, imgLink, userId, file): Observable<any> {
        const formData = new FormData();
        formData.append('respCode', respCode);
        formData.append('imgText', imgText);
        formData.append('imgLink', imgLink);
        formData.append('userId', userId);
        formData.append('imgInput', file);

        return this.httpClient.post(`${this.url}/admin/bqa/rich/addImage`, formData);
    }

    public getBqaApiList(workspaceId): Observable<any> {

        const body = new HttpParams()
            .set('workspaceId', workspaceId);

        return this.httpClient.post(`${this.url}/admin/bqa/rich/getBqaApiList`, body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    public setBqaApi(respCode, rcId, userId): Observable<any> {

        const body = new HttpParams()
            .set('respCode', respCode)
            .set('rcId', rcId)
            .set('userId', userId);

        return this.httpClient.post(`${this.url}/admin/bqa/rich/setBqaApi`, body.toString(), {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }

    public getTeamList(): Observable<any> {
        return this.httpClient.post(`${this.url}/mobis/legacy/teamlist`, null, {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
        });

    }
}
