import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs';
import { LoopBackConfig, LoopBackAuth } from '../adminsdk';

@Injectable()
export class FileService {

  constructor(
    private http: Http,
    private auth: LoopBackAuth
  ) { }


  public upload(container: any, formData, options?): Observable<any> {
    let _url: string = `${LoopBackConfig.getPath()}/${LoopBackConfig.getApiVersion()}/containers/${container}/upload`;
    return this.http.post(`${_url}`, formData, options);
  }
  public uploadex(container: any, workspaceId: number, teamId: number, formData, options?): Observable<any> {
    let _url: string = `${LoopBackConfig.getPath()}/${LoopBackConfig.getApiVersion()}/containers/${container}/uploadex/${workspaceId}/${teamId}`;

      if (options == null) {
          options = new RequestOptions();
      }
      if (options.headers == null) {
          options.headers = new Headers();
      }
      if (this.auth.getAccessTokenId()) {
          options.headers.append(
              'Authorization',
              LoopBackConfig.getAuthPrefix() + this.auth.getAccessTokenId()
          );
      }

    return this.http.post(`${_url}`, formData, options);
  }
  public getDownloadPath(container: any, filePath): string {
    // return `${LoopBackConfig.getPath()}/${LoopBackConfig.getApiVersion()}/containers/${container}/download/${filePath}`;
    return `${LoopBackConfig.getPath()}/${LoopBackConfig.getApiVersion()}/containers/${container}/downloadex?p=${encodeURI(filePath)}`;
  }
  public getDownloadPathEx(container: any, workspaceId, teamId, filename): string {
    return `${LoopBackConfig.getPath()}/${LoopBackConfig.getApiVersion()}/containers/${container}/downloadex/${workspaceId}/${teamId}/${filename}`;
  }
}
