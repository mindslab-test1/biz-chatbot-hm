import { Injectable, Inject } from '@angular/core';
import { ApiHelperService, HttpHeadersEx } from './api-helper.service';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { CommonUtilService } from '.';
import { environment as env } from '../environments/environment';
import { Observable } from 'rxjs';
import { BaseResponse } from '../model/base-response';

@Injectable()
export class FrontApiHelperService extends ApiHelperService {

  constructor(
      @Inject(Http) protected http: Http
    , @Inject(HttpClient) protected httpClient: HttpClient
    , @Inject(CommonUtilService) protected cm: CommonUtilService
  ) { 
    super(http, httpClient, cm);
    this.url = env.frontApiUrl;
  }

  api = env.FRONT_API_PATH;
  
  postJson(path: string, params: any, headers: HttpHeadersEx = {contentType: this.HEADER_CONTENT_TYPE_JSON}): Observable<BaseResponse> {
    // this.cm.isDev() && console.log('debug ', `front post json path: ${path}, params: ${JSON.stringify(params)}, headers: ${JSON.stringify(headers)}`);
    headers.contentType = this.HEADER_CONTENT_TYPE_JSON;
    // headers.authorization = this.getToken();
    return this._post(path, JSON.stringify(params), headers);
  }

  getToken() {
    //TODO LATER 180619 현재는 토큰 미사용 중. 별도 토큰 호출 api
    return '';
  }

}
