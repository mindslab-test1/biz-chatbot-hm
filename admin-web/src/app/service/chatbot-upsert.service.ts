import { Injectable } from '@angular/core';
import {Chatbot_config, Chatbot_rich_content, Chatbot_rich_contentApi, Cm_domain, Chatbot_msg, Chatbot_msgApi, LoopBackFilter, Chatbot_infoApi, Chatbot_info, Chatbot_buttonsApi, Project, Chatbot_config_sds_domain_rel, Chatbot_configApi} from '../adminsdk';
import { CommonModalService, CommonUtilService } from '.';
import { ChatbotInfoService } from './chatbot-info.service';
import { Msg, lang } from '../model/message';

@Injectable()
export class ChatbotUpsertService {
  private url: string;
  constants = {
    URL: {
      chatbot_management: 'chatbot-management',
      chatbot_container: 'chatbot',
      chat_base: 'chat-base',
      c_preitf: 'c-preitf',
      c_bqa: 'c-bqa',
      c_scenario: 'c-scenario',
      c_test: 'c-test',
      channel_connection: 'channel-connection',

      dialogs_container: 'dialogs',
      dialogs_log: 'logs',
      dialogs_log_detail: 'detail'

    },
    MSG_TYPE_CD : {
      GREETING: 'MT0001',
      UNKNOWN: 'MT0002',
      SDSMSG: 'MT0003'
    },
    BTN_OPTION : {
      GREETING: 'greeting',
      UNKNOWN: 'unknown'
    },
    CONTENT_TYPE : {
      BUTTON: 'RT0001',
      IMAGE: 'RT0002'
    },
    NOTICE_OPTION : {
      NOTICE1: 'notice1',
      NOTICE2: 'notice2',
      NOTICE3: 'notice3',
      NOTICE4: 'notice4',
    },
    USE_YN : {
      Y: 'Y'
    }
  };
  c = this.constants;
  d = this.chatbotInfoService.default;

  constructor(
      private modal: CommonModalService
      , private cm: CommonUtilService
      , private chatbotInfoService: ChatbotInfoService
      , private api: Chatbot_infoApi
      , private msgApi: Chatbot_msgApi
      , private contentApi: Chatbot_rich_contentApi
      , private configApi: Chatbot_configApi
  ) {
    this.init();
  }
  init() {
  }
  isUpdate = (bot_id) => bot_id !== this.d.botId;


  // management list =====================================






  // basic & msg, btn ====================================
  insertBot(data: Chatbot_info, callback = (_res) => {}) {
    this.api.create(data).subscribe(res => {
      // this.cm.devLog('save res', res);
      if (!res) {
        this.modal.openErrorAlert(Msg.err[lang].failCreateData);
        return false;
      }
      callback(res);
    }, err => {
      this.modal.openErrorAlert(err);
    });
  }
  deleteBot(data: Chatbot_info, callback = () => {}) {
    const filter: LoopBackFilter = {
      where: {
        team_id: data.team_id
      }
    };

    if (this.isUpdate(data.bot_id)) {
      filter.where.bot_id = data.bot_id;
    }
    const delData = {
      use_yn: 'N'
    };
    this.api.upsertWithWhere(filter.where, delData).subscribe(res => {
      callback();
    }, err => {
      this.modal.openErrorAlert(err);
    });
  }
  updateBot(data: Chatbot_info, callback = () => {}) {

    const filter: LoopBackFilter = {
      where: {
        team_id: data.team_id
      }
    };

    if (this.isUpdate(data.bot_id)) {
      filter.where.bot_id = data.bot_id;
    }
    this.api.updateChatbot(filter.where, data).subscribe(res => {
      // this.cm.devLog('save res', res);

      callback();
    }, err => {
      this.modal.openErrorAlert(err);
    });
  }
  getBotData(bot_id, callback = (_res) => {}, errCallback = (err) => { this.cm.override(); }, complete = () => {}) {
    // this.cm.devLog('set bot_id',this.bot_id);
    // let result;
    this.api.findById(bot_id).subscribe((res: Chatbot_info) => {
      if (res.use_yn === 'N') {
        errCallback(res);
        return;
      }
      callback(res);
    }, err => {
      this.modal.openErrorAlert(err);
    }, () => {
      complete();
    });
  }


  addMsg(item: Chatbot_msg, callback) {
    this.msgApi.create(item).subscribe((res: Chatbot_msg) => {
      callback(res);
    }, err => {
      this.modal.openErrorAlert(err);
    });
  }
  updateMsg(item: Chatbot_msg, callback) {
    this.msgApi.updateAttributes(item.msg_id, item).subscribe((res: Chatbot_msg) => {
      callback(res);
    }, err => {
      this.modal.openErrorAlert(err);
    });
  }
  removeMsg(item: Chatbot_msg, bot_id: number,  callback) {
    this.msgApi.deleteByIds(item.msg_id, bot_id).subscribe((res) => {
      if (!(res && res['count'] > 0)) {
        this.modal.openErrorAlert(Msg.err[lang].failDeleteData);
        return false;
      }
      callback(item);
    }, err => {
      this.modal.openErrorAlert(err);
    });
  }
  getMsgsData(result: Chatbot_msg, callback) {
    if (this.cm.isEmpty(result)) {
      this.modal.openErrorAlert(Msg.err[lang].failLoadData);
      return false;
    }
    const _pk = 'msg_id';
    const filter: LoopBackFilter = {
      where: {
        bot_id: result.bot_id,
        msg_type_cd: result.msg_type_cd
      },
      order: `${_pk} asc`
    };
    this.msgApi.find(filter).subscribe((res: Chatbot_msg[]) => {
      console.log('debug get msg', res);
      callback(res);
    }, err => {
      this.modal.openErrorAlert(err);
    });
  }
  updateBotConfig(data: Chatbot_config, callback) {
    const filter: LoopBackFilter = {
      where: {
        conf_id: data.conf_id
      }
    };

    if (this.isUpdate(data.bot_id)) {
      filter.where.bot_id = data.bot_id;
    }
    this.configApi.upsertWithWhere(filter.where, data).subscribe(res => {
      // this.cm.devLog('save res', res);

      callback();
    }, err => {
      this.modal.openErrorAlert(err);
    });
  }















  // domain & sds domain ====================================


  upsertConfig(rc_id, item: Chatbot_config , callback) {
    const where = {
      rc_id
    };
    this.contentApi.upsertWithWhere(where, item)
        .subscribe((res: Chatbot_rich_content) => {
          callback(res);
        }, err => {
          this.modal.openErrorAlert(err);
        });
  }


  sortDomain(a: Cm_domain, b: Cm_domain) {
    return a.domain_id - b.domain_id;
  }
  sortSdsDomain(a: Chatbot_config_sds_domain_rel, b: Chatbot_config_sds_domain_rel) {
    return a.domain_id - b.domain_id;
  }
  equalDomain = (selectedBqa: Cm_domain, data: Cm_domain) => {
    if (this.cm.isEmpty(data)) {
      return false;
    }
    const res = data.domain_id === selectedBqa.domain_id;
    return res;
  };
  notEqualDomain = (selectedBqa: Cm_domain, data: Cm_domain) => !this.equalDomain(selectedBqa, data);
  equalSdsDomain = (s: Chatbot_config_sds_domain_rel, v: Project) => {
    if (this.cm.isEmpty(v)) {
      return false;
    }
    const res = v.SEQ_NO === s.domain_id;
    return res;
  };
  equalSdsDomainEx = (s: Chatbot_config_sds_domain_rel, v: Chatbot_config_sds_domain_rel) => {
    if (this.cm.isEmpty(v)) {
      return false;
    }
    const res = v.domain_id === s.domain_id;
    return res;
  };
  notEqualSdsDomainEx = (s: Chatbot_config_sds_domain_rel, v: Chatbot_config_sds_domain_rel) => !this.equalSdsDomainEx(s, v);

  // Data Filter
  filterDomain = (conf: Chatbot_config) => {
    if (this.cm.isEmpty(conf.domains)) {
      return Array<Cm_domain>();
    }
    const temp: Cm_domain[] = conf.domains;
    return temp;
  };
  filterSdsDomain = (conf: Chatbot_config) => {
    if (this.cm.isEmpty(conf.sdsDomains)) {
      return Array<Chatbot_config_sds_domain_rel>();
    }
    const temp: Chatbot_config_sds_domain_rel[] = conf.sdsDomains;
    return temp;
  };

  parseProjectToSdsDomain = (selectedItem: Project) => {
    if (this.cm.isEmpty(selectedItem)
        || selectedItem.SEQ_NO < 1
        || this.cm.isEmpty(selectedItem.PROJECT_NAME)
    ) {
      return null;
    }
    const { conf_id, domain_id, domain_name } = {
      conf_id: -1
      , domain_id: selectedItem.SEQ_NO
      , domain_name: selectedItem.PROJECT_NAME
    };
    const res: Chatbot_config_sds_domain_rel = new Chatbot_config_sds_domain_rel({
      conf_id, domain_id, domain_name
    });
    return res;
  }
}
