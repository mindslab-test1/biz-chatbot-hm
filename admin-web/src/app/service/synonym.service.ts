import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {environment} from '../environments/environment';
import {Observable} from 'rxjs/Observable';

export interface SynonymEntity {
  id: any;
  target: string;
  synonymlist: string;
}

@Injectable()
export class SynonymService {
  private url: string;

  constructor(private http: Http) {
    this.url = environment.synonymUrl;
  }

  public select(limit: number, page: number, column: string = null, order: string = null): Observable<any> {
    const requestParam = {
      limit: limit,
      page: page,
      sort: {
        column: column,
        order: order
      }
    };
    return this.http.post(`${this.url}/synonym/select`, JSON.stringify(requestParam))
      .map((response: Response) => {
        return response.json();
      });
  }

  public insert(target: string, synonymlist: string): Observable<any> {
    const requestParam = {
      data: {
        target: target,
        synonymlist: synonymlist
      }
    };
    return this.http.post(`${this.url}/synonym/insert`, JSON.stringify(requestParam))
      .map((response: Response) => {
        return response.json();
      });
  }

  public update(entity: SynonymEntity): Observable<any> {
    const requestParam = {
      data: {
        target: entity.target,
        synonymlist: entity.synonymlist
      },
      id: entity.id
    };
    return this.http.post(`${this.url}/synonym/update`, JSON.stringify(requestParam))
      .map((response: Response) => {
        return response.json();
      });
  }

  public delete(dellist: number[]): Observable<any> {
    const requestParam = {
      data: {
        dellist: dellist
      }
    };
    return this.http.post(`${this.url}/synonym/delete`, JSON.stringify(requestParam))
      .map((response: Response) => {
        return response.status === 200;
      });
  }

  public search(limit: number, page: number, column: string = null, order: string = null, searchText: string = null): Observable<any> {
    const requestParam = {
      limit: limit,
      page: page,
      sort: {
        column: column,
        order: order
      },
      searchText: searchText
    };
    return this.http.post(`${this.url}/synonym/search`, JSON.stringify(requestParam))
      .map((response: Response) => {
        return response.json();
      });
  }

  public checkDuplicate(id: any, synonym: string): Observable<any> {
    const requestParam = {
      data: {
        synonym: synonym
      },
      id: id
    };
    return this.http.post(`${this.url}/synonym/check`, JSON.stringify(requestParam))
      .map((response: Response) => {
        return response.json();
      });
  }
}
