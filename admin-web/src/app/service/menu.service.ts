import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {MenuItem} from 'primeng/primeng';
import { environment as env } from '../environments/environment';
import { LoopBackFilter, Cm_menuApi, Cm_menu } from '../adminsdk';
import { AuthService } from './auth.service';
import { CommonUtilService } from './commonutil.service';
import { Observable } from 'rxjs';

@Injectable()
export class MenuService {
  
  constructor(private router: Router,
      private menuApi: Cm_menuApi,
      private authService: AuthService,
      private cm: CommonUtilService,
      private title: Title) {
  }
  
  init(callback: () => void = ()=>{ this.cm.override(); }, errorCallback: (err: any) => void = ()=>{ this.cm.override(); }) {
    // this.cm.isDev() && console.log('menu init...');

    //cache
    if(this.cm.isNotEmpty(this.getResponseMenusFromLocalStorage())){
      this.setVisibleMenu();
      callback();
      return;
    }

    this.cm.isDev() && console.log('menu refresh...');
    this.getParentMenuData().subscribe(res => {
        this.setMenu(res);
        callback();
      }, error => {
        errorCallback(error);
      }
    );
  }

  private getParentMenuData(): Observable<any[]> {
    // get menu
    const defaultParentMenuId = this.ROOT_PARENT_MENU_ID;
    const filter: LoopBackFilter = {
      where: {parent_menu_id: defaultParentMenuId},
      order: 'order'
    };
    const obsGetMenu = this.menuApi.find(filter);
    return obsGetMenu;
  }

  getMenu() {
    //ResponseMenu 가 이미 캐싱 되었다고 가정.
    this.setVisibleMenu();
    let res = this.getVisibleMenu();
    return res;
  }
  private setMenu(menus) {
    this.setResponseMenu(menus);
    this.setVisibleMenu(); 
  }

  private getResponseMenus() {
    return this.menus.response;
  }
  private setResponseMenu(menus) {
    let responseMenus = env._.cloneDeep(this.default_menus_response);
    menus.map(menu => {
      if(!this.isVisibleMenu(menu)) {
        return false;
      }
      responseMenus[menu.menu_id] = menu;
    });
    this.menus.response = responseMenus;
    this.setResponseMenusToLocalStorage();
  }

  private localStorageKeyMenus = 'chatbotMenus';
  removeResponseMenusFromLocalStorage() {
    localStorage.removeItem(this.localStorageKeyMenus);
  }
  private getResponseMenusFromLocalStorage() {
    const menusJsonStr = localStorage.getItem(this.localStorageKeyMenus);
    if(menusJsonStr != null && this.cm.isJsonString(menusJsonStr)){
      return JSON.parse(menusJsonStr);
    } else {
      return this.default_menus_response;
    }
  }
  private setResponseMenusToLocalStorage() {
    this.removeResponseMenusFromLocalStorage();
    localStorage.setItem(this.localStorageKeyMenus, JSON.stringify(this.menus.response));
  }

  private isVisibleMenu = (menu: Cm_menu)=>{
    if (menu.visible_yn != 'Y') {
      // this.cm.isDev() && console.log('block. visible_yn = n menu ==>',menu);
      return false;
    }
    if (this.hasNotMenuRole(menu)) {
      // this.cm.isDev() && console.log('block. menu role not found. ==>',menu);
      return false;
    }
    if(!this.authService.checkMenuAuthRead(menu)) {
      // this.cm.isDev() && console.log('block. menu read auth xxx ==>',menu);
      return false;
    }
    return true;
  };

  private getVisibleMenu() {
    return this.menus.visible;
  }
  private setVisibleMenu() {
    // let tempNoAuthMenuId = -1;
    // const _setNoAuthMenuId = (menu) => {
    //   tempNoAuthMenuId = menu.menu_id;
    // };
    // const _getNoAuthMenuId = () => {
    //   return tempNoAuthMenuId;
    // };

    this.menus.visible = env._.cloneDeep(this.default_menus_visible);
    if (this.cm.isEmpty(this.getResponseMenus())) {
        this.menus.response = this.getResponseMenusFromLocalStorage();
    }
    const menus_response = this.getResponseMenus();
    for (const id of Object.keys(menus_response)) {
      const menu: Cm_menu = menus_response[id];

      //block
      if (this._isNotRootMenu(menu)) {
        // this.cm.isDev() && console.log('block. no parent menu ==>',menu);
        continue;
      }
      if(!this.isVisibleMenu(menu)) {
        continue;
      }

      const filteredMenu = this._filterMenusChildrenAuth(menu);

      this.menus.visible[filteredMenu.menu_id] = env._.merge({children: []}, filteredMenu);
      // this.cm.isDev() && console.log('debug temp menu add', this.menus.visible[filteredMenu.menu_id]);
    }

    this.menus.visible = env._.orderBy(this.menus.visible, ['order'], ['asc']);
  }
  private _filterMenusChildrenAuth = (menu: Cm_menu) => {

    if (this.cm.isEmpty(menu.children)) {
      return menu;
    }
    const filteredChildren = menu.children.reduce((result: Cm_menu[], childMenu: Cm_menu)=>{
      const res = this.authService.checkMenuAuthRead(childMenu);
      if(!res){
        return result;
      }
      if (childMenu.visible_yn != 'Y') {
        // this.cm.isDev() && console.log('block. menu visible_yn no. ==>',menu);
        return result;
      }

      // 재귀 해소 [s]
      childMenu = this._filterMenusChildrenAuth(childMenu);
      // 재귀 해소 [e]


      // 재귀 해소 [s]
      // if (this.cm.isEmpty(childMenu.children)) {
      //   result.push(childMenu);
      //   return result;
      // }
      // const filteredCchildren = childMenu.children.reduce((rresult, cchildMenu)=>{
      //   const rres = this.authService.checkMenuAuthRead(cchildMenu);
      //   if(!rres) {
      //     return rresult;
      //   }



      //   if (this.cm.isEmpty(cchildMenu.children)) {
      //     rresult.push(cchildMenu);
      //     return rresult;
      //   }
      //   const filteredCcchildren = cchildMenu.children.reduce((rrresult, ccchildMenu)=>{
      //     const rrres = this.authService.checkMenuAuthRead(ccchildMenu);
      //     if(!rrres) {
      //       return rrresult;
      //     }

      //     // 3 depth 이하는 구현 안 함.

      //     rrresult.push(ccchildMenu);
      //     return rrresult;
      //   },[]);
      //   cchildMenu.children = filteredCcchildren;




      //   rresult.push(cchildMenu);
      //   return rresult;
      // },[]);
      // childMenu.children = filteredCchildren;
      // 재귀 해소 [e]




      result.push(childMenu);
      return result;
    },[]);
    menu.children = filteredChildren;

    return menu;
  };

  getCurrentMenuId() {
    return this.menus.current;
  }
  setCurrentMenu(menu_id) {
    this.menus.current = menu_id;
  }

  getCurrentMenu(currPath) {
    const {res, menu} = this.findCurrentMenu(currPath);
    if (res) {
      this.setCurrentMenu(menu.menu_id);
      const menus = this.getCurrentMenuHierarchy();
      return menus;
    } else {
      return [];
    }
    // return [this.menus.current];
  }

  getMenuIds = (menuItems) : Array<Number> => {
    const default_menuIds:Array<Number> = [-1];
    const menuIds:Array<Number> = menuItems.reduce((res,item)=>{
      res.push(item.menu_id);
      return res;
    },[]) || default_menuIds;
    return menuIds;
  };

  findCurrentMenu(currPath) {
    let resultFalse = {res:false, menu:{}};

    const DELIM = '/';
    // this.cm.isDev() && console.log('debug set curr menu :: url => ', url);

    // if(menu_id) {
    //   this.menus.current = menu_id;
    //   //TODO set parent_menu_ids
    //   return true;
    // }

    const path = currPath.split(DELIM);
    if (path.length <= 1 || path[1] !== env.container) {
      return resultFalse;
    }

    const _getFullPath = (menu_path) => (env.root + env.container + menu_path);
    const _isCorrectPath = (menu_path) => {
      const fullPath = this.cm.isNotEmpty(menu_path) ? _getFullPath(menu_path) : '';
      return (fullPath == currPath) //일반
          || (currPath.startsWith(fullPath) && this.cm.isNotEmpty(fullPath))  //path 에 id 등이 붙은 경우
          ;
    };
    const _resolveChildren = (menu) => {
      let result = {res: false, menu};
      if(_isCorrectPath(menu.menu_path)){
        result.res = true;
        return result;
      } else {
        if (this.cm.isNotEmpty(menu.children)) {
          const children = menu.children;
          for (const childMenu of children) {
            const subItem = childMenu;
            const {res, menu} = _resolveChildren(subItem);
            if(res) {
              return {res, menu};
            }
          }
        }
      }
      return result;
    };

    // this.cm.isDev() && console.log('debug ', '====\n', 'url ==>', currPath, '\n', 'path ==>', path, '\n===============');

    this.menus.response = this.getResponseMenusFromLocalStorage();
    const menus_response = this.getResponseMenus();
    for (const id of Object.keys(menus_response)) {
      const item = menus_response[id];
      // const item = this.getMenusFromLocalStorage()[id];
      const {res, menu} = _resolveChildren(item);
      if(res) {
        return {res, menu};
        // break;
      } else {
        // this.cm.isDev() && console.log('debug', 'skip menu, item', menu, ':', item);
      }
    }
    return resultFalse;
  }

  private getCurrentMenuHierarchy() {
    const _isRoot = this._isRoot;
    const _isChildren = v => (!_isRoot(v));

    const _findCurrChild = (_menus, targetMenuId) => {
      let result = {};
      for (const menu of Object.keys(_menus)) {
        const item = _menus[menu];
        const {res, selected} = _resolveChildren(item, targetMenuId, 'menu_id');
        if(res) {
          result = selected;
          break;
        }
      }
      return result;
    };

    const _resolveChildren = (item, target, expected) => {
      if (target == item[expected]) {
        return {res: true, selected: item};
      } else {
        if (this.cm.isNotEmpty(item.children)) {
          const childrens = item.children;
          for (const child of childrens) {
            const subItem = child;
            const {res, selected} = _resolveChildren(subItem, target, expected);
            if(res) {
              return {res, selected};
            }
          }
        }
        return {res: false, selected:{}};
      }
    };
    const _getParent = (item,curr,parents) => {

      const {res, selected} = _resolveChildren(item, curr.parent_menu_id, 'menu_id');
      if(res) {
        parents.unshift(selected);
        if(_isRoot(selected.parent_menu_id)) {
          return {res:true, parents};
        } else {
          return _getParent(item, selected, parents);
        }
      }
      return {res:true, parents};
    };
    const _getParents = (curr, _menus, parents = []) => {
      // for ( const menu of Object.keys(_menus)){
      //   const item = _menus[menu];
      //   const {res, selected} = _resolveChildren(item, curr.parent_menu_id, 'menu_id');
      //   if(res) {
      //     parents.unshift(selected);
      //     if(_isRoot(selected.parent_menu_id)) {
      //       return parents;
      //     } else {

      //     }
      //   }

      // }

      // parents.push(curr.menu_id);

      const _parseParents = (_parents, curr, depth = 0) => {
        if(_isChildren(curr.parent_menu_id)) {
          let p = this._getMenuItem(curr.parent_menu_id);
          if(p){
            _parents.unshift(p);
            return _parseParents(_parents, p);
          }
        }
        return parents;
      };
      parents = _parseParents(parents, curr);
      return parents;
    };

    const _filterCurrHierarchy = (_menus) => {
      const res = [];
      const item = _findCurrChild(_menus, this.menus.current);
      if(this.cm.isEmpty(item)) {
        return res;
      }
      res.push(item);
      if (_isRoot(item['parent_menu_id'])) {
        return res;
      } else {
        const parents = _getParents(item, _menus);
        res.unshift(...parents);
        return res;
      }
    };
    this.menus.response = this.getResponseMenusFromLocalStorage();
    const res = _filterCurrHierarchy(this.getResponseMenus());
    return res;
  }

  changeMenu = (event) => {
    this.router.navigateByUrl(env.container + event.item.queryParams['path']);
  };

  setTitle(menuItems): void  {
    if (menuItems.length < 1) {
      this.title.setTitle('ADMIN');
    }

    let titleList = [];
    menuItems.map((menu: Cm_menu)=>{
      titleList.push(menu.name);
    });
    this.title.setTitle('ADMIN-' + titleList.join('>'));
  }
  getBreadcrumbItem(menuItems): MenuItem[]  {
    if (menuItems.length < 1) {
      return [];
    }

    const ret: MenuItem[] = [];
    menuItems.map((menu: Cm_menu)=>{
      let item = this.cm.isEmpty(menu.menu_path) ? {
        label: menu.name
      } : {
        label: menu.name,
        command: this.changeMenu,
        queryParams: {
          path: menu.menu_path
        }
      };
      ret.push(item);
    });
    return ret;
  }

  private ROOT_PARENT_MENU_ID = -1;
  private default_menus_response: any = {};
  private default_menus_visible: any = {};
  private menus: any = {
    response: this.default_menus_response,
    visible: this.default_menus_visible,
    current: -1
  };
  private _isRootMenu = menu => {
    const parentMenuId = parseInt(menu.parent_menu_id, 10);
    return this._isRoot(parentMenuId);
  };
  private _isNotRootMenu = menu => !this._isRootMenu(menu);
  private _isRoot = v => v == this.ROOT_PARENT_MENU_ID;
  private _getMenuItem = (curr_menu_id) => {
    // let allTree = this.getResponseMenusFromLocalStorage();//all
    let allTree = this.getResponseMenus();//all
    if (this.cm.isEmpty(allTree)) {
      console.log('no data response menu');
    }

    let p;
    for ( const menu_id of Object.keys(allTree)){
      const item = allTree[menu_id];
      if(item.menu_id == curr_menu_id){
        p = item;
        return p;
      }else{
        for( let i=0; item.children.length>0 && i<item.children.length; i++ ){
          if(item.children[i].menu_id == curr_menu_id){
          p = item.children[i];
            return p;
          }
        }
      }
    }
    if (!p) {
      return null; 
    }
  };
  private hasMenuRole = (menu: Cm_menu) => (this.cm.isNotEmpty(menu) && this.cm.isNotEmpty(menu.menuRoles));
  private hasNotMenuRole = (menu: Cm_menu) => (!this.hasMenuRole(menu));
}
