import {Injectable} from '@angular/core';
import {Observable} from '../../../node_modules/rxjs';
import {CommonUtilService} from './index';
import {ChatbotDialogService} from './chatbot-dialog.service';
import {Dlg_historyApi} from '../adminsdk';
import {DialogErrorChat} from '../view/container/dialogs/dialog-logs/dialog-error.component';

@Injectable()
export class DialogErrorService {

    constructor(
        private cm: CommonUtilService
        , private dialogService: ChatbotDialogService
        , private api: Dlg_historyApi
    ) { }



    get SET_SEARCH_OPTIONS() {
        return {
              utter_id:  { key: 'utter_id', label: 'No',  direction: 'desc'}
            , botName:  { key: 'botName',  label: 'BotName' ,  direction: 'asc'}
            , sentence: { key: 'sentence', label: 'Sentence',  direction: 'asc'}
            , user_id :  { key: 'user_id',  label: 'User ID' ,  direction: 'asc'}
        };
    }

    getCountObs = (startDateStr, endDateStr, searchConditionJson, teamId): Observable<any> => {
        return this.api.getDlgErrorCount(startDateStr, endDateStr, searchConditionJson, teamId);
    };

    getListObs = (pg, pgSize, startDateStr, endDateStr, searchConditionJson, teamId): Observable<DialogErrorChat[]> => {
     console.log(pgSize);
        return this.api.getDlgErrorList(pg, pgSize, startDateStr, endDateStr, searchConditionJson, teamId);
    };

    getSearchConditionsJsonStr = (selectOption, searchStr) => {
        return JSON.stringify(this.getSearchConditions(selectOption, searchStr));
    };


    getSearchConditions = (selectOption, searchStr) => {
        const searchConditions = {};
        if (this.cm.isEmpty(selectOption)) {
            return searchConditions;
        }

        searchConditions[selectOption] = searchStr;
        return searchConditions;
    }

}
