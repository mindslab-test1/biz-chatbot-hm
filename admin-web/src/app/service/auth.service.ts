import { Injectable } from '@angular/core';
import { LoopBackAuth, Cm_menuInterface, Cm_menu_role, Cm_user } from '../adminsdk';
import { CommonUtilService } from './commonutil.service';
import { UserService } from './user.service';


/**
 * via biz-web/..../auth.service.ts
 */
@Injectable()
export class AuthService {

  isUseSttTa = null;

  constructor(
    private cm: CommonUtilService,
    private userService: UserService,
    private auth: LoopBackAuth,
  ) {}

  private author = {
    crudx: '11111',
    rux: '01101',
    rx: '01001',
    block: '00000'
  };
  checkCrudx(crudx) {
    const author = this.author;
    let res = false;
    switch (crudx) {
      case author.crudx: res=true ; break;
      case author.rux  : res=true ; break;
      case author.rx   : res=true ; break;
      case author.block: res=false; break;
      default          : res=false; break;
    }
    return res;
  }

  checkMenuAuthRead(menu: Cm_menuInterface) {
    return this.checkMenuAuth(menu);
  }
  checkMenuAuth(menu: Cm_menuInterface) {
    let roles: Cm_menu_role[] = menu.menuRoles;

    // no roles
    if (this.cm.isEmpty(roles)) {
      return false;
    }

    const _check = (user: Cm_user = this.userService.getCachedUserInfo()) => {
      const type = "role_id";

      // check role

      if(user && user.role_id) {
        const role = roles.find(role=>{
          return role.role_id == user.role_id;
        });
        const checkres = this.checkCrudx(role.crudx);
        // this.cm.isDev() && console.log('debug check crudx ::'
        //     , '  check res ==>',checkres
        //     , '\nrole ==>',role
        //     , '\nuser ==>', user
        //     , '\nroles ==>', roles
        //     , '\nmenu ==>', menu
        // );

        return checkres;
      }
      return false;
    };
    return _check();
  }


  // authentication methods
  clear(): void {
    this.auth.clear();
  }
}
