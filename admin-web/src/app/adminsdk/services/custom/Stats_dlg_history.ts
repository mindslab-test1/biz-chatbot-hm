/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Response } from '@angular/http';
import { SDKModels } from './SDKModels';
import { BaseLoopBackApi } from '../core/base.service';
import { LoopBackConfig } from '../../lb.config';
import { LoopBackAuth } from '../core/auth.service';
import { LoopBackFilter,  } from '../../models/BaseModels';
import { JSONSearchParams } from '../core/search.params';
import { ErrorHandler } from '../core/error.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import { Stats_dlg_history } from '../../models/Stats_dlg_history';
import { SocketConnection } from '../../sockets/socket.connections';


/**
 * Api services for the `Stats_dlg_history` model.
 */
@Injectable()
export class Stats_dlg_historyApi extends BaseLoopBackApi {

  constructor(
    @Inject(Http) protected http: Http,
    @Inject(SocketConnection) protected connection: SocketConnection,
    @Inject(SDKModels) protected models: SDKModels,
    @Inject(LoopBackAuth) protected auth: LoopBackAuth,
    @Inject(JSONSearchParams) protected searchParams: JSONSearchParams,
    @Optional() @Inject(ErrorHandler) protected errorHandler: ErrorHandler
  ) {
    super(http,  connection,  models, auth, searchParams, errorHandler);
  }

  /**
   * Patch an existing model instance or insert a new one into the data source.
   *
   * @param {object} data Request data.
   *
   *  - `data` – `{object}` - Model instance data
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Stats_dlg_history` object.)
   * </em>
   */
  public patchOrCreate(data: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "PATCH";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/stats_dlg_histories";
    let _routeParams: any = {};
    let _postBody: any = {
      data: data
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Patch attributes for a model instance and persist it into the data source.
   *
   * @param {any} id stats_dlg_history id
   *
   * @param {object} data Request data.
   *
   *  - `data` – `{object}` - An object of model property name/value pairs
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Stats_dlg_history` object.)
   * </em>
   */
  public patchAttributes(id: any, data: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "PATCH";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/stats_dlg_histories/:id";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {
      data: data
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {number} bot_id 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Stats_dlg_history` object.)
   * </em>
   */
  public cachetest(bot_id: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/stats_dlg_histories/cachetest";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof bot_id !== 'undefined' && bot_id !== null) _urlParams.bot_id = bot_id;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {date} start_date 
   *
   * @param {date} end_date 
   *
   * @param {string} boat_Ids 
   *
   * @param {string} type 
   *
   * @param {string} step 
   *
   * @param {number} team_id 
   *
   * @param {number} bot_id 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Stats_dlg_history` object.)
   * </em>
   */
  public getTopN(start_date: any = {}, end_date: any = {}, boat_Ids: any = {}, type: any = {}, step: any = {}, team_id: any = {}, bot_id: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/stats_dlg_histories/getTopN";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof start_date !== 'undefined' && start_date !== null) _urlParams.start_date = start_date;
    if (typeof end_date !== 'undefined' && end_date !== null) _urlParams.end_date = end_date;
    if (typeof boat_Ids !== 'undefined' && boat_Ids !== null) _urlParams.boat_Ids = boat_Ids;
    if (typeof type !== 'undefined' && type !== null) _urlParams.type = type;
    if (typeof step !== 'undefined' && step !== null) _urlParams.step = step;
    if (typeof team_id !== 'undefined' && team_id !== null) _urlParams.team_id = team_id;
    if (typeof bot_id !== 'undefined' && bot_id !== null) _urlParams.bot_id = bot_id;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {date} start_date 
   *
   * @param {date} end_date 
   *
   * @param {string} boat_Ids 
   *
   * @param {number} team_id 
   *
   * @param {number} bot_id 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Stats_dlg_history` object.)
   * </em>
   */
  public getStats(start_date: any = {}, end_date: any = {}, boat_Ids: any = {}, team_id: any = {}, bot_id: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/stats_dlg_histories/getStats";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof start_date !== 'undefined' && start_date !== null) _urlParams.start_date = start_date;
    if (typeof end_date !== 'undefined' && end_date !== null) _urlParams.end_date = end_date;
    if (typeof boat_Ids !== 'undefined' && boat_Ids !== null) _urlParams.boat_Ids = boat_Ids;
    if (typeof team_id !== 'undefined' && team_id !== null) _urlParams.team_id = team_id;
    if (typeof bot_id !== 'undefined' && bot_id !== null) _urlParams.bot_id = bot_id;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {date} start_date 
   *
   * @param {date} end_date 
   *
   * @param {string} boat_Ids 
   *
   * @param {string} type 
   *
   * @param {number} team_id 
   *
   * @param {number} bot_id 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Stats_dlg_history` object.)
   * </em>
   */
  public getTpsChart(start_date: any = {}, end_date: any = {}, boat_Ids: any = {}, type: any = {}, team_id: any = {}, bot_id: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/stats_dlg_histories/getTpsChart";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof start_date !== 'undefined' && start_date !== null) _urlParams.start_date = start_date;
    if (typeof end_date !== 'undefined' && end_date !== null) _urlParams.end_date = end_date;
    if (typeof boat_Ids !== 'undefined' && boat_Ids !== null) _urlParams.boat_Ids = boat_Ids;
    if (typeof type !== 'undefined' && type !== null) _urlParams.type = type;
    if (typeof team_id !== 'undefined' && team_id !== null) _urlParams.team_id = team_id;
    if (typeof bot_id !== 'undefined' && bot_id !== null) _urlParams.bot_id = bot_id;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {date} start_date 
   *
   * @param {date} end_date 
   *
   * @param {string} boat_Ids 
   *
   * @param {string} type 
   *
   * @param {number} team_id 
   *
   * @param {number} bot_id 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Stats_dlg_history` object.)
   * </em>
   */
  public getDialogChart(start_date: any = {}, end_date: any = {}, boat_Ids: any = {}, type: any = {}, team_id: any = {}, bot_id: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/stats_dlg_histories/getDialogChart";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof start_date !== 'undefined' && start_date !== null) _urlParams.start_date = start_date;
    if (typeof end_date !== 'undefined' && end_date !== null) _urlParams.end_date = end_date;
    if (typeof boat_Ids !== 'undefined' && boat_Ids !== null) _urlParams.boat_Ids = boat_Ids;
    if (typeof type !== 'undefined' && type !== null) _urlParams.type = type;
    if (typeof team_id !== 'undefined' && team_id !== null) _urlParams.team_id = team_id;
    if (typeof bot_id !== 'undefined' && bot_id !== null) _urlParams.bot_id = bot_id;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Stats_dlg_history` object.)
   * </em>
   */
  public clearCache(customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/stats_dlg_histories/clearCache";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * The name of the model represented by this $resource,
   * i.e. `Stats_dlg_history`.
   */
  public getModelName() {
    return "Stats_dlg_history";
  }
}
