/* tslint:disable */
import { Injectable } from '@angular/core';
import { Cm_cd_group } from '../../models/Cm_cd_group';
import { Cm_cd_detail } from '../../models/Cm_cd_detail';
import { Cm_menu_role } from '../../models/Cm_menu_role';
import { Cm_role } from '../../models/Cm_role';
import { Cm_login_history } from '../../models/Cm_login_history';
import { Cm_menu } from '../../models/Cm_menu';
import { Cm_team } from '../../models/Cm_team';
import { Cm_user } from '../../models/Cm_user';
import { Cm_workspace } from '../../models/Cm_workspace';
import { Cm_workspace_team } from '../../models/Cm_workspace_team';
import { Notification } from '../../models/Notification';
import { Engine } from '../../models/Engine';
import { LoopConfigFile } from '../../models/LoopConfigFile';
import { Grpc } from '../../models/Grpc';
import { Statistics } from '../../models/Statistics';
import { Cf_dnn } from '../../models/Cf_dnn';
import { Cf_simple } from '../../models/Cf_simple';
import { Chatbot_buttons } from '../../models/Chatbot_buttons';
import { Chatbot_config_domain_rel } from '../../models/Chatbot_config_domain_rel';
import { Chatbot_config_sds_domain_rel } from '../../models/Chatbot_config_sds_domain_rel';
import { Chatbot_config } from '../../models/Chatbot_config';
import { Chatbot_info } from '../../models/Chatbot_info';
import { Chatbot_msg } from '../../models/Chatbot_msg';
import { Chatbot_rich_content } from '../../models/Chatbot_rich_content';
import { Chatbot_rich_image } from '../../models/Chatbot_rich_image';
import { Cm_chatbot } from '../../models/Cm_chatbot';
import { Cm_domain } from '../../models/Cm_domain';
import { Container } from '../../models/Container';
import { Cm_domain_child } from '../../models/Cm_domain_child';
import { Cm_synonym } from '../../models/Cm_synonym';
import { Srch_morpheme } from '../../models/Srch_morpheme';
import { Srch_stopword } from '../../models/Srch_stopword';
import { Srch_autocomplete } from '../../models/Srch_autocomplete';
import { Srch_spellcheck } from '../../models/Srch_spellcheck';
import { Srch_related } from '../../models/Srch_related';
import { Srch_popular } from '../../models/Srch_popular';
import { Srch_ranking } from '../../models/Srch_ranking';
import { Srch_static } from '../../models/Srch_static';
import { Dlg_history } from '../../models/Dlg_history';
import { Chatbot_sds_port_his } from '../../models/Chatbot_sds_port_his';
import { Chatbot_session_rel } from '../../models/Chatbot_session_rel';
import { Project } from '../../models/Project';
import { If_api } from '../../models/If_api';
import { If_api_result_mapping } from '../../models/If_api_result_mapping';
import { If_api_params } from '../../models/If_api_params';
import { If_api_json_template } from '../../models/If_api_json_template';
import { Itf_sds } from '../../models/Itf_sds';
import { Itf_intent_domain_rel } from '../../models/Itf_intent_domain_rel';
import { Itf_corpus_category } from '../../models/Itf_corpus_category';
import { Itf_prebqa } from '../../models/Itf_prebqa';
import { Stats_chatbot_info } from '../../models/Stats_chatbot_info';
import { Stats_dlg_history } from '../../models/Stats_dlg_history';
import { Qa_indexing_history } from '../../models/Qa_indexing_history';
import { Monitor_server } from '../../models/Monitor_server';
import { Front_user_comment } from '../../models/Front_user_comment';
import { Nlp_synonym } from '../../models/Nlp_synonym';
import { Nlp_user_dic } from '../../models/Nlp_user_dic';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    Cm_cd_group: Cm_cd_group,
    Cm_cd_detail: Cm_cd_detail,
    Cm_menu_role: Cm_menu_role,
    Cm_role: Cm_role,
    Cm_login_history: Cm_login_history,
    Cm_menu: Cm_menu,
    Cm_team: Cm_team,
    Cm_user: Cm_user,
    Cm_workspace: Cm_workspace,
    Cm_workspace_team: Cm_workspace_team,
    Notification: Notification,
    Engine: Engine,
    LoopConfigFile: LoopConfigFile,
    Grpc: Grpc,
    Statistics: Statistics,
    Cf_dnn: Cf_dnn,
    Cf_simple: Cf_simple,
    Chatbot_buttons: Chatbot_buttons,
    Chatbot_config_domain_rel: Chatbot_config_domain_rel,
    Chatbot_config_sds_domain_rel: Chatbot_config_sds_domain_rel,
    Chatbot_config: Chatbot_config,
    Chatbot_info: Chatbot_info,
    Chatbot_msg: Chatbot_msg,
    Chatbot_rich_content: Chatbot_rich_content,
    Chatbot_rich_image: Chatbot_rich_image,
    Cm_chatbot: Cm_chatbot,
    Cm_domain: Cm_domain,
    Container: Container,
    Cm_domain_child: Cm_domain_child,
    Cm_synonym: Cm_synonym,
    Srch_morpheme: Srch_morpheme,
    Srch_stopword: Srch_stopword,
    Srch_autocomplete: Srch_autocomplete,
    Srch_spellcheck: Srch_spellcheck,
    Srch_related: Srch_related,
    Srch_popular: Srch_popular,
    Srch_ranking: Srch_ranking,
    Srch_static: Srch_static,
    Dlg_history: Dlg_history,
    Chatbot_sds_port_his: Chatbot_sds_port_his,
    Chatbot_session_rel: Chatbot_session_rel,
    Project: Project,
    If_api: If_api,
    If_api_result_mapping: If_api_result_mapping,
    If_api_params: If_api_params,
    If_api_json_template: If_api_json_template,
    Itf_sds: Itf_sds,
    Itf_intent_domain_rel: Itf_intent_domain_rel,
    Itf_corpus_category: Itf_corpus_category,
    Itf_prebqa: Itf_prebqa,
    Stats_chatbot_info: Stats_chatbot_info,
    Stats_dlg_history: Stats_dlg_history,
    Qa_indexing_history: Qa_indexing_history,
    Monitor_server: Monitor_server,
    Front_user_comment: Front_user_comment,
    Nlp_synonym: Nlp_synonym,
    Nlp_user_dic: Nlp_user_dic,

  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
