/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Response } from '@angular/http';
import { SDKModels } from './SDKModels';
import { BaseLoopBackApi } from '../core/base.service';
import { LoopBackConfig } from '../../lb.config';
import { LoopBackAuth } from '../core/auth.service';
import { LoopBackFilter,  } from '../../models/BaseModels';
import { JSONSearchParams } from '../core/search.params';
import { ErrorHandler } from '../core/error.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import { Dlg_history } from '../../models/Dlg_history';
import { SocketConnection } from '../../sockets/socket.connections';


/**
 * Api services for the `Dlg_history` model.
 */
@Injectable()
export class Dlg_historyApi extends BaseLoopBackApi {

  constructor(
    @Inject(Http) protected http: Http,
    @Inject(SocketConnection) protected connection: SocketConnection,
    @Inject(SDKModels) protected models: SDKModels,
    @Inject(LoopBackAuth) protected auth: LoopBackAuth,
    @Inject(JSONSearchParams) protected searchParams: JSONSearchParams,
    @Optional() @Inject(ErrorHandler) protected errorHandler: ErrorHandler
  ) {
    super(http,  connection,  models, auth, searchParams, errorHandler);
  }

  /**
   * Patch an existing model instance or insert a new one into the data source.
   *
   * @param {object} data Request data.
   *
   *  - `data` – `{object}` - Model instance data
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Dlg_history` object.)
   * </em>
   */
  public patchOrCreate(data: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "PATCH";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/dlg_histories";
    let _routeParams: any = {};
    let _postBody: any = {
      data: data
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * Patch attributes for a model instance and persist it into the data source.
   *
   * @param {any} id dlg_history id
   *
   * @param {object} data Request data.
   *
   *  - `data` – `{object}` - An object of model property name/value pairs
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Dlg_history` object.)
   * </em>
   */
  public patchAttributes(id: any, data: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "PATCH";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/dlg_histories/:id";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {
      data: data
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {date} start_date 
   *
   * @param {date} end_date 
   *
   * @param {string} searchOptions 
   *
   * @param {string} team_id 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Dlg_history` object.)
   * </em>
   */
  public getSessionCount(start_date: any = {}, end_date: any = {}, searchOptions: any = {}, team_id: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/dlg_histories/getSessionCount";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof start_date !== 'undefined' && start_date !== null) _urlParams.start_date = start_date;
    if (typeof end_date !== 'undefined' && end_date !== null) _urlParams.end_date = end_date;
    if (typeof searchOptions !== 'undefined' && searchOptions !== null) _urlParams.searchOptions = searchOptions;
    if (typeof team_id !== 'undefined' && team_id !== null) _urlParams.team_id = team_id;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {number} pg 
   *
   * @param {number} pg_size 
   *
   * @param {date} start_date 
   *
   * @param {date} end_date 
   *
   * @param {string} searchOptions 
   *
   * @param {string} team_id 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Dlg_history` object.)
   * </em>
   */
  public getSessionList(pg: any, pg_size: any, start_date: any = {}, end_date: any = {}, searchOptions: any = {}, team_id: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/dlg_histories/getSessionList";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof pg !== 'undefined' && pg !== null) _urlParams.pg = pg;
    if (typeof pg_size !== 'undefined' && pg_size !== null) _urlParams.pg_size = pg_size;
    if (typeof start_date !== 'undefined' && start_date !== null) _urlParams.start_date = start_date;
    if (typeof end_date !== 'undefined' && end_date !== null) _urlParams.end_date = end_date;
    if (typeof searchOptions !== 'undefined' && searchOptions !== null) _urlParams.searchOptions = searchOptions;
    if (typeof team_id !== 'undefined' && team_id !== null) _urlParams.team_id = team_id;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {date} start_date 
   *
   * @param {date} end_date 
   *
   * @param {string} searchOptions 
   *
   * @param {string} team_id 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Dlg_history` object.)
   * </em>
   */
  public getDlgErrorCount(start_date: any = {}, end_date: any = {}, searchOptions: any = {}, team_id: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/dlg_histories/getDlgErrorCount";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof start_date !== 'undefined' && start_date !== null) _urlParams.start_date = start_date;
    if (typeof end_date !== 'undefined' && end_date !== null) _urlParams.end_date = end_date;
    if (typeof searchOptions !== 'undefined' && searchOptions !== null) _urlParams.searchOptions = searchOptions;
    if (typeof team_id !== 'undefined' && team_id !== null) _urlParams.team_id = team_id;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {number} pg 
   *
   * @param {number} pg_size 
   *
   * @param {date} start_date 
   *
   * @param {date} end_date 
   *
   * @param {string} searchOptions 
   *
   * @param {string} team_id 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * <em>
   * (The remote method definition does not provide any description.
   * This usually means the response is a `Dlg_history` object.)
   * </em>
   */
  public getDlgErrorList(pg: any, pg_size: any, start_date: any = {}, end_date: any = {}, searchOptions: any = {}, team_id: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/dlg_histories/getDlgErrorList";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof pg !== 'undefined' && pg !== null) _urlParams.pg = pg;
    if (typeof pg_size !== 'undefined' && pg_size !== null) _urlParams.pg_size = pg_size;
    if (typeof start_date !== 'undefined' && start_date !== null) _urlParams.start_date = start_date;
    if (typeof end_date !== 'undefined' && end_date !== null) _urlParams.end_date = end_date;
    if (typeof searchOptions !== 'undefined' && searchOptions !== null) _urlParams.searchOptions = searchOptions;
    if (typeof team_id !== 'undefined' && team_id !== null) _urlParams.team_id = team_id;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * The name of the model represented by this $resource,
   * i.e. `Dlg_history`.
   */
  public getModelName() {
    return "Dlg_history";
  }
}
