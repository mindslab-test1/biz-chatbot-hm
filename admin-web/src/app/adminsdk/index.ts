/**
* @module SDKModule
* @author Jonathan Casarrubias <t:@johncasarrubias> <gh:jonathan-casarrubias>
* @license MIT 2016 Jonathan Casarrubias
* @version 2.1.0
* @description
* The SDKModule is a generated Software Development Kit automatically built by
* the LoopBack SDK Builder open source module.
*
* The SDKModule provides Angular 2 >= RC.5 support, which means that NgModules
* can import this Software Development Kit as follows:
*
*
* APP Route Module Context
* ============================================================================
* import { NgModule }       from '@angular/core';
* import { BrowserModule }  from '@angular/platform-browser';
* // App Root
* import { AppComponent }   from './app.component';
* // Feature Modules
* import { SDK[Browser|Node|Native]Module } from './shared/sdk/sdk.module';
* // Import Routing
* import { routing }        from './app.routing';
* @NgModule({
*  imports: [
*    BrowserModule,
*    routing,
*    SDK[Browser|Node|Native]Module.forRoot()
*  ],
*  declarations: [ AppComponent ],
*  bootstrap:    [ AppComponent ]
* })
* export class AppModule { }
*
**/
import { JSONSearchParams } from './services/core/search.params';
import { ErrorHandler } from './services/core/error.service';
import { LoopBackAuth } from './services/core/auth.service';
import { LoggerService } from './services/custom/logger.service';
import { SDKModels } from './services/custom/SDKModels';
import { InternalStorage, SDKStorage } from './storage/storage.swaps';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CookieBrowser } from './storage/cookie.browser';
import { StorageBrowser } from './storage/storage.browser';
import { SocketBrowser } from './sockets/socket.browser';
import { SocketDriver } from './sockets/socket.driver';
import { SocketConnection } from './sockets/socket.connections';
import { RealTime } from './services/core/real.time';
import { Cm_cd_groupApi } from './services/custom/Cm_cd_group';
import { Cm_cd_detailApi } from './services/custom/Cm_cd_detail';
import { Cm_menu_roleApi } from './services/custom/Cm_menu_role';
import { Cm_roleApi } from './services/custom/Cm_role';
import { Cm_login_historyApi } from './services/custom/Cm_login_history';
import { Cm_menuApi } from './services/custom/Cm_menu';
import { Cm_teamApi } from './services/custom/Cm_team';
import { Cm_userApi } from './services/custom/Cm_user';
import { Cm_workspaceApi } from './services/custom/Cm_workspace';
import { Cm_workspace_teamApi } from './services/custom/Cm_workspace_team';
import { NotificationApi } from './services/custom/Notification';
import { EngineApi } from './services/custom/Engine';
import { LoopConfigFileApi } from './services/custom/LoopConfigFile';
import { GrpcApi } from './services/custom/Grpc';
import { StatisticsApi } from './services/custom/Statistics';
import { Cf_dnnApi } from './services/custom/Cf_dnn';
import { Cf_simpleApi } from './services/custom/Cf_simple';
import { Chatbot_buttonsApi } from './services/custom/Chatbot_buttons';
import { Chatbot_config_domain_relApi } from './services/custom/Chatbot_config_domain_rel';
import { Chatbot_config_sds_domain_relApi } from './services/custom/Chatbot_config_sds_domain_rel';
import { Chatbot_configApi } from './services/custom/Chatbot_config';
import { Chatbot_infoApi } from './services/custom/Chatbot_info';
import { Chatbot_msgApi } from './services/custom/Chatbot_msg';
import { Chatbot_rich_contentApi } from './services/custom/Chatbot_rich_content';
import { Chatbot_rich_imageApi } from './services/custom/Chatbot_rich_image';
import { Cm_chatbotApi } from './services/custom/Cm_chatbot';
import { Cm_domainApi } from './services/custom/Cm_domain';
import { ContainerApi } from './services/custom/Container';
import { Cm_domain_childApi } from './services/custom/Cm_domain_child';
import { Cm_synonymApi } from './services/custom/Cm_synonym';
import { Srch_morphemeApi } from './services/custom/Srch_morpheme';
import { Srch_stopwordApi } from './services/custom/Srch_stopword';
import { Srch_autocompleteApi } from './services/custom/Srch_autocomplete';
import { Srch_spellcheckApi } from './services/custom/Srch_spellcheck';
import { Srch_relatedApi } from './services/custom/Srch_related';
import { Srch_popularApi } from './services/custom/Srch_popular';
import { Srch_rankingApi } from './services/custom/Srch_ranking';
import { Srch_staticApi } from './services/custom/Srch_static';
import { Dlg_historyApi } from './services/custom/Dlg_history';
import { Chatbot_sds_port_hisApi } from './services/custom/Chatbot_sds_port_his';
import { Chatbot_session_relApi } from './services/custom/Chatbot_session_rel';
import { ProjectApi } from './services/custom/Project';
import { If_apiApi } from './services/custom/If_api';
import { If_api_result_mappingApi } from './services/custom/If_api_result_mapping';
import { If_api_paramsApi } from './services/custom/If_api_params';
import { If_api_json_templateApi } from './services/custom/If_api_json_template';
import { Itf_sdsApi } from './services/custom/Itf_sds';
import { Itf_intent_domain_relApi } from './services/custom/Itf_intent_domain_rel';
import { Itf_corpus_categoryApi } from './services/custom/Itf_corpus_category';
import { Itf_prebqaApi } from './services/custom/Itf_prebqa';
import { Stats_chatbot_infoApi } from './services/custom/Stats_chatbot_info';
import { Stats_dlg_historyApi } from './services/custom/Stats_dlg_history';
import { Qa_indexing_historyApi } from './services/custom/Qa_indexing_history';
import { Monitor_serverApi } from './services/custom/Monitor_server';
import { Front_user_commentApi } from './services/custom/Front_user_comment';
import { Nlp_synonymApi } from './services/custom/Nlp_synonym';
import { Nlp_user_dicApi } from './services/custom/Nlp_user_dic';
/**
* @module SDKBrowserModule
* @description
* This module should be imported when building a Web Application in the following scenarios:
*
*  1.- Regular web application
*  2.- Angular universal application (Browser Portion)
*  3.- Progressive applications (Angular Mobile, Ionic, WebViews, etc)
**/
@NgModule({
  imports:      [ CommonModule, HttpModule ],
  declarations: [ ],
  exports:      [ ],
  providers:    [
    ErrorHandler,
    SocketConnection
  ]
})
export class SDKBrowserModule {
  static forRoot(internalStorageProvider: any = {
    provide: InternalStorage,
    useClass: CookieBrowser
  }): ModuleWithProviders {
    return {
      ngModule  : SDKBrowserModule,
      providers : [
        LoopBackAuth,
        LoggerService,
        JSONSearchParams,
        SDKModels,
        RealTime,
        Cm_cd_groupApi,
        Cm_cd_detailApi,
        Cm_menu_roleApi,
        Cm_roleApi,
        Cm_login_historyApi,
        Cm_menuApi,
        Cm_teamApi,
        Cm_userApi,
        Cm_workspaceApi,
        Cm_workspace_teamApi,
        NotificationApi,
        EngineApi,
        LoopConfigFileApi,
        GrpcApi,
        StatisticsApi,
        Cf_dnnApi,
        Cf_simpleApi,
        Chatbot_buttonsApi,
        Chatbot_config_domain_relApi,
        Chatbot_config_sds_domain_relApi,
        Chatbot_configApi,
        Chatbot_infoApi,
        Chatbot_msgApi,
        Chatbot_rich_contentApi,
        Chatbot_rich_imageApi,
        Cm_chatbotApi,
        Cm_domainApi,
        ContainerApi,
        Cm_domain_childApi,
        Cm_synonymApi,
        Srch_morphemeApi,
        Srch_stopwordApi,
        Srch_autocompleteApi,
        Srch_spellcheckApi,
        Srch_relatedApi,
        Srch_popularApi,
        Srch_rankingApi,
        Srch_staticApi,
        Dlg_historyApi,
        Chatbot_sds_port_hisApi,
        Chatbot_session_relApi,
        ProjectApi,
        If_apiApi,
        If_api_result_mappingApi,
        If_api_paramsApi,
        If_api_json_templateApi,
        Itf_sdsApi,
        Itf_intent_domain_relApi,
        Itf_corpus_categoryApi,
        Itf_prebqaApi,
        Stats_chatbot_infoApi,
        Stats_dlg_historyApi,
        Qa_indexing_historyApi,
        Monitor_serverApi,
        Front_user_commentApi,
        Nlp_synonymApi,
        Nlp_user_dicApi,
        internalStorageProvider,
        { provide: SDKStorage, useClass: StorageBrowser },
        { provide: SocketDriver, useClass: SocketBrowser }
      ]
    };
  }
}
/**
* Have Fun!!!
* - Jon
**/
export * from './models/index';
export * from './services/index';
export * from './lb.config';
export * from './storage/storage.swaps';
export { CookieBrowser } from './storage/cookie.browser';
export { StorageBrowser } from './storage/storage.browser';
