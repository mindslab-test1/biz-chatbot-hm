/* tslint:disable */
import {
  If_api_result_mapping,
  If_api_params,
  If_api_json_template
} from '../index';

declare var Object: any;
export interface If_apiInterface {
  "if_id"?: number;
  "workspace_id": number;
  "api_name": string;
  "description": string;
  "endpoint": string;
  "content_type"?: string;
  "use_json_temp_yn"?: string;
  "method": string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  resultMapping?: If_api_result_mapping[];
  params?: If_api_params[];
  jsonTemplate?: If_api_json_template[];
}

export class If_api implements If_apiInterface {
  "if_id": number;
  "workspace_id": number;
  "api_name": string;
  "description": string;
  "endpoint": string;
  "content_type": string;
  "use_json_temp_yn": string;
  "method": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  resultMapping: If_api_result_mapping[];
  params: If_api_params[];
  jsonTemplate: If_api_json_template[];
  constructor(data?: If_apiInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `If_api`.
   */
  public static getModelName() {
    return "If_api";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of If_api for dynamic purposes.
  **/
  public static factory(data: If_apiInterface): If_api{
    return new If_api(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'If_api',
      plural: 'If_apis',
      path: 'If_apis',
      properties: {
        "if_id": {
          name: 'if_id',
          type: 'number'
        },
        "workspace_id": {
          name: 'workspace_id',
          type: 'number'
        },
        "api_name": {
          name: 'api_name',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "endpoint": {
          name: 'endpoint',
          type: 'string'
        },
        "content_type": {
          name: 'content_type',
          type: 'string'
        },
        "use_json_temp_yn": {
          name: 'use_json_temp_yn',
          type: 'string',
          default: 'N'
        },
        "method": {
          name: 'method',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        resultMapping: {
          name: 'resultMapping',
          type: 'If_api_result_mapping[]',
          model: 'If_api_result_mapping'
        },
        params: {
          name: 'params',
          type: 'If_api_params[]',
          model: 'If_api_params'
        },
        jsonTemplate: {
          name: 'jsonTemplate',
          type: 'If_api_json_template[]',
          model: 'If_api_json_template'
        },
      }
    }
  }
}
