/* tslint:disable */

declare var Object: any;
export interface Itf_sdsInterface {
  "itf_id"?: number;
  "bot_id": number;
  "project_name": string;
  "itf_order": number;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
}

export class Itf_sds implements Itf_sdsInterface {
  "itf_id": number;
  "bot_id": number;
  "project_name": string;
  "itf_order": number;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  constructor(data?: Itf_sdsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Itf_sds`.
   */
  public static getModelName() {
    return "Itf_sds";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Itf_sds for dynamic purposes.
  **/
  public static factory(data: Itf_sdsInterface): Itf_sds{
    return new Itf_sds(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Itf_sds',
      plural: 'Itf_sds',
      path: 'Itf_sds',
      properties: {
        "itf_id": {
          name: 'itf_id',
          type: 'number'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
        "project_name": {
          name: 'project_name',
          type: 'string'
        },
        "itf_order": {
          name: 'itf_order',
          type: 'number'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
