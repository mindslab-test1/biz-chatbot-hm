/* tslint:disable */
import {
  Cm_cd_group
} from '../index';

declare var Object: any;
export interface Cm_cd_detailInterface {
  "full_code": string;
  "detail_code": string;
  "group_code": string;
  "detail_name": string;
  "use_yn": string;
  "descript"?: string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  cm_cd_group?: Cm_cd_group;
}

export class Cm_cd_detail implements Cm_cd_detailInterface {
  "full_code": string;
  "detail_code": string;
  "group_code": string;
  "detail_name": string;
  "use_yn": string;
  "descript": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  cm_cd_group: Cm_cd_group;
  constructor(data?: Cm_cd_detailInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_cd_detail`.
   */
  public static getModelName() {
    return "Cm_cd_detail";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_cd_detail for dynamic purposes.
  **/
  public static factory(data: Cm_cd_detailInterface): Cm_cd_detail{
    return new Cm_cd_detail(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_cd_detail',
      plural: 'Cm_cd_details',
      path: 'Cm_cd_details',
      properties: {
        "full_code": {
          name: 'full_code',
          type: 'string'
        },
        "detail_code": {
          name: 'detail_code',
          type: 'string'
        },
        "group_code": {
          name: 'group_code',
          type: 'string'
        },
        "detail_name": {
          name: 'detail_name',
          type: 'string'
        },
        "use_yn": {
          name: 'use_yn',
          type: 'string'
        },
        "descript": {
          name: 'descript',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        cm_cd_group: {
          name: 'cm_cd_group',
          type: 'Cm_cd_group',
          model: 'Cm_cd_group'
        },
      }
    }
  }
}
