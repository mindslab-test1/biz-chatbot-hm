/* tslint:disable */
import {
  Chatbot_config_sds_domain_rel,
  Chatbot_rich_content
} from '../index';

declare var Object: any;
export interface Chatbot_configInterface {
  "conf_id"?: number;
  "bot_id": number;
  "greeting_msg_btn_id"?: number;
  "unknown_msg_btn_id"?: number;
  "synonym_yn": string;
  "scf_yn": string;
  "dnn_yn": string;
  "bqa_reconfirm_msg"?: string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  "bqa_search_flow_type"?: number;
  "bqa_search_solr_or_match_range"?: number;
  "notice1_img_id"?: number;
  "notice1_use_yn"?: string;
  "notice2_img_id"?: number;
  "notice2_use_yn"?: string;
  "notice3_img_id"?: number;
  "notice3_use_yn"?: string;
  "notice4_img_id"?: number;
  "notice4_use_yn"?: string;
  sdsDomains?: Chatbot_config_sds_domain_rel[];
  richContentGreeting?: Chatbot_rich_content;
  richContentUnknown?: Chatbot_rich_content;
  notice1?: Chatbot_rich_content;
  notice2?: Chatbot_rich_content;
  notice3?: Chatbot_rich_content;
  notice4?: Chatbot_rich_content;
  domains?: any[];
}

export class Chatbot_config implements Chatbot_configInterface {
  "conf_id": number;
  "bot_id": number;
  "greeting_msg_btn_id": number;
  "unknown_msg_btn_id": number;
  "synonym_yn": string;
  "scf_yn": string;
  "dnn_yn": string;
  "bqa_reconfirm_msg": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  "bqa_search_flow_type": number;
  "bqa_search_solr_or_match_range": number;
  "notice1_img_id": number;
  "notice1_use_yn": string;
  "notice2_img_id": number;
  "notice2_use_yn": string;
  "notice3_img_id": number;
  "notice3_use_yn": string;
  "notice4_img_id": number;
  "notice4_use_yn": string;
  sdsDomains: Chatbot_config_sds_domain_rel[];
  richContentGreeting: Chatbot_rich_content;
  richContentUnknown: Chatbot_rich_content;
  notice1: Chatbot_rich_content;
  notice2: Chatbot_rich_content;
  notice3: Chatbot_rich_content;
  notice4: Chatbot_rich_content;
  domains: any[];
  constructor(data?: Chatbot_configInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Chatbot_config`.
   */
  public static getModelName() {
    return "Chatbot_config";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Chatbot_config for dynamic purposes.
  **/
  public static factory(data: Chatbot_configInterface): Chatbot_config{
    return new Chatbot_config(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Chatbot_config',
      plural: 'Chatbot_configs',
      path: 'Chatbot_configs',
      properties: {
        "conf_id": {
          name: 'conf_id',
          type: 'number'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
        "greeting_msg_btn_id": {
          name: 'greeting_msg_btn_id',
          type: 'number'
        },
        "unknown_msg_btn_id": {
          name: 'unknown_msg_btn_id',
          type: 'number'
        },
        "synonym_yn": {
          name: 'synonym_yn',
          type: 'string',
          default: 'N'
        },
        "scf_yn": {
          name: 'scf_yn',
          type: 'string',
          default: 'N'
        },
        "dnn_yn": {
          name: 'dnn_yn',
          type: 'string',
          default: 'N'
        },
        "bqa_reconfirm_msg": {
          name: 'bqa_reconfirm_msg',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
        "bqa_search_flow_type": {
          name: 'bqa_search_flow_type',
          type: 'number'
        },
        "bqa_search_solr_or_match_range": {
          name: 'bqa_search_solr_or_match_range',
          type: 'number'
        },
        "notice1_img_id": {
          name: 'notice1_img_id',
          type: 'number'
        },
        "notice1_use_yn": {
          name: 'notice1_use_yn',
          type: 'string',
          default: 'N'
        },
        "notice2_img_id": {
          name: 'notice2_img_id',
          type: 'number'
        },
        "notice2_use_yn": {
          name: 'notice2_use_yn',
          type: 'string',
          default: 'N'
        },
        "notice3_img_id": {
          name: 'notice3_img_id',
          type: 'number'
        },
        "notice3_use_yn": {
          name: 'notice3_use_yn',
          type: 'string',
          default: 'N'
        },
        "notice4_img_id": {
          name: 'notice4_img_id',
          type: 'number'
        },
        "notice4_use_yn": {
          name: 'notice4_use_yn',
          type: 'string',
          default: 'N'
        },
      },
      relations: {
        sdsDomains: {
          name: 'sdsDomains',
          type: 'Chatbot_config_sds_domain_rel[]',
          model: 'Chatbot_config_sds_domain_rel'
        },
        richContentGreeting: {
          name: 'richContentGreeting',
          type: 'Chatbot_rich_content',
          model: 'Chatbot_rich_content'
        },
        richContentUnknown: {
          name: 'richContentUnknown',
          type: 'Chatbot_rich_content',
          model: 'Chatbot_rich_content'
        },
        notice1: {
          name: 'notice1',
          type: 'Chatbot_rich_content',
          model: 'Chatbot_rich_content'
        },
        notice2: {
          name: 'notice2',
          type: 'Chatbot_rich_content',
          model: 'Chatbot_rich_content'
        },
        notice3: {
          name: 'notice3',
          type: 'Chatbot_rich_content',
          model: 'Chatbot_rich_content'
        },
        notice4: {
          name: 'notice4',
          type: 'Chatbot_rich_content',
          model: 'Chatbot_rich_content'
        },
        domains: {
          name: 'domains',
          type: 'any[]',
          model: ''
        },
      }
    }
  }
}
