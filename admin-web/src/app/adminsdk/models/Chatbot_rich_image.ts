/* tslint:disable */

declare var Object: any;
export interface Chatbot_rich_imageInterface {
  "rc_id": number;
  "image_path"?: string;
  "image_text"?: string;
  "link_url"?: string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
}

export class Chatbot_rich_image implements Chatbot_rich_imageInterface {
  "rc_id": number;
  "image_path": string;
  "image_text": string;
  "link_url": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  constructor(data?: Chatbot_rich_imageInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Chatbot_rich_image`.
   */
  public static getModelName() {
    return "Chatbot_rich_image";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Chatbot_rich_image for dynamic purposes.
  **/
  public static factory(data: Chatbot_rich_imageInterface): Chatbot_rich_image{
    return new Chatbot_rich_image(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Chatbot_rich_image',
      plural: 'Chatbot_rich_images',
      path: 'Chatbot_rich_images',
      properties: {
        "rc_id": {
          name: 'rc_id',
          type: 'number'
        },
        "image_path": {
          name: 'image_path',
          type: 'string'
        },
        "image_text": {
          name: 'image_text',
          type: 'string'
        },
        "link_url": {
          name: 'link_url',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
