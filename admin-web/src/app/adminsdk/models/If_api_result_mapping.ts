/* tslint:disable */

declare var Object: any;
export interface If_api_result_mappingInterface {
  "mapping_id"?: number;
  "if_id": number;
  "json_path": string;
  "disp_name": string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
}

export class If_api_result_mapping implements If_api_result_mappingInterface {
  "mapping_id": number;
  "if_id": number;
  "json_path": string;
  "disp_name": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  constructor(data?: If_api_result_mappingInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `If_api_result_mapping`.
   */
  public static getModelName() {
    return "If_api_result_mapping";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of If_api_result_mapping for dynamic purposes.
  **/
  public static factory(data: If_api_result_mappingInterface): If_api_result_mapping{
    return new If_api_result_mapping(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'If_api_result_mapping',
      plural: 'If_api_result_mappings',
      path: 'If_api_result_mappings',
      properties: {
        "mapping_id": {
          name: 'mapping_id',
          type: 'number'
        },
        "if_id": {
          name: 'if_id',
          type: 'number'
        },
        "json_path": {
          name: 'json_path',
          type: 'string'
        },
        "disp_name": {
          name: 'disp_name',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
