/* tslint:disable */

declare var Object: any;
export interface If_api_paramsInterface {
  "if_id"?: number;
  "param_order": number;
  "param_type": string;
  "param_name": string;
  "param_value": string;
  "param_test_value"?: string;
  "description"?: string;
}

export class If_api_params implements If_api_paramsInterface {
  "if_id": number;
  "param_order": number;
  "param_type": string;
  "param_name": string;
  "param_value": string;
  "param_test_value": string;
  "description": string;
  constructor(data?: If_api_paramsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `If_api_params`.
   */
  public static getModelName() {
    return "If_api_params";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of If_api_params for dynamic purposes.
  **/
  public static factory(data: If_api_paramsInterface): If_api_params{
    return new If_api_params(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'If_api_params',
      plural: 'If_api_params',
      path: 'If_api_params',
      properties: {
        "if_id": {
          name: 'if_id',
          type: 'number'
        },
        "param_order": {
          name: 'param_order',
          type: 'number'
        },
        "param_type": {
          name: 'param_type',
          type: 'string'
        },
        "param_name": {
          name: 'param_name',
          type: 'string'
        },
        "param_value": {
          name: 'param_value',
          type: 'string'
        },
        "param_test_value": {
          name: 'param_test_value',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
