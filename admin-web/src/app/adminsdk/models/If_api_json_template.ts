/* tslint:disable */

declare var Object: any;
export interface If_api_json_templateInterface {
  "templ_id"?: number;
  "if_id": number;
  "json_string": string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
}

export class If_api_json_template implements If_api_json_templateInterface {
  "templ_id": number;
  "if_id": number;
  "json_string": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  constructor(data?: If_api_json_templateInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `If_api_json_template`.
   */
  public static getModelName() {
    return "If_api_json_template";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of If_api_json_template for dynamic purposes.
  **/
  public static factory(data: If_api_json_templateInterface): If_api_json_template{
    return new If_api_json_template(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'If_api_json_template',
      plural: 'If_api_json_templates',
      path: 'If_api_json_templates',
      properties: {
        "templ_id": {
          name: 'templ_id',
          type: 'number'
        },
        "if_id": {
          name: 'if_id',
          type: 'number'
        },
        "json_string": {
          name: 'json_string',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
