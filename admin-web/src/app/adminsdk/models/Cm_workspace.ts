/* tslint:disable */
import {
  Cm_team
} from '../index';

declare var Object: any;
export interface Cm_workspaceInterface {
  "workspace_id"?: number;
  "workspace_name"?: string;
  "lang_code"?: string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  teams?: Cm_team[];
}

export class Cm_workspace implements Cm_workspaceInterface {
  "workspace_id": number;
  "workspace_name": string;
  "lang_code": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  teams: Cm_team[];
  constructor(data?: Cm_workspaceInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_workspace`.
   */
  public static getModelName() {
    return "Cm_workspace";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_workspace for dynamic purposes.
  **/
  public static factory(data: Cm_workspaceInterface): Cm_workspace{
    return new Cm_workspace(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_workspace',
      plural: 'Cm_workspaces',
      path: 'Cm_workspaces',
      properties: {
        "workspace_id": {
          name: 'workspace_id',
          type: 'number'
        },
        "workspace_name": {
          name: 'workspace_name',
          type: 'string'
        },
        "lang_code": {
          name: 'lang_code',
          type: 'string',
          default: 'LC0001'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        teams: {
          name: 'teams',
          type: 'Cm_team[]',
          model: 'Cm_team'
        },
      }
    }
  }
}
