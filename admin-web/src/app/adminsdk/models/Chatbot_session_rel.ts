/* tslint:disable */
import {
  Dlg_history,
  Chatbot_info
} from '../index';

declare var Object: any;
export interface Chatbot_session_relInterface {
  "session_id": string;
  "bot_id": number;
  "his_id": number;
  dlgHistorys?: Dlg_history[];
  chatbot?: Chatbot_info;
}

export class Chatbot_session_rel implements Chatbot_session_relInterface {
  "session_id": string;
  "bot_id": number;
  "his_id": number;
  dlgHistorys: Dlg_history[];
  chatbot: Chatbot_info;
  constructor(data?: Chatbot_session_relInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Chatbot_session_rel`.
   */
  public static getModelName() {
    return "Chatbot_session_rel";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Chatbot_session_rel for dynamic purposes.
  **/
  public static factory(data: Chatbot_session_relInterface): Chatbot_session_rel{
    return new Chatbot_session_rel(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Chatbot_session_rel',
      plural: 'Chatbot_session_rels',
      path: 'Chatbot_session_rels',
      properties: {
        "session_id": {
          name: 'session_id',
          type: 'string'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
        "his_id": {
          name: 'his_id',
          type: 'number'
        },
      },
      relations: {
        dlgHistorys: {
          name: 'dlgHistorys',
          type: 'Dlg_history[]',
          model: 'Dlg_history'
        },
        chatbot: {
          name: 'chatbot',
          type: 'Chatbot_info',
          model: 'Chatbot_info'
        },
      }
    }
  }
}
