/* tslint:disable */

declare var Object: any;
export interface Stats_dlg_historyInterface {
  "utter_id"?: number;
  "session_id": string;
  "bot_id": number;
  "user_id": string;
  "dialog_seq": number;
  "speaker": string;
  "sentence"?: string;
  "content_json"?: string;
  "domain_name": string;
  "msg_type_cd": string;
  "dlg_step"?: string;
  "timestamp": Date;
}

export class Stats_dlg_history implements Stats_dlg_historyInterface {
  "utter_id": number;
  "session_id": string;
  "bot_id": number;
  "user_id": string;
  "dialog_seq": number;
  "speaker": string;
  "sentence": string;
  "content_json": string;
  "domain_name": string;
  "msg_type_cd": string;
  "dlg_step": string;
  "timestamp": Date;
  constructor(data?: Stats_dlg_historyInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Stats_dlg_history`.
   */
  public static getModelName() {
    return "Stats_dlg_history";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Stats_dlg_history for dynamic purposes.
  **/
  public static factory(data: Stats_dlg_historyInterface): Stats_dlg_history{
    return new Stats_dlg_history(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Stats_dlg_history',
      plural: 'Stats_dlg_histories',
      path: 'Stats_dlg_histories',
      properties: {
        "utter_id": {
          name: 'utter_id',
          type: 'number'
        },
        "session_id": {
          name: 'session_id',
          type: 'string'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
        "user_id": {
          name: 'user_id',
          type: 'string'
        },
        "dialog_seq": {
          name: 'dialog_seq',
          type: 'number'
        },
        "speaker": {
          name: 'speaker',
          type: 'string'
        },
        "sentence": {
          name: 'sentence',
          type: 'string'
        },
        "content_json": {
          name: 'content_json',
          type: 'string'
        },
        "domain_name": {
          name: 'domain_name',
          type: 'string',
          default: ''
        },
        "msg_type_cd": {
          name: 'msg_type_cd',
          type: 'string',
          default: ''
        },
        "dlg_step": {
          name: 'dlg_step',
          type: 'string'
        },
        "timestamp": {
          name: 'timestamp',
          type: 'Date'
        },
      },
      relations: {
      }
    }
  }
}
