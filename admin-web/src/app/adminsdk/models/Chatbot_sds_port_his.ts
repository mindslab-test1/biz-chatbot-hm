/* tslint:disable */
import {
  Chatbot_info,
  Chatbot_session_rel
} from '../index';

declare var Object: any;
export interface Chatbot_sds_port_hisInterface {
  "his_id"?: number;
  "bot_id": number;
  "session_id": string;
  "project_port": number;
  "sds_ip": string;
  "open_dtm": Date;
  "close_dtm"?: Date;
  "close_reason_cd"?: string;
  chatbot?: Chatbot_info;
  chatSession?: Chatbot_session_rel;
}

export class Chatbot_sds_port_his implements Chatbot_sds_port_hisInterface {
  "his_id": number;
  "bot_id": number;
  "session_id": string;
  "project_port": number;
  "sds_ip": string;
  "open_dtm": Date;
  "close_dtm": Date;
  "close_reason_cd": string;
  chatbot: Chatbot_info;
  chatSession: Chatbot_session_rel;
  constructor(data?: Chatbot_sds_port_hisInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Chatbot_sds_port_his`.
   */
  public static getModelName() {
    return "Chatbot_sds_port_his";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Chatbot_sds_port_his for dynamic purposes.
  **/
  public static factory(data: Chatbot_sds_port_hisInterface): Chatbot_sds_port_his{
    return new Chatbot_sds_port_his(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Chatbot_sds_port_his',
      plural: 'Chatbot_sds_port_his',
      path: 'Chatbot_sds_port_his',
      properties: {
        "his_id": {
          name: 'his_id',
          type: 'number'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
        "session_id": {
          name: 'session_id',
          type: 'string'
        },
        "project_port": {
          name: 'project_port',
          type: 'number'
        },
        "sds_ip": {
          name: 'sds_ip',
          type: 'string'
        },
        "open_dtm": {
          name: 'open_dtm',
          type: 'Date'
        },
        "close_dtm": {
          name: 'close_dtm',
          type: 'Date'
        },
        "close_reason_cd": {
          name: 'close_reason_cd',
          type: 'string'
        },
      },
      relations: {
        chatbot: {
          name: 'chatbot',
          type: 'Chatbot_info',
          model: 'Chatbot_info'
        },
        chatSession: {
          name: 'chatSession',
          type: 'Chatbot_session_rel',
          model: 'Chatbot_session_rel'
        },
      }
    }
  }
}
