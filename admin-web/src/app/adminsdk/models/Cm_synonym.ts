/* tslint:disable */

declare var Object: any;
export interface Cm_synonymInterface {
  "synonym_id"?: number;
  "workspace_id": number;
  "word": string;
  "synonym_list": string;
}

export class Cm_synonym implements Cm_synonymInterface {
  "synonym_id": number;
  "workspace_id": number;
  "word": string;
  "synonym_list": string;
  constructor(data?: Cm_synonymInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_synonym`.
   */
  public static getModelName() {
    return "Cm_synonym";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_synonym for dynamic purposes.
  **/
  public static factory(data: Cm_synonymInterface): Cm_synonym{
    return new Cm_synonym(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_synonym',
      plural: 'Cm_synonyms',
      path: 'Cm_synonyms',
      properties: {
        "synonym_id": {
          name: 'synonym_id',
          type: 'number'
        },
        "workspace_id": {
          name: 'workspace_id',
          type: 'number'
        },
        "word": {
          name: 'word',
          type: 'string'
        },
        "synonym_list": {
          name: 'synonym_list',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
