/* tslint:disable */
import {
  Chatbot_info
} from '../index';

declare var Object: any;
export interface Itf_prebqaInterface {
  "preitf_id"?: number;
  "bot_id": number;
  "pattern": string;
  "itf_order"?: number;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  chatbotInfo?: Chatbot_info;
}

export class Itf_prebqa implements Itf_prebqaInterface {
  "preitf_id": number;
  "bot_id": number;
  "pattern": string;
  "itf_order": number;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  chatbotInfo: Chatbot_info;
  constructor(data?: Itf_prebqaInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Itf_prebqa`.
   */
  public static getModelName() {
    return "Itf_prebqa";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Itf_prebqa for dynamic purposes.
  **/
  public static factory(data: Itf_prebqaInterface): Itf_prebqa{
    return new Itf_prebqa(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Itf_prebqa',
      plural: 'Itf_prebqas',
      path: 'Itf_prebqas',
      properties: {
        "preitf_id": {
          name: 'preitf_id',
          type: 'number'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
        "pattern": {
          name: 'pattern',
          type: 'string'
        },
        "itf_order": {
          name: 'itf_order',
          type: 'number'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        chatbotInfo: {
          name: 'chatbotInfo',
          type: 'Chatbot_info',
          model: 'Chatbot_info'
        },
      }
    }
  }
}
