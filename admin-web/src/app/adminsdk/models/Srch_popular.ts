/* tslint:disable */

declare var Object: any;
export interface Srch_popularInterface {
  "ID"?: number;
  "KEYWORD": string;
}

export class Srch_popular implements Srch_popularInterface {
  "ID": number;
  "KEYWORD": string;
  constructor(data?: Srch_popularInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Srch_popular`.
   */
  public static getModelName() {
    return "Srch_popular";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Srch_popular for dynamic purposes.
  **/
  public static factory(data: Srch_popularInterface): Srch_popular{
    return new Srch_popular(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Srch_popular',
      plural: 'Srch_populars',
      path: 'Srch_populars',
      properties: {
        "ID": {
          name: 'ID',
          type: 'number'
        },
        "KEYWORD": {
          name: 'KEYWORD',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
