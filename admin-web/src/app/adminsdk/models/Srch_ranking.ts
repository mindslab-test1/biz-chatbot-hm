/* tslint:disable */

declare var Object: any;
export interface Srch_rankingInterface {
  "ID"?: number;
  "FIELD_NAME": string;
  "BOOST"?: number;
}

export class Srch_ranking implements Srch_rankingInterface {
  "ID": number;
  "FIELD_NAME": string;
  "BOOST": number;
  constructor(data?: Srch_rankingInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Srch_ranking`.
   */
  public static getModelName() {
    return "Srch_ranking";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Srch_ranking for dynamic purposes.
  **/
  public static factory(data: Srch_rankingInterface): Srch_ranking{
    return new Srch_ranking(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Srch_ranking',
      plural: 'Srch_rankings',
      path: 'Srch_rankings',
      properties: {
        "ID": {
          name: 'ID',
          type: 'number'
        },
        "FIELD_NAME": {
          name: 'FIELD_NAME',
          type: 'string'
        },
        "BOOST": {
          name: 'BOOST',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
