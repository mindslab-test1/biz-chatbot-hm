/* tslint:disable */
import {
  Cf_dnn,
  Cf_simple,
  Chatbot_config,
  Chatbot_msg
} from '../index';

declare var Object: any;
export interface Stats_chatbot_infoInterface {
  "bot_id"?: number;
  "team_id": number;
  "bot_name": string;
  "description"?: string;
  "img_path"?: string;
  "use_yn": string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  cfDnns?: Cf_dnn[];
  cfSimples?: Cf_simple[];
  chatbotConfigs?: Chatbot_config[];
  chatbotMsgs?: Chatbot_msg[];
}

export class Stats_chatbot_info implements Stats_chatbot_infoInterface {
  "bot_id": number;
  "team_id": number;
  "bot_name": string;
  "description": string;
  "img_path": string;
  "use_yn": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  cfDnns: Cf_dnn[];
  cfSimples: Cf_simple[];
  chatbotConfigs: Chatbot_config[];
  chatbotMsgs: Chatbot_msg[];
  constructor(data?: Stats_chatbot_infoInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Stats_chatbot_info`.
   */
  public static getModelName() {
    return "Stats_chatbot_info";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Stats_chatbot_info for dynamic purposes.
  **/
  public static factory(data: Stats_chatbot_infoInterface): Stats_chatbot_info{
    return new Stats_chatbot_info(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Stats_chatbot_info',
      plural: 'Stats_chatbot_infos',
      path: 'Stats_chatbot_infos',
      properties: {
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
        "team_id": {
          name: 'team_id',
          type: 'number'
        },
        "bot_name": {
          name: 'bot_name',
          type: 'string'
        },
        "description": {
          name: 'description',
          type: 'string'
        },
        "img_path": {
          name: 'img_path',
          type: 'string'
        },
        "use_yn": {
          name: 'use_yn',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        cfDnns: {
          name: 'cfDnns',
          type: 'Cf_dnn[]',
          model: 'Cf_dnn'
        },
        cfSimples: {
          name: 'cfSimples',
          type: 'Cf_simple[]',
          model: 'Cf_simple'
        },
        chatbotConfigs: {
          name: 'chatbotConfigs',
          type: 'Chatbot_config[]',
          model: 'Chatbot_config'
        },
        chatbotMsgs: {
          name: 'chatbotMsgs',
          type: 'Chatbot_msg[]',
          model: 'Chatbot_msg'
        },
      }
    }
  }
}
