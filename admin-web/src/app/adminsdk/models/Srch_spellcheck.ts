/* tslint:disable */

declare var Object: any;
export interface Srch_spellcheckInterface {
  "ID"?: number;
  "SPELL_KEYWORD"?: string;
  "TYPE"?: string;
}

export class Srch_spellcheck implements Srch_spellcheckInterface {
  "ID": number;
  "SPELL_KEYWORD": string;
  "TYPE": string;
  constructor(data?: Srch_spellcheckInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Srch_spellcheck`.
   */
  public static getModelName() {
    return "Srch_spellcheck";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Srch_spellcheck for dynamic purposes.
  **/
  public static factory(data: Srch_spellcheckInterface): Srch_spellcheck{
    return new Srch_spellcheck(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Srch_spellcheck',
      plural: 'Srch_spellchecks',
      path: 'Srch_spellchecks',
      properties: {
        "ID": {
          name: 'ID',
          type: 'number'
        },
        "SPELL_KEYWORD": {
          name: 'SPELL_KEYWORD',
          type: 'string'
        },
        "TYPE": {
          name: 'TYPE',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
