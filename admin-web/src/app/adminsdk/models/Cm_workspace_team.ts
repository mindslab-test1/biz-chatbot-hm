/* tslint:disable */
import {
  Cm_workspace,
  Cm_team
} from '../index';

declare var Object: any;
export interface Cm_workspace_teamInterface {
  "workspace_id": number;
  "team_id": number;
  workspace?: Cm_workspace;
  team?: Cm_team;
}

export class Cm_workspace_team implements Cm_workspace_teamInterface {
  "workspace_id": number;
  "team_id": number;
  workspace: Cm_workspace;
  team: Cm_team;
  constructor(data?: Cm_workspace_teamInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_workspace_team`.
   */
  public static getModelName() {
    return "Cm_workspace_team";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_workspace_team for dynamic purposes.
  **/
  public static factory(data: Cm_workspace_teamInterface): Cm_workspace_team{
    return new Cm_workspace_team(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_workspace_team',
      plural: 'Cm_workspace_teams',
      path: 'Cm_workspace_teams',
      properties: {
        "workspace_id": {
          name: 'workspace_id',
          type: 'number'
        },
        "team_id": {
          name: 'team_id',
          type: 'number'
        },
      },
      relations: {
        workspace: {
          name: 'workspace',
          type: 'Cm_workspace',
          model: 'Cm_workspace'
        },
        team: {
          name: 'team',
          type: 'Cm_team',
          model: 'Cm_team'
        },
      }
    }
  }
}
