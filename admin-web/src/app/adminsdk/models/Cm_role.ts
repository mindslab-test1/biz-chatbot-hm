/* tslint:disable */
import {
  Cm_user
} from '../index';

declare var Object: any;
export interface Cm_roleInterface {
  "role_id": string;
  "role_name": string;
  "descript"?: string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  users?: Cm_user[];
}

export class Cm_role implements Cm_roleInterface {
  "role_id": string;
  "role_name": string;
  "descript": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  users: Cm_user[];
  constructor(data?: Cm_roleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_role`.
   */
  public static getModelName() {
    return "Cm_role";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_role for dynamic purposes.
  **/
  public static factory(data: Cm_roleInterface): Cm_role{
    return new Cm_role(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_role',
      plural: 'Cm_roles',
      path: 'Cm_roles',
      properties: {
        "role_id": {
          name: 'role_id',
          type: 'string'
        },
        "role_name": {
          name: 'role_name',
          type: 'string'
        },
        "descript": {
          name: 'descript',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        users: {
          name: 'users',
          type: 'Cm_user[]',
          model: 'Cm_user'
        },
      }
    }
  }
}
