/* tslint:disable */
import {
  Chatbot_buttons,
  Chatbot_rich_image
} from '../index';

declare var Object: any;
export interface Chatbot_rich_contentInterface {
  "rc_id"?: number;
  "content_type_cd": string;
  "title"?: string;
  "content"?: string;
  "img_path"?: string;
  "link_path"?: string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  buttons?: Chatbot_buttons[];
  image?: Chatbot_rich_image;
}

export class Chatbot_rich_content implements Chatbot_rich_contentInterface {
  "rc_id": number;
  "content_type_cd": string;
  "title": string;
  "content": string;
  "img_path": string;
  "link_path": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  buttons: Chatbot_buttons[];
  image: Chatbot_rich_image;
  constructor(data?: Chatbot_rich_contentInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Chatbot_rich_content`.
   */
  public static getModelName() {
    return "Chatbot_rich_content";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Chatbot_rich_content for dynamic purposes.
  **/
  public static factory(data: Chatbot_rich_contentInterface): Chatbot_rich_content{
    return new Chatbot_rich_content(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Chatbot_rich_content',
      plural: 'Chatbot_rich_contents',
      path: 'Chatbot_rich_contents',
      properties: {
        "rc_id": {
          name: 'rc_id',
          type: 'number'
        },
        "content_type_cd": {
          name: 'content_type_cd',
          type: 'string'
        },
        "title": {
          name: 'title',
          type: 'string'
        },
        "content": {
          name: 'content',
          type: 'string'
        },
        "img_path": {
          name: 'img_path',
          type: 'string'
        },
        "link_path": {
          name: 'link_path',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        buttons: {
          name: 'buttons',
          type: 'Chatbot_buttons[]',
          model: 'Chatbot_buttons'
        },
        image: {
          name: 'image',
          type: 'Chatbot_rich_image',
          model: 'Chatbot_rich_image'
        },
      }
    }
  }
}
