/* tslint:disable */

declare var Object: any;
export interface GrpcInterface {
  "id"?: number;
}

export class Grpc implements GrpcInterface {
  "id": number;
  constructor(data?: GrpcInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Grpc`.
   */
  public static getModelName() {
    return "Grpc";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Grpc for dynamic purposes.
  **/
  public static factory(data: GrpcInterface): Grpc{
    return new Grpc(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Grpc',
      plural: 'Grpc',
      path: 'Grpc',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
