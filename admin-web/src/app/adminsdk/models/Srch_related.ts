/* tslint:disable */

declare var Object: any;
export interface Srch_relatedInterface {
  "ID"?: number;
  "KEYWORD": string;
}

export class Srch_related implements Srch_relatedInterface {
  "ID": number;
  "KEYWORD": string;
  constructor(data?: Srch_relatedInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Srch_related`.
   */
  public static getModelName() {
    return "Srch_related";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Srch_related for dynamic purposes.
  **/
  public static factory(data: Srch_relatedInterface): Srch_related{
    return new Srch_related(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Srch_related',
      plural: 'Srch_relateds',
      path: 'Srch_relateds',
      properties: {
        "ID": {
          name: 'ID',
          type: 'number'
        },
        "KEYWORD": {
          name: 'KEYWORD',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
