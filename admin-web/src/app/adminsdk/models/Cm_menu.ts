/* tslint:disable */
import {
  Cm_menu_role
} from '../index';

declare var Object: any;
export interface Cm_menuInterface {
  "menu_id"?: number;
  "parent_menu_id": number;
  "name": string;
  "menu_path": string;
  "icon": string;
  "order": number;
  "visible_yn": string;
  "sub_urls"?: string;
  "acl"?: string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  menuRoles?: Cm_menu_role[];
  children?: any[];
}

export class Cm_menu implements Cm_menuInterface {
  "menu_id": number;
  "parent_menu_id": number;
  "name": string;
  "menu_path": string;
  "icon": string;
  "order": number;
  "visible_yn": string;
  "sub_urls": string;
  "acl": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  menuRoles: Cm_menu_role[];
  children: any[];
  constructor(data?: Cm_menuInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_menu`.
   */
  public static getModelName() {
    return "Cm_menu";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_menu for dynamic purposes.
  **/
  public static factory(data: Cm_menuInterface): Cm_menu{
    return new Cm_menu(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_menu',
      plural: 'Cm_menus',
      path: 'Cm_menus',
      properties: {
        "menu_id": {
          name: 'menu_id',
          type: 'number'
        },
        "parent_menu_id": {
          name: 'parent_menu_id',
          type: 'number'
        },
        "name": {
          name: 'name',
          type: 'string'
        },
        "menu_path": {
          name: 'menu_path',
          type: 'string'
        },
        "icon": {
          name: 'icon',
          type: 'string'
        },
        "order": {
          name: 'order',
          type: 'number'
        },
        "visible_yn": {
          name: 'visible_yn',
          type: 'string'
        },
        "sub_urls": {
          name: 'sub_urls',
          type: 'string'
        },
        "acl": {
          name: 'acl',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        menuRoles: {
          name: 'menuRoles',
          type: 'Cm_menu_role[]',
          model: 'Cm_menu_role'
        },
        children: {
          name: 'children',
          type: 'any[]',
          model: ''
        },
      }
    }
  }
}
