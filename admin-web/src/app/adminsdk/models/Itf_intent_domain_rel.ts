/* tslint:disable */

declare var Object: any;
export interface Itf_intent_domain_relInterface {
  "rel_id"?: number;
  "bot_id": number;
  "intent_name": string;
  "sds_domain": string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
}

export class Itf_intent_domain_rel implements Itf_intent_domain_relInterface {
  "rel_id": number;
  "bot_id": number;
  "intent_name": string;
  "sds_domain": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  constructor(data?: Itf_intent_domain_relInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Itf_intent_domain_rel`.
   */
  public static getModelName() {
    return "Itf_intent_domain_rel";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Itf_intent_domain_rel for dynamic purposes.
  **/
  public static factory(data: Itf_intent_domain_relInterface): Itf_intent_domain_rel{
    return new Itf_intent_domain_rel(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Itf_intent_domain_rel',
      plural: 'Itf_intent_domain_rels',
      path: 'Itf_intent_domain_rels',
      properties: {
        "rel_id": {
          name: 'rel_id',
          type: 'number'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
        "intent_name": {
          name: 'intent_name',
          type: 'string'
        },
        "sds_domain": {
          name: 'sds_domain',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
