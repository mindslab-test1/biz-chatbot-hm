/* tslint:disable */

declare var Object: any;
export interface LoopConfigFileInterface {
  "id"?: number;
}

export class LoopConfigFile implements LoopConfigFileInterface {
  "id": number;
  constructor(data?: LoopConfigFileInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `LoopConfigFile`.
   */
  public static getModelName() {
    return "LoopConfigFile";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of LoopConfigFile for dynamic purposes.
  **/
  public static factory(data: LoopConfigFileInterface): LoopConfigFile{
    return new LoopConfigFile(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'LoopConfigFile',
      plural: 'LoopConfigFiles',
      path: 'LoopConfigFiles',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
