/* tslint:disable */
import {
  Cm_role,
  Cm_menu
} from '../index';

declare var Object: any;
export interface Cm_menu_roleInterface {
  "menu_id"?: number;
  "role_id": string;
  "crudx": string;
  role?: Cm_role;
  menu?: Cm_menu;
}

export class Cm_menu_role implements Cm_menu_roleInterface {
  "menu_id": number;
  "role_id": string;
  "crudx": string;
  role: Cm_role;
  menu: Cm_menu;
  constructor(data?: Cm_menu_roleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_menu_role`.
   */
  public static getModelName() {
    return "Cm_menu_role";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_menu_role for dynamic purposes.
  **/
  public static factory(data: Cm_menu_roleInterface): Cm_menu_role{
    return new Cm_menu_role(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_menu_role',
      plural: 'Cm_menu_roles',
      path: 'Cm_menu_roles',
      properties: {
        "menu_id": {
          name: 'menu_id',
          type: 'number'
        },
        "role_id": {
          name: 'role_id',
          type: 'string'
        },
        "crudx": {
          name: 'crudx',
          type: 'string'
        },
      },
      relations: {
        role: {
          name: 'role',
          type: 'Cm_role',
          model: 'Cm_role'
        },
        menu: {
          name: 'menu',
          type: 'Cm_menu',
          model: 'Cm_menu'
        },
      }
    }
  }
}
