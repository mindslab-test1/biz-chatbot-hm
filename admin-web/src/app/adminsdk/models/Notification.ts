/* tslint:disable */
import {
  Cm_user
} from '../index';

declare var Object: any;
export interface NotificationInterface {
  "userId": number;
  "message": string;
  "createdAt": Date;
  "id"?: number;
  "user_id"?: string;
  user?: Cm_user;
}

export class Notification implements NotificationInterface {
  "userId": number;
  "message": string;
  "createdAt": Date;
  "id": number;
  "user_id": string;
  user: Cm_user;
  constructor(data?: NotificationInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Notification`.
   */
  public static getModelName() {
    return "Notification";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Notification for dynamic purposes.
  **/
  public static factory(data: NotificationInterface): Notification{
    return new Notification(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Notification',
      plural: 'Notifications',
      path: 'Notifications',
      properties: {
        "userId": {
          name: 'userId',
          type: 'number'
        },
        "message": {
          name: 'message',
          type: 'string'
        },
        "createdAt": {
          name: 'createdAt',
          type: 'Date'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
        "user_id": {
          name: 'user_id',
          type: 'string'
        },
      },
      relations: {
        user: {
          name: 'user',
          type: 'Cm_user',
          model: 'Cm_user'
        },
      }
    }
  }
}
