/* tslint:disable */
import {
  Chatbot_info
} from '../index';

declare var Object: any;
export interface Chatbot_msgInterface {
  "msg_id"?: number;
  "bot_id": number;
  "msg_type_cd": string;
  "msg": string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  chatbotInfo?: Chatbot_info;
}

export class Chatbot_msg implements Chatbot_msgInterface {
  "msg_id": number;
  "bot_id": number;
  "msg_type_cd": string;
  "msg": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  chatbotInfo: Chatbot_info;
  constructor(data?: Chatbot_msgInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Chatbot_msg`.
   */
  public static getModelName() {
    return "Chatbot_msg";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Chatbot_msg for dynamic purposes.
  **/
  public static factory(data: Chatbot_msgInterface): Chatbot_msg{
    return new Chatbot_msg(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Chatbot_msg',
      plural: 'Chatbot_msgs',
      path: 'Chatbot_msgs',
      properties: {
        "msg_id": {
          name: 'msg_id',
          type: 'number'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
        "msg_type_cd": {
          name: 'msg_type_cd',
          type: 'string'
        },
        "msg": {
          name: 'msg',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        chatbotInfo: {
          name: 'chatbotInfo',
          type: 'Chatbot_info',
          model: 'Chatbot_info'
        },
      }
    }
  }
}
