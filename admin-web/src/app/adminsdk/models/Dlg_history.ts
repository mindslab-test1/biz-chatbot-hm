/* tslint:disable */

declare var Object: any;
export interface Dlg_historyInterface {
  "utter_id"?: number;
  "session_id": string;
  "bot_id": number;
  "user_id": string;
  "dialog_seq": number;
  "speaker": string;
  "sentence"?: string;
  "content_json"?: string;
  "domain_name": string;
  "msg_type_cd": string;
  "dlg_step"?: string;
  "timestamp": Date;
}

export class Dlg_history implements Dlg_historyInterface {
  "utter_id": number;
  "session_id": string;
  "bot_id": number;
  "user_id": string;
  "dialog_seq": number;
  "speaker": string;
  "sentence": string;
  "content_json": string;
  "domain_name": string;
  "msg_type_cd": string;
  "dlg_step": string;
  "timestamp": Date;
  constructor(data?: Dlg_historyInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Dlg_history`.
   */
  public static getModelName() {
    return "Dlg_history";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Dlg_history for dynamic purposes.
  **/
  public static factory(data: Dlg_historyInterface): Dlg_history{
    return new Dlg_history(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Dlg_history',
      plural: 'Dlg_histories',
      path: 'Dlg_histories',
      properties: {
        "utter_id": {
          name: 'utter_id',
          type: 'number'
        },
        "session_id": {
          name: 'session_id',
          type: 'string'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
        "user_id": {
          name: 'user_id',
          type: 'string'
        },
        "dialog_seq": {
          name: 'dialog_seq',
          type: 'number'
        },
        "speaker": {
          name: 'speaker',
          type: 'string'
        },
        "sentence": {
          name: 'sentence',
          type: 'string'
        },
        "content_json": {
          name: 'content_json',
          type: 'string'
        },
        "domain_name": {
          name: 'domain_name',
          type: 'string',
          default: ''
        },
        "msg_type_cd": {
          name: 'msg_type_cd',
          type: 'string',
          default: ''
        },
        "dlg_step": {
          name: 'dlg_step',
          type: 'string'
        },
        "timestamp": {
          name: 'timestamp',
          type: 'Date'
        },
      },
      relations: {
      }
    }
  }
}
