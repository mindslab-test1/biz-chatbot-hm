/* tslint:disable */

declare var Object: any;
export interface Nlp_synonymInterface {
  "synonym_id"?: number;
  "type": number;
  "word": string;
  "synonym_list": string;
}

export class Nlp_synonym implements Nlp_synonymInterface {
  "synonym_id": number;
  "type": number;
  "word": string;
  "synonym_list": string;
  constructor(data?: Nlp_synonymInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Nlp_synonym`.
   */
  public static getModelName() {
    return "Nlp_synonym";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Nlp_synonym for dynamic purposes.
  **/
  public static factory(data: Nlp_synonymInterface): Nlp_synonym{
    return new Nlp_synonym(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Nlp_synonym',
      plural: 'Nlp_synonyms',
      path: 'Nlp_synonyms',
      properties: {
        "synonym_id": {
          name: 'synonym_id',
          type: 'number'
        },
        "type": {
          name: 'type',
          type: 'number'
        },
        "word": {
          name: 'word',
          type: 'string'
        },
        "synonym_list": {
          name: 'synonym_list',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
