/* tslint:disable */

declare var Object: any;
export interface Chatbot_buttonsInterface {
  "rc_id": number;
  "btn_order": number;
  "title": string;
  "user_utter": string;
  "sds_domain"?: string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
}

export class Chatbot_buttons implements Chatbot_buttonsInterface {
  "rc_id": number;
  "btn_order": number;
  "title": string;
  "user_utter": string;
  "sds_domain": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  constructor(data?: Chatbot_buttonsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Chatbot_buttons`.
   */
  public static getModelName() {
    return "Chatbot_buttons";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Chatbot_buttons for dynamic purposes.
  **/
  public static factory(data: Chatbot_buttonsInterface): Chatbot_buttons{
    return new Chatbot_buttons(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Chatbot_buttons',
      plural: 'Chatbot_buttons',
      path: 'Chatbot_buttons',
      properties: {
        "rc_id": {
          name: 'rc_id',
          type: 'number'
        },
        "btn_order": {
          name: 'btn_order',
          type: 'number'
        },
        "title": {
          name: 'title',
          type: 'string'
        },
        "user_utter": {
          name: 'user_utter',
          type: 'string'
        },
        "sds_domain": {
          name: 'sds_domain',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
