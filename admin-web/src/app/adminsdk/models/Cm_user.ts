/* tslint:disable */
import {
  Cm_team,
  Cm_role
} from '../index';

declare var Object: any;
export interface Cm_userInterface {
  "user_id"?: string;
  "team_id": number;
  "role_id": string;
  "user_name": string;
  "activated_yn": string;
  "email_verified_yn"?: string;
  "verification_token"?: string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  "is_change_pass"?: string;
  "username"?: string;
  "email": string;
  "password"?: string;
  accessTokens?: any[];
  team?: Cm_team;
  role?: Cm_role;
}

export class Cm_user implements Cm_userInterface {
  "user_id": string;
  "team_id": number;
  "role_id": string;
  "user_name": string;
  "activated_yn": string;
  "email_verified_yn": string;
  "verification_token": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  "is_change_pass": string;
  "username": string;
  "email": string;
  "password": string;
  accessTokens: any[];
  team: Cm_team;
  role: Cm_role;
  constructor(data?: Cm_userInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_user`.
   */
  public static getModelName() {
    return "Cm_user";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_user for dynamic purposes.
  **/
  public static factory(data: Cm_userInterface): Cm_user{
    return new Cm_user(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_user',
      plural: 'Cm_users',
      path: 'Cm_users',
      properties: {
        "user_id": {
          name: 'user_id',
          type: 'string'
        },
        "team_id": {
          name: 'team_id',
          type: 'number'
        },
        "role_id": {
          name: 'role_id',
          type: 'string'
        },
        "user_name": {
          name: 'user_name',
          type: 'string'
        },
        "activated_yn": {
          name: 'activated_yn',
          type: 'string'
        },
        "email_verified_yn": {
          name: 'email_verified_yn',
          type: 'string',
          default: 'N'
        },
        "verification_token": {
          name: 'verification_token',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
        "is_change_pass": {
          name: 'is_change_pass',
          type: 'string',
          default: 'N'
        },
        "username": {
          name: 'username',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "password": {
          name: 'password',
          type: 'string'
        },
      },
      relations: {
        accessTokens: {
          name: 'accessTokens',
          type: 'any[]',
          model: ''
        },
        team: {
          name: 'team',
          type: 'Cm_team',
          model: 'Cm_team'
        },
        role: {
          name: 'role',
          type: 'Cm_role',
          model: 'Cm_role'
        },
      }
    }
  }
}
