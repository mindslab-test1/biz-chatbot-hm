/* tslint:disable */

declare var Object: any;
export interface Qa_indexing_historyInterface {
  "history_id"?: number;
  "type"?: string;
  "domain_id"?: number;
  "total"?: number;
  "status"?: string;
  "msg"?: string;
  "indexing_started_dtm"?: Date;
  "committed_dtm"?: Date;
  "taken_time"?: string;
  "creator_id"?: string;
  "created_dtm"?: Date;
}

export class Qa_indexing_history implements Qa_indexing_historyInterface {
  "history_id": number;
  "type": string;
  "domain_id": number;
  "total": number;
  "status": string;
  "msg": string;
  "indexing_started_dtm": Date;
  "committed_dtm": Date;
  "taken_time": string;
  "creator_id": string;
  "created_dtm": Date;
  constructor(data?: Qa_indexing_historyInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Qa_indexing_history`.
   */
  public static getModelName() {
    return "Qa_indexing_history";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Qa_indexing_history for dynamic purposes.
  **/
  public static factory(data: Qa_indexing_historyInterface): Qa_indexing_history{
    return new Qa_indexing_history(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Qa_indexing_history',
      plural: 'Qa_indexing_histories',
      path: 'Qa_indexing_histories',
      properties: {
        "history_id": {
          name: 'history_id',
          type: 'number'
        },
        "type": {
          name: 'type',
          type: 'string'
        },
        "domain_id": {
          name: 'domain_id',
          type: 'number'
        },
        "total": {
          name: 'total',
          type: 'number'
        },
        "status": {
          name: 'status',
          type: 'string'
        },
        "msg": {
          name: 'msg',
          type: 'string'
        },
        "indexing_started_dtm": {
          name: 'indexing_started_dtm',
          type: 'Date'
        },
        "committed_dtm": {
          name: 'committed_dtm',
          type: 'Date'
        },
        "taken_time": {
          name: 'taken_time',
          type: 'string'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
      },
      relations: {
      }
    }
  }
}
