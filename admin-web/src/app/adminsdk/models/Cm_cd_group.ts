/* tslint:disable */
import {
  Cm_cd_detail
} from '../index';

declare var Object: any;
export interface Cm_cd_groupInterface {
  "group_code": string;
  "code_name": string;
  "use_yn": string;
  "descript"?: string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  details?: Cm_cd_detail[];
}

export class Cm_cd_group implements Cm_cd_groupInterface {
  "group_code": string;
  "code_name": string;
  "use_yn": string;
  "descript": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  details: Cm_cd_detail[];
  constructor(data?: Cm_cd_groupInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_cd_group`.
   */
  public static getModelName() {
    return "Cm_cd_group";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_cd_group for dynamic purposes.
  **/
  public static factory(data: Cm_cd_groupInterface): Cm_cd_group{
    return new Cm_cd_group(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_cd_group',
      plural: 'Cm_cd_groups',
      path: 'Cm_cd_groups',
      properties: {
        "group_code": {
          name: 'group_code',
          type: 'string'
        },
        "code_name": {
          name: 'code_name',
          type: 'string'
        },
        "use_yn": {
          name: 'use_yn',
          type: 'string'
        },
        "descript": {
          name: 'descript',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        details: {
          name: 'details',
          type: 'Cm_cd_detail[]',
          model: 'Cm_cd_detail'
        },
      }
    }
  }
}
