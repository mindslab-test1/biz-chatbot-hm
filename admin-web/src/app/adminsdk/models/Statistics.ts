/* tslint:disable */

declare var Object: any;
export interface StatisticsInterface {
  "id"?: number;
}

export class Statistics implements StatisticsInterface {
  "id": number;
  constructor(data?: StatisticsInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Statistics`.
   */
  public static getModelName() {
    return "Statistics";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Statistics for dynamic purposes.
  **/
  public static factory(data: StatisticsInterface): Statistics{
    return new Statistics(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Statistics',
      plural: 'Statistics',
      path: 'Statistics',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
