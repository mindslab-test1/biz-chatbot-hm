/* tslint:disable */

declare var Object: any;
export interface EngineInterface {
  "name": string;
  "categories": Array<any>;
  "bookmarkList"?: Array<any>;
  "id"?: number;
}

export class Engine implements EngineInterface {
  "name": string;
  "categories": Array<any>;
  "bookmarkList": Array<any>;
  "id": number;
  constructor(data?: EngineInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Engine`.
   */
  public static getModelName() {
    return "Engine";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Engine for dynamic purposes.
  **/
  public static factory(data: EngineInterface): Engine{
    return new Engine(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Engine',
      plural: 'Engines',
      path: 'Engines',
      properties: {
        "name": {
          name: 'name',
          type: 'string'
        },
        "categories": {
          name: 'categories',
          type: 'Array&lt;any&gt;'
        },
        "bookmarkList": {
          name: 'bookmarkList',
          type: 'Array&lt;any&gt;'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
