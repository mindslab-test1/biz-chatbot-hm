/* tslint:disable */

declare var Object: any;
export interface Srch_morphemeInterface {
  "MORPH_NO"?: number;
  "KEYWORD": string;
  "MORPH_TAG": string;
  "MEAN_CLASSIFICATION"?: string;
  "LAST_MORPH"?: string;
  "STATUS"?: string;
  "TYPE"?: string;
  "MORPH_INDEX_INFO"?: string;
}

export class Srch_morpheme implements Srch_morphemeInterface {
  "MORPH_NO": number;
  "KEYWORD": string;
  "MORPH_TAG": string;
  "MEAN_CLASSIFICATION": string;
  "LAST_MORPH": string;
  "STATUS": string;
  "TYPE": string;
  "MORPH_INDEX_INFO": string;
  constructor(data?: Srch_morphemeInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Srch_morpheme`.
   */
  public static getModelName() {
    return "Srch_morpheme";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Srch_morpheme for dynamic purposes.
  **/
  public static factory(data: Srch_morphemeInterface): Srch_morpheme{
    return new Srch_morpheme(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Srch_morpheme',
      plural: 'Srch_morphemes',
      path: 'Srch_morphemes',
      properties: {
        "MORPH_NO": {
          name: 'MORPH_NO',
          type: 'number'
        },
        "KEYWORD": {
          name: 'KEYWORD',
          type: 'string'
        },
        "MORPH_TAG": {
          name: 'MORPH_TAG',
          type: 'string'
        },
        "MEAN_CLASSIFICATION": {
          name: 'MEAN_CLASSIFICATION',
          type: 'string'
        },
        "LAST_MORPH": {
          name: 'LAST_MORPH',
          type: 'string'
        },
        "STATUS": {
          name: 'STATUS',
          type: 'string',
          default: 'Y'
        },
        "TYPE": {
          name: 'TYPE',
          type: 'string'
        },
        "MORPH_INDEX_INFO": {
          name: 'MORPH_INDEX_INFO',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
