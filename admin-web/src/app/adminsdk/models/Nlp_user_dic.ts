/* tslint:disable */

declare var Object: any;
export interface Nlp_user_dicInterface {
  "user_dic_id"?: number;
  "type": number;
  "word": string;
  "custom_word": string;
}

export class Nlp_user_dic implements Nlp_user_dicInterface {
  "user_dic_id": number;
  "type": number;
  "word": string;
  "custom_word": string;
  constructor(data?: Nlp_user_dicInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Nlp_user_dic`.
   */
  public static getModelName() {
    return "Nlp_user_dic";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Nlp_user_dic for dynamic purposes.
  **/
  public static factory(data: Nlp_user_dicInterface): Nlp_user_dic{
    return new Nlp_user_dic(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Nlp_user_dic',
      plural: 'Nlp_user_dics',
      path: 'Nlp_user_dics',
      properties: {
        "user_dic_id": {
          name: 'user_dic_id',
          type: 'number'
        },
        "type": {
          name: 'type',
          type: 'number'
        },
        "word": {
          name: 'word',
          type: 'string'
        },
        "custom_word": {
          name: 'custom_word',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
