/* tslint:disable */
import {
  Cm_team
} from '../index';

declare var Object: any;
export interface Cm_chatbotInterface {
  "chatbot_id"?: string;
  "chatbot_name": string;
  "team_id": number;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  team?: Cm_team;
}

export class Cm_chatbot implements Cm_chatbotInterface {
  "chatbot_id": string;
  "chatbot_name": string;
  "team_id": number;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  team: Cm_team;
  constructor(data?: Cm_chatbotInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_chatbot`.
   */
  public static getModelName() {
    return "Cm_chatbot";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_chatbot for dynamic purposes.
  **/
  public static factory(data: Cm_chatbotInterface): Cm_chatbot{
    return new Cm_chatbot(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_chatbot',
      plural: 'Cm_chatbots',
      path: 'Cm_chatbots',
      properties: {
        "chatbot_id": {
          name: 'chatbot_id',
          type: 'string'
        },
        "chatbot_name": {
          name: 'chatbot_name',
          type: 'string'
        },
        "team_id": {
          name: 'team_id',
          type: 'number'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        team: {
          name: 'team',
          type: 'Cm_team',
          model: 'Cm_team'
        },
      }
    }
  }
}
