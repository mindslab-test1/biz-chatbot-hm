/* tslint:disable */
import {
  Chatbot_info
} from '../index';

declare var Object: any;
export interface Cf_simpleInterface {
  "scf_id"?: number;
  "bot_id": number;
  "domain_id": number;
  "pattern": string;
  "sds_domain": string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  chatbotInfo?: Chatbot_info;
}

export class Cf_simple implements Cf_simpleInterface {
  "scf_id": number;
  "bot_id": number;
  "domain_id": number;
  "pattern": string;
  "sds_domain": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  chatbotInfo: Chatbot_info;
  constructor(data?: Cf_simpleInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cf_simple`.
   */
  public static getModelName() {
    return "Cf_simple";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cf_simple for dynamic purposes.
  **/
  public static factory(data: Cf_simpleInterface): Cf_simple{
    return new Cf_simple(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cf_simple',
      plural: 'Cf_simples',
      path: 'Cf_simples',
      properties: {
        "scf_id": {
          name: 'scf_id',
          type: 'number'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
        "domain_id": {
          name: 'domain_id',
          type: 'number'
        },
        "pattern": {
          name: 'pattern',
          type: 'string'
        },
        "sds_domain": {
          name: 'sds_domain',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        chatbotInfo: {
          name: 'chatbotInfo',
          type: 'Chatbot_info',
          model: 'Chatbot_info'
        },
      }
    }
  }
}
