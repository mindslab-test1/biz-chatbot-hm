/* tslint:disable */

declare var Object: any;
export interface Srch_staticInterface {
  "USER_DIV_CODE"?: string;
  "USER_DIV_NM"?: string;
  "id"?: number;
}

export class Srch_static implements Srch_staticInterface {
  "USER_DIV_CODE": string;
  "USER_DIV_NM": string;
  "id": number;
  constructor(data?: Srch_staticInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Srch_static`.
   */
  public static getModelName() {
    return "Srch_static";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Srch_static for dynamic purposes.
  **/
  public static factory(data: Srch_staticInterface): Srch_static{
    return new Srch_static(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Srch_static',
      plural: 'Srch_statics',
      path: 'Srch_statics',
      properties: {
        "USER_DIV_CODE": {
          name: 'USER_DIV_CODE',
          type: 'string'
        },
        "USER_DIV_NM": {
          name: 'USER_DIV_NM',
          type: 'string'
        },
        "id": {
          name: 'id',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
