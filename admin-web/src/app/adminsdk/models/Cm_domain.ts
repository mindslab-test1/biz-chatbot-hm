/* tslint:disable */
import {
  Cm_domain_child
} from '../index';

declare var Object: any;
export interface Cm_domainInterface {
  "domain_id"?: number;
  "team_id"?: number;
  "parent_domain_id"?: number;
  "depth"?: number;
  "domain_name"?: string;
  "descript"?: string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  children?: Cm_domain_child[];
}

export class Cm_domain implements Cm_domainInterface {
  "domain_id": number;
  "team_id": number;
  "parent_domain_id": number;
  "depth": number;
  "domain_name": string;
  "descript": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  children: Cm_domain_child[];
  constructor(data?: Cm_domainInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_domain`.
   */
  public static getModelName() {
    return "Cm_domain";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_domain for dynamic purposes.
  **/
  public static factory(data: Cm_domainInterface): Cm_domain{
    return new Cm_domain(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_domain',
      plural: 'Cm_domains',
      path: 'Cm_domains',
      properties: {
        "domain_id": {
          name: 'domain_id',
          type: 'number'
        },
        "team_id": {
          name: 'team_id',
          type: 'number'
        },
        "parent_domain_id": {
          name: 'parent_domain_id',
          type: 'number'
        },
        "depth": {
          name: 'depth',
          type: 'number'
        },
        "domain_name": {
          name: 'domain_name',
          type: 'string'
        },
        "descript": {
          name: 'descript',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        children: {
          name: 'children',
          type: 'Cm_domain_child[]',
          model: 'Cm_domain_child'
        },
      }
    }
  }
}
