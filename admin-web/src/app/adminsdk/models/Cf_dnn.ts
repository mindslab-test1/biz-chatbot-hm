/* tslint:disable */
import {
  Chatbot_info
} from '../index';

declare var Object: any;
export interface Cf_dnnInterface {
  "dnn_id"?: number;
  "bot_id": number;
  chatbotInfo?: Chatbot_info;
}

export class Cf_dnn implements Cf_dnnInterface {
  "dnn_id": number;
  "bot_id": number;
  chatbotInfo: Chatbot_info;
  constructor(data?: Cf_dnnInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cf_dnn`.
   */
  public static getModelName() {
    return "Cf_dnn";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cf_dnn for dynamic purposes.
  **/
  public static factory(data: Cf_dnnInterface): Cf_dnn{
    return new Cf_dnn(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cf_dnn',
      plural: 'Cf_dnns',
      path: 'Cf_dnns',
      properties: {
        "dnn_id": {
          name: 'dnn_id',
          type: 'number'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
      },
      relations: {
        chatbotInfo: {
          name: 'chatbotInfo',
          type: 'Chatbot_info',
          model: 'Chatbot_info'
        },
      }
    }
  }
}
