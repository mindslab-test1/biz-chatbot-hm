/* tslint:disable */

declare var Object: any;
export interface Front_user_commentInterface {
  "comment_id"?: number;
  "user_id": string;
  "bot_id": string;
  "channel": string;
  "title": string;
  "content": string;
  "crated_dtm": Date;
  "updated_dtm": Date;
}

export class Front_user_comment implements Front_user_commentInterface {
  "comment_id": number;
  "user_id": string;
  "bot_id": string;
  "channel": string;
  "title": string;
  "content": string;
  "crated_dtm": Date;
  "updated_dtm": Date;
  constructor(data?: Front_user_commentInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Front_user_comment`.
   */
  public static getModelName() {
    return "Front_user_comment";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Front_user_comment for dynamic purposes.
  **/
  public static factory(data: Front_user_commentInterface): Front_user_comment{
    return new Front_user_comment(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Front_user_comment',
      plural: 'Front_user_comments',
      path: 'Front_user_comments',
      properties: {
        "comment_id": {
          name: 'comment_id',
          type: 'number'
        },
        "user_id": {
          name: 'user_id',
          type: 'string'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'string'
        },
        "channel": {
          name: 'channel',
          type: 'string'
        },
        "title": {
          name: 'title',
          type: 'string'
        },
        "content": {
          name: 'content',
          type: 'string'
        },
        "crated_dtm": {
          name: 'crated_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
      },
      relations: {
      }
    }
  }
}
