/* tslint:disable */

declare var Object: any;
export interface Monitor_serverInterface {
  "mon_id"?: number;
  "server_name": string;
  "cpu_used": number;
  "mem_total": number;
  "mem_usage": number;
  "mem_used": number;
  "disk_total": number;
  "disk_usage": number;
  "disk_used": number;
  "crated_dtm": Date;
}

export class Monitor_server implements Monitor_serverInterface {
  "mon_id": number;
  "server_name": string;
  "cpu_used": number;
  "mem_total": number;
  "mem_usage": number;
  "mem_used": number;
  "disk_total": number;
  "disk_usage": number;
  "disk_used": number;
  "crated_dtm": Date;
  constructor(data?: Monitor_serverInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Monitor_server`.
   */
  public static getModelName() {
    return "Monitor_server";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Monitor_server for dynamic purposes.
  **/
  public static factory(data: Monitor_serverInterface): Monitor_server{
    return new Monitor_server(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Monitor_server',
      plural: 'Monitor_servers',
      path: 'Monitor_servers',
      properties: {
        "mon_id": {
          name: 'mon_id',
          type: 'number'
        },
        "server_name": {
          name: 'server_name',
          type: 'string'
        },
        "cpu_used": {
          name: 'cpu_used',
          type: 'number'
        },
        "mem_total": {
          name: 'mem_total',
          type: 'number'
        },
        "mem_usage": {
          name: 'mem_usage',
          type: 'number'
        },
        "mem_used": {
          name: 'mem_used',
          type: 'number'
        },
        "disk_total": {
          name: 'disk_total',
          type: 'number'
        },
        "disk_usage": {
          name: 'disk_usage',
          type: 'number'
        },
        "disk_used": {
          name: 'disk_used',
          type: 'number'
        },
        "crated_dtm": {
          name: 'crated_dtm',
          type: 'Date'
        },
      },
      relations: {
      }
    }
  }
}
