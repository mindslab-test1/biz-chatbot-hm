/* tslint:disable */

declare var Object: any;
export interface Srch_autocompleteInterface {
  "ID"?: number;
  "AUTO_KEYWORD"?: string;
  "CATEGORY"?: string;
  "TYPE"?: string;
  "SCORE"?: number;
}

export class Srch_autocomplete implements Srch_autocompleteInterface {
  "ID": number;
  "AUTO_KEYWORD": string;
  "CATEGORY": string;
  "TYPE": string;
  "SCORE": number;
  constructor(data?: Srch_autocompleteInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Srch_autocomplete`.
   */
  public static getModelName() {
    return "Srch_autocomplete";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Srch_autocomplete for dynamic purposes.
  **/
  public static factory(data: Srch_autocompleteInterface): Srch_autocomplete{
    return new Srch_autocomplete(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Srch_autocomplete',
      plural: 'Srch_autocompletes',
      path: 'Srch_autocompletes',
      properties: {
        "ID": {
          name: 'ID',
          type: 'number'
        },
        "AUTO_KEYWORD": {
          name: 'AUTO_KEYWORD',
          type: 'string'
        },
        "CATEGORY": {
          name: 'CATEGORY',
          type: 'string'
        },
        "TYPE": {
          name: 'TYPE',
          type: 'string'
        },
        "SCORE": {
          name: 'SCORE',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
