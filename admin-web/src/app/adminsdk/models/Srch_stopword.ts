/* tslint:disable */

declare var Object: any;
export interface Srch_stopwordInterface {
  "STOP_NO"?: number;
  "STOPWORD": string;
}

export class Srch_stopword implements Srch_stopwordInterface {
  "STOP_NO": number;
  "STOPWORD": string;
  constructor(data?: Srch_stopwordInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Srch_stopword`.
   */
  public static getModelName() {
    return "Srch_stopword";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Srch_stopword for dynamic purposes.
  **/
  public static factory(data: Srch_stopwordInterface): Srch_stopword{
    return new Srch_stopword(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Srch_stopword',
      plural: 'Srch_stopwords',
      path: 'Srch_stopwords',
      properties: {
        "STOP_NO": {
          name: 'STOP_NO',
          type: 'number'
        },
        "STOPWORD": {
          name: 'STOPWORD',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
