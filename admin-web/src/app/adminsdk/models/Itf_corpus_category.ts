/* tslint:disable */

declare var Object: any;
export interface Itf_corpus_categoryInterface {
  "cor_id"?: number;
  "bot_id": number;
  "domain_id": number;
  "intent_name": string;
  "question": string;
  "sds_domain": string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  "trainted_dtm"?: Date;
}

export class Itf_corpus_category implements Itf_corpus_categoryInterface {
  "cor_id": number;
  "bot_id": number;
  "domain_id": number;
  "intent_name": string;
  "question": string;
  "sds_domain": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  "trainted_dtm": Date;
  constructor(data?: Itf_corpus_categoryInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Itf_corpus_category`.
   */
  public static getModelName() {
    return "Itf_corpus_category";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Itf_corpus_category for dynamic purposes.
  **/
  public static factory(data: Itf_corpus_categoryInterface): Itf_corpus_category{
    return new Itf_corpus_category(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Itf_corpus_category',
      plural: 'Itf_corpus_categories',
      path: 'Itf_corpus_categories',
      properties: {
        "cor_id": {
          name: 'cor_id',
          type: 'number'
        },
        "bot_id": {
          name: 'bot_id',
          type: 'number'
        },
        "domain_id": {
          name: 'domain_id',
          type: 'number'
        },
        "intent_name": {
          name: 'intent_name',
          type: 'string'
        },
        "question": {
          name: 'question',
          type: 'string'
        },
        "sds_domain": {
          name: 'sds_domain',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
        "trainted_dtm": {
          name: 'trainted_dtm',
          type: 'Date'
        },
      },
      relations: {
      }
    }
  }
}
