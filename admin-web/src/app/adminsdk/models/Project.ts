/* tslint:disable */

declare var Object: any;
export interface ProjectInterface {
  "SEQ_NO"?: number;
  "PROJECT_NAME"?: string;
  "DESCRIPTION"?: string;
  "USER_SEQ_NO"?: number;
  "ITF_YN": string;
}

export class Project implements ProjectInterface {
  "SEQ_NO": number;
  "PROJECT_NAME": string;
  "DESCRIPTION": string;
  "USER_SEQ_NO": number;
  "ITF_YN": string;
  constructor(data?: ProjectInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Project`.
   */
  public static getModelName() {
    return "Project";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Project for dynamic purposes.
  **/
  public static factory(data: ProjectInterface): Project{
    return new Project(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Project',
      plural: 'Projects',
      path: 'Projects',
      properties: {
        "SEQ_NO": {
          name: 'SEQ_NO',
          type: 'number'
        },
        "PROJECT_NAME": {
          name: 'PROJECT_NAME',
          type: 'string'
        },
        "DESCRIPTION": {
          name: 'DESCRIPTION',
          type: 'string'
        },
        "USER_SEQ_NO": {
          name: 'USER_SEQ_NO',
          type: 'number'
        },
        "ITF_YN": {
          name: 'ITF_YN',
          type: 'string',
          default: 'N'
        },
      },
      relations: {
      }
    }
  }
}
