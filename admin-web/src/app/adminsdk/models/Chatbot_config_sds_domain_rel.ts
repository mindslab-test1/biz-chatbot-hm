/* tslint:disable */
import {
  Chatbot_config
} from '../index';

declare var Object: any;
export interface Chatbot_config_sds_domain_relInterface {
  "conf_id": number;
  "domain_id": number;
  "domain_name": string;
  chatbotConfig?: Chatbot_config;
}

export class Chatbot_config_sds_domain_rel implements Chatbot_config_sds_domain_relInterface {
  "conf_id": number;
  "domain_id": number;
  "domain_name": string;
  chatbotConfig: Chatbot_config;
  constructor(data?: Chatbot_config_sds_domain_relInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Chatbot_config_sds_domain_rel`.
   */
  public static getModelName() {
    return "Chatbot_config_sds_domain_rel";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Chatbot_config_sds_domain_rel for dynamic purposes.
  **/
  public static factory(data: Chatbot_config_sds_domain_relInterface): Chatbot_config_sds_domain_rel{
    return new Chatbot_config_sds_domain_rel(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Chatbot_config_sds_domain_rel',
      plural: 'Chatbot_config_sds_domain_rels',
      path: 'Chatbot_config_sds_domain_rels',
      properties: {
        "conf_id": {
          name: 'conf_id',
          type: 'number'
        },
        "domain_id": {
          name: 'domain_id',
          type: 'number'
        },
        "domain_name": {
          name: 'domain_name',
          type: 'string'
        },
      },
      relations: {
        chatbotConfig: {
          name: 'chatbotConfig',
          type: 'Chatbot_config',
          model: 'Chatbot_config'
        },
      }
    }
  }
}
