/* tslint:disable */
import {
  Cm_user
} from '../index';

declare var Object: any;
export interface Cm_login_historyInterface {
  "history_id"?: number;
  "remote"?: string;
  "agent"?: string;
  "signed_yn"?: boolean;
  "msg"?: string;
  "user_id"?: string;
  "created_dtm"?: Date;
  users?: Cm_user;
}

export class Cm_login_history implements Cm_login_historyInterface {
  "history_id": number;
  "remote": string;
  "agent": string;
  "signed_yn": boolean;
  "msg": string;
  "user_id": string;
  "created_dtm": Date;
  users: Cm_user;
  constructor(data?: Cm_login_historyInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_login_history`.
   */
  public static getModelName() {
    return "Cm_login_history";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_login_history for dynamic purposes.
  **/
  public static factory(data: Cm_login_historyInterface): Cm_login_history{
    return new Cm_login_history(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_login_history',
      plural: 'Cm_login_histories',
      path: 'Cm_login_histories',
      properties: {
        "history_id": {
          name: 'history_id',
          type: 'number'
        },
        "remote": {
          name: 'remote',
          type: 'string'
        },
        "agent": {
          name: 'agent',
          type: 'string'
        },
        "signed_yn": {
          name: 'signed_yn',
          type: 'boolean'
        },
        "msg": {
          name: 'msg',
          type: 'string'
        },
        "user_id": {
          name: 'user_id',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date',
          default: new Date(0)
        },
      },
      relations: {
        users: {
          name: 'users',
          type: 'Cm_user',
          model: 'Cm_user'
        },
      }
    }
  }
}
