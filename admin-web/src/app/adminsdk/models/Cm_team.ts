/* tslint:disable */
import {
  Cm_user,
  Cm_workspace_team
} from '../index';

declare var Object: any;
export interface Cm_teamInterface {
  "team_id"?: number;
  "team_name"?: string;
  "descript"?: string;
  "created_dtm"?: Date;
  "updated_dtm"?: Date;
  "creator_id"?: string;
  "updator_id"?: string;
  users?: Cm_user[];
  workspaces?: any[];
  workspaceRel?: Cm_workspace_team[];
}

export class Cm_team implements Cm_teamInterface {
  "team_id": number;
  "team_name": string;
  "descript": string;
  "created_dtm": Date;
  "updated_dtm": Date;
  "creator_id": string;
  "updator_id": string;
  users: Cm_user[];
  workspaces: any[];
  workspaceRel: Cm_workspace_team[];
  constructor(data?: Cm_teamInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cm_team`.
   */
  public static getModelName() {
    return "Cm_team";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cm_team for dynamic purposes.
  **/
  public static factory(data: Cm_teamInterface): Cm_team{
    return new Cm_team(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cm_team',
      plural: 'Cm_teams',
      path: 'Cm_teams',
      properties: {
        "team_id": {
          name: 'team_id',
          type: 'number'
        },
        "team_name": {
          name: 'team_name',
          type: 'string'
        },
        "descript": {
          name: 'descript',
          type: 'string'
        },
        "created_dtm": {
          name: 'created_dtm',
          type: 'Date'
        },
        "updated_dtm": {
          name: 'updated_dtm',
          type: 'Date'
        },
        "creator_id": {
          name: 'creator_id',
          type: 'string'
        },
        "updator_id": {
          name: 'updator_id',
          type: 'string'
        },
      },
      relations: {
        users: {
          name: 'users',
          type: 'Cm_user[]',
          model: 'Cm_user'
        },
        workspaces: {
          name: 'workspaces',
          type: 'any[]',
          model: ''
        },
        workspaceRel: {
          name: 'workspaceRel',
          type: 'Cm_workspace_team[]',
          model: 'Cm_workspace_team'
        },
      }
    }
  }
}
