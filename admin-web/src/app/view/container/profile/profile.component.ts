import { Component, OnInit, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { Cm_userApi, Cm_roleApi, Cm_teamApi, Cm_user, LoopBackFilter, Cm_team, Cm_role } from '../../../adminsdk';
import { CommonUtilService, CommonModalService, MenuService, AuthService, AlertModalEvent } from '../../../service';
import { Router } from '@angular/router';
import {environment as env, environment} from '../../../environments/environment';
import { Option } from '../../../model/dom-models';
import { UserService } from '../../../service/user.service';
import { DateService } from '../../../service/date.service';
import { Msg, lang } from '../../../model/message';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(
    private router: Router,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService,
    private dateService: DateService,
    private authService: AuthService,
    private userService: UserService,
    private menuService: MenuService,
    private teamApi: Cm_teamApi,
    private roleApi: Cm_roleApi,
    private userApi: Cm_userApi
  ) { }

  ngOnInit() {
    this.init();
  }

  userId = this.userService.getCurrentUserId();
  currUserInfo: Cm_user = this.userService.getCachedUserInfo();
  isAdmin = false;
  isSupervisor = false;

  @ViewChild('upsertDialogUserId')
  upsertDialogUserId: ElementRef;
  @ViewChild('upsertDialogEmail')
  upsertDialogEmail: ElementRef;
  @ViewChild('upsertDialogName')
  upsertDialogName: ElementRef;
  @ViewChild('upsertDialogPassword')
  upsertDialogPassword: ElementRef;
  @ViewChild('upsertDialogPasswordConfirm')
  upsertDialogPasswordConfirm: ElementRef;
  @ViewChild('upsertDialogActivatedYn')
  upsertDialogActivatedYn: ElementRef;
  _activatedYn:boolean = true;
  get activatedYn() {
    return this._activatedYn;
  }
  set activatedYn(activatedYn) {
    this._activatedYn = activatedYn;
  }
  selectedYn() {
    return this._activatedYn ? 'Y' : 'N';
  }
  isActivated(activatedYn) {
    return (activatedYn == 'Y') ? true : false;
  }
  isChangePassword(){
    return this.cm.isNotEmpty(this.upsertDialogPassword.nativeElement.value);
  }
  isNotChangePassword(){
    return !this.isChangePassword();
  }

  data: Cm_user; //code list

  // selectedTeam: Cm_team;
  teamList: Option[] = [];
  selectedTeamId: number = -1;
  // selectedGroup: Cm_role;
  groupList: Option[] = [];
  selectedGroupId: string = '';
  init(){
    const item = this.data = this.currUserInfo;
    this._isAdmin(()=>{
      this.initOptions();
     /* if(this.isSupervisor) {
        this.selectedTeamId = this.userService.getTeamId();
      } else {
        this.selectedTeamId = item.team.team_id;
      }*/



      this.selectedGroupId = item.role_id;
      this.activatedYn = this.isActivated(item.activated_yn);
      this.upsertDialogUserId.nativeElement.value = item.user_id;
      this.upsertDialogEmail.nativeElement.value = item.email;
      this.upsertDialogName.nativeElement.value = item.user_name;
      this.upsertDialogPassword.nativeElement.value = '';
      this.upsertDialogPasswordConfirm.nativeElement.value = '';
    });
  }

  _isAdmin = (callback = (): void=>{}) => {
    const item: Cm_user = this.currUserInfo;
    this.roleApi.isAdmin(item.role_id, this.userId).subscribe(res=>{
      console.log('res',res);
      this.isAdmin = res;
      if(!res) {
        callback();
        return;
      }

      this.roleApi.isSupervisor(item.role_id).subscribe(_res=>{
        console.log('_res',_res);
        this.isSupervisor = _res;
        callback();
      }, _err=>{
        this.commonModalService.openErrorAlert(_err);
      });
    }, err=>{
      this.commonModalService.openErrorAlert(err);
    });
  };
  exclude = this.userService.exclude;

  initOptions() {
      const args = [
          this.userService.getWorkSpace().pop()
      ];


    this.teamApi.selectTeamByWorkspace(...args).subscribe((list: Cm_team[]) => {
      const myTeam = list.filter((item) => item.team_id === this.userService.getCurrentUserTeamId());
      console.log('userTeam :' + myTeam);
      this.teamList = myTeam.map((el: Cm_team) => {
        const item: Option = {
          label: el.team_name,
          value: el.team_id
        };
        return item;
      });

      this.selectedTeamId = this.teamList[0].value;

    },error => {
      this.commonModalService.openErrorAlert(error);
    });

    const filterRole: LoopBackFilter = {
        where : { role_id : this.currUserInfo.role_id },
        order : 'order asc'
      };
    this.roleApi.find( filterRole ).subscribe((list: Cm_role[]) => {
        const myRole = list.filter((item) => item.role_id === this.selectedGroupId);
        this.groupList = myRole.map((el: Cm_role) => {
        const item: Option = {
          label: el.role_name,
          value: el.role_id
        };
        return item;
      });

        console.log(this.groupList);

    },error=>{
      this.commonModalService.openErrorAlert(error);
    });
  }
  onTeamListSelect(event) {
    console.log(event);
  }
  onGroupListSelect(event){
    console.log(event);

  }


  update(event) {
    console.log('update event =>',event);
    this.commonModalService.openConfirmDialog(Msg.com[lang].confirmUpdate, ()=>{
      this._update();
    });
  }


  _update() {

    this.data.updated_dtm = this.dateService.getNowDate();
    this.data.updator_id = this.userId;
    this._setParams();
    this.validate(() => {
      // this.userApi.updateAttributes(this.data.user_id, this.data).subscribe();
      if (this.isChangePassword()) {
        const workspaceId = this.userService.getWorkSpace().pop();
        this.userApi.checkLastPasswords(this.data.user_id, this.data.password,
                                         workspaceId, this.data.team_id ).subscribe( res => {
           if (!res) {
             this.updateUserProfile();
           } else {
               this.commonModalService.openErrorAlert('이전에 사용한 암호입니다. 다른패스워드를 입력하십시오.');
           }
        });
      } else {
        this.updateUserProfile();
      }
    });
  }


  updateUserProfile() {

      const filter: LoopBackFilter = {
          where: {
              user_id: this.data.user_id,
              team_id: this.data.team_id
          }
      };

      this.userApi.upsertWithWhere(filter.where, this.data).subscribe(
          res => {
              this.commonModalService.openSuccessAlert(Msg.com[lang].succUpdateData);
              if(this.isNotChangePassword()) {
                  this.goHome();
                  return;
              }

              this.userService.login({
                  username: this.userId, password: this.data.password
              }).subscribe(subRes => {
                  this.cm.isDev() && console.log('login...');
                  this.menuService.init(()=>{
                      this.goHome();
                  }, subError => {
                      this.commonModalService.openErrorAlert(subError);
                  });
              }, error => {
                  this.authService.clear();
                  this.commonModalService.openErrorAlert(error);
              });
          }, err => {
              this.commonModalService.openErrorAlert(err);
      });
  }

  /**
   * via biz-web/..../auth.service.ts
   * 가정: 할당 받은 워크스페이스가 없으면, 애초에 로그인 할 수 없다
   */
  goHome(): void {
    requestAnimationFrame(()=>{
      const user = this.userService.getCachedUserInfo();
      if (user) {
        const roleId = this.userService.getUserRoleId();

        if (roleId && roleId !== this.userService.getRoles.ADMIN) {
          this.router.navigateByUrl(env.defaultPath2);
        } else {
          this.router.navigateByUrl(env.defaultPath);
        }
      } else {
        this.router.navigateByUrl(environment.root);
      }
    });
  }
  private _setParams() {
    this.data.team_id = this.selectedTeamId;
    this.data.role_id = this.selectedGroupId;
    this.data.user_id = this.upsertDialogUserId.nativeElement.value;
    if(this.isChangePassword()) {
      this.data.password = this.upsertDialogPassword.nativeElement.value;
    } else {
      delete this.data.password;
    }
    this.data.email = this.upsertDialogEmail.nativeElement.value;
    this.data.user_name = this.upsertDialogName.nativeElement.value;
    this.data.activated_yn = this.selectedYn();

    //loopback 특성... 제거 필요..
    this.data.username = this.upsertDialogUserId.nativeElement.value;
  }
  validate(callback: any = ()=>{}) {

    const _validate=(data)=>{

      const Validator = {
        user_id: {
          required: true,
          minLength: 4,
          maxLength: 20
        },
        team_id: {
          required: true
        },
        role_id: {
          required: true
        },
        email: {
          required: true
        },
        user_name: {
          required: true,
          minLength: 3,
          maxLength: 50
        },
        password: {
          required: true,
          minLength: 6,
          maxLength: 14
        },
        activated_yn: {
          required: true
        }
      };

      for(const id in Validator){
        if(id == 'password'){
          if(this.isNotChangePassword()){
            continue;
          }

          if(this.upsertDialogPassword.nativeElement.value != this.upsertDialogPasswordConfirm.nativeElement.value) {
            this.commonModalService.openErrorAlert(`password를 동일하게 입력해주십시오.`);
            return false;
          }else{

            if(!this.checkPassword(this.upsertDialogPassword.nativeElement.value)) {
                this.commonModalService.openErrorAlert(`패스워드는 10자 이상 영문(대, 소문자), 숫자, 특수문자가 모두 포함되어야 합니다.'`);
                return false;
            }
          }
        }
        const item = Validator[id];
        if (item.required
            && (
              data[id] == -1
              || this.cm.isEmpty(data[id])
            )
        ) {
          this.commonModalService.openErrorAlert(`${id}를 입력해주십시오.`);
          return false;
        }
        if (
             this.cm.isNotEmpty(item.minLength)
          && this.cm.isNotEmpty(item.maxLength)
          && item.minLength == item.maxLength
          && data[id].length != item.minLength
        ) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.minLength} 자리로 정확하게 입력해주십시오.`);
          return false;
        }
        if (this.cm.isNotEmpty(item.minLength) && data[id].length < item.minLength) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.minLength} 자리 이상 입력해주십시오.`);
          return false;
        }
        if (this.cm.isNotEmpty(item.maxLength) && data[id].length > item.maxLength) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.maxLength} 자리 이하 입력해주십시오.`);
          return false;
        }
      }

      return true;
    };

    if (_validate(this.data)) {
    } else {
      return false;
    }
    callback();
  }


    checkPassword(value): boolean {
        const rexp  = /^.*(?=^.{10,}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[~,!,@,#,$,*,(,),=,+,_,.,|]).*$/;
        if (!rexp.test(value)) {
            return false;
        } else {
            return true;
        }
    }







}
