import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonModalService, CommonUtilService, UtilService } from '../../../../service';
import { Router, ActivatedRoute } from '@angular/router';
import { Option } from '../../../../model/dom-models';
import { LoopBackFilter, Dlg_historyApi, Dlg_history } from '../../../../adminsdk';
import { Paginator } from '../../../../common/paginator/paginator.component';
import { environment } from '../../../../environments/environment';
import { DateService } from '../../../../service/date.service';
import { DialogLogService } from '../../../../service/dialog-log.service';
import { Msg, lang } from '../../../../model/message';
import { UserService } from '../../../../service/user.service';
import { DataService } from '../../../../service/data.service';

export interface Chatbot_sds_port_his_ex {
  session_id: string;
  bot_id?: string;
  bot_name?: string;
  open_dtm: Date;
  close_dtm: Date;
  domain_count: number;
  dialog_count: number;
  unknown_count: number;
  user_id: string;
}

@Component({
  selector: 'app-dialog-logs',
  templateUrl: './dialog-logs.component.html',
  styleUrls: ['./dialog-logs.component.scss']
})
export class DialogLogsComponent implements OnInit, OnDestroy {
  constructor(
    private router: Router
    , private activatedRoute: ActivatedRoute
    , private commonModalService: CommonModalService
    , private cm: CommonUtilService
    , private ds: DataService
    , private dateService: DateService
    , private service: DialogLogService
    , private userService: UserService
    , private api: Dlg_historyApi
    , private spreadSheetService: UtilService
  ) {
  }

  now: Date = this.dateService.getNowDate();
  private _startDate: Date = this.dateService.getDefaultStartDateBeforeOneWeek(this.now);
  get startDate(): Date {
    return this._startDate;
  }
  set startDate(startDate) {
    startDate = this.dateService.setDefaultStartTime(startDate);
    this._startDate = startDate;
  }
  private _endDate: Date = this.dateService.getDefaultEndDate(this.now);
  get endDate(): Date {
    return this._endDate;
  }
  set endDate(endDate) {
    endDate = this.dateService.setDefaultEndTime(endDate);
    this._endDate = endDate;
  }

  public optionList: Option[];
  _selectOption = '';
  get selectOption(): string {
    return this._selectOption;
  }
  set selectOption(v: string) {
    this._selectOption = v;

    this.searchStr = '';
  }
  header = [
    { id: '세션 ID', style: { width: '206px' } },
    { id: '챗봇명', style: { width: '85px' } },
    { id: '대화 시작', style: { width: '96px' } },
    { id: '대화 종료', style: { width: '96px' } },
    { id: '도메인 수', style: { width: '48px' } },
    { id: '대화 수', style: { width: '48px' } },
    { id: '미답변 수', style: { width: '48px' } },
    { id: '사용자 ID', style: { width: '46px' } }
  ];
  public items: Chatbot_sds_port_his_ex[] = [];
  public selectedSessionId: string;

  get title() {
    return Msg.com[lang].titleDialogLog;
  }
  get defaultOrderColumn() {
    return 'his_id';
  }

  get currListPath() {
    return ['dialogs', 'logs'];
  }
  get currDetailPath() {
    return ['dialogs', 'logs', 'detail'];
  }
  // orderColumn = this.defaultOrderColumn;
  orderDirection = 'desc';

  searchStr = '';
  searchOption: LoopBackFilter = {};

  private _isWorking = false;
  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if (this._isWorking === v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }

  ngOnInit() {
    this.init();
  }
  ngOnDestroy() {
    this.ds.clearData();
  }
  init() {
    this.optionList = [
        {label: 'Select', value: ''}
    ];
    for (const key in this.service.SET_SEARCH_OPTIONS) {
      const res = {label: this.service.SET_SEARCH_OPTIONS[key].label, value: key };
      this.optionList.push(res);
    }

    const previousUrl = this.cm.getPreviousUrl();
    if (!previousUrl.includes('/dialogs/logs')) {
        this.cm.clearLocalStorageItem('hash');
    }

    const hash = this.cm.getLocalStorageItem('hash');
    if (this.cm.isNotEmpty(hash)) {
        // console.log(hash);
        const searchV = this.service.parseSearch(hash);
        this.startDate = searchV.startDate;
        this.endDate = searchV.endDate;
        this.selectOption = searchV.selectOption;
        this.searchStr = searchV.searchStr;
    }

    // this.activatedRoute.params.subscribe(params => {
    //   // console.log(params);
    //
    //   if (this.cm.isNotEmpty(params[this.service.searchKey])) {
    //
    //     const searchV = this.service.parseSearch(params[this.service.searchKey]);
    //     this.startDate = searchV.startDate;
    //     this.endDate = searchV.endDate;
    //     this.selectOption = searchV.selectOption;
    //     this.searchStr = searchV.searchStr;
    //   }
    //
    // });

    this.initList();
  }

  // teamId = this.userService.getTeamIdForSupervisor();
  teamId = this.userService.getTeamId();

  paginator = new Paginator();
  initList() {
    if (this.paginator.pageSize !== this.getCurrPageSize()) {
      this.setPageSize(this.getCurrPageSize());
    }
    this.isWorking = true;
    this.getCount(() => {
      this.isWorking = false;
    });
  }

  public search(event: any = null) {
    if (this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }
    if (event === null || event.keyCode === 13) {
      this.isWorking = true;
      // this.router.navigate([environment.container, ...this.currListPath, {search: this.service.getSearchJsonStr(this.selectOption, this.searchStr, this.startDate, this.endDate)}]);
      this.setHash(this.service.getSearchJsonStr(this.selectOption, this.searchStr, this.startDate, this.endDate));
      this.getList();
    }
  }

  private setHash(value) {
      if (value !== null) {
          this.cm.setLocalStorageItem('hash', value);
      }
  }

  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    if (tempP === JSON.stringify(this.paginator)
      && this.paginator.totalPages !== 1
    ) {
      // 갱신된 값이 없으면 다시 그리지 않음.
      return true;
    }
    if (this.paginator.totalCount <= 0) {
      return true;
    }
    this.isWorking = true;
    this._getList();
  }

  selectPageSize: string = this.paginator.DEFAULT_PAGE_SIZE.toString();
  onChangePageSize(data) {
    this.selectPageSize = data.selectOption;
    if (this.paginator.pageSize !== this.getCurrPageSize()) {
      this.setPageSize(this.getCurrPageSize());
    }
    this.getList();
  }
  getCurrPageSize() {
    return Number(this.selectPageSize);
  }

  private getCount(callback?) {

    this.service.getCountObs(
      this.dateService.getMinDateStr(this.startDate),
      this.dateService.getMinDateStrEx(this.endDate),
      this.service.getSearchConditionsJsonStr(this.selectOption, this.searchStr),
      this.teamId
    ).subscribe(res => {
      // filter
      if (this.cm.isEmpty(res)) {
        this.isWorking = false;
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        return false;
      }
      if (res.count <= 0) {
        console.log('no data');
        this._setItems([]);
        this.isWorking = false;
        this.setTotalCount(this.items.length);
        return false;
      }

      // logic
      const count = res.count;
      this.setTotalCount(count);


      callback && callback();


    }, err => {
      this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  }
  private getList() {
    this.isWorking = true;
    this.getCount(() => {
      if (this.paginator.totalCount <= 0) {
        this._setItems([]);
        this.isWorking = false;
        return false;
      }
      this._getList();
    });
  }
  private _setItems = (result: Chatbot_sds_port_his_ex[]) => {
    this.items = result;
  }
  private _getList = (callback = (): void => {}) => {
    this.setSearchOption();

    this._setItems([]);
    this.service.getListObs(
      this.paginator.currentPage,
      this.getCurrPageSize(),
      this.dateService.getMinDateStr(this.startDate),
      this.dateService.getMinDateStrEx(this.endDate),
      this.service.getSearchConditionsJsonStr(this.selectOption, this.searchStr),
      this.teamId
    ).subscribe((res: Chatbot_sds_port_his_ex[]) => {
      if (res) {
        this._setItems(res);
        this.isWorking = false;
        callback();
      }
      this.isWorking = false;
    }, err => {
      this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  }
  setSearchOption() {
    this.searchOption.skip = this.paginator.pageIndex * this.paginator.pageSize;
    this.searchOption.limit = this.paginator.pageSize;
    // this.searchOption.order = `${this.orderColumn} ${this.orderDirection}`;
  }
  setTotalCount(count: number) {
    this.paginator.totalCount = count;
  }
  setPageSize(pageSize: number) {
    this.paginator.pageSize = pageSize;
  }

  public modelSelect(session_id, team_id) {
    // console.log('team_id :' + team_id);
    this.setHash(this.service.getSearchJsonStr(this.selectOption, this.searchStr, this.startDate, this.endDate));
    this.router.navigate([environment.container, ...this.currDetailPath, session_id]);
    // this.router.navigate([environment.container,...this.currDetailPath,session_id,{search: this.service.getSearchJsonStr(this.selectOption, this.searchStr, this.startDate, this.endDate), team_id : team_id}]);
  }


  private getFilter(targetSessionIds) {
    const {order} = this.searchOption;
    const filter: LoopBackFilter = {
      where: {
        and: [
          {
            session_id: {
              neq: 'system.0000000000'
            }
          }
        ],
        timestamp: {
           gte: this.dateService.getMinDateStr(this.startDate)
          , lte: this.dateService.getMinDateStrEx(this.endDate)
        },
      }
      , order
    };

    // 검색 조건
    const searchConditions = this.service.getSearchConditions(this.selectOption, this.searchStr);
    const parsedSearchConditions = this.service.parseSearchConditions(searchConditions);
    filter.where.and.push(...parsedSearchConditions);

    if (this.selectOption !== this.service.SET_SEARCH_OPTIONS.session_id.key) {
      filter.where.and.push({session_id : {inq: targetSessionIds}});
    }
    return filter;
  }

  public downloadFile() {
    this._downloadFile();
  }

  private _finishWorking = () => {
    this.commonModalService.closeAlertDialog();
    this.isWorking = false;
  }

  private _downloadFile() {
    let items: any[];

    this.commonModalService.openCommonDialogOnlyMsg('엑셀 다운로드 중...', '엑셀 다운로드 중...');
    this.isWorking = true;
    this.service.getListObs(
      this.paginator.DEFAULT_CURR_PAGE,
      this.paginator.totalCount,
      this.dateService.getMinDateStr(this.startDate),
      this.dateService.getMinDateStrEx(this.endDate),
      this.service.getSearchConditionsJsonStr(this.selectOption, this.searchStr),
      this.teamId
    ).subscribe(res => {
      items = res;
    }, err => {
      this._finishWorking();
      this.commonModalService.openErrorAlert(err);
    }, () => {
      if (this.cm.isEmpty(items)) {
        this._finishWorking();
        console.log('다운로드할 데이터가 없음.');
        return;
      }

      // this.cm.isDev() && console.log("res :", res)
      const targetSessionIds = items.reduce((ids, item) => {
        ids.push(item.session_id);
        return ids;
      }, []);
      const sessionInfos = items.reduce((ids, item) => {
        ids.push(item);
        return ids;
      }, []);

      const filter: LoopBackFilter = this.getFilter(targetSessionIds);

      let resList = [];
      this.api.find(filter).subscribe((res: Dlg_history[]) => {
        if (this.cm.isEmpty(res)) {
          this._finishWorking();
          console.log('다운로드할 데이터가 없음.');
          return;
        }
        resList = this.service.setItems(res, sessionInfos);
        renderSpreadSheets(resList);
        this._finishWorking();
      }, err => {
        this._finishWorking();
        this.commonModalService.openErrorAlert(err);
      });

      const renderSpreadSheets = (resList: any[]) => {
        const headers = [
            '챗봇 ID'
          , '챗봇명'
          , '순번'
          , 'Session ID'
          , '시간'
          , '사용자 ID'
          , '대화 순서'
          , '질문'
          , '답변'
          , '엔진'
          , '도메인'
          , '미답변'
          , '버튼, 캐로셀 데이터'
        ];
        const bodys = resList.map((v) => this.cm.parseObjToArr(v));
        const dateFormat = 'YYYY-MM-DD';
        const dateStr = `${this.dateService.format(this.startDate, dateFormat)} - ${this.dateService.format(this.endDate, dateFormat)}`;
        const fileName = `DialogList ${dateStr}`;
        const sheetData: any[] = [headers, ...bodys];


        const MAX = 65536;
        this.spreadSheetService.exportExcelEx(fileName, sheetData, MAX);
        // if(bodys.length > MAX) {
        //   this.commonModalService.openErrorAlert('데이터가 너무 많아 엑셀 다운로드가 불가능합니다.');
        //   return false;
        // }
        // this.spreadSheetService.exportExcel(fileName, sheetData);
      };
    });
  }
}
