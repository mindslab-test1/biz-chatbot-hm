import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonModalService, CommonUtilService, UtilService} from '../../../../service';
import {DataService} from '../../../../service/data.service';
import {DateService} from '../../../../service/date.service';
import {UserService} from '../../../../service/user.service';
import {Dlg_historyApi, LoopBackFilter} from '../../../../adminsdk';
import {Paginator} from '../../../../common/paginator/paginator.component';
import {DialogErrorService} from '../../../../service/dialog-error.service';
import {lang, Msg} from '../../../../model/message';
import {Option} from '../../../../model/dom-models';


export interface DialogErrorChat {
    utter_id: number;
    botName: string;
    sentence: string;
    speaker: string;
    user_id: number;
    dlgTime: string;
}


export interface DialogErrorChatEx {
    utter_id: number;
    botName: string;
    sentence: string;
    user_id: number;
    dlgTime: string;
}


@Component({
    selector: './dialog-error',
    templateUrl : 'dialog-error.component.html',
    styleUrls: ['./dialog-error.component.scss']
})
export class DialogsErrorComponent implements OnInit, OnDestroy {

    constructor(private router: Router
        , private activatedRoute: ActivatedRoute
        , private commonModalService: CommonModalService
        , private cm: CommonUtilService
        , private ds: DataService
        , private dateService: DateService
        , private service: DialogErrorService
        , private userService: UserService
        , private api: Dlg_historyApi
        , private spreadSheetService: UtilService) {}


    public items: DialogErrorChat[] = [];
    searchOption: LoopBackFilter = {};

    now: Date = this.dateService.getNowDate();
    searchStr = '';

    private _startDate: Date = this.dateService.getDefaultStartDateBeforeOneWeek(this.now);
    private _endDate: Date = this.dateService.getDefaultEndDate(this.now);
    private _isWorking = false;

    teamId = this.userService.getTeamId();
    paginator = new Paginator();


    public optionList: Option[];
    _selectOption = '';

    selectPageSize: string = this.paginator.DEFAULT_PAGE_SIZE.toString();

    get selectOption(): string {
        return this._selectOption;
    }
    set selectOption(v: string) {
        this._selectOption = v;
    }

    get startDate(): Date {
        return this._startDate;
    }
    set startDate(startDate) {
        startDate = this.dateService.setDefaultStartTime(startDate);
        this._startDate = startDate;
    }

    get isWorking() {
        return this._isWorking;
    }
    set isWorking(v: boolean) {
        if (this._isWorking === v) {
            return;
        }
        this._isWorking = v;
        this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
    }


    header = [
        { id: '순번',     style: { width: '100px' } },
        { id: '챗봇',     style: { width: '85px' } },
        { id: '질문',     style: { width: '150px' } },
        { id: 'User ID', style: { width: '80px' } },
        { id: '대화시간',  style: { width: '100px' } }
    ];


    get endDate(): Date {
        return this._endDate;
    }
    set endDate(endDate) {
        console.log(endDate);
        endDate = this.dateService.setDefaultEndTime(endDate);
        this._endDate = endDate;
    }

    get title() {
        return '답변불가 대화현황';
    }

    ngOnInit() {
        this.init();
    }
    ngOnDestroy() {
        this.ds.clearData();
    }

    init() {

        this.optionList = [
            {label: 'Select', value: ''}
        ];

        for (const key in this.service.SET_SEARCH_OPTIONS) {
            const res = {label: this.service.SET_SEARCH_OPTIONS[key].label, value: key };
            this.optionList.push(res);
        }


        this.initList();
    }


    private _setItems = (result: DialogErrorChat[]) => {
        this.items = result;
    };

    initList() {
        if (this.paginator.pageSize !== this.getCurrPageSize()) {
            this.setPageSize(this.getCurrPageSize());
        }
        this.isWorking = true;
        this.getCount(() => {
            if (this.paginator.totalCount <= 0) {
                this._setItems([]);
                this.isWorking = false;
            }
       //     this._getList();
        });
    }


    private getCount(callback?) {

        this.service.getCountObs(
            this.dateService.getMinDateStr(this.startDate),
            this.dateService.getMinDateStrEx(this.endDate),
            this.service.getSearchConditionsJsonStr(this.selectOption, this.searchStr),
            this.teamId
        ).subscribe(res => {
            // filter
            if (this.cm.isEmpty(res)) {
                this.isWorking = false;
                this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
                return false;
            }
            if (res.count <= 0) {
                console.log('no data');
                this._setItems([]);
                this.isWorking = false;
                this.setTotalCount(this.items.length);
                return false;
            }

            const count = res.count;
            this.setTotalCount(count);

            callback();


        }, err => {
            this.isWorking = false;
            this.commonModalService.openErrorAlert(err);
        });
    }



    private _getList = (callback = (): void => {}) => {
        this.setSearchOption();

        this._setItems([]);
        this.service.getListObs(
            this.paginator.currentPage,
            this.getCurrPageSize(),
            this.dateService.getMinDateStr(this.startDate),
            this.dateService.getMinDateStrEx(this.endDate),
            this.service.getSearchConditionsJsonStr(this.selectOption, this.searchStr),
            this.teamId
        ).subscribe((res: DialogErrorChat[]) => {
            if (res) {
                this._setItems(res);
                this.isWorking = false;
                callback();
            }
            this.isWorking = false;
        }, err => {
            this.isWorking = false;
            this.commonModalService.openErrorAlert(err);
        });
    };


    setSearchOption() {
        this.searchOption.skip = this.paginator.pageIndex * this.paginator.pageSize;
        this.searchOption.limit = this.paginator.pageSize;
        // this.searchOption.order = `${this.orderColumn} ${this.orderDirection}`;
    }

    setTotalCount(count: number) {
        console.log(count);
        this.paginator.totalCount = count;
    }


    setPageSize(pageSize: number) {
        this.paginator.pageSize = pageSize;
    }


    onPaginator(event) {
        const tempP = JSON.stringify(this.paginator);
        this.cm.prop(this.paginator, event.paginator);

        if (tempP === JSON.stringify(this.paginator)  && this.paginator.totalPages !== 1) {
            // 갱신된 값이 없으면 다시 그리지 않음.
            return true;
        }
        if (this.paginator.totalCount <= 0) {
            return true;
        }
        this.isWorking = true;
        this._getList();
    }

    onChangePageSize(data) {
        this.selectPageSize = data.selectOption;
        if (this.paginator.pageSize !== this.getCurrPageSize()) {
            this.setPageSize(this.getCurrPageSize());
        }
        this.getList();
    }
    getCurrPageSize() {
        return Number(this.selectPageSize);
    }


    private getList() {
        this.isWorking = true;
        this.getCount(() => {
            if (this.paginator.totalCount <= 0) {
                this._setItems([]);
                this.isWorking = false;
                return false;
            }
            this._getList();
        });
    }


    public search(event: any = null) {
        if (this.isWorking) {
            this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
            return;
        }
        if (event === null || event.keyCode === 13) {
            this.isWorking = true;
            this.getList();
        }
    }


    downloadFile () {
        this._downloadFile();
    }

    private _finishWorking = () => {
        this.commonModalService.closeAlertDialog();
        this.isWorking = false;
    };


    private _downloadFile() {

        const renderSpreadSheets = (resList: any[]) => {
            const headers = [
                '순번'
                , '챗봇'
                , '질문'
                , 'User ID'
                , '대화시간'
            ];
            const bodys = resList.map((v) => this.cm.parseObjToArr(v));
            const dateFormat = 'YYYY-MM-DD';
            const dateStr = `${this.dateService.format(this.startDate, dateFormat)} - ${this.dateService.format(this.endDate, dateFormat)}`;
            const fileName = `DialogErrorList ${dateStr}`;
            const sheetData: any[] = [headers, ...bodys];


            const MAX = 65536;
            this.spreadSheetService.exportExcelEx(fileName, sheetData, MAX);
        };


       let items: any[];

        this.commonModalService.openCommonDialogOnlyMsg('엑셀 다운로드 중...', '엑셀 다운로드 중...');
        this.isWorking = true;
        console.log(this.paginator.totalCount);
        this.service.getListObs(
            this.paginator.DEFAULT_CURR_PAGE,
            this.paginator.totalCount,
            this.dateService.getMinDateStr(this.startDate),
            this.dateService.getMinDateStrEx(this.endDate),
            this.service.getSearchConditionsJsonStr(this.selectOption, this.searchStr),
            this.teamId
        ).subscribe((res: DialogErrorChat[]) => {
            items = res;
        }, err => {
            this._finishWorking();
            this.commonModalService.openErrorAlert(err);
        }, () => {
            if (this.cm.isEmpty(items)) {
                this._finishWorking();
                console.log('다운로드할 데이터가 없음.');
                return;
            }

            const resList = this.setExcelItems(items);
            renderSpreadSheets(resList);
            this._finishWorking();



        });
    }

    private setExcelItems(result) {

        const resList = result.reduce((res, item: DialogErrorChat, idx) => {

            const v: DialogErrorChatEx = {
                utter_id: item.utter_id,
                botName: item.botName,
                sentence: item.sentence,
                user_id: item.user_id,
                dlgTime: this.dateService.format(new Date(item.dlgTime))
            };
            res.push(v);
            return res;
        }, []);

        return resList;
    }

}
