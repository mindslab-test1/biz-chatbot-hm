import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Dlg_history, Dlg_historyApi, LoopBackFilter, Chatbot_info, Chatbot_rich_content } from '../../../../adminsdk';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonModalService, CommonUtilService } from '../../../../service';
import { environment } from '../../../../environments/environment';
import { Paginator } from '../../../../common/paginator/paginator.component';
import { Speaker, MessageType, Type, Message } from '../../../../model/chatbot-message';
import { Chatbot_sds_port_his_ex } from './dialog-logs.component';
import { DialogLogService } from '../../../../service/dialog-log.service';
import { Msg, lang } from '../../../../model/message';
import { IChatbotConst } from '../../../../model/chatbot-const';
import { CommonModalComponent } from '../../../../common';
import { ChatbotDialogService } from '../../../../service/chatbot-dialog.service';
import { RespDialog, ChatbotRichContent } from '../../../../model/response-dialog';
import { DateService } from '../../../../service/date.service';
import { DataService } from '../../../../service/data.service';

export interface Dlg_history_ex {
  bot_id: number;
  bot_name: string;
  utter_id: number;
  session_id: string;
  timestamp: Date;
  user_id: string;
  dialog_seq: number;
  question: string;
  answer: string;
  content_json: string;
  domain_name: string;
  msg_type_cd: string;
  dlg_step: string;
}

@Component({
  selector: 'app-dialog-logs-detail',
  templateUrl: './dialog-logs-detail.component.html',
  styleUrls: ['./dialog-logs-detail.component.scss']
})
export class DialogLogsDetailComponent implements OnInit, OnDestroy {
  constructor(
      private router: Router
    , private activatedRoute: ActivatedRoute
    , private commonModalService: CommonModalService
    , private cm: CommonUtilService
    , private ds: DataService
    , private dateService: DateService
    , private api: Dlg_historyApi
    , private service: DialogLogService
    , private dialogService: ChatbotDialogService
  ) {
  }

  public session_id = '';
  orderColumn = 'dialog_seq';

  public startDate: Date;
  public endDate: Date;

  selectOption: string = '';

  public items: Dlg_history_ex[] = [];

  get title(){
    return '대화 이력 상세';
  }
  get currListPath(){
    return ['dialogs', 'logs'];
  }
  searchJsonStr = '';


  searchStr = '';
  searchOption: LoopBackFilter = {};

  cc = IChatbotConst;

  team_id: number;

  ngOnInit() {
    this.init();
  }
  ngOnDestroy() {
    this.ds.clearData();
  }

  private _isWorking: boolean = false;
  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if(this._isWorking == v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }

  init() {
    this.activatedRoute.params.subscribe(params => {
      // console.log(params);
      this.session_id = params[this.service.SET_SEARCH_OPTIONS.session_id.key];
      // this.session_id = this.activatedRoute.snapshot.paramMap.get('session_id');
      if (this.cm.isEmpty(this.session_id)) {
        this.commonModalService.openErrorAlert(Msg.err[lang].invalidAccess);
        return;
      }

      // this.team_id = params[this.service.teamIdKey];
      this.team_id = parseInt(this.cm.getLocalStorageItem('teamId'));

      if (this.cm.isEmpty(this.team_id)) {
        this.commonModalService.openErrorAlert(Msg.err[lang].invalidAccess);
        return;
      }

      // if (this.cm.isNotEmpty(params[this.service.searchKey])) {
      //
      //   this.searchJsonStr = params[this.service.searchKey];
      //
      //   const searchV = this.service.parseSearch(this.searchJsonStr);
      //   this.startDate = searchV.startDate;
      //   this.endDate = searchV.endDate;
      //   this.selectOption = searchV.selectOption;
      //   this.searchStr = searchV.searchStr;
      // }

      const hash = this.cm.getLocalStorageItem('hash');
      if (this.cm.isNotEmpty(hash)) {
          // console.log(hash);
          const searchV = this.service.parseSearch(hash);
          this.startDate = searchV.startDate;
          this.endDate = searchV.endDate;
          this.selectOption = searchV.selectOption;
          this.searchStr = searchV.searchStr;
      }

      this.initList();
    });
  }
  paginator = new Paginator();
  initList(){
    this.isWorking = true;
    this.service.getItemObs(
      this.dateService.getMinDateStr(this.startDate),
      this.dateService.getMinDateStrEx(this.endDate),
      this.selectOption, this.searchStr, this.session_id, this.team_id
    ).subscribe((res: Chatbot_sds_port_his_ex[])=>{
      if(!res || res.length != 1){
        this.commonModalService.openErrorAlert(Msg.err[lang].invalidAccess);
        this.isWorking = false;
        return;
      }
      this._setParent(res[0]);

      if(this.paginator.pageSize != this.getCurrPageSize()) {
        this.setPageSize(this.getCurrPageSize());
      }
      this.getCount(()=>{
        this.isWorking = false;
      });
    }, err=>{
      this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  }

  vm;
  private _setParent = (result: Chatbot_sds_port_his_ex)=>{
    this.vm = this.cm.parseObjToArrEx(result);
  };

  public goToList() {
    // this.router.navigate([environment.container, ...this.currListPath, {search: this.searchJsonStr}]);
    this.router.navigate([environment.container, ...this.currListPath]);
  }
  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    if(tempP == JSON.stringify(this.paginator)
      && this.paginator.totalPages != 1
    ){
      //갱신된 값이 없으면 다시 그리지 않음.
      return true;
    }
    if(this.paginator.totalCount <= 0) {
      return true;
    }
    this.getList();
  }

  //질문 답변 페어이기 때문에 2 배수를 더 가져옴.
  dialogPairPageSize = (this.paginator.DEFAULT_PAGE_SIZE*2);
  selectPageSize: string = (this.dialogPairPageSize).toString();
  getCurrPageSize() {
    return Number(this.selectPageSize);
  }

  private getFilter() {
    const filter: LoopBackFilter = {
      where: {
        and:[
              {session_id: this.session_id},
              {team_id: this.team_id}
            ]
      },
      // skip: this.searchOption.skip > 0 ? (this.searchOption.skip - 1) : 0,
      // limit: (this.searchOption.limit + 2),
      skip: this.searchOption.skip,
      limit: this.searchOption.limit,
      order: this.searchOption.order
    };
    return filter;
  }
  getDlgPairCount(res) {
    const count = res.count;
    const cnt = Math.ceil(count/2) || 0;
    return cnt;
  }
  private getCount(callback?) {
    const filter = this.getFilter();
    // filter.where.speaker = Speaker.USER;

    this.api.count(filter.where).subscribe(res=>{
      //filter
      if(this.cm.isEmpty(res)) {
        this.isWorking = false;
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        return false;
      }
      if(res.count <= 0) {
        console.log('no data');
        this.items = this.service.setItems([]);
        this.isWorking = false;
        this.setTotalCount(this.items.length);
        return false;
      }

      //logic
      this.setTotalCount(this.getDlgPairCount(res));


      callback && callback();


    }, err=>{
      this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  }
  private getList = () => {
    this.isWorking = true;
    this.getCount(()=>{
      if(this.paginator.totalCount <= 0) {
        this.isWorking = false;
        this.items = this.service.setItems([]);
        return false;
      }
      this._getList();
    });
  };
  private _getList = (callback = (): void=>{}) => {
    this.setSearchOption();
    const filter = this.getFilter();
    this.api.find(filter).subscribe((res: Dlg_history[])=>{
      console.log('debug query res getTalkLog =====\n',res);
      if(res){
        this.isWorking = false;
        this.items = this.service.setItems(res);
        callback();
      }
      this.isWorking = false;
    }, error => {
      this.isWorking = false;
      this.commonModalService.openErrorAlert(error);
    });
  };
  setSearchOption(){

    let extraCount = 0;
    let skipCount  = 0;

      /**
       * 질문과 답변이 세트이기 때문에...
       * 첫장에서는 1 ROW 를 재껴야..
       * 두번째 부터는 첫줄에 재낀 줄을 추가해야 정상동작함.
       */
    if (this.paginator.pageIndex === 0) {
        skipCount = 0;
        extraCount = -1;
      } else {
        skipCount =  1;
        extraCount = 0;
      }


    this.searchOption.skip = this.paginator.pageIndex * this.paginator.pageSize;
    this.searchOption.limit = this.paginator.pageSize;
    this.searchOption.order = `${this.orderColumn} asc`;
  }
  setTotalCount(count: number){
    this.paginator.totalCount = count;
  }
  setPageSize(pageSize: number){
    this.paginator.pageSize = pageSize;
  }


  Type = Type;
  MessageType = MessageType;
  messages: Message[] = [];
  @ViewChild('dialogDetailModal')
  dialogDetailModal: CommonModalComponent;
  viewAnswerDetail (item: Dlg_history_ex) {
    if(this.cm.isEmpty(item)
        || this.cm.isEmpty(item.content_json)) {
      return;
    }
    const content: ChatbotRichContent = JSON.parse(item.content_json);
    const contentTypeCd = content.contentTypeCd;

    let details = [];
    if(contentTypeCd == this.cc.RICH_CONTENT_TYPE_BUTTON) {
      details = content.buttons;
    }
    if(contentTypeCd == this.cc.RICH_CONTENT_TYPE_CAROUSEL) {
      details = content.carousels;
    }
    const v: RespDialog = this.dialogService.parseRespDialog(item.bot_id, contentTypeCd, details);
    this.messages = [{
        type: Type.SERVER
      , msgType: this.dialogService.getMessageRichContentType(contentTypeCd)
      , message: item.answer
      , data: v
    }];

    this.initDialogDetail(()=>{
      this.dialogDetailModal.modalOpen();
    });
  }
  initDialogDetail(callback = ():void=>{}){
    this.dialogDetailModal.setModalStyle(795, 530);
    callback();
  }
  modalClose() {
    this.dialogDetailModal.modalClose();
  }
}
