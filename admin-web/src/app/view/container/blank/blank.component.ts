import {AfterViewChecked, Component, EventEmitter, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {environment as env} from "../../../environments/environment";

@Component({
  selector: 'app-blank',
  templateUrl: './blank.component.html',
  styleUrls: ['./blank.component.scss']
})
export class BlankComponent implements OnInit {

  constructor(  private router: Router) { }

  ngOnInit() {

      this.router.navigateByUrl(env.defaultPath);
  }

}
