import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {CommonModalService} from '../../../../service';

@Component({
  selector: 'app-index-logs',
  templateUrl: './index-logs.component.html',
  styleUrls: ['./index-logs.component.scss']
})
export class IndexLogsComponent implements OnInit {

  constructor(
      private router: Router
      , private activatedRoute: ActivatedRoute
      , private commonModalService: CommonModalService
  ) { }

  ngOnInit() {

  }

}
