import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {CommonModalService} from '../../../../service';

@Component({
  selector: 'app-index-stat',
  templateUrl: './index-stat.component.html',
  styleUrls: ['./index-stat.component.scss']
})
export class IndexStatComponent implements OnInit {

  constructor(
      private router: Router
      , private activatedRoute: ActivatedRoute
      , private commonModalService: CommonModalService
  ) { }

  ngOnInit() {

  }

}
