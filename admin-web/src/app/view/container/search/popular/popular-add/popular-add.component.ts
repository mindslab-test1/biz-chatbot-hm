
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { CommonModalService } from '../../../../../service/common-modal.service';
import {Srch_popular} from '../../../../../adminsdk/models';
import {Srch_popularApi} from '../../../../../adminsdk/services/custom';

@Component({
  selector: 'app-popular-add',
  templateUrl: './popular-add.component.html',
  styleUrls: ['./popular-add.component.scss']
})
export class PopularAddComponent implements OnInit {


  synonymlist = new Array<string>();
  synonmWord:string;
  valid:boolean = false;

  @Output() addClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal: CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  srch_popular: Srch_popular;

  constructor(private srchPopularApi: Srch_popularApi, private commonModalService: CommonModalService) {

    this.srch_popular = new Srch_popular();
  }

  ngOnInit() {

  }

  modalOpen(){
    this.commonModal.modalOpen();

    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  popularAddSave(){
    console.log(this.srch_popular);
    if(this.srch_popular== null  || this.srch_popular.KEYWORD == ""
    ){
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.srchPopularApi.upsert(this.srch_popular).subscribe(res =>{
      }, error => {
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('등록되었습니다');
          this.addClose.emit(true);
          this.modalClose();
        }
      );
    }
  }

}
