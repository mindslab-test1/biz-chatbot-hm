import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { UtilService, CommonModalService, CommonUtilService } from '../../../../service/index';
import { LoopBackFilter } from '../../../../adminsdk/models/BaseModels';
import { PopularEditComponent } from './popular-edit/popular-edit.component';
import { PopularAddComponent } from './popular-add/popular-add.component';
import { PopularRemoveComponent } from './popular-remove/popular-remove.component';
import { UserService } from '../../../../service/user.service';
import { DataService } from '../../../../service/data.service';
import { Msg, lang } from '../../../../model/message';
import { CommonModalComponent } from '../../../../common';
import { Srch_popular } from '../../../../adminsdk/models/Srch_popular';
import {Srch_popularApi} from '../../../../adminsdk/services/custom';


@Component({
    selector: 'app-popular',
    templateUrl: './popular.component.html',
    styleUrls: ['./popular.component.scss'],
})

export class PopularComponent implements OnInit, OnDestroy {

  totalCount = 0;
  totalPage = 0;
  pg = 1;
  pgSize = 10;
  selectText = '';
  items: Srch_popular[];
  selectedItems: Srch_popular[] = [];
  selectedItem: Srch_popular;
  @ViewChild('addChild') addChild: PopularAddComponent;
  @ViewChild('editChild') editChild: PopularEditComponent;
  @ViewChild('removeChild') removeChild: PopularRemoveComponent;
  private _isWorking = false;

  constructor(
      private commonModalService: CommonModalService,
      private srchPopularApi: Srch_popularApi,
      private utilService: UtilService,
      private ds: DataService,
      private cm: CommonUtilService

  ) {
  }

  ngOnInit() {
    // this.workspaceId = this.userService.getWorkspaceIds().pop();
    this.getList(1);
  }
  ngOnDestroy() {
    this.ds.clearData();
  }

  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if (this._isWorking === v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }
  onSearchKey($event) {
    if (this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }

    this.getList(1);
  }

  getList(pg) {
    this.pg = pg;
    const pgSize = this.pgSize;

    this.isWorking = true;
    this.srchPopularApi.countList( this.selectText).subscribe(res => {
      if (res[0].cnt <= 0) {
        this.isWorking = false;
        this.totalCount = 0;
        this.totalPage = 1;
        // this.commonModalService.openErrorAlert('No data');
        // console.log('no data');
      } else {
        this.totalCount = res[0].cnt;
        this.totalPage = Math.ceil(res[0].cnt / pgSize);
        const skip = (pg - 1) * pgSize;
        const limit = pgSize;
        const order = `ID asc`;
        console.log('this.totalCount' , this.totalCount);
        this.srchPopularApi.getList( skip, limit, order, this.selectText).subscribe(res => {
          console.log('res :', res);
          this.items = res;
          this.isWorking = false;
        }, subErr => {
          this.isWorking = false;
          console.log(subErr);
        });
      }
    }, err => {
      this.isWorking = false;
      console.error(err);
    }, () => {
    });
  }
  pageChanged(res) {
    console.log(res);
    this.getList(res);
  }
  popularEditOpen(res) {
    this.editChild.modalOpen(res);
  }

  removesOpen(evnet) {
    const items  = this.selectedItems;
    console.log('debug set remove', event, items);
    if (this.cm.isEmpty(items)) {
      console.log('debug remove. stop. no select items');
      return;
    }
    this.removeChild.modalOpen(items);
  }
  popularRemoveOpen(res) {
    const items = [];
    items.push(res);
    this.removeChild.modalOpen(items);
  }
  popularAddOpen() {
    this.addChild.modalOpen();
  }
  modalClose(res) {
    if (res) {
      this.getList(1);
    }
  }

  private _finishWorking = () => {
    this.commonModalService.closeAlertDialog();
    this.isWorking = false;
  };

  public uploadFile(e: any) {
    if (!e.srcElement.value) {
      return false;
    }

    if (confirm('Are you sure you want to upload the file?')) {

      this.commonModalService.openCommonDialogOnlyMsg('엑셀 업로드 중...', '엑셀 업로드 중...');
      this.isWorking = true;
      this.utilService.importExcel(e).subscribe(items => {
        console.log(items);
        const array = new Array<Srch_popular>();
        let index = 0;
        let result = 0;
        for (const item of items) {
          console.log(typeof item[0]);
          if (index > 0 ) {

            if (item[1] !== undefined) {
              const srch_popular = new Srch_popular();
              if (item[0] > 0) {
                srch_popular.ID = item[0];
              } else {
                srch_popular.ID = 0;
              }
              srch_popular.KEYWORD = item[1];
              // 중복제거 필요
              this.srchPopularApi.upsert(srch_popular).subscribe(response => {
                    console.log(response);
                    console.log('update => ID: ' + item[0] + 'KEYWORD: ' + item[1] );
                    result++;
                  }
              );
            }
          }
          index++;
        }
        this._finishWorking();
        if (result === index) {
          alert('Your request complete successfully.');
        }
        this.getList(1);
      }, error => {
        this._finishWorking();
        console.error(error);
      });
    }
  }

  public downloadFile() {
    const sheetData: any[] = [];
    let items;

    const filter: LoopBackFilter = {
      order: `ID asc`
    };

    this.commonModalService.openCommonDialogOnlyMsg('엑셀 다운로드 중...', '엑셀 다운로드 중...');
    this.isWorking = true;
    this.srchPopularApi.find(filter).subscribe(res => {
          console.log('res :', res);
          items = res;
        }, error => {
          this._finishWorking();
          console.error(error);
          // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
        },
        () => {
          if (items.length > 0) {
            sheetData.push(['ID', 'KEYWORD']);
            for (const item of items) {
              sheetData.push([item.ID, item.KEYWORD ]);
            }
            this.utilService.exportExcel('PupularList', sheetData);
          }
          this._finishWorking();
        }
    );
  }

}
