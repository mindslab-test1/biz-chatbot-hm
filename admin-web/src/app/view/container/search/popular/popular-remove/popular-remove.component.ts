import {Component, OnInit, ViewChild, Output, EventEmitter} from '@angular/core';
import {CommonModalComponent} from '../../../../../common';
import {LoopBackFilter} from '../../../../../adminsdk/models/BaseModels';
import {CommonModalService} from '../../../../../service/common-modal.service';
import {Srch_popular} from '../../../../../adminsdk/models';
import {CommonUtilService} from '../../../../../service';
import {Srch_popularApi} from '../../../../../adminsdk/services/custom';

@Component({
    selector: 'app-popular-remove',
    templateUrl: './popular-remove.component.html',
    styleUrls: ['./popular-remove.component.scss']
})
export class PopularRemoveComponent implements OnInit {

    valid: boolean = false;

    @Output() removeClose = new EventEmitter<Boolean>();
    @ViewChild('commonModal') commonModal: CommonModalComponent;

    selectedItem: Srch_popular;
    selectedItems: Srch_popular[] = [];
    items: Srch_popular[];

    constructor(
        private srchPopularApi: Srch_popularApi,
        private commonModalService: CommonModalService,
        private cm: CommonUtilService
    ) {

        this.selectedItem = new Srch_popular();
    }

    ngOnInit() {

    }

    modalOpen(res) {
        this.selectedItems = res;
        this.commonModal.modalOpen();
    }

    modalClose() {

        this.commonModal.modalClose();
    }

    remove() {
        if (this.selectedItems.length == 1) {
            this.removesItem();
        } else {
            this.removesItem();
        }
    }

    private removesItem() {
        const ids = this.selectedItems.reduce((res, ele) => {
            res.push(ele.ID);
            return res;
        }, []);
        if (this.cm.isEmpty(ids)) {
            this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
            return false;
        }
        this.srchPopularApi.deleteByIds(ids).subscribe(
            res => {
                this.commonModalService.openSuccessAlert('삭제되었습니다');
                this.removeClose.emit(true);
                this.selectedItems = [];
                this.modalClose();
            }, error => {
                console.log('delete Error');
            });
    }
}
