
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { Srch_popular, Srch_popularInterface} from '../../../../../adminsdk/models/Srch_popular';
import { CommonModalService } from '../../../../../service/common-modal.service';
import {Srch_popularApi} from '../../../../../adminsdk/services/custom';

@Component({
  selector: 'app-popular-edit',
  templateUrl: './popular-edit.component.html',
  styleUrls: ['./popular-edit.component.scss']
})
export class PopularEditComponent implements OnInit {


  valid: boolean = false;

  @Output() editClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal: CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  srch_popular: Srch_popular;

  constructor(private srchPopularApi: Srch_popularApi, private commonModalService: CommonModalService) {

    this.srch_popular = new Srch_popular();
  }

  ngOnInit() {
  }

  modalOpen(res){
    console.log(res);
    this.srch_popular = res;
    this.commonModal.modalOpen();
    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  popularEditSave(){
    if( this.srch_popular == null
    ){
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.srchPopularApi.upsert(this.srch_popular).subscribe(res =>{
          
        console.log(res);
      }, error => {
        console.error(error);
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('수정되었습니다');
          this.editClose.emit(true);
          this.modalClose();
        }
      );
    }
  }

}
