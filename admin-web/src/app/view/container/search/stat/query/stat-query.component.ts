import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonModalService, CommonUtilService} from '../../../../../service';
import {lang, Msg} from '../../../../../model/message';
import {ActivatedRoute, Router} from '@angular/router';
import {DateService} from '../../../../../service/date.service';
import {Chatbot_info, Chatbot_infoApi, Cm_user, LoopBackFilter, Monitor_serverApi, Stats_dlg_historyApi} from '../../../../../adminsdk';
import {Option, OptionEx} from '../../../../../model/dom-models';
import {UserService} from '../../../../../service/user.service';
import {chatbotStepBqa, chatbotStepSds} from '../../../../../model/chatbot-step';
import { EtlService} from '../../../../../service/etl.service';
import {DataService} from '../../../../../service/data.service';
import {Subscription} from 'rxjs';

export interface Rank {
  no?: number;
  name: string;
  count: number;
}

export interface ResponseTps {
  period: string;
  tps: number;
}
export interface ResponseDialogResult {
  period: string;
  success: number;
  fail: number;
  unknown: number;
}


@Component({
  selector: 'app-etl',
  templateUrl: 'stat-query.component.html',
  styleUrls: ['./stat-query.component.scss']
})

export class StatQueryComponent implements OnInit, OnDestroy {


  constructor(
    private router: Router
    , private activatedRoute: ActivatedRoute
    , private commonModalService: CommonModalService
    , private cm: CommonUtilService
    , private ds: DataService
    , private dateService: DateService
    , private userService: UserService
    , private api: Stats_dlg_historyApi
    , private monitorApi: Monitor_serverApi
    // , private api: Dlg_historyApi
    , private chatbotInfoApi: Chatbot_infoApi
    , private etlService: EtlService
  ) {}
/*
  data : any;
  this.data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'First Dataset',
        data: [65, 59, 80, 81, 56, 55, 40],
        fill: false,
        borderColor: '#4bc0c0'
      },
      {
        label: 'Second Dataset',
        data: [28, 48, 40, 19, 86, 27, 90],
        fill: false,
        borderColor: '#565656'
      }
    ]
  }*/

  private eventSubscription: Subscription;

  teamId: number;
  monitorServer: string[] = [];
  qnaRanks: Rank[] = [];
  scenarioRanks: Rank[] = [];

  userId = this.userService.getCurrentUserId();
  currUserInfo: Cm_user = this.userService.getCachedUserInfo();

  rangeOption = this.etlService.rangeOption;

  dd = this.etlService.default;
  set = this.etlService.SET;
  dds = this.dd.statistics;
  sc = this.set.CHART;

  private _isWorking = false;

  isWorkingStatDialogResult = false;
  isWorkingStatTps = false;
  isWorkingRank = false;
  isWorkingCpuUsed = false;
  isWorkingMemUsed = false;
  isWorkingDiskUsed = false;

  public serverList: Option[];

  ngOnInit() {
     /* this.eventSubscription = this.userService.isLoadHeaderEvent.subscribe((res) => {
          //localStorage에 저장된 teamId 가 넘어옴..
          this.teamId = res;
          this.init();
      })*/

      this.teamId = parseInt(this.userService.initTeamId);
      this.init();

  }


  ngOnDestroy() {
    this.ds.clearData();
   // this.eventSubscription.unsubscribe();
  }


  private init() {
    this.optionList = [
      {label: '챗봇 전체', value: ''}
    ];


    this.isWorking = true;
    this.initChatbotList(():void => {
      this._search();
    });
  }


  rangeByTime = () => this.etlService.rangeByTime(this.selectedRange);
  rangeByDay = () => this.etlService.rangeByDay(this.selectedRange);




  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if(this._isWorking === v) {
      return;
    }

    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }


  private initChatbotList(callback = ():void=>{ this.cm.override(); }){
    const filter: LoopBackFilter = {
       where: { and: [{ team_id: this.teamId }, { use_yn: 'Y' }] }
    };


    this.chatbotInfoApi.find(filter).subscribe((list: Chatbot_info[]) => {
      const temp = list.map((el: Chatbot_info) => {
        const item: Option = {
          label: el.bot_name,
          value: el.bot_id
        };
        return item;
      });
      this.optionList.push(...temp);

      callback();
    },error => {
      this.isWorking = false;
      this.commonModalService.openErrorAlert(error);
    });
  }

  now: Date = this.dateService.getNowDate();
  private _startDate: Date = this.dateService.getDefaultStartDate(this.now);

  get startDate(): Date {
    return this._startDate;
  }

  set startDate(startDate) {
    startDate = this.dateService.setDefaultStartTime(startDate);
    this._startDate = startDate;
  }

  private _endDate: Date = this.dateService.getDefaultEndDate(this.now);
  get endDate(): Date {
    return this._endDate;
  }
  set endDate(endDate) {
    // endDate.setDate(endDate.getDate() + 1);
    endDate = this.dateService.setDefaultEndTime(endDate);
    this._endDate = endDate;

    if(this.etlService.rangeByTime(this.selectedRange)) {
      this.startDate = this.dateService.setDefaultStartTime(this.endDate);
    } else {
      this._minDate = this.dateService.getDefaultStartDateBeforeOneMonth(this.endDate, this.set.INTERVAL);
    }
  }
  private _minDate = this.dateService.getDefaultStartDateBeforeOneMonth(this.endDate, this.set.INTERVAL);
  get minDate() {
    return this._minDate;
  }

  public optionList: Option[];
  _selectOption: string = '';
  _selectServerOption: string = '';
  _selectServerOption2: string = '';
  _selectServerOption3: string = '';

  get selectOption(): string{
    return this._selectOption;
  }
  set selectOption(v: string) {
    this._selectOption = v;
  }


  get selectServerOption(): string{
        return this._selectServerOption;
  }
  set selectServerOption(v: string) {
        this._selectServerOption = v;
  }


  get selectServerOption2(): string{
        return this._selectServerOption2;
  }
  set selectServerOption2(v: string) {
      this._selectServerOption2 = v;
  }

  get selectServerOption3(): string{
      return this._selectServerOption3;
  }
  set selectServerOption3(v: string) {
      this._selectServerOption3 = v;
  }


// 조회 시간,일자 범위1ㅂ
  private _range = this.rangeOption.day;
  get selectedRange() {
    return this._range;
  }
  set selectedRange(v) {
    this._range = v;

    if(this.etlService.rangeByTime(this.selectedRange)){
      this.startDate = this.dateService.getDefaultStartDate(this.endDate);
    } else {
      this.startDate = this.dateService.getDefaultStartDateBeforeOneMonth(this.endDate);
    }
  }

  get title () {
    return Msg.com[lang].titleDashboard;
  }

  search(event: any = null) {
    if(this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }
    this.isWorking = true;
    this._search();
  }

  _search() {
    this.renderStats();
  }

  renderStats() {


    this.getSessionData((res) => {
      this.renderStatTotal(res);
      this.isWorking = false;
    });

   this.getTpsData((res) => {
      this.renderStatTps(res);
    });

    this.getDlgResData((res) => {
      this.renderStatDialogResult(res);
    });

     this.getQnaTopNData((res) => {
      this.renderStatQnaRank(res);
      this.getScenarioTop();
      this.isWorkingRank = false;
    });


     // serverStatus.comp 로 이관..우선 흔적은 남겼음.
/*
     if (typeof this.monitorServer !== 'undefined' && this.monitorServer.length > 0) {
         const memSverver = this.selectServerOption2 || this.monitorServer[0];
         this.getMemUsed(memSverver, (res) => {
             this.renderMemUsed(res);
         });

         const cpuServer = this.selectServerOption || this.monitorServer[0];
         this.getCpuUsed(cpuServer, (res) => {
             this.renderCpuUsed(res);
             this.isWorkingCpuUsed = false;
         });

         const diskServer = this.selectServerOption3 || this.monitorServer[0];
         this.getDisksed(diskServer, (res) => {
             this.renderDiskUsed(res);
             this.isWorkingDiskUsed = false;
         });
     }*/
  }


  statistics: OptionEx[] = this.etlService.statistics;
  renderStatTotalDefault() {
    this.renderStatTotal([this.dds.mcloudCnt, this.dds.integrityCnt, this.dds.wikiCnt, this.dds.qmsCnt]);
  }
  renderStatTotal(baseData) {
    // baseData == parseData == statics
    const parseData = this.etlService.parseStatTotal(baseData);
    this.statistics = parseData;
  }
  renderStatTps(baseData) {
    if(this.cm.isEmpty(baseData)) {
      this.cm.isDev() && console.log('no data','tps');
      this.tpsChartData = [];
      this.tpsChartLabels = [];
    }


    const parseData = this.etlService.parseStatTps(baseData, this.startDate, this.endDate, this.selectedRange);
    // this.tpsChartLegend = false;
    // this.tpsChartType = 'line'

    const unit = this.etlService.getUnit(this.selectedRange);
    const max = this.etlService.getYAxesMaxTps(baseData);
    const stepSize = this.etlService.getStepSize(parseData.period, baseData);
    const format = this.etlService.getFormat(this.selectedRange);

    parseData.datas[0]['fill'] = false;
    parseData.datas[0]['borderWidth'] = 2;
    parseData.datas[0]['cubicInterpolationMode'] = 'monotone';
    this.tpsChartData = parseData.datas;
    requestAnimationFrame(()=>{
      this.tpsChartOptions = this.etlService.parseChartOptions(max, stepSize, unit, format);
      this.tpsChartLabels = parseData.period;
      this.isWorkingStatTps = false;
    });
  }
  renderStatDialogResult(res) {
    if(this.cm.isEmpty(res)) {
      this.cm.isDev() && console.log('no data','dlg res');
      this.dialogResultChartData = [];
      this.dialogResultChartLabels = [];
    }

    const parseData = this.etlService.parseStatDialogResult(res, this.startDate, this.endDate, this.selectedRange);
    // this.dialogResultChartLegend = false;
    // this.dialogResultChartType = 'line';


    const unit = this.etlService.getUnit(this.selectedRange);
    const max = this.etlService.getYAxesMaxDialogResult(res);
    const stepSize = this.etlService.getStepSize(parseData.period, res);
    const format = this.etlService.getFormat(this.selectedRange);
    this.dialogResultChartData = parseData.datas;
    requestAnimationFrame(()=>{
      this.dialogResultChartOptions = this.etlService.parseChartOptions(max, stepSize, unit, format);
      // this.cm.isDev() && console.log('dres labels', unit, max, stepSize, format , parseData);
      this.dialogResultChartLabels = parseData.period;
      this.isWorkingStatDialogResult = false;
    });
  }


  getSessionData(callback = (res): void => { this.cm.override(); }) {

    const boatIds: string[] = [];

    this.optionList.map((obj, i) => {
      if (i > 0) {
        boatIds.push(obj.value);
      }
    });


    if(typeof boatIds === 'undefined' || boatIds.length === 0 ){
      boatIds.push('-1');
    }


    const args =[
      this.dateService.getMinDateStr(this.startDate),
      this.dateService.getMinDateStrEx(this.endDate),
      boatIds.join(','),
      this.teamId
    ];
    if(this.cm.isNotEmpty(this.selectOption)){
      args.push(parseInt(this.selectOption));
    }


    // obs
    this.api.getStats(...args).subscribe(res => {
      // filter
      if(this.cm.isEmpty(res)) {
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        this.isWorking = false;
        return false;
      }
      if(res.count <= 0) {
        this.cm.isDev() && console.log('no session data');
        this.renderStatTotalDefault();
        this.isWorking = false;
        return false;
      }

      // console.log(res);

      callback && callback(res);

    }, err => {
      this.commonModalService.openErrorAlert(err);
      this.isWorking = false;
    });

  }

  getTpsData(callback = (res): void=>{ this.cm.override(); }){
    this.isWorkingStatTps = true;

     const boatIds: string[] = [];

    this.optionList.map((obj, i) =>{
      if (i > 0) {
        boatIds.push(obj.value);
      }
    });

    if (typeof boatIds === 'undefined' || boatIds.length === 0 ) {
      boatIds.push('-1');
    }

    const args = [
      this.dateService.getMinDateStr(this.startDate),
      this.dateService.getMinDateStrEx(this.endDate),
      boatIds.join(','),
      this.selectedRange,
      this.teamId
    ];
    if(this.cm.isNotEmpty(this.selectOption)){
      args.push(parseInt(this.selectOption));
    }
    this.api.getTpsChart(...args).subscribe(res => {
      // filter
      if (this.cm.isEmpty(res)) {
        // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        this.cm.isDev() && console.log(Msg.err[lang].failLoadData,'tps data');
        this.isWorking = false;
        this.isWorkingStatTps = false;
        callback([]);
        return false;
      }
      callback(res);

    }, err => {
      this.commonModalService.openErrorAlert(err);
      this.isWorking = false;
      this.isWorkingStatTps = false;
    });
  }

  getDlgResData(callback = (res): void=>{ this.cm.override(); }){
    this.isWorkingStatDialogResult = true;

    const boatIds: string[] = [];

    this.optionList.map((obj, i) => {
      if (i > 0) {
        boatIds.push(obj.value);
      }
    });

    if (typeof boatIds === 'undefined' || boatIds.length === 0 ){
      boatIds.push('-1');
    }

    const args = [
      this.dateService.getMinDateStr(this.startDate),
      this.dateService.getMinDateStrEx(this.endDate),
      boatIds.join(','),
      this.selectedRange,
      this.teamId
    ];
    if(this.cm.isNotEmpty(this.selectOption)) {
      args.push(parseInt(this.selectOption));
    }
    // obs
    this.api.getDialogChart(...args).subscribe(res => {
      // filter
      if (this.cm.isEmpty(res)) {
        // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'dlg res data');
        this.isWorking = false;
        this.isWorkingStatDialogResult = false;
        callback([]);
        return false;
      }

      callback(res);

    }, err => {
      this.commonModalService.openErrorAlert(err);
      this.isWorking = false;
      this.isWorkingStatDialogResult = false;
    });
  }




  getQnaTopNData(callback = (res): void => { this.cm.override(); }){
     this.isWorkingRank = true;

     const boatIds: string[] = [];

     this.optionList.map((obj, i) => {
      if (i > 0) {
        boatIds.push(obj.value);
      }
    });

    if(typeof boatIds === 'undefined' || boatIds.length === 0 ){
      boatIds.push('-1');
    }

    const args = [
        this.dateService.getMinDateStr(this.startDate),
        this.dateService.getMinDateStrEx(this.endDate),
        boatIds.join(','),
        this.selectedRange,
        chatbotStepBqa[1],
        this.teamId
    ];


    if (this.cm.isNotEmpty(this.selectOption)) {
      args.push(parseInt(this.selectOption));
    }
    // obs
    this.api.getTopN(...args).subscribe(res => {
      // filter
      if (this.cm.isEmpty(res)) {
        // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'dlg res data');
        this.isWorkingRank = false;
        callback([]);
        return false;
      }

      callback(res);

    }, err => {
      this.commonModalService.openErrorAlert(err);
      this.isWorkingRank = false;
    });
  }



  getScenarioTop() {
    this.getScenarioTopNData((res) => {
      this.renderStatScenarioRank(res);
    });
  }

   getScenarioTopNData(callback = (res): void => { this.cm.override(); }){
     this.isWorkingRank = true;

     const boatIds: string[] = [];

     this.optionList.map((obj, i) => {
      if (i > 0) {
        boatIds.push(obj.value);
      }
    });

    if (typeof boatIds === 'undefined' || boatIds.length === 0 ) {
      boatIds.push('-1');
    }

    const args = [
        this.dateService.getMinDateStr(this.startDate),
        this.dateService.getMinDateStrEx(this.endDate),
        boatIds.join(','),
        this.selectedRange,
        chatbotStepSds[2],
        this.teamId
    ];


    if (this.cm.isNotEmpty(this.selectOption)) {
      args.push(parseInt(this.selectOption));
    }
    // obs
    this.api.getTopN(...args).subscribe(res => {
      // filter
      if (this.cm.isEmpty(res)) {
        // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'dlg res data');
        this.isWorkingRank = false;
        callback([]);
        return false;
      }

      callback(res);

    }, err => {
      this.commonModalService.openErrorAlert(err);
      this.isWorkingRank = false;
    });
  }


  renderStatQnaRank(res) {
    this.qnaRanks = [];

    res.map( (v, i) => {
        this.qnaRanks.push({
            name: v.domain_name,
            count: v.cnt,
            no: (i + 1)
        });
    });
  }

  renderStatScenarioRank(res) {

    this.scenarioRanks = [];

     res.map( (v, i) => {
        this.scenarioRanks.push({
            name: v.domain_name,
            count: v.cnt,
            no: (i + 1)
        });
    });
  }


  // For Chart =================================================================


  // TPS 차트 [s] ======================================================
  ddc = this.dd.chart;
  tpsChartOptions:any = this.ddc.options;
  tpsChartLabels:string[] = [];
  tpsChartType:string = 'line';
  tpsChartLegend:boolean = true;

  tpsChartData:any[] = [];

  // events
  tpsChartClicked(e:any):void {
    this.cm.isDev() && console.log('tps chart click',e);
  }

  tpsChartHovered(e:any):void {
    this.cm.isDev() && console.log('tps chart hover',e);
  }
  // TPS 차트 [e] ======================================================


  // 대화 결과 차트 [s] ======================================================
  dialogResultChartOptions:any = this.ddc.option;
  dialogResultChartLabels:string[] = [];
  dialogResultChartData:any[] = [];









}
