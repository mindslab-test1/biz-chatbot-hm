import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

import {UtilService, CommonModalService, CommonUtilService} from '../../../../../service/index';
import { DataService } from '../../../../../service/data.service';
import { Msg, lang } from '../../../../../model/message';
import { CommonModalComponent } from '../../../../../common';
import {Option} from '../../../../../model/dom-models';
import {DateService} from '../../../../../service/date.service';
import {StatFails} from '../../../../../model/stat-fails';
import {Srch_static} from '../../../../../adminsdk/models';
import {ElasticsearchService2} from '../../../../../service/elasticsearch2.service';
import {Srch_staticApi} from '../../../../../adminsdk/services/custom';





@Component({
    selector: 'app-stat-fails',
    templateUrl: './stat-fail.component.html',
    styleUrls: ['./stat-fail.component.scss']
})

export class StatFailComponent implements OnInit, OnDestroy {

  private static readonly INDEX = 'mobis_chatbot_querylog';

  statFails: StatFails[];

  totalCount: number = 0;
  totalPage: number = 0;
  pg: number = 1;
  pgSize: number = 20;
  selectText = '';
  _selectOption = '';
  searchStr = '';
  teamList: Srch_static[];
  option: Option[];




  div = {
    "term": {
      "user_div_code": this.selectOption
    }
  }

  range = {
    "range": {
      "insert_date": {
        //"gte": this.dateService.formatDay(this.startDate),
        "gte": "2019-04-01",
        "lte": this.dateService.formatDay(this.endDate),
        "time_zone": "+09:00"
      }
    }
  }


  query = {
    "query": {
      "bool": {
        "filter": {
          "bool": {
            "must": [
              this.range
            ]
          }
        }
      }
    },
    "aggs": {
      "fail_keyword": {
        "terms": {
          "field": "keyword",
          "size": 30
        }
      }
    },
    "size" : 0
  }





  constructor(private commonModalService: CommonModalService,
              private utilService: UtilService,
              private ds: DataService,
              private dateService: DateService,
              private cm: CommonUtilService,
              private es: ElasticsearchService2,
              private srchStaticApi: Srch_staticApi

  ) {
  }

  now: Date = this.dateService.getNowDate();
  private _startDate: Date = this.dateService.getDefaultStartDateBeforeOneMonth(this.now);
  private _endDate: Date = this.dateService.getDefaultEndDate(this.now);




  get selectOption(): string {
    return this._selectOption;
  }
  set selectOption(v: string) {
    this._selectOption = v;
  }
  get startDate(): Date {
    return this._startDate;
  }
  set startDate(startDate) {
    startDate = this.dateService.setDefaultStartTime(startDate);
    this._startDate = startDate;
  }

  get endDate(): Date {
    return this._endDate;
  }

  set endDate(endDate) {
    endDate = this.dateService.setDefaultStartTime(endDate);
    this._endDate = endDate;
  }

  public search(event: any = null) {
    if (this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }
    if (event === null || event.keyCode === 13) {
      this.isWorking = true;
      this.getList(1);
    }
  }


  ngOnInit() {
    this.srchStaticApi.getList().subscribe(res => {
      this.teamList = res;

      this.option = [
        {label: '전체' , value: ''}
      ];
      for (const key in this.teamList){
        const rs = {label: this.teamList[key].USER_DIV_NM, value: this.teamList[key].USER_DIV_CODE };
        if (this.teamList[key].USER_DIV_NM != null){
          this.option.push(rs);
        }
      }

    }, subErr => {
      this.isWorking = false;
      console.log(subErr);
    });
    this.getList(1);
  }


  ngOnDestroy() {
    this.ds.clearData();
  }

  private _isWorking: boolean = false;
  get isWorking() {
    return this._isWorking;
  }

  set isWorking(v: boolean) {
    if (this._isWorking == v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }

  onSearchKey($event) {
    if (this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }
    this.getList(1);

  }

  onTeamListSelect(event) {
    console.log(event);
    // console.log(event);
  }

  getList(pg) {
    this.pg = pg;
    const pgSize = this.pgSize;
    this.isWorking = true;


    if(this.selectOption != null){
      //this.range = JSON.parse(JSON.stringify(this.range) + ',' +  JSON.stringify(this.div));
    }


    this.es.getAllCount(StatFailComponent.INDEX, this.query).then(res => {
      if (res.aggregations.fail_keyword.buckets.length <= 0) {
        this.isWorking = false;
        this.totalCount = 0;
        this.totalPage = 1;
      } else {
        console.log('pg ==' + res.aggregations.fail_keyword.buckets.length);
        this.totalCount = res.aggregations.fail_keyword.buckets.length;
        this.totalPage = Math.floor(res.aggregations.fail_keyword.buckets.length / pgSize) + 1;
        const skip = (pg - 1) * pgSize;
        const limit = pgSize;
        this.isWorking = false;

        this.es.getAllaggs(StatFailComponent.INDEX, this.query, skip, limit).then(res => {
          this.statFails = res.aggregations.fail_keyword.buckets;
          this.isWorking = false;
        });

      }

    }, error => {
      this.isWorking = false;
      console.error(error);
    }).then(() => {
      //console.log(' Completed!');
    });
  }


  pageChanged(res) {
    this.getList(res);
  }


  private _finishWorking = () => {
    this.commonModalService.closeAlertDialog();
    this.isWorking = false;
  };
}
