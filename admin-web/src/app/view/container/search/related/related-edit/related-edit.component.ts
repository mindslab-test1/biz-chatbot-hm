
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { Srch_autocomplete, Srch_autocompleteInterface} from '../../../../../adminsdk/models/Srch_autocomplete';
import { CommonModalService } from '../../../../../service/common-modal.service';
import {Srch_autocompleteApi} from "../../../../../adminsdk/services/custom/Srch_autocomplete";
import {Srch_related} from '../../../../../adminsdk/models';
import {Srch_relatedApi} from '../../../../../adminsdk/services/custom';

@Component({
  selector: 'app-related-edit',
  templateUrl: './related-edit.component.html',
  styleUrls: ['./related-edit.component.scss']
})
export class RelatedEditComponent implements OnInit {


  valid:boolean = false;

  @Output() editClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  srch_related:Srch_related;

  constructor(private srchRelatedApi: Srch_relatedApi, private commonModalService: CommonModalService) {

    this.srch_related = new Srch_related();
  }

  ngOnInit() {
  }

  modalOpen(res){
    console.log(res);
    this.srch_related = res;
    this.commonModal.modalOpen();
    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  relatedEditSave(){
    if( this.srch_related == null
    ){
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.srchRelatedApi.upsert(this.srch_related).subscribe(res =>{
          
        console.log(res);
      }, error => {
        console.error(error);
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('수정되었습니다');
          this.editClose.emit(true);
          this.modalClose();
        }
      );
    }
  }

}
