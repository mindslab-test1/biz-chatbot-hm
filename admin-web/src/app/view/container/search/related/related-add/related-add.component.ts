
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { Srch_autocompleteApi } from '../../../../../adminsdk/services/custom/Srch_autocomplete';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { Srch_autocomplete, Srch_autocompleteInterface } from '../../../../../adminsdk/models/Srch_autocomplete';
import { CommonModalService } from '../../../../../service/common-modal.service';
import {Srch_related} from '../../../../../adminsdk/models';
import {Srch_relatedApi} from '../../../../../adminsdk/services/custom';

@Component({
  selector: 'app-related-add',
  templateUrl: './related-add.component.html',
  styleUrls: ['./related-add.component.scss']
})
export class RelatedAddComponent implements OnInit {


  synonymlist = new Array<string>();
  synonmWord:string;
  valid:boolean = false;

  @Output() addClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  srch_related:Srch_related;

  constructor(private srchRelatedApi: Srch_relatedApi, private commonModalService: CommonModalService) {

    this.srch_related= new Srch_related();
  }

  ngOnInit() {

  }

  modalOpen(){
    this.commonModal.modalOpen();

    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  relatedAddSave(){
    console.log(this.srch_related);
    if(this.srch_related== null  || this.srch_related.KEYWORD == ""
    ){
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.srchRelatedApi.upsert(this.srch_related).subscribe(res =>{
      }, error => {
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('등록되었습니다');
          this.addClose.emit(true);
          this.modalClose();
        }
      );
    }
  }

}
