
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { Srch_autocomplete, Srch_autocompleteInterface} from '../../../../../adminsdk/models/Srch_autocomplete';
import { CommonModalService } from '../../../../../service/common-modal.service';
import {Srch_autocompleteApi} from "../../../../../adminsdk/services/custom/Srch_autocomplete";

@Component({
  selector: 'app-autocomplete-edit',
  templateUrl: './autocomplete-edit.component.html',
  styleUrls: ['./autocomplete-edit.component.scss']
})
export class AutocompleteEditComponent implements OnInit {


  valid:boolean = false;

  @Output() editClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  srch_autocomplete:Srch_autocomplete;

  constructor(private srchAutocompleteApi: Srch_autocompleteApi, private commonModalService: CommonModalService) {

    this.srch_autocomplete = new Srch_autocomplete();
  }

  ngOnInit() {
  }

  modalOpen(res){
    console.log(res);
    this.srch_autocomplete = res;
    this.commonModal.modalOpen();
    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  autocompleteEditSave(){
    console.log("autocomplete_ID::" +this.srch_autocomplete.ID);
    if( this.srch_autocomplete == null
    ){
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.srchAutocompleteApi.upsert(this.srch_autocomplete).subscribe(res =>{
          
        console.log(res);
      }, error => {
        console.error(error);
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('수정되었습니다');
          this.editClose.emit(true);
          this.modalClose();
        }
      );
    }
  }

}
