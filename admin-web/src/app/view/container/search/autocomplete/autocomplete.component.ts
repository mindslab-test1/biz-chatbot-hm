import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { UtilService, CommonModalService, CommonUtilService } from '../../../../service/index';
import { LoopBackFilter } from '../../../../adminsdk/models/BaseModels';
import { AutocompleteEditComponent } from './autocomplete-edit/autocomplete-edit.component';
import { AutocompleteAddComponent } from './autocomplete-add/autocomplete-add.component';
import { AutocompleteRemoveComponent } from './autocomplete-remove/autocomplete-remove.component';
import { UserService } from '../../../../service/user.service';
import { DataService } from '../../../../service/data.service';
import { Msg, lang } from '../../../../model/message';
import { CommonModalComponent } from '../../../../common';
import { Srch_autocomplete } from '../../../../adminsdk/models/Srch_autocomplete';
import {Srch_autocompleteApi} from '../../../../adminsdk/services/custom';
import {Srch_spellcheck} from '../../../../adminsdk/models';


@Component({
    selector: 'app-autocomplete',
    templateUrl: './autocomplete.component.html',
    styleUrls: ['./autocomplete.component.scss'],
})

export class AutocompleteComponent implements OnInit, OnDestroy {

  totalCount = 0;
  totalPage = 0;
  pg = 1;
  pgSize = 10;
  selectText = '';
  items: Srch_autocomplete[];
  selectedItems: Srch_autocomplete[] = [];
  selectedItem: Srch_autocomplete;
  @ViewChild('addChild') addChild: AutocompleteAddComponent;
  @ViewChild('editChild') editChild: AutocompleteEditComponent;
  @ViewChild('removeChild') removeChild: AutocompleteRemoveComponent;
  private _isWorking = false;

  constructor(
      private commonModalService: CommonModalService,
      private srchAutocmpleteApi: Srch_autocompleteApi,
      private utilService: UtilService,
      private ds: DataService,
      private cm: CommonUtilService

  ) {
  }

  ngOnInit() {
    // this.workspaceId = this.userService.getWorkspaceIds().pop();
    this.getList(1);
  }
  ngOnDestroy() {
    this.ds.clearData();
  }

  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if (this._isWorking === v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }
  onSearchKey($event) {
    if (this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }

    this.getList(1);
  }

  getList(pg) {
    this.pg = pg;
    const pgSize = this.pgSize;

    this.isWorking = true;
    this.srchAutocmpleteApi.countList( this.selectText).subscribe(res => {
      if (res[0].cnt <= 0) {
        this.isWorking = false;
        this.totalCount = 0;
        this.totalPage = 1;
        // this.commonModalService.openErrorAlert('No data');
        // console.log('no data');
      } else {
        this.totalCount = res[0].cnt;
        this.totalPage = Math.ceil(res[0].cnt / pgSize);
        const skip = (pg - 1) * pgSize;
        const limit = pgSize;
        const order = `ID asc`;
        console.log('this.totalCount' , this.totalCount);
        this.srchAutocmpleteApi.getList( skip, limit, order, this.selectText).subscribe(res => {
          console.log('res :', res);
          this.items = res;
          this.isWorking = false;
        }, subErr => {
          this.isWorking = false;
          console.log(subErr);
        });
      }
    }, err => {
      this.isWorking = false;
      console.error(err);
    }, () => {
    });
  }
  pageChanged(res) {
    console.log(res);
    this.getList(res);
  }
  autocompleteEditOpen(res) {
    this.editChild.modalOpen(res);
  }

  removesOpen(evnet) {
    const items  = this.selectedItems;
    console.log('debug set remove', event, items);
    if (this.cm.isEmpty(items)) {
      console.log('debug remove. stop. no select items');
      return;
    }
    this.removeChild.modalOpen(items);
  }
  autocompleteRemoveOpen(res) {
    const items = [];
    items.push(res);
    this.removeChild.modalOpen(items);
  }
  autocompleteAddOpen() {
    this.addChild.modalOpen();
  }
  modalClose(res) {
    if (res) {
      this.getList(1);
    }
  }

  private _finishWorking = () => {
    this.commonModalService.closeAlertDialog();
    this.isWorking = false;
  };

  public uploadFile(e: any) {
    if (!e.srcElement.value) {
      return false;
    }

    if (confirm('Are you sure you want to upload the file?')) {

      this.commonModalService.openCommonDialogOnlyMsg('엑셀 업로드 중...', '엑셀 업로드 중...');
      this.isWorking = true;
      this.utilService.importExcel(e).subscribe(items => {
        console.log(items);
        const array = new Array<Srch_autocomplete>();
        let index = 0;
        let result = 0;
        for (const item of items) {
          console.log(typeof item[0]);
          if (index > 0 ) {

            if (item[1] !== undefined) {
              const srch_autocomplete = new Srch_autocomplete();
              if (item[0] > 0) {
                srch_autocomplete.ID = item[0];
              } else {
                srch_autocomplete.ID = 0;
              }
              srch_autocomplete.AUTO_KEYWORD = item[1];
              srch_autocomplete.CATEGORY = item[2];
              srch_autocomplete.TYPE = item[3];
              srch_autocomplete.SCORE = item[4];

              // 중복제거 필요
              this.srchAutocmpleteApi.upsert(srch_autocomplete).subscribe(response => {
                    console.log(response);
                    console.log('update => ID: ' + item[0] + 'KEYWORD: ' + item[1] + ', CATEGORY: ' +  item[2] + ', TYPE = ' + item[3] + ', SCORE = ' + item[4]);
                    result++;
                  }
              );
            }
          }
          index++;
        }
        this._finishWorking();
        if (result === index) {
          alert('Your request complete successfully.');
        }
        this.getList(1);
      }, error => {
        this._finishWorking();
        console.error(error);
      });
    }
  }

  public downloadFile() {
    const sheetData: any[] = [];
    let items;

    const filter: LoopBackFilter = {
      order: `ID asc`
    };

    this.commonModalService.openCommonDialogOnlyMsg('엑셀 다운로드 중...', '엑셀 다운로드 중...');
    this.isWorking = true;
    this.srchAutocmpleteApi.find(filter).subscribe(res => {
      console.log('res :', res);
      items = res;
    }, error => {
      this._finishWorking();
      console.error(error);
      // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
    },
      () => {
        if (items.length > 0) {
          sheetData.push(['ID', 'AUTO_KEYWORD', 'CATEGORY', 'TYPE', 'SCORE']);
          for (const item of items) {
            sheetData.push([item.ID, item.AUTO_KEYWORD, item.CATEGORY, item.TYPE, item.SCORE]);
          }
          this.utilService.exportExcel('AutoKeywordList', sheetData);
        }
        this._finishWorking();
      }
    );
  }

}
