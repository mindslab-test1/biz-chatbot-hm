
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { Srch_autocompleteApi } from '../../../../../adminsdk/services/custom/Srch_autocomplete';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { Srch_autocomplete, Srch_autocompleteInterface } from '../../../../../adminsdk/models/Srch_autocomplete';
import { CommonModalService } from '../../../../../service/common-modal.service';

@Component({
  selector: 'app-autocomplete-add',
  templateUrl: './autocomplete-add.component.html',
  styleUrls: ['./autocomplete-add.component.scss']
})
export class AutocompleteAddComponent implements OnInit {


  synonymlist = new Array<string>();
  synonmWord:string;
  valid:boolean = false;

  @Output() addClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  srch_autocomplete:Srch_autocomplete;

  constructor(private srchAutocompleteApi: Srch_autocompleteApi, private commonModalService: CommonModalService) {

    this.srch_autocomplete= new Srch_autocomplete();
  }

  ngOnInit() {

  }

  modalOpen(){
    this.commonModal.modalOpen();

    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  autocompleteAddSave(){
    //his.srch_stopword.stop_no = this.synonymlist.join(',')
    console.log(this.srch_autocomplete);
    if(this.srch_autocomplete== null  || this.srch_autocomplete.AUTO_KEYWORD == ""
    ){
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.srchAutocompleteApi.upsert(this.srch_autocomplete).subscribe(res =>{
      }, error => {
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('등록되었습니다');
          this.addClose.emit(true);
          this.modalClose();
        }
      );
    }
  }

}
