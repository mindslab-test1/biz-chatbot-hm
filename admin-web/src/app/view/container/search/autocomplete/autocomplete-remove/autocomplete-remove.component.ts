import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { Srch_autocompleteApi} from '../../../../../adminsdk/services/custom/Srch_autocomplete';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { Srch_autocomplete, Srch_autocompleteInterface} from '../../../../../adminsdk/models/Srch_autocomplete';
import { CommonModalService } from '../../../../../service/common-modal.service';
import {Srch_spellcheck} from "../../../../../adminsdk/models";
import {CommonUtilService} from "../../../../../service";

@Component({
  selector: 'app-autocomplete-remove',
  templateUrl: './autocomplete-remove.component.html',
  styleUrls: ['./autocomplete-remove.component.scss']
})
export class AutocompleteRemoveComponent implements OnInit {

  valid:boolean = false;

  @Output() removeClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;

  selectedItem:Srch_autocomplete;
  selectedItems: Srch_autocomplete[] = [];
  items: Srch_autocomplete[];

  constructor(
      private srchAutocompletApi: Srch_autocompleteApi,
      private commonModalService: CommonModalService,
      private cm: CommonUtilService
  ) {

    this.selectedItem = new Srch_autocomplete();
  }

  ngOnInit() {

  }

  modalOpen(res){
    this.selectedItems = res;
    this.commonModal.modalOpen();
  }

  modalClose(){

    this.commonModal.modalClose();
  }

    remove() {
        if (this.selectedItems.length == 1) {
            this.removesItem();
        } else {
            this.removesItem();
        }
    }

    private removesItem() {
            const ids = this.selectedItems.reduce((res,ele)=>{
                res.push(ele.ID);
                return res;
            },[]);
            if (this.cm.isEmpty(ids)){
                this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
                return false;
            }
            this.srchAutocompletApi.deleteByIds(ids).subscribe(
                res => {
                    this.commonModalService.openSuccessAlert('삭제되었습니다');
                    this.removeClose.emit(true);
                    this.selectedItems = [];
                    this.modalClose();
                }, error => {
                    console.log('delete Error');
                });
        }
}
