import {Component, OnInit, OnDestroy} from '@angular/core';
import {ElasticsearchService} from '../../../../../service/elasticsearch.service';
import {UtilService, CommonModalService, CommonUtilService} from '../../../../../service/index';
import {LoopBackFilter} from '../../../../../adminsdk/models/BaseModels';
import {UserService} from '../../../../../service/user.service';
import {DataService} from '../../../../../service/data.service';
import {Msg, lang} from '../../../../../model/message';
import {CommonModalComponent} from '../../../../../common';
// import {Srch_etlLogs} from '../../../../../adminsdk/models/Srch_etlLogs';
// import {Srch_etlLogsApi} from '../../../../../adminsdk/services/custom';
import {DateService} from '../../../../../service/date.service';
import {Paginator} from '../../../../../common/paginator/paginator.component';
import {EtlLogs, EtlLogsSource} from '../../../../../model/etl_logs';


@Component({
    selector: 'app-etlLogs',
    templateUrl: './etl-logs.component.html',
    styleUrls: ['./etl-logs.component.scss'],
})

export class EtlLogsComponent implements OnInit, OnDestroy {

    private static readonly INDEX = '.etl_batch_log';

    etlLogsSource: EtlLogsSource[];
    etlLogs: EtlLogs[];

    totalCount = 0;
    errorTotalCount = 0;
    errorTotalPage = 0;
    totalPage = 0;
    errorPg = 1;
    pg = 1;
    pgSize = 10;
    errorPgSize = 10;
    selectText = '';
    // items: Srch_etlLogs[];
    // selectedItems: Srch_etlLogs[] = [];
    // selectedItem: Srch_etlLogs;


    constructor(private commonModalService: CommonModalService,
                // private srchEtlLogsApi: Srch_etlLogsApi,
                private utilService: UtilService,
                private ds: DataService,
                private dateService: DateService,
                private cm: CommonUtilService,
                private es: ElasticsearchService
    ) {
    }

    now: Date = this.dateService.getNowDate();
    private _errorStartDate: Date = this.dateService.getDefaultStartDateBeforeOneWeek(this.now);
    private _errorEndDate: Date = this.dateService.getDefaultEndDate(this.now);
    private _startDate: Date = this.dateService.getDefaultStartDateBeforeOneWeek(this.now);
    private _endDate: Date = this.dateService.getDefaultEndDate(this.now);

    // 에러로그
    get errorStartDate(): Date {
        return this._errorStartDate;
    }

    set errorStartDate(startDate) {
        startDate = this.dateService.setDefaultStartTime(startDate);
        this._errorStartDate = startDate;
    }

    get errorEndDate(): Date {
        return this._errorEndDate;
    }

    set errorEndDate(endDate) {
        endDate = this.dateService.setDefaultEndTime(endDate);
        this._errorEndDate = endDate;
    }

    get startDate(): Date {
        return this._startDate;
    }

    set startDate(startDate) {
        startDate = this.dateService.setDefaultStartTime(startDate);
        this._startDate = startDate;
    }

    get endDate(): Date {
        return this._endDate;
    }

    set endDate(endDate) {
        endDate = this.dateService.setDefaultStartTime(endDate);
        this._endDate = endDate;
    }


    ngOnInit() {
        this.getErrorList(1);
        this.getList(1);
    }


    ngOnDestroy() {
        this.ds.clearData();
    }

    // tslint:disable-next-line:member-ordering
    private _isWorking = false;
    get isWorking() {
        return this._isWorking;
    }

    set isWorking(v: boolean) {
        if (this._isWorking === v) {
            return;
        }
        this._isWorking = v;
        this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
    }

    onSearchKey($event) {
        if (this.isWorking) {
            this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
            return;
        }
        this.getList(1);

    }

    onSearchErrorKey($event) {
        if (this.isWorking) {
            this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
            return;
        }
        this.getErrorList(1);
    }

    getList(pg) {
        const queryalldocs = {
            'query': {
                'bool': {
                    'must': [
                        {
                            'match_all': {}
                        },
                        {
                            'range': {
                                'start_time_without_time': {
                                    'gte': this.dateService.formatDay(this.startDate)
                                }
                            }
                        },
                        {
                            'range': {
                                'end_time_without_time': {
                                    'lte': this.dateService.formatDay(this.endDate)
                                }
                            }
                        }
                    ]
                }
            },
            'sort': {
                'start_time': 'desc'
            }
        };
        this.pg = pg;
        const pgSize = this.pgSize;

        this.isWorking = true;

        this.es.getAllCount(EtlLogsComponent.INDEX, queryalldocs).then(res => {
            console.log(res.hits.total);
            if (res.hits.total <= 0) {
                this.isWorking = false;
                this.totalCount = 0;
                this.totalPage = 1;
            } else {

                this.totalCount = res.hits.total;
                this.totalPage = Math.ceil(res.hits.total / pgSize) ;

                const skip = (pg - 1) * pgSize;
                const limit = pgSize;
                this.es.getAllDocuments(EtlLogsComponent.INDEX, queryalldocs, skip, limit).then(res => {
                    this.etlLogsSource = res.hits.hits;

                    this.etlLogs = [];
                    for (const c  of this.etlLogsSource) {
                        // console.log(c._source);
                        this.etlLogs.push(c._source);
                    }
                    this.isWorking = false;
                });
            }

            // console.log('res cnt == ' + response.hits.hits.length);
        }, error => {
            this.isWorking = false;
            console.error(error);
        }).then(() => {
            // console.log(' Completed!');
        });
    }

    // 에러로그
    getErrorList(pg) {
        this.errorPg = pg;
        const pgSize = this.errorPgSize;

        // this.isWorking = true;
        // this.srchEtlLogsApi.countList(this.selectText, this.dateService.getMinDateStr(this.errorStartDate), this.dateService.getMinDateStr(this.errorEndDate)).subscribe(res => {
        //     if (res.cnt <= 0) {
        //         this.isWorking = false;
        //         this.errorTotalCount = 0;
        //         this.errorTotalPage = 1;
        //         // this.commonModalService.openErrorAlert('No data');
        //         // console.log('no data');
        //     } else {
        //         this.errorTotalCount = res.cnt;
        //         this.errorTotalPage = Math.ceil(res.cnt / pgSize);
        //         const skip = (pg - 1) * pgSize;
        //         const limit = pgSize;
        //         const order = `R_OBJECT_ID desc`;
        //         this.srchEtlLogsApi.getList(skip, limit, order, this.selectText, this.dateService.getMinDateStr(this.errorStartDate), this.dateService.getMinDateStr(this.errorEndDate)).subscribe(res => {
        //             this.items = res;
        //             this.isWorking = false;
        //             console.log('this.items == ' + this.items);
        //         }, subErr => {
        //             this.isWorking = false;
        //             console.log(subErr);
        //         });
        //     }
        // }, err => {
        //     this.isWorking = false;
        //     console.error(err);
        // }, () => {
        // });
    }

    errorPageChanged(res) {
        if (res > 0 && res <= this.totalPage) {
            this.getErrorList(res);
        }
    }

    pageChanged(res) {
        if (res > 0 && res <= this.totalPage) {
            this.getList(res);
        }
    }

    modalClose(res) {
        if (res) {
            this.getErrorList(1);
        }
    }

    private _finishWorking = () => {
        this.commonModalService.closeAlertDialog();
        this.isWorking = false;
    }
}
