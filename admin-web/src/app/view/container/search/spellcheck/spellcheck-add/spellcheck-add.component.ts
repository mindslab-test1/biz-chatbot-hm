
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { Srch_spellcheckApi } from '../../../../../adminsdk/services/custom/Srch_spellcheck';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { Srch_spellcheck, Srch_spellcheckInterface } from '../../../../../adminsdk/models/Srch_spellcheck';
import { CommonModalService } from '../../../../../service/common-modal.service';

@Component({
  selector: 'app-spellcheck-add',
  templateUrl: './spellcheck-add.component.html',
  styleUrls: ['./spellcheck-add.component.scss']
})
export class SpellcheckAddComponent implements OnInit {



  valid:boolean = false;

  @Output() addClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  srch_spellcheck:Srch_spellcheck;

  constructor(private srchSpellcheckApi: Srch_spellcheckApi, private commonModalService: CommonModalService) {

    this.srch_spellcheck= new Srch_spellcheck();
  }

  ngOnInit() {

  }

  modalOpen(){
    this.commonModal.modalOpen();

    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  spellcheckAddSave(){
    //his.srch_stopword.stop_no = this.synonymlist.join(',')
    console.log(this.srch_spellcheck);
    if(this.srch_spellcheck == null || this.srch_spellcheck.TYPE == null || this.srch_spellcheck.SPELL_KEYWORD == null || this.srch_spellcheck.TYPE == "" || this.srch_spellcheck.SPELL_KEYWORD == ""
    ){
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.srchSpellcheckApi.upsert(this.srch_spellcheck).subscribe(res =>{
      }, error => {
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('등록되었습니다');
          this.addClose.emit(true);
          this.modalClose();
        }
      );
    }
  }

}
