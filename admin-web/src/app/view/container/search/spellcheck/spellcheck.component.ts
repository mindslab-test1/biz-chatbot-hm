import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { UtilService, CommonModalService, CommonUtilService } from '../../../../service/index';
import { LoopBackFilter } from '../../../../adminsdk/models/BaseModels';
import { SpellcheckEditComponent } from './spellcheck-edit/spellcheck-edit.component';
import { SpellcheckAddComponent } from './spellcheck-add/spellcheck-add.component';
import { SpellcheckRemoveComponent } from './spellcheck-remove/spellcheck-remove.component';
import { UserService } from '../../../../service/user.service';
import { DataService } from '../../../../service/data.service';
import { Msg, lang } from '../../../../model/message';
import { CommonModalComponent } from '../../../../common';
import { Srch_spellcheck } from '../../../../adminsdk/models/Srch_spellcheck';
import {Srch_spellcheckApi} from '../../../../adminsdk/services/custom';


@Component({
    selector: 'app-spellcheck',
    templateUrl: './spellcheck.component.html',
    styleUrls: ['./spellcheck.component.scss'],
})

export class SpellcheckComponent implements OnInit, OnDestroy {

  totalCount = 0;
  totalPage = 0;
  pg = 1;
  pgSize = 10;
  selectText = '';
  items: Srch_spellcheck[];
  selectedItems: Srch_spellcheck[] = [];
  selectedItem: Srch_spellcheck;
  @ViewChild('addChild') addChild: SpellcheckAddComponent;
  @ViewChild('editChild') editChild: SpellcheckEditComponent;
  @ViewChild('removeChild') removeChild: SpellcheckRemoveComponent;
  private _isWorking = false;

  constructor(private commonModalService: CommonModalService,
              private srchSpellcheckApi: Srch_spellcheckApi,
              private utilService: UtilService,
              private ds: DataService,
              private cm: CommonUtilService, ) {
  }

  ngOnInit() {
    // this.workspaceId = this.userService.getWorkspaceIds().pop();
    this.getList(1);
  }
  ngOnDestroy() {
    this.ds.clearData();
  }

  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if (this._isWorking === v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }
  onSearchKey($event) {
    if (this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }

    this.getList(1);
  }

    removesOpen(evnet) {
      const items  = this.selectedItems;
      console.log('debug set remove', event, items);
        if (this.cm.isEmpty(items)) {
            console.log('debug remove. stop. no select items');
            return;
        }
        this.removeChild.modalOpen(items);
    }


  getList(pg) {
    this.pg = pg;
    const pgSize = this.pgSize;

    this.isWorking = true;
    this.srchSpellcheckApi.countList( this.selectText).subscribe(res => {
      if (res[0].cnt <= 0) {
        this.isWorking = false;
        this.totalCount = 0;
        this.totalPage = 1;
        // this.commonModalService.openErrorAlert('No data');
        // console.log('no data');
      } else {
        this.totalCount = res[0].cnt;
        this.totalPage = Math.ceil(res[0].cnt / pgSize);
        const skip = (pg - 1) * pgSize;
        const limit = pgSize;
        const order = `ID asc`;
        console.log('this.totalCount' , this.totalCount);
        this.srchSpellcheckApi.getList( skip, limit, order, this.selectText).subscribe(res => {
          console.log('res :', res);
          this.items = res;
          this.isWorking = false;
        }, subErr => {
          this.isWorking = false;
          console.log(subErr);
        });
      }
    }, err => {
      this.isWorking = false;
      console.error(err);
    }, () => {
    });
  }
  pageChanged(res) {
    console.log(res);
    this.getList(res);
  }
  spellcheckEditOpen(res) {
    this.editChild.modalOpen(res);
  }
  spellcheckRemoveOpen(res) {
    const items = [];
    items.push(res);
    this.removeChild.modalOpen(items);
  }
  spellcheckAddOpen() {
    this.addChild.modalOpen();
  }
  modalClose(res) {
    if (res) {
      this.getList(1);
    }
  }

  private _finishWorking = () => {
    this.commonModalService.closeAlertDialog();
    this.isWorking = false;
  };

  public uploadFile(e: any) {
    if (!e.srcElement.value) {
      return false;
    }

    if (confirm('Are you sure you want to upload the file?')) {

      this.commonModalService.openCommonDialogOnlyMsg('엑셀 업로드 중...', '엑셀 업로드 중...');
      this.isWorking = true;
      this.utilService.importExcel(e).subscribe(items => {
        console.log(items);
        const array = new Array<Srch_spellcheck>();
        let index = 0;
        let result = 0;
        for (const item of items) {
          console.log(typeof item[0]);
          if (index > 0 ) {

            if (item[1] !== undefined) {
              const srch_spellcheck = new Srch_spellcheck();
              if (item[0] > 0) {
                srch_spellcheck.ID = item[0];
              } else {
                srch_spellcheck.ID = 0;
              }
              srch_spellcheck.SPELL_KEYWORD = item[1];
              srch_spellcheck.TYPE = item[2];

              // 중복제거 필요
              this.srchSpellcheckApi.upsert(srch_spellcheck).subscribe(response => {
                    console.log(response);
                    console.log('update => ID: ' + item[0] + 'SPELL_KEYWORD: ' + item[1] + ', TYPE: ' +  item[2] );
                    result++;
                  }
              );
            }
          }
          index++;
        }
        this._finishWorking();
        if (result === index) {
          alert('Your request complete successfully.');
        }
        this.getList(1);
      }, error => {
        this._finishWorking();
        console.error(error);
      });
    }
  }

  public downloadFile() {
    const sheetData: any[] = [];
    let items;

    const filter: LoopBackFilter = {
      order: `ID asc`
    };

    this.commonModalService.openCommonDialogOnlyMsg('엑셀 다운로드 중...', '엑셀 다운로드 중...');
    this.isWorking = true;
    this.srchSpellcheckApi.find(filter).subscribe(res => {
      console.log('res :', res);
      items = res;
    }, error => {
      this._finishWorking();
      console.error(error);
      // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
    },
      () => {
        if (items.length > 0) {
          sheetData.push(['ID', 'SPELL_KEYWORD', 'TYPE']);
          for (const item of items) {
            sheetData.push([item.ID, item.SPELL_KEYWORD, item.TYPE]);
          }
          this.utilService.exportExcel('SpellKeywordList', sheetData);
        }
        this._finishWorking();
      }
    );
  }

}
