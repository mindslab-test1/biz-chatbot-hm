
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { Srch_spellcheckInterface, Srch_spellcheck} from '../../../../../adminsdk/models/Srch_spellcheck';
import { CommonModalService } from '../../../../../service/common-modal.service';
import {Srch_spellcheckApi} from "../../../../../adminsdk/services/custom/Srch_spellcheck";

@Component({
  selector: 'app-spellcheck-edit',
  templateUrl: './spellcheck-edit.component.html',
  styleUrls: ['./spellcheck-edit.component.scss']
})
export class SpellcheckEditComponent implements OnInit {


  valid:boolean = false;

  @Output() editClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  srch_spellcheck:Srch_spellcheck;

  constructor(private srchSpellcheckApi: Srch_spellcheckApi, private commonModalService: CommonModalService) {

    this.srch_spellcheck = new Srch_spellcheck();
  }

  ngOnInit() {
  }

  modalOpen(res){
    console.log(res);
    this.srch_spellcheck = res;
    this.commonModal.modalOpen();
    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  spellcheckEditSave(){
    console.log("srch_spellcheck ID::" +this.srch_spellcheck.ID);
    if( this.srch_spellcheck == null || this.srch_spellcheck.TYPE == null || this.srch_spellcheck.SPELL_KEYWORD == null || this.srch_spellcheck.TYPE == "" || this.srch_spellcheck.SPELL_KEYWORD == ""
    ){
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.srchSpellcheckApi.upsert(this.srch_spellcheck).subscribe(res =>{
          
        console.log(res);
      }, error => {
        console.error(error);
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('수정되었습니다');
          this.editClose.emit(true);
          this.modalClose();
        }
      );
    }
  }

}
