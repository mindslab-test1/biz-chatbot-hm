import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { Srch_spellcheckApi} from '../../../../../adminsdk/services/custom/Srch_spellcheck';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { Srch_spellcheck , Srch_spellcheckInterface} from '../../../../../adminsdk/models/Srch_spellcheck';
import { CommonModalService } from '../../../../../service/common-modal.service';
import {typeOf} from "uri-js/dist/esnext/util";
import {CommonUtilService} from "../../../../../service";

@Component({
  selector: 'app-spellcheck-remove',
  templateUrl: './spellcheck-remove.component.html',
  styleUrls: ['./spellcheck-remove.component.scss']
})
export class SpellcheckRemoveComponent implements OnInit {

  valid:boolean = false;

  @Output() removeClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  selectedItem:Srch_spellcheck;
  selectedItems: Srch_spellcheck[] = [];
  items: Srch_spellcheck[];

  constructor(
      private srchSpellcheckApi: Srch_spellcheckApi,
      private commonModalService: CommonModalService,
      private cm: CommonUtilService,
  ) {

    this.selectedItem = new Srch_spellcheck();
  }

  ngOnInit() {

  }

  modalOpen(res){
      console.log("modalOpen" + res);
    this.selectedItems = res;
    console.log(this.selectedItems[0].ID);
    this.commonModal.modalOpen();
  }

  modalClose(){
    this.commonModal.modalClose();
  }

    remove() {
        if(this.selectedItems.length == 1){
            this.removesItem();
        } else {
            this.removesItem();
        }
    }

    private removesItem() {
        const ids = this.selectedItems.reduce((res,ele)=>{
            console.log("res == " + res);
            console.log("ele == " + ele.ID);
            res.push(ele.ID);
            return res;
        },[]);
        if (this.cm.isEmpty(ids)){
            this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
            return false;
        }
        this.srchSpellcheckApi.deleteByIds(ids).subscribe(
            res => {
                this.commonModalService.openSuccessAlert('삭제되었습니다');
                this.removeClose.emit(true);
                this.selectedItems = [];
                this.modalClose();
            }, error => {
                console.log('delete Error');
            });
    }

/*  spellcheckRemoveSave(event){
     this.srchSpellcheckApi.deleteByIds(this.srch_spellcheck.ID).subscribe(res =>{
      }, error => {
        console.error(error);
      },
        () => {
          this.commonModalService.openSuccessAlert('삭제되었습니다');
          this.removeClose.emit(true);
          this.modalClose();
        }
      );

  }*/
}
