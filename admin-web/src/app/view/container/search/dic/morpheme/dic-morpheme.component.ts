import {Component, OnInit, ViewChild, OnDestroy} from '@angular/core';

import {UtilService, CommonModalService, CommonUtilService, QaExcelVo} from '../../../../../service/index';
import {Srch_morphemeApi} from '../../../../../adminsdk/services/custom/Srch_morpheme';
import {LoopBackFilter} from '../../../../../adminsdk/models/BaseModels';
import {MorphemeEditComponent} from './morpheme-edit/morpheme-edit.component';
import {MorphemeAddComponent} from './morpheme-add/morpheme-add.component';
import {MorphemeRemoveComponent} from './morpheme-remove/morpheme-remove.component';
import {Srch_morpheme} from '../../../../../adminsdk/models/Srch_morpheme';
import {UserService} from '../../../../../service/user.service';
import {DataService} from '../../../../../service/data.service';
import {Msg, lang} from '../../../../../model/message';
import {CommonModalComponent} from '../../../../../common';
import {Option} from '../../../../../model/dom-models';
import {Client} from 'elasticsearch';


@Component({
    selector: 'app-dic-morpheme',
    templateUrl: './dic-morpheme.component.html',
    styleUrls: ['./dic-morpheme.component.scss']
})

export class DicMorphemeComponent implements OnInit, OnDestroy {

    totalCount = 0;
    totalPage = 0;
    pg = 1;
    pgSize = 10;
    selectText = '';
    items: Srch_morpheme[];
    selectedItems: Srch_morpheme[] = [];
    selectedItem: Srch_morpheme;

    @ViewChild('addChild') addChild: MorphemeAddComponent;
    @ViewChild('editChild') editChild: MorphemeEditComponent;
    @ViewChild('removeChild') removeChild: MorphemeRemoveComponent;
    private _isWorking = false;
    private client: Client;

    constructor(private commonModalService: CommonModalService, private srchMorphemeApi: Srch_morphemeApi, private utilService: UtilService, private userService: UserService, private ds: DataService, private cm: CommonUtilService) {
    }

    ngOnInit() {
        // this.workspaceId = this.userService.getWorkspaceIds().pop();
        this.getList(1);
    }

    ngOnDestroy() {
        this.ds.clearData();
    }

    get isWorking() {
        return this._isWorking;
    }

    set isWorking(v: boolean) {
        if (this._isWorking === v) {
            return;
        }
        this._isWorking = v;
        this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
    }

    onSearchKey($event) {
        if (this.isWorking) {
            this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
            return;
        }

        this.getList(1);
    }

    getList(pg) {
        this.pg = pg;
        const pgSize = this.pgSize;

        this.isWorking = true;
        this.srchMorphemeApi.countList(this.selectText).subscribe(res => {
            if (res[0].cnt <= 0) {
                this.items = [];
                this.isWorking = false;
                this.totalCount = 0;
                this.totalPage = 1;
                // this.commonModalService.openErrorAlert('No data');
                // console.log('no data');
            } else {
                this.totalCount = res[0].cnt;
                this.totalPage = Math.ceil(res[0].cnt / pgSize);
                const skip = (pg - 1) * pgSize;
                const limit = pgSize;
                const order = `morph_no desc`;
                console.log('this.totalCount', this.totalCount);
                this.srchMorphemeApi.getList(skip, limit, order, this.selectText).subscribe(res => {
                    // console.log("res :", res)
                    this.items = res;
                    this.isWorking = false;
                }, subErr => {
                    this.isWorking = false;
                    console.log(subErr);
                });
            }
        }, err => {
            this.isWorking = false;
            console.error(err);
        }, () => {
        });
    }

    pageChanged(res) {
        if (res > 0 && res <= this.totalPage) {
            this.getList(res);
        }
    }

    morphemeEditOpen(res) {
        this.editChild.modalOpen(res);
    }

    morphemeRemoveOpen(res) {
        const items = [];
        items.push(res);
        this.removeChild.modalOpen(items);
    }

    removesOpen(evnet) {
        const items = this.selectedItems;
        console.log('debug set remove', event, items);
        if (this.cm.isEmpty(items)) {
            console.log('debug remove. stop. no select items');
            return;
        }
        this.removeChild.modalOpen(items);
    }

    morphemeAddOpen() {
        this.addChild.modalOpen();
    }

    modalClose(res) {
        if (res) {
            this.getList(1);
        }
    }

    private _finishWorking = () => {
        this.commonModalService.closeAlertDialog();
        this.isWorking = false;
    };

    public uploadFile(e: any) {
        if (confirm('Are you sure you want to upload the file?')) {

            this.commonModalService.openCommonDialogOnlyMsg('엑셀 업로드 중...', '엑셀 업로드 중...');
            this.isWorking = true;
            this.utilService.importExcel(e).subscribe(items => {
                console.log('1 =====  ' + items);
                const array = new Array<Srch_morpheme>();
                let index = 0;
                let result = 0;
                for (const item of items) {
                    if (index > 0) {

                        if (item[1] !== undefined) {
                            const srch_morpheme = new Srch_morpheme();
                            if (item[0] > 0) {
                                srch_morpheme.MORPH_NO = item[0];
                            } else {
                                srch_morpheme.KEYWORD = '';
                            }
                            srch_morpheme.KEYWORD = item[1];
                            srch_morpheme.MORPH_TAG = item[2];
                            srch_morpheme.MEAN_CLASSIFICATION = item[3];
                            srch_morpheme.LAST_MORPH = item[4];
                            srch_morpheme.STATUS = item[5];
                            srch_morpheme.TYPE = item[6];
                            srch_morpheme.MORPH_INDEX_INFO = item[7];

                            // 중복제거 필요
                            this.srchMorphemeApi.upsert(srch_morpheme).subscribe(response => {
                                    console.log(response);
                                    result++;
                                }
                            );
                        }
                    }
                    index++;
                }
                this._finishWorking();
                if (result === index) {
                    alert('Your request complete successfully.');
                }
                this.getList(1);
            }, error => {
                this._finishWorking();
                console.error(error);
            });
        }
    }

    public applyServer() {
        if (confirm('서버 적용 하시겠습니까?')) {
            this.commonModalService.openCommonDialogOnlyMsg('서버적용 중...', '서버 적용중...');
            this.isWorking = true;

            this.srchMorphemeApi.apply().subscribe(
                res => {
                    if (res) {
                        alert('서버 적용 되었습니다.');
                    } else {
                        alert('서버 적용에 실패했습니다. 잠시 후 다시 시도해주세요.');
                    }
                }, error => {
                    alert('서버 적용에 실패했습니다. 잠시 후 다시 시도해주세요.');
                }
            );

            this._finishWorking();


        }
    }


    public downloadFile() {
        const sheetData: any[] = [];
        let items;

        const filter: LoopBackFilter = {
            order: `morph_no asc`
        };

        this.commonModalService.openCommonDialogOnlyMsg('엑셀 다운로드 중...', '엑셀 다운로드 중...');
        this.isWorking = true;
        this.srchMorphemeApi.find(filter).subscribe(res => {
                console.log('res == ', res);
                items = res;
            }, error => {
                this._finishWorking();
                console.error(error);
                // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
            },
            () => {
                if (items.length > 0) {
                    sheetData.push(['MORPH_NO', 'KEYWORD', 'MORPH_TAG', 'MEAN_CLASSIFICATION', 'LAST_MORPH', 'STATUS', 'TYPE', 'MORPH_INDEX_INFO']);
                    for (const item of items) {
                        sheetData.push([item.MORPH_NO, item.KEYWORD, item.MORPH_TAG, item.MEAN_CLASSIFICATION, item.LAST_MORPH, item.STATUS, item.TYPE, item.MORPH_INDEX_INFO]);
                    }
                    this.utilService.exportExcel('MorphemeList', sheetData);
                }
                this._finishWorking();
            }
        );
    }

}

