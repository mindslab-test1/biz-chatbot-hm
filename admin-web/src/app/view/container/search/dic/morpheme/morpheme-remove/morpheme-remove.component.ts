import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { CommonModalComponent } from '../../../../../../common';
import { Srch_morphemeApi } from '../../../../../../adminsdk/services/custom/Srch_morpheme';
import { LoopBackFilter } from '../../../../../../adminsdk/models/BaseModels';
import { Srch_morphemeInterface, Srch_morpheme } from '../../../../../../adminsdk/models/Srch_morpheme';
import { CommonModalService } from '../../../../../../service/common-modal.service';
import {CommonUtilService} from "../../../../../../service";

@Component({
  selector: 'app-morpheme-remove',
  templateUrl: './morpheme-remove.component.html',
  styleUrls: ['./morpheme-remove.component.scss']
})
export class MorphemeRemoveComponent implements OnInit {

  valid:boolean = false;

  @Output() removeClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
    selectedItem:Srch_morpheme;
    selectedItems: Srch_morpheme[] = [];
    items: Srch_morpheme[];

  constructor(
      private srchMorphemeApi: Srch_morphemeApi,
    private commonModalService: CommonModalService,
      private cm: CommonUtilService

  ) {

    this.selectedItem = new Srch_morpheme();
  }

  ngOnInit() {

  }

  modalOpen(res){
    this.selectedItems = res;
    this.commonModal.modalOpen();
  }

  modalClose(){

    this.commonModal.modalClose();
  }
    remove() {
        if(this.selectedItems.length == 1){
            this.removesItem();
        } else {
            this.removesItem();
        }
    }

    private removesItem() {
        const items = this.selectedItems;
        const ids = this.selectedItems.reduce((res,ele)=>{
            res.push(ele.MORPH_NO);
            return res;
        },[]);
        if (this.cm.isEmpty(ids)){
            this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
            return false;
        }
        this.srchMorphemeApi.deleteByIds(ids).subscribe(
            res => {
                this.commonModalService.openSuccessAlert('삭제되었습니다');
                this.removeClose.emit(true);
                this.selectedItems = [];
                this.modalClose();
            }, error => {
                console.log('delete Error');
            });
    }
}
