
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../../common';
import { Srch_morphemeApi } from '../../../../../../adminsdk/services/custom/Srch_morpheme';
import { LoopBackFilter } from '../../../../../../adminsdk/models/BaseModels';
import { Srch_morpheme, Srch_morphemeInterface } from '../../../../../../adminsdk/models/Srch_morpheme';
import { CommonModalService } from '../../../../../../service/common-modal.service';

@Component({
  selector: 'app-morpheme-edit',
  templateUrl: './morpheme-edit.component.html',
  styleUrls: ['./morpheme-edit.component.scss']
})
export class MorphemeEditComponent implements OnInit {

  valid:boolean = false;

  @Output() editClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  srch_morpheme:Srch_morpheme;
  last_morphList = [{label: 'T', value: 'T'}, {label: 'F', value: 'F'}];
  statusList = [{label: 'Y', value: 'Y'}, {label: 'N', value: 'N'}];
  typeList = [{label: 'NONE', value: 'NONE'}, {label: '활용', value: '활용'}, {label: '복합명사', value: '복합명사'}, {label: '기분석', value: '기분석'}];

  constructor(private srchMorphemeApi: Srch_morphemeApi, private commonModalService: CommonModalService) {

    this.srch_morpheme = new Srch_morpheme();
  }

  ngOnInit() {
  }

  onGroupListSelect(event) {
    console.log(event);
  }

  modalOpen(res){
    console.log(res);
    this.srch_morpheme = res;
    this.commonModal.modalOpen();
    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  morphemeEditSave(){

    console.log(this.srch_morpheme);
    if(this.srch_morpheme == null || this.srch_morpheme.MORPH_NO == null || this.srch_morpheme.KEYWORD == null ||
         this.srch_morpheme.KEYWORD == "" || this.srch_morpheme.MORPH_NO == 0 
    ){
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.srchMorphemeApi.upsert(this.srch_morpheme).subscribe(res =>{
        console.log(res);
      }, error => {
        console.error(error);
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('등록되었습니다');
          this.editClose.emit(true);
          this.modalClose();
        }
      );
    }
  }
 

}
