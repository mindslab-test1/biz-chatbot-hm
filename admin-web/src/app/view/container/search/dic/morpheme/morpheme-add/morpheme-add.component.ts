
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../../common';
import { Srch_morphemeApi } from '../../../../../../adminsdk/services/custom/Srch_morpheme';
import { LoopBackFilter } from '../../../../../../adminsdk/models/BaseModels';
import { Srch_morphemeInterface, Srch_morpheme } from '../../../../../../adminsdk/models/Srch_morpheme';
import { CommonModalService } from '../../../../../../service/common-modal.service';
import {Option} from '../../../../../../model/dom-models';

@Component({
  selector: 'app-morpheme-add',
  templateUrl: './morpheme-add.component.html',
  styleUrls: ['./morpheme-add.component.scss']
})
export class MorphemeAddComponent implements OnInit {


  valid:boolean = false;

  _defaultLast_Morph = [];
  _defaultType = [];

  @Output() addClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  srch_morpheme:Srch_morpheme;
  last_morphList = [{label: 'T', value: 'T'}, {label: 'F', value: 'F'}];
  statusList = [{label: 'Y', value: 'Y'}, {label: 'N', value: 'N'}];
  typeList = [{label: 'NONE', value: 'NONE'}, {label: '활용', value: '활용'}, {label: '복합명사', value: '복합명사'}, {label: '기분석', value: '기분석'}];

  constructor(private srchMorphemeApi: Srch_morphemeApi, private commonModalService: CommonModalService) {
    this.srch_morpheme = new Srch_morpheme();
  }

  ngOnInit() {

  }

  modalOpen(){
    this.commonModal.modalOpen();

    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }

  onGroupListSelect(event) {
     console.log(event);
  }

 
  morphemeAddSave(){
    console.log(this.srch_morpheme);
    if(this.srch_morpheme == null || this.srch_morpheme.MORPH_TAG == null  ||
        this.srch_morpheme.KEYWORD == null || this.srch_morpheme.KEYWORD == "" ||
        this.srch_morpheme.MORPH_TAG == null || this.srch_morpheme.MORPH_TAG == "" ||
        this.srch_morpheme.MEAN_CLASSIFICATION == null || this.srch_morpheme.MEAN_CLASSIFICATION == "" ||
        this.srch_morpheme.STATUS == null || this.srch_morpheme.STATUS == "" ||
        this.srch_morpheme.LAST_MORPH == null || this.srch_morpheme.LAST_MORPH == "" ||
        this.srch_morpheme.TYPE == null || this.srch_morpheme.TYPE == ""
    ) {
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.srchMorphemeApi.upsert(this.srch_morpheme).subscribe(res =>{
      }, error => {
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('등록되었습니다');
          this.addClose.emit(true);
          this.modalClose();
        }
      );
    }
  }
}
