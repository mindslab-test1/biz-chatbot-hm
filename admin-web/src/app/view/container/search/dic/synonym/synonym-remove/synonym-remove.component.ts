import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { CommonModalComponent } from '../../../../../../common';
import { Cm_synonymApi } from '../../../../../../adminsdk/services/custom/Cm_synonym';
import { LoopBackFilter } from '../../../../../../adminsdk/models/BaseModels';
import { Cm_synonymInterface, Cm_synonym } from '../../../../../../adminsdk/models/Cm_synonym';
import { CommonModalService } from '../../../../../../service/common-modal.service';
import {CommonUtilService} from "../../../../../../service";

@Component({
  selector: 'app-dic-synonym-remove',
  templateUrl: './synonym-remove.component.html',
  styleUrls: ['./synonym-remove.component.scss']
})
export class DicSynonymRemoveComponent implements OnInit {


  synonymlist = new Array<string>();
  synonmWord:string;


  valid:boolean = false;

  @Output() removeClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  selectedItem:Cm_synonym;
  items: Cm_synonym[];
  selectedItems: Cm_synonym[] = [];

  constructor(private cmSynonymApi: Cm_synonymApi,
    private commonModalService: CommonModalService,
              private  cm: CommonUtilService
  ) {

    this.selectedItem = new Cm_synonym();
  }

  ngOnInit() {

  }

  modalOpen(res){
    this.selectedItems = res;
    this.commonModal.modalOpen();
  }

  modalClose(){

    this.commonModal.modalClose();
  }
    remove() {
        if(this.selectedItems.length == 1){
            this.removesItem();
        } else {
            this.removesItem();
        }
    }

    private removesItem() {
        const items = this.selectedItems;
        const ids = this.selectedItems.reduce((res,ele)=>{
            res.push(ele.synonym_id);
            return res;
        },[]);
        if (this.cm.isEmpty(ids)){
            this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
            return false;
        }
        this.cmSynonymApi.deleteByIds(ids).subscribe(
            res => {
                this.commonModalService.openSuccessAlert('삭제되었습니다');
                this.removeClose.emit(true);
                this.selectedItems = [];
                this.modalClose();
            }, error => {
                console.log('delete Error');
            });
    }
}
