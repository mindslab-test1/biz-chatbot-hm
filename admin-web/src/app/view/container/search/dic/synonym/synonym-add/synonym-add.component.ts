
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../../common';
import { Cm_synonymApi } from '../../../../../../adminsdk/services/custom/Cm_synonym';
import { LoopBackFilter } from '../../../../../../adminsdk/models/BaseModels';
import { Cm_synonymInterface, Cm_synonym } from '../../../../../../adminsdk/models/Cm_synonym';
import { CommonModalService } from '../../../../../../service/common-modal.service';

@Component({
  selector: 'app-dic-synonym-add',
  templateUrl: './synonym-add.component.html',
  styleUrls: ['./synonym-add.component.scss']
})
export class DicSynonymAddComponent implements OnInit {


  synonymlist = new Array<string>();
  synonmWord:string;
  valid:boolean = false;

  @Output() addClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  cm_synonym:Cm_synonym;

  constructor(private cmSynonymApi: Cm_synonymApi, private commonModalService: CommonModalService) {

    this.cm_synonym = new Cm_synonym();
  }

  ngOnInit() {

  }

  modalOpen(workspaceId){
    this.cm_synonym.workspace_id = workspaceId;
    this.commonModal.modalOpen();

    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  synonymAddSave(){

    this.cm_synonym.synonym_list = this.synonymlist.join(',');
    console.log(this.cm_synonym);
    if(this.cm_synonym == null || this.cm_synonym.workspace_id == null || this.cm_synonym.synonym_list == null || this.cm_synonym.word == null
      || this.cm_synonym.workspace_id == 0 || this.cm_synonym.synonym_list == "" || this.cm_synonym.word == ""
    ){
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.cmSynonymApi.upsert(this.cm_synonym).subscribe(res =>{
      }, error => {
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('등록되었습니다');
          this.addClose.emit(true);
          this.modalClose();
        }
      );
    }
  }

  addSynonymName(e, text:string) {
    // console.log(text);
    if (e.key === 'Enter' && text != null && text != "") {

      this.cmSynonymApi.countList(this.cm_synonym.workspace_id, text).subscribe(res => {
        if (res[0].cnt > 0) {
          this.commonModalService.openErrorAlert('DB에 이미 등록된 데이터입니다');
        } else {
          const index = this.synonymlist.indexOf(text);
          if (index !== -1) {
            this.commonModalService.openErrorAlert('대표어 또는 동의어가 중복됩니다');
          }else{
            this.synonymlist.push(text);
          }
        }
        this.synonmWord = "";
      });
    }
  }
  removeSynonymName(target) {
    console.log(target);
    const index = this.synonymlist.indexOf(target);
    if (index !== -1) {
      this.synonymlist.splice(index, 1);
    }
  }

}
