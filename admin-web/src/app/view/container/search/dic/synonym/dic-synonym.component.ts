import {Component, OnInit, ViewChild, OnDestroy} from '@angular/core';

import {UtilService, CommonModalService, CommonUtilService} from '../../../../../service/index';
import {Cm_synonymApi} from '../../../../../adminsdk/services/custom/Cm_synonym';
import {LoopBackFilter} from '../../../../../adminsdk/models/BaseModels';
import {DicSynonymEditComponent} from './synonym-edit/synonym-edit.component';
import {DicSynonymAddComponent} from './synonym-add/synonym-add.component';
import {DicSynonymRemoveComponent} from './synonym-remove/synonym-remove.component';
import {Cm_synonym} from '../../../../../adminsdk/models/Cm_synonym';
import {UserService} from '../../../../../service/user.service';
import {DataService} from '../../../../../service/data.service';
import {Msg, lang} from '../../../../../model/message';


@Component({
    selector: 'app-dic-synonym',
    templateUrl: 'dic-synonym.component.html',
    styleUrls: ['./dic-synonym.component.scss']
})

export class DicSynonymComponent implements OnInit, OnDestroy {

    totalCount = 0;
    totalPage = 0;
    pg = 1;
    pgSize = 10;
    workspaceId = 0;
    selectText = '';
    items: Cm_synonym[];
    selectedItem: Cm_synonym;
    selectedItems: Cm_synonym[] = [];
    @ViewChild('addChild') addChild: DicSynonymAddComponent;
    @ViewChild('editChild') editChild: DicSynonymEditComponent;
    @ViewChild('removeChild') removeChild: DicSynonymRemoveComponent;

    constructor(private commonModalService: CommonModalService, private cmSynonymApi: Cm_synonymApi, private utilService: UtilService, private userService: UserService, private ds: DataService, private cm: CommonUtilService) {
    }

    ngOnInit() {
        this.workspaceId = this.userService.getWorkspaceIds().pop();
        this.getList(1);
    }

    ngOnDestroy() {
        this.ds.clearData();
    }

    // tslint:disable-next-line:member-ordering
    private _isWorking = false;
    get isWorking() {
        return this._isWorking;
    }

    set isWorking(v: boolean) {
        if (this._isWorking === v) {
            return;
        }
        this._isWorking = v;
        this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
    }

    onSearchKey($event) {
        if (this.isWorking) {
            this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
            return;
        }

        this.getList(1);
    }

    getList(pg) {
        this.pg = pg;
        const pgSize = this.pgSize;

        this.isWorking = true;
        this.cmSynonymApi.countList(this.workspaceId, this.selectText).subscribe(res => {
            if (res[0].cnt <= 0) {
                this.isWorking = false;
                this.totalCount = 0;
                this.totalPage = 1;
                this.items = [];
                // this.commonModalService.openErrorAlert('No data');
                // console.log('no data');
            } else {
                this.totalCount = res[0].cnt;
                this.totalPage = Math.ceil(res[0].cnt / pgSize);
                const skip = (pg - 1) * pgSize;
                const limit = pgSize;
                const order = `synonym_id desc`;
                console.log('this.totalCount', this.totalCount);
                this.cmSynonymApi.getList(this.workspaceId, skip, limit, order, this.selectText).subscribe(res => {
                    this.items = res;
                    this.isWorking = false;
                }, subErr => {
                    this.isWorking = false;
                    console.log(subErr);
                });
            }
        }, err => {
            this.isWorking = false;
            console.error(err);
        }, () => {
        });
    }

    pageChanged(res) {
        if (res > 0 && res <= this.totalPage) {
            this.getList(res);
        }
    }

    synonymEditOpen(res) {
        this.editChild.modalOpen(res);
    }

    synonymRemoveOpen(res) {
        const items = [];
        items.push(res);
        console.log('items  === ' + items);
        this.removeChild.modalOpen(items);
    }

    removesOpen(evnet) {
        const items = this.selectedItems;
        console.log('debug set remove', event, items);
        if (this.cm.isEmpty(items)) {
            console.log('debug remove. stop. no select items');
            return;
        }
        this.removeChild.modalOpen(items);
    }

    synonymAddOpen() {
        this.addChild.modalOpen(this.workspaceId);
    }

    modalClose(res) {
        if (res) {
            this.getList(1);
        }
    }

    private _finishWorking = () => {
        this.commonModalService.closeAlertDialog();
        this.isWorking = false;
    };

    public applyServer() {
        if (confirm('서버 적용 하시겠습니까?')) {
            this.commonModalService.openCommonDialogOnlyMsg('서버적용 중...', '서버 적용중...');
            this.isWorking = true;

            this.cmSynonymApi.apply().subscribe(
                res => {
                    if (res) {
                        alert('서버 적용 되었습니다.');
                    } else {
                        alert('서버 적용에 실패했습니다. 잠시 후 다시 시도해주세요.');
                    }
                }, error => {
                    alert('서버 적용에 실패했습니다. 잠시 후 다시 시도해주세요.');
                }
            );

            this._finishWorking();


        }
    }

    public uploadFile(e: any) {
        if (confirm('Are you sure you want to upload the file?')) {

            this.commonModalService.openCommonDialogOnlyMsg('엑셀 업로드 중...', '엑셀 업로드 중...');
            this.isWorking = true;
            this.utilService.importExcel(e).subscribe(items => {
                console.log(items);
                const array = new Array<Cm_synonym>();
                let index = 0;
                let result = 0;
                for (const item of items) {
                    let list: string [] = [];
                    const temp: string [] = [];
                    // const duTemp: string [] = [];
                    console.log(typeof item[0]);
                    if (index > 0) {

                        if (item[1] !== undefined) {
                            list = item[2].split(',');
                            for (const str of list) {
                                temp.push(str.trim());
                            }
                            const cm_synonym = new Cm_synonym();
                            if (item[0] > 0) {
                                cm_synonym.synonym_id = item[0];
                            } else {
                                cm_synonym.synonym_id = 0;
                            }
                            cm_synonym.workspace_id = this.workspaceId;
                            cm_synonym.word = item[1];
                            cm_synonym.synonym_list = temp.join(',');

                            // 중복제거 필요
                            this.cmSynonymApi.upsert(cm_synonym).subscribe(response => {
                                    console.log(response);
                                    console.log('update => id: ' + item[0] + 'word: ' + item[1] + ', synonymList: ' + temp.join(','));
                                    result++;
                                }
                            );
                        }
                    }
                    index++;
                }
                this._finishWorking();
                if (result === index) {
                    alert('Your request complete successfully.');
                }
                this.getList(1);
            }, error => {
                this._finishWorking();
                console.error(error);
            });
        }
    }

    public downloadFile() {
        const sheetData: any[] = [];
        let items;

        const filter: LoopBackFilter = {
            where: {workspace_id: this.workspaceId},
            order: `synonym_id asc`
        };

        this.commonModalService.openCommonDialogOnlyMsg('엑셀 다운로드 중...', '엑셀 다운로드 중...');
        this.isWorking = true;
        this.cmSynonymApi.find(filter).subscribe(res => {
                console.log('res :', res);
                items = res;
            }, error => {
                this._finishWorking();
                console.error(error);
                // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
            },
            () => {
                if (items.length > 0) {
                    sheetData.push(['ID', 'WORD', 'SYNONYMLIST']);
                    for (const item of items) {
                        sheetData.push([item.synonym_id, item.word, item.synonym_list]);
                    }
                    this.utilService.exportExcel('SynonymList', sheetData);
                }
                this._finishWorking();
            }
        );
    }

}
