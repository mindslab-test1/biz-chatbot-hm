import {Component, OnInit, ViewChild, Output, EventEmitter, ElementRef} from '@angular/core';
import {CommonModalComponent} from '../../../../../../common';
import {Srch_stopwordApi} from '../../../../../../adminsdk/services/custom/Srch_stopword';
import {LoopBackFilter} from '../../../../../../adminsdk/models/BaseModels';
import {Srch_stopwordInterface, Srch_stopword} from '../../../../../../adminsdk/models/Srch_stopword';
import {CommonModalService} from '../../../../../../service/common-modal.service';

@Component({
    selector: 'app-stopword-add',
    templateUrl: './stopword-add.component.html',
    styleUrls: ['./stopword-add.component.scss']
})
export class StopwordAddComponent implements OnInit {


    synonymlist = new Array<string>();
    synonmWord: string;
    valid = false;
    selectOption = 1;
    // TypeOptionList = [{label: 'TYPE', value: 'TYPE'},{label: 'TYPE', value: 'TYPE'}];
    // StatusOptionList = [{label: 'Y', value: 'Y'}, {label: 'N', value: 'N'}];

    @Output() addClose = new EventEmitter<Boolean>();
    @ViewChild('commonModal') commonModal: CommonModalComponent;
    @ViewChild('wordInput') wordInput: ElementRef;
    srch_stopword: Srch_stopword;

    constructor(private srchStopwordApi: Srch_stopwordApi, private commonModalService: CommonModalService) {

        this.srch_stopword = new Srch_stopword();
    }

    ngOnInit() {

    }

    modalOpen() {
        this.commonModal.modalOpen();

        setTimeout(() => this.wordInput.nativeElement.focus(), 0);
    }

    modalClose() {

        this.commonModal.modalClose();
    }

    stopwordAddSave() {
        // his.srch_stopword.stop_no = this.synonymlist.join(',')
        console.log(this.srch_stopword);
        if (this.srch_stopword == null || this.srch_stopword.STOPWORD === '' || this.srch_stopword.STOPWORD == null) {
            this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
        } else {
            this.srchStopwordApi.upsert(this.srch_stopword).subscribe(res => {
                }, error => {
                    // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
                },
                () => {
                    this.commonModalService.openSuccessAlert('등록되었습니다');
                    this.addClose.emit(true);
                    this.modalClose();
                }
            );
        }
    }

}
