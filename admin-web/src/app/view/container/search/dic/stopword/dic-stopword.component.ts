import {Component, OnInit, ViewChild, OnDestroy} from '@angular/core';

import {UtilService, CommonModalService, CommonUtilService} from '../../../../../service/index';
import {Srch_stopwordApi} from '../../../../../adminsdk/services/custom/Srch_stopword';
import {LoopBackFilter} from '../../../../../adminsdk/models/BaseModels';
import {StopwordEditComponent} from './stopword-edit/stopword-edit.component';
import {StopwordAddComponent} from './stopword-add/stopword-add.component';
import {StopwordRemoveComponent} from './stopword-remove/stopword-remove.component';
import {UserService} from '../../../../../service/user.service';
import {DataService} from '../../../../../service/data.service';
import {Msg, lang} from '../../../../../model/message';
import {CommonModalComponent} from '../../../../../common';
import {Srch_stopword} from '../../../../../adminsdk/models/Srch_stopword';
import {Http, Response} from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {isNodeArray} from 'tslint/lib/rules/typedefRule';
import {Srch_morpheme} from '../../../../../adminsdk/models';


@Component({
    selector: 'app-dic-stopword',
    templateUrl: './dic-stopword.component.html',
    styleUrls: ['./dic-stopword.component.scss'],
})

export class DicStopwordComponent implements OnInit, OnDestroy {

    totalCount = 0;
    totalPage = 0;
    pg = 1;
    pgSize = 10;
    selectText = '';
    items: Srch_stopword[];
    selectedItems: Srch_stopword[] = [];
    selectedItem: Srch_stopword;
    @ViewChild('addChild') addChild: StopwordAddComponent;
    @ViewChild('editChild') editChild: StopwordEditComponent;
    @ViewChild('removeChild') removeChild: StopwordRemoveComponent;
    private _isWorking = false;

    constructor(private commonModalService: CommonModalService,
                private srchStopwordApi: Srch_stopwordApi,
                private utilService: UtilService,
                private ds: DataService,
                private cm: CommonUtilService,
                private httpClient: HttpClient
    ) {
    }

    ngOnInit() {
        // this.workspaceId = this.userService.getWorkspaceIds().pop();
        this.getList(1);
    }

    ngOnDestroy() {
        this.ds.clearData();
    }

    get isWorking() {
        return this._isWorking;
    }

    set isWorking(v: boolean) {
        if (this._isWorking === v) {
            return;
        }
        this._isWorking = v;
        this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
    }

    onSearchKey($event) {
        if (this.isWorking) {
            this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
            return;
        }

        this.getList(1);
    }


    getList(pg) {
        this.pg = pg;
        const pgSize = this.pgSize;

        this.isWorking = true;
        this.srchStopwordApi.countList(this.selectText).subscribe(res => {
            if (res[0].cnt <= 0) {
                this.isWorking = false;
                this.totalCount = 0;
                this.totalPage = 1;
                this.items = [];
                // this.commonModalService.openErrorAlert('No data');
                // console.log('no data');
            } else {
                this.totalCount = res[0].cnt;
                this.totalPage = Math.ceil(res[0].cnt / pgSize);
                const skip = (pg - 1) * pgSize;
                const limit = pgSize;
                const order = `STOP_NO desc`;
                console.log('this.totalCount', this.totalCount);
                this.srchStopwordApi.getList(skip, limit, order, this.selectText).subscribe(res => {
                    console.log('res :', res);
                    this.items = res;
                    this.isWorking = false;
                }, subErr => {
                    this.isWorking = false;
                    console.log(subErr);
                });
            }
        }, err => {
            this.isWorking = false;
            console.error(err);
        }, () => {
        });
    }

    pageChanged(res) {
        if (res > 0 && res <= this.totalPage) {
            this.getList(res);
        }
    }

    stopwordEditOpen(res) {
        this.editChild.modalOpen(res);
    }

    stopwordRemoveOpen(res) {
        const items = [];
        items.push(res);
        console.log('items  === ' + items);
        this.removeChild.modalOpen(items);
    }

    removesOpen(evnet) {
        const items = this.selectedItems;
        console.log('debug set remove', event, items);
        if (this.cm.isEmpty(items)) {
            console.log('debug remove. stop. no select items');
            return;
        }
        this.removeChild.modalOpen(items);
    }

    stopwordAddOpen() {
        this.addChild.modalOpen();
    }

    modalClose(res) {
        if (res) {
            this.getList(1);
        }
    }

    private _finishWorking = () => {
        this.commonModalService.closeAlertDialog();
        this.isWorking = false;
    };

    public applyServer() {
        if (confirm('서버 적용 하시겠습니까?')) {
            this.commonModalService.openCommonDialogOnlyMsg('서버적용 중...', '서버 적용중...');
            this.isWorking = true;

            this.srchStopwordApi.apply().subscribe(
                res => {
                    if (res) {
                        alert('서버 적용 되었습니다.');
                    } else {
                        alert('서버 적용에 실패했습니다. 잠시 후 다시 시도해주세요.');
                    }
                }, error => {
                    alert('서버 적용에 실패했습니다. 잠시 후 다시 시도해주세요.');
                }
            );

            this._finishWorking();
            // console.log('success');

        }
    }

    public uploadFile(e: any) {
        if (!e.srcElement.value) {
            return false;
        }

        if (confirm('Are you sure you want to upload the file?')) {

            this.commonModalService.openCommonDialogOnlyMsg('엑셀 업로드 중...', '엑셀 업로드 중...');
            this.isWorking = true;
            this.utilService.importExcel(e).subscribe(items => {
                console.log('1 =====  ' + items);
                const array = new Array<Srch_stopword>();
                let index = 0;
                let result = 0;
                for (const item of items) {
                    if (index > 0) {

                        if (item[1] !== undefined) {
                            const srch_stopword = new Srch_stopword();
                            if (item[0] > 0) {
                                srch_stopword.STOP_NO = item[0];
                            } else {
                                srch_stopword.STOP_NO = 0;
                            }
                            srch_stopword.STOPWORD = item[1];

                            // 중복제거 필요
                            this.srchStopwordApi.upsert(srch_stopword).subscribe(response => {
                                    console.log(response);
                                    result++;
                                }
                            );
                        }
                    }
                    index++;
                }
                this._finishWorking();
                if (result === index) {
                    alert('Your request complete successfully.');
                }
                this.getList(1);
            }, error => {
                this._finishWorking();
                console.error(error);
            });
        }
    }

    public downloadFile() {
        const sheetData: any[] = [];
        let items;

        const filter: LoopBackFilter = {
            order: `STOP_NO asc`
        };

        this.commonModalService.openCommonDialogOnlyMsg('엑셀 다운로드 중...', '엑셀 다운로드 중...');
        this.isWorking = true;
        this.srchStopwordApi.find(filter).subscribe(res => {
                console.log('res :', res);
                items = res;
            }, error => {
                this._finishWorking();
                console.error(error);
                // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
            },
            () => {
                if (items.length > 0) {
                    sheetData.push(['STOP_NO', 'STOPWORD']);
                    for (const item of items) {
                        sheetData.push([item.STOP_NO, item.STOPWORD]);
                    }
                    this.utilService.exportExcel('StopwordList', sheetData);
                }
                this._finishWorking();
            }
        );
    }


}
