import {Component, OnInit, ViewChild, Output, EventEmitter} from '@angular/core';
import {CommonModalComponent} from '../../../../../../common';
import {Srch_stopwordApi} from '../../../../../../adminsdk/services/custom/Srch_stopword';
import {LoopBackFilter} from '../../../../../../adminsdk/models/BaseModels';
import {Srch_stopwordInterface, Srch_stopword} from '../../../../../../adminsdk/models/Srch_stopword';
import {CommonModalService} from '../../../../../../service/common-modal.service';
import {CommonUtilService} from '../../../../../../service';
import {element} from 'protractor';

@Component({
    selector: 'app-stopword-remove',
    templateUrl: './stopword-remove.component.html',
    styleUrls: ['./stopword-remove.component.scss']
})
export class StopwordRemoveComponent implements OnInit {

    valid: boolean = false;

    @Output() removeClose = new EventEmitter<Boolean>();
    @ViewChild('commonModal') commonModal: CommonModalComponent;
    selectedItem: Srch_stopword;
    selectedItems: Srch_stopword[] = [];
    items: Srch_stopword[];

    constructor(
        private srchStopwordApi: Srch_stopwordApi,
        private commonModalService: CommonModalService,
        private cm: CommonUtilService,
    ) {

        this.selectedItem = new Srch_stopword();
    }

    ngOnInit() {

    }

    modalOpen(res) {
        this.selectedItems = res;
        this.commonModal.modalOpen();
    }

    modalClose() {
        this.commonModal.modalClose();
    }

    remove() {
        if (this.selectedItems.length === 1) {
            this.removesItem();
        } else {
            this.removesItem();
        }
    }

    private removesItem() {
        const items = this.selectedItems;
        const ids = this.selectedItems.reduce((res, ele) => {
            res.push(ele.STOP_NO);
            return res;
        }, []);
        if (this.cm.isEmpty(ids)) {
            this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
            return false;
        }
        this.srchStopwordApi.deleteByIds(ids).subscribe(
            res => {
                this.commonModalService.openSuccessAlert('삭제되었습니다');
                this.removeClose.emit(true);
                this.selectedItems = [];
                this.modalClose();
            }, error => {
                console.log('delete Error');
            });
    }
}
