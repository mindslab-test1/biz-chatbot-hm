import {Component, OnInit, ViewChild, Output, EventEmitter, ElementRef} from '@angular/core';
import {CommonModalComponent} from '../../../../../../common';
import {Srch_stopwordApi} from '../../../../../../adminsdk/services/custom/Srch_stopword';
import {LoopBackFilter} from '../../../../../../adminsdk/models/BaseModels';
import {Srch_stopwordInterface, Srch_stopword} from '../../../../../../adminsdk/models/Srch_stopword';
import {CommonModalService} from '../../../../../../service/common-modal.service';

@Component({
    selector: 'app-stopword-edit',
    templateUrl: './stopword-edit.component.html',
    styleUrls: ['./stopword-edit.component.scss']
})
export class StopwordEditComponent implements OnInit {


    valid = false;

    @Output() editClose = new EventEmitter<Boolean>();
    @ViewChild('commonModal') commonModal: CommonModalComponent;
    @ViewChild('wordInput') wordInput: ElementRef;
    srch_stopword: Srch_stopword;
    selectOption = 1;

    constructor(private srchStopwordApi: Srch_stopwordApi, private commonModalService: CommonModalService) {

        this.srch_stopword = new Srch_stopword();
    }

    ngOnInit() {
    }

    modalOpen(res) {
        console.log(res);
        this.srch_stopword = res;
        this.commonModal.modalOpen();
        setTimeout(() => this.wordInput.nativeElement.focus(), 0);
    }

    modalClose() {

        this.commonModal.modalClose();
    }

    stopwordEditSave() {
        if (this.srch_stopword == null
        ) {
            this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
        } else {
            this.srchStopwordApi.upsert(this.srch_stopword).subscribe(res => {

                }, error => {
                    console.error(error);
                    // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
                },
                () => {
                    this.commonModalService.openSuccessAlert('등록되었습니다');
                    this.editClose.emit(true);
                    this.modalClose();
                }
            );
        }
    }

}
