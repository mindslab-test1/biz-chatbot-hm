import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { UtilService, CommonModalService, CommonUtilService } from '../../../../service/index';
import { LoopBackFilter } from '../../../../adminsdk/models/BaseModels';
import { RankingEditComponent } from './ranking-edit/ranking-edit.component';
import { RankingAddComponent } from './ranking-add/ranking-add.component';
import { RankingRemoveComponent } from './ranking-remove/ranking-remove.component';
import { UserService } from '../../../../service/user.service';
import { DataService } from '../../../../service/data.service';
import { Msg, lang } from '../../../../model/message';
import { CommonModalComponent } from '../../../../common';
import {Srch_rankingApi} from '../../../../adminsdk/services/custom';
import {Srch_ranking} from '../../../../adminsdk/models';


@Component({
    selector: 'app-ranking',
    templateUrl: './ranking.component.html',
    styleUrls: ['./ranking.component.scss'],
})

export class RankingComponent implements OnInit, OnDestroy {

  totalCount = 0;
  totalPage = 0;
  pg = 1;
  pgSize = 20;
  selectText = '';
  items: Srch_ranking[];
  selectedItems: Srch_ranking[] = [];
  selectedItem: Srch_ranking;
  @ViewChild('addChild') addChild: RankingAddComponent;
  @ViewChild('editChild') editChild: RankingEditComponent;
  @ViewChild('removeChild') removeChild: RankingRemoveComponent;
  private _isWorking = false;

  constructor(
      private commonModalService: CommonModalService,
      private srchRankingApi: Srch_rankingApi,
      private utilService: UtilService,
      private ds: DataService,
      private cm: CommonUtilService

  ) {
  }

  ngOnInit() {
    // this.workspaceId = this.userService.getWorkspaceIds().pop();
    this.getList(1);
  }
  ngOnDestroy() {
    this.ds.clearData();
  }

  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if (this._isWorking === v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }
  onSearchKey($event) {
    if (this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }

    this.getList(1);
  }

  getList(pg) {
    this.pg = pg;
    const pgSize = this.pgSize;

    this.isWorking = true;
    this.srchRankingApi.countList( this.selectText).subscribe(res => {
      if (res[0].cnt <= 0) {
        this.isWorking = false;
        this.totalCount = 0;
        this.totalPage = 1;
        // this.commonModalService.openErrorAlert('No data');
        // console.log('no data');
      } else {
        this.totalCount = res[0].cnt;
        this.totalPage = Math.ceil(res[0].cnt / pgSize);
        const skip = (pg - 1) * pgSize;
        const limit = pgSize;
        const order = `ID asc`;
        console.log('this.totalCount' , this.totalCount);
        this.srchRankingApi.getList( skip, limit, order, this.selectText).subscribe(res => {
          console.log('res :', res);
          this.items = res;
          this.isWorking = false;
        }, subErr => {
          this.isWorking = false;
          console.log(subErr);
        });
      }
    }, err => {
      this.isWorking = false;
      console.error(err);
    }, () => {
    });
  }
  pageChanged(res) {
    console.log(res);
    this.getList(res);
  }
  rankingEditOpen(res) {
    this.editChild.modalOpen(res);
  }

  removesOpen(evnet) {
    const items  = this.selectedItems;
    console.log('debug set remove', event, items);
    if (this.cm.isEmpty(items)) {
      console.log('debug remove. stop. no select items');
      return;
    }
    this.removeChild.modalOpen(items);
  }
  rankingRemoveOpen(res) {
    const items = [];
    items.push(res);
    this.removeChild.modalOpen(items);
  }
  rankingAddOpen() {
    this.addChild.modalOpen();
  }
  modalClose(res) {
    if (res) {
      this.getList(1);
    }
  }

  private _finishWorking = () => {
    this.commonModalService.closeAlertDialog();
    this.isWorking = false;
  };

}
