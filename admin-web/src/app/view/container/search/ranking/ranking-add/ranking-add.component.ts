
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { CommonModalService } from '../../../../../service/common-modal.service';
import {Srch_ranking} from '../../../../../adminsdk/models';
import {Srch_rankingApi} from '../../../../../adminsdk/services/custom';

@Component({
  selector: 'app-ranking-add',
  templateUrl: './ranking-add.component.html',
  styleUrls: ['./ranking-add.component.scss']
})
export class RankingAddComponent implements OnInit {


  synonymlist = new Array<string>();
  synonmWord:string;
  valid:boolean = false;

  @Output() addClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  srch_ranking:Srch_ranking;

  constructor(private srchRankingApi: Srch_rankingApi, private commonModalService: CommonModalService) {

    this.srch_ranking= new Srch_ranking();
  }

  ngOnInit() {

  }

  modalOpen(){
    this.commonModal.modalOpen();

    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  rankingAddSave(){
    if(this.srch_ranking== null  || this.srch_ranking.FIELD_NAME== ""
    ){
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.srchRankingApi.upsert(this.srch_ranking).subscribe(res =>{
      }, error => {
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('등록되었습니다');
          this.addClose.emit(true);
          this.modalClose();
        }
      );
    }
  }

}
