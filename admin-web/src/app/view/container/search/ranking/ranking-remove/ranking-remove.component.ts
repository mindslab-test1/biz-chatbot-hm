import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { CommonModalService } from '../../../../../service/common-modal.service';
import {Srch_ranking, Srch_spellcheck} from '../../../../../adminsdk/models';
import {CommonUtilService} from "../../../../../service";
import {Srch_rankingApi} from '../../../../../adminsdk/services/custom';

@Component({
  selector: 'app-ranking-remove',
  templateUrl: './ranking-remove.component.html',
  styleUrls: ['./ranking-remove.component.scss']
})
export class RankingRemoveComponent implements OnInit {

  valid:boolean = false

  @Output() removeClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;

  selectedItem:Srch_ranking;
  selectedItems: Srch_ranking[] = [];
  items: Srch_ranking[];

  constructor(
      private srchRankingApi: Srch_rankingApi,
      private commonModalService: CommonModalService,
      private cm: CommonUtilService
  ) {

    this.selectedItem = new Srch_ranking();
  }

  ngOnInit() {

  }

  modalOpen(res){
    this.selectedItems = res;
    this.commonModal.modalOpen();
  }

  modalClose(){

    this.commonModal.modalClose();
  }

    remove() {
        if (this.selectedItems.length == 1) {
            this.removesItem();
        } else {
            this.removesItem();
        }
    }

    private removesItem() {
            const ids = this.selectedItems.reduce((res,ele)=>{
                res.push(ele.ID);
                return res;
            },[]);
            if (this.cm.isEmpty(ids)){
                this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
                return false;
            }
            this.srchRankingApi.deleteByIds(ids).subscribe(
                res => {
                    this.commonModalService.openSuccessAlert('삭제되었습니다');
                    this.removeClose.emit(true);
                    this.selectedItems = [];
                    this.modalClose();
                }, error => {
                    console.log('delete Error');
                });
        }
}
