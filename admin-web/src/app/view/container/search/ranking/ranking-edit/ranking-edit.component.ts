
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { CommonModalService } from '../../../../../service/common-modal.service';
import {Srch_rankingApi} from '../../../../../adminsdk/services/custom';
import {Srch_ranking} from '../../../../../adminsdk/models';

@Component({
  selector: 'app-ranking-edit',
  templateUrl: './ranking-edit.component.html',
  styleUrls: ['./ranking-edit.component.scss']
})
export class RankingEditComponent implements OnInit {


  valid:boolean = false;

  @Output() editClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  srch_ranking:Srch_ranking;

  constructor(private srchRankingApi: Srch_rankingApi, private commonModalService: CommonModalService) {

    this.srch_ranking = new Srch_ranking();
  }

  ngOnInit() {
  }

  modalOpen(res){
    console.log(res);
    this.srch_ranking = res;
    this.commonModal.modalOpen();
    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  rankingEditSave(){
    if( this.srch_ranking == null
    ){
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    }else{
      this.srchRankingApi.upsert(this.srch_ranking).subscribe(res =>{
          
        console.log(res);
      }, error => {
        console.error(error);
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('수정되었습니다');
          this.editClose.emit(true);
          this.modalClose();
        }
      );
    }
  }

}
