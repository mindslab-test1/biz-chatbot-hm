
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import {CommonModalComponent} from '../../../../../common';
import {CommonModalService} from '../../../../../service';
import {Nlp_synonym} from '../../../../../adminsdk/models';
import {Nlp_synonymApi} from '../../../../../adminsdk/services/custom';

@Component({
  selector: 'nlp-dic-synonym-edit',
  templateUrl: './nlp-synonym-edit.component.html',
  styleUrls: ['./nlp-synonym-edit.component.scss']
})
export class NlpSynonymEditComponent implements OnInit {

  synonymList = new Array<string>();
  synonymWord: string;
  valid = false;

  @Output() editClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal: CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  nlp_synonym: Nlp_synonym;

  constructor(
      private nlpSynonymApi: Nlp_synonymApi
      , private commonModalService: CommonModalService
  ) {
    this.nlp_synonym = new Nlp_synonym();
  }

  ngOnInit() {
  }

  modalOpen(res) {
    console.log(res);
    this.nlp_synonym = res;
    this.synonymList = this.nlp_synonym.synonym_list.split(',');
    this.commonModal.modalOpen();
    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose() {

    this.commonModal.modalClose();
  }
  synonymEditSave() {

    this.nlp_synonym.synonym_list = this.synonymList.join(',');
    console.log('nlp_synonym :: ' + this.nlp_synonym);
    if (this.nlp_synonym == null || this.nlp_synonym.type == null || this.nlp_synonym.synonym_list == null || this.nlp_synonym.word == null
      || this.nlp_synonym.type === 0 || this.nlp_synonym.synonym_list === '' || this.nlp_synonym.word === ''
    ) {
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    } else {
      this.nlpSynonymApi.upsert(this.nlp_synonym).subscribe(res => {
        console.log(res);
      }, error => {
        console.error(error);
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('등록되었습니다');
          this.editClose.emit(true);
          this.modalClose();
        }
      );
    }
  }
  addSynonymName(e, text: string) {
    // console.log(text);
    if (e.key === 'Enter' && text != null && text !== '') {
      this.nlpSynonymApi.countList(this.nlp_synonym.type, text).subscribe(res => {
        if (res[0].cnt > 0) {
          this.commonModalService.openErrorAlert('DB에 이미 등록된 데이터입니다');
        } else {
          const index = this.synonymList.indexOf(text);
          if (index !== -1) {
            this.commonModalService.openErrorAlert('대표어 또는 동의어가 중복됩니다');
          } else {
            this.synonymList.push(text);
          }
        }
        this.synonymWord = '';
      });
    }
  }
  removeSynonymName(target) {
    console.log(target);
    const index = this.synonymList.indexOf(target);
    if (index !== -1) {
      this.synonymList.splice(index, 1);
    }
  }

}
