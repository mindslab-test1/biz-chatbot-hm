import {Component, OnInit, ViewChild, OnDestroy} from '@angular/core';

import {LoopBackFilter, Nlp_synonym} from '../../../../adminsdk/models';
import {Nlp_synonymApi} from '../../../../adminsdk/services/custom';
import {NlpSynonymAddComponent} from './synonym-add/nlp-synonym-add.component';
import {NlpSynonymEditComponent} from './synonym-edit/nlp-synonym-edit.component';
import {NlpSynonymRemoveComponent} from './synonym-remove/nlp-synonym-remove.component';
import {CommonModalService, CommonUtilService, UtilService} from '../../../../service';
import {UserService} from '../../../../service/user.service';
import {DataService} from '../../../../service/data.service';
import {Router} from '@angular/router';
import {lang, Msg} from '../../../../model/message';


@Component({
    selector: 'nlp-dic-synonym',
    templateUrl: 'nlp-synonym.component.html',
    styleUrls: ['./nlp-synonym.component.scss']
})

export class NlpSynonymComponent implements OnInit, OnDestroy {

    totalCount = 0;
    totalPage = 0;
    pg = 1;
    pgSize = 10;
    type = 0;
    selectText = '';
    items: Nlp_synonym[];
    selectedItem: Nlp_synonym;
    selectedItems: Nlp_synonym[] = [];
    @ViewChild('addChild') addChild: NlpSynonymAddComponent;
    @ViewChild('editChild') editChild: NlpSynonymEditComponent;
    @ViewChild('removeChild') removeChild: NlpSynonymRemoveComponent;
    title = '';

    constructor(
        private commonModalService: CommonModalService
        , private nlpSynonymApi: Nlp_synonymApi
        , private utilService: UtilService
        , private userService: UserService
        , private ds: DataService
        , private cm: CommonUtilService
        , private router: Router
    ) {
    }

    ngOnInit() {
        const url = this.router.url.split('/')[4];
        switch (url) {
            case 'kor_eng':
                this.type = 1;
                this.title = '국문 & 영문';
                break;
            case 'abbreviation':
                this.type = 2;
                this.title = '축약어';
                break;
            case 'synonyms':
                this.type = 3;
                this.title = '동의어';
                break;
            case 'spacing':
                this.type = 4;
                this.title = '띄어쓰기';
                break;
        }
        this.getList(1);
    }

    ngOnDestroy() {
        this.ds.clearData();
    }

    // tslint:disable-next-line:member-ordering
    private _isWorking = false;
    get isWorking() {
        return this._isWorking;
    }

    set isWorking(v: boolean) {
        if (this._isWorking === v) {
            return;
        }
        this._isWorking = v;
        this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
    }

    onSearchKey($event) {
        if (this.isWorking) {
            this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
            return;
        }

        this.getList(1);
    }

    getList(pg) {
        this.pg = pg;
        const pgSize = this.pgSize;

        this.isWorking = true;
        this.nlpSynonymApi.countList(this.type, this.selectText).subscribe(res => {
            if (res[0].cnt <= 0) {
                this.isWorking = false;
                this.totalCount = 0;
                this.totalPage = 1;
                this.items = [];
                // this.commonModalService.openErrorAlert('No data');
                // console.log('no data');
            } else {
                this.totalCount = res[0].cnt;
                this.totalPage = Math.ceil(res[0].cnt / pgSize);
                const skip = (pg - 1) * pgSize;
                const limit = pgSize;
                const order = `synonym_id desc`;
                console.log('this.totalCount', this.totalCount);
                this.nlpSynonymApi.getList(this.type, skip, limit, order, this.selectText).subscribe(res => {
                    this.items = res;
                    this.isWorking = false;
                }, subErr => {
                    this.isWorking = false;
                    console.log(subErr);
                });
            }
        }, err => {
            this.isWorking = false;
            console.error(err);
        }, () => {
        });
    }

    pageChanged(res) {
        if (res > 0 && res <= this.totalPage) {
            this.getList(res);
        }
    }

    synonymEditOpen(res) {
        this.editChild.modalOpen(res);
    }

    synonymRemoveOpen(res) {
        const items = [];
        items.push(res);
        console.log('items  === ' + items);
        this.removeChild.modalOpen(items);
    }

    removesOpen(evnet) {
        const items = this.selectedItems;
        console.log('debug set remove', event, items);
        if (this.cm.isEmpty(items)) {
            console.log('debug remove. stop. no select items');
            return;
        }
        this.removeChild.modalOpen(items);
    }

    synonymAddOpen() {
        this.addChild.modalOpen(this.type);
    }

    modalClose(res) {
        if (res) {
            this.getList(1);
        }
    }

    private _finishWorking = () => {
        this.commonModalService.closeAlertDialog();
        this.isWorking = false;
    }

    // public applyServer() {
    //     if (confirm('서버 적용 하시겠습니까?')) {
    //         this.commonModalService.openCommonDialogOnlyMsg('서버적용 중...', '서버 적용중...');
    //         this.isWorking = true;
    //
    //         this.nlpSynonymApi.apply().subscribe(
    //             res => {
    //                 if (res) {
    //                     alert('서버 적용 되었습니다.');
    //                 } else {
    //                     alert('서버 적용에 실패했습니다. 잠시 후 다시 시도해주세요.');
    //                 }
    //             }, error => {
    //                 alert('서버 적용에 실패했습니다. 잠시 후 다시 시도해주세요.');
    //             }
    //         );
    //
    //         this._finishWorking();
    //
    //
    //     }
    // }

    public uploadFile(e: any) {
        if (confirm('Are you sure you want to upload the file?')) {

            this.commonModalService.openCommonDialogOnlyMsg('엑셀 업로드 중...', '엑셀 업로드 중...');
            this.isWorking = true;
            this.utilService.importExcel(e).subscribe(items => {
                console.log(items);
                const array = new Array<Nlp_synonym>();
                let index = 0;
                let result = 0;
                for (const item of items) {
                    let list: string [] = [];
                    const temp: string [] = [];
                    // const duTemp: string [] = [];
                    console.log(typeof item[0]);
                    if (index > 0) {

                        if (item[1] !== undefined) {
                            list = item[2].split(',');
                            for (const str of list) {
                                temp.push(str.trim());
                            }
                            const nlp_synonym = new Nlp_synonym();
                            if (item[0] > 0) {
                                nlp_synonym.synonym_id = item[0];
                            } else {
                                nlp_synonym.synonym_id = 0;
                            }
                            nlp_synonym.type = this.type;
                            nlp_synonym.word = item[1];
                            nlp_synonym.synonym_list = temp.join(',');

                            // 중복제거 필요
                            this.nlpSynonymApi.upsert(nlp_synonym).subscribe(response => {
                                    console.log(response);
                                    console.log('update => id: ' + item[0] + 'word: ' + item[1] + ', synonymList: ' + temp.join(','));
                                    result++;
                                }
                            );
                        }
                    }
                    index++;
                }
                this._finishWorking();
                if (result === index) {
                    alert('Your request complete successfully.');
                }
                this.getList(1);
            }, error => {
                this._finishWorking();
                console.error(error);
            });
        }
    }

    public downloadFile() {
        const sheetData: any[] = [];
        let items;

        const filter: LoopBackFilter = {
            where: {
                type: this.type,
                text: this.selectText
            },
            order: `synonym_id asc`
        };

        this.commonModalService.openCommonDialogOnlyMsg('엑셀 다운로드 중...', '엑셀 다운로드 중...');
        this.isWorking = true;
        this.nlpSynonymApi.find(filter).subscribe(res => {
                console.log('res :', res);
                items = res;
            }, error => {
                this._finishWorking();
                console.error(error);
                // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
            },
            () => {
                if (items.length > 0) {
                    sheetData.push(['ID', 'WORD', 'SYNONYMLIST']);
                    for (const item of items) {
                        sheetData.push([item.synonym_id, item.word, item.synonym_list]);
                    }
                    this.utilService.exportExcel('NlpSynonymList', sheetData);
                }
                this._finishWorking();
            }
        );
    }

}
