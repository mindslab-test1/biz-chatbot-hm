import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import {CommonModalComponent} from '../../../../../common';
import {CommonModalService, CommonUtilService} from '../../../../../service';
import {Nlp_synonym} from '../../../../../adminsdk/models';
import {Nlp_synonymApi} from '../../../../../adminsdk/services/custom';


@Component({
  selector: 'nlp-dic-synonym-remove',
  templateUrl: './nlp-synonym-remove.component.html',
  styleUrls: ['./nlp-synonym-remove.component.scss']
})
export class NlpSynonymRemoveComponent implements OnInit {

  synonymList = new Array<string>();
  synonymWord: string;
  valid = false;

  @Output() removeClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal: CommonModalComponent;
  selectedItem: Nlp_synonym;
  items: Nlp_synonym[];
  selectedItems: Nlp_synonym[] = [];

  constructor(
      private nlpSynonymApi: Nlp_synonymApi
      , private commonModalService: CommonModalService
      , private  cm: CommonUtilService
  ) {
    this.selectedItem = new Nlp_synonym();
  }

  ngOnInit() {

  }

  modalOpen(res) {
    this.selectedItems = res;
    this.commonModal.modalOpen();
  }

  modalClose() {

    this.commonModal.modalClose();
  }
    remove() {
        if (this.selectedItems.length === 1) {
            this.removesItem();
        } else {
            this.removesItem();
        }
    }

    private removesItem() {
        const items = this.selectedItems;
        const ids = this.selectedItems.reduce((res, ele) => {
            res.push(ele.synonym_id);
            return res;
        }, []);
        if (this.cm.isEmpty(ids)) {
            this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
            return false;
        }
        this.nlpSynonymApi.deleteByIds(ids).subscribe(
            res => {
                this.commonModalService.openSuccessAlert('삭제되었습니다');
                this.removeClose.emit(true);
                this.selectedItems = [];
                this.modalClose();
            }, error => {
                console.log('delete Error');
            });
    }
}
