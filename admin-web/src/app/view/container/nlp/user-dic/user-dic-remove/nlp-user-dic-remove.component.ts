import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import {CommonModalComponent} from '../../../../../common';
import {CommonModalService, CommonUtilService} from '../../../../../service';
import {Nlp_user_dic} from '../../../../../adminsdk/models';
import {Nlp_user_dicApi} from '../../../../../adminsdk/services/custom';


@Component({
  selector: 'nlp-user-dic-remove',
  templateUrl: './nlp-user-dic-remove.component.html',
  styleUrls: ['./nlp-user-dic-remove.component.scss']
})
export class NlpUserDicRemoveComponent implements OnInit {

  @Output() removeClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal: CommonModalComponent;
  selectedItem: Nlp_user_dic;
  items: Nlp_user_dic[];
  selectedItems: Nlp_user_dic[] = [];

  constructor(
      private nlpUserDicApi: Nlp_user_dicApi
      , private commonModalService: CommonModalService
      , private  cm: CommonUtilService
  ) {
    this.selectedItem = new Nlp_user_dic();
  }

  ngOnInit() {

  }

  modalOpen(res) {
    this.selectedItems = res;
    this.commonModal.modalOpen();
  }

  modalClose() {

    this.commonModal.modalClose();
  }
    remove() {
        if (this.selectedItems.length === 1) {
            this.removesItem();
        } else {
            this.removesItem();
        }
    }

    private removesItem() {
        const items = this.selectedItems;
        const ids = this.selectedItems.reduce((res, ele) => {
            res.push(ele.user_dic_id);
            return res;
        }, []);
        if (this.cm.isEmpty(ids)) {
            this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
            return false;
        }
        this.nlpUserDicApi.deleteByIds(ids).subscribe(
            res => {
                this.commonModalService.openSuccessAlert('삭제되었습니다');
                this.removeClose.emit(true);
                this.selectedItems = [];
                this.modalClose();
            }, error => {
                console.log('delete Error');
            });
    }
}
