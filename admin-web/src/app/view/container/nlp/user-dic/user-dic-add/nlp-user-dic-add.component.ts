
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import {CommonModalComponent} from '../../../../../common';
import {CommonModalService} from '../../../../../service';
import {Nlp_user_dic} from '../../../../../adminsdk/models';
import {Nlp_user_dicApi} from '../../../../../adminsdk/services/custom';

@Component({
  selector: 'nlp-user-dic-add',
  templateUrl: './nlp-user-dic-add.component.html',
  styleUrls: ['./nlp-user-dic-add.component.scss']
})
export class NlpUserDicAddComponent implements OnInit {

  @Output() addClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal: CommonModalComponent;
  @ViewChild('wordInput') wordInput: ElementRef;
  nlpUserDic: Nlp_user_dic;

  constructor(
      private nlpUserDicApi: Nlp_user_dicApi
      , private commonModalService: CommonModalService
  ) {
    this.nlpUserDic = new Nlp_user_dic();
  }

  ngOnInit() {

  }

  modalOpen(type) {
    this.nlpUserDic.type = type;
    this.commonModal.modalOpen();

    setTimeout(() => this.wordInput.nativeElement.focus(), 0);
  }

  modalClose() {
    this.commonModal.modalClose();
  }

  addSave() {
    console.log(this.nlpUserDic);
    if (this.nlpUserDic == null || this.nlpUserDic.type == null || this.nlpUserDic.custom_word == null || this.nlpUserDic.word == null
      || this.nlpUserDic.type === 0 || this.nlpUserDic.custom_word === '' || this.nlpUserDic.word === ''
    ) {
      this.commonModalService.openErrorAlert('데이터를 입력해 주세요');
    } else {
      this.nlpUserDicApi.upsert(this.nlpUserDic).subscribe(res => {
      }, error => {
        // this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Create Q&A Fail' }];
      },
        () => {
          this.commonModalService.openSuccessAlert('등록되었습니다');
          this.addClose.emit(true);
          this.modalClose();
        }
      );
    }
  }
}
