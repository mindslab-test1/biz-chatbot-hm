import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MenuService, CommonModalService, CommonUtilService } from '../../../service';
import { environment as env } from '../../../environments/environment';
import { Cm_menu } from '../../../adminsdk';

declare var $: any;

@Component({
  selector: 'main-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(
      private router: Router,
      private menuService: MenuService,
      private commonModalService: CommonModalService,
      private cm: CommonUtilService
  ) {
    this.init();
  }

  private _defaultMenuId = -1;
  @Input() currentMenuIds: Array<Number> = [];
  @Input() currentMenuId: Number = this._defaultMenuId;
  isSimpleMenu = true;

  @Input() menuInfo: any = {};
  // menuInfo: any = {};
  get menuInfoKeys() {
    if (this.cm.isEmpty(this.menuInfo)) {
      return null;
    }
    return this.keys(this.menuInfo);
  }
  MENU_CLASS: any = {
    OPEN: 'open',
    CLOSE: 'close',
    SELECTED: 'selected'
  };

  init() {
    this.isSimpleMenu = true;
    // this.menuService.init();
  }

  $setCurrentMenu(event, url, menu_id) {
    // return this.menuService.setCurrentMenu(url, menu_id);
  }

  keys(dict): Array<string> {
    return Object.keys(dict);
  }


  ngOnInit() {
    this.toggleMenuType();
    this.refreshMenu();
  }

  private refreshMenu() {
    this.menuInfo = env._.cloneDeep(this.menuService.getMenu());
  }

  /**
   * Toggle to simple menu and open menu.
   * @returns {none} None.
   */
  toggleMenuType() {
    this.isSimpleMenu = !this.isSimpleMenu;
    $('.main-section')[this.isSimpleMenu ? 'addClass' : 'removeClass']('icon-menu');
    // this.closeMenuType();
  }
  resetMenuSlide() {
    this.openMenuIds = [];
    this.closeAllSlide();
  }
  closeAllSlide() {
    $('ul').slideUp(0).removeAttr('style');
  }
  animateSlide(target) {
    // const parent = $(target).parent();
    // const toggleTarget = $(target).siblings('ul');
    // if(parent.hasClass(this.MENU_CLASS.OPEN)){
    //   parent.removeClass(this.MENU_CLASS.OPEN);
    //   parent.addClass(this.MENU_CLASS.CLOSE);
    // } else {
    //   parent.removeClass(this.MENU_CLASS.CLOSE);
    //   parent.addClass(this.MENU_CLASS.OPEN);
    // }
    // toggleTarget.slideToggle(100);

    // console.log('target',target);
    // console.log('parent',target.parentElement);
    const parent = target.parentElement;
    const toggleTarget = target.nextElementSibling;
    if (!toggleTarget) {
      return;
    }
    $(toggleTarget).slideToggle(100);
  }
  openMenuIds: number[] = [];
  isOpenedMenu = (menu_id) => this.openMenuIds.includes(menu_id);
  isClosedMenu = (menu_id) => !this.isOpenedMenu(menu_id);
  toggleMenu($event, menu_id: number) {
    menu_id = Number(menu_id);
    if (this.isOpenedMenu(menu_id)) {
      this.openMenuIds = this.openMenuIds.filter((openMenuId: number) => (menu_id !== openMenuId));
    } else {
      this.openMenuIds.push(Number(menu_id));
    }
    this.openMenuIds = this.openMenuIds;
  }
  isOpen(menu: Cm_menu) {
    return (this.selectedMenu(menu) && this.hasChildren(menu)) || this.isOpenedMenu(menu.menu_id);
  }
  isClose(menu: Cm_menu) {
    return !this.isOpen(menu);
  }

  /**
   * Callback to invoke when a parent menu is clicked.
   * @param {object} event
   * @param {number} menu_id : menu id
   * @param {string} path
   * @returns {none} None.
   */
  onSelectParent(event, menu_id: number, menu) {
    const target = event.currentTarget;
    // console.log(this.currentMenu.indexOf(this.menuInfo.menu_id));
    // if(this.currentMenu.indexOf(parseInt(id)) !== this._defaultMenuId) {
    //   return false;
    // }
    // console.log('debug','1depth');
    const path = menu.menu_path;
    if (path === '') {
      if (this.isSimpleMenu && this.menuInfo[menu_id].children.length > 0) {
        this.resetMenuSlide();
        this.router.navigate([`/${env.container}${this.menuInfo[menu_id].children[0].path}`]);
      } else {
        if (this.cm.isNotEmpty(menu.children)) {
          this.animateSlide(target);
          this.toggleMenu(event, menu_id);
        }
      }
    } else {
      this.resetMenuSlide();
      const url = `/${env.container}${path}`;
      this.$setCurrentMenu(event, url, menu_id);
      this.router.navigate([url]);
    }
  }

  /**
   * Callback to invoke when a parent menu is clicked.
   * 2depth 부모 구현 위해서 onSelectParent 복사해서 만듦.
   * @param {object} event
   * @param {string} path
   * @returns {none} None.
   */
  onSelectParent2Depth(event, menu_id: number, menu: Cm_menu) {
    const target = event.currentTarget;
    // console.log(this.currentMenu.indexOf(this.menuInfo.menu_id));
    // if(this.currentMenu.indexOf(parseInt(id)) !== this._defaultMenuId) {
    //   return false;
    // }
    // console.log('debug','2depth');
    const path = menu.menu_path;
    if (path === '') {
      if (this.cm.isNotEmpty(menu.children)) {
        this.animateSlide(target);
        this.toggleMenu(event, menu_id);
      }
    } else {
      this.resetMenuSlide();
      const url = `/${env.container}${path}`;
      this.$setCurrentMenu(event, url, menu_id);
      this.router.navigate([url]);
    }
  }
  /**
   * Callback to invoke when a child menu is clicked.
   * @param {object} event
   * @param {string} path
   * @returns {none} None.
   */
  onSelectChild(event, menu_id: number, menu: Cm_menu) {
    const target = event.currentTarget;
    // console.log('debug','child');
    if (this.currentMenuId === menu_id) {
      return;
    }
    this.resetMenuSlide();
    if (this.cm.isNotEmpty(menu.children)) {
      this.animateSlide(target);
      this.toggleMenu(event, menu_id);
    }
    // this.animateSlide(target);
    // this.toggleMenu(event, menu_id);

    const path = menu.menu_path;
    const url = `/${env.container}${path}`;
    this.$setCurrentMenu(event, url, menu_id);
    this.router.navigate([url]);
  }
  selectedMenu = (menu: Cm_menu) => this.currentMenuIds.indexOf(menu.menu_id) !== this._defaultMenuId;
  noSelectedMenu = (menu: Cm_menu) => !this.selectedMenu(menu);
  hasChildren = (menu: Cm_menu) => this.cm.isNotEmpty(menu.children) && menu.children.length > 0;
  hasNoChildren = (menu: Cm_menu) => !this.hasChildren(menu);
  focusMenu = (menu: Cm_menu) => this.currentMenuId === menu.menu_id;
}

