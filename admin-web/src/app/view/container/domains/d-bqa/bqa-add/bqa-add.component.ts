import {Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef} from '@angular/core';
import {FormGroup, FormArray, Validators, FormBuilder, Form} from '@angular/forms';
import {BasicQAService} from '../../../../../service';
import {Cm_domainApi} from '../../../../../adminsdk/services/custom';
import {LoopBackFilter, Cm_domain} from '../../../../../adminsdk/models';
import {UserService} from '../../../../../service/user.service';
import {CommonModalService, CommonUtilService} from '../../../../../service';
import {ValidatorService} from '../../../../../service/validator.service';
import {FileService} from '../../../../../service/file.service';
import {Option} from '../../../../../model/dom-models';
import {Chatbot_info} from '../../../../../adminsdk';



@Component({
  selector: 'app-bqa-add',
  templateUrl: './bqa-add.component.html',
  styleUrls: ['./bqa-add.component.scss']
})
export class BqaAddComponent implements OnInit {

  addForm: FormGroup;
  answerList: FormArray;
  answerGroup: FormGroup;
  questionList: FormArray;
  domainName = '';
  domainId = 0;
  teamId;
  workspaceId;
  userId;
  roleId;
  @Input() addFlag: Boolean = false;
  @Output() addRemove = new EventEmitter<Boolean>();

  // Rich Contents
  c = this.basicQAService.constants;
  originContentTypeCd = [];
  originContentTypeCds = '';
  _isButton: boolean;
  _isImage: boolean;
  _isCarousel: boolean;
  _isApi: boolean;
  _apiIfId;
  _attribute1Val;
  _attribute2Val;

  _rcButtons = [];
  _rcImage;
  _rcApiList = [];

  _rc_img_path = this.c.IMG_OPTION.DEFAULT_IMG;
  _rc_img_text = '';
  _rc_img_link = '';
  _rc_img_file;

  teamList: Option[];
  selectedTeamcode: string;
  scrollHeight: string;

  @ViewChild('rc_img_input')
  rc_img_input: ElementRef;

  constructor(
      private fb: FormBuilder,
      private basicQAService: BasicQAService,
      private cmDomainApi: Cm_domainApi,
      private commonModalService: CommonModalService,
      private cm: CommonUtilService,
      private validator: ValidatorService,
      private userService: UserService,
      private fileService: FileService
  ) { }

  ngOnInit() {
    this.teamId = this.userService.getTeamId();
    this.workspaceId = this.userService.getWorkspaceIds().pop();
    this.userId = this.userService.getCurrentUserId();
    this.roleId = this.userService.getUserRoleId();
    this.teamListInit();
    this.scrollHeight = '120px';
  }

  public qnaAddOpen(domainId) {

    this.domainId = domainId;
    this.teamId = this.userService.getTeamId();
    this.getDomain(domainId);

    this.questionInit();
    this.rcInit();
  }

  teamListInit() {

      this.teamList = [];
      this.basicQAService.getTeamList().subscribe(res => {
          const temp = res.map((el) => {
              const item: Option = {
                  label: el.sText,
                  value: el.objId
              };
              return item;
          });

          this.teamList.push(...temp);
          console.log(this.teamList);
      });
  }

  getDomain(domainId) {
    if (domainId === 0) {
      this.commonModalService.openErrorAlert('도메인을 선택해주세요');
      return;
    }
    const filter: LoopBackFilter = {
      where: { and: [{ domain_id: Number(domainId) }, { team_id: this.teamId }] },
    };

    if (this.roleId === this.userService.getRoles.ADMIN) {
        filter.where = { and: [{ domain_id: Number(domainId) }] };
    } else {
        filter.where = { and: [{ domain_id: Number(domainId) }, { team_id: this.teamId }] };
    }

    this.cmDomainApi.count(filter.where).subscribe(res => {

          if (res.count <= 0) {
            this.commonModalService.openErrorAlert('도메인을 선택해주세요');
          } else {
            this.cmDomainApi.findOne(filter).subscribe((res2: Cm_domain) => {
              this.domainName = res2.domain_name;
              this.domainId = domainId;
            });
          }
        }, error => {
          this.commonModalService.openErrorAlert('도메인 찾기에 문제가 있습니다.');
          console.error(error);
        }, () => {
        }
    );
  }

  answerInit() {
    this.addForm = this.fb.group({
      question: ['', [Validators.required]],
      answerList: this.fb.array([ this.createAnswerItem() ]),
      domainId: [this.domainId],
      attribute1: [null],
      attribute2: [null]
    });
    this.answerList = this.addForm.get('answerList') as FormArray;
  }

  questionInit() {
    this.addForm = this.fb.group({
      answerGroup: this.createAnswerItem(),
      questionList: this.fb.array([ this.createQuestionItem() ]),
      domainId: [this.domainId],
      attribute1: [null],
      attribute2: [null]
    });

    this.questionList = this.addForm.get('questionList') as FormArray;
    this.answerGroup = this.addForm.get('answerGroup') as FormGroup;
  }


  addAnswerItem(): void {
    this.answerList.push(this.createAnswerItem());
  }

  addQuestionItem(): void {
    this.questionList.push(this.createQuestionItem());
  }

  createAnswerItem(): FormGroup {
    return this.fb.group({
      answer: ['', [Validators.required, Validators.maxLength(500)]],
      respCode: [null],
      useRcFlag: [false],
      qaRichVo: [null],
      contentTypeCd: [null]
    });
  }

    createQuestionItem(): FormGroup {
    return this.fb.group({
      question: ['', [Validators.required, Validators.maxLength(500)]]
    });
  }

  get selectTeamOption(): string {
    return this.selectedTeamcode;
  }

  set selectTeamOption(v: string) {
    this.selectedTeamcode = v;
  }


  rcInit() {
    this.initIsRcs();
    this.rcSetNull();

  /*  for (let i = 0; i < this.answerList.length; i++) {
      this.originContentTypeCd = [];
      const contentTypeCd = this.answerGroup.get('contentTypeCd').value;
      console.log( contentTypeCd);
      this.originContentTypeCd[i] = contentTypeCd;
    }*/

   console.log( this.answerGroup);

   const contentTypeCd = this.answerGroup.get('contentTypeCd').value;
   this.originContentTypeCds = contentTypeCd;
   this.getBqaRichContent();
  }
  rcSetNull() {
    this.rcButtons = null;
    this.rcImage = null;
    this.rcApiList = null;

    this.rc_img_path = this.c.IMG_OPTION.DEFAULT_IMG;
    this.rc_img_text = '';
    this.rc_img_link = '';
    this.rc_img_file = null;
  }
  getBqaRichContent() {
    const respCode = this.answerGroup.get('respCode').value;

    if (respCode === null || respCode === undefined) {
      return;
    }
    this.basicQAService.getBqaRichContent(respCode).subscribe(result => {
          // 해당 Div 적용 및 데이터 적용
          this.cm.devLog(result.data);
          if (result.data && result.data.contentTypeCd) {
            const type = result.data.contentTypeCd;
            if (type === this.c.CONTENT_TYPE.BUTTON) {
              this.rcButtons = result.data.buttons;

            } else if (type === this.c.CONTENT_TYPE.IMAGE) {
              this.rcImage = result.data.image;
              if (this.rcImage) {
                if (this.rcImage.imagePath) {
                  this.rc_img_path = this.fileService.getDownloadPath(this.c.IMG_OPTION.CONTAINER, this.rcImage.imagePath);
                }
                if (this.rcImage.imageText) {
                  this.rc_img_text = this.rcImage.imageText;
                }
                if (this.rcImage.linkUrl) {
                  this.rc_img_link = this.rcImage.linkUrl;
                }
              }
              this.rc_img_file = null;

            } else if (type === this.c.CONTENT_TYPE.CAROUSEL) {
              // TODO
              // this.rcCarousels = result.data.carousels;

            } else if (type === this.c.CONTENT_TYPE.API) {
              this.getBqaApiList();
              this.apiIfId = result.data.ifId;
            }
          } else {
            this.cm.devLog('No Rich Content');
          }
        }, err => {
          console.log(err);
          this.commonModalService.openErrorAlert('Q&A RICH 가져오기 실패');
        }
    );
  }

  onAddSubmit(form: FormGroup) {

    if (this.domainId === 0) {
      this.commonModalService.openErrorAlert('도메인을 선택하세요');
      return;
    }
    this.basicQAService.createQna(form.value).subscribe(res => {

          if (res.code === 0) {
            this.commonModalService.openSuccessAlert('Q&A 생성 성공');
          } else {
            this.commonModalService.openErrorAlert('Q&A 생성 실패');
          }
        }, error => {
          console.error(error);
        },
        () => {
          this.addRemove.emit(true);
        }
    );
  }

  deleteRow(i) {
    this.answerList.removeAt(i);
    if (this.answerList.value.length === 0) {
      this.answerList = this.addForm.get('answerList') as FormArray;
      this.answerList.push(this.createAnswerItem());
    }
  }

  isUseRcFlag() {
    console.log(this.answerGroup.get('useRcFlag').value);
    return this.answerGroup.get('useRcFlag').value;
  }

  initIsRcs() {
    this._isButton = false;
    this._isImage = false;
    this._isCarousel = false;
    this._isApi = false;
  }
  isButton() {
    const contentTypeCd = this.getContentTypeCd();
    this._isButton = contentTypeCd === this.c.CONTENT_TYPE.BUTTON;
    return this._isButton;
  }
  isImage() {
    const contentTypeCd = this.getContentTypeCd();
    this._isImage = contentTypeCd === this.c.CONTENT_TYPE.IMAGE;
    return this._isImage;
  }
  isCarousel() {
    const contentTypeCd = this.getContentTypeCd();
    this._isCarousel = contentTypeCd === this.c.CONTENT_TYPE.CAROUSEL;
    return this._isCarousel;
  }
  isApi() {
    const contentTypeCd = this.getContentTypeCd();
    this._isApi = contentTypeCd === this.c.CONTENT_TYPE.API;
    return this._isApi;
  }
  isNone() {
    const contentTypeCd = this.getContentTypeCd();
    return contentTypeCd === null || contentTypeCd === undefined || contentTypeCd === this.c.CONTENT_TYPE.NONE;
  }
  isOrigin(i) {
    const contentTypeCd = this.getContentTypeCd();
    const originContentTypeCd = this.getOriginContentTypeCd();
    return contentTypeCd === originContentTypeCd;
  }

  getContentTypeCd() {

    return this.answerGroup.get('contentTypeCd').value;
  }

  getOriginContentTypeCd() {
    return this.originContentTypeCds;
  }
  setOriginContentTypeCd(value) {
    this.originContentTypeCds = value;
  }

  upsertBqaRc() {
    // const questionId = this.addForm.get('questionId').value;
    // const answerId = this.answerGroup.get('answerId').value;
    let respCode = this.answerGroup.get('respCode').value;
    let qaRichVo = this.answerGroup.get('qaRichVo').value;
    const contentTypeCd = this.answerGroup.get('contentTypeCd').value;
    const originContentTypeCd = this.getOriginContentTypeCd();


    if (contentTypeCd === null) {
      this.commonModalService.openErrorAlert('RICH 타입을 설정하세요');
      return;
    }
    if (respCode === null || respCode === undefined) {
      // 임시 respCode 생성
      respCode = 'BQA_RC_TEMP_' + this.domainId;
    }

    if (originContentTypeCd !== contentTypeCd) {

      if (respCode !== null && originContentTypeCd !== null && originContentTypeCd !== this.c.CONTENT_TYPE.NONE) {

        let msg = '이전 설정(_type_)이 삭제됩니다. 계속하시겠습니까?';
        msg = msg.replace('_type_', this.fnGetTypeStr(originContentTypeCd));
        if (!confirm(msg)) {
          return;
        } else {
          this._deleteInsertBqaRichContent(respCode, contentTypeCd, originContentTypeCd);
        }
      } else {
        // 최초 생성
        this._insertBqaRichContent(respCode, contentTypeCd);
      }

      if (qaRichVo === null) {
        qaRichVo = { respCode: respCode, contentTypeCd: contentTypeCd };
        this.answerGroup.get('qaRichVo').setValue(qaRichVo);
      } else {
        this.answerGroup.get('qaRichVo').value.respCode = respCode;
        this.answerGroup.get('qaRichVo').value.contentTypeCd = contentTypeCd;
      }
    }
  }

  _deleteInsertBqaRichContent(respCode, contentTypeCd, originContentTypeCd) {
    // Rc제거 후 추가
    this.basicQAService.deleteBqaRichContent(respCode, originContentTypeCd).subscribe(res => {

          this.cm.devLog(res);
          if (res.code === 0) {
            this.cm.devLog(res.data);
            this.answerGroup.get('respCode').setValue(null);
            this.answerGroup.get('qaRichVo').setValue(null);
            this.rcSetNull();
            this.commonModalService.openSuccessAlert('Q&A RICH 삭제 성공');

            if (contentTypeCd !== this.c.CONTENT_TYPE.NONE) {
              this._insertBqaRichContent(respCode, contentTypeCd);
            } else {
              this.setOriginContentTypeCd(contentTypeCd);
            }

          } else {
            this.commonModalService.openErrorAlert('해당 컨텐츠가 존재하지 않습니다.');
          }
        }, error => {
          console.log(error);
          this.commonModalService.openErrorAlert('Q&A RICH 삭제 실패');
        }
    );
  }

  _insertBqaRichContent(respCode, contentTypeCd) {
    // Rc를 추가한다
    this.basicQAService.insertBqaRichContent(respCode, contentTypeCd, this.userId).subscribe(res => {

          this.cm.devLog(res);
          if (res.code === 0) {
            // respCode 설정
            this.answerGroup.get('respCode').setValue(respCode);
            this.setOriginContentTypeCd(contentTypeCd);
            this.commonModalService.openSuccessAlert('Q&A RICH 추가 성공');

            if (contentTypeCd !== 'RT9001') {
              this.getBqaRichContent();
            } else {
              this.getBqaApiList();
            }

          } else {
            this.commonModalService.openErrorAlert('Q&A RICH 추가 실패');
          }
        }, error => {
          console.log(error);
          this.commonModalService.openErrorAlert('Q&A RICH 추가 실패');
        }
    );
  }

  fnGetTypeStr(contentTypeCd) {
    let typeStr = '';
    if (contentTypeCd === this.c.CONTENT_TYPE.BUTTON) {
      typeStr = 'BUTTON';
    } else if (contentTypeCd === this.c.CONTENT_TYPE.IMAGE) {
      typeStr = 'IMAGE';
    } else if (contentTypeCd === this.c.CONTENT_TYPE.CAROUSEL) {
      typeStr = 'CAROUSEL';
    } else if (contentTypeCd === this.c.CONTENT_TYPE.API) {
      typeStr = 'API';
    } else if (contentTypeCd === this.c.CONTENT_TYPE.NONE) {
      typeStr = 'NONE';
    }
    return typeStr;
  }

  /*BUTTON 관련 함수*/
  get rcButtons() {
    return this._rcButtons;
  }
  set rcButtons(v) {
    this._rcButtons = v;
  }

  enterRcButton($event, i) {
    if (!this.cm.isEnter($event)) {
      return false;
    }
    this.addRcButton(i);
  }
  addRcButton(i) {
    const respCode = this.answerGroup.get('respCode').value;
    // @ts-ignore
    const title = document.getElementById('rcButtonTitle' + i).value;
    // @ts-ignore
    const utter = document.getElementById('rcButtonContent' + i).value;
    const btnOrder = 0;   // 추가일시에는 0

    const data = {
      buttonTitle: title,
      buttonUserUtter: utter
    };
    const validator = {
      buttonTitle: {
        required: true,
        maxLength: 64
      },
      buttonUserUtter: {
        required: true,
        maxLength: 64
      }
    };
    if (this.validator.isInvalid(data, validator)) {
      return false;
    }

    this._upsertBtn(i, respCode, btnOrder, title, utter, '추가');

    // @ts-ignore
    document.getElementById('rcButtonTitle' + i).value = '';
    // @ts-ignore
    document.getElementById('rcButtonContent' + i).value = '';
  }
  updateRcButton(i, item) {
    const respCode = this.answerGroup.get('respCode').value;
    const btnOrder = item.btnOrder;

    // @ts-ignore
    const title = document.getElementById('buttonTitle' + i + btnOrder).value;
    // @ts-ignore
    const utter = document.getElementById('buttonUtter' + i + btnOrder).value;

    const data = {
      buttonTitle: title,
      buttonUserUtter: utter
    };
    const validator = {
      buttonTitle: {
        required: true,
        maxLength: 64
      },
      buttonUserUtter: {
        required: true,
        maxLength: 64
      }
    };

    if (this.validator.isInvalid(data, validator)) {
      return false;
    }

    this._upsertBtn(i, respCode, btnOrder, title, utter, '수정');
  }
  removeRcButton(i, item) {
    const respCode = this.answerGroup.get('respCode').value;
    const btnOrder = item.btnOrder;

    this._removeBtn(i, respCode, btnOrder);
  }

  _upsertBtn(i, respCode, btnOrder, title, utter, type) {
    this.basicQAService.addButton(respCode, btnOrder, title, utter, this.userId).subscribe(res => {

          this.cm.devLog(res);
          if (res.code === 0) {
            // this.commonModalService.openSuccessAlert('Q&A RICH 버튼 ' + type + ' 성공');

            // RichContent목록 가져오기
            this.getBqaRichContent();
          } else {
            this.commonModalService.openErrorAlert('Q&A RICH 버튼 ' + type + ' 실패');
          }
        }, error => {
          console.log(error);
          this.commonModalService.openErrorAlert('Q&A RICH 버튼 ' + type + ' 실패');
        },
    );
  }
  _removeBtn(i, respCode, btnOrder) {
    if (respCode === null || btnOrder < 1) {
      this.commonModalService.openErrorAlert('입력값이 잘못되었습니다.');
      return;
    }
    this.basicQAService.delButton(respCode, btnOrder).subscribe(res => {

          this.cm.devLog(res);
          if (res.code === 0) {
            // this.commonModalService.openSuccessAlert('Q&A RICH 버튼 삭제 성공');
            // RichContent목록 가져오기
            this.getBqaRichContent();
          } else {
            this.commonModalService.openErrorAlert('Q&A RICH 버튼 삭제 실패');
          }
        }, error => {
          console.log(error);
          this.commonModalService.openErrorAlert('Q&A RICH 버튼 삭제 실패');
        },
    );
  }
  /*BUTTON 관련 함수*/

  /*IMAGE 관련 함수*/
  get rcImage() {
    return this._rcImage;
  }
  set rcImage(v) {
    this._rcImage = v;
  }
  get rc_img_path(): string {
    return this._rc_img_path;
  }
  set rc_img_path(v) {
    this._rc_img_path = v;
  }
  get rc_img_text(): string {
    return this._rc_img_text;
  }
  set rc_img_text(v) {
    this._rc_img_text = v;
  }
  get rc_img_link(): string {
    return this._rc_img_link;
  }
  set rc_img_link(v) {
    this._rc_img_link = v;
  }
  get rc_img_file() {
    return this._rc_img_file;
  }
  set rc_img_file(file) {
    this.cm.devLog('set file');
    this._rc_img_file = file;
  }

  onFileChange() {
    const files: FileList = this.rc_img_input.nativeElement.files;
    if (files.length < 1) {
      this.cm.devLog('no file');
      this.rc_img_path = this.c.IMG_OPTION.DEFAULT_IMG;
      this.rc_img_file = null;
      return;
    }
    // For Preview
    const file = files[0];
    const reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onload = () => {
      this.rc_img_path = reader.result;
      this.rc_img_file = file;
      this.cm.devLog('read file');
    };
  }

  addRcImage(i) {
    const respCode = this.answerGroup.get('respCode').value;
    // @ts-ignore
    const imgText = document.getElementById('imageText' + i).value;
    // @ts-ignore
    const imgLink = document.getElementById('imageLink' + i).value;
    const userId = this.userId;
    const file = this.rc_img_file;

    this.basicQAService.addImage(respCode, imgText, imgLink, userId, file).subscribe(res => {
      this.cm.devLog(res);
        if (res.code === 0) {
          this.commonModalService.openSuccessAlert('Q&A RICH - IMAGE 수정 성공');
        } else {
          this.commonModalService.openErrorAlert('Q&A RICH - IMAGE 수정 실패');
        }
      }, error => {
        console.log(error);
        this.commonModalService.openErrorAlert('Q&A RICH - IMAGE 수정 실패');
      }
    );
  }
  /*IMAGE 관련 함수*/

  /*APIS 관련 함수*/
  get rcApiList() {
    return this._rcApiList;
  }
  set rcApiList(v) {
    this._rcApiList = v;
  }

  getBqaApiList() {
    this.basicQAService.getBqaApiList(this.workspaceId).subscribe(res => {

          this.cm.devLog(res);
          if (res.code === 0) {
            if (res && res.data && res.data.list) {
              for (let i = 0; i < res.data.list.length; i++) {
                this.rcApiList = res.data.list;
              }
            }

          } else {
            this.commonModalService.openErrorAlert('Q&A RICH - API 가져오기 실패');
          }
        }, error => {
          console.log(error);
          this.commonModalService.openErrorAlert('Q&A RICH - API 가져오기 실패');
        },
    );
  }

  get apiIfId() {
    return this._apiIfId;
  }
  set apiIfId(v) {
    this._apiIfId = v;
  }
  setApiIfId($event, i) {
    this._apiIfId = $event.target.value;
  }
  setBqaApi(i) {
    const respCode = this.answerGroup.get('respCode').value;
    const rcId = this.apiIfId;

    this.basicQAService.setBqaApi(respCode, rcId, this.userId).subscribe(res => {

          this.cm.devLog(res);
          if (res.code === 0) {
            this.commonModalService.openSuccessAlert('Q&A RICH - API 수정 성공');
          } else {
            this.commonModalService.openErrorAlert('Q&A RICH - API 수정 실패');
          }
        }, error => {
          console.log(error);
          this.commonModalService.openErrorAlert('Q&A RICH - API 수정 실패');
        },
    );
  }
  /*APIS 관련 함수*/
}
