import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, Validators, FormBuilder, FormControl } from '@angular/forms';
import { BasicQAService } from '../../../../../service/bqa.service';
import { Message } from 'primeng/components/common/api';
import { Cm_domainApi } from '../../../../../adminsdk/services/custom/Cm_domain';
import { Cm_domain } from '../../../../../adminsdk/models/Cm_domain';
import { CommonModalService } from '../../../../../service/common-modal.service';
import {UserService} from '../../../../../service/user.service';


@Component({
  selector: 'app-bqa-domain-edit',
  templateUrl: './bqa-domain-edit.component.html',
  styleUrls: ['./bqa-domain-edit.component.scss']
})
export class BqaDomainEditComponent implements OnInit {

  editForm: FormGroup;
  msgs: Message[];
  domain: Cm_domain;

  @Input() editFlag:Boolean = false;

  @Output() editRemove = new EventEmitter<Boolean>();
  
  constructor(
    private fb: FormBuilder,
    private cmDomainApi: Cm_domainApi,
    private commonModalService: CommonModalService,
    private userSerivce: UserService
  ) { 

    this.domain = new Cm_domain();
  }

  ngOnInit() {
    
    this.domain.depth = 1;
    this.domain.domain_id = 0;
    this.domain.parent_domain_id = -1;
  }

  public domainEditOpen(e) {
    
    this.editForm = this.fb.group({
      domain_name: ['', [Validators.required]],
      descript: ['', [Validators.required]],
      depth: new FormControl({value: '', disabled: true}),
     });

    
    if(e){
      
      this.domain.domain_id = e.domain_id;
      this.domain.team_id = Number(this.userSerivce.teamId);
      this.domain.domain_name = e.domain_name;
      this.domain.descript = e.descript;
      this.domain.depth = e.depth;
      this.domain.parent_domain_id = e.parent_domain_id;
    }

  }

  onEditSubmit() {
    this.cmDomainApi.upsert(this.domain).subscribe(res =>{ 
      this.commonModalService.openSuccessAlert('도메인 수정 성공');
    }, error => {
      console.log(error);
      this.commonModalService.openSuccessAlert('도메인 수정 실패');
    },() => {
        this.editRemove.emit(true);
      }
    );
  }
}
