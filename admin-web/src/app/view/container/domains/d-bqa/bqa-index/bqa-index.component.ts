import { Qa_indexing_historyApi } from '../../../../../adminsdk/services/custom/Qa_indexing_history';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { BasicQAService } from '../../../../../service/bqa.service';
import { Message } from 'primeng/components/common/api';
import { Paginator } from '../../../../../common/paginator/paginator.component';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { CommonUtilService } from '../../../../../service/commonutil.service';
import { DataService } from '../../../../../service/data.service';


@Component({
  selector: 'app-bqa-index',
  templateUrl: './bqa-index.component.html',
  styleUrls: ['./bqa-index.component.scss']
})
export class BqaIndexComponent implements OnInit, OnDestroy {
  pgSize = 5;
  progress_value = 0;
  msgs: Message[];
  paginator = new Paginator();
  items;

  indexingType = [
    {
      value: 'F',
      desc: '전체',
    },
    {
      value: 'A',
      desc: '추가',
    },
    {
      value: 'D',
      desc: '도메인',
    }
  ];

  constructor(
    private basicQAService: BasicQAService,
    private qa_indexing_historyApi: Qa_indexing_historyApi,
    private cm: CommonUtilService,
    private ds: DataService
  ) { }

  ngOnInit() {

    this.paginator.pageSize = this.pgSize;
    this.paginator.currentPage = 1;
    this.getList(1);
  }
  ngOnDestroy() {
    this.ds.clearData();
  }

  private _isWorking: boolean = false;
  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if(this._isWorking == v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }

  fullIndexing() {

    this.isWorking = true;
    this.basicQAService.fullIndexing().subscribe(res => {
      console.log(res);
      this.progress_value = 100;
      setTimeout(() => {
        this.progress_value = 0;
      }, 2000);
      this.msgs = [{ severity: 'success', summary: 'Success', detail: 'Full Indexing complete' }];
      this.isWorking = false;
    },err=>{
      this.isWorking = false;
      console.log(err);
    },()=>{
      this.getList(1);
    });
  }

  addIndexing() {

    this.isWorking = true;
    this.basicQAService.addIndexing().subscribe(res => {
      this.progress_value = 100;

      setTimeout(() => {
        this.progress_value = 0;
      }, 2000);
      this.msgs = [{ severity: 'success', summary: 'Success', detail: 'Add Indexing complete' }];
      this.isWorking = false;
    },err=>{
      this.isWorking = false;
      console.log(err);
    },()=>{
      this.getList(1);
    });
  }

  getList(pg) {
    this.paginator.currentPage = pg;

    const filter: LoopBackFilter = {

      skip: (pg - 1) * this.paginator.pageSize,
      limit: this.paginator.pageSize,
      order: `history_id desc`
    };
    this.isWorking = true;
    this.qa_indexing_historyApi.count().subscribe(res => {
      if (res.count <= 0) {
        this.isWorking = false;
      } else {
        this.paginator.totalCount = res.count;

        this.qa_indexing_historyApi.find(filter).subscribe(res => {
          this.items = res;
          this.isWorking = false;
        });
      }

    }, error => {
      this.isWorking = false;
      console.error(error);
    }, () => {
    }
    );
  }

  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    if(tempP == JSON.stringify(this.paginator)
      && this.paginator.totalPages != 1
    ){
      return true;
    }
    if(this.paginator.totalCount <= 0) {
      return true;
    }
    this.getList(this.paginator.currentPage);
  }
}
