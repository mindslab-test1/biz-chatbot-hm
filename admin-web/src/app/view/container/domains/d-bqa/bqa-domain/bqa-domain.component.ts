import { element } from 'protractor';
import { Cm_domain } from '../../../../../adminsdk/models/Cm_domain';
import { Component, OnInit, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { Cm_domainApi } from '../../../../../adminsdk/services/custom/Cm_domain';
import { TreeNode, Message } from 'primeng/components/common/api';
import { BqaListComponent } from '../bqa-list/bqa-list.component';
import { BqaDomainAddComponent } from '../bqa-domain-add/bqa-domain-add.component';
import { BqaDomainEditComponent } from '../bqa-domain-edit/bqa-domain-edit.component';
import { UserService } from '../../../../../service/user.service';
import { BasicQAService } from '../../../../../service/bqa.service';
import { DataService } from '../../../../../service/data.service';

@Component({
  selector: 'app-bqa-domain',
  templateUrl: './bqa-domain.component.html',
  styleUrls: ['./bqa-domain.component.scss']
})
export class BqaDomainComponent implements OnInit, OnDestroy {
  filesTree: TreeNode[];
  addFlag:Boolean = false;
  team_id;
  array = [];
  editFlag:Boolean = false;
  @ViewChild('addChild') addChild:BqaDomainAddComponent;
  @ViewChild('editChild') editChild:BqaDomainEditComponent;
  constructor(
    private cmDomainApi: Cm_domainApi,
    private ds: DataService,
    private userSerivce: UserService,
    private basicQAService: BasicQAService,
  ) { }

  ngOnInit() {
    this.team_id = this.userSerivce.getTeamId();
    this.getList();
  }
  ngOnDestroy() {
    this.ds.clearData();
  }

  private _isWorking: boolean = false;
  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if(this._isWorking == v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }

  getList() {
    const filter: LoopBackFilter = {
      
      where: { depth: 1 ,team_id: this.team_id , domain_id: {neq: 0}},
      include: "children"
    };

    this.isWorking = true;
    this.cmDomainApi.count(filter.where).subscribe(res => {
      
      if (res.count <= 0) {
        this.isWorking = false;
      }else{
        this.cmDomainApi.find(filter).subscribe(res =>{
          this.filesTree = res;
          this.isWorking = false;
        }, subErr=>{
          this.isWorking = false;
          console.log(subErr);
        });
      }
    }, error => {
      this.isWorking = false;
      console.log(error);
    }, () =>{
    });
  }
  editModalOpen(e){
    this.editChild.domainEditOpen(e);
    this.editFlag = true;
  }
  editModalClose(event){
    if(event){
      this.getList();
    }
    
    this.editFlag = false;
  }
  count = 0;
  removeChildren(entity){
    this.count++;
    this.cmDomainApi.deleteById(entity.domain_id).subscribe(res => {
      
    }, error => {
      console.error(error);
    }, () =>{
      this.array.push(entity.domain_id);
      
      if(entity.children != null && entity.children != 'undefined'){
        for(let x = 0; x < entity.children.length ; x++){
          this.removeChildren(entity.children[x]);
        }
      }
      this.count--;
      if(this.count == 0){
        
        this.basicQAService.deleteQnaDomainId(this.array).subscribe(res => console.log("deleteQnaDomainId",res));
      }
    });
    
  }

  remove(entity){
    if (confirm("정말 삭제하시겠습니까?")) {
      this.array = [];
      this.removeChildren(entity);
      this.getList();
    }
  }
  addModalOpen(e){
    this.addChild.domainAddOpen(e);
    this.addFlag = true;
  }

  addModalClose(event){
    if(event){
      this.getList();
    }
    
    this.addFlag = false;
  }

}
