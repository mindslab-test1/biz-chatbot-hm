import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BasicQAService, QaExcelVo, RicahExcelVo} from '../../../../../service/bqa.service';
import {BqaAddComponent} from '../bqa-add/bqa-add.component';
import {BqaEditComponent} from '../bqa-edit/bqa-edit.component';
import {Message} from 'primeng/components/common/api';
import {UtilService} from '../../../../../service/util.service';
import {UserService} from '../../../../../service/user.service';
import {CommonUtilService} from '../../../../../service/commonutil.service';
import {Paginator} from '../../../../../common/paginator/paginator.component';
import {CommonModalService} from '../../../../../service';
import {DataService} from '../../../../../service/data.service';
import {lang, Msg} from '../../../../../model/message';

@Component({
  selector: 'app-bqa-list',
  templateUrl: './bqa-list.component.html',
  styleUrls: ['./bqa-list.component.scss']
})
export class BqaListComponent implements OnInit, OnDestroy {
  items;
  pgSize = 10;
  addFlag: Boolean = false;
  editFlag: Boolean = false;
  msgs: Message[];
  domainId = 0;
  domainIds = [];
  userId;
  paginator = new Paginator();
  selectPageSize: string = this.paginator.DEFAULT_PAGE_SIZE.toString();
  type = 0;
  selectOption = 1;
  searchText = '';
  optionList = [{label: '질문', value: '1'}, {label: '응답', value: '2'}];

  @ViewChild('addChild') addChild: BqaAddComponent;
  @ViewChild('editChild') editChild: BqaEditComponent;

  private _isWorking = false;
  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if (this._isWorking === v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }

  constructor(
      private basicQAService: BasicQAService,
      private utilService: UtilService,
      private userSerivce: UserService,
      private commonModalService: CommonModalService,
      private cm: CommonUtilService,
      private ds: DataService
  ) {
    this.searchText = '';
    this.selectOption = 1;
    this.paginator.pageSize = this.getCurrPageSize();
    this.paginator.currentPage = 1;
    //   this.getList(this.paginator.currentPage, this.pgSize, this.domainId);
    this.userId = this.userSerivce.getCurrentUserId();

  }

  ngOnInit() {
    // this.domainIds.push(0);

  }
  ngOnDestroy() {
    this.ds.clearData();
  }

  addModalClose(event) {
    if (event) {
      this.getList(this.paginator.currentPage, this.pgSize, this.domainId);
    }

    this.addFlag = false;
  }
  editModalClose(event) {
    if (event) {
      this.getList(this.paginator.currentPage, this.pgSize, this.domainId);
    }

    this.editFlag = false;
  }
  domainIndexing(domainId) {
    if (this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }

    if (domainId === 0) {
      this.commonModalService.openErrorAlert('도메인을 선택해주세요');
    } else {
      this.isWorking = true;
      this.basicQAService.domainIndexing(domainId).subscribe(res => {
            this.isWorking = false;
            this.commonModalService.openSuccessAlert(res.msg);
          }, err => {
            this.isWorking = false;
          }, () => {
          }
      );
    }

  }
  addModalOpen(domainId) {
    this.addChild.qnaAddOpen(domainId);
    this.addFlag = true;
  }

  editModalOpen(entity) {
    this.editChild.qnaUpdateOpen(entity);
    this.editFlag = true;
  }
  getList(pg, pageSize, domainId) {
    this.domainId = domainId;
    let type = 0;
    let search = '';
    if (this.searchText == null || this.searchText.length === 0 || this.searchText === undefined) {

    } else {
      type = this.type;
      search = this.searchText;
    }

    this.isWorking = true;
    this.basicQAService.gets(pg, pageSize, this.domainIds,  domainId, type, search).subscribe(res => {
        console.log(res);

      this.paginator.currentPage = pg;
      this.items = res.data.list;
      this.paginator.totalCount = res.data.totalCount;
      this.isWorking = false;
    }, err => {
      this.isWorking = false;
    });
  }

  public removeOpen(entity) {
    if (confirm('정말 삭제하시겠습니까?')) {

        console.log(entity);
      this.basicQAService.deleteQna(entity).subscribe(res => {

            if (res.code === 0) {
              this.msgs = [{ severity: 'success', summary: 'Success', detail: 'Delete Q&A complete' }];
            } else {
              this.msgs = [{ severity: 'fail', summary: 'Fail', detail: res.msg }];
            }
          }, error => {
            console.log(error);
            this.msgs = [{ severity: 'fail', summary: 'Fail', detail: 'Delete Q&A Fail' }];
          },
          () => {
            this.getList(this.paginator.currentPage, this.pgSize, this.domainId);
          }
      );
    }
  }

  private _finishWorking = () => {
    this.commonModalService.closeAlertDialog();
    this.isWorking = false;
  };



  public uploadExcel(e: any) {
    if (!e.srcElement.value) {
      return false;
    }

    if (confirm('Are you sure you want to upload the file?')) {
      this.commonModalService.openCommonDialogOnlyMsg('엑셀 업로드 중...', '엑셀 업로드 중...');
      this.isWorking = true;
      this.utilService.importExcel(e).subscribe(items => {

        const array = new Array<QaExcelVo>();

        // 첫번째 줄 넘어감
        for (let i = 1 ; i < items.length ; i++) {

          if (items[i][1] !== undefined) {
            const qaExcelVo = new QaExcelVo();
            qaExcelVo.domainId = this.domainId;
            qaExcelVo.questionId = items[i][0];
            qaExcelVo.question = this.cm.replaceAll(items[i][1], '\r\r', '\r');
            qaExcelVo.attribute1 = items[i][2];
            qaExcelVo.attribute2 = items[i][3];
            qaExcelVo.answerId = items[i][4];
            qaExcelVo.answer = this.cm.replaceAll(items[i][5], '\r\r', '\r');
            qaExcelVo.userId = this.userId;
            array.push(qaExcelVo);
          }
        }


        this.basicQAService.insertExcel(array).subscribe(res => {
              this._finishWorking();
              this.commonModalService.openSuccessAlert(res.msg + ': (' + res.data + ' data)');
            }, err => {
              this._finishWorking();

            }, () => {
              this.getList(this.paginator.currentPage, this.pgSize, this.domainId);
            }
        );
      }, err => {
        this._finishWorking();
      });
    }
  }


  public uploadRichExcel(e: any) {
    if (!e.srcElement.value) {
      return false;
    }

    if (confirm('Are you sure you want to upload the file?')) {
      this.commonModalService.openCommonDialogOnlyMsg('엑셀 업로드 중...', '엑셀 업로드 중...');
      this.isWorking = true;
      const data = new FormData();

      // data.append('domainId', this.domainId + '');
      data.append('userId',  this.userId);
      data.append('file', e.target.files[0]);

      this.basicQAService.insertBqaRichExcelfile(data).subscribe(res => {
         this._finishWorking();
         this.commonModalService.openSuccessAlert(res.msg + ': (' + res.data + ' data)');
         }, err => {this._finishWorking(); }, () => {
          this.getList(this.paginator.currentPage, this.pgSize, this.domainId);
      });

    }
  }

  /**
   * multi-part 안쓴버전..
   * 혹시몰라 안지움...
   * 테스트필요..
   */
 /* public uploadRichExcel(e: any) {
    if (!e.srcElement.value) {
      return false;
    }

    if (confirm('Are you sure you want to upload the file?')) {
      this.commonModalService.openCommonDialogOnlyMsg('엑셀 업로드 중...', '엑셀 업로드 중...');
      this.isWorking = true;
      this.utilService.importExcel(e).subscribe(items => {

        const array = new Array<RicahExcelVo>();

        // 첫번째 줄 넘어감
        for (let i = 1 ; i < items.length ; i++) {

          if (items[i][1] !== undefined) {
            const qaExcelVo = new RicahExcelVo();
            qaExcelVo.userId = this.userId;
            qaExcelVo.questionId = items[i][0];
            qaExcelVo.btnOrder = items[i][1];
            qaExcelVo.type =  items[i][2];
            qaExcelVo.btnTitle = this.cm.replaceAll(items[i][3], '\r\r', '\r');
            qaExcelVo.btnUtter = this.cm.replaceAll(items[i][4], '\r\r', '\r');
            qaExcelVo.imgText = this.cm.replaceAll(items[i][5], '\r\r', '\r');
            qaExcelVo.imgLinkUrl = items[i][6];
            qaExcelVo.apiId = items[i][7];
            array.push(qaExcelVo);
          }
        }

        this.basicQAService.insertBqaRichExcelfile(array).subscribe(res => {
              this._finishWorking();
              this.commonModalService.openSuccessAlert(res.msg + ': (' + res.data + ' data)');
            }, err => {
              this._finishWorking();

            }, () => {
              this.getList(this.paginator.currentPage, this.pgSize, this.domainId);
            }
        );
      }, err => {
        this._finishWorking();
      });
    }
  }*/


  public downloadExcel() {
    const sheetData: any[] = [];
    let items;
    if (this.domainId === 0) {
      this.commonModalService.openErrorAlert('도메인을 선택해 주세요');
      return;
    }


    this.commonModalService.openCommonDialogOnlyMsg('엑셀 다운로드 중...', '엑셀 다운로드 중...');
    this.isWorking = true;
    this.basicQAService.getsExcel(this.domainId).subscribe(res => {
      items = res.data.list;
    }, error => {
      this._finishWorking();
      console.error(error);
    }, () => {
      if (items.length > 0) {
        // sheetData.push(['ID', 'Question', 'ID', 'ANSWER']);
        sheetData.push(['ID', 'Question', 'Attribute1', 'Attribute2', 'ID', 'ANSWER']);
        for (const item of items) {
          // sheetData.push([item.questionId, item.question, item.questionId, item.answer]);
          sheetData.push([item.questionId, item.question, item.attribute1, item.attribute2, item.questionId, item.answer]);
        }
        this.utilService.exportExcel('QuestionList', sheetData);
      }
      this._finishWorking();
    });
  }

  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    if (tempP === JSON.stringify(this.paginator)
        && this.paginator.totalPages !== 1
    ) {
      // 갱신된 값이 없으면 다시 그리지 않음.
      return true;
    }
    if (this.paginator.totalCount <= 0) {
      return true;
    }
    this.getList(this.paginator.currentPage, this.paginator.pageSize, this.domainId);
  }
  search(type, value) {
    if (this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }

    if (value.length === 1) {
      this.commonModalService.openErrorAlert('검색어를 입력 해주세요(두자 이상)');
      return;
    }
    this.type = type;
    this.searchText = value;
    this.getList(this.paginator.currentPage, this.pgSize, this.domainId);
  }

  onChangePageSize(data) {
    this.selectPageSize = data.selectOption;
    // this.initList();
    if (this.paginator.pageSize !== this.getCurrPageSize()) {
      this.setPageSize(this.getCurrPageSize());
      this.paginator.currentPage = 1;
    }
    this.getList(this.paginator.currentPage, this.pgSize, this.domainId);
  }
  getCurrPageSize() {
    return Number(this.selectPageSize);
  }
  setPageSize(pageSize: number) {
    this.paginator.pageSize = pageSize;
    this.pgSize = pageSize;
  }
}

