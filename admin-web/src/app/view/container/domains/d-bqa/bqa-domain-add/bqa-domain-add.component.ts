import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, Validators, FormBuilder, FormControl } from '@angular/forms';
import { BasicQAService } from '../../../../../service/bqa.service';
import { Message } from 'primeng/components/common/api';
import { Cm_domainApi } from '../../../../../adminsdk/services/custom/Cm_domain';
import { Cm_domain } from '../../../../../adminsdk/models/Cm_domain';
import { UserService } from '../../../../../service/user.service';
import { CommonModalService } from '../../../../../service/common-modal.service';

@Component({
  selector: 'app-bqa-domain-add',
  templateUrl: './bqa-domain-add.component.html',
  styleUrls: ['./bqa-domain-add.component.scss']
})
export class BqaDomainAddComponent implements OnInit {
  addForm: FormGroup;
  
  domain: Cm_domain;
  @Input() addFlag:Boolean = false;
  @Output() addRemove = new EventEmitter<Boolean>();
  team_id;
  constructor(
    private fb: FormBuilder,
    private cmDomainApi: Cm_domainApi,
    private commonModalService: CommonModalService,
    private userSerivce: UserService
    
  ) { 

    this.domain = new Cm_domain();
    this.team_id = this.userSerivce.getTeamId();
  }

  ngOnInit() {

  }

  public domainAddOpen(e) {
    
    this.addForm = this.fb.group({
      domain_name: ['', [Validators.required]],
      descript: ['', [Validators.required]],
      depth: new FormControl({value: e.depth+1, disabled: true}),
     });
     this.domain.domain_id = 0;
     this.domain.domain_name = '';
     this.domain.descript = '';
     this.domain.team_id = this.team_id;
    if(e){
      this.domain.depth = e.depth+1;
      this.domain.parent_domain_id = e.domain_id;
    }else{
      this.domain.depth = 1;
      this.domain.parent_domain_id = -1;
    }
  }

  onAddSubmit() {
    
    this.cmDomainApi.create(this.domain).subscribe(res =>{ 
      this.commonModalService.openSuccessAlert('도메인 생성 성공');
    }, error => {
      this.commonModalService.openSuccessAlert('도메인 생성 성공');
    },() => {
        this.addRemove.emit(true);
      }
    );
  }
  
}
