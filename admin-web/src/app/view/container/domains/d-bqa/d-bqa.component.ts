import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { BqaListComponent } from './bqa-list/bqa-list.component';
import { MenuItem } from 'primeng/primeng';
import {LoopBackFilter, Cm_domainApi, Cm_domain, Cm_workspace_team, Cm_workspace_teamApi} from '../../../../adminsdk';
import { UserService } from '../../../../service/user.service';

@Component({
  selector: 'app-d-bqa',
  templateUrl: './d-bqa.component.html',
  styleUrls: ['./d-bqa.component.scss']
})
export class DBqaComponent implements OnInit {

  items: MenuItem[];
  activeItem: MenuItem;
  select = 'list';
  filesTree: Array<Cm_domain> = [];
  domain_name = 'All';
  domain_id = 0;
  team_id;
  role_id;
  workspaceId;
  teamIds = [];
  @ViewChild('listChild') listChild: BqaListComponent;
  @ViewChild('fileView') fileInput: ElementRef;
  @ViewChild('fileView2') fileInput2: ElementRef;
  constructor(
    private cmDomainApi: Cm_domainApi,
    private userService: UserService,
    private workspaceRelApi: Cm_workspace_teamApi
  ) {
   }

  ngOnInit() {
    this.team_id = this.userService.getTeamId();
    this.role_id = this.userService.getUserRoleId();
    this.items = [
      { label: 'Q&A List', target: 'list', icon: 'fa-list-alt'},
      { label: 'Q&A Index', target: 'index', icon: 'fa-files-o' },
      { label: 'Q&A DOMAIN', target: 'domain', icon: 'fa-language' }
    ];

    // mobis 요청사항
    if (this.role_id !== this.userService.getRoles.ADMIN) {
        this.items.splice(1, 2);
    }

    this.activeItem = this.items[0];
    this.getList();
  }
  menuClick(e) {
    this.select = e.activeItem.target;

    if (e.activeItem.target === 'list') {
    this.getList();
    }
  }
  onTreeSelect(emitData) {
     // console.log('여기');

    if (emitData.data.domain_id !== -999999) {
        this.domain_name = emitData.data.domain_name;
        this.domain_id = emitData.data.domain_id;
        this.listChild.getList(1, 10, emitData.data.domain_id);
    } else {
      this.domain_id = emitData.data.domain_id;
      this.listChild.domainId = emitData.data.domain_id;
    }
  }

  getList() {
      if (this.role_id === this.userService.getRoles.ADMIN) {
          const filter: LoopBackFilter = {
              where: {workspace_id: this.workspaceId}
          };

          this.workspaceRelApi.find(filter).subscribe((res: Cm_workspace_team[]) => {
              if (res && res.length > 0) {
                  for (let i = 0; i < res.length; i++) {
                      this.teamIds.push(res[i].team_id);
                  }
              }
              this.getTreeData();
          });
      } else {
          this.getTreeData();
      }
  }
  getTreeData() {
    const filter: LoopBackFilter = {
      // where: { depth: 1 , team_id: this.team_id },
      include: 'children'
    };

    if (this.teamIds.length > 0) {
      filter.where = { depth: 1 , team_id: {inq: this.teamIds}};
    } else {
      filter.where = { depth: 1 , team_id: this.team_id };
    }

    this.filesTree = [];

    this.cmDomainApi.count(filter.where).subscribe(res => {

      if (res.count <= 0) {
          const rootDomain = new Cm_domain();

          rootDomain.children = [];
          rootDomain.parent_domain_id = -1;
          rootDomain.domain_id = -999999;
          rootDomain.depth  = 1;
          rootDomain.team_id = this.team_id;
          rootDomain.domain_name = '데이터가 없습니다.';

          this.filesTree.push(rootDomain);
          this.domain_id = -999999;
          this.listChild.getList(1, 10, -999999);

      } else {

        this.cmDomainApi.find(filter).subscribe(( res: Cm_domain[]) => {

            /* 최상위 루트를 만들어준다. */
            const rootDomain = new Cm_domain();
            rootDomain.children = [];
            rootDomain.parent_domain_id = -1;
            rootDomain.domain_id = 0;
            rootDomain.depth  = 1;
            rootDomain.team_id = this.team_id;
            rootDomain.domain_name = 'ALL';

            // 최상위 루트부터 집어넣고
            this.filesTree.push(rootDomain);
            // 나머지를 push
            res.map(x => this.filesTree.push(x));
            // domainId 전체를 저장할 배열 선언
            const ids: number[] = [];

            // 모든 도메인 id를 배열에 저장
            res.map(x => ids.push(x.domain_id));

            // 가장 상위 도메인을 저장.. all 이 되겠지..
            this.domain_id = this.filesTree[0].domain_id;

            // ALL 일경우 전체 검색을 위해서 배열을 list comp 에 넘긴다.
            this.listChild.domainIds = ids;

            // bqa 리스트 최초 실행
            this.listChild.getList(1, 10, this.filesTree[0].domain_id);

        });
      }
    }, error => {

    }, () => {
    }
    );
  }
  addModalOpen() {
    this.listChild.addModalOpen(this.domain_id);
  }
  domainIndexing() {
    this.listChild.domainIndexing(this.domain_id);
  }
  uploadExcel(e) {
    this.listChild.uploadExcel(e);
    this.fileInput.nativeElement.value = '';
  }
  uploadRichExcel(e) {
    this.listChild.uploadRichExcel(e);
    this.fileInput2.nativeElement.value = '';
  }

  downloadExcel() {
    this.listChild.downloadExcel();
  }

  /*클릭이벤트 강제발생 */
  openExcelUpload(e) {
    this.fileInput.nativeElement.click();
  }

  /*클릭이벤트 강제발생 */
  openRichExcelUpload(e) {
    this.fileInput2.nativeElement.click();
  }

  /*나중에 필요할까해서 만들어둠*/
  initValue(fileInput) {}
}
