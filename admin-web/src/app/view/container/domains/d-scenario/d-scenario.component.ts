import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { CommonUtilService, CommonModalService } from '../../../../service';
import { DomSanitizer, SafeResourceUrl, SafeHtml } from '@angular/platform-browser';
import { Http } from '@angular/http';
import { Msg, lang } from '../../../../model/message';
import { environment as env } from '../../../../environments/environment';
import { SdsApiHelperService } from '../../../../service/sds-api-helper.service';
import { ValidatorService } from '../../../../service/validator.service';

@Component({
  selector: 'app-d-scenario',
  templateUrl: './d-scenario.component.html',
  styleUrls: ['./d-scenario.component.scss']
})
export class DScenarioComponent implements OnInit, OnDestroy {

  // sdsHost:string = env.serverHost;
  sdsHost = 'localhost';
  sdsPort: number = env.serverPort;
  urlDomainlist: SafeResourceUrl;

  constructor(
    private http: Http,
    private cm: CommonUtilService,
    private sanitizer: DomSanitizer,
    private commonModalService: CommonModalService,
    private sdsApi: SdsApiHelperService,
    private validator: ValidatorService
  ) { }

  sds = this.sdsApi.api;

  ngOnInit() {
    this.init();
  }
  ngOnDestroy() {
    // TODO 로그아웃
    // this.sdsApi.post(this.sds.view_logout, {}).subscribe(res=>{
    //   if(this.validator.isInvalidResponse(res)){
    //     this.commonModalService.openErrorAlert(res.message || Msg.err[lang].failLogout);
    //     return;
    //   }
    //   console.log('logout');
    // }, err=>{
    //   this.commonModalService.openErrorAlert(err);
    // });
  }
  // ngAfterViewInit() {
  //   // this.handleIframe();
  // }
  init() {
    // const url = `${this.sdsApi.url}${this.sds.view_login}`;
    const url = `${this.sdsApi.url}${this.sds.view_login}`;
    this.urlDomainlist = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    this.initIframe();
    // const url = `http://${env.sdsApiUrl}/view/projectlist.do`;
    // this.urlDomainlist = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    // this.sdsAccount(()=>{
    //   this.initIframe();
    // });
  }

  @ViewChild('frameWrapper')
  frameWrapper: ElementRef;
  @ViewChild('frameScenario')
  frameScenario: ElementRef;

  initIframe() {
    requestAnimationFrame(() => {
      this._initIframe();
    });
  }
  _initIframe() {

    const iframe: HTMLIFrameElement = this.frameScenario.nativeElement;
    // this.setUrl();
    iframe.width = '100%';
    iframe.height = '100%';
    iframe.style.overflow = 'auto';

    const iframeWrapper: HTMLDivElement = this.frameWrapper.nativeElement;
    iframeWrapper.style.height = '100%';

    /*
    내부 바디에... 줄 수 있다면 아래속성 주면 1280 에서 딱 맞음.
    transform: scale(0.8,0.8);
    overflow: auto;
    */
  }

  // handleIframe() {
  // }



  // private sdsAccount(callback = ()=>{ this.cm.override(); }) {
  //   let url = 'http://'+this.sdsHost+':'+this.sdsPort+'/sds-api/admin/sds/auth/loginajax';
  //   let body = {
  //     "userId": "admin",
  //     "pass": "green1234~"
  //   };

  //   this.sdsApi.post('/admin/sds/auth/loginajax', body).subscribe(res=>{
  //     requestAnimationFrame(()=>{
  //       callback();
  //     });
  //   },err=>{
  //     this.commonModalService.openErrorAlert(err);
  //   });
  // }
}
