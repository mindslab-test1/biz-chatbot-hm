import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { CommonModalComponent } from '../../../../../../common';
import { Cm_synonymApi } from '../../../../../../adminsdk/services/custom/Cm_synonym';
import { LoopBackFilter } from '../../../../../../adminsdk/models/BaseModels';
import { Cm_synonymInterface, Cm_synonym } from '../../../../../../adminsdk/models/Cm_synonym';
import { CommonModalService } from '../../../../../../service/common-modal.service';

@Component({
  selector: 'app-synonym-remove',
  templateUrl: './synonym-remove.component.html',
  styleUrls: ['./synonym-remove.component.scss']
})
export class SynonymRemoveComponent implements OnInit {


  synonymlist = new Array<string>();
  synonmWord:string;


  valid:boolean = false;

  @Output() removeClose = new EventEmitter<Boolean>();
  @ViewChild('commonModal') commonModal:CommonModalComponent;
  cm_synonym:Cm_synonym;

  constructor(private cmSynonymApi: Cm_synonymApi,
    private commonModalService: CommonModalService
  ) {

    this.cm_synonym = new Cm_synonym();
  }

  ngOnInit() {

  }

  modalOpen(res){
    this.cm_synonym = res;
    this.commonModal.modalOpen();
  }

  modalClose(){

    this.commonModal.modalClose();
  }
  synonymRemoveSave(){
     this.cmSynonymApi.deleteByIds(this.cm_synonym.synonym_id, this.cm_synonym.workspace_id).subscribe(res =>{
      }, error => {
        console.error(error);
      },
        () => {
          this.commonModalService.openSuccessAlert('등록되었습니다');
          this.removeClose.emit(true);
          this.modalClose();
        }
      );

  }
}
