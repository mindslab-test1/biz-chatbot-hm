import {Component, OnInit, ViewChild, OnDestroy, EventEmitter} from '@angular/core';
import { UserService } from '../../../../service/user.service';
import { If_apiApi } from '../../../../adminsdk/services/custom/If_api';
import { LoopBackFilter } from '../../../../adminsdk/models/BaseModels';
import { Router, ActivatedRoute } from '@angular/router';
import { Paginator } from '../../../../common/paginator/paginator.component';
import { CommonUtilService } from '../../../../service/commonutil.service';
import {AlertModalEvent, CommonModalService} from '../../../../service/common-modal.service';
import { DataService } from '../../../../service/data.service';

@Component({
  selector: 'app-api-mgmt',
  templateUrl: './api-mgmt.component.html',
  styleUrls: ['./api-mgmt.component.scss']
})
export class ApiMgmtComponent implements OnInit, OnDestroy {

  items;
  pgSize = 10;
  workspaceId = 0;
  paginator = new Paginator();
  selectPageSize: string = this.paginator.DEFAULT_PAGE_SIZE.toString();

  private _isWorking = false;
  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if (this._isWorking === v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }

  constructor(  private route: ActivatedRoute, private ifApi: If_apiApi, private userService: UserService, private router: Router
      , private ds: DataService
      , private commonModalService: CommonModalService
      , private cm: CommonUtilService) { }

  ngOnInit() {
    this.workspaceId = this.userService.getWorkspaceIds().pop();
    this.paginator.pageSize = this.pgSize;
    this.paginator.currentPage = 1;
    this.getList(1);
  }
  ngOnDestroy() {
    this.ds.clearData();
  }

  getList(pg) {
    this.paginator.currentPage = pg;

    const filter: LoopBackFilter = {
      where: {workspace_id : this.workspaceId},
      skip: (pg - 1) * this.paginator.pageSize,
      limit: this.paginator.pageSize,
      order: `if_id desc`
    };

    this.isWorking = true;
    this.ifApi.count(filter.where).subscribe(res => {

          if (res.count <= 0) {
            this.isWorking = false;
          } else {
            this.paginator.totalCount = res.count;

            this.ifApi.find(filter).subscribe(res => {
              this.items = res;
              this.isWorking = false;
            }, subErr => {
              this.isWorking = false;
            });
          }

        }, err => {
          this.isWorking = false;
          // console.error(err);
        }, () => {
        }
    );
  }
  apiRemoveOpen(e) {

    // console.log(e);
    const remove = new EventEmitter<AlertModalEvent>();
    remove.subscribe(res => {
      if (res.isOk) {
        this.ifApi.deleteApi(e.if_id, e.workspace_id).subscribe(res => {
        }, err => {
          console.error(err);
        }, () => {
          // this.commonModalService.openAlertDialog('Success', '삭제 성공', false, null);
          this.commonModalService.openSuccessAlert('삭제 성공', 'Success');
          this.getList(1);
        });
      }
    }, err => {

    });
    this.commonModalService.openAlertDialog('삭제', '정말 삭제하시겠습니까?', true, remove);
  }
  apiEditOpen(e) {

    this.router.navigate(['../api-mgmt/a-update', { workspaceId: this.workspaceId, ifId: e.if_id }], { relativeTo: this.route });
  }
  apiAddOpen() {
    this.router.navigate(['../api-mgmt/a-add', { workspaceId: this.workspaceId}], { relativeTo: this.route });
  }

  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    if (tempP === JSON.stringify(this.paginator)
        && this.paginator.totalPages !== 1
    ) {
      // 갱신된 값이 없으면 다시 그리지 않음.
      return true;
    }
    if (this.paginator.totalCount <= 0) {
      return true;
    }
    this.getList(this.paginator.currentPage);
  }

  onChangePageSize(data) {
    this.selectPageSize = data.selectOption;
    // this.initList();
    if (this.paginator.pageSize !== this.getCurrPageSize()) {
      this.setPageSize(this.getCurrPageSize());
      this.paginator.currentPage = 1;
    }
    this.getList(this.paginator.currentPage);
  }
  getCurrPageSize() {
    return Number(this.selectPageSize);
  }
  setPageSize(pageSize: number) {
    this.paginator.pageSize = pageSize;
  }

}
