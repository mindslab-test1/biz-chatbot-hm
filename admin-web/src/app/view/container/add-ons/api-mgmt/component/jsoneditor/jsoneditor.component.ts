import { filter } from 'rxjs/operator/filter';
import { Component, OnInit, ElementRef, Input, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import * as editor from 'jsoneditor';
const jp = require('jsonpath');
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'json-editor',
  template: '<div #jsonEditorContainer></div>'
})

export class JsonEditorComponent implements OnInit {
  private editor: any;
  private optionsDiffer: any;
  private dataDiffer: any;

  @ViewChild('jsonEditorContainer') jsonEditorContainer: ElementRef;

  @Input() options: JsonEditorOptions = new JsonEditorOptions();
  @Input() data: Object = {};

  constructor() { }

  ngOnInit() {
    this.editor = new editor(this.jsonEditorContainer.nativeElement, this.options, this.data);
  }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes.data.firstChange) return;
    this.destroy();
    this.editor = new editor(this.jsonEditorContainer.nativeElement, this.options, changes.data.currentValue);
   } 
  public collapseAll() {
    this.editor.collapseAll();
  }

  public expandAll() {
    this.editor.expandAll();
  }

  public focus() {
    this.editor.focus();
  }

  public get(): JSON {
    return this.editor.get();
  }

  public getMode(): JsonEditorMode {
    return this.editor.getMode() as JsonEditorMode;
  }

  public getName(): string {
    return this.editor.getName();
  }

  public getText(): string {
    return this.editor.getText();
  }

  public set(json: JSON) {
    this.editor.set(json);
  }

  public setMode(mode: JsonEditorMode) {
    this.editor.setMode(mode);
  }

  public setName(name: string) {
    this.editor.setName(name);
  }

  public setSchema(schema: any) {
    this.editor.setSchema(schema);
  }

  public setOptions(newOptions: JsonEditorOptions) {
    if (this.editor) {
      this.editor.destroy();
    }
    this.options = newOptions;
    this.ngOnInit();
  }

  public destroy() {
    this.editor.destroy();
  }
  public getPathArray() {
    let path = "$";
    let list = this.jsonEditorContainer.nativeElement.querySelectorAll('.jsoneditor-treepath-element');
    if(list === null || list.length === 1){
      return null;
    }
    var array = [];
    var result = [];
    for(let i = 1; i < list.length ; i++){
      let data = list[i].innerText;
      if(i + 1 == list.length ){

        if(isNaN(data)){
          var temp = path;
          temp +=".";
          temp += data;
          array.push(temp);          
          temp += ".*";
          array.push(temp); 
          var temp2 = path;
          temp2 +="..";
          temp2 += data;
          array.push(temp2);

        }else{
          path += `[`+data+`]`;
          array.push(path);
        }
      }else{

        if(isNaN(data)){
          path +=".";
          path += data;
        }else{
          path += `[`+data+`]`;
        }
      }
     }
      try{
        if(array.length > 0 ){
          for(let i = 0 ; i < array.length ; i++ ){
            const obj = { label: "", value : ""};
            let value = jp.query(this.data, array[i]);
            obj.label = array[i];
            obj.value = array[i];
            result.push(obj);
          }
        }
      
      }catch(err){
      console.log(err);
      }
    return result;
  }
  public getPath() {


    
    let path = "$";
    let list = this.jsonEditorContainer.nativeElement.querySelectorAll('.jsoneditor-treepath-element');
    if(list === null || list.length === 1){
      return null;
    }
    var array = [];
    for(let i = 1; i < list.length ; i++){
      let data = list[i].innerText;
      if(isNaN(data)){
        path +=".";
        path += data;
      }else{
        path += `[`+data+`]`;
      }
      
    }

    return path;
  }
}

export type JsonEditorMode = 'tree' | 'view' | 'form' | 'code' | 'text';

export interface JsonEditorTreeNode {
  field: String,
  value: String,
  path: String[]
}

export class JsonEditorOptions {
  public ace: Object;
  public ajv: Object;
  public onChange: () => void;
  public onEditable: (node: JsonEditorTreeNode | {}) => boolean | { field: boolean, value: boolean };
  public onError: (error: any) => void;
  public onModeChange: (newMode: JsonEditorMode, oldMode: JsonEditorMode) => void;
  public escapeUnicode: boolean;
  public sortObjectKeys: boolean;
  public history: boolean;
  public mode: JsonEditorMode;
  public modes: JsonEditorMode[];
  public name: String;
  public schema: Object;
  public search: boolean;
  public indentation: Number;
  public theme: Number;
  public language: String;
  public languages: Object;

  constructor() {
    this.escapeUnicode = false;
    this.sortObjectKeys = false;
    this.history = true;
    this.mode = 'tree';
    this.search = true;
    this.indentation = 2;
  }

}
