import {Component, EventEmitter, OnInit, ViewChild} from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { Option } from '../../../../../model/dom-models';
import { If_api } from '../../../../../adminsdk/models/If_api';
import { FormGroup, FormArray, Validators, FormBuilder} from '@angular/forms';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { JsonEditorComponent, JsonEditorOptions } from '../component/jsoneditor/jsoneditor.component';
import { If_apiApi } from '../../../../../adminsdk/services/custom/If_api';
import { UserService } from '../../../../../service/user.service';
import { If_api_json_template } from '../../../../../adminsdk/models/If_api_json_template';
import {AlertModalEvent, CommonModalService} from '../../../../../service/common-modal.service';
import { JsonpathComponent } from '../jsonpath/jsonpath.component';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
// JsonEditorComponent, JsonEditorOptions
const jp = require('jsonpath');

@Component({
  selector: 'app-a-update',
  templateUrl: './a-update.component.html',
  styleUrls: ['./a-update.component.scss']
})
export class AUpdateComponent implements OnInit {

  ifId: number;
  // json editor
  editorOptions: JsonEditorOptions;
  data: any;

  // json
  useJsonRequestFlag = false;
  invalidJsonFlag = true;
  jsonTemplate: If_api_json_template;

  // api
  useRequestFlag = false;
  apiSuccessFlag = false;
  updateItem: If_api;
  updateForm: FormGroup;

  // api request param
  paramForm: FormGroup;
  headerList: FormArray;
  paramList: FormArray;
  jsonList: FormArray;
  methodList: Option[] = [{
    label: 'GET',
    value: 'GET'
  }, {
    label: 'POST',
    value: 'POST'
  }];

  typeList: Option[] = [{
    label: 'application/x-www-form-urlencoded',
    value: 'application/x-www-form-urlencoded'
  }, {
    label: 'application/json',
    value: 'application/json'
  }];

  // api response
  resForm: FormGroup;
  resList: FormArray;

  workspaceId: number;
  userId: string;

  @ViewChild(JsonEditorComponent) editor: JsonEditorComponent;
  @ViewChild(JsonpathComponent) jsonpathHelper: JsonpathComponent;

  constructor(
      private route: ActivatedRoute
      , private fb: FormBuilder
      , private userService: UserService
      , private httpClient: HttpClient
      , private if_apiApi: If_apiApi
      , private router: Router
      , private commonModalService: CommonModalService
  ) {
    this.updateItem = new If_api();
    this.jsonTemplate = new If_api_json_template();

    this.editorOptions = new JsonEditorOptions();
    this.editorOptions.modes = ['tree']; // set all allowed modes
  }

  ngOnInit() {
    this.initApiForm();
    this.initResForm();
    this.initParamForm();

    this.ifId = Number(this.route.snapshot.paramMap.get('ifId'));
    this.workspaceId = Number(this.route.snapshot.paramMap.get('workspaceId'));
    this.userId = this.userService.getCurrentUserId();

    this.getIfApi();
  }

  getIfApi() {
    const filter: LoopBackFilter = {
      include: ['resultMapping', 'params', 'jsonTemplate'],
      where: {workspace_id : this.workspaceId}
    };

    this.if_apiApi.findById(this.ifId, filter).subscribe((res: If_api) => {
     // console.log(res);
      this.updateItem = res;
      this.setApiForm(this.updateItem.api_name, this.updateItem.description, this.updateItem.endpoint, this.updateItem.content_type, this.updateItem.method);
    }, err => {

      this.commonModalService.openErrorAlert('Api 찾기 실패');
      console.error(err);
      this.router.navigate(['../api-mgmt'], { relativeTo: this.route });
    }, () => {

      if (this.updateItem.use_json_temp_yn === 'Y') {
        if (this.updateItem.jsonTemplate && this.updateItem.jsonTemplate.length > 0) {
          this.useJsonRequestFlag = true;
          this.jsonTemplate.json_string = this.updateItem.jsonTemplate[0].json_string;

          this.valid(this.jsonTemplate.json_string);
        }
      }
      if (this.updateItem.params && this.updateItem.params.length > 0) {
        this.useRequestFlag = true;
        for (let x = 0; x < this.updateItem.params.length; x++) {
          const paramType = this.updateItem.params[x].param_type;
          const paramName = this.updateItem.params[x].param_name;
          const paramValue = this.updateItem.params[x].param_value;
          const paramTestValue = this.updateItem.params[x].param_test_value;
          const description = this.updateItem.params[x].description;
          if (this.updateItem.params[x].param_type === 'PT0001') {
            if (this.useJsonRequestFlag) {
              this.jsonList.push(this.setParamItem(paramType, paramName, paramValue, paramTestValue, description));
            } else {
              this.paramList.push(this.setParamItem(paramType, paramName, paramValue, paramTestValue, description));
            }
          } else if (this.updateItem.params[x].param_type === 'PT0002') {
            this.headerList.push(this.setParamItem(paramType, paramName, paramValue, paramTestValue, description));
          }
        }
      }
      this.apiSuccessFlag = true;
      if (this.updateItem.resultMapping || this.updateItem.resultMapping.length > 0) {
        for (let x = 0; x < this.updateItem.resultMapping.length; x++) {
          const jsonPath = this.updateItem.resultMapping[x].json_path;
          const dispName = this.updateItem.resultMapping[x].disp_name;
          this.resList.push(this.setResItem(jsonPath, dispName));
        }
      }
    });
  }

  setApiForm(apiName, description, endpoint, contentType, method) {
    this.updateForm = this.fb.group({
      api_name: [apiName, [Validators.required, Validators.maxLength(100)]],
      description: [description, [Validators.required, Validators.maxLength(100)]],
      endpoint: [endpoint, [Validators.required, Validators.maxLength(200)]],
      content_type: [contentType, ],
      method: [method, [Validators.required]]
    });
  }
  initApiForm() {
    this.updateForm = this.fb.group({
      api_name: ['', [Validators.required, Validators.maxLength(100)]],
      description: ['', [Validators.required, Validators.maxLength(100)]],
      endpoint: ['', [Validators.required, Validators.maxLength(200)]],
      content_type: ['', ],
      method: ['', [Validators.required]]
    });
  }
  initParamForm() {
    this.paramForm = this.fb.group({
      headerList: this.fb.array([]),
      jsonList: this.fb.array([]),
      paramList: this.fb.array([])
    });
    this.jsonList = this.paramForm.get('jsonList') as FormArray;
    this.paramList = this.paramForm.get('paramList') as FormArray;
    this.headerList = this.paramForm.get('headerList') as FormArray;
  }
  initResForm() {
    this.resForm = this.fb.group({
      resList: this.fb.array([])
    });
    this.resList = this.resForm.get('resList') as FormArray;
  }

  changeUseRequest(useRequestFlag, useJsonRequestFlag) {
    const method = this.updateItem.method;
    this.initParamForm();

    if (useRequestFlag) {
      if (method === 'POST' && useJsonRequestFlag) {
        if (this.paramList) {
          this.paramList.reset();
        }
        this.jsonList.push(this.createParamItem('PT0001'));
      } else {
        if (this.jsonList) {
          this.jsonList.reset();
        }
        this.paramList.push(this.createParamItem('PT0001'));
      }
      this.headerList.push(this.createParamItem('PT0002'));

    } else {
      // this.initParamForm();
      this.useJsonRequestFlag = false;
    }
  }

  createParamItem(paramType) {
    return this.fb.group({
      param_type: [paramType, [Validators.required]],
      param_name: ['', [Validators.required]],
      param_value: ['', [Validators.required]],
      param_test_value: ['', [Validators.required]],
      description: ['', ]
    });
  }
  addParamItem(paramType): void {
    if (paramType === 'PT0001') {
      if (this.useJsonRequestFlag) {
        this.jsonList.push(this.createParamItem(paramType));
      } else {
        this.paramList.push(this.createParamItem(paramType));
      }
    } else if (paramType === 'PT0002') {
      this.headerList.push(this.createParamItem(paramType));
    }
  }
  setParamItem(paramType, paramName, paramValue, paramTestValue, description) {
    return this.fb.group({
      param_type: [paramType, [Validators.required]],
      param_name: [paramName, [Validators.required]],
      param_value: [paramValue, [Validators.required]],
      param_test_value: [paramTestValue, [Validators.required]],
      description: [description, ]
    });
  }

  createResItem() {
    return this.fb.group({
      json_path: ['', [Validators.required]],
      disp_name: ['', [Validators.required]]
    });
  }
  addResItem(): void {
    this.resList.push(this.createResItem());
  }
  setResItem(jsonPath, dispName) {
    return this.fb.group({
      json_path: [jsonPath, [Validators.required]],
      disp_name: [dispName, [Validators.required]]
    });
  }

  valid(jsonFormat) {
    this.invalidJsonFlag = !this.isJson(jsonFormat);
  }

  isJson(data) {
    try {
      JSON.parse(data);
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  }

  requestApi() {
    const method = this.updateItem.method;
    let url = this.updateItem.endpoint;

    if (url && url.length > 0) {
      if (url.indexOf('localhost') > -1) {
        const ipAddress = window.location.hostname;
        url = url.replace('localhost', ipAddress);
      }
    } else {
      this.commonModalService.openErrorAlert('Please Input URL');
      return;
    }
    let contentType = 'application/json';
    let params;
    let body;
    let header;

    if (method === 'POST') {
      contentType = this.updateItem.content_type;
    } else if (method === 'GET') {
      contentType = '';
      body = '';
    } else {
      console.log('Error method value');
      return;
    }

    if (this.useRequestFlag) {

      if (method === 'POST') {
        body = this.setApiBody();
      } else if (method === 'GET') {
        params = this.setApiParameter();
      }

    }
    header = this.setApiHeader().set('Content-Type', contentType);

    this.httpClient.request(method, url, {
      headers: header,
      body: body,
      params: params,
      observe: 'response',
      responseType: 'json'
    }).subscribe(res => {
      try {
        this.data = res.body;
        this.apiSuccessFlag = true;
      } catch (err) {

        this.commonModalService.openErrorAlert('Json parse fail' + err);
        this.apiSuccessFlag = false;
      }
    }, error => {
      this.apiSuccessFlag = false;

      this.commonModalService.openErrorAlert(error.message);
      console.error('error', error);
    }, () => {

    });
  }

  changeRequestData() {
    if (this.apiSuccessFlag) {
      this.initResForm();
      this.addResItem();
      this.useJsonRequestFlag = false;
      this.jsonTemplate.json_string = '';
      this.apiSuccessFlag = false;
    }
  }

  cancel() {

    const outer = new EventEmitter<AlertModalEvent>();
    outer.subscribe(res => {

      if (res.isOk) {
        this.router.navigate(['../'], { relativeTo: this.route });
      }
    }, err => {

    });
    this.commonModalService.openAlertDialog('이동', '나가시겠습니까', true, outer);
  }
  editorClick() {
    const path = this.editor.getPath();

    if (path) {
      try {
        const value = jp.query(this.data, path);
        this.setJsonPath(path);
      } catch (err) {

        this.commonModalService.openErrorAlert('Need JSON Format' + err);
        console.log(err);
      }
    }
  }

  setApiBody() {
    if (this.updateItem.content_type === 'application/json') {
      if (this.useJsonRequestFlag) {
        return this.jsonTemplate.json_string;
      } else {
        const form = this.paramForm.get('paramList')['controls'];
        const jsonData = this.getFormArrayToJSON(form);
        return jsonData;
      }
    } else {
      const form = this.paramForm.get('paramList')['controls'];
      const httpParams = this.getFormArrayToObject(form);
      return httpParams.toString();
    }
  }
  getFormArrayToJSON(form: FormArray): string {
    const data = {};

    if (form != null && form.length > 0) {
      for (let x = 0; x < form.length; x++) {
        const key = form[x].getRawValue().param_name;
        const value = form[x].getRawValue().param_test_value;
        if (key == null || key === '' || value == null || value === '') {

        } else {
          data[key] = value;
        }
      }

    }
    return JSON.stringify(data);
  }
  getFormArrayToObject(form: FormArray): HttpParams {
    const data = {};
    let httpParams = new HttpParams();
    if (form != null && form.length > 0) {
      for (let x = 0; x < form.length; x++) {
        const key = form[x].getRawValue().param_name;
        const value = form[x].getRawValue().param_test_value;
        if (key == null || key === '' || value == null || value === '') {

        } else {
          data[key] = value;
        }
      }
      Object.keys(data).forEach(function (key) {
        httpParams = httpParams.append(key, data[key]);
      });
    }
    return httpParams;
  }
  getFormArrayToHeader(form: FormArray): HttpHeaders {
    const data = {};
    let httpParams = new HttpHeaders();
    if (form != null && form.length > 0) {
      for (let x = 0; x < form.length; x++) {
        const key = form[x].getRawValue().param_name;
        const value = form[x].getRawValue().param_test_value;
        if (key == null || key === '' || value == null || value === '') {

        } else {
          data[key] = value;
        }
      }
      Object.keys(data).forEach(function (key) {
        httpParams = httpParams.append(key, data[key]);
      });
    }
    return httpParams;
  }

  setApiParameter(): HttpParams {
    const form = this.paramForm.get('paramList')['controls'];
    const httpParams = this.getFormArrayToObject(form);
    return httpParams;
  }
  setApiHeader(): HttpHeaders {
    const form = this.paramForm.get('headerList')['controls'];
    const httpHeaders = this.getFormArrayToHeader(form);
    return httpHeaders;
  }
  setJsonPath(pathExpression) {
    const form = this.resForm.get('resList')['controls'];

    if (form != null && form.length > 0) {
      form[form.length - 1]['controls']['json_path'].setValue(pathExpression);
    }
  }

  removeRes(i) {
    this.resList.removeAt(i);
    if (this.resList.length === 0) {
      this.changeRequestData();
    }
  }
  removeHeader(i) {
    this.headerList.removeAt(i);
  }
  removeParam(i) {
    this.paramList.removeAt(i);
  }
  removeJsonParam(i) {
    this.jsonList.removeAt(i);
  }
  save() {
    this.updateItem.api_name = this.updateForm.value.api_name;
    this.updateItem.endpoint = this.updateForm.value.endpoint;
    this.updateItem.description = this.updateForm.value.description;
    this.updateItem.content_type = this.updateForm.value.content_type;
    this.updateItem.method = this.updateForm.value.method;
    this.updateItem.workspace_id = this.workspaceId;
    this.updateItem.updator_id = this.userId;

    let arr = [];
    if (this.useRequestFlag) {
      this.updateItem.use_json_temp_yn = 'N';
      arr = arr.concat(this.paramForm.value.headerList);

      if (this.updateItem.method === 'POST' && this.useJsonRequestFlag) {

        this.updateItem.use_json_temp_yn = 'Y';
        arr = arr.concat(this.paramForm.value.jsonList);

      } else {
        arr = arr.concat(this.paramForm.value.paramList);
      }
    }
    this.if_apiApi.updateApi(this.updateItem, this.resForm.value.resList, arr, this.jsonTemplate, this.workspaceId).subscribe(res => {
          // console.log(res);
        }, err => {

          this.commonModalService.openErrorAlert('API 수정 실패');
          console.error(err);
        }, () => {

          this.commonModalService.openSuccessAlert('API 수정 성공');
          this.router.navigate(['../'], { relativeTo: this.route });
        }
    );
  }

  getJsonpathInput(value) {
    this.resList.push(this.setResItem(value, ''));
  }
  jsonpathOpen(text) {
    this.jsonpathHelper.jsonpathOpen(text);
  }
}

