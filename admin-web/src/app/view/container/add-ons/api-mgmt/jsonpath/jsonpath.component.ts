import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { JsonEditorComponent, JsonEditorOptions } from '../component/jsoneditor/jsoneditor.component';
import { CommonModalService } from '../../../../../service/common-modal.service';
import { SelectItem } from 'primeng/primeng';

const jp = require('jsonpath');
@Component({
  selector: 'app-jsonpath',
  templateUrl: './jsonpath.component.html',
  styleUrls: ['./jsonpath.component.scss']
})
export class JsonpathComponent implements OnInit {

  jsonpathFlag:Boolean = false;
  jsonpaths = [];
  selectedCity1;
  //json editor
  editorOptions: JsonEditorOptions;
  data: any;
  result: any;
  outputData: string;
  
  manualFlag:Boolean = false;
  manualData;
  @ViewChild(JsonEditorComponent) editorHelper: JsonEditorComponent;
  
  @Output() jsonpathInput = new EventEmitter<Object>();
  constructor(private commonModalService: CommonModalService) { 
  }
  setPath(e){
    if(e){
      this.outputData = e;
      try{
        this.result = jp.query(this.data, e);
      }catch{
        this.result = null;
      }
    }
  }
  ngOnInit() {
  }
    

    jsonpathOpen(data: string){

      this.jsonpathFlag = true;
      if(data){
        this.data = data;
      }
    }
    modalClose(){
      this.jsonpathFlag = false;
    }
    changeFlag(){
      this.manualFlag = !this.manualFlag;
    }
    editorClick() {
      if(this.manualFlag){
        this.manualData = this.editorHelper.getPath();
        this.setPath(this.manualData);
      }else{
        let array = this.editorHelper.getPathArray();
        if (array) {
          try {
            if(array  && array.length > 0){
              this.jsonpaths = array;
              this.setPath(array[0].value);
            }
          } catch (err) {
            // console.error(err);
          }
        }
      }
    }
    save(){
      
      this.jsonpathInput.emit(this.outputData);
      this.jsonpathFlag = false;
    }
    changeManualData(data){
      this.setPath(data);
    }
}
