import { Component, OnInit, OnDestroy } from '@angular/core';
import { BasicQAService } from '../../../../service/bqa.service';
import { DataService } from '../../../../service/data.service';
import { MessageExService } from '../../../../service';

@Component({
  selector: 'app-nlp-test',
  templateUrl: './nlp-test.component.html',
  styleUrls: ['./nlp-test.component.scss']
})
export class NlpTestComponent implements OnInit, OnDestroy {

  nlpItems;
  
  constructor(
    private basicQAService: BasicQAService,
    private ds: DataService
  ) { }

  ngOnInit() {
  }
  ngOnDestroy() {
    this.ds.clearData();
  }

  private _isWorking: boolean = false;
  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if(this._isWorking == v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }

  nlp(data) {
    this.isWorking = true;

    this.basicQAService.nlp(data).subscribe(res => {
      this.isWorking = false;
        this.nlpItems = res.data.list;
    }, err=>{
      this.isWorking = false;
    });
  }
}
