import { Component, OnInit, Input } from '@angular/core';
import { ChatbotDialogService } from '../../../../service/chatbot-dialog.service';
import { UtterData } from '../../../../model/response-dialog';
import { BqaDebug, BqaSearch, BqaDebugEx } from '../../../../model/response-debug';
import { CommonUtilService } from '../../../../service';

const _ = require('lodash');
@Component({
  selector: 'app-chatbot-debug-bqa',
  templateUrl: './chatbot-debug-bqa.component.html',
  styleUrls: ['./chatbot-debug-bqa.component.scss']
})
export class ChatbotDebugBqaComponent implements OnInit {

  constructor(
      private cm: CommonUtilService
    , private dialogService: ChatbotDialogService
  ) { }

  ngOnInit() {
  }
  d = this.dialogService.default;

  private _currUtterData: UtterData = this.d.uttrData;
  @Input() set currUtterData(v: UtterData){
    this._currUtterData = v;

    if (this.cm.isEmpty(v.bqaDebug)
      || this.cm.isEmpty(v.bqaDebug.data)
    ) {
      if (this.cm.isEmpty(v.bqaDebugEx)
        || this.cm.isEmpty(v.bqaDebugEx.data)
      ) {
        this.cm.isDev() && console.log('debug 정보가 없습니다.');
        return;
      } else {
        this.resolveBqaDebugEx(v);
        return;
      }
    } else {
      this.resolveBqaDebug(v);
    }
  }
  resolveBqaDebug(v: UtterData) {
    const debug: BqaDebug = v.bqaDebug.data;
    const currStatus: string = this._getStatus(debug);
    this.vm = this.parseUtterToView(debug, currStatus);
    this.currStatus = currStatus;
  }
  resolveBqaDebugEx(v: UtterData) {
    const debug: BqaDebugEx = v.bqaDebugEx.data;
    const currStatus: string = this._getStatus(debug);
    this.vm = this.parseUtterToView(debug, currStatus);
    this.currStatus = currStatus;

  }
  parseUtterToView(debug: BqaDebug | BqaDebugEx, status: string) {
    const bqaSearch = debug.list[0];
    let vm = {
        domains: this._parseDomainNames(debug, status)
      , status
      , question: this._parseQuestion(debug, status)
      , keywords: this._parseKeywords(debug, status)
      , answers: this._parseAnswers(debug, status)
      , err: ''
    };
    return vm;
  }
  status = {
      exactMatching: 'exactMatching'
    , primaryMorphemeMatching: 'primaryMorphemeMatching'
    , secondaryMorphemeMatching: 'secondaryMorphemeMatching'
    , invalid: 'invalid'
  };
  private _getStatus=(debug: BqaDebug | BqaDebugEx)=>{
    const v: number = debug.status;
    switch(v) {
      case 1: return this.status.exactMatching;
      case 2: return this.status.primaryMorphemeMatching;
      case 3: return this.status.secondaryMorphemeMatching;
      default: return this.status.invalid;
    }
  };
  private _parseDomainNames = (debug: BqaDebug | BqaDebugEx, status: string)=>{
    const bqaSearch = debug.list[0];
    switch(status) {
      case this.status.exactMatching:
      case this.status.primaryMorphemeMatching:
      case this.status.secondaryMorphemeMatching:
        return [debug.usedDomainName];

      default:
        return [];
    }
  };
  private _parseQuestion = (debug: BqaDebug | BqaDebugEx, status: string)=>{
    const bqaSearch = debug.list[0];
    switch(status) {
      case this.status.exactMatching:
      case this.status.primaryMorphemeMatching:
      case this.status.secondaryMorphemeMatching:
        return debug.question || bqaSearch.question;

      default:
        return [];
    }
  };
  KEYWORD_DELIM = ' ';
  private _parseKeywords =(debug: BqaDebug | BqaDebugEx, status: string)=>{
    switch(status) {
      case this.status.exactMatching:
        return [];
      
      case this.status.primaryMorphemeMatching:
        const bqaSearch = debug.list[0];
        return bqaSearch.questionMorph.split(this.KEYWORD_DELIM);

      case this.status.secondaryMorphemeMatching:
        return _.reduce(debug.list,(res, item)=>{
          // TDOO 질문의 키워드로 바뀌여어야 함. 현재는 응답의 키워드임.
          // res.push(item.questionMorph.split(this.KEYWORD_DELIM));
          res.push(item.questionMorph);
          return res;
        },[]);
      default:
        return [];
    }
  };
  private _parseAnswers=(debug: BqaDebug | BqaDebugEx, status: string)=>{
    
    const _resolveExactMatching = ()=>{
      const answer = debug.answer || 'debug: 답변을 찾지 못함.';
      const res = {
        answer
      };
      return res;
    };
    const _resolvePrimaryMorphemeMatching = ()=>{
      // FIXME
      const res = debug.list.reduce((resList, item: BqaSearch)=>{
        if(this.cm.isEmpty(item.answerList)) {
          return resList;
        }
        if(this.cm.isNotEmpty(resList)){
          // 최초의 결과값만 필요하므로 하나만 넘김.
          return resList;
        }

        const answer = item.answerList[0].answer;
        const score = item.score;
        const _res = {
            answer
          , score
        };
        resList.push(_res);
        return resList;
      }, []);
      return res;
    };
    const _resolveSecondaryMorphemeMatching = ()=>{
      const res = debug.list.reduce((resList, item: BqaSearch)=>{
        if(this.cm.isEmpty(item.question)) {
          return resList;
        }
        const questionCandidate = item.question;
        const score = item.score;
        const _res = {
            questionCandidate
          , score
        };
        resList.push(_res);
        return resList;
      }, []);
      return res;
    };
    switch(status) {
      case this.status.exactMatching:
        return _resolveExactMatching();

      case this.status.primaryMorphemeMatching:
        return _resolvePrimaryMorphemeMatching();

      case this.status.secondaryMorphemeMatching:
        return _resolveSecondaryMorphemeMatching();

      default:
        return _resolveExactMatching();
    }
  };

  private defaultVm: any = {};
  private _vm: any = this.defaultVm;
  get vm(): any {
    return this._vm;
  }
  set vm(v: any) {
    this._vm = v;
  }
  private _currStatus: any = this.status.invalid;
  get currStatus(): any {
    return this._currStatus;
  }
  set currStatus(v: any) {
    this._currStatus = v;
  }
  isExactMatching(){
    return this.currStatus == this.status.exactMatching;
  }
  isPrimaryMorphemeMatching(){
    return this.currStatus == this.status.primaryMorphemeMatching;
  }
  isSecondaryMorphemeMatching(){
    return this.currStatus == this.status.secondaryMorphemeMatching;
  }
  isInvalid(){
    return this.currStatus == this.status.invalid;
  }
}
