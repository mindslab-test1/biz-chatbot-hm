import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { DataService } from '../../../../service/data.service';
import { CommonUtilService } from '../../../../service';
import { ChatbotUpsertService } from '../../../../service/chatbot-upsert.service';
import { ChatbotInfoService } from '../../../../service/chatbot-info.service';

export interface MenuItemEx {
  label?: string;
  urls?: string[];
  chatbotTestAvailable?: boolean;
}
@Component({
  selector: 'app-chatbot-upsert-shared',
  templateUrl: './chatbot-upsert-shared.component.html',
  styleUrls: ['./chatbot-upsert-shared.component.scss']
})
export class ChatbotUpsertSharedComponent implements OnInit, OnDestroy {
  c = this.service.constants;
  d = this.chatbotInfoService.default;
  private _bot_id = this.d.botId;
  private _title = this.d.title;
  private _isSave = true;
  chatbotTopMenu: MenuItemEx[] = [];
  // activeMenu: MenuItemEx;
  _activeMenu: MenuItemEx;
  activeMenuInterval;
  @Output() outer = new EventEmitter(false);
  private _isQna = false;
  private _isScenario = false;

  constructor(
      private router: Router
    , private cm: CommonUtilService
    , private ds: DataService
    , private service: ChatbotUpsertService
    , private chatbotInfoService: ChatbotInfoService
  ) {}
  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    clearInterval(this.activeMenuInterval);
    // this.subscription.unsubscribe();
    this.ds.clearData();
  }
  init() {
    // this.setSubscription();
    this.initMenuItems();
    this.checkActiveMenuInterval();
  }

  @Input() set bot_id(v) {
    // console.log('debug set bot_id', v);
    if (v === this.d.botId) {
      // this.cm.isDev() && console.log('not yet ready bot_id');
      return;
    }
    this._bot_id = v;
    this.initMenuItems();
  }
  get bot_id() {
    return this._bot_id;
  }

  get title() {
    return this._title;
  }
  @Input() set title(title) {
    if (this._title === title) {
      return;
    }
    this._title = title;
    this.outer.emit({ cmd: this.ds.CMD.TITLE, title });
  }

  get isSave() {
    return this._isSave;
  }
  @Input() set isSave(isSave) {
    this._isSave = isSave;
    this.outer.emit({ cmd: this.ds.CMD.IS_SAVE, isSave });
  }

  onChangeTitle(event) {
    if (this.cm.isEnter(event)) {
      this.save(event);
      return;
    }
    this.isSave = false;
  }


  goToList() {
    this.router.navigate([environment.container, this.c.URL.chatbot_management]);
  }

  get activeMenu(): MenuItemEx {
    return this._activeMenu;
  }
  @Input() set activeMenu(v: MenuItemEx) {
    if (!v) {
      return;
    }
    this._activeMenu = v;

    this.ds.sendData(this.ds.CMD.CHATBOT_TEST_AVAILABLE, {v});
    // this.outer.emit({ cmd: this.ds.CMD.CHATBOT_TEST_AVAILABLE, v });
  }
  initMenuItems() {
    const menus = [
        ['기본정보' , ['', environment.container, this.c.URL.chatbot_container, this.c.URL.chat_base], false]
      , ['PREITF'  , ['', environment.container, this.c.URL.chatbot_container, this.c.URL.c_preitf], false]
      , ['Q&A'     , ['', environment.container, this.c.URL.chatbot_container, this.c.URL.c_bqa], false]
      , ['시나리오' , ['', environment.container, this.c.URL.chatbot_container, this.c.URL.c_scenario], false]
      , ['테스트'   , ['', environment.container, this.c.URL.chatbot_container, this.c.URL.c_test], true]
      // , ['채널 연동', ['', environment.container, this.c.URL.chatbot_container, this.c.URL.channel_connection], true]
    ];
    const _setBotId = (menu) => {
      // const _setLinks = (v): Array<any> => {
      //   if (this.bot_id > this.d.botId) {
      //     v.push(this.bot_id);
      //   }
      //   return v;
      // };
      const label = menu[0];
      const urls = ((v): Array<any> => {
        if (this.bot_id > this.d.botId) {
          v.push(this.bot_id);
        }
        return v;
      })(menu[1]);
      const chatbotTestAvailable = menu[2];
      return [label, urls, chatbotTestAvailable];
    };
    menus.map(menu => _setBotId(menu));

    const _getMenuItem = (menu): MenuItemEx => {
      return { label: menu[0], urls: menu[1], chatbotTestAvailable: menu[2] };
    };
    this.chatbotTopMenu = menus.map(menu => _getMenuItem(menu));
    this.activeMenu = this.chatbotTopMenu[0];
  }

  save(event) {
    this.outer.emit({ cmd: this.ds.CMD.SAVE, data: event });
  }

  menuClick(event, menu) {
    const activeMenu: MenuItemEx = menu.activeItem;
    this.router.navigate(activeMenu.urls);
    this.activeMenu = activeMenu;
    // clearInterval(this.activeMenuInterval);
    // this.checkActiveMenuInterval();
  }

  checkActiveMenuInterval() {
    const isValidActiveMenu = (hash: string, currActiveMenuUrls: string[]) => hash.indexOf(currActiveMenuUrls.join('/')) > -1;
    const isInvalidActiveMenu = (hash: string, currActiveMenuUrls: string[]) => !isValidActiveMenu(hash, currActiveMenuUrls);
    this.activeMenuInterval = setInterval((__this) => {
      const hash = document.location.hash;
      if (isInvalidActiveMenu(hash, __this.activeMenu.urls)) {
        __this.activeMenu = __this.chatbotTopMenu.find((topMenu: MenuItemEx) => isValidActiveMenu(hash, topMenu.urls));
        this.cm.devLog('check menu => change');
        // clearInterval(this.activeMenuInterval);
      }
      const qnaUrl = (['', environment.container, __this.c.URL.chatbot_container, __this.c.URL.c_bqa]).join('/');
      __this.isQna = (hash.indexOf(qnaUrl) >= 0);
      const scenarioUrl = (['', environment.container, __this.c.URL.chatbot_container, __this.c.URL.c_scenario]).join('/');
      __this.isScenario = (hash.indexOf(scenarioUrl) >= 0);
    }, 150, this);
  }
  get isQna() {
    return this._isQna;
  }
  set isQna(v) {
    if (this._isQna === v) {
      return ;
    }
    this._isQna = v;
    this.ds.sendData(this.ds.CMD.IS_QNA, {isQna: v});
  }
  get isScenario() {
    return this._isScenario;
  }
  set isScenario(v) {
    if (this._isScenario === v) {
      return ;
    }
    this._isScenario = v;
    this.ds.sendData(this.ds.CMD.IS_SCENARIO, {isScenario: v});
  }
}
