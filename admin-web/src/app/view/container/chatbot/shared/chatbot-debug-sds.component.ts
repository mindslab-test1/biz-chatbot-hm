import { Component, Input, OnInit } from '@angular/core';
import { ChatbotDialogService } from '../../../../service/chatbot-dialog.service';
import { UtterData } from '../../../../model/response-dialog';
import { CommonUtilService } from '../../../../service';
import { SdsDebug } from '../../../../model/response-debug-sds';

@Component({
  selector: 'app-chatbot-debug-sds',
  templateUrl: './chatbot-debug-sds.component.html',
  styleUrls: ['./chatbot-debug-sds.component.scss']
})
export class ChatbotDebugSdsComponent implements OnInit {
  constructor(
    private cm: CommonUtilService
  , private dialogService: ChatbotDialogService
  ) { }

  ngOnInit() {
  }
  d = this.dialogService.default;

  private _currUtterData: UtterData = this.d.uttrData;
  @Input() set currUtterData(v: UtterData){
    this._currUtterData = v;
    if (this.cm.isEmpty(v.debug)) {
      this.cm.isDev() && console.log('debug 정보가 없습니다.');
      return;
    }
    this.vm = this.parseUtterToView(v);
  }
  parseUtterToView(v: UtterData) {
    const debug = v.debug;
    let vm = {
        domain: v.sdsDomain || ''
      // , prevTask: ''
      // , currTask: ''
      , entities: this.parseEntities(debug)
      , userIntents: this.parseUserIntents(debug)
      , systemIntents: this.parseSystemIntents(debug)
      , err: ''
    };
    return vm;
  }

  private parseEntities=(debug: SdsDebug)=>{
    const d = this.delim;
    const res = this._parseSluList(debug.slu).reduce((resList, item)=>{
      const {k, v} = item;
      if(k.indexOf(this.USER_INTENT_KEY) < 0) {
        return resList;
      }
      if( v.indexOf(d.sluTab) < 0
      ) {
        this.cm.isDev() && console.log(`No ${d.sluTab}`);
        return resList;
      }
      const sp = this.splitEntities(v.split(d.sluTab)[0]);
      sp.forEach(_item => {
        if(_item.startsWith(d.lt) && _item.endsWith(d.gt)) {
          let tempV = _item.split('');
          _item  = tempV.slice(1,tempV.length-1).join('');
        }
        
        let evStr = _item.indexOf(d.equal) < 0 ? `${d.equal}${_item}` : _item;
        const res = this._getEV(evStr);
        resList.push(res);
      });
      return resList;
    },[]);
    return res;
  };
  private splitEntities=(entityStr:string): string[]=>{
    const d = this.delim;
    const ls = entityStr.split('');
    let list: string[] = [];
    let temp: string[] = [];
    const lsLastIdx = ls.length - 1;
    ls.forEach((item, idx)=>{
      switch(item){
      case d.lt:
        if(this.cm.isNotEmpty(temp.length)) {
          list.push(temp.join(''));
        }
        temp = [];
        break;
      case d.gt:
        list.push(temp.join(''));
        temp = [];
        break;
      default:
        temp.push(item);
      break;
      }
      if(lsLastIdx == idx) {
        if(this.cm.isNotEmpty(temp.length)) {
            list.push(temp.join(''));
        }
      }  
    });
    return list;
  };
  private parseEntitiesEx=(debug: SdsDebug)=>{
    const d = this.delim;
    const res = this._parseSluList(debug.slu).reduce((resList, item)=>{
      const {k, v} = item;
      if(k.indexOf(this.USER_INTENT_KEY) < 0) {
        return resList;
      }
      if( v.indexOf(d.sharp) < 0
        || v.indexOf(d.open) <= v.indexOf(d.sharp)
        || v.indexOf(d.close) <= v.indexOf(d.open)
      ) {
        this.cm.isDev() && console.log('invalid slu');
        return resList;
      }
      const sp = v.split(d.sharp)[1].split(d.open)[1].split(d.close)[0];
      if(sp.indexOf(d.comma) > 0) {
        sp.split(d.comma).forEach(_item => {
          if(_item.indexOf(d.equal) < 0) {
            return true; //continue
          }
          
          const res = this._getEV(_item);
          resList.push(res);
        });
      } else {
        const _item = sp;
        if(_item.indexOf(d.equal) < 0) {
          return resList;
        }
        const res = this._getEV(_item);
        resList.push(res);
      }
      return resList;
    },[]);
    return res;
  };
  private parseUserIntents=(debug: SdsDebug)=>{
    const d = this.delim;
    const res = this._parseSluList(debug.slu).reduce((resList, item)=>{
      const {k, v} = item;
      if(k.indexOf(this.USER_INTENT_KEY) < 0
        || v.indexOf(d.sharp) < 0
        || v.indexOf(d.open) <= v.indexOf(d.sharp)
        || v.indexOf(d.close) <= v.indexOf(d.open)
      ) {
        return resList;
      }
      const res = {
          type:  v.split(d.sharp)[1].split(d.open)[1].split(d.close)[0] || ''
        ,intent: v.split(d.sharp)[1].split(d.open)[0] || ''
      };
      resList.push(res);
      return resList;
    },[]);
    return res;
  };
  private parseSystemIntents=(debug: SdsDebug)=>{
    const d = this.delim;
    if(this.cm.isEmpty(debug.da)) {
      return [];
    }
    const {k, v} = (da=>{
      let k = '';
      let v = '';
      const openIdx = da.indexOf(d.open);
      const closeIdx = da.indexOf(d.close);
      if(openIdx > 0 && closeIdx > openIdx){
        const sp = da.split(d.open);
        k = sp[0] || '';
        v = sp[1].split(d.close)[0] || '';
      }
      return { k, v };
    })(debug.da);

    const res = [
      {
          type: v
        , intent: k
      }
    ];
    return res;
  };

  private _vm: any = {};
  get vm(): any {
    return this._vm;
  }
  set vm(v: any) {
    this._vm = v;
  }


  private delim = {
      open: '('
    , close: ')'
    , colon: ':'
    , comma: ','
    , equal: '='
    , sharp: '#'
    , sluTab: '<TAB>'
    , lt: '<'
    , gt: '>'
    , ln: '\n'
  };
  USER_INTENT_KEY = 'SLU1';
  private _parseSluList=(slu: string)=>{
    const d = this.delim;
    const sluList = slu.trim().split(d.ln);
    const res = sluList.reduce((resList, item)=>{
      if(item.indexOf(d.colon) < 1) {
        this.cm.isDev() && console.log('invalid slu skip');
        return resList;
      }
      const sp = item.split(d.colon);
      const k = sp[0].trim();
      const v = sp[1].trim();
      resList.push({k, v});
      return resList;
    },[]);
    return res;
  };
  private _getEV=(sp: string)=>{
    const d = this.delim;
    const res = {
        entity: sp.split(d.equal)[0]
      , value: sp.split(d.equal)[1]
    };
    return res;
  };
}
