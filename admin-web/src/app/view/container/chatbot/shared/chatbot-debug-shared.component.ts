import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { DataService } from '../../../../service/data.service';
import { ChatbotDialogService } from '../../../../service/chatbot-dialog.service';
import { CommonUtilService } from '../../../../service';

@Component({
  selector: 'app-chatbot-debug-shared',
  templateUrl: './chatbot-debug-shared.component.html',
  styleUrls: ['./chatbot-debug-shared.component.scss']
})
export class ChatbotDebugSharedComponent implements OnInit, OnDestroy {

  constructor(
    private ds: DataService
    , private dialogService: ChatbotDialogService
    , private cm: CommonUtilService
  ) { }

  ngOnInit() {
  }
  ngOnDestroy() {
    this.ds.clearData();
  }
  d = this.dialogService.default;
  chatbotStepGroupIsInvalid(v){
    return this.dialogService.chatbotStepGroupIsInvalid(v);
  }
  chatbotStepGroupIsInit(v){
    return this.dialogService.chatbotStepGroupIsInit(v);
  }
  chatbotStepGroupIsBqa(v){
    return this.dialogService.chatbotStepGroupIsBqa(v);
  }
  chatbotStepGroupIsScf(v){
    return this.dialogService.chatbotStepGroupIsScf(v);
  }
  chatbotStepGroupIsSds(v){
    return this.dialogService.chatbotStepGroupIsSds(v);
  }
  private _isDebug: boolean;
  get isDebug(): boolean {
    return this._isDebug;
  }
  @Input() set isDebug(v: boolean) {
    if (this._isDebug == v) {
      return;
    }
    this._isDebug = v;
  } 
  @Input() alwaysOn;
  @Input() currStep = this.d.chatbotStep;
  @Input() currUtterData = this.d.uttrData;
  @Input() sessionId = this.d.sessionId;
  
  /*
   * TODO TODO TODO TODO
   *  디버그 대상에 따라 데이터 갈아쳐야 함. 
   *  bqa, sds 구분 및 데이타 타입 고려
   */

  toggleDebug(event) {
    if (this.alwaysOn) {
      this.cm.isDev() && console.log('통합 테스트용 토글 하지 않음.');
      return;
    }
    this.isDebug = !this.isDebug;
    const isDebug = this.isDebug;
    this.ds.sendData(this.ds.CMD.IS_DEBUG, {isDebug})
  }
}
