import { Component, OnInit, Input } from '@angular/core';
import { ChatbotDialogService } from '../../../../service/chatbot-dialog.service';

@Component({
  selector: 'app-chatbot-debug-init-greet',
  templateUrl: './chatbot-debug-init-greet.component.html',
  styleUrls: ['./chatbot-debug-init-greet.component.scss']
})
export class ChatbotDebugInitGreetComponent implements OnInit {

  constructor(
    private dialogService: ChatbotDialogService
  ) { }

  ngOnInit() {
  }
  d = this.dialogService.default;
  @Input() currUtterData = this.d.uttrData;

}
