import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { UtilService } from '../../../../../service';
import { CommonUtilService } from '../../../../../service';
import { UserService } from '../../../../../service/user.service';
import { Paginator } from '../../../../../common/paginator/paginator.component';
import { LoopBackFilter } from '../../../../../adminsdk/models';
import { PreAddComponent } from './pre-add.component';
import { TreeNode } from 'primeng/components/common/treenode';
import { PreEditComponent } from './pre-edit.component';
import { CommonModalService, AlertModalEvent } from '../../../../../service';
import { Itf_prebqa } from '../../../../../adminsdk/models';
import { Itf_prebqaApi } from '../../../../../adminsdk/services/custom';


@Component({
  selector: 'app-pre-classifier',
  templateUrl: './pre-classifier.component.html',
  styleUrls: ['./pre-classifier.component.scss']
})
export class PreClassifierComponent implements OnInit {
  pgSize = 5;
  userId;
  botId: number;
  paginator = new Paginator();
  items: TreeNode[];
  itf_prebqa: Itf_prebqa;
  maxItfOrder = 0;

  @ViewChild('addModal') addModal: PreAddComponent;
  @ViewChild('editModal') editModal: PreEditComponent;
  constructor(private utilService: UtilService,
    private userSerivce: UserService,
    private cm: CommonUtilService,
    private itf_prebqaApi: Itf_prebqaApi,
    private commonModalService: CommonModalService
  ) {

    this.userId = this.userSerivce.getCurrentUserId();
  }

  ngOnInit() {
    // this.init();
  }

  init(botId) {
    // this.cm.devLog("init!!");
    this.paginator.pageSize = this.pgSize;
    this.paginator.currentPage = 1;

    if (botId && botId !== 0) {
      this.getList(this.paginator.currentPage, this.pgSize, botId);
    } else {
      this.items = null;
    }
  }

  addModalClose(event) {
    // this.cm.devLog("addNodalClose", event);
    if (event) {
      this.getList(this.paginator.currentPage, this.pgSize, this.botId);
    }
  }
  editModalClose(event) {
    // this.cm.devLog("editModalClose", event);
    if (event) {
      this.getList(this.paginator.currentPage, this.pgSize, this.botId);
    }
  }

  addModalOpen(itf_prebqa: Itf_prebqa) {
    this.botId = itf_prebqa.bot_id;
    itf_prebqa.itf_order = this.maxItfOrder + 1;
    this.addModal.scfAddOpen(itf_prebqa);
  }

  editModalOpen(e) {
    this.editModal.scfEditOpen(e);
  }
  getList(pg, pageSize, botId) {
    this.botId = botId;
    this.paginator.currentPage = pg;

    const filter: LoopBackFilter = {
      where: { and: [{ bot_id: botId }] },
      skip: (pg - 1) * this.paginator.pageSize,
      limit: this.paginator.pageSize,
      order: `preitf_id desc`
    };
    // add-ons/api-mgmt/a-add/a-add
    this.itf_prebqaApi.count(filter.where).subscribe(res => {
      // this.cm.devLog("count", res);
      if (res.count <= 0) {
        this.items = null;
        this.maxItfOrder = 0;
        return;
      } else {
        this.paginator.totalCount = res.count;

        this.itf_prebqaApi.find(filter).subscribe(res2 => {
          // this.cm.devLog("res :", res)
          this.items = res2;
          this.maxItfOrder = this.getMaxItfOrder(res2);
        });
      }

      }, error => {
        console.error(error);
      }, () => {
      }
    );

  }
  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    if (tempP === JSON.stringify(this.paginator)
      && this.paginator.totalPages !== 1
    ) {
      // 갱신된 값이 없으면 다시 그리지 않음.
      return true;
    }
    if (this.paginator.totalCount <= 0) {
      return true;
    }
    this.getList(this.paginator.currentPage, this.pgSize, this.botId);
  }

  removeOpen(entity) {
    // this.cm.isDev() && console.log(entity);

    const outer = new EventEmitter<AlertModalEvent>();
    outer.subscribe(res => {
      // this.cm.devLog('res',res);
      if (res.isOk) {
        this.itf_prebqaApi.deleteById(entity.preitf_id).subscribe(res2 => {
          // this.cm.devLog(res);
        }, error => {
          this.commonModalService.openErrorAlert(error);
        },
          () => {
            this.commonModalService.openAlertDialog('Success', '삭제 성공', false, null);
            this.getList(this.paginator.currentPage, this.pgSize, this.botId);
          }
        );
      }
    }, err => {
      this.cm.devLog('err', err);

    });
    this.commonModalService.openAlertDialog('삭제', '정말 삭제 하시겠습니까?', true, outer);
  }

  getMaxItfOrder(items) {
    let maxItfOrder = 0;
    if (items && items.length > 0) {
      for (const item of items) {
        const itf_order = item.itf_order;
        if (maxItfOrder < itf_order) {
          maxItfOrder = itf_order;
        }
      }
    }
    return maxItfOrder;
  }
}

