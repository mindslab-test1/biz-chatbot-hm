import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Cm_domainApi } from '../../../../../adminsdk/services/custom';
import { UserService } from '../../../../../service/user.service';
import { CommonModalService } from '../../../../../service';
import { CommonUtilService } from '../../../../../service';
import { Itf_prebqa } from '../../../../../adminsdk/models';
import { Itf_prebqaApi } from '../../../../../adminsdk/services/custom';

@Component({
  selector: 'app-pre-edit',
  templateUrl: './pre-edit.component.html',
  styleUrls: ['./pre-edit.component.scss']
})
export class PreEditComponent implements OnInit {
  editFlag = false;
  editForm: FormGroup;
  itf_prebqa: Itf_prebqa;

  @Output() editRemove = new EventEmitter<Boolean>();
  @ViewChild('patternInput') patternInput: ElementRef;
  constructor(
    private fb: FormBuilder,
    private cmDomainApi: Cm_domainApi,
    private userSerivce: UserService,
    private itf_prebqaApi: Itf_prebqaApi,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService
  ) { }

  ngOnInit() {
  }

  preEditInit(itf_prebqa: Itf_prebqa) {
    this.editForm = this.fb.group({
      pattern: [itf_prebqa.pattern, [Validators.required]]
    });

    setTimeout(() => this.patternInput.nativeElement.focus(), 0);
  }

  public scfEditOpen(itf_prebqa: Itf_prebqa) {
    this.editFlag = true;
    this.itf_prebqa = itf_prebqa;
    this.preEditInit(itf_prebqa);
  }

  valid(text) {
    try {
      RegExp(text);
      return true;
    } catch (e) {
      alert(e);
      return false;
    }
  }

  onEditSubmit(value) {

    this.itf_prebqa.pattern = value.pattern;

    if (this.valid(value.pattern)) {
      this.itf_prebqaApi.upsert(this.itf_prebqa).subscribe(res => {
        // this.cm.devLog(res);
      }, err => {
        this.commonModalService.openErrorAlert(err);
      }, () => {
        this.commonModalService.openAlertDialog('Success', '수정 성공', false, null);
        this.editFlag = false;
        this.editRemove.emit(true);
      });

    } else {
      this.commonModalService.openErrorAlert('올바른 정규표현식이 아닙니다.');
    }
  }
  editClose() {
    this.editFlag = false;
  }
}
