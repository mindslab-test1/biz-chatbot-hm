import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Cm_domainApi } from '../../../../../adminsdk/services/custom';
import { UserService } from '../../../../../service/user.service';
import { CommonModalService } from '../../../../../service';
import { CommonUtilService } from '../../../../../service';
import { Itf_prebqa } from '../../../../../adminsdk/models';
import { Itf_prebqaApi } from '../../../../../adminsdk/services/custom';

@Component({
  selector: 'app-pre-add',
  templateUrl: './pre-add.component.html',
  styleUrls: ['./pre-add.component.scss']
})
export class PreAddComponent implements OnInit {
  addFlag = false;
  addForm: FormGroup;
  itf_prebqa: Itf_prebqa;

  @Output() addRemove = new EventEmitter<Boolean>();
  @ViewChild('patternInput') patternInput: ElementRef;
  constructor(
    private fb: FormBuilder,
    private cmDomainApi: Cm_domainApi,
    private userSerivce: UserService,
    private itf_prebqaApi: Itf_prebqaApi,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService
  ) { }

  ngOnInit() {
  }

  preAddInit(itf_prebqa: Itf_prebqa) {
    this.addForm = this.fb.group({
      pattern: ['', [Validators.required]]
    });

    setTimeout(() => this.patternInput.nativeElement.focus(), 0);
  }

  public scfAddOpen(itf_prebqa: Itf_prebqa) {
    this.addFlag = true;
    this.itf_prebqa = itf_prebqa;
    this.preAddInit(itf_prebqa);
  }

  valid(text) {
    try {
      RegExp(text);
      return true;
    } catch (e) {
      alert(e);
      return false;
    }
  }

  onAddSubmit(value) {

    this.itf_prebqa.pattern = value.pattern;

    if (this.valid(value.pattern)) {
      this.itf_prebqaApi.upsert(this.itf_prebqa).subscribe(res => {
        // this.cm.devLog(res);
      }, err => {
        this.commonModalService.openErrorAlert(err);
      }, () => {
        this.commonModalService.openAlertDialog('Success', '추가 성공', false, null);
        this.addFlag = false;
        this.addRemove.emit(true);
      });

    } else {
      this.commonModalService.openErrorAlert('올바른 정규표현식이 아닙니다.');
    }
  }
  addClose() {
    this.addFlag = false;
  }
}
