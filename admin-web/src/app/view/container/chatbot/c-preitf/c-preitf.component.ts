import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonModalService, CommonUtilService} from '../../../../service';
import {DataService} from '../../../../service/data.service';
import {UserService} from '../../../../service/user.service';
import {Chatbot_config, Chatbot_info, Cm_user, Itf_prebqa } from '../../../../adminsdk';
import {ChatbotUpsertService} from '../../../../service/chatbot-upsert.service';
import {Subscription} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {ValidatorService} from '../../../../service/validator.service';
import {ChatbotInfoService} from '../../../../service/chatbot-info.service';
import {lang, Msg} from '../../../../model/message';
import {DateService} from '../../../../service/date.service';
import {PreClassifierComponent} from './pre-classifiler/pre-classifier.component';

@Component({
  selector: 'app-c-preitf',
  templateUrl: './c-preitf.component.html',
  styleUrls: [
    './../chatbot-container.component.scss'
    , './c-preitf.component.scss'
  ]
})
export class CPreitfComponent implements OnInit, OnDestroy {
  private _isSave = true;
  private _data: Chatbot_info;
  config: Chatbot_config = new Chatbot_config();
  subscription: Subscription;
  c = this.service.constants;
  d = this.chatbotInfoService.default;
  currPath = this.c.URL.c_preitf;
  private _bot_id = this.d.botId;

  private _title = '';

  userId = this.userService.getCurrentUserId();
  currUserInfo: Cm_user = this.userService.getCachedUserInfo();

  @ViewChild('preitfModal') preitfModal: PreClassifierComponent;
  constructor(
    private router: Router
    , private activatedRoute: ActivatedRoute
    , private commonModalService: CommonModalService
    , private cm: CommonUtilService
    , private ds: DataService
    , private dateService: DateService
    , private userService: UserService
    , private service: ChatbotUpsertService
    , private chatbotInfoService: ChatbotInfoService
    , private validator: ValidatorService
  ) {
  }


  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.cm.devLog('unsubscribe at destroy on container');
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
    this.ds.clearData();
  }

  init() {
    this.setSubscription();
    this.activatedRoute.params.subscribe(params => {
      if (params.bot_id) {
        this.cm.devLog('\n====\n' + params.bot_id + '\n====\n\n');
        this.bot_id = params.bot_id;
        this.getBotData();
        // this.getSdsData();
      } else {
        this.cm.devLog('\n====\ninsert\n====\n\n');
      }
    });
  }


  setSubscription() {
    this.subscription = this.ds.getData().subscribe(res => {
      if (!res) {
        return false;
      }
      const _resolveSave = (data) => {
        this.save();
      };
      const _resolveIsSave = (data) => {
        this.isSave = data.isSave;
      };
      const _resolveTitle = (data) => {
        if (!data.title) {
          // this.cm.isDev() && console.log('empty title')
        }
        this.title = data.title;
      };

      switch (res.cmd) {
        case this.ds.CMD.SAVE:
          _resolveSave(res.data);
          break;
        case this.ds.CMD.IS_SAVE:
          _resolveIsSave(res.data);
          break;
        case this.ds.CMD.TITLE:
          _resolveTitle(res.data);
          break;
        default:
          break;
      }
    }, err => {
      this.cm.devLog('err', err);
    });
  }

  @Input() set bot_id(v) {
    if (v === this.d.botId) {
      throw new Error('invalid bot_id');
    }
    this._bot_id = v;
  }
  get bot_id() {
    return this._bot_id;
  }

  set isSave(isSave: boolean) {
    if (this._isSave === isSave) {
      return;
    }
    this._isSave = isSave;
    this.ds.sendData(this.ds.CMD.IS_SAVE, { isSave });
  }
  get isSave() {
    return this._isSave;
  }

  set title(v) {
    if (this._title === v) {
      return;
    }
    this._title = v;
  }
  get title() {
    return this._title;
  }
  set data(v: Chatbot_info) {
    this._data = v;
  }
  get data() {
    return this._data;
  }

  addPattern() {

    const itf_prebqa = new Itf_prebqa();
    itf_prebqa.bot_id = this.bot_id;
    itf_prebqa.creator_id = this.userId;
    itf_prebqa.updator_id = this.userId;

    this.preitfModal.addModalOpen(itf_prebqa);
  }

  getBotData() {
    this.isSave = true;
    this.service.getBotData(this.bot_id, (res: Chatbot_info) => {
      // filter
      if (this.cm.isEmpty(res)) {
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        return false;
      }
      if (this.cm.isEmpty(res.chatbotConfigs)) {
        this.commonModalService.openErrorAlert(`${Msg.err[lang].failLoadData} : chatbotConfigs`);
        return false;
      }

      // this.cm.isDev() && console.log('res', res);

      this.data = res;
      // set title
      this.title = res.bot_name;

      this.config = res.chatbotConfigs[0];
      this.ds.sendData(this.ds.CMD.GET, this.data);
    }, err => {
      this.ignoreIsSaveAndGoToList();
    }, () => {
      this.preitfModal.init(this.bot_id);
    });
  }
  private ignoreIsSaveAndGoToList() {
    this.isSave = false;
    let i = 0;
    const close = setInterval(() => {
      this.isSave = true;
      if (this.isSave && i > 0) {
        clearInterval(close);
        this.commonModalService.openErrorAlert(Msg.err[lang].invalidAccess);
        this.router.navigate([environment.container, this.c.URL.chatbot_management]);
      } else {
        i++;
        this.cm.devLog('interval close');
      }
    }, 15);
  }

  getCurrConfig(): Chatbot_config {
    if (this.cm.isEmpty(this.data.chatbotConfigs)) {
      return this.config;
    }
    return this.cm.prop(this.data.chatbotConfigs[0], this.config);
  }
  setData() {
    this.data.bot_name = this.title;
    this.data.team_id = this.userService.getTeamId();
    this.data.updated_dtm = this.dateService.getNowDate();
    this.data.updator_id = this.userId;
  }
  save() {
    if (this.service.isUpdate(this.bot_id)) {
      this.data.bot_id = this.bot_id;
      // this.setConfig();

      this.data.chatbotConfigs = [this.getCurrConfig()];
      this.setData();
      if (!this.validate()) {
        return false;
      }
    } else {
      this.data = new Chatbot_info();
      this.data.use_yn = this.c.USE_YN.Y;
      this.data.created_dtm = this.dateService.getNowDate();
      this.data.creator_id = this.userId;

      this.data.chatbotConfigs = [this.getCurrConfig()];

      this.setData();
      if (!this.validate()) {
        return false;
      }
      this.service.insertBot(this.data, (res) => {
        this.isSave = true;
        this.commonModalService.openSuccessAlert(Msg.com[lang].succCreateData);

        const _setUpdate = (res2: Chatbot_info) => {
          this.bot_id = res2.bot_id;
          this.router.navigate([environment.container, 'chatbot', this.currPath, this.bot_id]);
        };
        _setUpdate(res);
      });
    }
  }
  validate() {
    if (this.validator.validate(this.data, {
      bot_name: {
        required: true,
        maxLength: 128
      }
    })) {
      return true;
    }
    return false;
  }
}
