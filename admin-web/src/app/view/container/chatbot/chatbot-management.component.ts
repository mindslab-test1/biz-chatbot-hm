import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { CommonModalService, CommonUtilService } from '../../../service';
import { environment as env } from '../../../environments/environment';
import { Chatbot_infoApi, Chatbot_info, LoopBackFilter, Cm_user } from '../../../adminsdk';
import { Paginator } from '../../../common/paginator/paginator.component';
import { CommonModalComponent } from '../../../common';
import { UserService } from '../../../service/user.service';
import { ChatbotUpsertService } from '../../../service/chatbot-upsert.service';
import { Msg, lang } from '../../../model/message';
import { DateService } from '../../../service/date.service';
import { ChatbotInfoService } from '../../../service/chatbot-info.service';
import { DataService } from '../../../service/data.service';

const _ = env._;

interface OptionList {
  name: string;
  code: number;
}
@Component({
  selector: 'app-chatbot-management',
  templateUrl: './chatbot-management.component.html',
  styleUrls: ['./chatbot-management.component.scss']
})
export class ChatbotManagementComponent implements OnInit, OnDestroy {

  constructor(
      private router: Router
    , private ds: DataService
    , private dateService: DateService
    , private userService: UserService
    , private api: Chatbot_infoApi
    , private commonModalService: CommonModalService
    , private cm: CommonUtilService
    , private service: ChatbotUpsertService
    , private chatbotInfoService: ChatbotInfoService
  ) {
  }
  ngOnInit() {
    this.init();
  }
  ngOnDestroy() {
    this.ds.clearData();
  }

  c = this.service.constants;
  si = this.chatbotInfoService.set;

  private _isWorking: boolean = false;
  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if(this._isWorking == v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }

  container_chatbot_img = this.si.container_chatbot_img;

  paginator = new Paginator();
  private _defaultPageSize: string = '9';
  selectPageSize: string = this._defaultPageSize;
  searchStr = '';
  searchOption: LoopBackFilter = {};

  items: Chatbot_info[];
  selectedItem: Chatbot_info;

  sampleItems: Chatbot_info[];
  selectedSampleItems: Chatbot_info[] = [];

  title = '챗봇';
  primaryKey='bot_id';

  userId = this.userService.getCurrentUserId();
  currUserInfo: Cm_user = this.userService.getCachedUserInfo();

  sdsModels: any;
  qaModels: any;
  isAdd = false;

  init(){
    this.initList();
  }
  initList(){
    if(this.paginator.pageSize != this.getCurrPageSize()) {
      this.setPageSize(this.getCurrPageSize());
    }
    this.isWorking = true;
    this.getCount(()=>{
      this.isWorking = false;
    });
  }

  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    if(tempP == JSON.stringify(this.paginator)
      && this.paginator.totalPages != 1
    ){
      //갱신된 값이 없으면 다시 그리지 않음.
      return true;
    }
    if(this.paginator.totalCount <= 0) {
      return true;
    }
    // console.log('debug temp onPaginator getList',this.paginator.pageSize);
    this.getList();
  }

  getCurrPageSize() {
    return Number(this.selectPageSize);
  }

  @ViewChild('listWrap') listWrap: ElementRef;
  calcPageSize(): number {
    const defaultPageSize = Number(this._defaultPageSize);
    let setPage = defaultPageSize;
    let wrapWidth = 0;
    wrapWidth = this.listWrap.nativeElement.clientWidth;
    const LIST_ITEM_SIZE = 290;
    // this.cm.isDev() && console.log('calcPageSize', wrapWidth, setPage);

    const LIST_COL_CNT_CALC_START = 4;
    if(wrapWidth < LIST_ITEM_SIZE*LIST_COL_CNT_CALC_START) {
      return setPage;
    }
    const LIST_ROW_CNT_EXPECT = 3;
    setPage = Math.floor(wrapWidth/LIST_ITEM_SIZE) * LIST_ROW_CNT_EXPECT;
    return setPage;
  }

  getFilter(): LoopBackFilter {
    /* 추가 검색 조건을 위해 and 를 만듦. */
    const filter: LoopBackFilter = {
      where: {
        team_id: this.userService.getTeamId(),
        and: [
          {
            bot_name: {
              neq: "sdsModelTest"
            }
          }, {
            use_yn: {
              neq: 'N'
            }
          }
        ],
      },
      skip: this.searchOption.skip,
      limit: this.searchOption.limit,
      order: `${this.primaryKey} asc`
    };


    if(this.cm.isNotEmpty(this.searchStr)){
      filter.where.and.push({
        bot_name: {
          like: '%' + this.searchStr + '%'
        }
      });
    }
    return filter;
  };
  getCount(callback?) {
    const filter = this.getFilter();

    this.api.count(filter.where).subscribe(res=>{
      //filter
      if(this.cm.isEmpty(res)) {
        this.isWorking = false;
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        return false;
      }
      if(res.count <= 0) {
        this.cm.isDev() && console.log('no data');
        this._setItems([]);
        this.isWorking = false;
        this.setTotalCount(this.items.length);
        return false;
      }

      //logic
      const count = res.count;
      this.setTotalCount(count);


      callback && callback();


    }, err=>{
      this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  }
  private getList() {
    this.isWorking = true;
    this.getCount(()=>{
      if(this.paginator.totalCount <= 0) {
        this._setItems([]);
        this.isWorking = false;
        return false;
      }
      this._getList();
    });
  }
  private _getList = (callback = (): void=>{}) => {
    this._setItems([]);

    const calc = this.calcPageSize();
    this.setPageSize(calc);
    // const calc = this.calcPageSize();
    // const curr = this.getCurrPageSize();
    // if(calc != curr){
    //   this.setPageSize(calc);
    // }

    this.setSearchOption();
    const filter = this.getFilter();
    const data = filter;
    this.api.search(data).subscribe((res)=>{
      if(res){
        this._setItems(res);
        this.isWorking = false;
        callback();
      }
      this.isWorking = false;
    }, err=>{
      this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  };
  _setItems = (result:Chatbot_info[])=>{
    this.items = result;
  };


  setTotalCount(count: number){
    this.paginator.totalCount = count;
  }
  setPageSize(pageSize: number){
    // console.log('debug temp setPageSize',pageSize);
    this.paginator.pageSize = pageSize;
  }

  onSearch() {
    this.getList();
  }
  onSearchKey(event) {
    // this.cm.isDev() && console.log('debug search key event',event);
    if (event.keyCode === 13) {
      this.onSearch();
    } else {
      // this.getList();

      // globalFilter 적용시 아래 코드 이용.
      // if(this.cm.isEmpty(this.searchStr) || this.cm.isEmpty(this.items)){
      //   this.getList();
      // } else {
      //   this.setTotalCount(this.items.length);
      // }
    }
  }

  public navAddChatbot() {
    this.router.navigate([env.container,this.c.URL.chatbot_container,this.c.URL.chat_base]);
  }














  // view item upsert ====================================================


  @ViewChild('insertSampleModal')
  insertSampleModal: CommonModalComponent;
  // @ViewChild('upsertModal')
  // upsertModal: CommonModalComponent;
  @ViewChild('removeModal')
  removeModal: CommonModalComponent;
  // @ViewChild('testModal')
  // testModal: CommonModalComponent;
  @ViewChild('linkModal')
  linkModal: CommonModalComponent;

  sampleModelAddOpen() {
    const team_id = this.userService.getTeamId();
    this.sampleItems = [
      new Chatbot_info({team_id,bot_name:"튜토리얼봇",description:"descript",use_yn:"Y",img_path:""}),
      new Chatbot_info({team_id,bot_name:"튜토리얼봇2",description:"descript2",use_yn:"Y",img_path:""})
    ];
    this.insertSampleModal.modalOpen();
  }
  sampleModelClose() {
    this.insertSampleModal.modalClose();
  }
  selectSampleItem(event, entity: Chatbot_info) {
    if(this.selectedSampleItems.length>1){
      this.selectedSampleItems = [];
      this.selectedSampleItems.push(entity);
    }
    this.cm.isDev() && console.log(this.selectedSampleItems);
  }
  addSave(event, entity: Chatbot_info) {
    this._addSave(entity);
  }
  private _addSave(entity: Chatbot_info) {
    if(this.cm.isEmpty(entity)){
      this.commonModalService.openErrorAlert('선택된 아이템이 없습니다.');
      return;
    }
    this._save(entity, ()=>{
      this._saveDone();
    });
  }
  sampleModelAddSave() {
    if(this.cm.isEmpty(this.selectedSampleItems)){
      this.commonModalService.openErrorAlert(`샘플 ${this.title}을 선택해주십시오.`);
      return false;
    }
    const lastIdx = this.selectedSampleItems.length - 1;
    this.selectedSampleItems.map((item: Chatbot_info, idx)=>{
      this._save(item, ()=>{
        if((idx == lastIdx)) {
          this._saveDone();
        }
      });
    });
  }
  private _save = (item: Chatbot_info, callback = ():void=>{})=>{
    //console.log('test');
    this.service.insertBot(this.setItem(item),(res: Chatbot_info)=>{
      addOptionalData(res);
    });
    const addOptionalData = (res)=>{
      this.service.getBotData(res.bot_id, (_res)=>{
        item = this.cm.prop(item,_res);
        item.chatbotConfigs[0].domains.map(domain=>domain['conf_id'] = _res.chatbotConfigs[0].conf_id);
        item.chatbotConfigs[0].sdsDomains.map(domain=>domain.conf_id = _res.chatbotConfigs[0].conf_id);
        //console.log('item',item);

        this.service.updateBot(this.setItem(item),()=>{
          callback();
        });
      }, err=>{
        this.commonModalService.openErrorAlert(Msg.err[lang].invalidAccess);
      });
    };
  };
  private _saveDone = ()=>{
    alert('Created.');
    this.getList();
    this.insertSampleModal.modalClose();
  };

  // @ViewChild('modelContext')
  // modelContext: ElementRef;

  // @ViewChild('contextBackground')
  // contextBackground: ElementRef;
  // openContextBackground() {
  //   this.modelContext.nativeElement.style.display = '';
  //   this.contextBackground.nativeElement.style.display = '';
  // }
  // closeContextBackground() {
  //   this.modelContext.nativeElement.style.display = 'none';
  //   this.contextBackground.nativeElement.style.display = 'none';
  // }
  // openContext(event, chatBot: Chatbot_info) {
  //   this.cm.isDev() && console.log('event', event, '|| context', chatBot);
  //   this.modelContext.nativeElement.style.left = `${event.pageX-156}px`;
  //   this.modelContext.nativeElement.style.top = `${event.pageY}px`;
  //   this.openContextBackground();

  //   this.selectedItem = chatBot;
  // }
  // closeContext() {
  //   this.closeContextBackground();

  //   this.selectedItem = null;
  // }
  // selectContextMenu(){
  //   this.closeContextBackground();
  // }

  setItem(data: Chatbot_info){
    const now = this.dateService.getNowDate();
    const item = data;
    item.created_dtm = now;
    item.creator_id = this.userId;
    item.updated_dtm = now;
    item.updator_id = this.userId;
    return item;
  }
  modelDuplicateOpen(chatBot: Chatbot_info) {
    let dupData: Chatbot_info = _.cloneDeep(chatBot);
    const DUP_SUFFIX = '_사본';
    delete dupData.bot_id;
    dupData.bot_name += DUP_SUFFIX;
    delete dupData.chatbotConfigs[0].bot_id;
    delete dupData.chatbotConfigs[0].conf_id;
    dupData.chatbotMsgs.map(msg=>delete msg.msg_id);
    dupData.chatbotConfigs[0].domains.map(domain=>delete domain.conf_id);
    dupData.chatbotConfigs[0].sdsDomains.map(domain=>delete domain.conf_id);

    // this.selectContextMenu();
    this._addSave(dupData);
    this.cm.isDev() && console.log('dup');

  }

  setSearchOption(){
    this.searchOption.skip = this.paginator.pageIndex * this.paginator.pageSize;
    this.searchOption.limit = this.paginator.pageSize;
  }
  public modelSettingOpen(entity: Chatbot_info) {
    this.router.navigate([env.container,'chatbot','chat-base',entity.bot_id]);
  }

  linkModalOpen() {
    this.linkModal.modalOpen();
  }
  linkModalClose() {
    this.linkModal.modalClose();
  }

  modelLogOpen(entity: Chatbot_info = this.selectedItem) {
    const dialogDetailPath = [this.c.URL.dialogs_container,
    this.c.URL.dialogs_log, this.c.URL.dialogs_log_detail];
    this.router.navigate([env.container,...dialogDetailPath
      ,{search: JSON.stringify({bot_id: entity.bot_id})}]);
  }

  modelRemoveOpen(entity: Chatbot_info) {
    // this.selectContextMenu();
    if (this.cm.isEmpty(entity)) {
      this.cm.isDev() && console.log('선택된 아이템이 없음.');
      return false;
    }
    this.selectedItem = entity;
    this.removeModal.modalOpen();
  }

  modelRemoveSave() {
    if(!this.selectedItem) {
      this.cm.isDev() && console.log('선택된 아이템이 없음.');
      return false;
    }
    this.service.deleteBot(this.selectedItem, ()=>{
      this.getList();
      this.removeModalClose();
    });
    // this.api.deleteById(this.selectedItem.bot_id).subscribe(res => {
    //   this.getList();
    //   this.removeModalClose();
    // }, error => {
    //   this.commonModalService.openErrorAlert(error);
    // });
  }
  removeModalClose() {
    this.removeModal.modalClose();
    this.selectedItem = null;
  }
  public uploadFile(event: any) {
  }

}
