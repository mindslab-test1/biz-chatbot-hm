import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Chatbot_buttons, Chatbot_buttonsApi, Chatbot_config, Chatbot_configApi, Chatbot_info, Chatbot_msg, Chatbot_rich_content, Chatbot_rich_contentApi, Chatbot_rich_image, Chatbot_rich_imageApi, Cm_user} from '../../../../adminsdk';
import {CommonModalService, CommonUtilService} from '../../../../service';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../../environments/environment';
import {ChatbotUpsertService} from '../../../../service/chatbot-upsert.service';
import {FormBuilder} from '@angular/forms';
import {DataService} from '../../../../service/data.service';
import {Subscription} from 'rxjs';
import {FileService} from '../../../../service/file.service';
import {UserService} from '../../../../service/user.service';
import {ValidatorService} from '../../../../service/validator.service';
import {ChatbotInfoService} from '../../../../service/chatbot-info.service';
import {lang, Msg} from '../../../../model/message';
import {DateService} from '../../../../service/date.service';


@Component({
  selector: 'app-chat-base',
  templateUrl: './chat-base.component.html',
  styleUrls: ['./chat-base.component.scss']
})
export class ChatBaseComponent implements OnInit, OnDestroy {

  userId = this.userService.getCurrentUserId();
  currUserInfo: Cm_user = this.userService.getCachedUserInfo();
  subscription: Subscription;
  c = this.service.constants;
  di = this.chatbotInfoService.default;
  si = this.chatbotInfoService.set;
  currPath = this.c.URL.chat_base;
  private _bot_id = this.di.botId;
  private _isSave = true;
  private _title = '';
  private _data: Chatbot_info;
  private _description: string;

  private _img_path: string = this.di.img_path;
  private _thumb_img_path: string = this.di.img_path;

  @ViewChild('img_input')
  img_input: ElementRef;
  @ViewChild('thumb_img_input')
  thumb_img_input: ElementRef;

  maxSize = this.si.max_size;
  maxSizeView = this.si.max_size_view;
  thumbnailSize = this.si.max_size_view;
  private _file;
  private _thumbFile;

  loading = false;
  isSaveMsg = true;
  isSaveGreetingMsg = true;
  isSaveBtn = true;
  isSaveUnknownMsg = true;
  isSaveGreetingBtn = true;
  isSaveUnknownBtn = true;

  unknownMessage: string;
  _unknownMessages: Chatbot_msg[] = [];
  greetingMessage: string;
  _greetingMessages: Chatbot_msg[] = [];
  @ViewChild('greetingMessagesEle')
  greetingMessagesEle: ElementRef;
  @ViewChild('unknownMessagesEle')
  unknownMessagesEle: ElementRef;

  greetingButton: string;
  greetingButtonContent: string;
  _greetingButtons: Chatbot_buttons[] = [];
  unknownButton: string;
  unknownButtonContent: string;
  _unknownButtons: Chatbot_buttons[] = [];

  @ViewChild('greetingButtonsEle')
  greetingButtonsEle: ElementRef;
  @ViewChild('unknownButtonsEle')
  unknownButtonsEle: ElementRef;

  greetingButtonIsHide = true;
  unknownButtonIsHide = true;

  private _default_notice_img_path = '../../../../../assets/images/noimage.png';
  notice1ImageIsHide = true;
  notice2ImageIsHide = true;
  notice3ImageIsHide = true;
  notice4ImageIsHide = true;

  _notice1_img_path = this._default_notice_img_path;
  _notice2_img_path = this._default_notice_img_path;
  _notice3_img_path = this._default_notice_img_path;
  _notice4_img_path = this._default_notice_img_path;

  _notice1Img: Chatbot_rich_image = new Chatbot_rich_image();
  _notice2Img: Chatbot_rich_image = new Chatbot_rich_image();
  _notice3Img: Chatbot_rich_image = new Chatbot_rich_image();
  _notice4Img: Chatbot_rich_image = new Chatbot_rich_image();

  _notice1_img_file;
  _notice2_img_file;
  _notice3_img_file;
  _notice4_img_file;

  @ViewChild('notice1_img_input')
  notice1_img_input: ElementRef;
  @ViewChild('notice2_img_input')
  notice2_img_input: ElementRef;
  @ViewChild('notice3_img_input')
  notice3_img_input: ElementRef;
  @ViewChild('notice4_img_input')
  notice4_img_input: ElementRef;

  notice1_use_yn: string;
  notice2_use_yn: string;
  notice3_use_yn: string;
  notice4_use_yn: string;

  synonym_yn: string;

  constructor(
      private router: Router
      , private activatedRoute: ActivatedRoute
      , private commonModalService: CommonModalService
      , private cm: CommonUtilService
      , private ds: DataService
      , private dateService: DateService
      , private userService: UserService
      , private configApi: Chatbot_configApi
      , private contentApi: Chatbot_rich_contentApi
      , private btnApi: Chatbot_buttonsApi
      , private imageApi: Chatbot_rich_imageApi
      , private chatbotInfoService: ChatbotInfoService
      , private service: ChatbotUpsertService
      , private fb: FormBuilder
      , private fileService: FileService
      , private validator: ValidatorService
  ) {
  }

  ngOnInit() {
    this.init();
  }
  ngOnDestroy() {
    this.cm.devLog('unsubscribe at destroy on container');
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
    this.ds.clearData();
  }
  init() {
    this.initEvents();
    this.setSubscription();
    this.activatedRoute.params.subscribe(params => {
      if (params.bot_id) {
        this.cm.devLog('\n====\n' + params.bot_id + '\n====\n\n');
        this.bot_id = params.bot_id;
        this.getBotData();
      } else {
        this.cm.devLog('\n====\ninsert\n====\n\n');
        // this.ds.sendData(this.ds.CMD.INSERT,{});
      }
    });
  }
  setSubscription() {
    this.subscription = this.ds.getData().subscribe(res => {
      if (!res) {
        return false;
      }
      const _resolveSave = (data) => {

        const url = data.url || '';
        this.save(url);
      };
      const _resolveIsSave = (data) => {
        this.isSave = data.isSave;
      };
      const _resolveTitle = (data) => {
        if (!data.title) {
          // this.cm.devLog('empty title')
        }
        this.title = data.title;
      };

      // this.cm.devLog('res',res);
      const {cmd, data} = res;
      switch (cmd) {
          // case this.ds.CMD.INSERT:
          //   _resolveInsert(data);
          //   break;
          // case this.ds.CMD.GET:
          //   _resolveGet(data);
          //   break;
        case this.ds.CMD.SAVE:
          _resolveSave(data);
          break;
        case this.ds.CMD.IS_SAVE:
          _resolveIsSave(data);
          break;
        case this.ds.CMD.TITLE:
          _resolveTitle(data);
          break;
        default:
          // this.cm.devLog('미처리 cmd');
          break;
      }
    }, err => {
      this.cm.devLog('err', err);
    });
  }



  @Input() set bot_id(v) {
    if (v === this.di.botId) {
      throw new Error('invalid bot_id');
    }
    this._bot_id = v;
  }
  get bot_id() {
    return this._bot_id;
  }

  set isSave(isSave: boolean) {
    if (this._isSave === isSave) {
      return;
    }
    this._isSave = isSave;
    this.ds.sendData(this.ds.CMD.IS_SAVE, {isSave});
  }
  get isSave() {
    return this._isSave;
  }

  set title(v) {
    if (this._title === v) {
      return;
    }
    this._title = v;
  }
  get title() {
    return this._title;
  }

  set data(v: Chatbot_info) {
    this._data = v;
  }
  get data() {
    return this._data;
  }


  getBotData() {
    // this.cm.devLog('set bot_id',this.bot_id);
    this.service.getBotData(this.bot_id, (res: Chatbot_info) => {
      // filter
      if (this.cm.isEmpty(res)) {
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        return false;
      }
      if (this.cm.isEmpty(res.chatbotConfigs)) {
        this.commonModalService.openErrorAlert(`${Msg.err[lang].failLoadData} : chatbotConfigs`);
        return false;
      }

      // this.cm.devLog('res',res);
      this.data = res;

      // set title
      this.title = res.bot_name;

      // set descript
      this.description = res.description;

      // set img_path
      if (res.img_path) {
        this.img_path = this.chatbotInfoService.getImgPath(res.img_path);
      }

      // set message
      const msgList = res.chatbotMsgs;
      const greetings = this.cm.filter(msgList, 'msg_type_cd', this.c.MSG_TYPE_CD.GREETING);
      const unknowns = this.cm.filter(msgList, 'msg_type_cd', this.c.MSG_TYPE_CD.UNKNOWN);
      const config: Chatbot_config = res.chatbotConfigs[0];
      this.synonym_yn = config.synonym_yn;
      this.greetingMessages = greetings;
      this.unknownMessages = unknowns;
      this.greetingButtons = this._filterGreetingRichContent(config).buttons;
      this.unknownButtons = this._filterUnknownRichContent(config).buttons;

      // 공지사항 이미지
      this.loadNoticeImage(config, this.c.NOTICE_OPTION.NOTICE1);
      this.notice1_use_yn = config.notice1_use_yn;
      this.notice1ImageIsHide = !this.notice1UseYn;

      this.loadNoticeImage(config, this.c.NOTICE_OPTION.NOTICE2);
      this.notice2_use_yn = config.notice2_use_yn;
      this.notice2ImageIsHide = !this.notice2UseYn;

      this.loadNoticeImage(config, this.c.NOTICE_OPTION.NOTICE3);
      this.notice3_use_yn = config.notice3_use_yn;
      this.notice3ImageIsHide = !this.notice3UseYn;

      this.loadNoticeImage(config, this.c.NOTICE_OPTION.NOTICE4);
      this.notice4_use_yn = config.notice4_use_yn;
      this.notice4ImageIsHide = !this.notice4UseYn;

      this.isSaveMsg = true;
      this.isSaveBtn = true;

      this.ds.sendData(this.ds.CMD.GET, this.data);
    }, err => {
      this.ignoreIsSaveAndGoToList();
    });
  }
  private ignoreIsSaveAndGoToList() {
    this.isSave = false;
    let i = 0;
    const close = setInterval(() => {
      this.isSave = true;
      if (this.isSave && i > 0) {
        clearInterval(close);
        this.commonModalService.openErrorAlert(Msg.err[lang].invalidAccess);
        this.router.navigate([environment.container, this.c.URL.chatbot_management]);
      } else {
        i++;
        this.cm.devLog('interval close');
      }
    }, 15);
  }


  set description(v) {
    this.isSave = false;
    this._description = v;
  }
  get description() {
    return this._description;
  }

  set img_path(v) {
    this.isSave = false;
    this._img_path = v;
  }
  get img_path() {
    return this._img_path;
  }

  set thumb_img_path(v) {
    this.isSave = false;
    this._thumb_img_path = v;
  }
  get thumb_img_path() {
    return this._thumb_img_path;
  }


  initEvents() {
    this.thumb_img_input.nativeElement.onload = () => {

      // 썸네일 이미지 보여주기
      if (this.img_path === this.di.img_path) {
        return true;
      }

      const thumbEle = this.thumb_img_input.nativeElement;
      const _width = thumbEle.naturalWidth;
      const _height = thumbEle.naturalHeight;
      if (this.chatbotInfoService.isNotRequireResize(_width, _height, this.thumbnailSize)) {
        this.thumbFile = this.file;
        return;
      }

      const {width, height} = this.chatbotInfoService.calcResizeWH(_width, _height, this.thumbnailSize);

      thumbEle.width = width;
      thumbEle.height = height;
      const _setThumbnailFile = () => {

        const c = document.createElement('canvas');
        const ctx = c.getContext('2d');
        c.width = width;     // update canvas size to match image
        c.height = height;
        // draw in image
        ctx.drawImage(thumbEle, 0, 0, width, height);
        // get content as JPEG blob
        c.toBlob((blob) => {
          // here the image is a blob
          this.thumbFile = blob;
        }, 'image/jpeg', 1);
      };
      _setThumbnailFile();
    };
    this.img_input.nativeElement.onload = () => {
      if (this.img_path === this.di.img_path) {
        return true;
      }

      const _width = this.img_input.nativeElement.naturalWidth;
      const _height = this.img_input.nativeElement.naturalHeight;

      const MAX_SIZE = this.maxSize;

      /// validate
      if (_width > MAX_SIZE || _height > MAX_SIZE) {
        this.file = null;
        this._img_path = this.di.img_path;
        this.commonModalService.openErrorAlert('삽입 불가능한 이미지 입니다.');
        return true;
      }

      const {width, height} = this.chatbotInfoService.calcResizeWH(_width, _height, this.maxSizeView);
      this.img_input.nativeElement.width = width;
      this.img_input.nativeElement.height = height;

      // const _setFile = ()=>{
      //   if(this.cm.isEmpty(this.file)){
      //     return ;
      //   }

      //   const file = this.file;
      //   /* reactive form에서 input[type="file"]을 지원하지 않는다.
      //     즉 파일 선택 시에 값이 폼컨트롤에 set되지 않는다
      //     https://github.com/angular/angular.io/issues/3466
      //     form validation을 위해 file.name을 폼컨트롤에 set한다. */

      //   // this.file = file;
      // };
      // _setFile();
    };
  }
  onFileChange(event, fileInput: HTMLInputElement) {
    const files: FileList = fileInput.files;
    if (this.cm.isEmpty(files)) {
      console.log('no file');
      return;
    }
    // For Preview
    const file = files[0];
    const reader = new FileReader();

    /* 브라우저는 보안 문제로 인해 파일 경로의 참조를 허용하지 않는다.
      따라서 파일 경로를 img 태그에 바인딩할 수 없다.
      FileReader.readAsDataURL 메소드를 사용하여 이미지 파일을 읽어
      base64 인코딩된 스트링 데이터를 취득한 후, img 태그에 바인딩한다. */
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.img_path = reader.result;
      console.log('read file');
      this.file = file;

      this.thumb_img_path = reader.result;

    };
  }
  private set file(file) {
    console.log('set file');
    this._file = file;
  }
  private get file() {
    return this._file;
  }
  private set thumbFile(thumbFile) {
    console.log('set thumbFile');
    this._thumbFile = thumbFile;
  }
  private get thumbFile() {
    return this._thumbFile;
  }

  // TODO loading 이 true 일 때 저장 버튼을 비활성화하여 업로드를 방해하지 않도록?
  // [disabled]="form.invalid || loading">
  // Save
  // <i class="fa fa-spinner fa-spin fa-fw" *ngIf="loading"></i>

  uploadChatbotImgFile(callback) {
    // this.cm.devLog('uploadChatbotImgFile');
    // this.cm.devLog('file',this.file, '|| form', this.form);

    // shield
    if (this.cm.isEmpty(this.file)) {
      callback();
      return;
    }

    // logic
    this.loading = true;
    const formData = new FormData();
    formData.append('imgInput', this.file, `bot_${this.bot_id}_${this.file.name}`);
    // this.fileService.upload(this.s.container_chatbot_img, formData)
    this.fileService.uploadex(this.si.container_chatbot_img,
        this.userService.getWorkspaceIds().pop(),
        this.userService.getTeamId(),
        formData
    ).subscribe(res => {
      this.loading = false;
      // this.cm.devLog('debug upload res',res);
      const resJson = JSON.parse(res.text());
      const imgPath = resJson.path;
      const fileName = resJson.name;
      // const fileName = `${JSON.parse(res.text()).result.files.imgInput[0].name}`;
      this.data.img_path = imgPath;
      // this.cm.devLog('debug img_path',this.data.img_path);
      this.isSave = false;

      // this.imgInput.setValue(null);
      callback();

      this.thumbnailUpload(fileName);
    }, err => {
      this.commonModalService.openErrorAlert(err);
    });
  }
  thumbnailUpload(parentFileName) {
    if (this.cm.isEmpty(parentFileName)) {
      console.log('Failed to thumbnail upload. not found parent file name', parentFileName);
      return false;
    }
    this.loading = true;
    const thumbFormData = new FormData();
    thumbFormData.append('imgInput', this.thumbFile, `${parentFileName}.${this.si.ext_thumb}`);
    this.fileService.uploadex(this.si.container_chatbot_img,
        this.userService.getWorkspaceIds().pop(),
        this.userService.getTeamId(),
        thumbFormData
    ).subscribe(res => {
      this.loading = false;
      const fileName = `${JSON.parse(res.text()).name}`;
      console.log('thumbnail file name', fileName);
    }, err => {
      this.commonModalService.openErrorAlert(err);
    });
  }


  // isChange($event) {
  //   this.isSave = false;
  //   this.isSaveMsg = false;
  // }
  isChangeGreeting($event) {
    this.isSave = false;
    this.isSaveGreetingMsg = false;
  }
  isChangeUnknown($event) {
    this.isSave = false;
    this.isSaveUnknownMsg = false;
  }
  // isChangeBtn($event) {
  //   this.isSave = false;
  //   this.isSaveBtn = false;
  // }
  isChangeGreetingBtn($event) {
    this.isSave = false;
    this.isSaveGreetingBtn = false;
  }
  isChangeUnknownBtn($event) {
    this.isSave = false;
    this.isSaveUnknownBtn = false;
  }

  get greetingMessages(): Chatbot_msg[] {
    return this._greetingMessages;
  }
  set greetingMessages(v: Chatbot_msg[]) {
    this.isSave = false;
    this.isSaveMsg = false;
    this._greetingMessages = v;
  }

  set unknownMessages(v: Chatbot_msg[]) {
    this.isSave = false;
    this.isSaveMsg = false;
    this._unknownMessages = v;
  }
  get unknownMessages(): Chatbot_msg[] {
    return this._unknownMessages;
  }

  previewGreeting(event) {
    this.ds.sendData(this.ds.CMD.PREVIEW_MESSAGE, {isGreeting: true});
  }
  previewUnknown(event) {
    this.ds.sendData(this.ds.CMD.PREVIEW_MESSAGE, {isGreeting: false});
  }



  enterGreeting($event) {
    if (!this.cm.isEnter($event)) {
      return false;
    }
    // this.addGreeting();
  }
  enterUnknown($event) {
    if (!this.cm.isEnter($event)) {
      return false;
    }
    // this.addUnknown();
  }
  isUpdate() {
    if (!this.service.isUpdate(this.bot_id)) {
      this.commonModalService.openErrorAlert('아직 챗봇 데이터가 저장되지 않았습니다.');
      return false;
    }
    return true;
  }
  addGreeting() {
    if (this.validator.isInvalid({
      greetingMessage: this.greetingMessage
    }, {
      greetingMessage: {
        required: true,
        maxLength: 1024
      }
    }, () => {
      return this.isUpdate();
    })
    ) {
      return false;
    }

    const item = new Chatbot_msg({
      bot_id: this.bot_id,
      msg_type_cd: this.c.MSG_TYPE_CD.GREETING,
      msg: this.greetingMessage,
      creator_id: this.userId,
      updator_id: this.userId
    });
    this.service.addMsg(item, (res) => {
      this.greetingMessage = '';
      this.refreshGreetingMsgs(res);
    });
  }
  addUnknown() {
    if (this.validator.isInvalid({
      unknownMessage: this.unknownMessage
    }, {
      unknownMessage: {
        required: true,
        maxLength: 1024
      }
    }, () => {
      return this.isUpdate();
    })
    ) {
      return false;
    }

    const item = new Chatbot_msg({
      bot_id: this.bot_id,
      msg_type_cd: this.c.MSG_TYPE_CD.UNKNOWN,
      msg: this.unknownMessage,
      creator_id: this.userId,
      updator_id: this.userId
    });
    this.service.addMsg(item, (res) => {
      this.unknownMessage = '';
      this.refreshUnknownMsgs(res);
    });
  }
  updateGreeting($event, item: Chatbot_msg) {
    if (this.validator.isInvalid({
      greetingMessage: item.msg
    }, {
      greetingMessage: {
        required: true,
        maxLength: 1024
      }
    }, () => {
      return this.isUpdate();
    })
    ) {
      return false;
    }

    item = this.cm.prop(item, {
      updated_dtm: this.dateService.getNowDate(),
      updator_id: this.userId
    });

    this.service.updateMsg(item, (res) => {
      this.refreshGreetingMsgs(res);
    });
  }
  updateUnknown($event, item: Chatbot_msg) {
    if (this.validator.isInvalid({
      unknownMessage: item.msg
    }, {
      unknownMessage: {
        required: true,
        maxLength: 1024
      }
    }, () => {
      return this.isUpdate();
    })
    ) {
      return false;
    }

    item = this.cm.prop(item, {
      updated_dtm: this.dateService.getNowDate(),
      updator_id: this.userId
    });

    this.service.updateMsg(item, (res) => {
      this.refreshUnknownMsgs(res);
    });
  }
  removeGreeting($event, item) {
    if (this.service.isUpdate(this.bot_id)) {
      this.service.removeMsg(item, this.bot_id, (res) => {
        console.log(res);
        // this.cm.devLog('ren gre',res);
        this.refreshGreetingMsgs(res);
      });
    }
  }
  removeUnknown($event, item) {
    this.service.removeMsg(item, this.bot_id, (res) => {
      // this.cm.devLog('ren unk',res);
      this.refreshUnknownMsgs(res);
    });
  }
  refreshMsgs() {
    this.data.chatbotMsgs = [];
    this.data.chatbotMsgs.push(...this.greetingMessages);
    this.data.chatbotMsgs.push(...this.unknownMessages);
  }

  refreshGreetingMsgs(res: Chatbot_msg) {
    this.service.getMsgsData(res, (result) => {
      this.greetingMessages = result;
      this.refreshMsgs();
      this.scrollDownEle(this.greetingMessagesEle);
      this.isSaveGreetingMsg = true;
    });
  }
  refreshUnknownMsgs(res: Chatbot_msg) {
    this.service.getMsgsData(res, (result) => {
      this.unknownMessages = result;
      this.refreshMsgs();
      this.scrollDownEle(this.unknownMessagesEle);
      this.isSaveUnknownMsg = true;
    });
  }


  get greetingButtons(): Chatbot_buttons[] {
    return this._greetingButtons;
  }
  set greetingButtons(v: Chatbot_buttons[]) {
    this.isSaveBtn = false;
    // this.cm.devLog('set greet');
    this._greetingButtons = v;

    this.greetingButtonIsHide = this.cm.isEmpty(v);
  }

  get unknownButtons(): Chatbot_buttons[] {
    return this._unknownButtons;
  }
  set unknownButtons(v: Chatbot_buttons[]) {
    this.isSaveBtn = false;
    this._unknownButtons = v;

    this.unknownButtonIsHide = this.cm.isEmpty(v);
  }

  enterGreetingButton($event) {
    if (!this.cm.isEnter($event)) {
      return false;
    }
    this.addGreetingButton();
  }
  enterUnknownButton($event) {
    if (!this.cm.isEnter($event)) {
      return false;
    }
    this.addUnknownButton();
  }



  private _getBtnOrder = (richContent) => {
    const res = (richContent.buttons ? richContent.buttons.length : 0) + 1;
    return res;
  };
  addGreetingButton() {
    if (this.validator.isInvalid({
      Greeting_버튼명: this.greetingButton,
      Greeting_전송값: this.greetingButtonContent
    }, {
      Greeting_버튼명: {
        required: true,
        maxLength: 64
      },
      Greeting_전송값: {
        required: true,
        maxLength: 64
      }
    }, () => {
      return this.isUpdate();
    })
    ) {
      return false;
    }

    const _addGreetingButton = () => {
      const item = new Chatbot_buttons({
        rc_id: richContent.rc_id,
        btn_order: this._getBtnOrder(richContent),
        title: this.greetingButton,
        user_utter: this.greetingButtonContent,
        creator_id: this.userId,
        updator_id: this.userId
      });

      this.addBtn(item, (res) => {
        this.greetingButton = '';
        this.greetingButtonContent = '';
        this.refreshGreetingBtns(res);
      });
    };

    let richContent: Chatbot_rich_content = this._filterGreetingRichContent(this.data.chatbotConfigs[0]);
    if (this.cm.isEmpty(richContent.rc_id)) {
      // this.cm.devLog('debug richContentGreeting 정의되지 않음.');
      const item = new Chatbot_rich_content();
      this.addContent(item, this.c.CONTENT_TYPE.BUTTON, this.c.BTN_OPTION.GREETING, (res) => {

        richContent = res;
        this.upsertConfigGreetingBtn(res.rc_id, () => {

          this.refreshRichContentGreeting(res, () => {
            _addGreetingButton();
          });
        });


      });
    } else {
      _addGreetingButton();
    }
  }
  addUnknownButton() {
    if (this.validator.isInvalid({
      Unknown_버튼명: this.unknownButton,
      Unknown_전송값: this.unknownButtonContent
    }, {
      Unknown_버튼명: {
        required: true,
        maxLength: 64
      },
      Unknown_전송값: {
        required: true,
        maxLength: 64
      }
    }, () => {
      return this.isUpdate();
    })
    ) {
      return false;
    }

    const _addUnknownButton = () => {
      const item = new Chatbot_buttons({
        rc_id: richContent.rc_id,
        btn_order: this._getBtnOrder(richContent),
        title: this.unknownButton,
        user_utter: this.unknownButtonContent,
        creator_id: this.userId,
        updator_id: this.userId
      });

      this.addBtn(item, (res) => {
        this.unknownButton = '';
        this.unknownButtonContent = '';
        this.refreshUnknownBtns(res);
      });
    };

    let richContent: Chatbot_rich_content = this._filterUnknownRichContent(this.data.chatbotConfigs[0]);
    if (this.cm.isEmpty(richContent.rc_id)) {
      // this.cm.devLog('debug richContentUnknown 정의되지 않음.');
      const item = new Chatbot_rich_content();
      this.addContent(item, this.c.CONTENT_TYPE.BUTTON, this.c.BTN_OPTION.UNKNOWN, (res) => {

        richContent = res;
        this.upsertConfigUnknownBtn(res.rc_id, () => {

          this.refreshRichContentUnknown(res, () => {
            _addUnknownButton();
          });
        });


      });
    } else {
      _addUnknownButton();
    }
  }
  updateGreetingButton($event, item: Chatbot_buttons) {
    if (this.validator.isInvalid({
      Greeting_버튼명: item.title,
      Greeting_전송값: item.user_utter
    }, {
      Greeting_버튼명: {
        required: true,
        maxLength: 64
      },
      Greeting_전송값: {
        required: true,
        maxLength: 64
      }
    }, () => {
      return this.isUpdate();
    })
    ) {
      return false;
    }

    item = this.cm.prop(item, {
      updated_dtm: this.dateService.getNowDate(),
      updator_id: this.userId
    });

    if (item.rc_id < 1 || item.btn_order < 1) {
      this.commonModalService.openErrorAlert('invalid params');
      return;
    }
    this.updateBtn(item, (res) => {
      this.refreshGreetingBtns(res);
    });
  }
  updateUnknownButton($event, item: Chatbot_buttons) {
    if (this.validator.isInvalid({
      Unknown_버튼명: item.title,
      Unknown_전송값: item.user_utter
    }, {
      Unknown_버튼명: {
        required: true,
        maxLength: 64
      },
      Unknown_전송값: {
        required: true,
        maxLength: 64
      }
    }, () => {
      return this.isUpdate();
    })
    ) {
      return false;
    }

    item = this.cm.prop(item, {
      updated_dtm: this.dateService.getNowDate(),
      updator_id: this.userId
    });

    if (item.rc_id < 1 || item.btn_order < 1) {
      this.commonModalService.openErrorAlert('invalid params');
      return;
    }

    this.updateBtn(item, (res) => {
      this.refreshUnknownBtns(res);
    });
  }
  removeGreetingButton($event, item) {
    this.removeBtn(item, (res) => {
      // this.cm.devLog('ren gre',res);
      this.refreshGreetingBtns(res);
    });
  }
  removeUnknownButton($event, item) {
    this.removeBtn(item, (res) => {
      // this.cm.devLog('ren unk',res);
      this.refreshUnknownBtns(res);
    });
  }

  refreshGreetingBtns(res: Chatbot_buttons) {
    this._getConfigData((config: Chatbot_config) => {
      const greetingBtns = this._filterGreetingRichContent(config).buttons;
      this.data.chatbotConfigs[0].richContentGreeting.buttons = greetingBtns;
      this.greetingButtons = greetingBtns;
      this.scrollDownEle(this.greetingButtonsEle);
      this.isSaveGreetingBtn = true;
    });
  }

  refreshUnknownBtns(res: Chatbot_buttons) {
    this._getConfigData((config: Chatbot_config) => {
      // const temp: Chatbot_rich_content = this._filterUnknownRichContent(config);
      const unknownBtns = this._filterUnknownRichContent(config).buttons;
      this.data.chatbotConfigs[0].richContentUnknown.buttons = unknownBtns;
      this.unknownButtons = unknownBtns;
      this.scrollDownEle(this.unknownButtonsEle);
      this.isSaveUnknownBtn = true;
    });
  }
  refreshRichContentGreeting(res: Chatbot_rich_content, callback = (): void => {}) {

    // TODO
    this._getConfigData((config: Chatbot_config) => {
      this.data.chatbotConfigs[0] = config;

      const temp: Chatbot_rich_content = this._filterGreetingRichContent(config);
      const greetingBtns = temp.buttons;
      this.data.chatbotConfigs[0].richContentGreeting.buttons = greetingBtns;
      this.greetingButtons = greetingBtns;
      this.scrollDownEle(this.greetingButtonsEle);
      this.isSaveGreetingBtn = true;
    });
    callback();
  }
  refreshRichContentUnknown(res: Chatbot_rich_content, callback = (): void => {}) {

    // TODO
    this._getConfigData((config: Chatbot_config) => {
      this.data.chatbotConfigs[0] = config;

      const temp: Chatbot_rich_content = this._filterUnknownRichContent(config);
      const unknownBtns = temp.buttons;
      this.data.chatbotConfigs[0].richContentUnknown.buttons = unknownBtns;
      this.unknownButtons = unknownBtns;
      this.scrollDownEle(this.unknownButtonsEle);
      this.isSaveUnknownBtn = true;
    });
    callback();
  }


  setData() {
    this.data.bot_name = this.title;
    this.data.description = this.description;
    // this.img_path 는 보여주기 위한 값으로만 사용하고 이미지를 로딩하기 위한 파일명은 data.img_path 에 있음.
    // if(this.img_path != this.d.img_path) {
    //   this.data.img_path = this.img_path;
    // }
    this.data.team_id = this.userService.getTeamId();
    this.data.updated_dtm = this.dateService.getNowDate();
    this.data.updator_id = this.userId;

    if (!this.isSaveGreetingMsg) {
      this.refreshMsgs();
    }
    if (!this.isSaveUnknownMsg) {
      this.refreshMsgs();
    }
    if (!this.isSaveGreetingBtn) {
      this.data.chatbotConfigs[0].richContentGreeting.buttons = this.greetingButtons;
    }
    if (!this.isSaveUnknownBtn) {
      this.data.chatbotConfigs[0].richContentUnknown.buttons = this.unknownButtons;
    }

  }
  save(url?) {
    if (this.service.isUpdate(this.bot_id)) {
      this.data.bot_id = this.bot_id;
      this.setData();
      if (!this.validateData()) {
        return false;
      }
      this.uploadChatbotImgFile(() => {

        // this.cm.devLog('after upload file.');

        this.service.updateBot(this.data, () => {

          this.isSave = true;
          this.commonModalService.openSuccessAlert(Msg.com[lang].succUpdateData);
          if (this.cm.isNotEmpty(url)) {
            this.ds.sendData(this.ds.CMD.EXIT, {url});
          }
        });

      });
    } else {
      this.data = new Chatbot_info();
      this.data.use_yn = this.c.USE_YN.Y;
      this.data.created_dtm = this.dateService.getNowDate();
      this.data.creator_id = this.userId;

      this.setData();
      if (!this.validateData()) {
        return false;
      }

      this.uploadChatbotImgFile(() => {
        this.service.insertBot(this.data, (res) => {

          this.isSave = true;
          this.commonModalService.openSuccessAlert(Msg.com[lang].succCreateData);
          if (this.cm.isNotEmpty(url)) {
            this.ds.sendData(this.ds.CMD.EXIT, {url});
          } else {
            const _setUpdate = (res: Chatbot_info) => {
              this.bot_id = res.bot_id;
              this.router.navigate([environment.container, 'chatbot', this.currPath, this.bot_id]);
            };
            _setUpdate(res);
          }
        });
      });
    }
  }
  validateData() {
    const Validator = {
      bot_name: {
        required: true,
        maxLength: 128
      },
      description: {
        maxLength: 1024
        // },
        // img_path: {
        //   maxLength: 128
      }
    };

    if (this.validator.validate(this.data, Validator, () => {

      // TODO
      // config vali

      // TODO
      // msg vali
      return true;
    })) {
      return true;
    }
    // this.commonModalService.openErrorAlert('valid Error.');
    return false;
  }


  addBtn(item: Chatbot_buttons, callback) {
    this.btnApi.create(item).subscribe((res: Chatbot_buttons) => {
      callback(res);
    }, err => {
      this.commonModalService.openErrorAlert(err);
    });
  }
  updateBtn(item: Chatbot_buttons, callback) {
    const rc_id = item.rc_id;
    const btn_order = item.btn_order;
    const where = {
      rc_id,
      btn_order
    };
    this.btnApi.upsert(item).subscribe((res: Chatbot_buttons) => {
      callback(res);
    }, err => {
      this.commonModalService.openErrorAlert(err);
    });
  }
  removeBtn(item: Chatbot_buttons, callback) {
    const rc_id = item.rc_id;
    const btn_order = item.btn_order;
    if (rc_id < 1 || btn_order < 1) {
      this.commonModalService.openErrorAlert(Msg.err[lang].invalidParams);
      return;
    }
    const where = {
      rc_id,
      btn_order
    };
    this.btnApi.destroyAll(where).subscribe((res) => {
      // this.cm.devLog('remove btn data',res);
      callback(item);
    }, err => {
      this.commonModalService.openErrorAlert(err);
    });
  }
  private _getConfigData(callback) {

    this.configApi.findById(this.data.chatbotConfigs[0].conf_id).subscribe((res: Chatbot_config) => {
      // this.cm.devLog('debug get btn', res);
      callback(res);
    }, err => {
      this.commonModalService.openErrorAlert(err);
    });
  }

  private addContent(item: Chatbot_rich_content, content_type_cd, option, callback) {
    const now = this.dateService.getNowDate();
    const prop = this.cm.prop({
      bot_id: this.data.bot_id
      , conf_id: this.data.chatbotConfigs[0].conf_id
      , content_type_cd: content_type_cd
      , created_dtm: now
      , creator_id: this.userId
      , updated_dtm: now
      , updator_id: this.userId

      , option: option
    }, item);
    this.contentApi.create(prop).subscribe((res: Chatbot_rich_content) => {
      callback(res);
    }, err => {
      this.commonModalService.openErrorAlert(err);
    });
  }


  private upsertConfigGreetingBtn(rc_id, callback) {
    const bot_id = this.data.bot_id;
    const conf_id = this.data.chatbotConfigs[0].conf_id;
    const item: Chatbot_config = new Chatbot_config({
      greeting_msg_btn_id: rc_id,
      bot_id,
      conf_id,
      synonym_yn: this.synonym_yn,
      scf_yn: 'N',
      dnn_yn: 'N',
      updator_id: this.userId
    });
    this.service.upsertConfig(rc_id, item, callback);
  }
  private upsertConfigUnknownBtn(rc_id, callback) {
    const bot_id = this.data.bot_id;
    const conf_id = this.data.chatbotConfigs[0].conf_id;
    const item: Chatbot_config = new Chatbot_config({
      unknown_msg_btn_id: rc_id,
      bot_id,
      conf_id,
      synonym_yn: this.synonym_yn,
      scf_yn: 'N',
      dnn_yn: 'N',
      updator_id: this.userId
    });
    this.service.upsertConfig(rc_id, item, callback);
  }



  // Data Filter
  private _filterGreetingRichContent = (conf: Chatbot_config) => {
    if (this.cm.isEmpty(conf.richContentGreeting)) {
      return new Chatbot_rich_content({buttons: [], content_type_cd: this.c.CONTENT_TYPE.BUTTON});
    }
    const temp: Chatbot_rich_content = conf.richContentGreeting;
    return temp;
  };
  private _filterUnknownRichContent = (conf: Chatbot_config) => {
    if (this.cm.isEmpty(conf.richContentUnknown)) {
      return new Chatbot_rich_content({buttons: [], content_type_cd: this.c.CONTENT_TYPE.BUTTON});
    }
    const temp: Chatbot_rich_content = conf.richContentUnknown;
    return temp;
  };


  // UI
  toggleGreetingButton() {
    this.greetingButtonIsHide = !this.greetingButtonIsHide;
  }
  getGreetingButtonState() {
    const res = this.greetingButtonIsHide;
    return res;
  }
  toggleUnknownButton() {
    this.unknownButtonIsHide = !this.unknownButtonIsHide;
  }
  getUnknownButtonState() {
    const res = this.unknownButtonIsHide;
    return res;
  }

  /* START of Notice Image */
  toggleNotice1Image() {
    this.notice1ImageIsHide = !this.notice1ImageIsHide;
  }
  getNotice1Image() {
    return this.notice1ImageIsHide;
  }
  toggleNotice2Image() {
    this.notice2ImageIsHide = !this.notice2ImageIsHide;
  }
  getNotice2Image() {
    return this.notice2ImageIsHide;
  }
  toggleNotice3Image() {
    this.notice3ImageIsHide = !this.notice3ImageIsHide;
  }
  getNotice3Image() {
    return this.notice3ImageIsHide;
  }
  toggleNotice4Image() {
    this.notice4ImageIsHide = !this.notice4ImageIsHide;
  }
  getNotice4Image() {
    return this.notice4ImageIsHide;
  }

  get notice1_img_path(): string {
    return this._notice1_img_path;
  }
  set notice1_img_path(v) {
    this._notice1_img_path = v;
  }
  get notice2_img_path(): string {
    return this._notice2_img_path;
  }
  set notice2_img_path(v) {
    this._notice2_img_path = v;
  }
  get notice3_img_path(): string {
    return this._notice3_img_path;
  }
  set notice3_img_path(v) {
    this._notice3_img_path = v;
  }
  get notice4_img_path(): string {
    return this._notice4_img_path;
  }
  set notice4_img_path(v) {
    this._notice4_img_path = v;
  }

  get notice1Img(): Chatbot_rich_image {
    return this._notice1Img;
  }
  set notice1Img(v) {
    this._notice1Img = v;
  }
  get notice2Img(): Chatbot_rich_image {
    return this._notice2Img;
  }
  set notice2Img(v) {
    this._notice2Img = v;
  }
  get notice3Img(): Chatbot_rich_image {
    return this._notice3Img;
  }
  set notice3Img(v) {
    this._notice3Img = v;
  }
  get notice4Img(): Chatbot_rich_image {
    return this._notice4Img;
  }
  set notice4Img(v) {
    this._notice4Img = v;
  }

  loadNoticeImage(config, option) {
    const contentType = this.c.CONTENT_TYPE.IMAGE;
    switch (option) {
      case this.c.NOTICE_OPTION.NOTICE1:
        this.notice1Img = this._filterRichContent(config, contentType, option).image;
        if (this.notice1Img && this.notice1Img.image_path) {
          this.notice1_img_path = this.chatbotInfoService.getImgPath(this.notice1Img.image_path);
        } else {
          this.notice1_img_path = this._default_notice_img_path;
          this.notice1Img = new Chatbot_rich_image();
          this.notice1Img.link_url = '';
        }
        this.notice1_img_file = null;
        break;
      case this.c.NOTICE_OPTION.NOTICE2:
        this.notice2Img = this._filterRichContent(config, contentType, option).image;
        if (this.notice2Img && this.notice2Img.image_path) {
          this.notice2_img_path = this.chatbotInfoService.getImgPath(this.notice2Img.image_path);
        } else {
          this.notice2_img_path = this._default_notice_img_path;
          this.notice2Img = new Chatbot_rich_image();
          this.notice2Img.link_url = '';
        }
        this.notice2_img_file = null;
        break;
      case this.c.NOTICE_OPTION.NOTICE3:
        this.notice3Img = this._filterRichContent(config, contentType, option).image;
        if (this.notice3Img && this.notice3Img.image_path) {
          this.notice3_img_path = this.chatbotInfoService.getImgPath(this.notice3Img.image_path);
        } else {
          this.notice3_img_path = this._default_notice_img_path;
          this.notice3Img = new Chatbot_rich_image();
          this.notice3Img.link_url = '';
        }
        this.notice3_img_file = null;
        break;
      case this.c.NOTICE_OPTION.NOTICE4:
        this.notice4Img = this._filterRichContent(config, contentType, option).image;
        if (this.notice4Img && this.notice4Img.image_path) {
          this.notice4_img_path = this.chatbotInfoService.getImgPath(this.notice4Img.image_path);
        } else {
          this.notice4_img_path = this._default_notice_img_path;
          this.notice4Img = new Chatbot_rich_image();
          this.notice4Img.link_url = '';
        }
        this.notice4_img_file = null;
        break;
    }
  }

  onFileChangeNotice(num) {
    let ele;
    switch (num) {
      case 1:
        ele = this.notice1_img_input.nativeElement;
        break;
      case 2:
        ele = this.notice2_img_input.nativeElement;
        break;
      case 3:
        ele = this.notice3_img_input.nativeElement;
        break;
      case 4:
        ele = this.notice4_img_input.nativeElement;
        break;
    }

    const files: FileList = ele.files;
    if (files.length < 1) {
      console.log('no file');
      switch (num) {
        case 1:
          this.notice1_img_path = this._default_notice_img_path;
          this.notice1_img_file = null;
          break;
        case 2:
          this.notice2_img_path = this._default_notice_img_path;
          this.notice2_img_file = null;
          break;
        case 3:
          this.notice3_img_path = this._default_notice_img_path;
          this.notice3_img_file = null;
          break;
        case 4:
          this.notice4_img_path = this._default_notice_img_path;
          this.notice4_img_file = null;
          break;
      }
      return;
    }
    // For Preview
    const file = files[0];
    const reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onload = () => {
      switch (num) {
        case 1:
          this.notice1_img_path = reader.result;
          this.notice1_img_file = file;
          break;
        case 2:
          this.notice2_img_path = reader.result;
          this.notice2_img_file = file;
          break;
        case 3:
          this.notice3_img_path = reader.result;
          this.notice3_img_file = file;
          break;
        case 4:
          this.notice4_img_path = reader.result;
          this.notice4_img_file = file;
          break;
      }
      console.log('read file');
    };
  }
  private set notice1_img_file(file) {
    console.log('set file');
    // this.isSave = false;
    this._notice1_img_file = file;
  }
  private get notice1_img_file() {
    return this._notice1_img_file;
  }
  private set notice2_img_file(file) {
    console.log('set file');
    // this.isSave = false;
    this._notice2_img_file = file;
  }
  private get notice2_img_file() {
    return this._notice2_img_file;
  }
  private set notice3_img_file(file) {
    console.log('set file');
    // this.isSave = false;
    this._notice3_img_file = file;
  }
  private get notice3_img_file() {
    return this._notice3_img_file;
  }
  private set notice4_img_file(file) {
    console.log('set file');
    // this.isSave = false;
    this._notice4_img_file = file;
  }
  private get notice4_img_file() {
    return this._notice4_img_file;
  }

  // Data Filter
  private _filterRichContent (conf: Chatbot_config, content_type_cd, option) {
    let rich_content: Chatbot_rich_content;
    if (content_type_cd === this.c.CONTENT_TYPE.BUTTON) {
      if (option === this.c.BTN_OPTION.GREETING) {
        if (this.cm.isEmpty(conf.richContentGreeting)) {
          rich_content = new Chatbot_rich_content({buttons: [], content_type_cd: content_type_cd});
        } else {
          rich_content = conf.richContentGreeting;
        }
      } else if (option === this.c.BTN_OPTION.UNKNOWN) {
        if (this.cm.isEmpty(conf.richContentUnknown)) {
          rich_content = new Chatbot_rich_content({buttons: [], content_type_cd: content_type_cd});
        } else {
          rich_content = conf.richContentUnknown;
        }
      }
    } else if (content_type_cd === this.c.CONTENT_TYPE.IMAGE) {
      switch (option) {
        case this.c.NOTICE_OPTION.NOTICE1:
          if (this.cm.isEmpty(conf.notice1)) {
            rich_content = new Chatbot_rich_content({image: null, content_type_cd: content_type_cd});
          } else {
            rich_content = conf.notice1;
          }
          break;
        case this.c.NOTICE_OPTION.NOTICE2:
          if (this.cm.isEmpty(conf.notice2)) {
            rich_content = new Chatbot_rich_content({image: null, content_type_cd: content_type_cd});
          } else {
            rich_content = conf.notice2;
          }
          break;
        case this.c.NOTICE_OPTION.NOTICE3:
          if (this.cm.isEmpty(conf.notice3)) {
            rich_content = new Chatbot_rich_content({image: null, content_type_cd: content_type_cd});
          } else {
            rich_content = conf.notice3;
          }
          break;
        case this.c.NOTICE_OPTION.NOTICE4:
          if (this.cm.isEmpty(conf.notice4)) {
            rich_content = new Chatbot_rich_content({image: null, content_type_cd: content_type_cd});
          } else {
            rich_content = conf.notice4;
          }
          break;
      }
    }
    return rich_content;
  }



  saveNoticeImg(num) {


    const content_type = this.c.CONTENT_TYPE.IMAGE;
    let option;
    let file;
    switch (num) {
      case 1:
        option = this.c.NOTICE_OPTION.NOTICE1;
        file = this.notice1_img_file;
        break;
      case 2:
        option = this.c.NOTICE_OPTION.NOTICE2;
        file = this.notice2_img_file;
        break;
      case 3:
        option = this.c.NOTICE_OPTION.NOTICE3;
        file = this.notice3_img_file;
        break;
      case 4:
        option = this.c.NOTICE_OPTION.NOTICE4;
        file = this.notice4_img_file;
        break;
    }

    if (this.isUpdate()) {
      let richContent: Chatbot_rich_content = this._filterRichContent(this.data.chatbotConfigs[0], content_type, option);
      if (this.cm.isEmpty(richContent.rc_id)) {
        // 최초 생성시에는 이미지 반드시 필요
        if (file) {
          const item = new Chatbot_rich_content();
          this.addContent(item, content_type, option, (res) => {
            richContent = res;
            this.updateNoticeUseYn(res.rc_id, option, () => {
              this.uploadNoticeImgFile(file, `${option}_${this.bot_id}_${file.name}`, () => {
                this.upsertNoticeImage(res.rc_id, richContent, option);
              });
            });
          });
        } else {
          this.commonModalService.openErrorAlert('이미지를 등록해주시기 바랍니다.');
          return;
        }
      } else {
        if (file) {
          this.updateNoticeUseYn(richContent.rc_id, option, () => {
            this.uploadNoticeImgFile(file, `${option}_${this.bot_id}_${file.name}`, () => {
              this.upsertNoticeImage(richContent.rc_id, richContent, option);
            });
          });
        } else { // 이미지 수정 없이 Url만 수정 시
          this.updateNoticeUseYn(richContent.rc_id, option, () => {
            this.upsertNoticeImage(richContent.rc_id, richContent, option);
          });
        }
      }
    }
  }

  private updateNoticeUseYn(rc_id, option, callback) {
    const data = this.data.chatbotConfigs[0];
    data.updator_id = this.userId;
    data.updated_dtm = this.dateService.getNowDate();

    switch (option) {
      case this.c.NOTICE_OPTION.NOTICE1:
        data.notice1_img_id = rc_id;
        break;
      case this.c.NOTICE_OPTION.NOTICE2:
        data.notice2_img_id = rc_id;
        break;
      case this.c.NOTICE_OPTION.NOTICE3:
        data.notice3_img_id = rc_id;
        break;
      case this.c.NOTICE_OPTION.NOTICE4:
        data.notice4_img_id = rc_id;
        break;
    }

    this.service.updateBotConfig(data, callback);
  }

  // 공지사항 이미지 업로드
  private uploadNoticeImgFile(file, filename, callback) {
    // shield
    if (this.cm.isEmpty(file)) {
      callback();
      return;
    }

    // logic
    this.loading = true;
    const formData = new FormData();
    formData.append('imgInput', file, filename);
    this.fileService.uploadex(this.si.container_chatbot_img,
        this.userService.getWorkspaceIds().pop(),
        this.userService.getTeamId(),
        formData
    ).subscribe(res => {
      this.loading = false;
      const resJson = JSON.parse(res.text());
      const imgPath = resJson.path;
      const type = filename.split('_')[0];

      switch (type) {
        case this.c.NOTICE_OPTION.NOTICE1:
          if (!this.notice1Img) {
            this.notice1Img = new Chatbot_rich_image();
          }
          this.notice1Img.image_path = imgPath;
          break;
        case this.c.NOTICE_OPTION.NOTICE2:
          if (!this.notice2Img) {
            this.notice2Img = new Chatbot_rich_image();
          }
          this.notice2Img.image_path = imgPath;
          break;
        case this.c.NOTICE_OPTION.NOTICE3:
          if (!this.notice3Img) {
            this.notice3Img = new Chatbot_rich_image();
          }
          this.notice3Img.image_path = imgPath;
          break;
        case this.c.NOTICE_OPTION.NOTICE4:
          if (!this.notice4Img) {
            this.notice4Img = new Chatbot_rich_image();
          }
          this.notice4Img.image_path = imgPath;
          break;
      }

      this.isSave = false;
      callback();
    }, err => {
      this.commonModalService.openErrorAlert(err);
    });
  }

  // chatbot_rich_image 테이블에 upsert
  private upsertNoticeImage(rc_id, richContent, option) {

    let notice;
    switch (option) {
      case this.c.NOTICE_OPTION.NOTICE1:
        notice = this.notice1Img;
        break;
      case this.c.NOTICE_OPTION.NOTICE2:
        notice = this.notice2Img;
        break;
      case this.c.NOTICE_OPTION.NOTICE3:
        notice = this.notice3Img;
        break;
      case this.c.NOTICE_OPTION.NOTICE4:
        notice = this.notice4Img;
        break;
    }

    // Validation
    if (!notice.image_path) {
      this.commonModalService.openErrorAlert('이미지를 등록해주시기 바랍니다.');
      return;
    }

    const item = new Chatbot_rich_image({
      rc_id: rc_id,
      image_path: notice.image_path,
      image_text: notice.image_text,
      link_url: notice.link_url,
      creator_id: notice.creator_id ? notice.creator_id : this.userId,
      updator_id: this.userId,
      created_dtm: notice.created_dtm ? notice.created_dtm : this.dateService.getNowDate(),
      updated_dtm: this.dateService.getNowDate()
    });

    this.imageApi.upsert(item).subscribe((res) => {
      this.commonModalService.openSuccessAlert('IMAGE 추가/수정 성공');

      // 이미지 Reload
      richContent.image = res;
      if (option === this.c.NOTICE_OPTION.NOTICE1) {
        if (this.cm.isEmpty(this.data.chatbotConfigs[0].notice1)) {
          this.data.chatbotConfigs[0].notice1 = richContent;
        }
      } else if (option === this.c.NOTICE_OPTION.NOTICE2) {
        if (this.cm.isEmpty(this.data.chatbotConfigs[0].notice2)) {
          this.data.chatbotConfigs[0].notice2 = richContent;
        }
      } else if (option === this.c.NOTICE_OPTION.NOTICE3) {
          if (this.cm.isEmpty(this.data.chatbotConfigs[0].notice3)) {
              this.data.chatbotConfigs[0].notice3 = richContent;
          }
      } else if (option === this.c.NOTICE_OPTION.NOTICE4) {
          if (this.cm.isEmpty(this.data.chatbotConfigs[0].notice4)) {
              this.data.chatbotConfigs[0].notice4 = richContent;
          }
      }
      this.loadNoticeImage(this.data.chatbotConfigs[0], option);
    }, err => {
      this.commonModalService.openErrorAlert(err);
    });
  }

  get notice1UseYn(): boolean {
    return this.notice1_use_yn === 'Y';
  }
  set notice1UseYn(v: boolean) {
    if (v) {
      this.notice1_use_yn = 'Y';
    } else {
      this.notice1_use_yn = 'N';
    }

    this.data.chatbotConfigs[0].notice1_use_yn = this.notice1_use_yn;
  }
  get notice2UseYn(): boolean {
    return this.notice2_use_yn === 'Y';
  }
  set notice2UseYn(v: boolean) {
    if (v) {
      this.notice2_use_yn = 'Y';
    } else {
      this.notice2_use_yn = 'N';
    }

    this.data.chatbotConfigs[0].notice2_use_yn = this.notice2_use_yn;
  }
  get notice3UseYn(): boolean {
    return this.notice3_use_yn === 'Y';
  }
  set notice3UseYn(v: boolean) {
    if (v) {
      this.notice3_use_yn = 'Y';
    } else {
      this.notice3_use_yn = 'N';
    }

    this.data.chatbotConfigs[0].notice3_use_yn = this.notice3_use_yn;
  }
  get notice4UseYn(): boolean {
    return this.notice4_use_yn === 'Y';
  }
  set notice4UseYn(v: boolean) {
    if (v) {
      this.notice4_use_yn = 'Y';
    } else {
      this.notice4_use_yn = 'N';
    }

    this.data.chatbotConfigs[0].notice4_use_yn = this.notice4_use_yn;
  }
  /* END of Notice Image */

  get synonymFlag(): boolean {
    if (this.synonym_yn === 'Y') {
      return true;
    } else {
      return false;
    }
  }
  set synonymFlag(v: boolean) {
    if (v) {
      this.synonym_yn = 'Y';
    } else {
      this.synonym_yn = 'N';
    }

    this.data.chatbotConfigs[0].synonym_yn = this.synonym_yn;
  }
  isChangeSynonymFlag() {
    this.isSave = false;
  }

  // UI
  scrollDownEle(ele: ElementRef) {
    this.cm.scrollDownEle(ele);
  }
}
