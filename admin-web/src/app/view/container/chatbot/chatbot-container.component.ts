import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { ChatbotUpsertSharedComponent, MenuItemEx } from './shared/chatbot-upsert-shared.component';
import { CommonUtilService, CommonModalService, MessageExService } from '../../../service';
import { Chatbot_info, Cm_user, Chatbot_infoApi, Chatbot_msg, Chatbot_buttons } from '../../../adminsdk';
import { Router } from '@angular/router';
import { DataService } from '../../../service/data.service';
import { Subscription } from 'rxjs';
import { UserService } from '../../../service/user.service';
import { Type, MessageType, Speaker, Message } from '../../../model/chatbot-message';
import { CommonModalComponent } from '../../../common';
import { ChatBotComponent } from './c-client/chatbot.component';
import { ChatbotUpsertService } from '../../../service/chatbot-upsert.service';
import { BaseResponse } from '../../../model/base-response';
import { RespDialog, UtterData } from '../../../model/response-dialog';
import { ChatbotDialogService } from '../../../service/chatbot-dialog.service';
import { ReqDialog, ReqCloseDialog, ReqOpenDialog } from '../../../model/chatbot-request';
import { FrontApiHelperService } from '../../../service/front-api-helper.service';
import { ChatbotInfoService } from '../../../service/chatbot-info.service';
import { BqaDebug } from '../../../model/response-debug';
import { Msg, lang } from '../../../model/message';
import { DateService } from '../../../service/date.service';
import { ValidatorService } from '../../../service/validator.service';
import { BackApiHelperService } from '../../../service/back-api-helper.service';

const _ = require('lodash');

@Component({
  selector: 'app-chatbot-container',
  templateUrl: './chatbot-container.component.html',
  styleUrls: ['./chatbot-container.component.scss'
    , './shared/chatbot-shared.component.scss'
  ]
})
export class ChatbotContainerComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router
    , private ds: DataService
    , private commonModalService: CommonModalService
    , private cm: CommonUtilService
    , private dateService: DateService
    , private userService: UserService
    , private api: Chatbot_infoApi
    , private backApi: BackApiHelperService
    , private frontApi: FrontApiHelperService
    , private service: ChatbotUpsertService
    , private chatbotInfoService: ChatbotInfoService
    , private dialogService: ChatbotDialogService
    , private validator: ValidatorService
  ) {
  }

  onActivate($event) {
    this.cm.isDev() && console.log('onActivate :: ', $event.constructor.name
    , $event
    );
  }

  ngOnInit() {
    this.init();
  }
  ngOnDestroy() {
    this.cm.isDev() && console.log('unsubscribe at destroy on container');
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
    this.ds.clearData();
    if(this.currChatSessionId != this.dd.sessionId) {
      this.chatEnd();
    }
  }
  init(){
    this.setSubscription();
  }
  subscription: Subscription;
  setSubscription(){
    this.subscription = this.ds.getData().subscribe(res=>{
      if(!res){
        return false;
      }
      // const _resolveInsert = (data)=>{
      //   //TODO
      // };
      const _resolveGet = (data: Chatbot_info)=>{
        this.bot_id = data.bot_id;
        this.title = data.bot_name;
        this.data = data;
        this.isSave = true;
      };
      // const _resolveSave = (data)=>{
      //   this.isSave = data.isSave;
      // };
      const _resolveIsSave = (data)=>{
        this.isSave = data.isSave;
      };
      const _resolveSetDebugTarget = (data: RespDialog)=>{
        //currUtterData 으로 현재 디버그 창에 보여줄 데이터를 넘기고
        // 처리는 debug component 에서 함.
        if(this.cm.isEmpty(data)
          || this.cm.isEmpty(data.uttrData)) {
            this.cm.isDev() && console.log('debug 데이터가 아님.');
          return;
        }
        if(this.cm.equalObj(this.currUtterData, data.uttrData)) {
          return;
        }
        const uttrData = data.uttrData;
        if(this.cm.isEmpty(uttrData.debug)
            && this.cm.isEmpty(uttrData.bqaDebug)
            && this.cm.isEmpty(uttrData.bqaDebugEx)
        ) {
          this.cm.isDev() && console.log('해당 data에 debug data가 없음.');
        }
        this.currUtterData = uttrData;
      };
      const _resolvePreviewMessage = (data)=>{
        if( this.cm.isEmpty(data) ) {
          this.previewMessage();
        } else {
          const isGreeting = data.isGreeting;
          this.previewMessage(isGreeting);
        }
      };
      const _resolveChatbotTestAvalilabe = (data)=>{
        this.chatbotWorking = false;

        const v:MenuItemEx = data.v;
        if(!v) {
          this.cm.isDev() && console.log('null');
          return;
        }
        // this.currMenu = data.v;
        if(this.chatbotTestAvailable == data.v.chatbotTestAvailable) {
          return;
        }
        this.chatbotTestAvailable = v.chatbotTestAvailable;
      };

      const _resolveChatEnd = ()=>{
        if(this.bot_id == this.di.botId) {
          this.renderServerMessage(`${Msg.com[lang].tagAdmin}${Msg.err[lang].chatbotSaveFirst}`);
          return;
        }
        if(this.currChatSessionId != this.dd.sessionId) {
          this.chatEnd();
        }
      };


      // this.cm.isDev() && console.log('ds getData', res);
      switch(res.cmd){
        // case this.ds.CMD.INSERT:
        //   _resolveInsert(res.data);
        //   break;
        case this.ds.CMD.GET:
          _resolveGet(res.data);
          break;
        // case this.ds.CMD.SAVE:
        //   _resolveSave(res.data);
        //   break;
        case this.ds.CMD.IS_SAVE:
          _resolveIsSave(res.data);
          break;
        case this.ds.CMD.IS_DEBUG:
          this._resolveToggleDebug(res.data);
          break;
        case this.ds.CMD.SET_DEBUG_TARGET:
          _resolveSetDebugTarget(res.data);
          break;
        case this.ds.CMD.PREVIEW_MESSAGE:
          _resolvePreviewMessage(res.data);
          break;
        case this.ds.CMD.CHATBOT_TEST_AVAILABLE:
          _resolveChatbotTestAvalilabe(res.data);
          break;
        case this.ds.CMD.CHAT_END:
          _resolveChatEnd();
          break;
        default:
          // this.cm.isDev() && console.log('미처리 cmd');
          break;
      }
    }, err=>{
      this.cm.isDev() && console.log('err',err);
    });
  }
  c = this.service.constants;
  di = this.chatbotInfoService.default;
  dd = this.dialogService.default;
  front = this.frontApi.api;
  back = this.backApi.api;


  private _title = '';
  set title(title) {
    if(this._title == title) {
      return;
    }
    this.isSave = false;
    this._title = title;
    this.ds.sendData(this.ds.CMD.TITLE, {title});
  }
  get title() {
    return this._title;
  }

  private _bot_id = this.di.botId;
  set bot_id(v){
    if (v == this.di.botId) {
      throw 'invalid bot_id';
    }
    this._bot_id = v;
  }
  get bot_id(){
    return this._bot_id;
  }

  data: Chatbot_info = null;

  private _isSave: boolean = false;
  @Input() set isSave(v: boolean) {
    if(this._isSave == v) {
      return;
    }
    this._isSave = v;
    this.ds.sendData(this.ds.CMD.IS_SAVE, {isSave: v});
  }
  get isSave() {
    return this._isSave;
  }

  chatbotTestAvailable = false;

  private _isDebug: boolean = false;
  get isDebug(): boolean {
    return this._isDebug;
  }
  set isDebug(v: boolean) {
    if (this._isDebug == v) {
      return;
    }
    this._isDebug = v;
  }
  private _resolveToggleDebug = (data)=>{
    if(this.isDebug == data.isDebug) {
      return;
    }
    this.isDebug = data.isDebug;
  };

  private _currUtterData: UtterData = this.dd.uttrData;
  set currUtterData(v: UtterData) {
    this._currUtterData = v;

    if(this.cm.isEmpty(v)){
      this.currStep = this.dd.chatbotStep;
      return;
    }
    if(this.currStep != v.currStep) {
      this.currStep = v.currStep;
    }
  }
  get currUtterData(): UtterData {
    return this._currUtterData;
  }


  private _currStep = this.dd.chatbotStep;
  set currStep(v) {
    this._currStep = v;
  }
  get currStep(): any {
    return this._currStep;
  }

  onHeader(emitData){
    // this.cm.isDev() && console.log('debug container onHeader',emitData);
    const _resolveSave = (data)=>{

      this.save();
    };
    const _resolveIsSave = (data)=>{
      this.isSave = data.isSave;
    };
    const _resolveTitle = (data)=>{
      if (!data.title) {
        // this.cm.isDev() && console.log('empty title')
      }
      this.title = data.title;
    };
    const cmd = emitData.cmd;
    switch(cmd){
      case this.ds.CMD.SAVE: _resolveSave(emitData); break;
      case this.ds.CMD.IS_SAVE: _resolveIsSave(emitData); break;
      case this.ds.CMD.TITLE: _resolveTitle(emitData); break;
      default: throw 'violation cmd';
    }
  }


  @ViewChild('chatbotUpsertShared')
  chatbotUpsertShared: ChatbotUpsertSharedComponent;

  save(){
    this.ds.sendData(this.ds.CMD.SAVE, {});
  }











  //chatbot test

  userId = this.userService.getCurrentUserId();
  currUserInfo: Cm_user = this.userService.getCachedUserInfo();


  @ViewChild('testModal')
  testModal: CommonModalComponent;
  public chatTestOpen(entity: Chatbot_info) {
    this.messageQueue.length = 0;
    this.testModal.modalOpen();
  }
  testModalClose() {
    this.testModal.modalClose();
  }




  @ViewChild(ChatBotComponent) chatting: ChatBotComponent;
  messageQueue: Message[] = [];
  public chatStatusHandler(emitData: any) {
    switch (emitData.cmd) {
      case this.ds.CMD.CHAT_START:
        this.chatStart();
        break;
      case this.ds.CMD.CHAT_END:
        this.chatEnd();
        break;
      case this.ds.CMD.CHAT_CLEAR:
        this.chatClear();
        break;
      case this.ds.CMD.IS_DEBUG:
        this._resolveToggleDebug(emitData.data);
        break;
      default:
      this.cm.isDev() && console.log('violation cmd');
        break;
    }
  }

  private _currChatInfo: RespDialog = this.dd.chatInfo;
  get currChatInfo(): RespDialog {
    return this._currChatInfo;
  }
  set currChatInfo(v) {
    this._currChatInfo = v;
  }

  private _currChatSessionId: string = this.dd.sessionId;
  get currChatSessionId(): string {
    return this._currChatSessionId;
  }
  set currChatSessionId(v: string){
    if(this.cm.isEmpty(v)) {
      this.cm.isDev() && console.log('Reset Session.');
      this._currChatInfo = this.dd.chatInfo;
    }
    this._currChatSessionId = v;
  }
  private resetSession() {
    this.currChatSessionId = this.dd.sessionId;
    this.renderServerMessage(`${Msg.com[lang].tagAdmin}${Msg.com[lang].chatbotSessionClose}`);
  }

  renderClientMessage(message: string) {
    this.messageQueue.push(_.cloneDeep({
      type: Type.CLIENT,
      msgType: MessageType.TEXT,
      message: message
    }));
    this.chatting.chattingScroll();
  }
  renderServerMessage(message: string = '', msgType: string = MessageType.TEXT, data?: RespDialog) {
    const msgQueue: Message = {
      type: Type.SERVER,
      msgType,
      message
    };
    if(data) {
      msgQueue.data = data;
    }

    this.messageQueue.push(_.cloneDeep(msgQueue));
    this.renderServerEnded(data);


    this.chatting.chattingScroll();
  }

  renderServerEnded(data?: RespDialog){
    let conData = undefined;
/*
    const test ='{  \n' +
        '   "code":0,\n' +
        '   "msg":"SUCCESS",\n' +
        '   "data":{  \n' +
        '      "speaker":"CHATBOT",\n' +
        '      "botId":37,\n' +
        '      "userId":"1",\n' +
        '      "sessionId":"47b6a537-3d85-4441-aef9-f48ae0d8a318",\n' +
        '      "timestamp":"2018-11-13T21:24:07Z",\n' +
        '      "sentence":"서울 날씨는 Haze입니다.",\n' +
        '      "dialogSeq":11,\n' +
        '      "debug":{  \n' +
        '         "system":"system: __RESCODE_SIMPLE_WEATHER_6_442 서울 날씨는 _condition_입니다.\\n",\n' +
        '         "slu":"태깅: 서울/서울/NN/city_지명\\nPOI:서울/서울/NN/city \\nSLU1:  <City.name=서울><TAB> #say_city_name(City.name=\\"서울\\") (0.196285)\\n\\n",\n' +
        '         "da":"inform_weather(City.name=\\"서울\\")",\n' +
        '         "isDialogEnd":"1",\n' +
        '         "slu_weight":"0.196285"\n' +
        '      },\n' +
        '      "sluWeight":0.196285,\n' +
        '      "da":"inform_weather(City.name=\\"서울\\")",\n' +
        '      "currStep":"Interface",\n' +
        '      "sdsDomain":"SIMPLE_WEATHER",\n' +
        '      "isDialogEnded":"1",\n' +
        '      "reconfirmMsg":{  \n' +
        '         "msg":"무엇을 도와 드릴까요?",\n' +
        '         "richContent":{  \n' +
        '            "contentTypeCd":"RT0001",\n' +
        '            "buttons":[  \n' +
        '               {  \n' +
        '                  "btnOrder":1,\n' +
        '                  "title":"날씨조회",\n' +
        '                  "userUtter":"날씨조회"\n' +
        '               },\n' +
        '               {  \n' +
        '                  "btnOrder":2,\n' +
        '                  "title":"피자주문",\n' +
        '                  "userUtter":"피자주문"\n' +
        '               }\n' +
        '            ]\n' +
        '         }\n' +
        '      }\n' +
        '   }\n' +
        '}';

    const testJson = JSON.parse(test);*/

    if(typeof data !=='undefined' && typeof data.uttrData !== 'undefined'){
      conData = data.uttrData;


      const isDialogEnded = conData.isDialogEnded;

      //console.log('isDialogEnded :' + isDialogEnded);

      if(parseInt(isDialogEnded) == 1){
          if(typeof conData.reconfirmMsg !=='undefined'){
          //if(typeof testJson.data.reconfirmMsg !=='undefined'){
          //const message = testJson.data.reconfirmMsg.msg;
          //const richContent = testJson.data.reconfirmMsg.richContent;

          const message = conData.reconfirmMsg.msg;
          const richContent = conData.reconfirmMsg.richContent;

          const bot_id = conData.botId;
          const contentTypeCd = richContent.contentTypeCd;
          const buttons = richContent.buttons;
          const endedData = this.dialogService.parseRespDialog(bot_id, contentTypeCd, buttons);

          if(endedData) {
              const msgQueue: Message = {
                type: Type.SERVER,
                msgType: MessageType.BUTTON,
                message
              };

              if(endedData) {
                msgQueue.data = endedData;
              }

              this.messageQueue.push(_.cloneDeep(msgQueue));
          }
        }
      }
    }
  }


  chatStart() {
    if(this.bot_id == this.di.botId) {
      this.renderServerMessage(`${Msg.com[lang].tagAdmin}${Msg.err[lang].chatbotSaveFirst}`);
      return;
    }
    if(this.currChatSessionId != this.dd.sessionId) {
      this.chatEnd(this._chatStart);
      return;
    }
    this._chatStart();
  }
  private _chatStart = ()=>{
    const params: ReqOpenDialog = {
      chatbotId: this.bot_id.toString(),
      accessChannel: "WEB_BROWSER",
      user: {
        userName: this.currUserInfo.user_name,
        userId: this.userId
      },
      showDebug: true
    };
    this.frontApi.postJson(this.front.open_dialog, params).subscribe((res: BaseResponse) => {
      this.chatbotWorking = false;
      this.chatting.chattingScroll();

      this.cm.isDev() && console.log('start res',res);
      if(this.isInvalidChatbotAnswer(res)){
        return;
      }
      const data: RespDialog = res.data;

      this.currChatInfo = data;
      this.currChatSessionId = data.sessionId;

      const uttrData: UtterData = data.uttrData;
      const debugData = this.dialogService.parseUttrToRespDialog(uttrData, this.currChatInfo);
      this.ds.sendData(this.ds.CMD.SET_DEBUG_TARGET, debugData);

      const greetingMessage = data.uttrData.sentence;
      if (this.cm.isEmpty(greetingMessage)) {
        this.cm.isDev() && console.log('No greeting');
        return;
      }
      const msgType = this.dialogService.getContentType(data.uttrData);

      this.renderServerMessage(greetingMessage, msgType, data);

    }, err=>{
      this.chatbotWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  };
  chatEnd(callback = ():void=>{}) {
    if(this.bot_id == this.di.botId) {
      this.renderServerMessage(`${Msg.com[lang].tagAdmin}${Msg.err[lang].chatbotSaveFirst}`);
      return;
    }
    if(this.cm.isEmpty(this.currChatSessionId)) {
      this.renderServerMessage(`${Msg.com[lang].tagAdmin}${Msg.err[lang].sessionNotOpenedYet}`);
      return;
    }


    const params: ReqCloseDialog = {
      chatbotId: this.bot_id.toString(),
      accessChannel: "WEB_BROWSER",
      sessionId: this.currChatSessionId,
      user: {
        userName: this.currUserInfo.user_name,
        userId: this.userId
      }
    };
    this.frontApi.postJson(this.front.close_dialog, params).subscribe((res: BaseResponse) => {
      this.cm.isDev() && console.log('end res',res);
      this.resetSession();

      this.currUtterData = this.dd.uttrData;

      callback();
    }, err=>{
      this.commonModalService.openErrorAlert(err);
    });
  };
  chatClear(callback = ():void=>{}) {
    this.messageQueue = [];
  }

  chatbotWorking = false;
  public sendMessageHandler(message: string) {
    if(this.cm.isEmpty(this.currChatSessionId)) {
      this.chatbotWorking = false;
      this.renderServerMessage(`${Msg.com[lang].tagAdmin}${Msg.err[lang].sessionNotOpenedYet}`);
      return;
    }
    this.chatbotWorking = true;
    this.chatSend(message);
  }
  public chatSend(message: string) {
    if(this.cm.isEmpty(this.currChatSessionId)) {
      this.chatbotWorking = false;
      this.renderServerMessage(`${Msg.com[lang].tagAdmin}${Msg.err[lang].sessionNotOpenedYet}`);
      return;
    }

    this.renderClientMessage(message);
    this.renderServerMessage();  // 안주면 사용자 말이 안찍힌다...이게 필요 없고...그냥..타이밍문제인것같은데 우선 그냥 둠...

    const params: ReqDialog = {
      speaker: Speaker.USER,
      userId: this.userId,
      sessionId: this.currChatSessionId,
      timestamp: this.dateService.getNowDate().toISOString(),
      sentence: message
    };
    this.frontApi.postJson(this.front.send_text, params).subscribe((res: BaseResponse) => {
      this.chatbotWorking = false;
      this.chatting.chattingScroll();
      this.messageQueue.pop();

      this.cm.isDev() && console.log('send text res',res);
      if(this.isInvalidChatbotAnswer(res)){
        return;
      }


      const uttrData: UtterData = res.data;
      const data = this.dialogService.parseUttrToRespDialog(uttrData, this.currChatInfo);
      this.ds.sendData(this.ds.CMD.SET_DEBUG_TARGET, data);
      const answerText = uttrData.sentence || '';
      if (this.cm.isEmpty(answerText)) {
        this.cm.isDev() && console.log('No Answer.');
      }
      const msgType = this.dialogService.getContentType(uttrData);



      this.renderServerMessage(answerText, msgType, data);

    }, error => {
      this.chatbotWorking = false;
      this.chatting.chattingScroll();
      this.messageQueue.pop();

      this.renderServerMessage(MessageExService.parseErrorMessage(error));

      this.resetSession();
    }, () => {
      this.chatbotWorking = false;
    });
  }


  previewMessage(isGreeting: boolean = true) {
    if(this.bot_id == this.di.botId) {
      this.chatbotWorking = false;
      this.renderServerMessage(`${Msg.com[lang].tagAdmin}${Msg.err[lang].chatbotSaveFirst}`);
      return;
    }
    this._previewMessage(isGreeting);
  }
  private _previewMessage = (isGreeting: boolean)=>{
    this.chatbotWorking = false;
    this.chatting.chattingScroll();

    const chatbotMsg: Chatbot_msg = this._getMsgData(isGreeting);
    let greetingMessage = '';
    if (typeof chatbotMsg !== 'undefined') {
      greetingMessage = chatbotMsg.msg;
    }
    if (this.cm.isEmpty(greetingMessage)) {
      this.cm.isDev() && console.log('No greeting');
      return;
    }
    const data: RespDialog = this.dialogService.parseChatbotInfoToRespDialog(isGreeting, this.data) || null;
    const contentTypeCd = this.cm.isEmpty(data) ? '' : data.uttrData.richContent.contentTypeCd;
    const msgType = this.dialogService.getMessageRichContentType(contentTypeCd);

    this.renderServerMessage(greetingMessage, msgType, data);
  };

  private _getMsgData = (isGreeting: boolean) => {
    if (isGreeting) {
      const chatbotMsg: Chatbot_msg = this.dialogService.getChatbotGreetingMessage(this.data);
      return chatbotMsg;
    } else {
      const chatbotMsg: Chatbot_msg = this.dialogService.getChatbotUnknownMessage(this.data);
      return chatbotMsg;
    }
  };


  bqaSearch(params){
    this.chatbotWorking = true;
    this.renderClientMessage(params.question);
    this.renderServerMessage();
    this.backApi.post(this.back.send_chat, params).subscribe((res: BaseResponse) => {
      this.chatbotWorking = false;


      this.chatting.chattingScroll();
      this.messageQueue.pop();

      this.cm.isDev() && console.log('send text res',res);
      if (this.cm.isEmpty(res)) {
        this.renderServerMessage(`${Msg.com[lang].tagAdmin}${Msg.err[lang].chatbotNotFoundAnswer}`);
        return;
      }
      if (this.validator.isInvalidCd(res.code)) {

        const errMsg = res.msg;
        this.renderServerMessage(errMsg);
        return;
      }
      if (this.cm.isEmpty(res.data)) {
        this.renderServerMessage(`${Msg.com[lang].tagAdmin}${Msg.err[lang].chatbotNotFoundAnswer}`);
        return;
      }

      const uttrData: BqaDebug = res.data;

      const RECONFIRM_CODE = 999000;
      if (res.code == RECONFIRM_CODE) {
        const parseReconfirmMsg = ()=>{
          const bqa_reconfirm_msg = this.data.chatbotConfigs[0].bqa_reconfirm_msg
            || `${Msg.com[lang].tagAdmin} 설정된 재확인 메시지가 없음.`;
          const answerText = bqa_reconfirm_msg;
          const msgType = MessageType.BUTTON;
          const contentTypeCd = this.dialogService.c.RICH_CONTENT_TYPE_BUTTON;
          const dialogs = uttrData.list.reduce((resList: any[], v)=>{
            resList.push({
               title: v.question
              ,userUtter: v.question
            });
            return resList;
          },[]);
          const buttons = this.dialogService.parseDialogsToButton(dialogs);
          let data: RespDialog = this.dialogService.parseRespDialog(this.bot_id, contentTypeCd, buttons);
          return {answerText, msgType, data};
        };
        const {answerText, msgType, data} = parseReconfirmMsg();
        this.renderServerMessage(answerText, msgType, data);
        return;
      }

      // BQA RichContent UI 처리
      const data: RespDialog = new class implements RespDialog {
        chatbotId: number;
        result: any;
        sessionId: string;
        userId: string;
        uttrData: UtterData = res.data;
      };

      const answerText = uttrData.answer || '';
      if (this.cm.isEmpty(answerText)) {
        this.cm.isDev() && console.log('No Answer.');
      }
      const msgType = this.dialogService.getContentType(data.uttrData);
      this.renderServerMessage(answerText, msgType, data);



    }, err=>{
      this.chatbotWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  }
  scenarioSearch(message) {
    this.chatbotWorking = true;
    this.renderClientMessage(message);
    this.renderServerMessage();
    const params = {
       botId:this.bot_id
      ,utter:message
    };
    this.scQuery(params, ()=>{
      this.itfQuery(params);
    });
  }
  scQuery(params, callback){
    this.backApi.post(this.back.sc_query, params).subscribe((res: BaseResponse) => {
      this.chatbotWorking = false;


      if(this.validator.isInvalidResponse(res)
          || this.validator.hasNoDataResponse(res)
          || this.cm.isEmpty(res.data.domain)
      ) {
        this.chatbotWorking = true;
        this.cm.isFunction(callback) && callback();
        return;
      }


      this.chatting.chattingScroll();
      this.messageQueue.pop();

      this.cm.isDev() && console.log('send text res',res);
      if (this.isInvalidChatbotAnswer(res)){
        return;
      }

      const uttrData = res.data;
      const answerText = this._parseClassifierPreview(uttrData, 'sc');
      if (this.cm.isEmpty(answerText)) {
        this.cm.isDev() && console.log('No Answer.');
      }
      const msgType = MessageType.TEXT;

      this.renderServerMessage(answerText, msgType);





    }, err=>{
      this.chatbotWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  }
  itfQuery(params){
    this.backApi.post(this.back.itf_admin_query, params).subscribe((res: BaseResponse) => {
      this.chatbotWorking = false;


      this.chatting.chattingScroll();
      this.messageQueue.pop();

      this.cm.isDev() && console.log('send text res',res);
      if(this.isInvalidChatbotAnswer(res)){
        return;
      }

      const uttrData = res.data;
      const answerText = this._parseClassifierPreview(uttrData, 'itf');
      if (this.cm.isEmpty(answerText)) {
        this.cm.isDev() && console.log('No Answer.');
      }
      const msgType = MessageType.TEXT;

      this.renderServerMessage(answerText, msgType);





    }, err=>{
      this.chatbotWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  }
  private _parseClassifierPreview(uttrData, type: string){
    let msgs = [];
    msgs.push(`도메인: ${uttrData.domain || '<<도메인을 찾지 못했습니다.>>'}`);
    msgs.push(`분류기: ${type == 'sc'? '패턴매칭' : '머신러닝'}`);
    return msgs.join('\n');
  }

  public sendMessagePreviewHandler(data) {
    const message: string = data.message;

    if(data.type == this.ds.CMD.PREVIEW_QNA) {
      const params = {
         question:message
        ,domainIds:this.data.chatbotConfigs[0].domains.reduce((res,item)=>{
          res.push(item.domain_id);
          return res;
        },[]).join(',')
        ,searchFlowType: this.data.chatbotConfigs[0].bqa_search_flow_type
        ,range: this.data.chatbotConfigs[0].bqa_search_solr_or_match_range
        ,debugYn:'N'
      };
      // this.cm.isDev() && console.log('debug temp this.data.chatbotConfigs[0].bqa_search_flow_type', this.data.chatbotConfigs[0].bqa_search_flow_type);
      this.bqaSearch(params);
      return;
    }

    if(data.type == this.ds.CMD.PREVIEW_SCENARIO) {
      this.scenarioSearch(message);
      return;
    }
    this.cm.isDev() && console.log('fail preview in container');
    this.chatbotWorking = false;
  }

  isInvalidChatbotAnswer(res: BaseResponse){
    if (this.cm.isEmpty(res)) {
      this.renderServerMessage(`${Msg.com[lang].tagAdmin}${Msg.err[lang].chatbotNotFoundAnswer}`);
      return true;
    }
    if (this.validator.isInvalidCd(res.code)) {
      const errMsg = res.msg;
      this.renderServerMessage(errMsg);
      return true;
    }
    if (this.cm.isEmpty(res.data)) {
      this.renderServerMessage(`${Msg.com[lang].tagAdmin}${Msg.err[lang].chatbotNotFoundAnswer}`);
      return true;
    }
    return false;
  }













  // UI
  rightColumnIsHide = false;
  toggleRightColumn() {
    this.rightColumnIsHide = !this.rightColumnIsHide;
  }
  getRightColumnState() {
    const res = this.rightColumnIsHide;
    return res;
  }

}
