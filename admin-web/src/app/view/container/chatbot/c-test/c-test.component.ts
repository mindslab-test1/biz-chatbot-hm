import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { CommonUtilService, CommonModalService } from '../../../../service';
import { DataService } from '../../../../service/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ChatbotInfoService } from '../../../../service/chatbot-info.service';
import { Subscription } from 'rxjs';
import { environment as env } from '../../../../environments/environment';
import { ChatbotUpsertService } from '../../../../service/chatbot-upsert.service';
import { Chatbot_info, Cm_user } from '../../../../adminsdk';
import { UserService } from '../../../../service/user.service';
import { ValidatorService } from '../../../../service/validator.service';
import { Msg, lang } from '../../../../model/message';
import { DateService } from '../../../../service/date.service';

@Component({
  selector: 'app-c-test',
  templateUrl: './c-test.component.html',
  styleUrls: ['./c-test.component.scss']
})
export class CTestComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router
    , private activatedRoute: ActivatedRoute
    , private commonModalService: CommonModalService
    , private cm: CommonUtilService
    , private ds: DataService
    , private dateService: DateService
    , private userService: UserService
    , private chatbotInfoService: ChatbotInfoService
    , private service: ChatbotUpsertService
    , private validator: ValidatorService
  ) { }

  ngOnInit() {
    this.init();
  }
  ngOnDestroy(){
    this.cm.isDev() && console.log('unsubscribe at destroy on container');

    this.ds.sendData(this.ds.CMD.CHAT_END, {});
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
    this.ds.clearData();
  }
  init(){

    this.setSubscription();
    this.activatedRoute.params.subscribe(params => {
      if(params.bot_id){
        this.cm.isDev() && console.log('\n====\n' + params.bot_id + '\n====\n\n');
        this.bot_id = params.bot_id;
        this.getBotData();
      } else {
        this.cm.isDev() && console.log('\n====\ninsert\n====\n\n');
        // this.ds.sendData(this.ds.CMD.INSERT,{});
      }
    });
  }
  subscription: Subscription;
  setSubscription(){
    this.subscription = this.ds.getData().subscribe(res=>{
      if(!res){
        return false;
      }
      const _resolveSave = (data)=>{
        
        this.save();
      };
      const _resolveIsSave = (data)=>{
        this.isSave = data.isSave;
      };
      const _resolveTitle = (data)=>{
        if (!data.title) {
          // this.cm.isDev() && console.log('empty title')
        }
        this.title = data.title;
      };

      // this.cm.isDev() && console.log('res',res);
      const {cmd, data} = res;
      switch(cmd){
        case this.ds.CMD.SAVE:
          _resolveSave(data);
          break;
        case this.ds.CMD.IS_SAVE:
          _resolveIsSave(data);
          break;
        case this.ds.CMD.TITLE:
          _resolveTitle(data);
          break;
        default:
          // this.cm.isDev() && console.log('미처리 cmd');
          break;
      }
    }, err=>{
      this.cm.isDev() && console.log('err',err);
    });
  }
  c = this.service.constants;
  d = this.chatbotInfoService.default;
  
  currPath = this.c.URL.c_test;
  
  private _bot_id = this.d.botId;
  @Input() set bot_id(v){
    if (v == this.d.botId) {
      throw 'invalid bot_id';
    }
    this._bot_id = v;
  } 
  get bot_id(){
    return this._bot_id;
  }

  private _isSave: boolean = true;
  set isSave(isSave: boolean) {
    if(this._isSave == isSave) {
      return;
    }
    this._isSave = isSave;
    this.ds.sendData(this.ds.CMD.IS_SAVE,{isSave});
  }
  get isSave() {
    return this._isSave;
  }

  private _title = '';
  set title(v) {
    if(this._title == v) {
      return;
    }
    this._title = v;
  }
  get title() {
    return this._title;
  }

  private _data: Chatbot_info;
  set data(v: Chatbot_info) {
    this._data = v;
  }
  get data() {
    return this._data;
  }

  getBotData() {
    // this.isDev() && console.log('set bot_id',this.bot_id);
    this.isSave = true;
    this.service.getBotData(this.bot_id,(res: Chatbot_info)=>{
      //filter
      if(this.cm.isEmpty(res)) {
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        return false;
      }
      if(this.cm.isEmpty(res.chatbotConfigs)) {
        this.commonModalService.openErrorAlert(`${Msg.err[lang].failLoadData} : chatbotConfigs`);
        return false;
      }

      // this.cm.isDev() && console.log('res',res);

      this.data = res;


      // set title
      this.title = res.bot_name;

      this.ds.sendData(this.ds.CMD.GET, this.data);
    }, err=>{
      this.ignoreIsSaveAndGoToList();
    });
  }
  private ignoreIsSaveAndGoToList(){
    this.isSave = false;
    let i = 0;
    const close = setInterval(()=>{
      this.isSave = true;
      if(this.isSave && i > 0) {
        clearInterval(close);
        this.commonModalService.openErrorAlert(Msg.err[lang].invalidAccess);
        this.router.navigate([env.container, this.c.URL.chatbot_management]);
      } else {
        i++;
        this.cm.isDev() && console.log('interval close');
      }
    }, 15);
  }

  userId = this.userService.getCurrentUserId();
  currUserInfo: Cm_user = this.userService.getCachedUserInfo();

  setData(){

    this.data.bot_name = this.title;
    this.data.team_id = this.userService.getTeamId();
    this.data.updated_dtm = this.dateService.getNowDate();
    this.data.updator_id = this.userId;
  }
  save(){
    if(this.service.isUpdate(this.bot_id)){
      this.data.bot_id = this.bot_id;
      this.setData();
      if(!this.validate()){
        return false;
      }
      this.service.updateBot(this.data, ()=>{
        this.isSave = true;
        this.commonModalService.openSuccessAlert(Msg.com[lang].succUpdateData);
      });
    } else {
      this.data = new Chatbot_info();
      this.data.use_yn = this.c.USE_YN.Y;
      this.data.created_dtm = this.dateService.getNowDate();
      this.data.creator_id = this.userId;
      
      this.setData();
      if(!this.validate()){
        return false;
      }
      this.service.insertBot(this.data, (res)=>{
        this.isSave = true;
        this.commonModalService.openSuccessAlert(Msg.com[lang].succCreateData);

        const _setUpdate = (res: Chatbot_info)=>{
          this.bot_id = res.bot_id;
          this.router.navigate([env.container,this.c.URL.chatbot_container,this.currPath,this.bot_id]);
        };
        _setUpdate(res);
      });
    }
  }
  validate() {
    if (this.validator.validate(this.data, {
      bot_name: {
        required: true,
        maxLength: 128
      }
    })) {
      return true;
    }
    // this.commonModalService.openErrorAlert('valid Error.');
    return false;
  }
}
