import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormArray, Validators, FormBuilder } from '@angular/forms';
import { Message, TreeNode } from 'primeng/components/common/api';
import { Cm_domainApi } from '../../../../../adminsdk/services/custom/Cm_domain';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { UserService } from '../../../../../service/user.service';
import { Cm_domain } from '../../../../../adminsdk/models/Cm_domain';
import { Cf_simpleApi } from '../../../../../adminsdk/services/custom/Cf_simple';
import { Cf_simple } from '../../../../../adminsdk/models/Cf_simple';
import { CommonModalService } from '../../../../../service/common-modal.service';
import { Itf_corpus_category } from '../../../../../adminsdk/models/Itf_corpus_category';
import { Itf_corpus_categoryApi } from '../../../../../adminsdk/services/custom/Itf_corpus_category';
import { CommonUtilService } from '../../../../../service';

@Component({
  selector: 'app-dnn-edit',
  templateUrl: './dnn-edit.component.html',
  styleUrls: ['./dnn-edit.component.scss']
})
export class DnnEditComponent implements OnInit {
  editFlag: boolean = false;
  editForm: FormGroup;
  itf_corpus_category: Itf_corpus_category;
  @Output() editRemove = new EventEmitter<Boolean>();
  @ViewChild('dnnInput') dnnInput: ElementRef;
  constructor(
    private fb: FormBuilder,
    private userSerivce: UserService,
    private itf_corpus_categoryApi: Itf_corpus_categoryApi,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService
  ) { }

  ngOnInit() {
  }

  dnnEditInit(itf_corpus_category: Itf_corpus_category) {
    this.editForm = this.fb.group({
      // intent_name: [itf_corpus_category.intent_name, [Validators.required]],
      question: [itf_corpus_category.question, [Validators.required]],
    });
    setTimeout(() => this.dnnInput.nativeElement.focus(), 0);
  }

  public dnnEditOpen(itf_corpus_category: Itf_corpus_category) {
    this.editFlag = true;
    this.itf_corpus_category = itf_corpus_category;
    this.dnnEditInit(itf_corpus_category);
  }

  onEditSubmit(value) {
    
    // if (value.intent_name.indexOf(text) != 0) {
    //   this.commonModalService.openErrorAlert("인텐트 이름 앞에 도메인 이름과 - 를 입력해주세요");
    //   return;
    // }
    
    this.itf_corpus_category.question = value.question;
    
    const updateCorpus = {
       question: value.question,
       updator_id: value.updator_id
      //  updated_dtm: 'DEFAULT'
    };
    const where = {
        cor_id : this.itf_corpus_category.cor_id,
        bot_id : this.itf_corpus_category.bot_id,
        domain_id : this.itf_corpus_category.domain_id
    };

    this.itf_corpus_categoryApi.upsertWithWhere(where, updateCorpus).subscribe(res => {
      // this.cm.isDev() && console.log(res);
    }, err => {
      this.commonModalService.openErrorAlert(err);
    }, () => {
      this.commonModalService.openAlertDialog("Success", "수정 성공", false, null);
      this.editFlag = false;
      this.editRemove.emit(true);
    });
  }
  editClose() {
    this.editFlag = false;
  }

  // _keyPress(event: any) {

  //   let regexMap = { // add your own
  //     integer: /^[0-9 ]*$/g,
  //     float: /^[+-]?([0-9]*[.])?[0-9]/g,
  //     words: /^[a-zA-Z0-9]+$/g,
  //     point25: /^\-?[0-9]*(?:\\.25|\\.50|\\.75|)$/g
  //   };
    
  //   let inputChar = String.fromCharCode(event.charCode);

  //   if (regexMap.words.test(inputChar) || inputChar === '_') {
  //   } else {
  //     alert("문자 숫자 _ 만 가능")
  //     this.cm.isDev() && console.log("prevent");
  //     event.preventDefault();
  //   }
  // }
}
