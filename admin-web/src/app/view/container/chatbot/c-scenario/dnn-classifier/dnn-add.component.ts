import { filter } from 'rxjs/operator/filter';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormArray, Validators, FormBuilder } from '@angular/forms';
import { Message, TreeNode } from 'primeng/components/common/api';
import { Cm_domainApi } from '../../../../../adminsdk/services/custom/Cm_domain';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { UserService } from '../../../../../service/user.service';
import { Cm_domain } from '../../../../../adminsdk/models/Cm_domain';
import { Cf_simpleApi } from '../../../../../adminsdk/services/custom/Cf_simple';
import { Cf_simple } from '../../../../../adminsdk/models/Cf_simple';
import { CommonModalService } from '../../../../../service/common-modal.service';
import { Itf_corpus_category, Itf_corpus_categoryApi } from '../../../../../adminsdk';
import { CommonUtilService } from '../../../../../service';

@Component({
  selector: 'app-dnn-add',
  templateUrl: './dnn-add.component.html',
  styleUrls: ['./dnn-add.component.scss']
})
export class DnnAddComponent implements OnInit {
  addFlag: boolean = false;
  addForm: FormGroup;
  itf_corpus_category: Itf_corpus_category;
  @Output() addRemove = new EventEmitter<Boolean>();
  @ViewChild('dnnInput') dnnInput: ElementRef;
  constructor(
    private fb: FormBuilder,
    private userSerivce: UserService,
    private itf_corpus_categoryApi: Itf_corpus_categoryApi,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService
  ) { }

  ngOnInit() {
  }

  dnnAddInit(itf_corpus_category: Itf_corpus_category) {
    this.addForm = this.fb.group({
      // intent_name: ['', [Validators.required]],
      question: ['', [Validators.required]]
    });
    setTimeout(() => this.dnnInput.nativeElement.focus(), 0);
  }
  public dnnAddOpen(itf_corpus_category: Itf_corpus_category) {
    this.addFlag = true;
    this.itf_corpus_category = itf_corpus_category;
    this.dnnAddInit(itf_corpus_category);
  }


  onAddSubmit(value) {


    let text: string = this.itf_corpus_category.sds_domain + "_INTENT";
    this.itf_corpus_category.intent_name = text;
    this.itf_corpus_category.question = value.question;

    this.itf_corpus_categoryApi.createCorpus(this.itf_corpus_category).subscribe(res => {
      // this.cm.isDev() && console.log(res);
    }, err => {
      this.commonModalService.openErrorAlert(err);
    }, () => {
      this.commonModalService.openAlertDialog("Success", "추가 성공", false, null);
      this.addFlag = false;
      this.addRemove.emit(true);
    });
  }
  addClose() {
    this.addFlag = false;
  }

  _keyPress(event: any) {

    let regexMap = { // add your own
      integer: /^[0-9 ]*$/g,
      float: /^[+-]?([0-9]*[.])?[0-9]/g,
      words: /^[a-zA-Z0-9]+$/g,
      point25: /^\-?[0-9]*(?:\\.25|\\.50|\\.75|)$/g
      
    };

    let inputChar = String.fromCharCode(event.charCode);

    if (regexMap.words.test(inputChar) || inputChar === '_') {
    } else {
      alert("문자 숫자 _ 만 가능");
      // this.cm.isDev() && console.log("prevent");
      event.preventDefault();
    }
  }
}
