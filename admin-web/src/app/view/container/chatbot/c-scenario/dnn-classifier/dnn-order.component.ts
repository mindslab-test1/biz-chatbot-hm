import { filter } from 'rxjs/operator/filter';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, Validators, FormBuilder } from '@angular/forms';
import { Message, TreeNode } from 'primeng/components/common/api';
import { Cm_domainApi } from '../../../../../adminsdk/services/custom/Cm_domain';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { UserService } from '../../../../../service/user.service';
import { Cm_domain } from '../../../../../adminsdk/models/Cm_domain';
import { Cf_simpleApi } from '../../../../../adminsdk/services/custom/Cf_simple';
import { Cf_simple } from '../../../../../adminsdk/models/Cf_simple';
import { CommonModalService } from '../../../../../service/common-modal.service';
import { Itf_corpus_category, Itf_corpus_categoryApi, Itf_sdsApi, Itf_sds } from '../../../../../adminsdk';
import { CommonUtilService } from '../../../../../service';

@Component({
  selector: 'app-dnn-order',
  templateUrl: './dnn-order.component.html',
  styleUrls: ['./dnn-order.component.scss']
})
export class DnnOrderComponent implements OnInit {
  orderFlag: boolean = false;
  orderForm: FormGroup;
  itf_corpus_category: Itf_corpus_category;
  arraySds;
  userId;
  
  constructor(
    private userSerivce: UserService,
    
    private itf_sdsApi: Itf_sdsApi,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService
  ) { 
    this.userId = this.userSerivce.getCurrentUserId();

  }

  ngOnInit() {
  }

  move (array, element, delta) {
    var index = array.indexOf(element);
    var newIndex = index + delta;
    if (newIndex < 0  || newIndex == array.length) return; //Already at the top or bottom.
    var indexes = [index, newIndex].sort(); //Sort the indixes
    array.splice(indexes[0], 2, array[indexes[1]], array[indexes[0]]); //Replace from lowest index, two elements, reverting the order
    this.arraySds = array;
  };
  
  moveUp(e){
    this.move(this.arraySds, e, -1);
  };
  
  moveDown (e){
    this.move(this.arraySds, e, 1);
  };
  

 
  public dnnOrderOpen(botId) {
    this.orderFlag = true;
    var filter = {
      where: {  bot_id: botId  }
      , order: 'itf_order ASC'
    };

    this.itf_sdsApi.find(filter).subscribe(res => {
      this.arraySds = res;
    });
    
  }
  down(e){
    this.cm.isDev() && console.log(e);


  }
  onOrderSubmit() {


    for(let i = 0 ; i < this.arraySds.length ; i++){
     this.arraySds[i].itf_order = i+1;
     this.arraySds[i].updator_id = this.userId;
     this.arraySds[i].updated_dtm = new Date();
    }
  
    this.itf_sdsApi.updateListOrder(this.arraySds).subscribe(res => {
    }, err => {
      this.commonModalService.openErrorAlert(err);
    }, () => {
      this.commonModalService.openAlertDialog("Success", "변경 성공", false, null);
      this.orderFlag = false;
    });
  }
  orderClose() {
    this.orderFlag = false;
  }
}
