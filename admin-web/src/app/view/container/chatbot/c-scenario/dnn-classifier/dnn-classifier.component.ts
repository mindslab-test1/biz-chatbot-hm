import { Itf_sdsApi } from '../../../../../adminsdk/services/custom/Itf_sds';
import { Itf_sds } from '../../../../../adminsdk/models/Itf_sds';
import { Cf_simpleApi } from '../../../../../adminsdk/services/custom/Cf_simple';
import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { UtilService } from '../../../../../service/util.service';
import { CommonUtilService } from '../../../../../service/commonutil.service';
import { UserService } from '../../../../../service/user.service';
import { Paginator } from '../../../../../common/paginator/paginator.component';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
// import { ClassifierAddComponent } from './classifier-add.component';
import { Cf_simple } from '../../../../../adminsdk/models/Cf_simple';
import { TreeNode } from 'primeng/components/common/treenode';
// import { ClassifierEditComponent } from './classifier-edit.component';
import { CommonModalService, AlertModalEvent } from '../../../../../service/common-modal.service';
import { DnnAddComponent } from './dnn-add.component';
import { DnnEditComponent } from './dnn-edit.component';
import { Cf_dnn } from '../../../../../adminsdk/models/Cf_dnn';
import { Cf_dnnApi } from '../../../../../adminsdk/services/custom/Cf_dnn';
import { Itf_corpus_category } from '../../../../../adminsdk/models/Itf_corpus_category';
import { Itf_intent_domain_rel } from '../../../../../adminsdk/models/Itf_intent_domain_rel';
import { Itf_corpus_categoryApi } from '../../../../../adminsdk';
import { DnnOrderComponent } from './dnn-order.component';



@Component({
  selector: 'app-dnn-classifier',
  templateUrl: './dnn-classifier.component.html',
  styleUrls: ['./dnn-classifier.component.scss']
})
export class DnnClassifierComponent implements OnInit {
  pgSize: number = 5;
  domainId: number;
  userId;
  botId: number;
  paginator = new Paginator();
  items: TreeNode[];
  dnnFlag: boolean = false;
  itf_corpus_category: Itf_corpus_category;

  @ViewChild('addModal') addModal: DnnAddComponent;
  @ViewChild('editModal') editModal: DnnEditComponent;
  @ViewChild('orderModal') orderModal: DnnOrderComponent;
  constructor(private utilService: UtilService,
    private userSerivce: UserService,
    private cm: CommonUtilService,
    private itf_sdsApi: Itf_sdsApi,
    private itf_corpus_categoryApi: Itf_corpus_categoryApi,
    private commonModalService: CommonModalService
  ) {

    this.userId = this.userSerivce.getCurrentUserId();
  }

  ngOnInit() {
    // this.init();
  }
  hide() {
    this.dnnFlag = false;
  }

  init(domainId, botId) {
    this.dnnFlag = true;
    this.paginator.pageSize = this.pgSize;
    this.paginator.currentPage = 1;

    if (domainId && domainId != 0) {
      this.getList(this.paginator.currentPage, this.pgSize, domainId, botId);
    } else {
      this.items = null;
    }
  }

  // createIntentFinder(botId) {

  //   let itf_sds = new Itf_sds();
  //   itf_sds.bot_id = botId;
  //   itf_sds.project_name = "INTENT_FINDER_" + botId;
  //   itf_sds.itf_order = 1;
  //   itf_sds.creator_id = this.userId;
  //   itf_sds.updator_id = this.userId;
  //   this.itf_sdsApi.upsert(itf_sds).subscribe(res => {
  //     this.cm.isDev() && console.log(res);
  //   }, err => {
  //     this.cm.isDev() && console.error(err);
  //   });
  // }
  // deleteIntentFinder(botId) {
  //   this.itf_sdsApi.deleteByBotId(botId).subscribe(res => {
  //     this.cm.isDev() && console.log(res);
  //   }, err => {
  //     this.cm.isDev() && console.error(err);
  //   });
  // }

  addModalClose(event) {
    // this.cm.isDev() && console.log("addNodalClose", event);
    if (event) {
      this.getList(this.paginator.currentPage, this.pgSize, this.domainId, this.botId);
    }
  }
  editModalClose(event) {
    // this.cm.isDev() && console.log("editModalClose", event);
    if (event) {
      this.getList(this.paginator.currentPage, this.pgSize, this.domainId, this.botId);
    }
  }


  orderModalOpen(botId) {
    this.orderModal.dnnOrderOpen(botId);
  }

  addModalOpen(itf_corpus_category: Itf_corpus_category) {
    this.addModal.dnnAddOpen(itf_corpus_category);
  }

  editModalOpen(e) {
    this.editModal.dnnEditOpen(e);
  }
  getList(pg, pageSize, domainId, botId) {
    this.domainId = domainId;
    this.botId = botId;
    this.paginator.currentPage = pg;

    const filter: LoopBackFilter = {
      where: { and: [{ domain_id: Number(domainId) }, { bot_id: botId }] },
      skip: (pg - 1) * this.paginator.pageSize,
      limit: this.paginator.pageSize,
      order: `cor_id desc`
    };
    // add-ons/api-mgmt/a-add/a-add
    this.itf_corpus_categoryApi.count(filter.where).subscribe(res => {
      // this.cm.isDev() && console.log("count", res);
      if (res.count <= 0) {
        this.items = null;
        return;
      } else {
        this.paginator.totalCount = res.count;

        this.itf_corpus_categoryApi.find(filter).subscribe(res => {
          // this.cm.isDev() && console.log("res :", res)
          this.items = res;
        });
      }

    }, error => {
      this.cm.isDev() && console.error(error);
    }, () => {
    }
    );

  }
  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    if (tempP == JSON.stringify(this.paginator)
      && this.paginator.totalPages != 1
    ) {
      //갱신된 값이 없으면 다시 그리지 않음.
      return true;
    }
    if (this.paginator.totalCount <= 0) {
      return true;
    }
    this.getList(this.paginator.currentPage, this.pgSize, this.domainId, this.botId);
  }

  removeOpen(entity) {
    // this.cm.isDev() && console.log(entity);
    let outer = new EventEmitter<AlertModalEvent>();
    outer.subscribe(res => {
      // this.cm.isDev() && console.log('res', res);
      if (res.isOk) {
        this.itf_corpus_categoryApi.deleteByCorpus(entity).subscribe(res => {
          // this.cm.isDev() && console.log(res);
        }, error => {
          this.commonModalService.openErrorAlert(error);
        },
          () => {
            this.commonModalService.openAlertDialog("Success", "삭제 성공", false, null);
            this.getList(this.paginator.currentPage, this.pgSize, this.domainId, this.botId);
          }
        );
      }
    }, err => {
      this.cm.isDev() && console.log('err', err);

    });
    this.commonModalService.openAlertDialog("삭제", "정말 삭제 하시겠습니까?", true, outer);
  }


  deleteByBotId(botId) {
    this.itf_corpus_categoryApi.deleteByBotId(botId).subscribe(res => {
      // this.cm.isDev() && console.log("res :", res)
    }, err => {
      this.cm.isDev() && console.log(err);
    }, () => {
      this.commonModalService.openAlertDialog("Success", "삭제 성공", false, null);
    }
    );
  }

  deleteByDomainId(domainId, botId) {
    this.itf_corpus_categoryApi.deleteByDomainId(domainId, botId).subscribe(res => {
      // this.cm.isDev() && console.log("res :", res)
    }, err => {
      this.cm.isDev() && console.log(err);
    }, () => {
      this.commonModalService.openAlertDialog("Success", "삭제 성공", false, null);
    }
    );
  }

}

