import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef , AfterContentInit} from '@angular/core';
import { FormGroup, FormArray, Validators, FormBuilder } from '@angular/forms';
import { Message, TreeNode } from 'primeng/components/common/api';
import { Cm_domainApi } from '../../../../../adminsdk/services/custom/Cm_domain';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { UserService } from '../../../../../service/user.service';
import { Cm_domain } from '../../../../../adminsdk/models/Cm_domain';
import { Cf_simpleApi } from '../../../../../adminsdk/services/custom/Cf_simple';
import { Cf_simple } from '../../../../../adminsdk/models/Cf_simple';
import { CommonModalService } from '../../../../../service/common-modal.service';
import { CommonUtilService } from '../../../../../service';

@Component({
  selector: 'app-classifier-add',
  templateUrl: './classifier-add.component.html',
  styleUrls: ['./classifier-add.component.scss']
})
export class ClassifierAddComponent implements OnInit, AfterContentInit {
  addFlag: boolean = false;
  addForm: FormGroup;
  cf_simple:Cf_simple;
  input1;
  @Output() addRemove = new EventEmitter<Boolean>();
  @ViewChild('patternInput') patternInput: ElementRef;
  constructor(
    private fb: FormBuilder,
    private cmDomainApi: Cm_domainApi,
    private userSerivce: UserService,
    private cf_simpleApi: Cf_simpleApi,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService
  ) { }

  ngOnInit() {
  }

  scfAddInit(cf_simple:Cf_simple){
    this.addForm = this.fb.group({
      pattern: ['', [Validators.required]]
    });    
    
    setTimeout(() => this.patternInput.nativeElement.focus(), 0);
  }
  ngAfterContentInit() {
    
  }
  
  public scfAddOpen(cf_simple: Cf_simple) {
    this.addFlag = true;
    this.cf_simple = cf_simple;
    this.scfAddInit(cf_simple);
  }
  valid(text){
    try{
      new RegExp(text);
      return true;
    }catch(e){
      alert(e);
      return false;
    }
  }
  onAddSubmit(value) {
    
    this.cf_simple.pattern = value.pattern;
    if(this.valid(value.pattern)){

    
    this.cf_simpleApi.upsert(this.cf_simple).subscribe(res =>{
      // this.cm.isDev() && console.log(res);
    },err=>{
      this.commonModalService.openErrorAlert(err);
    },()=>{
      this.commonModalService.openAlertDialog("Success","추가 성공",false,null);  
      this.addRemove.emit(true);
      this.addFlag = false;
    });

    }else{
      this.commonModalService.openErrorAlert("옳바른 정규표현식이 아닙니다.");
    }
  }
  addClose(){
    this.addFlag = false;
  }
}
