import { Cf_simpleApi } from '../../../../../adminsdk/services/custom/Cf_simple';
import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { CommonModalComponent } from '../../../../../common';
import { UtilService } from '../../../../../service/util.service';
import { CommonUtilService } from '../../../../../service/commonutil.service';
import { UserService } from '../../../../../service/user.service';
import { Paginator } from '../../../../../common/paginator/paginator.component';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { ClassifierAddComponent } from './classifier-add.component';
import { Cf_simple } from '../../../../../adminsdk/models/Cf_simple';
import { TreeNode } from 'primeng/components/common/treenode';
import { ClassifierEditComponent } from './classifier-edit.component';
import { CommonModalService, AlertModalEvent } from '../../../../../service/common-modal.service';


@Component({
  selector: 'app-simple-classifier',
  templateUrl: './simple-classifier.component.html',
  styleUrls: ['./simple-classifier.component.scss']
})
export class SimpleClassifierComponent implements OnInit {
  pgSize: number = 5;
  domainId: number;
  userId;
  botId: number;
  paginator = new Paginator();
  items: TreeNode[];
  scfFlag: boolean = false;
  cf_simple: Cf_simple;

  @ViewChild('addModal') addModal: ClassifierAddComponent;
  @ViewChild('editModal') editModal: ClassifierEditComponent;
  constructor(private utilService: UtilService,
    private userSerivce: UserService,
    private cm: CommonUtilService,
    private cf_simpleApi: Cf_simpleApi,
    private commonModalService: CommonModalService
  ) {

    this.userId = this.userSerivce.getCurrentUserId();
  }

  deleteByBotId(botId){
    this.cf_simpleApi.deleteByBotId(botId).subscribe(res => {
      // this.cm.isDev() && console.log("res :", res)
    },err =>{
      this.cm.isDev() && console.log(err);
    },() =>{
      this.commonModalService.openAlertDialog("Success","삭제 성공",false, null);
    }
  );
  }
  deleteByDomainId(domainId, botId){
    // this.cm.isDev() && console.log("deleteByDomainId",botId, domainId);
    this.cf_simpleApi.deleteByDomainId(domainId, botId).subscribe(res => {
      // this.cm.isDev() && console.log("res :", res)
    },err =>{
      this.cm.isDev() && console.log(err);
    },() =>{
      this.commonModalService.openAlertDialog("Success","삭제 성공",false, null);
    }
  );
  }

  ngOnInit() {
    // this.init();
  }
  hide() {
    this.scfFlag = false;
  }

  init(domainId, botId) {
    // this.cm.isDev() && console.log("init!!");
    this.scfFlag = true;
    this.paginator.pageSize = this.pgSize;
    this.paginator.currentPage = 1;

    if (domainId && domainId != 0) {
      this.getList(this.paginator.currentPage, this.pgSize, domainId, botId);
    }else{
      this.items = null;
    }
  }

  addModalClose(event) {
    // this.cm.isDev() && console.log("addNodalClose", event);
    if (event) {
      this.getList(this.paginator.currentPage, this.pgSize, this.domainId, this.botId);
    }
  }
  editModalClose(event) {
    // this.cm.isDev() && console.log("editModalClose", event);
    if (event) {
      this.getList(this.paginator.currentPage, this.pgSize, this.domainId, this.botId);
    }
  }

  addModalOpen(cf_simple: Cf_simple) {
    this.domainId = cf_simple.domain_id;
    this.botId = cf_simple.bot_id;
    this.addModal.scfAddOpen(cf_simple);
    
  }

  editModalOpen(e) {
    this.editModal.scfEditOpen(e);
  }
  getList(pg, pageSize, domainId, botId) {
    this.domainId = domainId;
    this.botId = botId;
    this.paginator.currentPage = pg;

    const filter: LoopBackFilter = {
      where: { and: [{ domain_id: Number(domainId) }, { bot_id: botId }] },
      skip: (pg - 1) * this.paginator.pageSize,
      limit: this.paginator.pageSize,
      order: `scf_id desc`
    };
    // add-ons/api-mgmt/a-add/a-add
    this.cf_simpleApi.count(filter.where).subscribe(res => {
      // this.cm.isDev() && console.log("count", res);
      if (res.count <= 0) {
        this.items = null;
        return;
      } else {
        this.paginator.totalCount = res.count;

        this.cf_simpleApi.find(filter).subscribe(res => {
          // this.cm.isDev() && console.log("res :", res)
          this.items = res;
        });
      }

    }, error => {
      console.error(error);
    }, () => {
    }
    );

  }
  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    if (tempP == JSON.stringify(this.paginator)
      && this.paginator.totalPages != 1
    ) {
      //갱신된 값이 없으면 다시 그리지 않음.
      return true;
    }
    if (this.paginator.totalCount <= 0) {
      return true;
    }
    this.getList(this.paginator.currentPage, this.pgSize, this.domainId, this.botId);
  }

  removeOpen(entity) {
    // this.cm.isDev() && console.log(entity);

    let outer = new EventEmitter<AlertModalEvent>();
    outer.subscribe(res => {
      // this.cm.isDev() && console.log('res',res);
      if(res.isOk){
        this.cf_simpleApi.deleteById(entity.scf_id).subscribe(res => {
          // this.cm.isDev() && console.log(res);
        }, error => {
          this.commonModalService.openErrorAlert(error);
        },
          () => {
            this.commonModalService.openAlertDialog("Success","삭제 성공",false, null);
            this.getList(this.paginator.currentPage,this.pgSize , this.domainId, this.botId);
          }
        );
      }
    }, err=>{
      this.cm.isDev() && console.log('err',err);

    });
    this.commonModalService.openAlertDialog("삭제", "정말 삭제 하시겠습니까?", true, outer);
  }
}

