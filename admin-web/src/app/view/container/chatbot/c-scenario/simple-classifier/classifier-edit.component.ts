import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormArray, Validators, FormBuilder } from '@angular/forms';
import { Message, TreeNode } from 'primeng/components/common/api';
import { Cm_domainApi } from '../../../../../adminsdk/services/custom/Cm_domain';
import { LoopBackFilter } from '../../../../../adminsdk/models/BaseModels';
import { UserService } from '../../../../../service/user.service';
import { Cm_domain } from '../../../../../adminsdk/models/Cm_domain';
import { Cf_simpleApi } from '../../../../../adminsdk/services/custom/Cf_simple';
import { Cf_simple } from '../../../../../adminsdk/models/Cf_simple';
import { CommonModalService } from '../../../../../service/common-modal.service';
import { CommonUtilService } from '../../../../../service';

@Component({
  selector: 'app-classifier-edit',
  templateUrl: './classifier-edit.component.html',
  styleUrls: ['./classifier-edit.component.scss']
})
export class ClassifierEditComponent implements OnInit {
  editFlag: boolean = false;
  editForm: FormGroup;
  cf_simple:Cf_simple;
  @Output() editRemove = new EventEmitter<Boolean>();
  @ViewChild('patternInput') patternInput: ElementRef;
  constructor(
    private fb: FormBuilder,
    private cmDomainApi: Cm_domainApi,
    private userSerivce: UserService,
    private cf_simpleApi: Cf_simpleApi,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService
  ) { }

  ngOnInit() {
  }

  scfEditInit(cf_simple:Cf_simple){
    this.editForm = this.fb.group({
      pattern: [cf_simple.pattern, [Validators.required]]
    });    
    setTimeout(() => this.patternInput.nativeElement.focus(), 0);
  }
  test(e){
    this.cm.isDev() && console.log(e);
  }
  public scfEditOpen(cf_simple: Cf_simple) {
    this.editFlag = true;
    this.cf_simple = cf_simple;
    this.scfEditInit(cf_simple);
  }

  onEditSubmit(value) {
    
    this.cf_simple.pattern = value.pattern;
    this.cf_simpleApi.upsert(this.cf_simple).subscribe(res =>{
      // this.cm.isDev() && console.log(res);
    },err=>{
      this.commonModalService.openErrorAlert(err);
    },()=>{
      this.commonModalService.openAlertDialog("Success","수정 성공",false ,null);
      this.editFlag = false;
      this.editRemove.emit(true);
    });
  }
  editClose(){
    this.editFlag = false;
  }
}
