import { Chatbot_msg } from '../../../../adminsdk/models/Chatbot_msg';
import { SdsService } from '../../../../service/sds.service';
import { Component, OnInit, ViewChild, OnDestroy, Input, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonModalService, CommonUtilService, AlertModalEvent } from '../../../../service';
import { DataService } from '../../../../service/data.service';
import { UserService } from '../../../../service/user.service';
import { Chatbot_config_sds_domain_relApi, Chatbot_info, LoopBackFilter, Chatbot_config, Cm_user, ProjectApi, Project, Chatbot_config_sds_domain_rel, Cf_simple, Itf_corpus_category, Itf_sdsApi, Itf_sds } from '../../../../adminsdk';
import { ChatbotUpsertService } from '../../../../service/chatbot-upsert.service';
import { Subscription } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { ValidatorService } from '../../../../service/validator.service';
import { ChatbotInfoService } from '../../../../service/chatbot-info.service';
import { DnnClassifierComponent } from './dnn-classifier/dnn-classifier.component';
import { SimpleClassifierComponent } from './simple-classifier/simple-classifier.component';
import { Msg, lang } from '../../../../model/message';
import { DateService } from '../../../../service/date.service';
import { SdsApiHelperService } from '../../../../service/sds-api-helper.service';
import { CommonModalComponent } from '../../../../common';
import { BaseResponse } from '../../../../model/base-response';
import { BackApiHelperService } from '../../../../service/back-api-helper.service';
import { EventEmitterHelper } from '../../../../model/event-emitter-helper';

@Component({
  selector: 'app-c-scenario',
  templateUrl: './c-scenario.component.html',
  styleUrls: [
    './../chatbot-container.component.scss'
    , './c-scenario.component.scss'
  ]
})
export class CScenarioComponent implements OnInit, OnDestroy {
  private _isMultiScenario: boolean = false;
  private dnnFlag: boolean = false;
  private scfFlag: boolean = false;
  private _isSave: boolean = true;
  private _data: Chatbot_info;
  private _selectedSdsList: Array<Chatbot_config_sds_domain_rel> = [];
  private _defaultScenarioId = -1;
  private _selectedScenarioId = this._defaultScenarioId;
  private _defaultScenarioList = [];
  private _scenarioList = this._defaultScenarioList;
  config: Chatbot_config = new Chatbot_config();
  subscription: Subscription;
  sdsList: Array<Project> = [];
  c = this.service.constants;
  d = this.chatbotInfoService.default;
  currPath = this.c.URL.c_scenario;
  private _bot_id = this.d.botId;
  scfSelectedDomainId: number;
  scfSelectedDomainName: string;
  dnnSelectedDomainId: number;
  dnnSelectedDomainName: string;
  
  sdsReconfirmMsg: Chatbot_msg = new Chatbot_msg();
  userId = this.userService.getCurrentUserId();
  currUserInfo: Cm_user = this.userService.getCachedUserInfo();

  @ViewChild('dnnModal') dnnModal: DnnClassifierComponent;
  @ViewChild('scfModal') scfModal: SimpleClassifierComponent;
  constructor(
    private router: Router
    , private activatedRoute: ActivatedRoute
    , private commonModalService: CommonModalService
    , private cm: CommonUtilService
    , private ds: DataService
    , private dateService: DateService
    , private userService: UserService
    , private configDomainApi: Chatbot_config_sds_domain_relApi
    , private sdsDomainApi: ProjectApi
    , private service: ChatbotUpsertService
    , private chatbotInfoService: ChatbotInfoService
    , private validator: ValidatorService
    , private sdsService: SdsService
    , private sdsApi: SdsApiHelperService
    , private backApi: BackApiHelperService
  ) {
  }

  private _completeLearning = true;
  get completeLearning() {
    return this._completeLearning;
  }
  set completeLearning(v){
    this._completeLearning = v;
  }
  progress_value = 0;
  learningResult = '';

  back = this.backApi.api;
  sds = this.sdsApi.api;

  learning() {
  
    this.sdsService.prepareLearning(this.bot_id, (projectName)=>{
      console.log('last action', projectName);
      this._learning(projectName);
    });
    // let projectName;
    // this.itf_sdsApi.findOne({
    //   where: { and: [{ bot_id }, { project_name: "INTENT_FINDER_" + bot_id }] }
    // }).subscribe((res: Itf_sds) => {
    //   // console.log("debug temp itf_sdsApi", res);
    //   if (res) {
    //     projectName = res.project_name;
    //   } else {
    //     console.log('no sds');
    //   }
    // }, err=>{
    //   this.commonModalService.openErrorAlert(err, "Error Intent finder");
    // }, () => {
    //   if(this.cm.isEmpty(projectName)) {
    //     this.commonModalService.openErrorAlert('학습할 도메인이 없습니다.', "Error Intent finder");
    //     return false;
    //   }
    //   this._learning(projectName);
    // });
  }
  progress_interval_ms = 400;
  blockModalCloseTimeoutMs = (3000 - this.progress_interval_ms);
  runningProgress() {
    const ls = [10, 15, 20, 25, 30, 35, 40, 45, 50, 51, 52, 53, 55, 60, 65, 70, 75, 80, 85, 90, 95];
    // console.log('debug temp run start');
    const lsLength = ls.length;
    let i = 0;
    let run = setInterval(()=>{
      if(this.completeLearning) {
        clearInterval(run);
      }
      if(i >= lsLength) {
        // i = 0;
        clearInterval(run);
      }
      // console.log('debug temp run progress', i, ls[i]);
      this.progress_value = ls[i];
      i++;
    }, this.progress_interval_ms);
  }
  closeItf(callback = ()=>{ this.cm.override(); }) {
    const botId = this.bot_id;
    const params = {
      botId
    };
    this.backApi.post(this.back.itf_close, params).subscribe((res: BaseResponse) => {
      if(this.validator.isInvalidResponse(res)) {
        this.commonModalService.openErrorAlert(res ? res.msg : Msg.err[lang].itfCloseFail);
        return false;
      }

      console.log('success close itf');
      if(this.cm.isFunction(callback)) {
        callback();
      }

    }, err=>{
      this.commonModalService.openErrorAlert(err);
    });
  }

  private _learning(projectName){
    this.closeItf(()=>{
      this.startLearning();
      const body = projectName;
      this.sdsApi.postBody(this.sds.itf_learn, body).subscribe(_res=>{
        console.log('debug temp learning res',_res);
        const res: BaseResponse = this.sdsApi.parseRes(_res);
        if(this.validator.isInvalidResponse(res)) {
          this.endLearningFail();
          return false;
        }
        if(this.validator.hasNoDataResponse(res)) {
          this.endLearningFail();
          return false;
        }
        this.learningResult = this.parseRespMsg(res.data);
      }, err=>{
        this.endLearningFail();
        this.commonModalService.openErrorAlert(err);
      }, ()=>{
        this.endLearningSucc();
      });
    });
  }
  parseRespMsg(data: string) {
    if(data == 'null'){
      data = 'data 없음.';
    }
    return data;
  }
  learningResultTitle = '';
  learningResultMsg = '';
  startLearning() {
    this.learningResult = '';
    this.completeLearning = false;
    this.learningResultTitle = '머신러닝 ITF 학습 중...';
    this.learningResultMsg = '학습 중...';
    this.progress_value = 0;
    this.runningProgress();
    this.blockModalOpen();
  }
  endLearningFail() {
    this.completeLearning = true;
    setTimeout(()=>{
      this.learningResultTitle = '머신러닝 ITF 학습 실패';
      this.learningResultMsg = '학습 실패';
      this.progress_value = 0;
      this.endLearning();
    }, this.progress_interval_ms);
  }
  endLearningSucc() {
    this.completeLearning = true;
    setTimeout(()=>{
      this.learningResultTitle = '머신러닝 ITF 학습 완료';
      this.learningResultMsg = '학습 완료';
      this.progress_value = 100;
      // this.endLearning();
    }, this.progress_interval_ms);
  }
  endLearning() {
    setTimeout(()=>{
      this.blockModalClose();
    }, this.blockModalCloseTimeoutMs);
  }
  changeDnn(dnnFlag) {


    if (this.config.dnn_yn == 'Y') {
      this.selectDnn(this.dnnSelectedDomainId, this.dnnSelectedDomainName);
    } else {
      this.dnnModal.hide();
    }
    this.isSave = false;
  }
  selectDnn(domainId, domainName) {
    this.dnnSelectedDomainId = domainId;
    this.dnnSelectedDomainName = domainName;
    this.dnnModal.init(domainId, this._bot_id);
  }

  changeScf(scfFlag) {
    
    if (this.config.scf_yn == 'Y') {
      this.selectScf(this.scfSelectedDomainId, this.scfSelectedDomainName);
    }else{
      this.scfModal.hide();
    }
    this.isSave = false;
  }
  selectScf(domainId, domainName) {
    this.scfSelectedDomainId = domainId;
    this.scfSelectedDomainName = domainName;
    this.scfModal.init(domainId, this._bot_id);
  }
  ngOnInit() {
    this.init();
  }

  ngOnDestroy() {
    this.cm.isDev() && console.log('unsubscribe at destroy on container');
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
    this.ds.clearData();
  }

  init() {
    this.setSubscription();
    this.activatedRoute.params.subscribe(params => {
      if (params.bot_id) {
        this.cm.isDev() && console.log('\n====\n' + params.bot_id + '\n====\n\n');
        this.bot_id = params.bot_id;
        this.getBotData();
        this.getSdsData();
      } else {
        this.cm.isDev() && console.log('\n====\ninsert\n====\n\n');
      }
    });
  }


  setSubscription() {
    this.subscription = this.ds.getData().subscribe(res => {
      if (!res) {
        return false;
      }
      const _resolveSave = (data) => {
        this.save();
      };
      const _resolveIsSave = (data) => {
        this.isSave = data.isSave;
      };
      const _resolveTitle = (data) => {
        if (!data.title) {
          // this.cm.isDev() && console.log('empty title')
        }
        this.title = data.title;
      };

      switch (res.cmd) {
        case this.ds.CMD.SAVE:
          _resolveSave(res.data);
          break;
        case this.ds.CMD.IS_SAVE:
          _resolveIsSave(res.data);
          break;
        case this.ds.CMD.TITLE:
          _resolveTitle(res.data);
          break;
        default:
          break;
      }
    }, err => {
      this.cm.isDev() && console.log('err', err);
    });
  }

  @Input() set bot_id(v) {
    if (v == this.d.botId) {
      throw 'invalid bot_id';
    }
    this._bot_id = v;
  }
  get bot_id() {
    return this._bot_id;
  }

  set isSave(isSave: boolean) {
    if (this._isSave == isSave) {
      return;
    }
    this._isSave = isSave;
    this.ds.sendData(this.ds.CMD.IS_SAVE, { isSave });
  }
  get isSave() {
    return this._isSave;
  }

  private _title = '';
  set title(v) {
    if (this._title == v) {
      return;
    }
    this._title = v;
  }
  get title() {
    return this._title;
  }
  set data(v: Chatbot_info) {
    this._data = v;
  }
  get data() {
    return this._data;
  }

  get selectedSdsList(): Array<Chatbot_config_sds_domain_rel> {
    return this._selectedSdsList;
  }
  set selectedSdsList(v: Array<Chatbot_config_sds_domain_rel>) {
    this._selectedSdsList = v;
    this._isMultiScenario = this._selectedSdsList.length > 1;
  }
  get isMultiScenario(): boolean {
    return this._isMultiScenario;
  }

  onTree(emitData) {
    this.cm.isDev() && console.log('emit tree', emitData);
    const selectedItem = emitData.data;
    switch (emitData.cmd) {
      case EventEmitterHelper.CMD.SELECT: this.selectSds(selectedItem); break;
      default: throw 'violation cmd';
    }
  }
  addPattern() {

    const cf_simple = new Cf_simple();
    cf_simple.domain_id = this.scfSelectedDomainId;
    cf_simple.bot_id = this.bot_id;
    cf_simple.sds_domain = this.scfSelectedDomainName;
    cf_simple.creator_id = this.userId;
    cf_simple.updator_id = this.userId;

    this.scfModal.addModalOpen(cf_simple);
  }

  addQuestion() {

    const itf_corpus_category = new Itf_corpus_category();
    itf_corpus_category.domain_id = this.dnnSelectedDomainId;
    itf_corpus_category.bot_id = this.bot_id;
    itf_corpus_category.sds_domain = this.dnnSelectedDomainName;
    itf_corpus_category.creator_id = this.userId;
    itf_corpus_category.updator_id = this.userId;

    this.dnnModal.addModalOpen(itf_corpus_category);
  }

  selectSds(selectedItem) {
    if(this.selectedSdsList.some((selectedSdsList)=>this.service.equalSdsDomain(selectedSdsList,selectedItem))){
      this.commonModalService.openErrorAlert(Msg.err[lang].domainAlreaySelection);
      return false;
    }
    this.selectedSdsList.push(this.service.parseProjectToSdsDomain(selectedItem));
    if (this.selectedSdsList && this.selectedSdsList.length == 1) {
      this.scfSelectedDomainId = this.selectedSdsList[0].domain_id;

      this.scfSelectedDomainName = this.selectedSdsList[0].domain_name;
      if (this.config.scf_yn == 'Y') {
        this.selectScf(this.scfSelectedDomainId, this.scfSelectedDomainName);
      }
      this.dnnSelectedDomainId = this.selectedSdsList[0].domain_id;
      this.dnnSelectedDomainName = this.selectedSdsList[0].domain_name;
      if (this.config.dnn_yn == 'Y') {
        this.selectDnn(this.dnnSelectedDomainId, this.dnnSelectedDomainName);
      }
    }

    // this.selectedSdsList = this.selectedSdsList;
    // this.selectedSdsList.sort(this.service.sortSdsDomain);
    this.data.chatbotConfigs[0].sdsDomains = this.selectedSdsList;
    this.isSave = false;
  }
  deselectSds(selectedItem) {
    // this.cm.isDev() && console.log('desel', selectedItem);
    let outer = new EventEmitter<AlertModalEvent>();
    outer.subscribe(res => {
      // this.cm.isDev() && console.log('res', res);
      if (res.isOk) {
        
        this.scfModal.deleteByDomainId(selectedItem.domain_id, this.bot_id);
        this.dnnModal.deleteByDomainId(selectedItem.domain_id, this.bot_id);
        this.scfModal.init(this.scfSelectedDomainId, this.bot_id);
        this.dnnModal.init(this.dnnSelectedDomainId, this.bot_id);
        this.selectedSdsList = this.selectedSdsList.filter((selectedSds: Chatbot_config_sds_domain_rel) => this.service.notEqualSdsDomainEx(selectedSds, selectedItem));
        this.data.chatbotConfigs[0].sdsDomains = this.selectedSdsList;
        this.isSave = false;
        this.changeScf(this.scfFlag);
        this.changeDnn(this.dnnFlag);
      }
    }, err => {
      this.cm.isDev() && console.log('err', err);
    });

    this.commonModalService.openAlertDialog("삭제", selectedItem.domain_name + "에 저장된 모든 정규식 또는 머신러닝이 삭제됩니다. 정말 삭제 하시겠습니까?", true, outer);
  }
  editIntentOrder(){
    this.dnnModal.orderModalOpen(this.bot_id);
  }
  getBotData() {
    this.isSave = true;
    this.service.getBotData(this.bot_id, (res: Chatbot_info) => {
      //filter
      if(this.cm.isEmpty(res)) {
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        return false;
      }
      if(this.cm.isEmpty(res.chatbotConfigs)) {
        this.commonModalService.openErrorAlert(`${Msg.err[lang].failLoadData} : chatbotConfigs`);
        return false;
      }

      // this.cm.isDev() && console.log('res', res);

      this.data = res;
      // set title
      this.title = res.bot_name;

      // set message
      // set message
      const msgList = res.chatbotMsgs;
      const sdsMsgs = this.cm.filter(msgList, 'msg_type_cd',this.c.MSG_TYPE_CD.SDSMSG);
      if(sdsMsgs && sdsMsgs.length > 0){
        
        this.sdsReconfirmMsg = sdsMsgs[0];
      }else{
        this.sdsReconfirmMsg = new Chatbot_msg();
        this.sdsReconfirmMsg.bot_id = this.bot_id;
        this.sdsReconfirmMsg.msg_type_cd = this.c.MSG_TYPE_CD.SDSMSG;
        this.sdsReconfirmMsg.msg = '';
        this.sdsReconfirmMsg.creator_id = this.userId;
        this.sdsReconfirmMsg.updator_id = this.userId;
      }
      const config: Chatbot_config = res.chatbotConfigs[0];
      this.selectedSdsList = this.service.filterSdsDomain(config);

      this.config = config;
      // this.config.dnn_yn = config.dnn_yn;
      this.ds.sendData(this.ds.CMD.GET, this.data);
    }, err=>{
      this.ignoreIsSaveAndGoToList();
    }, () => {
      if (this.selectedSdsList.length > 0 && this.config.scf_yn == 'Y') {
        this.scfSelectedDomainId = this.selectedSdsList[0].domain_id;
        this.scfSelectedDomainName = this.selectedSdsList[0].domain_name;
        this.scfModal.init(this.scfSelectedDomainId, this.bot_id);
      }
      if (this.selectedSdsList.length > 0 && this.config.dnn_yn == 'Y') {
        this.dnnSelectedDomainId = this.selectedSdsList[0].domain_id;
        this.dnnSelectedDomainName = this.selectedSdsList[0].domain_name;
        this.dnnModal.init(this.dnnSelectedDomainId, this.bot_id);
      }
    });
  }
  private ignoreIsSaveAndGoToList(){
    this.isSave = false;
    let i = 0;
    const close = setInterval(()=>{
      this.isSave = true;
      if(this.isSave && i > 0) {
        clearInterval(close);
        this.commonModalService.openErrorAlert(Msg.err[lang].invalidAccess);
        this.router.navigate([environment.container, this.c.URL.chatbot_management]);
      } else {
        i++;
        this.cm.isDev() && console.log('interval close');
      }
    }, 15);
  }

  getSdsData() {
    const filter: LoopBackFilter = {
      where: {
        SEQ_NO: { neq: 0 },
        ITF_YN: { neq: 'Y' }
      },
      order: 'SEQ_NO asc'
    };
    this.sdsDomainApi.count(filter.where).subscribe(res => {
      if (res.count <= 0) {
        // this.cm.isDev() && console.log('no data');
        return false;
      }
      this.sdsDomainApi.find(filter).subscribe((res: Array<Project>) => {
        // this.cm.isDev() && console.log("res :", res)
        this.sdsList = res;
      });
    }, err => {
      this.commonModalService.openErrorAlert(err);
    });
  }

  getCurrConfig(): Chatbot_config {
    if (this.cm.isEmpty(this.data.chatbotConfigs)) {
      return this.config;
    }
    return this.cm.prop(this.data.chatbotConfigs[0], this.config);
  }
  setData() {
    this.data.bot_name = this.title;
    this.data.team_id = this.userService.getTeamId();
    this.data.updated_dtm = this.dateService.getNowDate();
    this.data.updator_id = this.userId;
  }
  save() {
    if (this.service.isUpdate(this.bot_id)) {
      this.data.bot_id = this.bot_id;
      // this.setConfig();

      this.data.chatbotConfigs = [this.getCurrConfig()];
      this.setData();
      if (!this.validate()) {
        return false;
      }
      this.service.updateBot(this.data, () => {
        this.updateDomains(() => {
          this.isSave = true;
          this.commonModalService.openSuccessAlert(Msg.com[lang].domainSuccUpdate);
        });
      });
    } else {
      this.data = new Chatbot_info();
      this.data.use_yn = this.c.USE_YN.Y;
      this.data.created_dtm = this.dateService.getNowDate();
      this.data.creator_id = this.userId;

      this.data.chatbotConfigs = [this.getCurrConfig()];

      this.setData();
      if (!this.validate()) {
        return false;
      }
      this.service.insertBot(this.data, (res) => {
        this.isSave = true;
        this.commonModalService.openSuccessAlert(Msg.com[lang].succCreateData);

        const _setUpdate = (res: Chatbot_info) => {
          this.bot_id = res.bot_id;
          this.router.navigate([environment.container, 'chatbot', this.currPath, this.bot_id]);
        };
        _setUpdate(res);
      });
    }
  }
  updateDomains(callback = () => { this.cm.override(); }) {
    const conf_id = this.data.chatbotConfigs[0].conf_id;
    if(this.cm.isEmpty(conf_id)){
      this.commonModalService.openErrorAlert('Invalid conf_id');
      return false;
    }
    const filter: LoopBackFilter = {
      where: {
        conf_id: conf_id
      }
    };
    const _getData = () => {
      const res = this.selectedSdsList.map((v: Chatbot_config_sds_domain_rel) => {
        const { domain_id, domain_name } = v;
        v.conf_id = conf_id;
        return new Chatbot_config_sds_domain_rel({ conf_id, domain_id, domain_name });
      });
      // this.cm.isDev() && console.log('debug domains \n\n========================>\n', res);
      return res;
    };
    const data = _getData();

    this.configDomainApi.updatesRel(filter.where, data).subscribe(res => {
      // this.cm.isDev() && console.log('res domains', res);
      callback();
    }, err=>{
      this.commonModalService.openErrorAlert(err);
    });
  }
  validate() {
    if (this.validator.validate(this.data, {
      bot_name: {
        required: true,
        maxLength: 128
      }
    })) {
      return true;
    }
    return false;
  }
  get scfYn() {
    return this.config.scf_yn == 'Y';
  }
  set scfYn(v) {
    this.config.scf_yn = v ? 'Y' : 'N';
    this.isSave = false;
  }
  get dnnYn() {
    return this.config.dnn_yn == 'Y';
  }
  set dnnYn(v) {
    this.config.dnn_yn = v ? 'Y' : 'N';
    this.isSave = false;
  }
  get scenarioList() {
    return this._scenarioList;
  }
  set scenarioList(scenarioList) {
    this._scenarioList = scenarioList;
  }

  get selectedScenarioId() {
    return this._selectedScenarioId;
  }
  set selectedScenarioId(selectedScenarioId) {
    this._selectedScenarioId = selectedScenarioId;
  }

  onHeader(event) {
    const cmd = event.cmd;
    // this.cm.isDev() && console.log('event.cmd', cmd);
  }

  onScenarioSelect(event) {
  }
  // upsertScf(event){
  //   this.scfModal.modalOpen();
  // }
  // upsertDnn(event){
  //   this.dnncfModal.modalOpen();
  // }

  @ViewChild('blockModal')
  blockModal: CommonModalComponent;
  public blockModalOpen() {
    this.blockModal.modalOpen();
  }
  blockModalClose() {
    this.blockModal.modalClose();
  }
  changeSdsMsg(){
    this.data.chatbotMsgs = this.cm.reduce(this.data.chatbotMsgs,'msg_type_cd',this.c.MSG_TYPE_CD.SDSMSG);
    if(this.sdsReconfirmMsg && this.sdsReconfirmMsg.msg.length > 0){
      this.data.chatbotMsgs.push(this.sdsReconfirmMsg);
    }
  }
}
