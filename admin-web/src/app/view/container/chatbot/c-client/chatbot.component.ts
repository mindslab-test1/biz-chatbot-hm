import { Component, Input, Output, EventEmitter, OnDestroy, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CommonUtilService, CommonModalService } from '../../../../service';
import { DataService } from '../../../../service/data.service';
import { Subscription } from 'rxjs';
import { MessageType, Message, Type } from '../../../../model/chatbot-message';
import { Chatbot_info } from '../../../../adminsdk';
import { RespDialog } from '../../../../model/response-dialog';
import { ChatbotInfoService } from '../../../../service/chatbot-info.service';
import { ChatbotDialogService } from '../../../../service/chatbot-dialog.service';
import { ChatbotUpsertService } from '../../../../service/chatbot-upsert.service';
import { environment as env } from '../../../../environments/environment';
import { Msg, lang } from '../../../../model/message';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: [
      './chatbot.component.scss'
    , '../shared/chatbot-shared.component.scss'
  ]
})

export class ChatBotComponent implements OnInit, OnDestroy {
  constructor(
      private cm: CommonUtilService
    , private commonModalService: CommonModalService
    , private ds: DataService
    , private chatbotInfoService: ChatbotInfoService
    , private service: ChatbotUpsertService
    , private dialogService: ChatbotDialogService
  ) {
  }
  ngOnInit() {
    this.init();
  }
  ngOnDestroy(){
    this.cm.isDev() && console.log('unsubscribe at destroy on container');
    // unsubscribe to ensure no memory leaks
    this.subscription && this.subscription.unsubscribe();
    this.ds.clearData();
  }
  init(){
    // if(this.cm.isEmpty(this.messageQueue)){
    //   this.messageQueue = [];
    // }
    this.setSubscription();
  }
  subscription: Subscription;
  setSubscription(){
    this.subscription = this.ds.getData().subscribe(res=>{
      if(!res){
        return false;
      }

      const _resolveToggleDebug = (data)=>{
        if(this.isDebug == data.isDebug) {
          return;
        }
        this.isDebug = data.isDebug;
      };
      const _resolveToggleQna = (data)=>{
        this.isQna = data.isQna;
      };
      const _resolveToggleScenario = (data)=>{
        this.isScenario = data.isScenario;
      };
  
      // this.cm.isDev() && console.log('ds getData', res);
      switch(res.cmd){
        case this.ds.CMD.IS_DEBUG:
          _resolveToggleDebug(res.data);
          break;
        case this.ds.CMD.IS_QNA:
          _resolveToggleQna(res.data);
          break;
        case this.ds.CMD.IS_SCENARIO:
          _resolveToggleScenario(res.data);
          break;
          
        default:
          // this.cm.isDev() && console.log('미처리 cmd');
          break;
      }
    }, err=>{
      this.cm.isDev() && console.log('err',err);
    });
  }
  dd = this.dialogService.default;

  @Input() data: Chatbot_info;
  @Input() currChatInfo: RespDialog = this.dd.chatInfo;
  @Input() currChatSessionId: string = this.dd.sessionId;
  @Input() messageQueue: Message[];
  @Input() chatbotWorking: boolean = false;
  @Output() chatHandler = new EventEmitter<any>();
  @Output() sendMessageHandler = new EventEmitter<string>();
  @Output() sendMessagePreviewHandler = new EventEmitter<any>();
  public Type = Type;
  public MessageType = MessageType;
  private _isDebug: boolean;
  get isDebug(): boolean {
    return this._isDebug;
  }
  set isDebug(v) {
    if (this._isDebug == v) {
      return;
    }
    this._isDebug = v;
  }  
  _target = -1;
  get target(): number {
    return this._target;
  }
  set target(v) {
    this._target = v;
  }
  toggleDebug(event) {
    if(this.chatbotTestAvailable) {
      // this.cm.isDev() && console.log('통합 테스트용 토글 하지 않음.');
      return;
    }
    if(this.currChatSessionId == this.dd.sessionId) {
      this.commonModalService.openErrorAlert(Msg.err[lang].sessionNotOpenedYet);
      return;
    }
    this.isDebug = !this.isDebug;
    const isDebug = this.isDebug;
    this.ds.sendData(this.ds.CMD.IS_DEBUG, {isDebug});
  }

  inputMessage(event) {
    if (event === null) {
      this.sendMessage();
      return;
    }
    
    if (this.cm.isArrowUp(event) && this.cm.isKeyup(event)) {
      this.footer_input = this.inputHistGetPrev();
    }
    if (this.cm.isArrowDown(event) && this.cm.isKeyup(event)) {
      this.footer_input = this.inputHistGetNext();
    }
    if (this.cm.isEnter(event)) {
      this.sendMessage();
    }
  }

  chatStart() {
    const emitData = {cmd: this.ds.CMD.CHAT_START, data: {}};
    this.chatHandler.emit(emitData);
  }

  chatEnd() {
    if(this.currChatSessionId == this.dd.sessionId) {
      this.commonModalService.openErrorAlert(Msg.err[lang].sessionNotOpenedYet);
      return;
    }
    const emitData = {cmd: this.ds.CMD.CHAT_END, data: {}};
    this.chatHandler.emit(emitData);
  }

  chatClear() {
    const emitData = {cmd: this.ds.CMD.CHAT_CLEAR, data: {}};
    this.chatHandler.emit(emitData);
  }

  isQna = false;
  isScenario = false;
  footer_input = '';
  sendMessage() {
    if (this.isQna) {
      if (this.sendMessagePreviewHandler !== null && this.footer_input !== '' && this.footer_input !== '\n') {
        // this.cm.isDev() && console.log('sendMessage preview input: ',this.footer_input, '|| messageQueue: ',this.messageQueue);
        this.sendMessagePreviewHandler.emit({type: this.ds.CMD.PREVIEW_QNA, message: this.footer_input});

        this.inputHistAdd(this.footer_input);
      }
      this.footer_input = '';
      return;
    }
    if (this.isScenario) {
      if (this.sendMessagePreviewHandler !== null && this.footer_input !== '' && this.footer_input !== '\n') {
        // this.cm.isDev() && console.log('sendMessage preview input: ',this.footer_input, '|| sendMessage preview messageQueue: ',this.messageQueue);
        this.sendMessagePreviewHandler.emit({type: this.ds.CMD.PREVIEW_SCENARIO, message: this.footer_input});

        this.inputHistAdd(this.footer_input);
      }
      this.footer_input = '';
      return;
    }
    if (this.sendMessageHandler !== null && this.footer_input !== '' && this.footer_input !== '\n') {
      // this.cm.isDev() && console.log('sendMessage input: ',this.footer_input, '|| sendMessage messageQueue: ',this.messageQueue);
      this.sendMessageHandler.emit(this.footer_input);

      this.inputHistAdd(this.footer_input);
    }
    this.footer_input = '';
  }
  transferMessage(message: string) {
    if (this.isQna) {
      if (this.sendMessagePreviewHandler !== null) {
        this.cm.isDev() && console.log('sendMessage preview for qna button');
        this.sendMessagePreviewHandler.emit({type: this.ds.CMD.PREVIEW_QNA, message});

        this.inputHistAdd(message);
      }
      return;
    }
    if (this.isScenario) {
      if (this.sendMessagePreviewHandler !== null) {
        this.cm.isDev() && console.log('sendMessage preview for scenario button');
        this.sendMessagePreviewHandler.emit({type: this.ds.CMD.PREVIEW_SCENARIO, message});

        this.inputHistAdd(message);
      }
      return;
    }
    this.sendMessageHandler.emit(message);

    this.inputHistAdd(message);
  }


  @ViewChild('chatbotContentsWrap')
  chatbotContentsWrap: ElementRef;
  chattingScroll() {
    const chatDiv = this.chatbotContentsWrap.nativeElement;
    requestAnimationFrame(()=>{
      this.cm.scrollDownEle(this.chatbotContentsWrap);
      this.focusChattingFooterInput();
    })
  }

  @ViewChild('chattingFooterInput')
  chattingFooterInput: ElementRef;
  focusChattingFooterInput() {
    this.chattingFooterInput.nativeElement.focus();
  }

  private _chatbotTestAvailable: boolean = false;
  get chatbotTestAvailable(): boolean {
    return this._chatbotTestAvailable;
  }
  @Input() set chatbotTestAvailable(v: boolean) {
    if(this._chatbotTestAvailable == v){
      return;
    }
    this._chatbotTestAvailable = v;
    // FIXME 
    // 테스트 탭은 항상 디버그 창이 열려있도록함.
    // if(v) {
      // const isDebug = true;
      // this.ds.sendData(this.ds.CMD.IS_DEBUG, {isDebug});
    // }
  }
  

  private _chatDisable = true;
  set chatDisable(v) {
    if(this._chatDisable == v) {
      return;
    }
    this._chatDisable = v;
  }
  get chatDisable() {
    if(this.isQna) {
      return false;
    }
    if(this.isScenario) {
      return false;
    }
    let res = true;
    res = this.cm.isEmpty(this.currChatInfo) || this.chatbotWorking;
    return res;
  }











  //==============================================


  inputHist = [];
  inputHistIdx = -1;
  // private inputHist = [];
  // private inputHistIdx = -1;
  private HIS_MIN = 0;
  private HIS_MAX = 10;
  private inputHistIdxSet() {
    this.inputHistIdx = this.inputHist.length -1;
  }
  private inputHistGetPrev(): string {
    const res = this.inputHistGet();
    if(this.inputHistIdx > this.HIS_MIN) {
      this.inputHistIdx--;
    }
    return res;
  }
  private inputHistGetNext(): string {
    if(this.inputHist.length > this.inputHistIdx) {
      this.inputHistIdx++;
    }
    return this.inputHistGet();
  }
  private inputHistGet(): string {
    if(this.inputHistIdx == -1) {
      this.inputHistIdxSet();
    }
    return this.inputHist[this.inputHistIdx] || '';
  }
  private inputHistAdd(v: string) {
    if(this.inputHist.length > this.HIS_MAX) {
      this.inputHist = this.inputHist.slice(1, this.HIS_MAX);
    }
    if(this.cm.isEmpty(v)) {
      this.inputHistIdxSet();
      return;
    }
    this.inputHist.push(v);
    this.inputHistIdxSet();
  }



}
