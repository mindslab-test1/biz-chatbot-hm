import { Component, Input } from '@angular/core';
import { Chatbot_info } from '../../../../../adminsdk';
import { ChatbotInfoService } from '../../../../../service/chatbot-info.service';

declare var $: any;

@Component({
  selector: 'app-chatting-meta',
  templateUrl: './chatting.meta.component.html',
  styleUrls: ['./chatting.meta.component.scss'
    , '../../shared/chatbot-shared.component.scss'
  ]
})

export class ChattingMetaComponent {
  constructor(
    private chatbotInfoService: ChatbotInfoService
  ) {
  }

  @Input() message = '';
  botImg = "../../../../../assets/images/login_bg.jpg";
  @Input() data: Chatbot_info;
  @Input() entity;
}
