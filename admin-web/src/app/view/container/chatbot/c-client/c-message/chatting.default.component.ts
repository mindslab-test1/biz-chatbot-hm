import { Component, Input, ViewChild, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../../../../../service/data.service';
import { Chatbot_info } from '../../../../../adminsdk';
import { CommonUtilService } from '../../../../../service';
import { ChatbotInfoService } from '../../../../../service/chatbot-info.service';
import { ChatbotDialogService } from '../../../../../service/chatbot-dialog.service';
import { RespDialog } from '../../../../../model/response-dialog';
import { Msg, lang } from '../../../../../model/message';
import {MessageType} from '../../../../../model/chatbot-message';

declare var $: any;

@Component({
  selector: 'app-chatting-default',
  templateUrl: './chatting.default.component.html',
  styleUrls: ['./chatting.default.component.scss'
  , '../../shared/chatbot-shared.component.scss'
  ]
})

export class ChattingDefaultComponent implements OnInit, OnDestroy {
  constructor(
      private ds: DataService
    , private cm: CommonUtilService
    , private chatbotInfoService: ChatbotInfoService
    , private dialogService: ChatbotDialogService
  ) {
  }

  s = this.chatbotInfoService.set;
  d = this.chatbotInfoService.default;

  // nowLoading = true;
  private NO_MESSAGE = `${Msg.com[lang].tagAdmin}message 설정 없음.`;
  private _message = '';
  private _messageType = MessageType.TEXT;

  private _defaultBotImg = '../../../../../assets/images/chatbot-default.png';
  private _botImg = this._defaultBotImg;

  private _data;
  private _entity: RespDialog;
  @Input() chatbotWorking = false;

  @ViewChild('botImgEle')
  botImgEle: ElementRef;

  ngOnInit() {
    this.initEvents();
  }

  ngOnDestroy() {
    this.ds.clearData();
  }


  @Input() set message(v) {

    if (v === '') {
      // this.nowLoading = true;
      this._message = v || this.NO_MESSAGE;
    } else {
      // this.nowLoading = false;
      this._message = v;
    }

    // this._message = v;
    // this.nowLoading = false;
  }
  get message() {
    return this._message;
  }

   @Input() set messageType(v) {
     this._messageType = v;
   }
   get messageType() {
     return this._messageType;
   }

   get nowLoading() {
     let nowLoading = false;
     if (this.message === '' || this.message === this.NO_MESSAGE) {
       if (this.messageType === MessageType.TEXT) {
         nowLoading = true;
       } else {
         this.message = '요청하신 정보입니다.';
       }
     }
     return nowLoading;
   }

  get botImg(): string {
    return this._botImg;
  }
  set botImg(v) {
    if (v === this._defaultBotImg) {
      this._botImg = v;
      return;
    }

    this._botImg = this.chatbotInfoService.getThumbImgPath(v);
  }

  get data(): Chatbot_info {
    return this._data;
  }
  @Input() set data(v) {
    this._data = v;
    if (this.cm.isNotEmpty(v)
        && this.cm.isNotEmpty(v.img_path)
    ) {
      this.botImg = v.img_path;
    }
  }

  get entity(): RespDialog {
    return this._entity;
  }
  @Input() set entity(v) {
    this._entity = v;
  }

  selectDialog(event) {
    this.dialogService.changeDebug(this.entity);
  }

  initEvents() {
    this.botImgEle.nativeElement.onload = () => {
      if (this.botImg === this._defaultBotImg) {
        return true;
      }

      const _width = this.botImgEle.nativeElement.naturalWidth;
      const _height = this.botImgEle.nativeElement.naturalHeight;

      const { width, height } = this.chatbotInfoService.calcResizeWH(_width, _height, this.s.chat_max_size_view);
      this.botImgEle.nativeElement.width = width;
      this.botImgEle.nativeElement.height = height;
    };
  }
}
