import { Component, Input } from '@angular/core';
import { Chatbot_info } from '../../../../../adminsdk';

declare var $: any;

@Component({
  selector: 'app-chatting-client',
  templateUrl: './chatting.client.component.html',
  styleUrls: ['./chatting.default.component.scss'
  , '../../shared/chatbot-shared.component.scss'
  ]
})

export class ChattingClientComponent {

  constructor() {
  }
  
  @Input() message: string;
  @Input() data: Chatbot_info;
  @Input() entity;
}
