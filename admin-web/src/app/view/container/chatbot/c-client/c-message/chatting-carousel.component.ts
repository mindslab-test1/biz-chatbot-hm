import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Chatbot_info } from '../../../../../adminsdk';
import { ChatbotRichCarousel, RespDialog } from '../../../../../model/response-dialog';
import { CommonUtilService, CommonModalService } from '../../../../../service';
import { Msg, lang } from '../../../../../model/message';
import { ChatbotDialogService } from '../../../../../service/chatbot-dialog.service';

@Component({
  selector: 'app-chatting-carousel',
  templateUrl: './chatting-carousel.component.html',
  styleUrls: ['./chatting-carousel.component.scss']
})
export class ChattingCarouselComponent implements OnInit {

  constructor(
      private cm: CommonUtilService
    , private modal: CommonModalService
    , private dialogService: ChatbotDialogService
  ) {
  }
  container_sds_img = 'sds-img';

  ngOnInit() {

  }

  @Input() message = '';
  @Input() data: Chatbot_info;

  _entity: RespDialog;
  get entity(): RespDialog {
    return this._entity;
  }
  @Input() set entity(v) {
    if (this.cm.isEmpty(v)
      || this.cm.isEmpty(v.uttrData)
      || this.cm.isEmpty(v.uttrData.richContent)
    ) {
      return;
    }
    this._entity = v;
    if (this.cm.isEmpty(v.uttrData.richContent.carousels)) {
      return;
    }
    this.carousels = v.uttrData.richContent.carousels;
  }

  carousels: Array<ChatbotRichCarousel> = [];

  @Output() sendMessageHandler = new EventEmitter<string>();

  @Input() chatbotWorking: boolean = false;

  selectDialog($event) {
    this.dialogService.changeDebug(this.entity);
  }
  
  selectCarousel($event, item: ChatbotRichCarousel) {
    if (this.chatbotWorking) {
      this.modal.openErrorAlert(Msg.com[lang].nowLoadingChatbot);
      return false;
    }
    this.sendMessageHandler.emit(item.userUtter);
  }
}
