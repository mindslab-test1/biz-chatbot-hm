import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ChatbotRichImage, RespDialog} from '../../../../../model/response-dialog';
import { CommonUtilService, CommonModalService } from '../../../../../service';
import { Chatbot_info } from '../../../../../adminsdk';
import { ChatbotDialogService } from '../../../../../service/chatbot-dialog.service';
import { lang, Msg } from '../../../../../model/message';
import {WindowRef} from '../../../shared/windowRef';
import {FileService} from '../../../../../service/file.service';

declare var $: any;

@Component({
  selector: 'app-chatting-image',
  templateUrl: './chatting.image.component.html',
  styleUrls: ['./chatting.image.component.scss'
  , '../../shared/chatbot-shared.component.scss'
  ]
})

export class ChattingImageComponent {
  constructor(
      private cm: CommonUtilService
    , private modal: CommonModalService
    , private dialogService: ChatbotDialogService
    , private windowRef: WindowRef
    , private fileService: FileService
  ) {
  }

  container_sds_img = 'sds-img';

  @Input() message = '';
  @Input() data: Chatbot_info;

  _entity: RespDialog;
  get entity(): RespDialog {
    return this._entity;
  }
  @Input() set entity(v) {
    if (this.cm.isEmpty(v)
      || this.cm.isEmpty(v.uttrData)
      || this.cm.isEmpty(v.uttrData.richContent)
    ) {
      return;
    }
    this._entity = v;
    if (this.cm.isEmpty(v.uttrData.richContent.image)) {
      return;
    }
    this._image = v.uttrData.richContent.image;
  }

  _image: ChatbotRichImage;
  get image(): ChatbotRichImage {
    return this._image;
  }

  @Output() sendMessageHandler = new EventEmitter<string>();

  @Input() chatbotWorking = false;

  selectDialog(event) {
    this.dialogService.changeDebug(this.entity);
  }
  selectImage($event) {
    if (this.chatbotWorking) {
      this.modal.openErrorAlert(Msg.com[lang].nowLoadingChatbot);
      return false;
    }

    this.windowRef.nativeWindow.open(this._image.linkUrl);
  }

  get imagePath(): string {
    let res = null;
    const imagePath = this.image.imagePath;

    if (imagePath.startsWith('http')) {
      res = imagePath;
    } else {
      res = this.fileService.getDownloadPath(this.container_sds_img, imagePath);
    }

    return res;
  }
}
