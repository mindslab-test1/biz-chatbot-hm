import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ChatbotButton, RespDialog } from '../../../../../model/response-dialog';
import { CommonUtilService, CommonModalService } from '../../../../../service';
import { Chatbot_info } from '../../../../../adminsdk';
import { ChatbotDialogService } from '../../../../../service/chatbot-dialog.service';
import { lang, Msg } from '../../../../../model/message';

declare var $: any;

@Component({
  selector: 'app-chatting-button',
  templateUrl: './chatting.button.component.html',
  styleUrls: ['./chatting.button.component.scss'
  , '../../shared/chatbot-shared.component.scss'
  ]
})

export class ChattingButtonComponent {
  constructor(
      private cm: CommonUtilService
    , private modal: CommonModalService
    , private dialogService: ChatbotDialogService
  ) {
  }

  @Input() message = '';
  @Input() data: Chatbot_info;

  _entity: RespDialog;
  get entity(): RespDialog {
    return this._entity;
  }
  @Input() set entity(v) {
    if (this.cm.isEmpty(v)
      || this.cm.isEmpty(v.uttrData)
      || this.cm.isEmpty(v.uttrData.richContent)
    ) {
      return;
    }
    this._entity = v;
    if (this.cm.isEmpty(v.uttrData.richContent.buttons)) {
      return;
    }
    this._buttons = v.uttrData.richContent.buttons;
  }
  
  _buttons: ChatbotButton[] = [];
  get buttons(): ChatbotButton[] {
    return this._buttons;
  }

  @Output() sendMessageHandler = new EventEmitter<string>();

  @Input() chatbotWorking: boolean = false;

  selectDialog(event) {
    this.dialogService.changeDebug(this.entity);
  }

  
  selectButton($event, item: ChatbotButton) {
    if (this.chatbotWorking) {
      this.modal.openErrorAlert(Msg.com[lang].nowLoadingChatbot);
      return false;
    }
    this.sendMessageHandler.emit(item.userUtter);
  }

  private chattingScroll() {
    setTimeout(() => {
      const chatDiv = $('.chatbot-contents-wrap');
      chatDiv.scrollTop(chatDiv.prop('scrollHeight'));
    }, 0);
  }
}
