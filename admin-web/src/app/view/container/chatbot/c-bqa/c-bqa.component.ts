import {Component, OnInit, ViewChild, OnDestroy, Input, AfterViewInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonModalService, CommonUtilService } from '../../../../service';
import { DataService } from '../../../../service/data.service';
import {Chatbot_info, Cm_user, LoopBackFilter, Cm_domainApi, Chatbot_config, Cm_domain, Chatbot_config_domain_relApi, Chatbot_config_domain_rel, Chatbot_msg, Cm_workspace_teamApi, Cm_workspace_team} from '../../../../adminsdk';
import { Subscription } from 'rxjs';
import { environment as env } from '../../../../environments/environment';
import { ChatbotUpsertService } from '../../../../service/chatbot-upsert.service';
import { UserService } from '../../../../service/user.service';
import { ValidatorService } from '../../../../service/validator.service';
import { ChatbotInfoService } from '../../../../service/chatbot-info.service';
import { Msg, lang } from '../../../../model/message';
import { DateService } from '../../../../service/date.service';
import { EventEmitterHelper } from '../../../../model/event-emitter-helper';

@Component({
  selector: 'app-c-bqa',
  templateUrl: './c-bqa.component.html',
  styleUrls: [
      './../chatbot-container.component.scss'
    , './c-bqa.component.scss'
  ]
})
export class CBqaComponent implements OnInit, OnDestroy, AfterViewInit {

  SEARCH_TYPE_DB_SOLRAND = 1;
  SEARCH_TYPE_DB_SOLRAND_SOLROR = 4;

  private _checkboxFlag = false;
  get checkboxFlag(): boolean {
      return this._checkboxFlag;

  }
  set checkboxFlag(v: boolean) {
    if (this.service.isUpdate(this.bot_id)) {
    this._checkboxFlag = v;
      if (this._checkboxFlag) {
        this.data.chatbotConfigs[0].bqa_search_flow_type = this.SEARCH_TYPE_DB_SOLRAND_SOLROR;
      } else {
        this.data.chatbotConfigs[0].bqa_search_flow_type = this.SEARCH_TYPE_DB_SOLRAND;
        this.data.chatbotConfigs[0].bqa_search_solr_or_match_range = 0;
        this.data.chatbotConfigs[0].bqa_reconfirm_msg = '';
      }
    }
  }
  private _matchRange = 0;
  get matchRange(): number {
    return this._matchRange;
  }
  set matchRange(v: number) {
    if (this.service.isUpdate(this.bot_id)) {
      const temp = this._matchRange;
      this._matchRange = v;
      if (temp !== v) {
        this.data.chatbotConfigs[0].bqa_search_solr_or_match_range = v;
        this.data = this.data;
      }
    } else {
      this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
    }
  }
  private _bqa_reconfirm_msg = '';

  get bqa_reconfirm_msg(): string {
    return this._bqa_reconfirm_msg;
  }
  set bqa_reconfirm_msg(v: string) {
    const temp = this._bqa_reconfirm_msg;
    this._bqa_reconfirm_msg = v;
    if (temp !== v) {
      this.data.chatbotConfigs[0].bqa_reconfirm_msg = v;
      this.data = this.data;
    }
  }

  constructor(
    private router: Router
    , private activatedRoute: ActivatedRoute
    , private commonModalService: CommonModalService
    , private cm: CommonUtilService
    , private ds: DataService
    , private dateService: DateService
    , private userService: UserService
    , private configDomainApi: Chatbot_config_domain_relApi
    , private domainApi: Cm_domainApi
    , private service: ChatbotUpsertService
    , private chatbotInfoService: ChatbotInfoService
    , private validator: ValidatorService
    , private workspaceRelApi: Cm_workspace_teamApi
  ) {

  }
  onRecommendChanged(e) {
    console.log(e);
    this.isSave = false;
  }
  ngOnInit() {
    this.init();
  }
  ngOnDestroy() {
    this.cm.isDev() && console.log('unsubscribe at destroy on container');
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
    this.ds.clearData();
  }
  ngAfterViewInit(): void {
    const treeObj = document.getElementsByClassName('ui-tree ui-corner-all')[0];

    // @ts-ignore
    treeObj.style.height = 'auto';
  }

  init() {

    this.setSubscription();
    this.activatedRoute.params.subscribe(params => {
      if (params.bot_id) {
        this.cm.isDev() && console.log('\n====\n' + params.bot_id + '\n====\n\n');
        this.bot_id = params.bot_id;
        this.getBotData();
        this.getList();
      } else {
        this.cm.isDev() && console.log('\n====\ninsert\n====\n\n');
        // this.ds.sendData(this.ds.CMD.INSERT,{});
      }
    });
  }
  subscription: Subscription;
  setSubscription() {
    this.subscription = this.ds.getData().subscribe(res => {
      if (!res) {
        return false;
      }

      const _resolveSave = (data) => {

        this.save();
      };
      const _resolveIsSave = (data) => {
        this.isSave = data.isSave;
      };
      const _resolveTitle = (data) => {
        if (!data.title) {
          // this.cm.isDev() && console.log('empty title')
        }
        this.title = data.title;
      };

      // this.cm.isDev() && console.log('res',res);
      switch (res.cmd) {
        // case this.ds.CMD.INSERT:
        //   _resolveInsert(res.data);
        //   break;
        // case this.ds.CMD.GET:
        //   _resolveGet(res.data);
        //   break;
        case this.ds.CMD.SAVE:
          _resolveSave(res.data);
          break;
        case this.ds.CMD.IS_SAVE:
          _resolveIsSave(res.data);
          break;
        case this.ds.CMD.TITLE:
          _resolveTitle(res.data);
          break;
        default:
          // this.cm.isDev() && console.log('미처리 cmd');
          break;
      }
    }, err => {
      this.cm.isDev() && console.log('err', err);
    });
  }
  c = this.service.constants;
  d = this.chatbotInfoService.default;

  currPath = this.c.URL.c_bqa;

  private _bot_id = this.d.botId;
  @Input() set bot_id(v) {
    if (v === this.d.botId) {
      throw new Error('invalid bot_id');
    }
    this._bot_id = v;
  }
  get bot_id() {
    return this._bot_id;
  }

  private _isSave = true;
  set isSave(isSave: boolean) {
    if (this._isSave === isSave) {
      return;
    }
    this._isSave = isSave;
    this.ds.sendData(this.ds.CMD.IS_SAVE, {isSave});
  }
  get isSave() {
    return this._isSave;
  }

  private _title = '';
  set title(v) {
    if (this._title === v) {
      return;
    }
    this._title = v;
  }
  get title() {
    return this._title;
  }

  private _data: Chatbot_info;
  set data(v: Chatbot_info) {
    this._data = v;
  }
  get data() {
    return this._data;
  }

  onTree(emitData) {
    // this.cm.isDev() && console.log('emit tree', emitData);
    const selectedItem = emitData.data;
    switch (emitData.cmd) {
      case EventEmitterHelper.CMD.SELECT: this.treeSelect(selectedItem); break;

      default: throw new Error('violation cmd');
    }
  }


  getBotData() {
    this.isSave = true;
    this.service.getBotData(this.bot_id, (res: Chatbot_info) => {
      // filter
      if (this.cm.isEmpty(res)) {
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        return false;
      }
      if (this.cm.isEmpty(res.chatbotConfigs)) {
        this.commonModalService.openErrorAlert(`${Msg.err[lang].failLoadData} : chatbotConfigs`);
        return false;
      }

      // this.cm.isDev() && console.log('res',res);

      this.data = res;
       // set title
      this.title = res.bot_name;
      // set message
      const config: Chatbot_config = res.chatbotConfigs[0];
      // this.bqaTrees = [];

      if (config.bqa_search_flow_type === this.SEARCH_TYPE_DB_SOLRAND_SOLROR) {

        this.checkboxFlag = true;
        this.bqa_reconfirm_msg = config.bqa_reconfirm_msg;
        this.matchRange = config.bqa_search_solr_or_match_range;
      }

      this.selectedBqas = this.service.filterDomain(config);

      // this.cm.isDev() && console.log(this.selectedBqas);

      this.ds.sendData(this.ds.CMD.GET, this.data);
    }, err => {
      this.ignoreIsSaveAndGoToList();
    });
  }
  private ignoreIsSaveAndGoToList() {
    this.isSave = false;
    let i = 0;
    const close = setInterval(() => {
      this.isSave = true;
      if (this.isSave && i > 0) {
        clearInterval(close);
        this.commonModalService.openErrorAlert(Msg.err[lang].invalidAccess);
        this.router.navigate([env.container, this.c.URL.chatbot_management]);
      } else {
        i++;
        this.cm.isDev() && console.log('interval close');
      }
    }, 15);
  }

  workspaceId = this.userService.getWorkspaceIds().pop();
  teamIds = [];
  getList() {
    const filter: LoopBackFilter = {
       where: { workspace_id: this.workspaceId }
    };

    this.workspaceRelApi.find(filter).subscribe((res: Cm_workspace_team[]) => {
      this.cm.isDev() && console.log(res);
      if (res && res.length > 0) {
        for (let i = 0; i < res.length; i++) {
          this.teamIds.push(res[i].team_id);
        }
      }
      this.getTreeData();
    });
  }
  getTreeData() {
    const filter: LoopBackFilter = {
      where: {
        depth: 1,
        domain_id: {neq: 0},
        // team_id: this.userService.teamId,
        team_id: {inq: this.teamIds},
        workspace_id: this.workspaceId
      },
      include: 'children'
    };
    this.domainApi.count(filter.where).subscribe(res => {
      if (res.count <= 0) {
        this.cm.isDev() && console.log('no data');
        return false;
      }
      this.domainApi.find(filter).subscribe((res: Array<Cm_domain>) => {
        // this.cm.isDev() && console.log("res :", res)
        this.bqaTrees = res;
      });
    }, err => {
      this.commonModalService.openErrorAlert(err);
    });
  }

  userId = this.userService.getCurrentUserId();
  currUserInfo: Cm_user = this.userService.getCachedUserInfo();

  setData() {
    this.data.bot_name = this.title;
    this.data.team_id = this.userService.getTeamId();
    this.data.updated_dtm = this.dateService.getNowDate();
    this.data.updator_id = this.userId;
  }
  save() {
    if (this.service.isUpdate(this.bot_id)) {
      this.data.bot_id = this.bot_id;
      this.setData();
      if (!this.validate()) {
        return false;
      }
      this.service.updateBot(this.data, () => {
        this.updateDomains(() => {
          this.isSave = true;
          this.commonModalService.openSuccessAlert(Msg.com[lang].succUpdateData);
        });
      });
    } else {
      this.data = new Chatbot_info();
      this.data.use_yn = this.c.USE_YN.Y;
      this.data.created_dtm = this.dateService.getNowDate();
      this.data.creator_id = this.userId;

      this.setData();
      if (!this.validate()) {
        return false;
      }
      this.service.insertBot(this.data, (res) => {
        this.isSave = true;
        this.commonModalService.openSuccessAlert(Msg.com[lang].succUpdateData);

        const _setUpdate = (res: Chatbot_info) => {
          this.bot_id = res.bot_id;
          this.router.navigate([env.container, this.c.URL.chatbot_container, this.currPath, this.bot_id]);
        };
        _setUpdate(res);
      });
    }
  }
  updateDomains(callback = () => {}) {
    const conf_id = this.data.chatbotConfigs[0].conf_id;
    if (this.cm.isEmpty(conf_id)) {
      this.commonModalService.openErrorAlert('Invalid conf_id');
      return false;
    }
    const filter: LoopBackFilter = {
      where: {
        conf_id: conf_id
      }
    };
    const _getData = () => {
      const res = this.selectedBqas.map((v: Cm_domain) => {
        const domain_id = v.domain_id;
        return new Chatbot_config_domain_rel({conf_id, domain_id});
      });
      this.cm.isDev() && console.log('debug domains \n\n========================>\n', res);
      return res;
    };
    const data = _getData();

    this.configDomainApi.updatesRel(filter.where, data).subscribe(res => {
      this.cm.isDev() && console.log('res domains', res);
      callback();
    }, err => {
      this.commonModalService.openErrorAlert(Msg.err[lang].domainFailUpdate);
    });

  }
  validate() {
    if (this.validator.validate(this.data, {
      bot_name: {
        required: true,
        maxLength: 128
      }
    })) {
      return true;
    }
    return false;
  }
  moveDomain() {

    this.router.navigate(['management/domains/d-bqa']);
  }

  bqaTrees: Array<Cm_domain> = [];
  selectedBqas: Array<Cm_domain> = [];
  treeSelect(selectedItem) {
    this.cm.isDev() && console.log('selec', selectedItem);
    if (this.selectedBqas.some((selectedBqa) => this.service.equalDomain(selectedBqa, selectedItem))) {
      this.commonModalService.openErrorAlert(Msg.err[lang].domainAlreaySelection);
      return false;
    }
    const v = env._.clone(selectedItem);
    if (v.hasOwnProperty('parent')) {
      delete v.parent;
    }
    if (v.hasOwnProperty('children')) {
      delete v.children;
    }
    this.selectedBqas.push(v);
    this.selectedBqas.sort(this.service.sortDomain);
    this.data.chatbotConfigs[0].domains = this.selectedBqas;
    this.isSave = false;
  }
  treeDeselect(selectedItem) {
    this.cm.isDev() && console.log('desel', selectedItem);
    this.selectedBqas = this.selectedBqas.filter((selectedBqa) => this.service.notEqualDomain(selectedBqa, selectedItem));
    this.selectedBqas.sort(this.service.sortDomain);
    this.data.chatbotConfigs[0].domains = this.selectedBqas;
    this.isSave = false;
  }
}
