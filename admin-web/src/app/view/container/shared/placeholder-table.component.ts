import { Component, OnInit, Input } from '@angular/core';

export interface placeholderTableHeader {
  id: string,
  style: object
}
@Component({
  selector: 'pcholder-table',
  templateUrl: './placeholder-table.component.html',
  styleUrls: ['./placeholder-table.component.scss']
})
export class PlaceholderTableComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @Input() set listLength(v: number) {
    // for(let i = 0;i<v;i++){
    //   this.list.push("a");
    // }
    this.list = new Array(v);
  }
  private _list: any[] = ['a','a','a','a','a','a','a','a','a','a'];
  get list(): any[] {
    return this._list;
  }
  @Input() set list(v: any[]) {
    this._list = v;
  }
  @Input() header = [
    { id: "", style: {} },
    { id: "", style: {} },
    { id: "", style: {} },
    { id: "", style: {} },
    { id: "", style: {} },
    { id: "", style: {} },
    { id: "", style: {} },
    { id: "", style: {} }
  ];

}
