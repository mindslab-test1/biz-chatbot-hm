import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pcholder',
  templateUrl: './placeholder.component.html',
  styleUrls: ['./placeholder.component.scss']
})
export class PlaceholderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
