import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { TreeNode, Message } from 'primeng/components/common/api';
import { CommonUtilService } from '../../../service';
import { EventEmitterHelper } from '../../../model/event-emitter-helper';

@Component({
  selector: 'app-prime-tree',
  templateUrl: './prime-tree.component.html',
  styleUrls: ['./prime-tree.component.scss']
})
export class PrimeTreeComponent implements OnInit {

  msgs: Message[];
  _trees: TreeNode[] = [];
  get trees(): TreeNode[]{
    return this._trees;
  }
  @Input() set trees(v){
    this._trees = v;
  }
  @Input() key: string = '';

  private _defaultStyle = {border:0, fontSize: '12px', color: '#4c4c4c', 'overflow-y': 'auto'};
  _style = this._defaultStyle;
  get style() {
    return this._style;
  }
  @Input() set style(style) {
    this.cm.prop(this._style,style);
  }

  @Output() outer = new EventEmitter(true);

  constructor(
    private cm: CommonUtilService
  ) { }

  ngOnInit() {
  }

  onNodeSelected(event) {
    console.log('temp debug node select event',event);
    const data = event.node;

    this.outer.emit({cmd: EventEmitterHelper.CMD.SELECT, data});
    this.msgs = [];
    this.msgs.push({severity: 'info', summary: 'Node Selected', detail: event.node.label});
  }
}
