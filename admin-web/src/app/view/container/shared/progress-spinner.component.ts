import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-progress-spinner',
  templateUrl: './progress-spinner.component.html',
  styleUrls: ['./progress-spinner.component.scss']
})
export class ProgressSpinnerComponent implements OnInit {

  constructor() { }

  @Input() style: Object = {};
  @Input() fill = 'none';
  @Input() strokeWidth = '2';
  @Input() animationDuration = '.8s';

  ngOnInit() {
  }

}
