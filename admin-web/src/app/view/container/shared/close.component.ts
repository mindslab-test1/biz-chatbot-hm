import { Component, OnInit, ViewChild, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { CommonModalComponent } from '../../../common';
import { Router } from '@angular/router';
import { DataService } from '../../../service/data.service';
import { UserService } from '../../../service/user.service';
import { environment as env } from '../../../environments/environment';
@Component({
  selector: 'app-close',
  templateUrl: './close.component.html',
  styleUrls: ['./close.component.scss']
})
export class CloseComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router,
    private userService: UserService,
    private ds: DataService
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.ds.clearData();
  }

  @Output() outer = new EventEmitter(false);

  @ViewChild('modal')
  modal: CommonModalComponent;
  modalClose() {
    this.modal.modalClose();
  }
  private _data = { url: '' };
  private _url: string = '';
  get url() {
    return this._url;
  }
  @Input() set url(url) {


    this._data.url = url;


    this._url = url;
  }
  get data() {
    return this._data;
  }


  exit() {
    const cmd = this.ds.CMD.EXIT;
    const data = this.data;
    this.outer.emit({cmd, data});
    this.modalClose();
    if(data.url == env.root){
      this.userService.logout();
    }
  }
  save() {
    const cmd = this.ds.CMD.SAVE;
    const data = this.data;
    if(data.url == env.root){
      data.url = this.router.url;
    }
    this.outer.emit({cmd, data});
    this.modalClose();
  }

}
