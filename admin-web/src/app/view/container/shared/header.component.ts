import {
    AfterViewChecked,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    ViewChild
} from '@angular/core';
import {Router} from '@angular/router';
import {MenuItem} from 'primeng/primeng';
import {
    AlertModalEvent,
    AuthService,
    CommonModalService,
    CommonUtilService,
    MenuService,
    MessageExService
} from '../../../service';
import {
    Cm_roleApi,
    Cm_team,
    Cm_teamApi,
    Cm_user,
    Cm_userApi,
    Cm_workspace,
    Cm_workspace_team,
    Cm_workspace_teamApi,
    Cm_workspaceApi,
    LoopBackFilter
} from '../../../adminsdk';
import {environment as env} from '../../../environments/environment';
import {UserService} from '../../../service/user.service';
import {lang, Msg} from '../../../model/message';
import {Option} from '../../../model/dom-models';
import {DataService} from '../../../service/data.service';
import {Subscription} from 'rxjs';
import {CookieService} from 'ng2-cookies';
import {LastLoginInfoPopupComponent} from '../../adminLogin/lastLoginInfoPopup.component';
import {CommonModalComponent} from "../../../common";
import { environment } from '../../../environments/environment';

declare var $: any;
const _ = require('lodash');

@Component({
  selector: 'header-section',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit, AfterViewChecked, OnDestroy {
  public user: Cm_user;

  userId: any;
  currUserInfo: Cm_user;
  workspaceId: any;
  teamId: any;
  isAdmin = false;
  isSupervisor = false;CommonModalComponent;
  isServiceAdmin = false;
  isChange = false;
  isSelectedWorkspace = false;
  eventSubscription: Subscription;
  viewFlag = false;

  @ViewChild('loginInfoPop') loginInfoPop: LastLoginInfoPopupComponent;
  @ViewChild('changePasstDialogModal') changePasstDialogModal: CommonModalComponent;


  @Input() items: MenuItem[];
  constructor(
      private router: Router,
      private userService: UserService,
      private menuService: MenuService,
      private commonModalService: CommonModalService,
      private cm: CommonUtilService,
      private roleApi: Cm_roleApi,
      private workspaceApi: Cm_workspaceApi,
      private workspaceRelApi: Cm_workspace_teamApi,
      private teamApi: Cm_teamApi,
      private ds: DataService,
      private cookie: CookieService,
      private userApi: Cm_userApi,
      private authService: AuthService,
  ) {
    this.userId = this.userService.getCurrentUserId();
    this.user = this.currUserInfo = this.userService.getCachedUserInfo();
    this.workspaceId = this.userService.getWorkspaceIds().pop();
    if (this.cm.isEmpty(this.user)) {
      this.commonModalService.openErrorAlert(Msg.err[lang].invalidAccess);
      this.router.navigate([]);
    }
  }
  ngOnInit() {
    this.init();
  }
  ngAfterViewChecked() {
    $('.navi-menu .ui-breadcrumb-chevron').addClass('icon-right');
  }

  headerStyle = {};

  init() {

     let filter: LoopBackFilter = {};

     this._isSupervisor((res) => {

       if (res) {
         this.headerStyle = {'min-width': '985px'};
         filter = { where: { and: [  {  workspace_id: {neq: this.exclude.workspaceId} } ]  },  order : 'workspace_id' };
       }else{
         filter =  { where: { and: [  { team_id: this.user.team_id }  ]  },  order : 'workspace_id' };
       }

       this.workspaceRelApi.find(filter).subscribe((_res: Cm_workspace_team[])=>{
         console.log(_res);
         // supervisor 일 때

         if ( this.userService.workspaceId === '' ||
             typeof this.userService.workspaceId === 'undefined' ||
             typeof this.userService.workspaceId === null) {

           this.selectedWorkspaceId = _res[0].workspace_id;
         }else{

           this.selectedWorkspaceId = parseInt(this.userService.workspaceId, 10);
         }

         const isExistLogin = localStorage.getItem('isLogin');

         if (this.user.is_change_pass === 'N') {
             this.changePasstDialogModal.modalOpen();
         } else {
             if (!isExistLogin) {
                 localStorage.setItem('isLogin', 'yes');
                 this.openLoinInfoPop();
             }
         }

         this.initSupervisorHeader();

       });

     });

  }

  goDefaultPage() {
      const roleId = this.userService.getUserRoleId();

      if (roleId && roleId !== this.userService.getRoles.ADMIN) {
          this.router.navigateByUrl(env.defaultPath2);
      } else {
          this.router.navigateByUrl(env.defaultPath);
      }
  }


  openLoinInfoPop() {
        this.loginInfoPop.openDialog(this.userId);
        this.viewFlag = true;
  }

  public closePopup(event) {
      this.viewFlag = false;
  }

  private _isSupervisor(callback = (res: boolean): void => {}) {

    const item: Cm_user = this.currUserInfo;
    const _resolveSupervisor = () => {

      if (item.role_id === this.userService.getRoles.ADMIN) {
        this.isSupervisor = true;
        this.isServiceAdmin = true;

      } else if (item.role_id === this.userService.getRoles.SERVICE_ADMIN) {
        this.isSupervisor = false;
        this.isServiceAdmin = true;

      } else {
         this.isSupervisor = false;
        this.isServiceAdmin = false;
      }

       callback(this.isSupervisor);

    };

    this.roleApi.isAdmin(item.role_id, this.userId).subscribe((res: boolean)=>{
      this.isAdmin = res;
      if(res) {
        _resolveSupervisor();
      } else {
        callback(false);
      }
    }, err => {
      this.commonModalService.openErrorAlert(err);
    });
  }

  exclude = this.userService.exclude;
  superTeamName = '';

  setTeamSupervisor() {

    const _setTeamId = () => {
      /*const filter = {
        where: {
          and: [
            {
              workspace_id: this.selectedWorkspaceId
            }
          ]
        },
        order : 'workspace_id'
      };*/

       let filter: LoopBackFilter = {};

      if (this.isSupervisor) {
         filter = { where: { and: [  {  workspace_id: this.selectedWorkspaceId } ]  },  order : 'workspace_id' };
       }else{
         filter =  { where: { and: [  {  workspace_id: this.selectedWorkspaceId } , { team_id: this.user.team_id }  ]  },  order : 'workspace_id' };
       }


      console.log(filter);

      this.workspaceApi.find(filter).subscribe((list: Cm_workspace[]) => {

        if(typeof list ==='undefined' || list === null || list.length === 0){
          const item: Option = {
            label:'없음',
            value: -1
          };

          this.teamList.push(item);

        }else{
          this.teamList = list[0].teams.map((el: Cm_team) => {
            const item: Option = {
              label: el.team_name,
              value: el.team_id
            };
            return item;
          });
        }

        // 처음 페이지 진입시에는 없겠지....
        if ( this.userService.teamId === '' ||
             typeof this.userService.teamId === 'undefined' ||
             typeof this.userService.teamId === null){

          this.userService.teamId = this.user.team_id.toString();

        }else if(this.isChange){
           this.userService.teamId = this.teamList[0].value;
           this.isChange = false;
        }

        this.selectedTeamId = parseInt(this.userService.teamId);

        this.superTeamName = this.teamList.find(v => (v.value === this.selectedTeamId)).label;
        const selTeam = this.teamList[0];
        // console.log(selTeam);
        this.superTeamName = selTeam.label;


        //team 변경되야 이벤트가 발생하도록 함...비동기...
        if(this.isSelectedWorkspace) {
            this.userService.loadTeamEvent();
        }


      }, err => {
        this.commonModalService.openErrorAlert(err);
      });
    };

    _setTeamId();

  }

  initSupervisorHeader() {

    let filter: LoopBackFilter = {};

    if (this.isSupervisor) {
      filter = { where: { and: [  {  workspace_id: {neq: this.exclude.workspaceId} } ]  },  order : 'workspace_id' };
    }else{
      filter =  { where: { and: [  { team_id: this.user.team_id }  ]  },  order : 'workspace_id' };
    }

    this.workspaceApi.find(filter).subscribe((list: Cm_workspace[]) => {
      this.workspaceList = list.map((el: Cm_workspace)=>{
        const item:Option = {
          label: el.workspace_name,
          value: el.workspace_id
        };
        return item;
      });


      this.setTeamSupervisor();

    },err=>{
      this.commonModalService.openErrorAlert(err);
    });
  }


  private _defaultWorkspaceId = -1;
  private _selectedWorkspaceId: number = this._defaultWorkspaceId;
  get selectedWorkspaceId(): number {
    return this._selectedWorkspaceId || parseInt(this.userService.workspaceId);
  }
  set selectedWorkspaceId(v) {
    this._selectedWorkspaceId = v;
    this.userService.workspaceId = this.selectedWorkspaceId.toString();
  }
  private _defaultTeamId =  this.userService.getTeamId();
  private _selectedTeamId: number = this._defaultTeamId;
  get selectedTeamId(): number {
    return this._selectedTeamId || parseInt(this.userService.teamId);
  }
  set selectedTeamId(v) {
    this._selectedTeamId = v;
    this.userService.teamId = this.selectedTeamId.toString();
  }
  workspaceList: Option[] = [];
  onWorkspaceSelect(event){
    console.log('debug temp', this.selectedWorkspaceId, this.workspaceId, event);
    this.isChange = true;
    this.setTeamSupervisor();

    this.isSelectedWorkspace = true;
    this.eventSubscription = this.userService.isLoadTeamEvent.subscribe((res) => {
       if(this.router.url === env.defaultPath){
        //빈페이지를 만든이유는 동일 url 에서는 중복라우팅이 안됨.
        this.router.navigateByUrl(`blank`);
      }else{
        this.router.navigateByUrl(env.defaultPath);
      }

      this.isSelectedWorkspace = false;
    });

    /*setTimeout(() => {
      if(this.router.url === env.defaultPath){
        //빈페이지를 만든이유는 동일 url 에서는 중복라우팅이 안됨.
        this.router.navigateByUrl(`blank`);
      }else{
        this.router.navigateByUrl(env.defaultPath);``
      }
    }, 200);*/



  }


  teamList: Option[] = [];
  onTeamSelect(event){
    console.log('debug temp', this.selectedTeamId, this.teamId, event);

    if(this.router.url === env.defaultPath){
       //빈페이지를 만든이유는 동일 url 에서는 중복라우팅이 안됨.
       this.router.navigateByUrl(`blank`);
    }else{
        this.router.navigateByUrl(env.defaultPath);
    }
  }

  onGetUser($event) {
    this.router.navigateByUrl(`${env.container}/profile`);
  }

  onLogout($event) {

    if (this.userService.isSaveFlag === false) {
      let url = env.root;
      this.ds.sendData(this.ds.CMD.NOTI_EXIT, {isSave: false, url});
    } else {
      let outer = new EventEmitter<AlertModalEvent>();
      outer.subscribe(res => {
        if (res.isOk) {
          this.userService.logout(() => {
            this.logoutAfter();
          }, err => {
              this.logoutForce(err);
          });
        }
      }, err => {
        this.cm.isDev() && console.log('err', MessageExService.parseErrorMessage(err));
      });

      this.commonModalService.openAlertDialog(Msg.com[lang].titleConfirm, Msg.com[lang].confirmLogout, true, outer);
    }




  }
  private logoutAfter() {
    // this.menuService.removeResponseMenusFromLocalStorage();
    // this.userService.clearLocalUserInfoStoraage();
    this.cm.clearLocalStorage();
    this.router.navigateByUrl(env.root);
  }

  private logoutForce(err) {
    this.cm.isDev() && console.log(MessageExService.parseErrorMessage(err));
    this.logoutAfter();
  }

  ngOnDestroy() {
    if (typeof this.eventSubscription !== 'undefined' && this.eventSubscription) {
        this.eventSubscription.unsubscribe();
    }
  }


  @ViewChild('inputPass')
  inputPass: ElementRef;
  @ViewChild('confirmPass')
  confirmPass: ElementRef;


  onChangePassword(){
    const password = this.inputPass.nativeElement.value;
    const confirmPassword = this.confirmPass.nativeElement.value;

    if(this.validatePass(password, confirmPassword)) {
        this.userApi.checkLastPasswords(this.user.user_id, password,
                                        this.selectedWorkspaceId, this.selectedTeamId ).subscribe( res => {
           if (!res) {
             this.updateUserProfile(password);
           } else {
             this.commonModalService.openErrorAlert('이전에 사용한 암호입니다. 다른패스워드를 입력하십시오.');
           }
        });
    }
  }


  /* updateUserProfile(password) {
      const userData: Cm_user = this.user; //code list

       const filter: LoopBackFilter = {
          where: {
              user_id: this.user.user_id,
              team_id: this.user.team_id
          }
      }

      userData.password = password;

      this.userApi.upsertWithWhere(filter.where, userData).subscribe(res=>{
        this.userService.login({
                  username: this.user.user_id, password: password
              }).subscribe(subRes => {
                  this.cm.isDev() && console.log('login...');
                  this.menuService.init(()=>{
                      this.goHome();
                      this.changePasstDialogModal.modalClose();
                  }, subError => {
                      this.commonModalService.openErrorAlert(subError);
                  });
              }, error => {
                  this.authService.clear();
                  this.commonModalService.openErrorAlert(error);
              });
      }, err=>{
        this.commonModalService.openErrorAlert(err);
      });
  }*/


   updateUserProfile(password) {
      const userData: Cm_user = this.user; //code list

       const filter: LoopBackFilter = {
          where: {
              user_id: this.user.user_id,
              team_id: this.user.team_id
          }
      };

      this.userApi.initPassword(this.user.user_id, password,
                   this.selectedWorkspaceId, this.selectedTeamId).subscribe(res=>{

        this.userService.login({
                  username: this.user.user_id, password: password
              }).subscribe(subRes => {
                  this.cm.isDev() && console.log('login...');
                  this.menuService.init(()=>{
                      this.goHome();
                      this.changePasstDialogModal.modalClose();
                  }, subError => {
                      this.commonModalService.openErrorAlert(subError);
                  });
              }, error => {
                  this.authService.clear();
                  this.commonModalService.openErrorAlert(error);
              });
      }, err=>{
        this.commonModalService.openErrorAlert(err);
      });
  }


  goHome(): void {
    requestAnimationFrame(()=>{
      const user = this.userService.getCachedUserInfo();
      if (user) {
          const roleId = this.userService.getUserRoleId();

          if (roleId && roleId !== this.userService.getRoles.ADMIN) {
              this.router.navigateByUrl(environment.defaultPath2);
          } else {
              this.router.navigateByUrl(environment.defaultPath);
          }
      } else {
        this.router.navigateByUrl(environment.root);
      }
    });
  }



  validatePass(password, confirmPassword){
    if(password !=  confirmPassword) {
      this.commonModalService.openErrorAlert(`password를 동일하게 입력해주십시오.`);
      return false;
    }else {
        if (!this.checkPassword(password)) {
            this.commonModalService.openErrorAlert(`패스워드는 10자 이상 영문(대, 소문자), 숫자, 특수문자가 모두 포함되어야 합니다.'`);
            return false;
        }
    }
    return true;
  }


  checkPassword(value): boolean {
      const rexp  = /^.*(?=^.{10,}$)(?=.*\d)(?=.*[a-zA-Z])(?=.*[~,!,@,#,$,*,(,),=,+,_,.,|]).*$/;

      if (!rexp.test(value)) {
          return false;
      } else {
          return true;
        }
    }


}
