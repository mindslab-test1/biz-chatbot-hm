import { Component, OnInit, ViewChild, asNativeElements, ElementRef, Input, OnDestroy } from '@angular/core';
import { Cm_cd_groupApi } from '../../../../adminsdk/services/custom/Cm_cd_group';
import { LoopBackFilter, Cm_cd_group } from '../../../../adminsdk';
import { Router } from '@angular/router';
import { CommonUtilService, CommonModalService, } from '../../../../service';
import { Paginator, PaginatorComponent, PaginatorInterface } from '../../../../common/paginator/paginator.component';
import { empty } from 'rxjs/observable/empty';
import { CommonModalComponent } from '../../../../common';
import { Option } from '../../../../model/dom-models';
import { UserService } from '../../../../service/user.service';
import { Msg, lang } from '../../../../model/message';
import { DateService } from '../../../../service/date.service';
import { DataService } from '../../../../service/data.service';
const _ = require('lodash');


@Component({
  selector: 'app-common-code-groups',
  templateUrl: './common-code-groups.component.html',
  styleUrls: ['./common-code-groups.component.scss']
})
export class CommonCodeGroupsComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService,
    private ds: DataService,
    private dateService: DateService,
    private userService: UserService,
    private cmGroupApi: Cm_cd_groupApi,
  ) {
  }

  ngOnInit() {
    this.init();
  }
  ngOnDestroy() {
    this.ds.clearData();
  }
  @ViewChild('useOnly') useOnly: ElementRef;
  paginator = new Paginator();
  selectPageSize: string = this.paginator.DEFAULT_PAGE_SIZE.toString();
  searchStr = '';
  searchOption: LoopBackFilter = {};
 
  items: Cm_cd_group[];
  selectedItems: Cm_cd_group[] = [];
  selectedItem: Cm_cd_group;
  title = '공통 코드 그룹';
  primaryKey='group_code';


  private _isWorking: boolean = false;
  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if(this._isWorking == v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }



  init(){

    this.initList();
  }
  initList(){
    if(this.paginator.pageSize != this.getCurrPageSize()) {
      this.setPageSize(this.getCurrPageSize());
    }
    this.isWorking = true;
    this.getCount(()=>{
      this.isWorking = false;
    });
  }



  // action ====================================================

  onSearch() {
    this.isWorking = true;
    this.getList();
  }
  onSearchKey(event) {
    if(this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }

    // this.cm.isDev() && console.log('debug search key event',event);
    if (event.keyCode === 13) {
      this.isWorking = true;
      this.getList();
    } else {
      if(this.cm.isEmpty(this.searchStr) || this.cm.isEmpty(this.items)){
        // this.cm.isDev() && console.log('debug empty.getList');
        this.isWorking = true;
        this.getList();
      } else {
        // this.cm.isDev() && console.log('debug no empty set totcnt');
        this.setTotalCount(this.items.length);
      }
    }
  }

  onFilterYn = ($event) => {
    if($event.checked){
      // ??
    }
    this.isWorking = true;
    this.getList();
  };

  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    if(tempP == JSON.stringify(this.paginator)
      && this.paginator.totalPages != 1
    ){
      //갱신된 값이 없으면 다시 그리지 않음.
      return true;
    }
    if(this.paginator.totalCount <= 0) {
      return true;
    }
    this.isWorking = true;
    this._getList();
  }
  onChangePageSize(data) {
    this.selectPageSize = data.selectOption;
    this.initList();
  }




  // method ====================================================

  getCurrPageSize() {
    return Number(this.selectPageSize);
  }
  getFilter() {
    const filter: LoopBackFilter = {
      where: {},
      skip: this.searchOption.skip,
      limit: this.searchOption.limit,
      order: `${this.primaryKey} asc`
    };

    if(this.useOnly.nativeElement.checked){
      filter.where.use_yn = 'Y';
    }
    
    if(this.cm.isNotEmpty(this.searchStr)){
      filter.where.group_code = {like: '%' + this.searchStr + '%'};
    }
    return filter;
  }
  getCount(callback?) {
    const filter = this.getFilter();

    this.cmGroupApi.count(filter.where).subscribe(res=>{
      //filter
      if(this.cm.isEmpty(res)) {
        this.isWorking = false;
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        return false;
      }
      if(res.count <= 0) {
        console.log('no data');
        this._setItems([]);
        this.isWorking = false;
        this.setTotalCount(this.items.length);
        return false;
      }

      //logic
      const count = res.count;
      this.setTotalCount(count);


      callback && callback();


    }, err=>{
      this.commonModalService.openErrorAlert(err);
    });
  }

  getList() {
    this.isWorking = true;
    this.getCount(()=>{
      if(this.paginator.totalCount <= 0) {
        this._setItems([]);
        this.isWorking = false;
        // return false;
      }
      this._getList();
    });

  }
  private _getList = () => {
    this.setSearchOption();
    const filter = this.getFilter();
    this.cmGroupApi.find(filter).subscribe(res=>{
      if(res){
        this.isWorking = false;
        this._setItems(res);
      }
      this.isWorking = false;
    }, err=>{
      this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  };
  private _setItems = (_res)=>{
    this.items = _res;
  };
  
  setSearchOption(){
    this.searchOption.skip = this.paginator.pageIndex * this.paginator.pageSize;
    this.searchOption.limit = this.paginator.pageSize;
  }
  modelSettingOpen(model: Cm_cd_group) {
    console.log('debug update',model);
  }


  setTotalCount(count: number){
    this.paginator.totalCount = count;
  }
  setPageSize(pageSize: number){
    this.paginator.pageSize = pageSize;
  }








  // view item upsert ====================================================

  @ViewChild('upsertDialogModal')
  upsertDialogModal: CommonModalComponent;

  @ViewChild('removeDialogModal')
  removeDialogModal: CommonModalComponent;
  
  @ViewChild('upsertDialogGroupCode')
  upsertDialogGroupCode: ElementRef;
  @ViewChild('upsertDialogName')
  upsertDialogName: ElementRef;
  @ViewChild('upsertDialogDescription')
  upsertDialogDescription: ElementRef;
  @ViewChild('upsertDialogUseYn')
  upsertDialogUseYn: ElementRef;
  _useYn:boolean = true;
  get useYn() {
    return this._useYn;
  }
  set useYn(useYn) {
    this._useYn = useYn;
  }
  selectedYn() {
    return this._useYn ? 'Y' : 'N';
  }
  isUse(useYn) {
    return (useYn == 'Y') ? true : false;
  }

  userId = this.userService.getCurrentUserId();

  groupList: Option[] = [];
  data: Cm_cd_group; //code list
  initUpsertDialog(){
    
    this.cmGroupApi.find( {'order' : 'group_code', 'where': {'use_yn':'Y'}} )
      .subscribe((list: Cm_cd_group[]) => {

        this.groupList = list.map((el: Cm_cd_group)=>{
          const item:Option = {
            label: el.code_name,
            value: el.group_code
          };
          return item;
        });
      },error=>{
        this.commonModalService.openErrorAlert(error); 
      });
  }
  onGroupListSelect(event){
    console.log(event);

  }

  /** action callbacks **/
  isUpdate: boolean = false;
  addOpen() {
    this.isUpdate = false;
    this.data = new Cm_cd_group();
    this.initUpsertDialog();
    
    this.useYn = true;
    this.upsertDialogGroupCode.nativeElement.value = '';
    this.upsertDialogName.nativeElement.value = '';
    this.upsertDialogDescription.nativeElement.value = '';
    this.upsertDialogModal.modalOpen();
  }
  updateOpen(item: Cm_cd_group) {
    this.isUpdate = true;
    this.data = item;
    this.initUpsertDialog();
    
    this.useYn = this.isUse(item.use_yn);
    this.upsertDialogGroupCode.nativeElement.value = item.group_code;
    this.upsertDialogName.nativeElement.value = item.code_name;
    this.upsertDialogDescription.nativeElement.value = item.descript;
    this.upsertDialogModal.modalOpen();
  }
  removeOpen(item: Cm_cd_group) {
    console.log(item);
    if (this.cm.isEmpty(item)) {
      return;
    }
    this.selectedItem = item;
    this.removeDialogModal.modalOpen();
  }

  removesOpen(event) {
    const items = this.selectedItems;
    console.log('debug set remove',event,items);
    console.log(this.selectedItems);
    if (this.cm.isEmpty(items)) {
      console.log('debug remove. stop. no select items');
      return;
    }
    this.removeDialogModal.modalOpen();
  }



  add(event) {
    console.log('add event =>',event);
    this.data.created_dtm = this.dateService.getNowDate();
    this.data.creator_id = this.userId;
    this._setParams();
    this.validate(()=>{

      this.cmGroupApi.create(this.data).subscribe(
      res => {
        alert( 'Created.');
        this.upsertDialogModal.modalClose();
        this.getList();
      }, err => {
        this.commonModalService.openErrorAlert( 'Can\'t create new detail code. Please try again.');
      });

    });
  }
  update(event) {
    console.log('update event =>',event);
    this.data.updated_dtm = this.dateService.getNowDate();
    this.data.updator_id = this.userId;
    this._setParams();
    this.validate(()=>{

      this.cmGroupApi.updateAttributes(this.data.group_code, this.data).subscribe(
      res => {
        alert( 'Updated.');
        this.upsertDialogModal.modalClose();
        this.getList();
      }, err => {
        this.commonModalService.openErrorAlert('upsert Error.');
      });

    });
  }
  private _setParams() {
    this.data.use_yn = this.selectedYn();
    this.data.group_code = this.upsertDialogGroupCode.nativeElement.value;
    this.data.code_name = this.upsertDialogName.nativeElement.value;
    this.data.descript = this.upsertDialogDescription.nativeElement.value;
  }
  validate(callback: any = ()=>{}) {

    const _validate=(data)=>{
      const Validator = {
        group_code: {
          required: true,
          minLength: 2,
          maxLength: 2,
        },
        code_name: {
          required: true
        },
        descript: {
        },
        use_yn: {
          required: true
        }
      };

      for(const id in Validator){
        const item = Validator[id];
        if (this.cm.isEmpty(data[id]) && item.required) {
          this.commonModalService.openErrorAlert(`${id}를 입력해주십시오.`);
          return false;
        }
        if (
             this.cm.isNotEmpty(item.minLength)
          && this.cm.isNotEmpty(item.maxLength)
          && item.minLength == item.maxLength
          && data[id].length != item.minLength
        ) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.minLength} 자리로 정확하게 입력해주십시오.`);
          return false;
        }
        if (this.cm.isNotEmpty(item.minLength) && data[id].length < item.minLength) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.minLength} 자리 이상 입력해주십시오.`);
          return false;
        }
        if (this.cm.isNotEmpty(item.maxLength) && data[id].length > item.maxLength) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.maxLength} 자리 이하 입력해주십시오.`);
          return false;
        }
      }
      return true;
    };

    if (_validate(this.data)) {
    } else {
      return false;
    }
    callback();
  }

  remove(event) {
    if(this.selectedItem){
      this.removeItem();
    } else {
      this.removesItem();
    }
  }

  private removeItem() {
    this.cmGroupApi.deleteById(this.selectedItem.group_code).subscribe(
      res => {
        if (res['count'] > 0) {
          alert('삭제되었습니다.');
          this.selectedItem = null;
          this.getCount();
          this.removeDialogModal.modalClose();
        }
      }, error => {
        console.log('delete Error');
    });
  }
  private removesItem() {
    const ids = this.selectedItems.reduce((res,ele)=>{
      res.push(ele.group_code);
      return res;
    },[]);
    if (this.cm.isEmpty(ids)){
      this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
      return false;
    }
    this.cmGroupApi.deleteByIds(ids).subscribe(
      res => {
        alert('삭제되었습니다.');
        this.selectedItems = [];
        this.getCount();
        this.removeDialogModal.modalClose();
      }, error => {
        console.log('delete Error');
    });
  }
}
