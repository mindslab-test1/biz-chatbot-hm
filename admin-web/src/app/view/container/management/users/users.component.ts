import { Component, OnInit, ViewChild, asNativeElements, ElementRef, Input, OnDestroy } from '@angular/core';
import {
    LoopBackFilter,
    Cm_userApi,
    Cm_user,
    Cm_roleApi,
    Cm_role,
    Cm_team,
    Cm_teamApi,
    Cm_workspace, Cm_workspaceApi
} from '../../../../adminsdk';
import { Router } from '@angular/router';
import { CommonUtilService, CommonModalService } from '../../../../service';
import { Paginator } from '../../../../common/paginator/paginator.component';
import { CommonModalComponent } from '../../../../common';
import { Option } from '../../../../model/dom-models';
import { UserService } from '../../../../service/user.service';
import { Msg, lang } from '../../../../model/message';
import { DateService } from '../../../../service/date.service';
import { environment as env } from '../../../../environments/environment';
import { DataService } from '../../../../service/data.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService,
    private ds: DataService,
    private dateService: DateService,
    private userService: UserService,
    private teamApi: Cm_teamApi,
    private roleApi: Cm_roleApi,
    private userApi: Cm_userApi,
    private workspaceApi: Cm_workspaceApi
  ) {
  }

  private _isWorking = false;
  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if (this._isWorking === v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }

  ngOnInit() {
    this.init();
  }
  ngOnDestroy() {
    this.ds.clearData();
  }
  @ViewChild('useOnly') useOnly: ElementRef;
  paginator = new Paginator();
  selectPageSize: string = this.paginator.DEFAULT_PAGE_SIZE.toString();
  searchStr = '';
  searchOption: LoopBackFilter = {};

  items: Cm_user[];
  selectedItems: Cm_user[] = [];
  selectedItem: Cm_user;

  adminDefaultPassword = env.adminDefaultPassword;

  workspaceId = this.userService.workspaceId;
  oldTeamId  = 0;
  is_change_pass = '';

  init() {
    this._isAdmin();
    this.initList();
  }
  initList() {
    if (this.paginator.pageSize !== this.getCurrPageSize()) {
      this.setPageSize(this.getCurrPageSize());
    }
    this.isWorking = true;
    this.getCount(() => {
      this.isWorking = false;
    });
  }
  _isAdmin = () => {
    const item: Cm_user = this.currUserInfo;
    this.roleApi.isAdmin(item.role_id, this.userId).subscribe(res => {
      this.isAdmin = res;
    }, err => {
      err && console.log('err', err.message);
    });
  };



  // action ====================================================

  onSearch() {
    this.isWorking = true;
    this.getList();
  }
  onSearchKey(event) {
    if (this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }
    // this.cm.isDev() && console.log('debug search key event',event);
    if (event.keyCode === 13) {
      this.isWorking = true;
      this.getList();
    } else {
      if (this.cm.isEmpty(this.searchStr) || this.cm.isEmpty(this.items)) {
        // this.cm.isDev() && console.log('debug empty.getList');
        this.isWorking = true;
        this.getList();
      } else {
        // this.cm.isDev() && console.log('debug no empty set totcnt');
        this.setTotalCount(this.items.length);
      }
    }
  }

  onFilterYn = ($event) => {
    if ($event.checked) {
      // ??
    }
    this.isWorking = true;
    this.getList();
  };

  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    if (tempP == JSON.stringify(this.paginator)
      && this.paginator.totalPages != 1
    ) {
      //갱신된 값이 없으면 다시 그리지 않음.
      return true;
    }
    if (this.paginator.totalCount <= 0) {
      return true;
    }

    this.isWorking = true;

    this._getList();
  }
  onChangePageSize(data) {
    this.selectPageSize = data.selectOption;
    this.initList();
  }




  // method ====================================================

  getCurrPageSize() {
    return Number(this.selectPageSize);
  }
  getFilter() {
    const filter: LoopBackFilter = {
      where: {
        team_id: this.userService.teamId,
        user_id: {neq: this.currUserInfo.user_id},
        role_id: this.getRoleFilter()
      },
      skip: this.searchOption.skip,
      limit: this.searchOption.limit,
      order: 'user_id'
    };

    if (this.useOnly.nativeElement.checked) {
      filter.where.activated_yn = 'Y';
    }

    if (this.cm.isNotEmpty(this.searchStr)) {
      filter.where.user_id = {like: '%' + this.searchStr + '%'};
    }
    return filter;
  }
  getCount(callback?) {
    const filter = this.getFilter();

    this.userApi.count(filter.where).subscribe(res => {
      //filter
      if (this.cm.isEmpty(res)) {
        this.isWorking = false;
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        return false;
      }
      if (res.count <= 0) {
        console.log('no data');
        this._setItems([]);
        this.isWorking = false;
        this.setTotalCount(this.items.length);
        return false;
      }

      //logic
      const count = res.count;
      this.setTotalCount(count);


      callback && callback();


    }, err => {
        this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  }

  getList() {
    this.isWorking = true;
    this.getCount(() => {
      if (this.paginator.totalCount <= 0) {
        this._setItems([]);
        this.isWorking = false;
        // return false;
      }
      this._getList();
    });

  }
  private _getList = () => {
    this.setSearchOption();
    const filter = this.getFilter();
    this.userApi.find(filter).subscribe(res => {
      if (res) {
        this._setItems(res);
        this.isWorking = false;
      }
      this.isWorking = false;
    }, err => {
      this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  };
  private _setItems = (res) => {
    this.items = res;
  };

  setSearchOption() {
    this.searchOption.skip = this.paginator.pageIndex * this.paginator.pageSize;
    this.searchOption.limit = this.paginator.pageSize;
  }
  modelSettingOpen(model: Cm_user) {
    // console.log('debug update',model);
  }


  setTotalCount(count: number) {
    this.paginator.totalCount = count;
  }
  setPageSize(pageSize: number) {
    this.paginator.pageSize = pageSize;
  }
  getRoleFilter() {
    const roleFilter = { nin : []};
    const roles = this.userService.getRoles;
    switch (this.currUserInfo.role_id) {
      case roles.ADMIN:
          // roleFilter.nin.push(roles.ADMIN);
          break;
      case roles.SERVICE_ADMIN:
          roleFilter.nin.push(roles.ADMIN);
          roleFilter.nin.push(roles.SERVICE_ADMIN);
          break;
    }
    return roleFilter;
  }








  // view item upsert ====================================================

  @ViewChild('upsertDialogModal')
  upsertDialogModal: CommonModalComponent;

  @ViewChild('removeDialogModal')
  removeDialogModal: CommonModalComponent;

  @ViewChild('upsertDialogUserId')
  upsertDialogUserId: ElementRef;
  @ViewChild('upsertDialogEmail')
  upsertDialogEmail: ElementRef;
  @ViewChild('upsertDialogName')
  upsertDialogName: ElementRef;
  @ViewChild('upsertDialogPassword')
  upsertDialogPassword: ElementRef;
  @ViewChild('upsertDialogPasswordConfirm')
  upsertDialogPasswordConfirm: ElementRef;
  @ViewChild('upsertDialogPasswordOrigin')
  upsertDialogPasswordOrigin: ElementRef;
  @ViewChild('upsertDialogActivatedYn')
  upsertDialogActivatedYn: ElementRef;
  @ViewChild('upsertDialogResetPassword')
  upsertDialogResetPassword: ElementRef;


  _activatedYn = true;
  get activatedYn() {
    return this._activatedYn;
  }
  set activatedYn(activatedYn) {
    this._activatedYn = activatedYn;
  }
  selectedYn() {
    return this._activatedYn ? 'Y' : 'N';
  }
  isActivated(activatedYn) {
    return (activatedYn == 'Y') ? true : false;
  }
  // isChangePassword(){
  //   return this.cm.isNotEmpty(this.upsertDialogPassword.nativeElement.value);
  // }
  // isNotChangePassword(){
  //   return !this.isChangePassword();
  // }


  userId = this.userService.getCurrentUserId();
  currUserInfo: Cm_user = this.userService.getCachedUserInfo();
  isAdmin = false;

  data: Cm_user; //code list

  // selectedTeam: Cm_team;
  teamList: Option[] = [];
  selectedTeamId = -1;
  // selectedGroup: Cm_role;
  groupList: Option[] = [];
  selectedGroupId = '';
  //isreSetPassword = false;

  initUpsertDialog() {
    const filterTeam: LoopBackFilter = {where : {and: [{workspace_id:  this.workspaceId}]}};

     this.workspaceApi.find(filterTeam).subscribe((list: Cm_workspace[]) => {

        this.teamList = list[0].teams.map((el: Cm_team) => {
          const item: Option = {
            label: el.team_name,
            value: el.team_id
          };
          return item;
        });

     }, error => {
      this.commonModalService.openErrorAlert(error);
    });



   /* this.teamApi.find( filterTeam ).subscribe((list: Cm_team[]) => {
      this.teamList = list.map((el: Cm_team)=>{
        const item:Option = {
          label: el.team_name,
          value: el.team_id
        };
        return item;
      });
    },error=>{
      this.commonModalService.openErrorAlert(error);
    });*/

    const filterRole: LoopBackFilter = {
      where : { role_id : this.getRoleFilter() },
      order : 'order asc'
    };
    this.roleApi.find( filterRole ).subscribe((list: Cm_role[]) => {
      this.groupList = list.map((el: Cm_role) => {
        const item: Option = {
          label: el.role_name,
          value: el.role_id
        };
        return item;
      });

      // mobis required
      for (let i = this.groupList.length - 1; i >= 0; i--) {
        if (this.groupList[i].value === 'CS_AGENT' || this.groupList[i].value === 'SERVICE_ADMIN') {
          this.groupList.splice(i, 1);
        }
      }

    }, error => {
      this.commonModalService.openErrorAlert(error);
    });
  }
  onTeamListSelect(event) {
      console.log(this.selectedTeamId);
      console.log(event);
    // console.log(event);
  }
  onGroupListSelect(event) {
    // console.log(event);

  }

  /** action callbacks **/
  isUpdate = false;
  addOpen() {

    this.isUpdate = false;
    this.data = new Cm_user();
    this.initUpsertDialog();

    this.selectedTeamId = this.userService.getTeamId();
    this.selectedGroupId = '';
    this.activatedYn = true;
    this.upsertDialogUserId.nativeElement.value = '';
    this.upsertDialogEmail.nativeElement.value = '';
    this.upsertDialogName.nativeElement.value = '';
    // this.upsertDialogPassword.nativeElement.value = '';
    // this.upsertDialogPasswordOrigin.nativeElement.value = '';
    // this.upsertDialogPasswordConfirm.nativeElement.value = '';
    this.upsertDialogModal.modalOpen();
  }
  updateOpen(item: Cm_user) {
    this.isUpdate = true;
    this.data = item;
    this.initUpsertDialog();

    this.selectedTeamId = item.team.team_id;
    this.oldTeamId = item.team.team_id;
    this.selectedGroupId = item.role_id;
    this.activatedYn = this.isActivated(item.activated_yn);
    this.upsertDialogUserId.nativeElement.value = item.user_id;
    this.upsertDialogEmail.nativeElement.value = item.email;
    this.upsertDialogName.nativeElement.value = item.user_name;
    this.is_change_pass = item.is_change_pass;
    // this.upsertDialogPassword.nativeElement.value = '';
    // this.upsertDialogPasswordOrigin.nativeElement.value = item.password;
    // this.upsertDialogPasswordConfirm.nativeElement.value = '';
    this.upsertDialogModal.modalOpen();
  }
  resetPassword(item: Cm_user) {
    this.commonModalService.openConfirmDialog(`<p>
      ${Msg.com[lang].confirmResetPassword}
      <br>※ password는 기본값 ${this.adminDefaultPassword} 으로 세팅 됩니다.
      </p>`
        , () => {
      this.userApi.setPasswordEx(
          item.user_id, this.adminDefaultPassword
      ).subscribe(res => {
        this.commonModalService.openSuccessAlert(Msg.com[lang].succUpdateData);
      }, err => {
        this.commonModalService.openErrorAlert(err);
      });
    }, true);
  }
  removeOpen(item: Cm_user) {
    if (this.cm.isEmpty(item)) {
      return;
    }
    this.selectedItem = item;
    this.removeDialogModal.modalOpen();
  }

  removesOpen(event) {
    const items = this.selectedItems;
    if (this.cm.isEmpty(items)) {
      // console.log('debug remove. stop. no select items');
      return;
    }
    this.removeDialogModal.modalOpen();
  }



  add(event) {
    if (!confirm('등록하시겠습니까?')) {
      return false;
    }

    this.data.created_dtm = this.dateService.getNowDate();
    this.data.creator_id = this.userId;
    // this.data.password = this.upsertDialogPassword.nativeElement.value || this.upsertDialogPasswordOrigin.nativeElement.value;
    this.data.password = this.adminDefaultPassword;
    this._setParams();
    this.validate(() => {

      this.userApi.create(this.data).subscribe(
      res => {
        alert( 'Created.');
        this.upsertDialogModal.modalClose();
        this.getList();
      }, err => {
        console.log(err);
        let msg = 'Can\'t create new detail code. Please try again.';
        if (err) {
          msg += `\nCause: ${err.message}`;
        }
        this.commonModalService.openErrorAlert(msg);
      });

    });
  }
  update(event) {
    if (!confirm('수정하시겠습니까?')) {
      return false;
    }

    this.data.updated_dtm = this.dateService.getNowDate();
    this.data.updator_id = this.userId;
    this.data.is_change_pass = this.is_change_pass;
    delete this.data.password;
    this._setParams();
    this.validate(() => {

      // this.userApi.updateAttributes(this.data.user_id, this.data).subscribe();

        console.log('orign teamId : ' + this.oldTeamId);

      const filter: LoopBackFilter = {
        where: {
          user_id: this.data.user_id,
          team_id: this.oldTeamId
        }
      };

      console.log(this.data);

      this.userApi.upsertWithWhere(filter.where, this.data).subscribe(res => {
        this.commonModalService.openSuccessAlert(Msg.com[lang].succUpdateData);
        this.upsertDialogModal.modalClose();
        this.getList();
      }, err => {
        this.commonModalService.openErrorAlert('upsert Error.');
      });

    });
  }


  initUserPassword() {

      if (!confirm('사용자 패스워드를 초기화 하시겠습니까?')) {
          return false;
      }

      this.isWorking = true;
      this.is_change_pass = 'N';

      this.userApi.setPasswordEx(
          this.data.user_id, this.adminDefaultPassword, this.workspaceId, this.selectedTeamId
      ).subscribe( re => {
          this.commonModalService.openSuccessAlert(Msg.com[lang].titleSuccess);
          this.isWorking = false;
      }, err => {
        this.isWorking = false;
        this.commonModalService.openErrorAlert(err);
      });
  }

  private _setParams() {
    this.data.team_id = this.selectedTeamId;
    this.data.role_id = this.selectedGroupId;
    this.data.user_id = this.upsertDialogUserId.nativeElement.value;
    this.data.email = this.upsertDialogEmail.nativeElement.value;
    this.data.user_name = this.upsertDialogName.nativeElement.value;
    this.data.activated_yn = this.selectedYn();

    // loopback 특성... 제거 필요..
    this.data.username = this.upsertDialogUserId.nativeElement.value;
  }
  validate(callback: any = () => {}) {

    const _validate = (data) => {
      // const passwordNoUpdate = this.cm.isEmpty(this.upsertDialogPassword.nativeElement.value)
      //     && this.cm.isNotEmpty(this.upsertDialogPasswordOrigin.nativeElement.value);

      const Validator = {
        user_id: {
          required: true,
          minLength: 4,
          maxLength: 20
        },
        team_id: {
          required: true
        },
        role_id: {
          required: true
        },
        email: {
          required: true
        },
        user_name: {
          required: true,
          minLength: 3,
          maxLength: 50
        },
        // password: {
        //   required: true,
        //   minLength: 6,
        //   maxLength: 14
        // },
        activated_yn: {
          required: true
        }
      };

      const regexp = /[\{\}\[\]\/?.,;:|\)*~`!^\-+<>@\#$%&\\\=\(\'\"]/gi;

        for (const id in Validator) {
        // if(id == 'password'){
        //   if(this.isNotChangePassword()){
        //     continue;
        //   }

        //   if(this.upsertDialogPassword.nativeElement.value != this.upsertDialogPasswordConfirm.nativeElement.value) {
        //     this.commonModalService.openErrorAlert(`password를 동일하게 입력해주십시오.`);
        //     return false;
        //   }
        // }
        const item = Validator[id];
        if (item.required  && ( data[id] === -1   || this.cm.isEmpty(data[id]))) {
          this.commonModalService.openErrorAlert(`${id}를 입력해주십시오.`);
          return false;
        }

        if (id !== 'email' && regexp.test(data[id])) {
          this.commonModalService.openErrorAlert(`${id}에 특수기호를 입력하실수 없습니다.`);
          return false;
        }


        if (
             this.cm.isNotEmpty(item.minLength)
          && this.cm.isNotEmpty(item.maxLength)
          && item.minLength === item.maxLength
          && data[id].length !== item.minLength
        ) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.minLength} 자리로 정확하게 입력해주십시오.`);
          return false;
        }
        if (this.cm.isNotEmpty(item.minLength) && data[id].length < item.minLength) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.minLength} 자리 이상 입력해주십시오.`);
          return false;
        }
        if (this.cm.isNotEmpty(item.maxLength) && data[id].length > item.maxLength) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.maxLength} 자리 이하 입력해주십시오.`);
          return false;
        }
      }

      // if(passwordNoUpdate) {
      // } else {
      //   if(this.upsertDialogPassword.nativeElement.value != this.upsertDialogPasswordConfirm.nativeElement.value) {
      //     this.commonModalService.openErrorAlert(`password를 동일하게 입력해주십시오.`);
      //     return false;
      //   }
      // }
      return true;
    };

    if (_validate(this.data)) {
    } else {
      return false;
    }
    callback();
  }

  remove(event) {
    if (this.selectedItem) {
      this.removesItem([this.selectedItem], () => {
        this.selectedItem = null;
      });
    } else {
      this.removesItem(this.selectedItems, () => {
        this.selectedItems = [];
      });
    }
  }

  private removesItem(selectedItems, callback) {
    if (this.cm.isEmpty(selectedItems)) {
      this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
      return false;
    }

    const ids = selectedItems.reduce((res, ele) => {
      res.push(ele.user_id);
      return res;
    }, []);
    if (this.cm.isEmpty(ids)) {
      this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
      return false;
    }

    this.userApi.deleteByIds(ids).subscribe(
    res => {
      alert('삭제되었습니다.');
      this.getCount();
      this.removeDialogModal.modalClose();
      this.getList();
    }, error => {
      console.log('delete Error');
    }, () => {
      callback();
    });
  }
}
