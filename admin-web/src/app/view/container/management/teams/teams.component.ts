import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
    Cm_roleApi,
    Cm_team,
    Cm_teamApi,
    Cm_user,
    Cm_workspace,
    Cm_workspace_teamApi,
    Cm_workspaceApi,
    LoopBackFilter
} from '../../../../adminsdk';
import {Router} from '@angular/router';
import {CommonModalService, CommonUtilService} from '../../../../service';
import {Paginator} from '../../../../common/paginator/paginator.component';
import {CommonModalComponent} from '../../../../common';
import {Option} from '../../../../model/dom-models';
import {UserService} from '../../../../service/user.service';
import {lang, Msg} from '../../../../model/message';
import {DateService} from '../../../../service/date.service';
import {DataService} from '../../../../service/data.service';

const _ = require('lodash');

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit, OnDestroy {

  paginator = new Paginator();
  selectPageSize: string = this.paginator.DEFAULT_PAGE_SIZE.toString();
  searchStr = '';
  searchOption: LoopBackFilter = {};

  items: Cm_team[];
  selectedItems: Cm_team[] = [];
  selectedItem: Cm_team;
  title = '팀';
  primaryKey='team_id';
  searchKey='workspace_id';
  teamList:Cm_team[] =[];

  userId = this.userService.getCurrentUserId();
  currUserInfo: Cm_user = this.userService.getCachedUserInfo();
  workspaceId = this.userService.workspaceId;
  isAdmin = false;
  isServiceAdmin = false;



  constructor(
    private router: Router,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService,
    private ds: DataService,
    private dateService: DateService,
    private userService: UserService,
    private workspaceApi: Cm_workspaceApi,
    private workspaceTeamApi: Cm_workspace_teamApi,
    private teamApi: Cm_teamApi,
    private roleApi: Cm_roleApi

  ) {
    this.userId = this.userService.getCurrentUserId();
    this.currUserInfo = this.userService.getCachedUserInfo();
  }

  private _isWorking: boolean = false;

  get isWorking() {
    return this._isWorking;
  }

  set isWorking(v: boolean) {
    if(this._isWorking == v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }

  ngOnInit() {
    this.init();
  }
  ngOnDestroy() {
    this.ds.clearData();
  }


  init(){
    this._isAdmin(()=>{
      this.initList();
    });
  }

  initList(){
    if(this.paginator.pageSize != this.getCurrPageSize()) {
      this.setPageSize(this.getCurrPageSize());
    }
    this.isWorking = true;
    this.getCount(()=>{
      this.isWorking = false;
    });
  }



  private _isAdmin = (callback) => {
    const item: Cm_user = this.currUserInfo;
    this.roleApi.isSupervisor(item.role_id).subscribe( isSuper => {
      this.isAdmin = isSuper;
      this.roleApi.isAdmin(item.role_id, this.userId).subscribe(res=>{
        if(!res) {
          this.commonModalService.openErrorAlert(Msg.err[lang].invalidAccess);
          return;
        }
        this.isServiceAdmin = res;

      }, err=>{
        this.commonModalService.openErrorAlert(err);
      });
      callback();

      }, err=>{
      this.commonModalService.openErrorAlert(err);
    });

  };



  // action ====================================================

  onSearch() {
    this.isWorking = true;
    this.getList();
  }
  onSearchKey(event) {
    if(this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }
    // this.cm.isDev() && console.log('debug search key event',event);
    // this.cm.isDev() && console.log('debug search key event keyCode',event.keyCode);
    if (event.keyCode === 13) {
      this.isWorking = true;
      this.getList();
    }/* else {
      if(this.cm.isEmpty(this.searchStr) || this.cm.isEmpty(this.items)){
        // this.cm.isDev() && console.log('debug empty.getList');
        this.isWorking = true;
        this.getList();
      } else {
        // this.cm.isDev() && console.log('debug no empty set totcnt');
        this.setTotalCount(this.items.length);
      }
    }*/
  }

  onFilterYn = ($event) => {
    if($event.checked){
      // ??
    }
    this.isWorking = true;
    this.getList();
  };

  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    // this.cm.isDev() && console.log('debug pageChange event'
    //     ,'\nbefore ==>\n',event.paginator
    //     ,'\n========================'
    //     ,'\nafter ==>\n',this.paginator
    //     ,'\n========================'
    // );
    if(tempP == JSON.stringify(this.paginator)
      && this.paginator.totalPages != 1
    ){
      //갱신된 값이 없으면 다시 그리지 않음.
      return true;
    }
    if(this.paginator.totalCount <= 0) {
      return true;
    }

    this.isWorking = true;
    this._getList();
  }
  onChangePageSize(data) {
    this.selectPageSize = data.selectOption;
    this.initList();
  }




  // method ====================================================

  getCurrPageSize() {
    return Number(this.selectPageSize);
  }


  exclude = this.userService.exclude;
  targetTeamIds = [];


  getFilter() {
    const filter: LoopBackFilter = {
      where: {
        and: [
          {
            workspace_id:  this.workspaceId
          }
        ]
      },
      skip: this.searchOption.skip,
      limit: this.searchOption.limit,
      order: `${this.searchKey} asc`
    };

    if(this.cm.isNotEmpty(this.searchStr)){
      filter.where[this.primaryKey] = {like: '%' + this.searchStr + '%'};
    }

    return filter;
  }


  getCount(callback?) {

   let searchText = null;

   if(this.cm.isNotEmpty(this.searchStr)){
      searchText =  this.searchStr;
    }

    let args =[
       this.userService.workspaceId,
      // this.userService.getWorkspaceIds(),
      searchText,
    ];


    this.teamApi.selectTeamByWorkspaceCount(...args).subscribe(res=>{
        if (res[0].cnt <= 0) {
          this.isWorking = false;
          this.setTotalCount(0);
          this.setPageSize(1);
          return false;
          // this.commonModalService.openErrorAlert('No data');
          // console.log('no data');
        }

        // const count = this.teamList.length;
        this.setTotalCount(res[0].cnt);

        callback && callback();

      }, err => {
        this.isWorking = false;
        this.commonModalService.openErrorAlert(err);
    });

  }

  getList() {
    this.isWorking = true;

    this.getCount(()=>{
      if(this.paginator.totalCount <= 0) {
        this._setItems([]);
        this.isWorking = false;
        // return false;
      }
      this._getList();
    });
  }
  private _getList = (callback = (): void=>{}) => {

    this.setSearchOption();
    let searchText = null;

    if(this.cm.isNotEmpty(this.searchStr)){
      searchText =  this.searchStr;
    }

    let args =[
       this.userService.workspaceId,
      // this.userService.getWorkspaceIds(),
      searchText,
      this.searchOption.skip,
      this.searchOption.limit,
    ];


    this.teamApi.selectTeamByWorkspace(...args).subscribe((list) => {

      if (!this.cm.isEmpty(list)) {
          this._setItems(list);
          this.isWorking = false;
          callback();
        this.isWorking = false;
      }

    }, err => {
      this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });



    /*this.teamApi.find(filter).subscribe((res:Cm_team[])=>{
      if(res){
        this._setItems(res);
        this.isWorking = false;
        callback();
      }
      this.isWorking = false;
    }, err=>{
      this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });*/
  };

  _setItems = (result:Cm_team[])=>{
    this.items = result;
  };

  setSearchOption(){
    this.searchOption.skip = this.paginator.pageIndex * this.paginator.pageSize;
    this.searchOption.limit = this.paginator.pageSize;
  }
  modelSettingOpen(model: Cm_team) {
    console.log('debug update',model);
  }



  setTotalCount(count: number){
    this.paginator.totalCount = count;
  }
  setPageSize(pageSize: number){
    this.paginator.pageSize = pageSize;
  }








  // view item upsert ====================================================

  @ViewChild('upsertDialogModal')
  upsertDialogModal: CommonModalComponent;

  @ViewChild('removeDialogModal')
  removeDialogModal: CommonModalComponent;

  @ViewChild('upsertDialogId')
  upsertDialogId: ElementRef;
  @ViewChild('upsertDialogName')
  upsertDialogName: ElementRef;
  @ViewChild('upsertDialogDescription')
  upsertDialogDescription: ElementRef;

  // selectedWorkspaceList: Cm_workspace[] = [];
  // selectedWorkspaceIds: number[] = [];
  selectedWorkspace: Cm_workspace;

  // _defaultWorkspaceId = -1;
  _defaultWorkspaceId = Number(this.workspaceId); // localStorage에 있는 workspaceId로 고정
  _selectedWorkspaceId: number = this._defaultWorkspaceId;
  get selectedWorkspaceId(): number {
    return this._selectedWorkspaceId;
  }
  set selectedWorkspaceId(v) {
    /* Workspace 자동고정으로 인해서 해당 부분 주석 처리 */
    // this._selectedWorkspaceId = v;
    // if(v == this._defaultWorkspaceId) {
    //   this.selectedWorkspace = new Cm_workspace();
    //   return ;
    // }
    this.selectedWorkspace = this.workspaceResponseList.find((ele:Cm_workspace)=>v == ele.workspace_id);
  }
  data: Cm_team; //code list
  initUpsertDialog(callback = ():void=>{}){
    this.upsertDialogModal.setModalStyle(795);
    // this.upsertDialogModal.setModalStyle(null,null,{background:'red'});


    const filter = {
      where: {
        and: [
          {
            workspace_id: this.workspaceId

          }
        ]
      },
      order : 'workspace_id'
    };

    this.workspaceApi.find( filter )
      .subscribe((list: Cm_workspace[]) => {
        this.workspaceList = this.workspaceResponseList = list;
        this.workspaceList2 = list.map((el: Cm_workspace)=>{
          const item:Option = {
            label: el.workspace_name,
            value: el.workspace_id
          };
          return item;
        });

        callback();
      },error=>{
        this.commonModalService.openErrorAlert(error);
      });
  }
  onGroupListSelect(event){
    console.log(event);

  }

  _defaultAction = (event)=>{};
  private _action = this._defaultAction;
  setAction(func: any) {
    this._action = func;
  }
  @HostListener('window:keyup', ['$event'])
  onKey(event: KeyboardEvent) {
    const resolve = {
      enter: (event) => {
        if(this.cm.equalFunction(this._action, this._defaultAction)) {
          return false;
        }
        this._action(event);
      }
    };
    switch (event.keyCode) {
      case 13:
        resolve.enter(event);
        break;
      default:
        // console.log('정의되지 않은 이벤트');
        break;
    }
  }

  /** action callbacks **/
  isUpdate: boolean = false;
  addOpen() {
    console.log('admin :' + this.isAdmin);
    this.isUpdate = false;
    this.data = new Cm_team();
    this.initUpsertDialog(()=>{
      // this.selectedWorkspaceIds = [];
      // this.selectedWorkspaceList = [];
      this.selectedWorkspaceId = this._defaultWorkspaceId;

      this.setAction(this.add);
      _open();
    });
    const _open = () => {
      this.upsertDialogId.nativeElement.value = '';
      this.upsertDialogName.nativeElement.value = '';
      this.upsertDialogDescription.nativeElement.value = '';
      this.upsertDialogModal.modalOpen();
    };
  }
  updateOpen(item: Cm_team) {
    //console.log(item);
    this.isUpdate = true;
    this.data = item;
    this.initUpsertDialog(()=>{
      const rels = this.data.workspaceRel;
      // this.selectedWorkspaceIds = rels.reduce((res,item:Cm_workspace_team)=>{
      //   res.push(item.workspace_id);
      //   return res;
      // },[]);
      // this.selectedWorkspaceList = this.workspaceResponseList.filter((ele:Cm_workspace)=>this.selectedWorkspaceIds.includes(ele.workspace_id));
      // this.workspaceList = this.workspaceResponseList.filter((ele:Cm_workspace)=>(!this.selectedWorkspaceIds.includes(ele.workspace_id)));
      this.selectedWorkspaceId = this.cm.isNotEmpty(rels) ? rels[0].workspace_id : this._defaultWorkspaceId;
      this.workspaceList = this.workspaceResponseList.filter((ele:Cm_workspace)=>(this.selectedWorkspaceId != ele.workspace_id));

      this.setAction(this.update);
      _open();
    });

    const _open = () => {
      this.upsertDialogId.nativeElement.value = item[this.primaryKey];
      this.upsertDialogName.nativeElement.value = item.team_name;
      this.upsertDialogDescription.nativeElement.value = item.descript;
      this.upsertDialogModal.modalOpen();
    };
  }
  removeOpen(item: Cm_team) {
    if (this.cm.isEmpty(item)) {
      return;
    }
    this.selectedItem = item;
    this.setAction(this.remove);
    this.removeDialogModal.modalOpen();
  }

  removesOpen(event) {
    const items = this.selectedItems;
    console.log('debug set remove',event,items);
    if (this.cm.isEmpty(items)) {
      console.log('debug remove. stop. no select items');
      return;
    }
    this.setAction(this.remove);
    this.removeDialogModal.modalOpen();
  }



  complete = {
    add: true,
    update: true
  };
  add(event) {
    if(!this.complete.add) {
      return false;
    }
    this.complete.add = false;

    // this.cm.isDev() && console.log('add event =>',event);
    this.data.created_dtm = this.dateService.getNowDate();
    this.data.creator_id = this.userId;
    this._setParams();
    this.validate(()=>{

      this.teamApi.create(this.data).subscribe(
      res => {
        this.commonModalService.openSuccessAlert(Msg.com[lang].succCreateData);
        this.upsertDialogModal.modalClose();
        this.getList();
      }, err=>{
        this.complete.add = true;
        this.commonModalService.openErrorAlert(Msg.err[lang].failCreateData);
        // this.commonModalService.openErrorAlert( 'Can\'t create new detail code. Please try again.');
      }, () => {
        this.complete.add = true;
      });
    });
  }
  update(event) {
    if(!this.complete.update) {
      return false;
    }
    this.complete.update = false;

    // this.cm.isDev() && console.log('update event =>',event);
    this.data.updated_dtm = this.dateService.getNowDate();
    this.data.updator_id = this.userId;
    this._setParams();
    this.validate(()=>{

      this.teamApi.updateAttributes(this.data[this.primaryKey], this.data).subscribe(
      res => {
        this.commonModalService.openSuccessAlert(Msg.com[lang].succUpdateData);
        this.upsertDialogModal.modalClose();
        this.getList();
      }, err => {
        this.complete.update = true;
        this.commonModalService.openErrorAlert(Msg.err[lang].failUpdateData);
        // this.commonModalService.openErrorAlert('upsert Error.');
      }, () => {
        this.complete.update = true;
      });

    });
  }
  private _setParams() {
    // this.data.workspaces = this.selectedWorkspaceList;
    this.data.workspaces = [this.selectedWorkspace];
    this.data.team_name = this.upsertDialogName.nativeElement.value;
    this.data.descript = this.upsertDialogDescription.nativeElement.value;
  }
  validate(callback: any = ()=>{}) {

    const _validate=(data)=>{
      const Validator = {
        team_name: {
          required: true,
          maxLength: 128
        },
        descript: {
          maxLength: 255
        }
      };

      for(const id in Validator){
        const item = Validator[id];
        if (this.cm.isEmpty(data[id]) && item.required) {
          this.commonModalService.openErrorAlert(`${id}를 입력해주십시오.`);
          return false;
        }
        if (
             this.cm.isNotEmpty(item.minLength)
          && this.cm.isNotEmpty(item.maxLength)
          && item.minLength == item.maxLength
          && data[id].length != item.minLength
        ) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.minLength} 자리로 정확하게 입력해주십시오.`);
          return false;
        }
        if (this.cm.isNotEmpty(item.minLength) && data[id].length < item.minLength) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.minLength} 자리 이상 입력해주십시오.`);
          return false;
        }
        if (this.cm.isNotEmpty(item.maxLength) && data[id].length > item.maxLength) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.maxLength} 자리 이하 입력해주십시오.`);
          return false;
        }
      }

      // if(this.cm.isEmpty(this.selectedWorkspaceList)){
      // }
      if(this.cm.isEmpty(this.selectedWorkspace)){
        this.commonModalService.openErrorAlert(`workspace를 선택해주십시오.`);
        return false;
      }

      return true;
    };

    if (_validate(this.data)) {
    } else {
      return false;
    }
    callback();
  }

  remove(event) {
    if(this.selectedItem){
      this.removeItem();
    } else {
      this.removesItem();
    }
  }

  private removeItem() {
    this.teamApi.deleteById(this.selectedItem[this.primaryKey]).subscribe(
      res => {
        if (res['count'] > 0) {
          this.commonModalService.openSuccessAlert(Msg.com[lang].succDeleteData);
          this.selectedItem = null;
          this.getCount();
          this.removeDialogModal.modalClose();
        }
      }, err=>{
        this.commonModalService.openErrorAlert(err);
        // this.commonModalService.openErrorAlert(Msg.err[lang].failDeleteData);
    });
  }
  private removesItem() {
    const ids = this.selectedItems.reduce((res,ele)=>{
      res.push(ele[this.primaryKey]);
      return res;
    },[]);
    if (this.cm.isEmpty(ids)){
      this.commonModalService.openErrorAlert('삭제할 항목을 선택해주십시오.');
      return false;
    }
    this.teamApi.deleteByIds(ids).subscribe(
      res => {
        this.commonModalService.openSuccessAlert(Msg.com[lang].succDeleteData);
        this.selectedItems = [];
        this.getCount();
        this.removeDialogModal.modalClose();
      }, err=>{
        this.commonModalService.openErrorAlert(err);
        // this.commonModalService.openErrorAlert(Msg.err[lang].failDeleteData);
    });
  }



  workspaceList = [];
  workspaceResponseList  = [];
  workspaceList2: Option[] = [];
  onWorkspaceSelect(event){
  }
  // workspaceSelect(v) {
  //   if (this.cm.isNotEmpty(this.workspaceList)) {
  //     this.workspaceList = this.workspaceList.filter(workspace=>(!this.cm.equalObj(workspace,v)));
  //     this.workspaceList.sort(this._sortItem);
  //   }
  //   this.selectedWorkspaceList.push(v);
  //   this.selectedWorkspaceList.sort(this._sortItem);
  // }
  // workspaceDeselect(v) {
  //   if (this.cm.isNotEmpty(this.selectedWorkspaceList)) {
  //     this.selectedWorkspaceList = this.selectedWorkspaceList.filter(selectedGroup=>(!this.cm.equalObj(selectedGroup,v)));
  //     this.selectedWorkspaceList.sort(this._sortItem);
  //   }
  //   this.workspaceList.push(v);
  //   this.workspaceList.sort(this._sortItem);
  // }
  // _sortItem(a: Cm_workspace, b: Cm_workspace){
  //   return a.workspace_id - b.workspace_id;
  // }
}
