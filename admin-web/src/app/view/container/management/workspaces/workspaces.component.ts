import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { LoopBackFilter, Cm_workspaceApi, Cm_teamApi, Cm_workspace, Cm_team, Cm_workspace_teamApi, Cm_workspace_team } from '../../../../adminsdk';
import { CommonUtilService, CommonModalService } from '../../../../service';
import { Paginator } from '../../../../common/paginator/paginator.component';
import { CommonModalComponent } from '../../../../common';
import { Option } from '../../../../model/dom-models';
import { UserService } from '../../../../service/user.service';
import { Msg, lang } from '../../../../model/message';
import { DateService } from '../../../../service/date.service';
import { DataService } from '../../../../service/data.service';

@Component({
  selector: 'app-workspaces',
  templateUrl: './workspaces.component.html',
  styleUrls: ['./workspaces.component.scss']
})
export class WorkspacesComponent implements OnInit, OnDestroy {

  constructor(
    private commonModalService: CommonModalService,
    private cm: CommonUtilService,
    private ds: DataService,
    private dateService: DateService,
    private userService: UserService,
    private teamApi: Cm_teamApi,
    private workspaceApi: Cm_workspaceApi,
    private workspaceTeamApi: Cm_workspace_teamApi
  ) {
  }

  ngOnInit() {
    this.init();
  }
  ngOnDestroy() {
    this.ds.clearData();
  }
  paginator = new Paginator();
  selectPageSize: string = this.paginator.DEFAULT_PAGE_SIZE.toString();
  searchStr = '';
  searchOption: LoopBackFilter = {};
  
  items: Cm_workspace[];
  selectedItems: Cm_workspace[] = [];
  selectedItem: Cm_workspace;


  private _isWorking: boolean = false;
  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if(this._isWorking == v) {
      return;
    }
    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }




  init(){
    this.initList();
    this.prepareDialogData();
  }
  initList(){
    if(this.paginator.pageSize != this.getCurrPageSize()) {
      this.setPageSize(this.getCurrPageSize());
    }
    this.isWorking = true;
    this.getCount(()=>{
      this.isWorking = false;
    });
  }

  alreadyInvolvedTeams: number[] = [];
  exclude = this.userService.exclude;
  prepareDialogData() {
    const filter: LoopBackFilter = {
      where: {
        and: [
          {
            team_id: {
              neq: this.exclude.teamId
            }
          }, {
            workspace_id: {
              neq: this.exclude.workspaceId
            }
          }
        ]
      },
      order : 'team_id'
    };
    this.workspaceTeamApi.find(filter).subscribe((list: Cm_workspace_team[]) => {
      this.alreadyInvolvedTeams = this.cm.getKeys(list, 'team_id');
    },error=>{
      this.commonModalService.openErrorAlert(error); 
    });
  }



  // action ====================================================

  onSearch() {
    this.isWorking = true;
    this.getList();
  }
  onSearchKey(event) {
    if(this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }

    // this.cm.isDev() && console.log('debug search key event',event);
    // this.cm.isDev() && console.log('debug search key event keyCode',event.keyCode);
    if (event.keyCode === 13) {
      this.isWorking = true;
      this.getList();
    } else {
      if(this.cm.isEmpty(this.searchStr) || this.cm.isEmpty(this.items)){
        // this.cm.isDev() && console.log('debug empty.getList');
        this.getList();
      } else {
        // this.cm.isDev() && console.log('debug no empty set totcnt');
        this.setTotalCount(this.items.length);
      }
    }
  }

  onFilterYn = ($event) => {
    if($event.checked){
      // ??
    }
    this.getList();
  };

  onPaginator(event) {
    const tempP = JSON.stringify(this.paginator);
    this.cm.prop(this.paginator, event.paginator);
    if(tempP == JSON.stringify(this.paginator)
      && this.paginator.totalPages != 1
    ){
      //갱신된 값이 없으면 다시 그리지 않음.
      return true;
    }
    if(this.paginator.totalCount <= 0) {
      return true;
    }
    this.isWorking = true;
    this._getList();
  }
  onChangePageSize(data) {
    this.selectPageSize = data.selectOption;
    this.initList();
  }




  // method ====================================================

  getCurrPageSize() {
    return Number(this.selectPageSize);
  }
  getFilter() {
    const filter: LoopBackFilter = {
      where: {
        and: [
          {
            team_id: {
              neq: this.exclude.teamId
            }
          }, {
            workspace_id: {
              neq: this.exclude.workspaceId
            }
          }
        ]
      },
      skip: this.searchOption.skip,
      limit: this.searchOption.limit,
      order: 'workspace_id'
    };

    
    if(this.cm.isNotEmpty(this.searchStr)){
      filter.where.workspace_id = {like: '%' + this.searchStr + '%'};
    }
    return filter;
  }
  getCount(callback?) {
    const filter = this.getFilter();

    this.workspaceApi.count(filter.where).subscribe(res=>{
      //filter
      if(this.cm.isEmpty(res)) {
        this.isWorking = false;
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        return false;
      }
      if(res.count <= 0) {
        console.log('no data');
        this._setItems([]);
        this.isWorking = false;
        this.setTotalCount(this.items.length);
        return false;
      }

      //logic
      const count = res.count;
      this.setTotalCount(count);


      callback && callback();


    }, err=>{
      this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  }

  getList() {
    this.isWorking = true;
    this.getCount(()=>{
      if(this.paginator.totalCount <= 0) {
        this._setItems([]);
        this.isWorking = false;
        // return false;
      }
      this._getList();
    });
  }
  private _setItems = (result:Cm_workspace[])=>{
    this.items = result;
  };
  private _getList = () => {
    this.setSearchOption();
    const filter = this.getFilter();
    this.workspaceApi.find(filter).subscribe((res:Cm_workspace[])=>{
      if(res){
        this.isWorking = false;
        this._setItems(res);
      }
      this.isWorking = false;
    }, err=>{
      this.isWorking = false;
      this.commonModalService.openErrorAlert(err);
    });
  };
  setSearchOption(){
    this.searchOption.skip = this.paginator.pageIndex * this.paginator.pageSize;
    this.searchOption.limit = this.paginator.pageSize;
  }
  modelSettingOpen(model: Cm_workspace) {
    console.log('debug update',model);
  }



  setTotalCount(count: number){
    this.paginator.totalCount = count;
  }
  setPageSize(pageSize: number){
    this.paginator.pageSize = pageSize;
  }








  // view item upsert ====================================================

  @ViewChild('upsertDialogModal')
  upsertDialogModal: CommonModalComponent;

  @ViewChild('removeDialogModal')
  removeDialogModal: CommonModalComponent;
  
  @ViewChild('upsertDialogId')
  upsertDialogId: ElementRef;
  @ViewChild('upsertDialogName')
  upsertDialogName: ElementRef;
  

  userId = this.userService.getCurrentUserId();

  selectedLangCd = 'LC0001';
  langCdList: Option[] = [];
  selectedTeamList: Cm_team[] = [];
  selectedTeamIds: number[] = [];
  data: Cm_workspace; //code list
  initUpsertDialog(callback = ():void=>{}){
    this.upsertDialogModal.setModalStyle(795);
    // this.upsertDialogModal.setModalStyle(null,null,{background:'red'});

    // TODO getLangCd;

    const filter: LoopBackFilter = {
      where: {
        team_id: {
          nin: this.alreadyInvolvedTeams
        },
        and: [
          {
            team_id: {
              neq: this.exclude.teamId
            }
          }, {
            workspace_id: {
              neq: this.exclude.workspaceId
            }
          }
        ]
      },
      order : 'team_id'
    };
    this.teamApi.find(filter).subscribe((list: Cm_team[]) => {
      this.teamList = this.teamResponseList = list;
      
      callback();
    },error=>{
      this.commonModalService.openErrorAlert(error); 
    });
  }
  onGroupListSelect(event){
    console.log(event);

  }

  /** action callbacks **/
  isUpdate: boolean = false;
  addOpen() {
    this.isUpdate = false;
    this.data = new Cm_workspace();
    this.initUpsertDialog(()=>{
      this.selectedTeamIds = [];
      this.selectedTeamList = [];

      this.selectedLangCd = 'LC0001'; //FIXME
      _open();
    });
    const _open = () => {
      this.upsertDialogId.nativeElement.value = '';
      this.upsertDialogName.nativeElement.value = '';
      this.upsertDialogModal.modalOpen();
    };
  }
  updateOpen(item: Cm_workspace) {
    this.isUpdate = true;
    this.data = item;
    this.initUpsertDialog(()=>{
      this.selectedTeamIds = this.cm.getKeys(this.data.teams,'team_id');
      this.selectedTeamList = this.data.teams;
      this.teamList = this.teamResponseList.filter((ele:Cm_team)=>(!this.selectedTeamIds.includes(ele.team_id)));
      
      this.selectedLangCd = this.data.lang_code;
      _open();
    });
   
    const _open = () => {
      this.upsertDialogId.nativeElement.value = item.workspace_id;
      this.upsertDialogName.nativeElement.value = item.workspace_name;
      this.upsertDialogModal.modalOpen();
    };
  }
  removeOpen(item: Cm_workspace) {
    if (this.cm.isEmpty(item)) {
      return;
    }
    this.selectedItem = item;
    this.removeDialogModal.modalOpen();
  }

  removesOpen(event) {
    const items = this.selectedItems;
    if (this.cm.isEmpty(items)) {
      console.log('debug remove. stop. no select items');
      return;
    }
    this.removeDialogModal.modalOpen();
  }



  complete = {
    add: true,
    update: true
  };
  add(event) {
    if(!this.complete.add) {
      return false;
    }
    this.complete.add = false;

    // this.cm.isDev() && console.log('add event =>',event);
    this.data.created_dtm = this.dateService.getNowDate();
    this.data.creator_id = this.userId;
    this._setParams();
    this.validate(()=>{

      this.workspaceApi.create(this.data).subscribe(
      res => {
        alert( 'Created.');
        this.upsertModalClose();
        this.prepareDialogData();
      }, err => {
        this.commonModalService.openErrorAlert( 'Can\'t create new detail code. Please try again.');
      }, () => {
        this.complete.add = true;
      });

    });
    this.complete.add = true;
  }
  upsertModalClose() {
    this.upsertDialogModal.modalClose();
    this.getList();
  }
  update(event) {
    if(!this.complete.update) {
      return false;
    }
    this.complete.update = false;

    // this.cm.isDev() && console.log('update event =>',event);
    this.data.updated_dtm = this.dateService.getNowDate();
    this.data.updator_id = this.userId;
    this._setParams();
    this.validate(()=>{

      this.workspaceApi.updateAttributes(this.data.workspace_id, this.data).subscribe(
      res => {
        alert( 'Updated.');
        this.upsertModalClose();
        this.prepareDialogData();
      }, err => {
        this.commonModalService.openErrorAlert('upsert Error.');
      }, () => {
        this.complete.update = true;
      });

    });
    this.complete.update = true;
  }
  private _setParams() {
    this.data.teams = this.selectedTeamList;
    this.data.workspace_name = this.upsertDialogName.nativeElement.value;
    this.data.lang_code = this.selectedLangCd;
  }
  validate(callback: any = ()=>{}) {

    const _validate=(data)=>{
      const Validator = {
        workspace_name: {
          required: true,
          maxLength: 128
        },
        lang_code: {
          required: true
        }
      };

      for(const id in Validator){
        const item = Validator[id];
        if (this.cm.isEmpty(data[id]) && item.required) {
          this.commonModalService.openErrorAlert(`${id}를 입력해주십시오.`);
          return false;
        }
        if (
             this.cm.isNotEmpty(item.minLength)
          && this.cm.isNotEmpty(item.maxLength)
          && item.minLength == item.maxLength
          && data[id].length != item.minLength
        ) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.minLength} 자리로 정확하게 입력해주십시오.`);
          return false;
        }
        if (this.cm.isNotEmpty(item.minLength) && data[id].length < item.minLength) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.minLength} 자리 이상 입력해주십시오.`);
          return false;
        }
        if (this.cm.isNotEmpty(item.maxLength) && data[id].length > item.maxLength) {
          this.commonModalService.openErrorAlert(`${id}를 ${item.maxLength} 자리 이하 입력해주십시오.`);
          return false;
        }
      }

      // if(this.cm.isEmpty(this.selectedTeamList)){
      //   this.commonModalService.openErrorAlert(`team을 선택해주십시오.`);
      //   return false;
      // }

      return true;
    };

    if (_validate(this.data)) {
    } else {
      return false;
    }
    callback();
  }

  remove(event) {
    if(this.selectedItem){
      this.removeItem();
    } else {
      this.removesItem();
    }
  }

  removeModalClose(){
    this.removeDialogModal.modalClose();
    this.getList();
  }
  private removeItem() {
    this.workspaceApi.deleteById(this.selectedItem.workspace_id).subscribe(
      res => {
        if (res['count'] > 0) {
          alert('삭제되었습니다.');
          this.selectedItem = null;
          this.removeModalClose();
          this.prepareDialogData();
        }
      }, error => {
        console.log('delete Error');
    });
  }
  private removesItem() {
    const ids = this.selectedItems.reduce((res,ele)=>{
      res.push(ele.workspace_id);
      return res;
    },[]);
    const filter: LoopBackFilter = {
      where: {workspace_id: {inq:ids}}
    };
    this.workspaceApi.deleteByIds(ids).subscribe(
      res => {
        alert('삭제되었습니다.');
        this.selectedItems = [];
        this.removeModalClose();
        this.prepareDialogData();
      }, error => {
        console.log('delete Error');
    });
  }



  teamList: Cm_team[] = []; 
  teamResponseList: Cm_team[]  = [];
  teamSelect(v) {
    if (this.cm.isNotEmpty(this.teamList)) {
      this.teamList = this.teamList.filter(team=>(!this.cm.equalObj(team,v)));
      this.teamList.sort(this._sortItem);
    }
    this.selectedTeamList.push(v);
    this.selectedTeamList.sort(this._sortItem);
  }
  teamDeselect(v) {
    if (this.cm.isNotEmpty(this.selectedTeamList)) {
      this.selectedTeamList = this.selectedTeamList.filter(item=>(this.cm.notEqualObj(item,v)));
      this.selectedTeamList.sort(this._sortItem);
    }
    this.teamList.push(v);
    this.teamList.sort(this._sortItem);
  }
  _sortItem(a: Cm_team, b: Cm_team){
    return a.team_id - b.team_id;
  }
}
