import {AfterViewChecked, AfterViewInit, Component, EventEmitter, ViewChild, OnDestroy, Input, HostListener} from '@angular/core';
import {CommonModalEvent, CommonModalService, MenuService, ModalType, CommonUtilService } from '../../service';
import {NavigationEnd, Router, GuardsCheckStart, NavigationStart} from '@angular/router';
import {CommonModalComponent} from '../../common';
import {AlertModalEvent} from '../../service/common-modal.service';
import { MenuItem } from 'primeng/primeng';
import { DataService, DataInterface } from '../../service/data.service';
import { Subscription } from 'rxjs';
import { CloseComponent } from './shared/close.component';
import { Msg, lang } from '../../model/message';
import {UserService} from '../../service/user.service';
import {CookieService} from 'ng2-cookies';
declare const $: any;


@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styles: [
    `@keyframes dim {
      0% {
        background: rgba(255, 255, 255, 0)
      }
      50% {
        background: rgba(255, 255, 255, 0)
      }
      100% {
        background: rgba(255, 255, 255, 0.30)
      }
    }`
  ]
})
export class ContainerComponent implements AfterViewInit, AfterViewChecked, OnDestroy {
  constructor(
      private router: Router,
      private ds: DataService,
      private commonModalService: CommonModalService,
      private cm: CommonUtilService,
      private cookie: CookieService,
      private menuService: MenuService,
      private userService: UserService) {
    this.init();
  }
  
  onActivate($event) {
    // 로그인 완료 후 컨테이너 활성화 시..
    this.cm.isDev() && console.log('onActivate :: ', $event.constructor.name);
    this.isSave = true;
  }
  ngOnDestroy(){
    this.ds.clearData();
  }




  ngAfterViewInit() {
    this.commonModalService.modalHandler
      .subscribe((event: CommonModalEvent) => {
        this.initCallbacks();
        switch (event.modalType) {
          case ModalType.ALERT_MESSAGE_MODAL: {
            this.alertModalTitle = event.modalTitle;
            this.contentsIsHtml = event.contentsIsHtml;
            this.alertModalText = event.modalTextMessage;
            this.alertModalHasCancel = event.modalHasCancelButton;
            this.alertModalHasUserAction = event.modalHasUserAction;
            this.alertModalSectionStyle = event.modalSectionStyle;
            this.alertModalEventEmitter = event.eventEmitter;
            if (event.isOpen) {
              this.alertModal.modalOpen();
            } else {
              this.alertModal.modalClose();
            }
            break;
          }
          // case ModalType.DIALOG_MODEL_TRAINING_MODAL: {
          //   this.dialogModelTrainingModelId = event.modalExtraData['modelId'];
          //   this.dialogModelName = event.modalExtraData['modelName'];
          //   if (event.isOpen) {
          //     this.dialogModelTrainingModal.modalOpen();
          //   } else {
          //     this.dialogModelTrainingModal.modalClose();
          //   }
          //   break;
          // }
        }
      });
  }

  ngAfterViewChecked() {
    // $('.train-text i').mouseenter(function () {
    //   $(event.currentTarget).next('div').css('display', 'block');
    // }).mouseleave(function() {
    //   $(event.currentTarget).next('div').css('display', 'none');
    // });
  }


  // @ViewChild('dialogModelTrainingModal') dialogModelTrainingModal: CommonModalComponent;
  // dialogModelName: string;
  // dialogModelTrainingModelId: number;


  @ViewChild('alertModal') alertModal: CommonModalComponent;
  alertModalEventEmitter: EventEmitter<AlertModalEvent>;
  alertModalTitle: string;
  alertModalText: string = '';
  contentsIsHtml: boolean = false;
  alertModalHasCancel: boolean;
  alertModalHasUserAction: boolean = true;
  alertModalSectionStyle: any = {};
  
  headerBreadcrumb: MenuItem[];

  currentMenuIds: Array<Number> = [];
  currentMenuId: Number = -1;

  resetRouterEventsSubscribe (): boolean {
    // this.cm.isDev() && console.log('container router events', '\n========\n', this.router.events, '\nrotuer events observers length:', this.router.events['observers'].length, '\n========\n');
    const observers = this.router.events['observers'];
    const defaultRouterEventsCnt = 1;
    if(observers.length > defaultRouterEventsCnt){
      const _isChild=(parent)=>{return parent==null};
      this.router.events['observers'] = observers.filter(subscriber => {
        return _isChild(subscriber._parent) ? false : true;
      });
      // this.cm.isDev() && console.log('container router events reset!!!');
      return true;
    }
    return false;
  }


  init(){
    this.setRouterEvents();
    this.setSubscription();
  }
  subscription: Subscription;
  setSubscription(){
    this.subscription = this.ds.getData().subscribe(res=>{
      if(!res){
        return false;
      }
      const _resolveIsSave = (data)=>{
        this.isSave = data.isSave;
      };
      const _resolveNotiExit = (data)=>{
        this.close.url = data.url;
        this.close.modal.modalOpen();
      };
      const _resolveIsWorking = (data)=>{
        this.isWorking = data.isWorking;
      };
      
      // this.cm.isDev() && console.log('container :: ds getData', res);
      const {cmd, data} = res;
      switch(cmd){
        case this.ds.CMD.IS_SAVE:
          _resolveIsSave(data);
          break;
        case this.ds.CMD.NOTI_EXIT:
          _resolveNotiExit(data);
          break;
        case this.ds.CMD.EXIT:
          this._resolveExit(data);
          break;
        case this.ds.CMD.IS_WORKING:
          _resolveIsWorking(data);
          break;
        default:
          // console.log('미처리 cmd');
          break;
      }
    }, err=>{
      console.log('err',err);
    });
  }
  @ViewChild('close')
  close: CloseComponent;
  @ViewChild('progressSpinner')
  progressSpinner: CloseComponent;

  pauseUrl = '';
  previousUrl = '';
  _setCurrMenuInfo = (navInfo, menuItems) => {
    menuItems =this.menuService.getCurrentMenu(navInfo.url);
    this.currentMenuIds = this.menuService.getMenuIds(menuItems);
    this.currentMenuId = this.menuService.getCurrentMenuId();
    return menuItems;
  };
  setRouterEvents(){
    this.resetRouterEventsSubscribe();
    const _resolveGuardsCheckStart = (navInfo) => {
      // this.cm.isDev() && console.log('debug GuardsCheckStart navInfo',navInfo);
    };


    let menuItems = [];
    const _resolveNavigationStart = (navInfo: NavigationStart) => {
      // this.cm.isDev() && 
      // console.log('resolve navigationStart... :: navInfo ==>',navInfo);
      this.pauseUrl = navInfo.url;
    };
    const _resolveNavigationEnd = (navInfo) => {
      // this.cm.isDev() && 
      // console.log('resolve navigationEnd... :: navInfo ==>',navInfo.url);
      menuItems = this._setCurrMenuInfo(navInfo, menuItems);

      this.cm.setPreviousUrl(this.previousUrl);
      this.previousUrl = navInfo.url;

      // set title
      this.menuService.setTitle(menuItems);

      // set menu when input path on browser
      this.headerBreadcrumb =this.menuService.getBreadcrumbItem(menuItems);
    };
    this.router.events.subscribe(navInfo => {

      // this.cm.isDev() && 
      // console.log('navInfo',navInfo.constructor.name);
      if (navInfo instanceof NavigationStart) {
        _resolveNavigationStart(navInfo);
      }
      if (navInfo instanceof GuardsCheckStart) {
        _resolveGuardsCheckStart(navInfo);
      }
      if (navInfo instanceof NavigationEnd) {
        _resolveNavigationEnd(navInfo);
      }
    });
  }

  private initCallbacks() {
    this.alertModal.closeAllModals();
    this.alertModalEventEmitter = null;
    this.alertModalHasCancel = false;
    this.alertModalHasUserAction = true;
    this.alertModalSectionStyle = {};
    this.alertModalTitle = Msg.com[lang].titleAlert;
    this.contentsIsHtml = false;
    this.alertModalText = '';
  }

  onCloseAlertModal(isOk: boolean) {
    if (this.alertModalEventEmitter !== null) {
      const evt = new AlertModalEvent();
      evt.isOk = isOk;
      this.alertModalEventEmitter.emit(evt);
    }
  }


  private _isSave: boolean = true;
  get isSave() {
    return this._isSave;
  }
  @Input() set isSave(v: boolean) {
    if(this._isSave == v) {
      return;
    }
    this._isSave = v;
    this.ds.sendData(this.ds.CMD.IS_SAVE, {isSave: v});
  }

  @ViewChild('progressSpinnerModal') progressSpinnerModal: CommonModalComponent;
  private _defaultProgressSpinnerSectionStyle: any = {background: 'none', animation: 'dim 1.5s forwards'};
  progressSpinnerSectionStyle = this._defaultProgressSpinnerSectionStyle;
  private progressWorking: boolean = false;
  private _isWorking: boolean = false;
  get isWorking() {
    return this._isWorking;
  }
  @Input() set isWorking(v: boolean) {
    if(this._isWorking == v) {
      return;
    }
    this._isWorking = v;
    if(v) {
      if(this.progressWorking) {
        return;
      }
      this.progressWorking = true;
      // this.progressSpinnerModal.resetSectionStyle();
      // this.progressSpinnerSectionStyle = this._defaultProgressSpinnerSectionStyle;
      this.progressSpinnerModal.modalOpen();
    } else {
      this.progressSpinnerModal.modalClose();
      this.progressWorking = false;
    }
  }


  _resolveSave = (data)=>{
    this.ds.sendData(this.ds.CMD.SAVE,data);
  };
  _resolveExit = (data)=>{
    this.ds.sendData(this.ds.CMD.IS_SAVE,{isSave: true});
    this.lazyNavigate(data);
  };
  onCloser(emitData: DataInterface) {
    // console.log('container on closer',emitData);
    const {cmd, data} = emitData;
    switch (cmd) {
      case this.ds.CMD.SAVE:
        this._resolveSave(data);
        break;
      case this.ds.CMD.EXIT:
        this._resolveExit(data);
        break;
      default:
        throw "invalid cmd";
    }
  }
  lazyNavigate(data){
    let i = 0;
    const t = setInterval((data)=>{
      if(this.isSave) {
        clearInterval(t);
        this.router.navigateByUrl(data.url);
      }
      if(i++ > 500) {
        clearInterval(t);
        this.commonModalService.openErrorAlert('페이지 이동에 실패했습니다.');
      }
    }, 10, data);
  }

}
