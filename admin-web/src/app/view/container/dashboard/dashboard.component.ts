import {Component, OnDestroy, OnInit} from '@angular/core';
import {CommonModalService, CommonUtilService} from '../../../service';
import {lang, Msg} from '../../../model/message';
import {ActivatedRoute, Router} from '@angular/router';
import {DateService} from '../../../service/date.service';
import {Chatbot_info, Chatbot_infoApi, Cm_user, LoopBackFilter, Monitor_serverApi, Stats_dlg_historyApi} from '../../../adminsdk';
import {Option, OptionEx} from '../../../model/dom-models';
import {UserService} from '../../../service/user.service';
import {chatbotStepBqa, chatbotStepSds} from '../../../model/chatbot-step';
import {DashboardService} from '../../../service/dashboard.service';
import {DataService} from '../../../service/data.service';
import {Subscription} from 'rxjs';

export interface Rank {
  no?: number;
  name: string;
  count: number;
}

export interface ResponseTps {
  period: string;
  tps: number;
}
export interface ResponseDialogResult {
  period: string;
  success: number;
  // fail: number;
  unknown: number;
}

/*export interface ResponseCpuUsedResult {
    period: string;
    serverName: string;
    cpuUsed: number;
}

export interface ResponseMemUsedResult {
    period: string;
    serverName: string;
    memTotal: number;
    memUsage: number;
    memUsed: number;
}



export interface ResponseDiskUsedResult {
    period: string;
    serverName: string;
    diskTotal: number;
    diskUsage: number;
    diskUsed: number;
}*/


@Component({
  selector: 'app-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit, OnDestroy {
  constructor(
    private router: Router
    , private activatedRoute: ActivatedRoute
    , private commonModalService: CommonModalService
    , private cm: CommonUtilService
    , private ds: DataService
    , private dateService: DateService
    , private userService: UserService
    , private api: Stats_dlg_historyApi
    , private monitorApi: Monitor_serverApi
    // , private api: Dlg_historyApi
    , private chatbotInfoApi: Chatbot_infoApi
    , private dashboardService: DashboardService
  ) {}


  private eventSubscription: Subscription;

  teamId: number;
  monitorServer: string[] = [];
  qnaRanks: Rank[] = [];
  scenarioRanks: Rank[] = [];

  userId = this.userService.getCurrentUserId();
  currUserInfo: Cm_user = this.userService.getCachedUserInfo();

  rangeOption = this.dashboardService.rangeOption;

  dd = this.dashboardService.default;
  set = this.dashboardService.SET;
  dds = this.dd.statistics;
  sc = this.set.CHART;

  private _isWorking = false;

  isWorkingStatDialogResult = false;
  isWorkingStatTps = false;
  isWorkingRank = false;
  isWorkingCpuUsed = false;
  isWorkingMemUsed = false;
  isWorkingDiskUsed = false;

  public serverList: Option[];

  ngOnInit() {
     /* this.eventSubscription = this.userService.isLoadHeaderEvent.subscribe((res) => {
          //localStorage에 저장된 teamId 가 넘어옴..
          this.teamId = res;
          this.init();
      })*/

      this.teamId = parseInt(this.userService.initTeamId);
      this.init();

  }


  ngOnDestroy() {
    this.ds.clearData();
   // this.eventSubscription.unsubscribe();
  }


  private init() {
    this.optionList = [
      {label: '챗봇 전체', value: ''}
    ];

    this.getServerNames();

    this.isWorking = true;
    this.initChatbotList(():void => {
      this._search();
    });
  }


  rangeByTime = () => this.dashboardService.rangeByTime(this.selectedRange);
  rangeByDay = () => this.dashboardService.rangeByDay(this.selectedRange);




  get isWorking() {
    return this._isWorking;
  }
  set isWorking(v: boolean) {
    if(this._isWorking === v) {
      return;
    }

    this._isWorking = v;
    this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
  }


  private initChatbotList(callback = ():void=>{ this.cm.override(); }){
    const filter: LoopBackFilter = {
       where: { and: [{ team_id: this.teamId }, { use_yn: 'Y' }] }
    };


    this.chatbotInfoApi.find(filter).subscribe((list: Chatbot_info[]) => {
      const temp = list.map((el: Chatbot_info) => {
        const item: Option = {
          label: el.bot_name,
          value: el.bot_id
        };
        return item;
      });
      this.optionList.push(...temp);

      callback();
    },error => {
      this.isWorking = false;
      this.commonModalService.openErrorAlert(error);
    });
  }

  now: Date = this.dateService.getNowDate();
  private _startDate: Date = this.dateService.getDefaultStartDate(this.now);

  get startDate(): Date {
    return this._startDate;
  }

  set startDate(startDate) {
    startDate = this.dateService.setDefaultStartTime(startDate);
    this._startDate = startDate;
  }

  private _endDate: Date = this.dateService.getDefaultEndDate(this.now);
  get endDate(): Date {
    return this._endDate;
  }
  set endDate(endDate) {
    // endDate.setDate(endDate.getDate() + 1);
    endDate = this.dateService.setDefaultEndTime(endDate);
    this._endDate = endDate;

    if(this.dashboardService.rangeByTime(this.selectedRange)) {
      this.startDate = this.dateService.setDefaultStartTime(this.endDate);
    } else {
      this._minDate = this.dateService.getDefaultStartDateBeforeOneMonth(this.endDate, this.set.INTERVAL);
    }
  }
  private _minDate = this.dateService.getDefaultStartDateBeforeOneMonth(this.endDate, this.set.INTERVAL);
  get minDate() {
    return this._minDate;
  }

  public optionList: Option[];
  _selectOption: string = '';
  _selectServerOption: string = '';
  _selectServerOption2: string = '';
  _selectServerOption3: string = '';

  get selectOption(): string{
    return this._selectOption;
  }
  set selectOption(v: string) {
    this._selectOption = v;
  }


  get selectServerOption(): string{
        return this._selectServerOption;
  }
  set selectServerOption(v: string) {
        this._selectServerOption = v;
  }


  get selectServerOption2(): string{
        return this._selectServerOption2;
  }
  set selectServerOption2(v: string) {
      this._selectServerOption2 = v;
  }

  get selectServerOption3(): string{
      return this._selectServerOption3;
  }
  set selectServerOption3(v: string) {
      this._selectServerOption3 = v;
  }



  private _range = this.rangeOption.time;
  get selectedRange() {
    return this._range;
  }
  set selectedRange(v) {
    this._range = v;

    if(this.dashboardService.rangeByTime(this.selectedRange)){
      this.startDate = this.dateService.getDefaultStartDate(this.endDate);
    } else {
      this.startDate = this.dateService.getDefaultStartDateBeforeOneMonth(this.endDate);
    }
  }

  get title () {
    return Msg.com[lang].titleDashboard;
  }

  search(event: any = null) {
    if(this.isWorking) {
      this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
      return;
    }
    this.isWorking = true;
    this._search();
  }

  _search() {
    this.renderStats();
  }

  renderStats() {


    this.getSessionData((res) => {
      this.renderStatTotal(res);
      this.isWorking = false;
    });

   this.getTpsData((res) => {
      this.renderStatTps(res);
    });

    this.getDlgResData((res) => {
      this.renderStatDialogResult(res);
    });

     this.getQnaTopNData((res) => {
      this.renderStatQnaRank(res);
      this.getScenarioTop();
      this.isWorkingRank = false;
    });


     // serverStatus.comp 로 이관..우선 흔적은 남겼음.
/*
     if (typeof this.monitorServer !== 'undefined' && this.monitorServer.length > 0) {
         const memSverver = this.selectServerOption2 || this.monitorServer[0];
         this.getMemUsed(memSverver, (res) => {
             this.renderMemUsed(res);
         });

         const cpuServer = this.selectServerOption || this.monitorServer[0];
         this.getCpuUsed(cpuServer, (res) => {
             this.renderCpuUsed(res);
             this.isWorkingCpuUsed = false;
         });

         const diskServer = this.selectServerOption3 || this.monitorServer[0];
         this.getDisksed(diskServer, (res) => {
             this.renderDiskUsed(res);
             this.isWorkingDiskUsed = false;
         });
     }*/
  }


  statistics: OptionEx[] = this.dashboardService.statistics;
  renderStatTotalDefault() {
    this.renderStatTotal([this.dds.sessionCount, this.dds.dialogCount, this.dds.tpsAverage, this.dds.reliabilityAverage]);
  }
  renderStatTotal(baseData) {
    const parseData = this.dashboardService.parseStatTotal(baseData);
    this.statistics = parseData;
  }
  renderStatTps(baseData) {
    if(this.cm.isEmpty(baseData)) {
      this.cm.isDev() && console.log('no data','tps');
      this.tpsChartData = [];
      this.tpsChartLabels = [];
    }


    const parseData = this.dashboardService.parseStatTps(baseData, this.startDate, this.endDate, this.selectedRange);
    // this.tpsChartLegend = false;
    // this.tpsChartType = 'line'

    const unit = this.dashboardService.getUnit(this.selectedRange);
    const max = this.dashboardService.getYAxesMaxTps(baseData);
    const stepSize = this.dashboardService.getStepSize(parseData.period, baseData);
    const format = this.dashboardService.getFormat(this.selectedRange);

    parseData.datas[0]['fill'] = false;
    parseData.datas[0]['borderWidth'] = 2;
    parseData.datas[0]['cubicInterpolationMode'] = 'monotone';
    // res.datas[0]['borderDash'] = [3,1];
    // res.datas[0]['borderColor'] = '#4688E8';
    this.tpsChartData = parseData.datas;
    requestAnimationFrame(()=>{
      this.tpsChartOptions = this.dashboardService.parseChartOptions(max, stepSize, unit, format);
      // this.cm.isDev() && console.log('tps labels', unit, max, stepSize, format , parseData);
      this.tpsChartLabels = parseData.period;
      this.isWorkingStatTps = false;
    });
  }
  renderStatDialogResult(res) {
    if(this.cm.isEmpty(res)) {
      this.cm.isDev() && console.log('no data','dlg res');
      this.dialogResultChartData = [];
      this.dialogResultChartLabels = [];
    }

    const parseData = this.dashboardService.parseStatDialogResult(res, this.startDate, this.endDate, this.selectedRange);
    // this.dialogResultChartLegend = false;
    // this.dialogResultChartType = 'line';


    const unit = this.dashboardService.getUnit(this.selectedRange);
    const max = this.dashboardService.getYAxesMaxDialogResult(res);
    const stepSize = this.dashboardService.getStepSize(parseData.period, res);
    const format = this.dashboardService.getFormat(this.selectedRange);
    this.dialogResultChartData = parseData.datas;
    requestAnimationFrame(()=>{
      this.dialogResultChartOptions = this.dashboardService.parseChartOptions(max, stepSize, unit, format);
      // this.cm.isDev() && console.log('dres labels', unit, max, stepSize, format , parseData);
      this.dialogResultChartLabels = parseData.period;
      this.isWorkingStatDialogResult = false;
    });
  }


  getSessionData(callback = (res): void => { this.cm.override(); }) {

    const boatIds: string[] = [];

    this.optionList.map((obj, i) => {
      if (i > 0) {
        boatIds.push(obj.value);
      }
    });


    if(typeof boatIds === 'undefined' || boatIds.length === 0 ){
      boatIds.push('-1');
    }


    const args =[
      this.dateService.getMinDateStr(this.startDate),
      this.dateService.getMinDateStrEx(this.endDate),
      boatIds.join(','),
      this.teamId
    ];
    if(this.cm.isNotEmpty(this.selectOption)){
      args.push(parseInt(this.selectOption));
    }


    // obs
    this.api.getStats(...args).subscribe(res => {
      // filter
      if(this.cm.isEmpty(res)) {
        this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        this.isWorking = false;
        return false;
      }
      if(res.count <= 0) {
        this.cm.isDev() && console.log('no session data');
        this.renderStatTotalDefault();
        this.isWorking = false;
        return false;
      }

      // console.log(res);

      callback && callback(res);

    }, err => {
      this.commonModalService.openErrorAlert(err);
      this.isWorking = false;
    });

  }

  getTpsData(callback = (res): void=>{ this.cm.override(); }){
    this.isWorkingStatTps = true;

     const boatIds: string[] = [];

    this.optionList.map((obj, i) =>{
      if (i > 0) {
        boatIds.push(obj.value);
      }
    });

    if (typeof boatIds === 'undefined' || boatIds.length === 0 ) {
      boatIds.push('-1');
    }

    const args = [
      this.dateService.getMinDateStr(this.startDate),
      this.dateService.getMinDateStrEx(this.endDate),
      boatIds.join(','),
      this.selectedRange,
      this.teamId
    ];
    if(this.cm.isNotEmpty(this.selectOption)){
      args.push(parseInt(this.selectOption));
    }
    this.api.getTpsChart(...args).subscribe(res => {
      // filter
      if (this.cm.isEmpty(res)) {
        // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        this.cm.isDev() && console.log(Msg.err[lang].failLoadData,'tps data');
        this.isWorking = false;
        this.isWorkingStatTps = false;
        callback([]);
        return false;
      }
      callback(res);

    }, err => {
      this.commonModalService.openErrorAlert(err);
      this.isWorking = false;
      this.isWorkingStatTps = false;
    });
  }

  getDlgResData(callback = (res): void=>{ this.cm.override(); }){
    this.isWorkingStatDialogResult = true;

    const boatIds: string[] = [];

    this.optionList.map((obj, i) => {
      if (i > 0) {
        boatIds.push(obj.value);
      }
    });

    if (typeof boatIds === 'undefined' || boatIds.length === 0 ){
      boatIds.push('-1');
    }

    const args = [
      this.dateService.getMinDateStr(this.startDate),
      this.dateService.getMinDateStrEx(this.endDate),
      boatIds.join(','),
      this.selectedRange,
      this.teamId
    ];
    if(this.cm.isNotEmpty(this.selectOption)) {
      args.push(parseInt(this.selectOption));
    }
    // obs
    this.api.getDialogChart(...args).subscribe(res => {
      // filter
      if (this.cm.isEmpty(res)) {
        // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'dlg res data');
        this.isWorking = false;
        this.isWorkingStatDialogResult = false;
        callback([]);
        return false;
      }

      callback(res);

    }, err => {
      this.commonModalService.openErrorAlert(err);
      this.isWorking = false;
      this.isWorkingStatDialogResult = false;
    });
  }




  getQnaTopNData(callback = (res): void => { this.cm.override(); }){
     this.isWorkingRank = true;

     const boatIds: string[] = [];

     this.optionList.map((obj, i) => {
      if (i > 0) {
        boatIds.push(obj.value);
      }
    });

    if(typeof boatIds === 'undefined' || boatIds.length === 0 ){
      boatIds.push('-1');
    }

    const args = [
        this.dateService.getMinDateStr(this.startDate),
        this.dateService.getMinDateStrEx(this.endDate),
        boatIds.join(','),
        this.selectedRange,
        chatbotStepBqa[1],
        this.teamId
    ];


    if (this.cm.isNotEmpty(this.selectOption)) {
      args.push(parseInt(this.selectOption));
    }
    // obs
    this.api.getTopN(...args).subscribe(res => {
      // filter
      if (this.cm.isEmpty(res)) {
        // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'dlg res data');
        this.isWorkingRank = false;
        callback([]);
        return false;
      }

      callback(res);

    }, err => {
      this.commonModalService.openErrorAlert(err);
      this.isWorkingRank = false;
    });
  }



  getScenarioTop() {
    this.getScenarioTopNData((res) => {
      this.renderStatScenarioRank(res);
    });
  }

   getScenarioTopNData(callback = (res): void => { this.cm.override(); }){
     this.isWorkingRank = true;

     const boatIds: string[] = [];

     this.optionList.map((obj, i) => {
      if (i > 0) {
        boatIds.push(obj.value);
      }
    });

    if (typeof boatIds === 'undefined' || boatIds.length === 0 ) {
      boatIds.push('-1');
    }

    const args = [
        this.dateService.getMinDateStr(this.startDate),
        this.dateService.getMinDateStrEx(this.endDate),
        boatIds.join(','),
        this.selectedRange,
        chatbotStepSds[2],
        this.teamId
    ];


    if (this.cm.isNotEmpty(this.selectOption)) {
      args.push(parseInt(this.selectOption));
    }
    // obs
    this.api.getTopN(...args).subscribe(res => {
      // filter
      if (this.cm.isEmpty(res)) {
        // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
        this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'dlg res data');
        this.isWorkingRank = false;
        callback([]);
        return false;
      }

      callback(res);

    }, err => {
      this.commonModalService.openErrorAlert(err);
      this.isWorkingRank = false;
    });
  }


  renderStatQnaRank(res) {
    this.qnaRanks = [];

    res.map( (v, i) => {
        this.qnaRanks.push({
            name: v.domain_name,
            count: v.cnt,
            no: (i + 1)
        });
    });
  }

  renderStatScenarioRank(res) {

    this.scenarioRanks = [];

     res.map( (v, i) => {
        this.scenarioRanks.push({
            name: v.domain_name,
            count: v.cnt,
            no: (i + 1)
        });
    });
  }


  // For Chart =================================================================


  // TPS 차트 [s] ======================================================
  ddc = this.dd.chart;
  tpsChartOptions:any = this.ddc.options;
  tpsChartLabels:string[] = [];
  tpsChartType:string = 'line';
  tpsChartLegend:boolean = true;

  tpsChartData:any[] = [];

  // events
  tpsChartClicked(e:any):void {
    this.cm.isDev() && console.log('tps chart click',e);
  }

  tpsChartHovered(e:any):void {
    this.cm.isDev() && console.log('tps chart hover',e);
  }
  // TPS 차트 [e] ======================================================


  // 대화 결과 차트 [s] ======================================================
  dialogResultChartOptions:any = this.ddc.options;
  dialogResultChartLabels:string[] = [];
  dialogResultChartType:string = 'bar';
  dialogResultChartLegend:boolean = true;

  dialogResultChartData:any[] = [];

  //CPU 사용률

  cpuResultChartOptions: any = this.ddc.options;
  cpuResultChartLabels: string[] = [];
  cpuResultChartData:any[] = [];
  cpuResultChartType:string = 'line';
  cpuResultChartLegend:boolean = true;
  cpuTotalMax: string;
  cpuTotalMin: string;
  cpuTotalAvg: string;


  //MEM 사용률

  memResultChartOptions: any = this.ddc.options;
  memResultChartLabels: string[] = [];
  memResultChartData:any[] = [];
  memResultChartType:string = 'line';
  memResultChartLegend:boolean = true;
  memTotal: string;
  memTotalUsed: string;
  memTotalUSageAvg: string;
  memTotalMax: string;
  memTotalMin: string;
  memTotalAvg: string;

  //DISK 사용률

  diskResultChartOptions: any = this.ddc.options;
  diskResultChartLabels: string[] = [];
  diskResultChartData:any[] = [];
  diskResultChartType:string = 'line';
  diskResultChartLegend:boolean = true;
  diskTotal: string;
  diskTotalUsed: string;
  diskTotalUSageAvg: string;
  diskTotalMax: string;
  diskTotalMin: string;
  diskTotalAvg: string;

  // events
  dialogResultChartClicked(e:any):void {
    this.cm.isDev() && console.log('dlgRes chart click',e);
  }

  dialogResultChartHovered(e:any):void {
    this.cm.isDev() && console.log('dlgRes chart hover',e);
  }

  // events
  cpuResultChartClicked(e: any): void {
    this.cm.isDev() && console.log('CpuUsed chart click', e);
  }

  cpuResultChartHovered(e: any): void {
    this.cm.isDev() && console.log('CpuUsed chart hover', e);
  }

  memResultChartClicked(e: any): void {
        this.cm.isDev() && console.log('MemUsed chart click', e);
  }

  memResultChartHovered(e: any): void {
        this.cm.isDev() && console.log('MemUsed chart hover', e);
  }


  diskResultChartClicked(e: any): void {
        this.cm.isDev() && console.log('diskUsed chart click', e);
  }

  diskResultChartHovered(e: any): void {
        this.cm.isDev() && console.log('diskUsed chart hover', e);
  }

  // 대화 결과 차트 [e] ======================================================


  getServerNames() {
      this.monitorApi.getServiceNames().subscribe(res => {
          if (this.cm.isEmpty(res)) {
              // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
              this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'serverName data');
              return false;
          }

          const tempList: Option[] = [];
          this.monitorServer = res;

          res.map((el: string) => {
            const item: Option = {
              label: el,
                value: el
            };
            tempList.push(item);
          });
          this.serverList = tempList;

      }, err => {
          this.commonModalService.openErrorAlert(err);
      });

  }

 /* getCpuUsed(serverName)  {
        const array: any[] = [];

        const args = [
            serverName,
            this.selectedRange,
            this.dateService.getMinDateStr(this.startDate),
            this.dateService.getMinDateStrEx(this.endDate)
        ];
        return this.monitorApi.getCpuUsed(...args);
    }

  renderCpuUsed(res) {
    this.isWorkingCpuUsed = true;
    if (this.cm.isEmpty(res)) {
      this.cm.isDev() && console.log('no data', 'Cpu Used');
      this.cpuResultChartData = [];
      this.cpuResultChartLabels = [];
    }

    const parseData = [];


    const temp =  this.dashboardService.parseCpuUsedResult(this._selectServerOption, res, this.startDate, this.endDate, this.selectedRange);
    parseData.push(temp);



    // 데이터 형식을 맞추기 위해서 씀...
    const data = {
          datas: []
          , period: []

    };

    for (const d of parseData) {
        for (let i = 0; i < d.datas.length; i++) {
          data.datas.push(d.datas[i]);
        }
    }

      data.datas[0]['fill'] = false;
      data.datas[0]['borderWidth'] = 2;
      data.datas[0]['cubicInterpolationMode'] = 'monotone';


    const unit = this.dashboardService.getUnit(this.selectedRange);
    const max = this.dashboardService.getYAxesMaxCpuUsedResult(res);
    const stepSize = this.dashboardService.getStepSize(parseData[0].period, res);
    const format = this.dashboardService.getFormat(this.selectedRange);
    this.cpuResultChartData = data.datas;

    requestAnimationFrame(() => {
      this.cpuResultChartOptions = this.dashboardService.parseChartOptions(max, stepSize, unit, format);
      // this.cm.isDev() && console.log('dres labels', unit, max, stepSize, format , parseData);
        this.cpuResultChartLabels = parseData[0].period;
        this.isWorkingCpuUsed = false;
    });

  }*/


    getCpuUsed(serverName, callback = (res): void => { this.cm.override(); })  {
        const array: any[] = [];

        const args = [
            serverName,
            this.selectedRange,
            this.dateService.getMinDateStr(this.startDate),
            this.dateService.getMinDateStrEx(this.endDate)
        ];

        this.selectServerOption = serverName;

        this.monitorApi.getCpuUsed(...args).subscribe( (res) => {
            if (this.cm.isEmpty(res)) {
                // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
                this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'dlg res data');
                this.isWorkingCpuUsed = false;
                this.cpuTotalMax = '-';
                this.cpuTotalMin = '-';
                this.cpuTotalAvg = '-';
                callback([]);
                return false;
            }

            this.cpuTotalMax = res.maxUsage + '%';
            this.cpuTotalMin = res.minUsage + '%';
            this.cpuTotalAvg = res.avgUsage + '%';

         //   console.log('cpuMAx: ' + this.cpuTotalMax);



            callback(res.data);
        }, err => {
            this.isWorkingCpuUsed = false;
            this.commonModalService.openErrorAlert(err);

        });
    }

    renderCpuUsed(res) {
        this.isWorkingCpuUsed = true;
        if (this.cm.isEmpty(res)) {
            this.cm.isDev() && console.log('no data', 'Cpu Used');
            this.cpuResultChartData = [];
            this.cpuResultChartLabels = [];
        }

        const parseData =  this.dashboardService.parseCpuUsedResult(this.selectServerOption, res, this.startDate, this.endDate, this.selectedRange);


        parseData.datas[0]['fill'] = false;
        parseData.datas[0]['borderWidth'] = 2;
        parseData.datas[0]['cubicInterpolationMode'] = 'monotone';



        const unit = this.dashboardService.getUnit(this.selectedRange);
        const max = this.dashboardService.getYAxesMaxCpuUsedResult(res);
        const stepSize = this.dashboardService.getStepSize(parseData.period, res);
        const format = this.dashboardService.getFormat(this.selectedRange);
        this.cpuResultChartData = parseData.datas;

        requestAnimationFrame(() => {
            this.cpuResultChartOptions = this.dashboardService.parseChartOptions(max, stepSize, unit, format);
            // this.cm.isDev() && console.log('dres labels', unit, max, stepSize, format , parseData);
            this.cpuResultChartLabels = parseData.period;
            this.isWorkingCpuUsed = false;
        });

    }


    getMemUsed(serverName, callback = (res): void => { this.cm.override(); })  {
        const array: any[] = [];
        this.isWorkingMemUsed = true;

        const args = [
            serverName,
            this.selectedRange,
            this.dateService.getMinDateStr(this.startDate),
            this.dateService.getMinDateStrEx(this.endDate)
        ];
        this.selectServerOption2 = serverName;

        this.monitorApi.getMemUsed(...args).subscribe( (res) => {
            if (this.cm.isEmpty(res)) {
                // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
                this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'dlg res data');
                this.isWorkingMemUsed = false;
                this.memTotalMax = '-';
                this.memTotalMin = '-';
                this.memTotalAvg = '-';
                callback([]);
                return false;
            }

            this.memTotalMax = this.getUsageChange(res.maxUsage);
            this.memTotalMin = this.getUsageChange(res.minUsage);
            this.memTotalAvg = this.getUsageChange(res.avgUsage);

            callback(res.data);
        }, err => {
          this.isWorkingMemUsed = false;
          this.commonModalService.openErrorAlert(err);

        });
    }


    renderMemUsed(res) {
        this.isWorkingMemUsed = true;
        if (this.cm.isEmpty(res)) {
            this.cm.isDev() && console.log('no data', 'Mem Used');
            this.memResultChartData = [];
            this.memResultChartLabels = [];
        }

        const parseData =  this.dashboardService.parseMemUsedResult(this.selectServerOption2, res, this.startDate, this.endDate, this.selectedRange);

        parseData.datas[0]['fill'] = false;
        parseData.datas[0]['borderWidth'] = 2;
        parseData.datas[0]['cubicInterpolationMode'] = 'monotone';


        const unit = this.dashboardService.getUnit(this.selectedRange);
        const max = this.dashboardService.getYAxesMaxMemUsedResult(res);
        const stepSize = this.dashboardService.getStepSize(parseData.period, res);
        const format = this.dashboardService.getFormat(this.selectedRange);
        this.memResultChartData = parseData.datas;

        requestAnimationFrame(() => {
            this.memResultChartOptions = this.dashboardService.parseChartOptions(max, stepSize, unit, format);
            // this.cm.isDev() && console.log('dres labels', unit, max, stepSize, format , parseData);
            this.memResultChartLabels = parseData.period;
            this.isWorkingMemUsed = false;
        });

    }


    getDisksed(serverName, callback = (res): void => { this.cm.override(); })  {
        const array: any[] = [];
        this.isWorkingDiskUsed = true;

        const args = [
            serverName,
            this.selectedRange,
            this.dateService.getMinDateStr(this.startDate),
            this.dateService.getMinDateStrEx(this.endDate)
        ];
        this.selectServerOption3 = serverName;

        this.monitorApi.getDiskUsed(...args).subscribe( (res) => {
            if (this.cm.isEmpty(res)) {
                // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
                this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'dlg res data');
                this.isWorkingDiskUsed = false;
                this.diskTotalMax = '-';
                this.diskTotalAvg = '-';
                this.diskTotalMin = '-';
                callback([]);
                return false;
            }

            this.diskTotalMax = this.getUsageChange(res.maxUsage);
            this.diskTotalAvg = this.getUsageChange(res.avgUsage);
            this.diskTotalMin = this.getUsageChange(res.minUsage);

            callback(res.data);
        }, err => {
            this.isWorkingDiskUsed = false;
            this.commonModalService.openErrorAlert(err);

        });
    }


    renderDiskUsed(res) {
        this.isWorkingDiskUsed = true;
        if (this.cm.isEmpty(res)) {
            this.cm.isDev() && console.log('no data', 'Mem Used');
            this.diskResultChartData = [];
            this.diskResultChartLabels = [];
        }

        const parseData =  this.dashboardService.parseDiskUsedResult(this.selectServerOption2, res, this.startDate, this.endDate, this.selectedRange);

        parseData.datas[0]['fill'] = false;
        parseData.datas[0]['borderWidth'] = 2;
        parseData.datas[0]['cubicInterpolationMode'] = 'monotone';


        const unit = this.dashboardService.getUnit(this.selectedRange);
        const max = this.dashboardService.getYAxesMaxDiskUsedResult(res);
        const stepSize = this.dashboardService.getStepSize(parseData.period, res);
        const format = this.dashboardService.getFormat(this.selectedRange);
        this.diskResultChartData = parseData.datas;

        requestAnimationFrame(() => {
            this.memResultChartOptions = this.dashboardService.parseChartOptions(max, stepSize, unit, format);
            // this.cm.isDev() && console.log('dres labels', unit, max, stepSize, format , parseData);
            this.diskResultChartLabels = parseData.period;
            this.isWorkingDiskUsed = false;
        });

    }


    getUsageChange(value) {

        let count = 0;
        let valueSize: string = '';

        while (true) {
            if (value > 1024) {
                value = (value / 1024 );
                count++;
            } else {
                 break;
            }
        }

        if (count  === 1 ) {
            valueSize = value.toFixed(2) + 'GB';
        } else if (count === 2) {
            valueSize = value.toFixed(2) + 'TB';
        }

        return valueSize;
    }



    changeCpuUsed(event) {

      this.getCpuUsed(event.value, (res) => {
          this.renderCpuUsed(res);
          this.isWorkingCpuUsed = false;
      });
    }


    changeMemUsed(event) {
        this.getMemUsed(event.value, (res) => {
            this.renderMemUsed(res);
            this.isWorkingMemUsed = false;
        });
    }


    changeDiskUsed(event) {
        this.getDisksed(event.value, (res) => {
            this.renderDiskUsed(res);
            this.isWorkingDiskUsed = false;
        });
    }

}
