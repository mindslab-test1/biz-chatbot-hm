import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CommonModalService, CommonUtilService} from '../../../service';
import {DataService} from '../../../service/data.service';
import {DateService} from '../../../service/date.service';
import {UserService} from '../../../service/user.service';
import {Chatbot_infoApi, Cm_user, Monitor_serverApi, Stats_dlg_historyApi} from '../../../adminsdk';
import {DashboardService} from '../../../service/dashboard.service';
import {Option} from '../../../model/dom-models';
import {lang, Msg} from '../../../model/message';


export interface ResponseCpuUsedResult {
    period: string;
    serverName: string;
    cpuUsed: number;
}

export interface ResponseMemUsedResult {
    period: string;
    serverName: string;
    memTotal: number;
    memUsage: number;
    memUsed: number;
}



export interface ResponseDiskUsedResult {
    period: string;
    serverName: string;
    diskTotal: number;
    diskUsage: number;
    diskUsed: number;
}



@Component({
    selector: 'app-monitor',
    templateUrl: 'serverStatus.component.html',
    styleUrls: ['./serverStatus.component.scss']
})

export  class ServerStatusComponent implements OnInit, OnDestroy {

    constructor(
        private router: Router
        , private activatedRoute: ActivatedRoute
        , private commonModalService: CommonModalService
        , private cm: CommonUtilService
        , private ds: DataService
        , private dateService: DateService
        , private userService: UserService
        , private api: Stats_dlg_historyApi
        , private monitorApi: Monitor_serverApi
        // , private api: Dlg_historyApi
        , private chatbotInfoApi: Chatbot_infoApi
        , private dashboardService: DashboardService
    ) {
    }


    teamId: number;
    monitorServer: string[] = [];


    userId = this.userService.getCurrentUserId();
    currUserInfo: Cm_user = this.userService.getCachedUserInfo();

    rangeOption = this.dashboardService.rangeOption;

    dd = this.dashboardService.default;
    set = this.dashboardService.SET;
    sc = this.set.CHART;

    private _isWorking = false;

    isWorkingCpuUsed = false;
    isWorkingMemUsed = false;
    isWorkingDiskUsed = false;

    public optionList: Option[];
    public serverList: Option[];

    _selectServerOption = '';
    _selectServerOption2 = '';
    _selectServerOption3 = '';

    ddc = this.dd.chart;

    // CPU 사용률
    cpuResultChartOptions: any = this.ddc.options;
    cpuResultChartLabels: string[] = [];
    cpuResultChartData: any[] = [];
    cpuResultChartType = 'line';
    cpuResultChartLegend = false;
    cpuTotalMax: string;
    cpuTotalMin: string;
    cpuTotalAvg: string;

    // MEM 사용률
    memResultChartOptions: any = this.ddc.options;
    memResultChartLabels: string[] = [];
    memResultChartData: any[] = [];
    memResultChartType = 'line';
    memResultChartLegend = false;
    memTotal: string;
    memTotalUsed: string;
    memTotalUSageAvg: string;
    memTotalMax: string;
    memTotalMin: string;
    memTotalAvg: string;

    // DISK 사용률
    diskResultChartOptions: any = this.ddc.options;
    diskResultChartLabels: string[] = [];
    diskResultChartData: any[] = [];
    diskResultChartType = 'line';
    diskResultChartLegend = false;
    diskTotal: string;
    diskTotalUsed: string;
    diskTotalUSageAvg: string;
    diskTotalMax: string;
    diskTotalMin: string;
    diskTotalAvg: string;


    now: Date = this.dateService.getNowDate();

    private _startDate: Date = this.dateService.getDefaultStartDate(this.now);
    private _endDate: Date = this.dateService.getDefaultEndDate(this.now);
    private _minDate = this.dateService.getDefaultStartDateBeforeOneMonth(this.endDate, this.set.INTERVAL);
    private _range = this.rangeOption.time;


    ngOnInit() {

        this.teamId = parseInt(this.userService.initTeamId, 10);
        this.init();

    }

    rangeByTime = () => this.dashboardService.rangeByTime(this.selectedRange);
    rangeByDay = () => this.dashboardService.rangeByDay(this.selectedRange);

    ngOnDestroy() {
        this.ds.clearData();
    }

    private init() {
        this.optionList = [
            {label: '챗봇 전체', value: ''}
        ];

        this.isWorking = true;
        this.getServerNames((): void => {
            this._search();
        });

        this.isWorking = true;
    }


    get isWorking() {
        return this._isWorking;
    }

    set isWorking(v: boolean) {
        if (this._isWorking === v) {
            return;
        }

        this._isWorking = v;
        this.ds.sendData(this.ds.CMD.IS_WORKING, {isWorking: v});
    }


    get startDate(): Date {
        return this._startDate;
    }

    set startDate(startDate) {
        startDate = this.dateService.setDefaultStartTime(startDate);
        this._startDate = startDate;
    }

    get endDate(): Date {
        return this._endDate;
    }
    set endDate(endDate) {
        // endDate.setDate(endDate.getDate() + 1);
        endDate = this.dateService.setDefaultEndTime(endDate);
        this._endDate = endDate;

        if (this.dashboardService.rangeByTime(this.selectedRange)) {
            this.startDate = this.dateService.setDefaultStartTime(this.endDate);
        } else {
            this._minDate = this.dateService.getDefaultStartDateBeforeOneMonth(this.endDate, this.set.INTERVAL);
        }
    }
    get minDate() {
        return this._minDate;
    }


    get selectServerOption(): string {
        return this._selectServerOption;
    }
    set selectServerOption(v: string) {
        this._selectServerOption = v;
    }


    get selectServerOption2(): string {
        return this._selectServerOption2;
    }
    set selectServerOption2(v: string) {
        this._selectServerOption2 = v;
    }

    get selectServerOption3(): string {
        return this._selectServerOption3;
    }
    set selectServerOption3(v: string) {
        this._selectServerOption3 = v;
    }



    get selectedRange() {
        return this._range;
    }
    set selectedRange(v) {
        this._range = v;

        if (this.dashboardService.rangeByTime(this.selectedRange)) {
            this.startDate = this.dateService.getDefaultStartDate(this.endDate);
        } else {
            this.startDate = this.dateService.getDefaultStartDateBeforeOneMonth(this.endDate);
        }
    }

    get title () {
        return Msg.com[lang].titleDashboard;
    }

    search(event: any = null) {
        if (this.isWorking) {
            this.commonModalService.openErrorAlert(Msg.err[lang].nowLoading);
            return;
        }
        this.isWorking = true;
        this._search();
    }

    _search() {
        this.renderStats();
    }

    renderStats() {
        this.isWorking = false;

        if (typeof this.monitorServer !== 'undefined' && this.monitorServer.length > 0) {
            const memSverver = this.selectServerOption2 || this.monitorServer[0];
            this.getMemUsed(memSverver, (res) => {
                this.renderMemUsed(res);
            });

            const cpuServer = this.selectServerOption || this.monitorServer[0];
            this.getCpuUsed(cpuServer, (res) => {
                this.renderCpuUsed(res);
                this.isWorkingCpuUsed = false;
            });

            const diskServer = this.selectServerOption3 || this.monitorServer[0];
            this.getDisksed(diskServer, (res) => {
                this.renderDiskUsed(res);
                this.isWorkingDiskUsed = false;
            });
        }
    }


    getServerNames(callback = (): void => { this.cm.override(); }) {
        this.monitorApi.getServiceNames().subscribe(res => {
            if (this.cm.isEmpty(res)) {
                // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
                this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'serverName data');
                return false;
            }

            const tempList: Option[] = [];
            this.monitorServer = res;

            res.map((el: string) => {
                const item: Option = {
                    label: el,
                    value: el
                };
                tempList.push(item);
            });
            this.serverList = tempList;

            this.isWorking = false;
            callback();

        }, err => {
            this.isWorking = false;
            this.commonModalService.openErrorAlert(err);
        });

    }

    /* getCpuUsed(serverName)  {
           const array: any[] = [];

           const args = [
               serverName,
               this.selectedRange,
               this.dateService.getMinDateStr(this.startDate),
               this.dateService.getMinDateStrEx(this.endDate)
           ];
           return this.monitorApi.getCpuUsed(...args);
       }

     renderCpuUsed(res) {
       this.isWorkingCpuUsed = true;
       if (this.cm.isEmpty(res)) {
         this.cm.isDev() && console.log('no data', 'Cpu Used');
         this.cpuResultChartData = [];
         this.cpuResultChartLabels = [];
       }

       const parseData = [];


       const temp =  this.dashboardService.parseCpuUsedResult(this._selectServerOption, res, this.startDate, this.endDate, this.selectedRange);
       parseData.push(temp);



       // 데이터 형식을 맞추기 위해서 씀...
       const data = {
             datas: []
             , period: []

       };

       for (const d of parseData) {
           for (let i = 0; i < d.datas.length; i++) {
             data.datas.push(d.datas[i]);
           }
       }

         data.datas[0]['fill'] = false;
         data.datas[0]['borderWidth'] = 2;
         data.datas[0]['cubicInterpolationMode'] = 'monotone';


       const unit = this.dashboardService.getUnit(this.selectedRange);
       const max = this.dashboardService.getYAxesMaxCpuUsedResult(res);
       const stepSize = this.dashboardService.getStepSize(parseData[0].period, res);
       const format = this.dashboardService.getFormat(this.selectedRange);
       this.cpuResultChartData = data.datas;

       requestAnimationFrame(() => {
         this.cpuResultChartOptions = this.dashboardService.parseChartOptions(max, stepSize, unit, format);
         // this.cm.isDev() && console.log('dres labels', unit, max, stepSize, format , parseData);
           this.cpuResultChartLabels = parseData[0].period;
           this.isWorkingCpuUsed = false;
       });

     }*/


    getCpuUsed(serverName, callback = (res): void => { this.cm.override(); })  {
        const array: any[] = [];

        const args = [
            serverName,
            this.selectedRange,
            this.dateService.getMinDateStr(this.startDate),
            this.dateService.getMinDateStrEx(this.endDate)
        ];

        this.selectServerOption = serverName;

        this.monitorApi.getCpuUsed(...args).subscribe( (res) => {
            if (this.cm.isEmpty(res)) {
                // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
                this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'dlg res data');
                this.isWorkingCpuUsed = false;
                this.cpuTotalMax = '-';
                this.cpuTotalMin = '-';
                this.cpuTotalAvg = '-';
                callback([]);
                return false;
            }

            this.cpuTotalMax = res.maxUsage + '%';
            this.cpuTotalMin = res.minUsage + '%';
            this.cpuTotalAvg = res.avgUsage + '%';



            callback(res.data);
        }, err => {
            this.isWorkingCpuUsed = false;
            this.commonModalService.openErrorAlert(err);

        });
    }


    renderCpuUsed(res) {
        this.isWorkingCpuUsed = true;
        if (this.cm.isEmpty(res)) {
            this.cm.isDev() && console.log('no data', 'Cpu Used');
            this.cpuResultChartData = [];
            this.cpuResultChartLabels = [];
        }

        const parseData =  this.dashboardService.parseCpuUsedResult(this.selectServerOption, res, this.startDate, this.endDate, this.selectedRange);


        parseData.datas[0]['fill'] = false;
        parseData.datas[0]['borderWidth'] = 2;
        parseData.datas[0]['cubicInterpolationMode'] = 'monotone';



        const unit = this.dashboardService.getUnit(this.selectedRange);
        const max = this.dashboardService.getYAxesMaxCpuUsedResult(res);
        const stepSize = this.dashboardService.getStepSize(parseData.period, res);
        const format = this.dashboardService.getFormat(this.selectedRange);
        this.cpuResultChartData = parseData.datas;

        requestAnimationFrame(() => {
            this.cpuResultChartOptions = this.dashboardService.parseChartOptions(max, stepSize, unit, format);
            // this.cm.isDev() && console.log('dres labels', unit, max, stepSize, format , parseData);
            this.cpuResultChartLabels = parseData.period;
            this.isWorkingCpuUsed = false;
        });

    }


    getMemUsed(serverName, callback = (res): void => { this.cm.override(); })  {
        const array: any[] = [];
        this.isWorkingMemUsed = true;

        const args = [
            serverName,
            this.selectedRange,
            this.dateService.getMinDateStr(this.startDate),
            this.dateService.getMinDateStrEx(this.endDate)
        ];
        this.selectServerOption2 = serverName;

        this.monitorApi.getMemUsed(...args).subscribe( (res) => {
            if (this.cm.isEmpty(res)) {
                // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
                this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'dlg res data');
                this.isWorkingMemUsed = false;
                this.memTotalMax = '-';
                this.memTotalMin = '-';
                this.memTotalAvg = '-';
                this.memTotal = '-';
                callback([]);
                return false;
            }

            this.memTotalMax = this.getUsageChange(res.maxUsage);
            this.memTotalMin = this.getUsageChange(res.minUsage);
            this.memTotalAvg = this.getUsageChange(res.avgUsage);
            this.memTotal = this.getUsageChange(res.data[0].memTotal);

            callback(res.data);
        }, err => {
            this.isWorkingMemUsed = false;
            this.commonModalService.openErrorAlert(err);

        });
    }


    renderMemUsed(res) {
        this.isWorkingMemUsed = true;
        if (this.cm.isEmpty(res)) {
            this.cm.isDev() && console.log('no data', 'Mem Used');
            this.memResultChartData = [];
            this.memResultChartLabels = [];
        }

        const parseData =  this.dashboardService.parseMemUsedResult(this.selectServerOption2, res, this.startDate, this.endDate, this.selectedRange);

        parseData.datas[0]['fill'] = false;
        parseData.datas[0]['borderWidth'] = 2;
        parseData.datas[0]['cubicInterpolationMode'] = 'monotone';


        const unit = this.dashboardService.getUnit(this.selectedRange);
        const max = this.dashboardService.getYAxesMaxMemUsedResult(res);
        const stepSize = this.dashboardService.getStepSize(parseData.period, res);
        const format = this.dashboardService.getFormat(this.selectedRange);
        this.memResultChartData = parseData.datas;

        requestAnimationFrame(() => {
            this.memResultChartOptions = this.dashboardService.parseChartOptions(max, stepSize, unit, format);
            // this.cm.isDev() && console.log('dres labels', unit, max, stepSize, format , parseData);
            this.memResultChartLabels = parseData.period;
            this.isWorkingMemUsed = false;
        });

    }


    getDisksed(serverName, callback = (res): void => { this.cm.override(); })  {
        const array: any[] = [];
        this.isWorkingDiskUsed = true;

        const args = [
            serverName,
            this.selectedRange,
            this.dateService.getMinDateStr(this.startDate),
            this.dateService.getMinDateStrEx(this.endDate)
        ];
        this.selectServerOption3 = serverName;

        this.monitorApi.getDiskUsed(...args).subscribe( (res) => {
            if (this.cm.isEmpty(res)) {
                // this.commonModalService.openErrorAlert(Msg.err[lang].failLoadData);
                this.cm.isDev() && console.log(Msg.err[lang].failLoadData, 'dlg res data');
                this.isWorkingDiskUsed = false;
                this.diskTotalMax = '-';
                this.diskTotalAvg = '-';
                this.diskTotalMin = '-';
                this.diskTotal = '-';
                callback([]);
                return false;
            }

            this.diskTotalMax = this.getUsageChange(res.maxUsage);
            this.diskTotalAvg = this.getUsageChange(res.avgUsage);
            this.diskTotalMin = this.getUsageChange(res.minUsage);
            this.diskTotal = this.getUsageChange(res.data[0].diskTotal);

            callback(res.data);
        }, err => {
            this.isWorkingDiskUsed = false;
            this.commonModalService.openErrorAlert(err);

        });
    }


    renderDiskUsed(res) {
        this.isWorkingDiskUsed = true;
        if (this.cm.isEmpty(res)) {
            this.cm.isDev() && console.log('no data', 'Mem Used');
            this.diskResultChartData = [];
            this.diskResultChartLabels = [];
        }

        const parseData =  this.dashboardService.parseDiskUsedResult(this.selectServerOption2, res, this.startDate, this.endDate, this.selectedRange);

        parseData.datas[0]['fill'] = false;
        parseData.datas[0]['borderWidth'] = 2;
        parseData.datas[0]['cubicInterpolationMode'] = 'monotone';


        const unit = this.dashboardService.getUnit(this.selectedRange);
        const max = this.dashboardService.getYAxesMaxDiskUsedResult(res);
        const stepSize = this.dashboardService.getStepSize(parseData.period, res);
        const format = this.dashboardService.getFormat(this.selectedRange);
        this.diskResultChartData = parseData.datas;

        requestAnimationFrame(() => {
            this.memResultChartOptions = this.dashboardService.parseChartOptions(max, stepSize, unit, format);
            // this.cm.isDev() && console.log('dres labels', unit, max, stepSize, format , parseData);
            this.diskResultChartLabels = parseData.period;
            this.isWorkingDiskUsed = false;
        });

    }

    getUsageChange(value) {

        let count = 0;
        let valueSize: string = '';

        while (true) {
            if (value > 1024) {
                value = (value / 1024 );
                count++;
            } else {
                break;
            }
        }

        if (count  === 1 ) {
            valueSize = value.toFixed(2) + 'GB';
        } else if (count === 2) {
            valueSize = value.toFixed(2) + 'TB';
        }

        return valueSize;
    }


    changeStauts(event) {
        this.changeCpuUsed(event);
        this.changeMemUsed(event);
        this.changeDiskUsed(event);
    }


    changeCpuUsed(event) {
        this.getCpuUsed(event.value, (res) => {
            this.renderCpuUsed(res);
            this.isWorkingCpuUsed = false;
        });
    }


    changeMemUsed(event) {
        this.getMemUsed(event.value, (res) => {
            this.renderMemUsed(res);
            this.isWorkingMemUsed = false;
        });
    }


    changeDiskUsed(event) {
        this.getDisksed(event.value, (res) => {
            this.renderDiskUsed(res);
            this.isWorkingDiskUsed = false;
        });
    }


    // events
    cpuResultChartClicked(e: any): void {
        this.cm.isDev() && console.log('CpuUsed chart click', e);
    }

    cpuResultChartHovered(e: any): void {
        this.cm.isDev() && console.log('CpuUsed chart hover', e);
    }

    memResultChartClicked(e: any): void {
        this.cm.isDev() && console.log('MemUsed chart click', e);
    }

    memResultChartHovered(e: any): void {
        this.cm.isDev() && console.log('MemUsed chart hover', e);
    }


    diskResultChartClicked(e: any): void {
        this.cm.isDev() && console.log('diskUsed chart click', e);
    }

    diskResultChartHovered(e: any): void {
        this.cm.isDev() && console.log('diskUsed chart hover', e);
    }



}
