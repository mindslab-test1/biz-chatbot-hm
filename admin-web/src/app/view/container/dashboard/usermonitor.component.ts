import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {Http} from '@angular/http';
import {CommonUtilService} from '../../../service';
import {environment as env} from '../../../environments/environment';

@Component({
    selector: 'app-user-monitor',
    templateUrl: 'usermonitor.component.html',
    styleUrls: ['./usermonitor.component.scss']
})
export class UsermonitorComponent implements OnInit, OnDestroy {
    kivana =  env.kabanaUrl + `/visualize/edit/4f496630-6d37-11e9-b703-cfa4ca01bdb2?embed=true`;
    term = '30d';
    urlDomainlist: SafeResourceUrl;

    @ViewChild('frameWrapper')
    frameWrapper: ElementRef;
    @ViewChild('frameScenario')
    frameScenario: ElementRef;
    _range = '';
    rangeOption = {
        day: 'day'
        , week: 'week'
        , month: 'month'
    };

    constructor(
        private http: Http,
        private cm: CommonUtilService,
        private sanitizer: DomSanitizer
    ) { }

    ngOnInit() {
        this.init();
    }


    ngOnDestroy() {}

    init() {
        this._range = this.rangeOption.month;
        const url = this.kivana + `&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-${this.term},mode:quick,to:now))`;
        //console.log(url);
        this.loadUrl(url);
    }

    loadUrl(url) {
        this.urlDomainlist = this.sanitizer.bypassSecurityTrustResourceUrl(url);
        requestAnimationFrame(() => {
            this._initIframe();
        });
    }


    get selectedRange() {
        return this._range;
    }

    set selectedRange(v) {
        console.log(v);
        this._range = v;
        let url = this.kivana ;

        if (v === 'week') {
            this.term = '7d';
        } else if (v === 'month') {
            this.term = '30d';
        } else if (v === 'day') {
            this.term = '1d';
        }
        url +=  `&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-${this.term},mode:quick,to:now))`;
     //   console.log(url);
        this.loadUrl(url);
    }

    _initIframe() {
        const iframe: HTMLIFrameElement = this.frameScenario.nativeElement;
        // this.setUrl();
        iframe.width = '100%';
        iframe.height = '100%';
        iframe.style.overflow = 'auto';

        const iframeWrapper: HTMLDivElement = this.frameWrapper.nativeElement;
        iframeWrapper.style.height = '100%';

        /*
        내부 바디에... 줄 수 있다면 아래속성 주면 1280 에서 딱 맞음.
        transform: scale(0.8,0.8);
        overflow: auto;
        */
    }
}
