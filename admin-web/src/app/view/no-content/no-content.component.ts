import { Component } from '@angular/core';

@Component({
  selector: 'no-content',
  template: `
    <div class="loading-content-container">
      <div class="loading-form-wrapper">
        <p class="logo">
            <img src="assets/images/logo.svg" alt="logo">
        </p>
        <p class="title">404: Page Missing</p>
      </div>
    </div>
  `,
  styleUrls: ['./no-contents.component.scss']
})
export class NoContentComponent {

}
