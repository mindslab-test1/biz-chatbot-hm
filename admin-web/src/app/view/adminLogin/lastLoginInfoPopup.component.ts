import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {UserService} from '../../service/user.service';

@Component({
  selector: 'app-popup',
  templateUrl: './lastLoginInfoPopup.component.html',
  styleUrls: ['./lastLoginInfoPopup.component.scss']
})


export class LastLoginInfoPopupComponent implements OnInit {
  lastAccess = '';
  accessIp = '';
  accessUser = '';
  signedYn = '';
  @Output() closePopUp = new EventEmitter(false);
  @Input() viewFlag;
  userId: string = '';
  constructor(  private userService: UserService) {

  }

  ngOnInit() {}


  public openDialog(id) {
      this.userId = id;
      this.init();
  }

  init(): void {


  //  this.mainDiv.nativeElement.style.top = '50%';
  //  this.mainDiv.nativeElement.marginTop = '30px';

    const workspace_id = this.userService.getWorkspaceIds().pop();
    const team_id = parseInt(this.userService.initTeamId, 10);

    this.userService.getLastLoginInfo(this.userId, workspace_id, team_id).subscribe(res => {
      this.lastAccess = res.accessTime;
      this.accessIp = res.remoteIp;
      this.accessUser = res.userId;

      if (res.signedYn) {
        this.signedYn = '성공';
      }else {
        this.signedYn = '실패';
      }
    });
  }


  public closePop(): void {
    this.closePopUp.emit(false);
  }
}
