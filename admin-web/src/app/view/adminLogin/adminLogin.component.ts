import {Component, OnInit, Inject, Input, EventEmitter, ViewChild, AfterViewInit, OnDestroy, HostListener} from '@angular/core';
import { Router } from '@angular/router';
import { environment as env } from '../../environments/environment';
import { AuthService, CommonModalService, AlertModalEvent, CommonModalEvent, ModalType, MenuService, CommonUtilService} from '../../service';
import { CommonModalComponent } from '../../common';
import { UserService } from '../../service/user.service';
import { Msg, lang } from '../../model/message';
import { CookieService } from 'ng2-cookies';
/*
  2018-09-19 로그인페이지 변경 요청
  실제 사용
*/
@Component({
  selector: 'app-login',
  templateUrl: 'adminLogin.component.html',
  styleUrls: ['./adminLogin.component.scss']
})
export class AdminLoginComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private menuService: MenuService,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService,
    private cookie: CookieService
  ) {

  }
  pageFlag:boolean = false;
  errFlag:boolean = false;
  disableFlag:boolean = false;
  ipt_check:boolean = false;
  cookieData:any;
  @Input() ipt_id: string;
  @Input() ipt_pw: string;

  ngOnInit() {
    const temp = this.cookie.get('ipt_check');
    if (temp && temp === '1') {
      this.ipt_check = true;
      const idTemp = this.cookie.get('ipt_id');
      if (idTemp && idTemp.length > 0) {
        this.ipt_id = idTemp;
      }
    }
  }

  ngOnDestroy() {
      console.log('close');
  }


  changeRequestData() {

    if ((typeof this.ipt_id !== 'undefined' && this.ipt_id.length === 0) ||
                                           (typeof this.ipt_pw !== 'undefined' && this.ipt_pw.length === 0)) {
      this.disableFlag = true;
    } else {
      this.disableFlag = false;
    }
  }

  public enterLogin($event) {
      if (!this.cm.isEnter($event) || this.disableFlag) {
          return false;
      }
      this.login();
  }

  public login() {

    this.userService.checkConnectedUser(this.ipt_id).subscribe( result => {

      if (result) {
          if (confirm('이미 로그인 중입니다. 기존로그인을 종료하시고, 접속하시겠습니까?')) {
              this.loginProcess();
          } else {
              return false;
          }
      } else {
          this.loginProcess();
      }

    });

  }

  public loginProcess() {

      // (this.cm.isDev()) && console.log('login...');
      this.userService.login({

          username: this.ipt_id,
          password: this.ipt_pw
          // ,team_id: 1, role_id: 'ADMIN'
      }).subscribe(res => {

          this.cm.isDev() && console.log('login...');
          this.menuService.removeResponseMenusFromLocalStorage();

          requestAnimationFrame(() => {
              this.menuService.init(() => {
                  this.cm.isDev() && console.log('login after frame go home...');

                  if (this.ipt_check) {
                      this.cookie.set('ipt_id', this.ipt_id);
                      this.cookie.set('ipt_check', '1');

                  } else {
                      this.cookie.delete('ipt_id');
                      this.cookie.delete('ipt_check');

                  }
                  this.goHome();
              }, subError => {
                  this.commonModalService.openErrorAlert(subError);
              });
          });
      }, error => {
          this.authService.clear();
          this.commonModalService.openErrorAlert(error);
      });
  }


  /**
   * via biz-web/..../auth.service.ts
   * 가정: 할당 받은 워크스페이스가 없으면, 애초에 로그인 할 수 없다
   */
  goHome(): void {
    requestAnimationFrame(() => {
      const user = this.userService.getCachedUserInfo();
      if (user) {
          const roleId = this.userService.getUserRoleId();

          if (roleId && roleId !== this.userService.getRoles.ADMIN) {
              this.router.navigateByUrl(env.defaultPath2);
          } else {
              this.router.navigateByUrl(env.defaultPath);
          }
      } else {
        this.router.navigateByUrl(env.root);
      }
    });
  }


  /**
   * ====================================================
   * ==== Modal 관련 코드
   * ==== via container.component.js
   * ====================================================
   */
  @ViewChild('alertModal')
  alertModal: CommonModalComponent;
  alertModalEventEmitter: EventEmitter<AlertModalEvent>;
  alertModalTitle: string;
  alertModalText: string;
  alertModalHasCancel: boolean;
  onCloseAlertModal(isOk: boolean) {
    if (this.alertModalEventEmitter !== null) {
      const evt = new AlertModalEvent();
      evt.isOk = isOk;
      this.alertModalEventEmitter.emit(evt);
    }
  }
  ngAfterViewInit() {
    this.commonModalService.modalHandler
    .subscribe((event: CommonModalEvent) => {
      this.initCallbacks();
      switch (event.modalType) {
      case ModalType.ALERT_MESSAGE_MODAL: {
        this.alertModalTitle = event.modalTitle;
        this.alertModalText = event.modalTextMessage;
        this.alertModalHasCancel = event.modalHasCancelButton;
        this.alertModalEventEmitter = event.eventEmitter;
        if (event.isOpen) {
          this.alertModal.modalOpen();
        } else {
          this.alertModal.modalClose();
        }
        break;
      }
      default : throw new Error('INVALID');
      }
    });
  }
  private initCallbacks() {
    this.alertModal.closeAllModals();
    this.alertModalEventEmitter = null;
    this.alertModalHasCancel = false;
    this.alertModalTitle = Msg.com[lang].titleAlert;
    this.alertModalText = '';
  }
}
