import { Component, OnInit, Inject, Input, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment as env } from '../../environments/environment';
import { AuthService, CommonModalService, AlertModalEvent, CommonModalEvent, ModalType, MenuService, CommonUtilService} from '../../service';
import { CommonModalComponent } from '../../common';
import { UserService } from '../../service/user.service';
import { Msg, lang } from '../../model/message';

/*
  2018-09-19 로그인페이지 변경 요청
  사용안함(보관용)
*/
@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private menuService: MenuService,
    private commonModalService: CommonModalService,
    private cm: CommonUtilService
  ) {

  }

  @Input() userId: string;
  @Input() password: string;

  ngOnInit() {
  }
  public login() {
    // (this.cm.isDev()) && console.log('login...');

    this.userService.login({
        username: this.userId, password: this.password
        // ,team_id: 1, role_id: 'ADMIN'
    }).subscribe(res => {
        this.cm.isDev() && console.log('login...');
        this.menuService.removeResponseMenusFromLocalStorage();
        requestAnimationFrame(()=>{
          this.menuService.init(()=>{
            this.cm.isDev() && console.log('login after frame go home...');
            this.goHome();
          }, subError => {
              this.commonModalService.openErrorAlert(subError);
          });
        });
    }, error => {
        this.authService.clear();
        this.commonModalService.openErrorAlert(error);
    });
  }

  /**
   * via biz-web/..../auth.service.ts
   * 가정: 할당 받은 워크스페이스가 없으면, 애초에 로그인 할 수 없다
   */
  goHome(): void {
    requestAnimationFrame(()=>{
      const user = this.userService.getCachedUserInfo();
      if (user) {
        const roleId = this.userService.getUserRoleId();

        if (roleId && roleId !== this.userService.getRoles.ADMIN) {
          this.router.navigateByUrl(env.defaultPath2);
        } else {
          this.router.navigateByUrl(env.defaultPath);
        }
      } else {
        this.router.navigateByUrl(env.root);
      }
    });
  }


  /**
   * ====================================================
   * ==== Modal 관련 코드
   * ==== via container.component.js
   * ====================================================
   */
  @ViewChild('alertModal')
  alertModal: CommonModalComponent;
  alertModalEventEmitter: EventEmitter<AlertModalEvent>;
  alertModalTitle: string;
  alertModalText: string;
  alertModalHasCancel: boolean;
  onCloseAlertModal(isOk: boolean) {
    if (this.alertModalEventEmitter !== null) {
      const evt = new AlertModalEvent();
      evt.isOk = isOk;
      this.alertModalEventEmitter.emit(evt);
    }
  }
  ngAfterViewInit() {
    this.commonModalService.modalHandler
    .subscribe((event: CommonModalEvent) => {
      this.initCallbacks();
      switch (event.modalType) {
      case ModalType.ALERT_MESSAGE_MODAL: {
        this.alertModalTitle = event.modalTitle;
        this.alertModalText = event.modalTextMessage;
        this.alertModalHasCancel = event.modalHasCancelButton;
        this.alertModalEventEmitter = event.eventEmitter;
        if (event.isOpen) {
          this.alertModal.modalOpen();
        } else {
          this.alertModal.modalClose();
        }
        break;
      }
      default : throw 'INVALID';
      }
    });
  }
  private initCallbacks() {
    this.alertModal.closeAllModals();
    this.alertModalEventEmitter = null;
    this.alertModalHasCancel = false;
    this.alertModalTitle = Msg.com[lang].titleAlert;
    this.alertModalText = '';
  }
}
