import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { LoopBackConfig } from './adminsdk';
import {environment} from './environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(
    private router: Router) {

      LoopBackConfig.setBaseURL(environment.adminApiUrl);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }
}
