import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';


/**
 * number 타입을 string 으로 변환하여 반환한다.
 */
@Pipe({
    name: 'NumberToString'
})
export class NumberToStringPipe implements PipeTransform {

    transform(value: number): string {
        return String(value);
    }
}


@Pipe({
  name: 'dateTimeFormat'
})
export class DateTimeFormatPipe  implements PipeTransform {
  transform(value: any, pattern: any): any {
      // console.log(pattern);
      let mom =  moment(value);
      let timeValue: string = '';

      if (typeof pattern !== 'undefined' && pattern.length > 0){
          timeValue = mom.format(pattern);
      } else {
          timeValue = mom.format('YY.MM.DD HH:mm:ss');
      }
    return timeValue;
  }
}
