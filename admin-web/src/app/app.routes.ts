/**
 * TODO 1차 개발범위 아닌 컴포넌트는 import 구문 및 route 주석 처리함.
 */

/**
 *
 */
import { Routes } from '@angular/router';
import { RouteActivate } from './provide/route.activate';
import { RouteDeactivate } from './provide/route.deactivate';
import { LoginComponent } from './view/login';
import { AdminLoginComponent } from './view/adminLogin';
import { NoContentComponent } from './view/no-content';

// common layout
import { ContainerComponent } from './view/container';

// contents
import { DashboardComponent } from './view/container/dashboard';
import { ServerStatusComponent } from './view/container/dashboard/serverStatus.component';
import {DialogsErrorComponent} from './view/container/dialogs/dialog-logs/dialog-error.component';
import { NlpTestComponent } from './view/container/add-ons/nlp-test/nlp-test.component';
import { DocsComponent } from './view/container/docs/docs.component';

import { WorkspacesComponent } from './view/container/management/workspaces/workspaces.component';
import { TeamsComponent } from './view/container/management/teams/teams.component';
import { UsersComponent } from './view/container/management/users/users.component';
import { CommonCodesComponent } from './view/container/management/common-codes/common-codes.component';
import { CommonCodeGroupsComponent } from './view/container/management/common-code-groups/common-code-groups.component';
import { ProfileComponent } from './view/container/profile/profile.component';

import { ChatBaseComponent } from './view/container/chatbot/chat-base/chat-base.component';
import { CPreitfComponent } from './view/container/chatbot/c-preitf/c-preitf.component';
import { CScenarioComponent } from './view/container/chatbot/c-scenario/c-scenario.component';
import { SimpleClassifierComponent } from './view/container/chatbot/c-scenario/simple-classifier/simple-classifier.component';
import { DnnClassifierComponent } from './view/container/chatbot/c-scenario/dnn-classifier/dnn-classifier.component';
// import { ChannelConnectionComponent } from './view/container/chatbot/channel-connection/channel-connection.component';
import { CBqaComponent } from './view/container/chatbot/c-bqa/c-bqa.component';
import { DBqaComponent } from './view/container/domains/d-bqa/d-bqa.component';
import { DScenarioComponent } from './view/container/domains/d-scenario/d-scenario.component';
// import { TaskGraphComponent } from './view/container/domains/d-scenario/task-graph/task-graph.component';
// import { EntitiesComponent } from './view/container/domains/d-scenario/entities/entities.component';
// import { IntentsComponent } from './view/container/domains/d-scenario/intents/intents.component';
// import { GlobalEntitiesComponent } from './view/container/add-ons/global-entities/global-entities.component';
// import { SlotSearchComponent } from './view/container/add-ons/global-entities/slot-search/slot-search.component';
// import { SlotDetailComponent } from './view/container/add-ons/global-entities/slot-detail/slot-detail.component';
// import { InstanceComponent } from './view/container/add-ons/global-entities/instance/instance.component';
// import { GlobalIntentsComponent } from './view/container/add-ons/global-intents/global-intents.component';
import { ApiMgmtComponent } from './view/container/add-ons/api-mgmt/api-mgmt.component';
import { AAddComponent } from './view/container/add-ons/api-mgmt/a-add/a-add.component';
import { AUpdateComponent } from './view/container/add-ons/api-mgmt/a-update/a-update.component';
// import { StopWordsComponent } from './view/container/add-ons/dic/stop-words/stop-words.component';
// import { ProhibitiveWordsComponent } from './view/container/add-ons/dic/prohibitive-words/prohibitive-words.component';
import { ChatbotManagementComponent } from './view/container/chatbot/chatbot-management.component';
import { ChatBotComponent } from './view/container/chatbot/c-client/chatbot.component';
import { ChatbotContainerComponent } from './view/container/chatbot/chatbot-container.component';

import { DialogLogsComponent } from './view/container/dialogs/dialog-logs/dialog-logs.component';
import { DialogLogsDetailComponent } from './view/container/dialogs/dialog-logs/dialog-logs-detail.component';
import { SynonymComponent } from './view/container/add-ons/dic/synonym/synonym.component';
import { CTestComponent } from './view/container/chatbot/c-test/c-test.component';
import {BlankComponent} from './view/container/blank/blank.component';
import {PreClassifierComponent} from './view/container/chatbot/c-preitf/pre-classifiler/pre-classifier.component';
// import {IndexStatComponent} from './view/container/search/index/index-stat.component';
// import {IndexLogsComponent} from './view/container/search/index/index-logs.component';
import {DicSynonymComponent} from './view/container/search/dic/synonym/dic-synonym.component';
import { DicMorphemeComponent } from './view/container/search/dic/morpheme/dic-morpheme.component';
import { DicStopwordComponent } from './view/container/search/dic/stopword/dic-stopword.component';
import {AutocompleteComponent} from './view/container/search/autocomplete/autocomplete.component';
import {SpellcheckComponent} from './view/container/search/spellcheck/spellcheck.component';
import {EtlStatComponent} from './view/container/search/etl/stat/etl-stat.component';
import {EtlLogsComponent} from './view/container/search/etl/logs/etl-logs.component';
import {PopularComponent} from './view/container/search/popular/popular.component';
import {RelatedComponent} from './view/container/search/related/related.component';
import {RankingComponent} from './view/container/search/ranking/ranking.component';
import {StatClickComponent} from './view/container/search/stat/click/stat-click.component';
import {StatPopularComponent} from './view/container/search/stat/popular/stat-popular.component';
import {StatFailComponent} from './view/container/search/stat/fail/stat-fail.component';
import {StatQueryComponent} from './view/container/search/stat/query/stat-query.component';
import {SuggestionOpinionComponent} from './view/container/add-ons/sug-op/suggestionOpinion.component';
import {UsermonitorComponent} from './view/container/dashboard/usermonitor.component';
import {TemActiveComponent} from './view/container/dashboard/temActive.component';
import {DeptActiveComponent} from './view/container/dashboard/deptActive.component';
import {NlpSynonymComponent} from './view/container/nlp/synonym/nlp-synonym.component';
import {NlpUserDicComponent} from './view/container/nlp/user-dic/nlp-user-dic.component';
// import { StatQueryComponent } from './view/container/search/stat/stat-query.component';
// import { NoAnswerLearningComponent } from './view/container/dialogs/no-answer-learning/no-answer-learning.component';

export const ROUTES: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: AdminLoginComponent },
  { path: 'blank', component: BlankComponent },
  { path: 'management', component: ContainerComponent, canActivate: [RouteActivate],
    // canDeactivate: [RouteDeactivate] ,
    children: [


      { path: 'dashboard', component: DashboardComponent },
      { path: 'monitor', component: ServerStatusComponent},
      { path: 'dash-user', component: UsermonitorComponent},
      { path: 'team-active', component: TemActiveComponent},
      { path: 'dept-active', component: DeptActiveComponent},


      /** 챗봇 */
      { path: 'chatbot-management', component: ChatbotManagementComponent, canActivate: [RouteActivate] },
      { path: 'chatbot', component: ChatbotContainerComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] ,
        children: [
          { path: 'chat-base', component: ChatBaseComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-preitf', component: CPreitfComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-preitf/pre-classifier', component: PreClassifierComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-bqa', component: CBqaComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-scenario', component: CScenarioComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-scenario/simple-classifier', component: SimpleClassifierComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-scenario/dnn-classifier', component: DnnClassifierComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-test', component: CTestComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          // { path: 'channel-connection', component: ChannelConnectionComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },

          { path: 'chat-base/:bot_id', component: ChatBaseComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-preitf/:bot_id', component: CPreitfComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-preitf/pre-classifier/:bot_id', component: PreClassifierComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-bqa/:bot_id', component: CBqaComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-scenario/:bot_id', component: CScenarioComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-scenario/simple-classifier/:bot_id', component: SimpleClassifierComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-scenario/dnn-classifier/:bot_id', component: DnnClassifierComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          { path: 'c-test/:bot_id', component: CTestComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },
          // { path: 'channel-connection/:bot_id', component: ChannelConnectionComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] },

          { path: 'c-client', component: ChatBotComponent, canActivate: [RouteActivate], canDeactivate: [RouteDeactivate] }
        ]
      },


      /** 도메인 관리 */
      { path: 'domains/d-bqa', component: DBqaComponent, canActivate: [RouteActivate] },
      { path: 'domains/d-scenario', component: DScenarioComponent, canActivate: [RouteActivate] },
      // { path: 'domains/d-scenario/task-graph', component: TaskGraphComponent, canActivate: [RouteActivate] },
      // { path: 'domains/d-scenario/entities', component: EntitiesComponent, canActivate: [RouteActivate] },
      // { path: 'domains/d-scenario/intents', component: IntentsComponent, canActivate: [RouteActivate] },

      /** 글로벌 엔티티 */
      // { path: 'add-ons/global-entities', component: GlobalEntitiesComponent, canActivate: [RouteActivate] },
      // { path: 'add-ons/global-entities/slot-search', component: SlotSearchComponent, canActivate: [RouteActivate] },
      // { path: 'add-ons/global-entities/slot-detail', component: SlotDetailComponent, canActivate: [RouteActivate] },
      // { path: 'add-ons/global-entities/instance', component: InstanceComponent, canActivate: [RouteActivate] },

      /** 글로벌 인텐드 */
      // TODO 추가 정의되면 내용 추가 예정
      // { path: 'add-ons/global-intents', component: GlobalIntentsComponent, canActivate: [RouteActivate] },

      /** API 관리 */
      { path: 'add-ons/api-mgmt', component: ApiMgmtComponent, canActivate: [RouteActivate] },
      { path: 'add-ons/api-mgmt/a-add', component: AAddComponent, canActivate: [RouteActivate] },
      { path: 'add-ons/api-mgmt/a-update', component: AUpdateComponent, canActivate: [RouteActivate] },

      /** NLP 테스트 */
      { path: 'add-ons/nlp-test', component: NlpTestComponent, canActivate: [RouteActivate] },

      /** 커스텀 사전 */
      { path: 'add-ons/dic/synonyms', component: SynonymComponent, canActivate: [RouteActivate] },
      // { path: 'add-ons/dic/stop-words', component: StopWordsComponent, canActivate: [RouteActivate] },
      // { path: 'add-ons/dic/prohibitive-words', component: ProhibitiveWordsComponent, canActivate: [RouteActivate] },

      /** 의견제안  */
      { path: 'add-ons/sug-op', component: SuggestionOpinionComponent, canActivate: [RouteActivate] },


      /** 검색엔진 */
      // { path: 'search/index/stat', component: IndexStatComponent, canActivate: [RouteActivate] },
      // { path: 'search/index/logs', component: IndexLogsComponent, canActivate: [RouteActivate] },

      /** 사전*/
      { path: 'search/dic/synonyms', component: DicSynonymComponent, canActivate: [RouteActivate] },
      { path: 'search/dic/stopwords', component: DicStopwordComponent, canActivate: [RouteActivate] },
      { path: 'search/dic/morphemes', component: DicMorphemeComponent, canActivate: [RouteActivate] },

      /* 자동 완성 */
      { path: 'search/autocompletes', component: AutocompleteComponent, canActivate: [RouteActivate] },

      // 인기검색어
      { path: 'search/popular', component: PopularComponent, canActivate: [RouteActivate] },

      // 연관검색어
      { path: 'search/related', component: RelatedComponent, canActivate: [RouteActivate] },

      // 랭킹
      { path: 'search/ranking', component: RankingComponent, canActivate: [RouteActivate] },


      // 오타교정
      { path: 'search/spellchecks', component: SpellcheckComponent, canActivate: [RouteActivate] },
      /** 통계*/
      // { path: 'search/stat/query', component: IndexStatComponent, canActivate: [RouteActivate] },
      { path: 'search/stat/click', component: StatClickComponent, canActivate: [RouteActivate] },
      { path: 'search/stat/popular', component: StatPopularComponent, canActivate: [RouteActivate] },
      { path: 'search/stat/fail', component: StatFailComponent, canActivate: [RouteActivate] },
      // { path: 'search/stat/query-group', component: IndexStatComponent, canActivate: [RouteActivate] },
      { path: 'search/stat/query', component: StatQueryComponent, canActivate: [RouteActivate] },

      /** etl */
      { path: 'search/etl/stat', component: EtlStatComponent },
      { path: 'search/etl/logs', component: EtlLogsComponent },


      /** 대화 모니터링 */
      { path: 'dialogs/logs', component: DialogLogsComponent, canActivate: [RouteActivate] },
      { path: 'dialogs/logs/detail', component: DialogLogsDetailComponent, canActivate: [RouteActivate] },
      { path: 'dialogs/logs/detail/:session_id', component: DialogLogsDetailComponent, canActivate: [RouteActivate] },
      // { path: 'dialogs/learning', component: NoAnswerLearningComponent, canActivate: [RouteActivate] },
      { path: 'dialogs/error', component: DialogsErrorComponent, canActivate: [RouteActivate] },


      /** 기본 관리 */
      { path: 'management/workspaces', component: WorkspacesComponent, canActivate: [RouteActivate] },
      { path: 'management/teams', component: TeamsComponent, canActivate: [RouteActivate] },
      { path: 'management/users', component: UsersComponent, canActivate: [RouteActivate] },
      { path: 'management/common-codes', component: CommonCodesComponent, canActivate: [RouteActivate] },
      { path: 'management/common-code-groups', component: CommonCodeGroupsComponent, canActivate: [RouteActivate] },


      /** 프로필 */
      { path: 'profile', component: ProfileComponent, canActivate: [RouteActivate] },

      /** 사용자 메뉴얼 */
      { path: 'docs', component: DocsComponent, canActivate: [RouteActivate] },

      /** NLP 사전처리 */
      { path: 'nlp/synonym/kor_eng', component: NlpSynonymComponent, canActivate: [RouteActivate] },
      { path: 'nlp/synonym/abbreviation', component: NlpSynonymComponent, canActivate: [RouteActivate] },
      { path: 'nlp/synonym/synonyms', component: NlpSynonymComponent, canActivate: [RouteActivate] },
      { path: 'nlp/synonym/spacing', component: NlpSynonymComponent, canActivate: [RouteActivate] },

      { path: 'nlp/user-dic/employees', component: NlpUserDicComponent, canActivate: [RouteActivate] },
      { path: 'nlp/user-dic/teams', component: NlpUserDicComponent, canActivate: [RouteActivate] },
      { path: 'nlp/user-dic/wrong_analysis', component: NlpUserDicComponent, canActivate: [RouteActivate] },
      { path: 'nlp/user-dic/special_characters', component: NlpUserDicComponent, canActivate: [RouteActivate] },
    ]






  },
  { path: '**', component: NoContentComponent },
];
