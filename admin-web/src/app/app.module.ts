import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
// import {LOCALE_ID} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Http, HttpModule, RequestOptions, XHRBackend} from '@angular/http';
import {Router, RouterModule} from '@angular/router';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {CookieService} from 'ng2-cookies';

import {ROUTES} from './app.routes';
import {
    AuthService,
    BasicQAService,
    CommonModalService,
    DefaultService,
    MenuService,
    MessageExService,
    SynonymService,
    UtilService
} from './service';

import {AppComponent} from './app.component';
import {LoginComponent} from './view/login';
import {AdminLoginComponent} from './view/adminLogin';
import {LastLoginInfoPopupComponent} from './view/adminLogin/lastLoginInfoPopup.component';
import {NoContentComponent} from './view/no-content';

import {SDKBrowserModule} from './adminsdk';
import {ClipboardModule} from 'ngx-clipboard';
// Common-UI
import {CommonModalComponent, CommonTableComponent} from './common';
import {
    BreadcrumbModule,
    CalendarModule,
    CheckboxModule,
    ChipsModule,
    ContextMenuModule,
    DataGridModule, DataScrollerModule,
    DataTableModule,
    DropdownModule,
    GrowlModule,
    InputSwitchModule,
    MenubarModule,
    PaginatorModule,
    ProgressBarModule,
    ProgressSpinnerModule,
    RadioButtonModule,
    SharedModule,
    TabMenuModule,
    TooltipModule,
    TreeModule,
    TreeTableModule
} from 'primeng/primeng';
// Common layout
import {ContainerComponent} from './view/container';
import {FooterComponent, HeaderComponent} from './view/container/shared';
import {MenuComponent} from './view/container/menu';
// Contents
import {DashboardComponent} from './view/container/dashboard';
import {ChattingDefaultComponent} from './view/container/chatbot/c-client/c-message/chatting.default.component';
import {ChattingClientComponent} from './view/container/chatbot/c-client/c-message/chatting.client.component';
import {ChattingButtonComponent} from './view/container/chatbot/c-client/c-message/chatting.button.component';
import {ChattingMetaComponent} from './view/container/chatbot/c-client/c-message/chatting.meta.component';
// import {NlpTestComponent} from './view/container/tool/index';
import {NlpTestComponent} from './view/container/add-ons/nlp-test/nlp-test.component';
import {CommonUtilService} from './service/commonutil.service';
import {DocsComponent} from './view/container/docs/docs.component';
import {WorkspacesComponent} from './view/container/management/workspaces/workspaces.component';
import {TeamsComponent} from './view/container/management/teams/teams.component';
import {UsersComponent} from './view/container/management/users/users.component';
import {CommonCodesComponent} from './view/container/management/common-codes/common-codes.component';
import {EmptyValuePipe} from './provide/empty-value.pipe';
import {PaginatorComponent} from './common/paginator/paginator.component';
import {CommonCodeGroupsComponent} from './view/container/management/common-code-groups/common-code-groups.component';
import {PageSizeSellectorComponent} from './common/paginator/page-size-sellector.component';
import {ProfileComponent} from './view/container/profile/profile.component';
import {ReduceJsonPipe} from './provide/reduce-json.pipe';
import {ChatBaseComponent} from './view/container/chatbot/chat-base/chat-base.component';
import {CPreitfComponent} from './view/container/chatbot/c-preitf/c-preitf.component';
import {PreClassifierComponent} from './view/container/chatbot/c-preitf/pre-classifiler/pre-classifier.component';
import {CScenarioComponent} from './view/container/chatbot/c-scenario/c-scenario.component';
import {SimpleClassifierComponent} from './view/container/chatbot/c-scenario/simple-classifier/simple-classifier.component';
import {DnnClassifierComponent} from './view/container/chatbot/c-scenario/dnn-classifier/dnn-classifier.component';
import {ChannelConnectionComponent} from './view/container/chatbot/channel-connection/channel-connection.component';
import {CBqaComponent} from './view/container/chatbot/c-bqa/c-bqa.component';
import {DBqaComponent} from './view/container/domains/d-bqa/d-bqa.component';
import {DScenarioComponent} from './view/container/domains/d-scenario/d-scenario.component';
import {TaskGraphComponent} from './view/container/domains/d-scenario/task-graph/task-graph.component';
import {EntitiesComponent} from './view/container/domains/d-scenario/entities/entities.component';
import {IntentsComponent} from './view/container/domains/d-scenario/intents/intents.component';
import {GlobalEntitiesComponent} from './view/container/add-ons/global-entities/global-entities.component';
import {SlotSearchComponent} from './view/container/add-ons/global-entities/slot-search/slot-search.component';
import {SlotDetailComponent} from './view/container/add-ons/global-entities/slot-detail/slot-detail.component';
import {InstanceComponent} from './view/container/add-ons/global-entities/instance/instance.component';
import {GlobalIntentsComponent} from './view/container/add-ons/global-intents/global-intents.component';
import {ApiMgmtComponent} from './view/container/add-ons/api-mgmt/api-mgmt.component';
import {AAddComponent} from './view/container/add-ons/api-mgmt/a-add/a-add.component';
import {AUpdateComponent} from './view/container/add-ons/api-mgmt/a-update/a-update.component';
import {StopWordsComponent} from './view/container/add-ons/dic/stop-words/stop-words.component';
import {ProhibitiveWordsComponent} from './view/container/add-ons/dic/prohibitive-words/prohibitive-words.component';
import {ChatbotManagementComponent} from './view/container/chatbot/chatbot-management.component';
import {ChatBotComponent} from './view/container/chatbot/c-client/chatbot.component';
import {IsEmptyPipe} from './provide/is-empty.pipe';
import {CloseComponent} from './view/container/shared/close.component';
import {ChatbotUpsertService} from './service/chatbot-upsert.service';
import {ChatbotUpsertSharedComponent} from './view/container/chatbot/shared/chatbot-upsert-shared.component';
import {ChatbotContainerComponent} from './view/container/chatbot/chatbot-container.component';
import {DataService} from './service/data.service';
import {DateService} from './service/date.service';
import {FileService} from './service/file.service';
import {FilePipe} from './provide/file.pipe';
import {BqaListComponent} from './view/container/domains/d-bqa/bqa-list/bqa-list.component';
import {BqaAddComponent} from './view/container/domains/d-bqa/bqa-add/bqa-add.component';
import {BqaEditComponent} from './view/container/domains/d-bqa/bqa-edit/bqa-edit.component';
import {BqaIndexComponent} from './view/container/domains/d-bqa/bqa-index/bqa-index.component';
import {PrimeTreeComponent} from './view/container/shared/prime-tree.component';
import {RouteDeactivate} from './provide/route.deactivate';
import {BqaDomainComponent} from './view/container/domains/d-bqa/bqa-domain/bqa-domain.component';
import {BqaDomainAddComponent} from './view/container/domains/d-bqa/bqa-domain-add/bqa-domain-add.component';
import {BqaDomainEditComponent} from './view/container/domains/d-bqa/bqa-domain-edit/bqa-domain-edit.component';
import {SynonymComponent} from './view/container/add-ons/dic/synonym/synonym.component';
import {SynonymAddComponent} from './view/container/add-ons/dic/synonym/synonym-add/synonym-add.component';
import {SynonymEditComponent} from './view/container/add-ons/dic/synonym/synonym-edit/synonym-edit.component';
import {SynonymRemoveComponent} from './view/container/add-ons/dic/synonym/synonym-remove/synonym-remove.component';
import {UserService} from './service/user.service';
import {DialogLogsComponent} from './view/container/dialogs/dialog-logs/dialog-logs.component';
import {DialogLogsDetailComponent} from './view/container/dialogs/dialog-logs/dialog-logs-detail.component';
import {HttpClientModule} from '@angular/common/http';
import {ValidatorService} from './service/validator.service';
import {ApiHelperService} from './service/api-helper.service';
import {NgJsonEditorModule} from './view/container/add-ons/api-mgmt/component/ng-jsoneditor/ng-jsoneditor.module';

import {ChatbotDialogService} from './service/chatbot-dialog.service';
import {FrontApiHelperService} from './service/front-api-helper.service';
import {ChatbotInfoService} from './service/chatbot-info.service';
import {ChattingCarouselComponent} from './view/container/chatbot/c-client/c-message/chatting-carousel.component';
import {LnToBrPipe} from './provide/ln-to-br.pipe';
import {ChatbotDebugSharedComponent} from './view/container/chatbot/shared/chatbot-debug-shared.component';
import {ChatbotDebugInitGreetComponent} from './view/container/chatbot/shared/chatbot-debug-init-greet.component';
import {ChatbotDebugBqaComponent} from './view/container/chatbot/shared/chatbot-debug-bqa.component';
import {ChatbotDebugSdsComponent} from './view/container/chatbot/shared/chatbot-debug-sds.component';
import {CTestComponent} from './view/container/chatbot/c-test/c-test.component';
import {PreAddComponent} from './view/container/chatbot/c-preitf/pre-classifiler/pre-add.component';
import {PreEditComponent} from './view/container/chatbot/c-preitf/pre-classifiler/pre-edit.component';
import {ClassifierAddComponent} from './view/container/chatbot/c-scenario/simple-classifier/classifier-add.component';
import {ClassifierEditComponent} from './view/container/chatbot/c-scenario/simple-classifier/classifier-edit.component';
import {DnnAddComponent} from './view/container/chatbot/c-scenario/dnn-classifier/dnn-add.component';
import {DnnEditComponent} from './view/container/chatbot/c-scenario/dnn-classifier/dnn-edit.component';
import {DnnOrderComponent} from './view/container/chatbot/c-scenario/dnn-classifier/dnn-order.component';
import {DialogLogService} from './service/dialog-log.service';
import {ConvertCasePipe} from './provide/convert-case.pipe';
import {IsDateStrPipe} from './provide/is-date-string.pipe';
import {SdsService} from './service/sds.service';
import {DashboardService} from './service/dashboard.service';
import {ChartsModule} from 'ng2-charts';
import {JsonpathComponent} from './view/container/add-ons/api-mgmt/jsonpath/jsonpath.component';
import {SdsApiHelperService} from './service/sds-api-helper.service';
import {BackApiHelperService} from './service/back-api-helper.service';
import {ProgressSpinnerComponent} from './view/container/shared/progress-spinner.component';
import {RouteActivate} from './provide/route.activate';
import {HttpInterceptorFactory} from './provide/http.interceptor';
import {PlaceholderComponent} from './view/container/shared/placeholder.component';
import {PlaceholderTableComponent} from './view/container/shared/placeholder-table.component';
import {BlankComponent} from './view/container/blank/blank.component';
import {NumberToStringPipe, DateTimeFormatPipe} from './shared/common.pipe';
import {WindowRef} from './view/container/shared/windowRef';
import {ChattingImageComponent} from './view/container/chatbot/c-client/c-message/chatting.image.component';
import {IndexStatComponent} from './view/container/search/index/index-stat.component';
import {IndexLogsComponent} from './view/container/search/index/index-logs.component';
import {ServerStatusComponent} from './view/container/dashboard/serverStatus.component';
import {DialogsErrorComponent} from './view/container/dialogs/dialog-logs/dialog-error.component';
import {DialogErrorService} from './service/dialog-error.service';
import { DicSynonymComponent } from './view/container/search/dic/synonym/dic-synonym.component';
import { DicSynonymEditComponent } from './view/container/search/dic/synonym/synonym-edit/synonym-edit.component';
import { DicSynonymRemoveComponent } from './view/container/search/dic/synonym/synonym-remove/synonym-remove.component';
import { DicSynonymAddComponent } from './view/container/search/dic/synonym/synonym-add/synonym-add.component';
import { DicMorphemeComponent } from './view/container/search/dic/morpheme/dic-morpheme.component';
import { MorphemeAddComponent } from './view/container/search/dic/morpheme/morpheme-add/morpheme-add.component';
import { MorphemeEditComponent } from './view/container/search/dic/morpheme/morpheme-edit/morpheme-edit.component';
import { MorphemeRemoveComponent } from './view/container/search/dic/morpheme/morpheme-remove/morpheme-remove.component';
import { DicStopwordComponent } from './view/container/search/dic/stopword/dic-stopword.component';
import { StopwordAddComponent } from './view/container/search/dic/stopword/stopword-add/stopword-add.component';
import { StopwordEditComponent } from './view/container/search/dic/stopword/stopword-edit/stopword-edit.component';
import { StopwordRemoveComponent } from './view/container/search/dic/stopword/stopword-remove/stopword-remove.component';
import {AutocompleteAddComponent} from './view/container/search/autocomplete/autocomplete-add/autocomplete-add.component';
import {AutocompleteEditComponent} from './view/container/search/autocomplete/autocomplete-edit/autocomplete-edit.component';
import {AutocompleteRemoveComponent} from './view/container/search/autocomplete/autocomplete-remove/autocomplete-remove.component';
import {AutocompleteComponent} from './view/container/search/autocomplete/autocomplete.component';
import {SpellcheckComponent} from './view/container/search/spellcheck/spellcheck.component';
import {SpellcheckAddComponent} from './view/container/search/spellcheck/spellcheck-add/spellcheck-add.component';
import {SpellcheckEditComponent} from './view/container/search/spellcheck/spellcheck-edit/spellcheck-edit.component';
import {SpellcheckRemoveComponent} from './view/container/search/spellcheck/spellcheck-remove/spellcheck-remove.component';
import {EtlStatComponent} from './view/container/search/etl/stat/etl-stat.component';

import { COMPOSITION_BUFFER_MODE } from '@angular/forms';
import {EtlService} from './service/etl.service';
import {EtlLogsComponent} from './view/container/search/etl/logs/etl-logs.component';
import {PopularComponent} from './view/container/search/popular/popular.component';
import {PopularAddComponent} from './view/container/search/popular/popular-add/popular-add.component';
import {PopularEditComponent} from './view/container/search/popular/popular-edit/popular-edit.component';
import {PopularRemoveComponent} from './view/container/search/popular/popular-remove/popular-remove.component';
import {RelatedComponent} from './view/container/search/related/related.component';
import {RelatedAddComponent} from './view/container/search/related/related-add/related-add.component';
import {RelatedEditComponent} from './view/container/search/related/related-edit/related-edit.component';
import {RelatedRemoveComponent} from './view/container/search/related/related-remove/related-remove.component';
import {RankingComponent} from './view/container/search/ranking/ranking.component';
import {RankingAddComponent} from './view/container/search/ranking/ranking-add/ranking-add.component';
import {RankingEditComponent} from './view/container/search/ranking/ranking-edit/ranking-edit.component';
import {RankingRemoveComponent} from './view/container/search/ranking/ranking-remove/ranking-remove.component';
import {StatClickComponent} from './view/container/search/stat/click/stat-click.component';
import {StatFailComponent} from './view/container/search/stat/fail/stat-fail.component';
import {StatPopularComponent} from './view/container/search/stat/popular/stat-popular.component';
import {StatQueryComponent} from './view/container/search/stat/query/stat-query.component';
import {ElasticsearchService} from './service/elasticsearch.service';
import {ElasticsearchService2} from './service/elasticsearch2.service';
import {SuggestionOpinionComponent} from './view/container/add-ons/sug-op/suggestionOpinion.component';
import {FrontUserCommentService} from './service/front_user_commnet.service';
import {UsermonitorComponent} from './view/container/dashboard/usermonitor.component';
import {TemActiveComponent} from './view/container/dashboard/temActive.component';
import {DeptActiveComponent} from './view/container/dashboard/deptActive.component';
import {NlpSynonymComponent} from './view/container/nlp/synonym/nlp-synonym.component';
import {NlpSynonymAddComponent} from './view/container/nlp/synonym/synonym-add/nlp-synonym-add.component';
import {NlpSynonymEditComponent} from './view/container/nlp/synonym/synonym-edit/nlp-synonym-edit.component';
import {NlpSynonymRemoveComponent} from './view/container/nlp/synonym/synonym-remove/nlp-synonym-remove.component';
import {NlpUserDicComponent} from './view/container/nlp/user-dic/nlp-user-dic.component';
import {NlpUserDicAddComponent} from './view/container/nlp/user-dic/user-dic-add/nlp-user-dic-add.component';
import {NlpUserDicEditComponent} from './view/container/nlp/user-dic/user-dic-edit/nlp-user-dic-edit.component';
import {NlpUserDicRemoveComponent} from './view/container/nlp/user-dic/user-dic-remove/nlp-user-dic-remove.component';

@NgModule({
    declarations: [

        AppComponent,
        LoginComponent,
        AdminLoginComponent,
        LastLoginInfoPopupComponent,

        NoContentComponent,

        // Common-UI
        CommonModalComponent,
        CommonTableComponent,

        // common layout
        ContainerComponent,
        HeaderComponent,
        FooterComponent,
        MenuComponent,

        // contents
        DashboardComponent,
        ServerStatusComponent,
        ChatBotComponent,
        ChattingDefaultComponent,
        ChattingClientComponent,
        ChattingButtonComponent,
        ChattingImageComponent,
        ChattingCarouselComponent,
        ChattingMetaComponent,
        // basic
        // chatbot management
        ChatbotManagementComponent,
        // tool
        NlpTestComponent,
        SynonymComponent,
        DicSynonymComponent,
        // monitoring
        DialogLogsComponent,
        DialogLogsDetailComponent,
        // docs
        DocsComponent,
        // management
        WorkspacesComponent,
        TeamsComponent,
        UsersComponent,
        CommonCodesComponent,
        PaginatorComponent,

        CommonCodeGroupsComponent,
        PageSizeSellectorComponent,
        ProfileComponent,

        EmptyValuePipe,
        ReduceJsonPipe,
        ChatBaseComponent,
        CPreitfComponent,
        CScenarioComponent,
        CTestComponent,
        PreClassifierComponent,
        SimpleClassifierComponent,
        DnnClassifierComponent,
        ChannelConnectionComponent,
        CBqaComponent,
        DBqaComponent,
        DScenarioComponent,
        TaskGraphComponent,
        EntitiesComponent,
        IntentsComponent,
        GlobalEntitiesComponent,
        SlotSearchComponent,
        SlotDetailComponent,
        InstanceComponent,
        GlobalIntentsComponent,
        ApiMgmtComponent,
        AAddComponent,
        AUpdateComponent,
        StopWordsComponent,
        ProhibitiveWordsComponent,
        IsEmptyPipe,
        CloseComponent,
        ChatbotUpsertSharedComponent,
        ChatbotContainerComponent,
        FilePipe,

        BqaListComponent,
        BqaAddComponent,
        BqaEditComponent,
        BqaIndexComponent,

        PrimeTreeComponent,
        BqaDomainComponent,
        BqaDomainAddComponent,
        BqaDomainEditComponent,

        // old menus
        SynonymAddComponent,
        SynonymEditComponent,
        SynonymRemoveComponent,
        DicSynonymAddComponent,
        DicSynonymEditComponent,
        DicSynonymRemoveComponent,
        DicMorphemeComponent,
        MorphemeAddComponent,
        MorphemeEditComponent,
        MorphemeRemoveComponent,
        DicStopwordComponent,
        StopwordAddComponent,
        StopwordEditComponent,
        StopwordRemoveComponent,
        AutocompleteComponent,
        AutocompleteAddComponent,
        AutocompleteEditComponent,
        AutocompleteRemoveComponent,
        SpellcheckComponent,
        SpellcheckAddComponent,
        SpellcheckEditComponent,
        SpellcheckRemoveComponent,
        PopularComponent,
        PopularAddComponent,
        PopularEditComponent,
        PopularRemoveComponent,
        RelatedComponent,
        RelatedAddComponent,
        RelatedEditComponent,
        RelatedRemoveComponent,
        RankingComponent,
        RankingAddComponent,
        RankingEditComponent,
        RankingRemoveComponent,
        EtlStatComponent,
        EtlLogsComponent,
        StatClickComponent,
        StatFailComponent,
        StatPopularComponent,
        StatQueryComponent,
        DashboardComponent,
        LnToBrPipe,
        ChatbotDebugSharedComponent,
        ChatbotDebugInitGreetComponent,
        ChatbotDebugBqaComponent,
        ChatbotDebugSdsComponent,
        CTestComponent,
        ConvertCasePipe,
        IsDateStrPipe,
        PreAddComponent,
        PreEditComponent,
        ClassifierAddComponent,
        ClassifierEditComponent,
        DnnAddComponent,
        DnnEditComponent,
        DnnOrderComponent,
        JsonpathComponent,
        ProgressSpinnerComponent,
        PlaceholderComponent,
        PlaceholderTableComponent,
        BlankComponent,
        NumberToStringPipe,
        DateTimeFormatPipe,

        // search
        IndexStatComponent,
        IndexLogsComponent,
        DialogsErrorComponent,

        SuggestionOpinionComponent,
        UsermonitorComponent,
        TemActiveComponent,
        DeptActiveComponent,

        // NLP
        NlpSynonymComponent,
        NlpSynonymAddComponent,
        NlpSynonymEditComponent,
        NlpSynonymRemoveComponent,
        NlpUserDicComponent,
        NlpUserDicAddComponent,
        NlpUserDicEditComponent,
        NlpUserDicRemoveComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,
        GrowlModule,
        RadioButtonModule,
        CheckboxModule,
        TooltipModule,
        CalendarModule,
        DropdownModule,
        TreeTableModule,
        TreeModule,
        InputSwitchModule,
        ChipsModule,
        TabMenuModule,
        PaginatorModule,
        DataTableModule,
        SharedModule,
        DataGridModule,
        ProgressBarModule,
        MenubarModule,
        ContextMenuModule,
        BreadcrumbModule,
        SDKBrowserModule.forRoot(),
        RouterModule.forRoot(ROUTES, {useHash: true}),
        ClipboardModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgJsonEditorModule,
        ChartsModule,
        ProgressSpinnerModule,
        DataScrollerModule
    ],
    providers: [
        AuthService,
        ApiHelperService,
        BackApiHelperService,
        BasicQAService,
        ChatbotDialogService,
        ChatbotInfoService,
        ChatbotUpsertService,
        CommonUtilService,
        CommonModalService,
        CookieService,
        DashboardService,
        EtlService,
        DataService,
        DateService,
        DefaultService,
        DialogLogService,
        MenuService,
        MessageExService,
        RouteActivate,
        RouteDeactivate,
        SynonymService,
        ElasticsearchService,
        ElasticsearchService2,
        // TemplateService,
        FileService,
        FrontApiHelperService,
        SdsApiHelperService,
        UserService,
        UtilService,
        ValidatorService,
        SdsService,
        WindowRef,
        DialogErrorService,
        FrontUserCommentService,
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        { provide: Http,
            useFactory: HttpInterceptorFactory,
            deps: [ XHRBackend, RequestOptions, Router, CookieService ]
        },
        {
            provide: COMPOSITION_BUFFER_MODE,
            useValue: false      },
        // ,{ provide: LOCALE_ID, useValue: 'en-GB' }
    ],
    exports: [
        NumberToStringPipe,
        DateTimeFormatPipe,

    ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
