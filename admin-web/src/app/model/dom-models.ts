export interface Option {
  label: string;
  value: any;
}

export interface OptionEx {
  label: string;
  value: any;
  unit: string;
}