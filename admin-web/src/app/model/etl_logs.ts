export interface EtlLogs {
  indexing_type: string;
  service_name: string;
  adindexing_add_sizedress: number;
  indexing_mod_size: number;
  indexing_del_size: number;
  indexing_total_size: number;
  index_name: string;
  alias_name: string;
  error_msg: string;
  execute_time: string;
  folder_path: string;
  start_time: string;
  end_time: string;
  start_time_without_time: string;
  end_time_without_time: string;




}

export interface EtlLogsSource {
  _source: EtlLogs;
}


