export interface ReqOpenDialog {
  "chatbotId": string,
  "accessChannel": string,
  "user": {
    "userName": string,
    "userId": any
  },
  "showDebug": boolean
}
export interface ReqCloseDialog {
  "chatbotId": string,
  "accessChannel": string,
  "sessionId": string,
  "user": {
    "userName": string,
    "userId": any
  }
}
export interface ReqDialog {
  "speaker": string,
  "userId": any,
  "sessionId": string,
  "timestamp": any,
  "sentence": string
}