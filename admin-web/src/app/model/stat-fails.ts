export interface Fails {
  key : string;
  doc_count : number;
  total : number;
}


export interface StatFails {
  buckets: Fails;
}


