/* Generated from Java with JSweet 2.0.0 - http://www.jsweet.org */
import { BqaDebugWrap } from './response-debug';
import { SdsDebug } from './response-debug-sds';
export interface RespDialog {
  sessionId: string;
  chatbotId: number;
  userId: string;
  // result: TALK_RESULT;
  result: any;
  uttrData: UtterData;
}
export const TALK_RESULT_S = {
  SUCCESS: 'SUCCESS',
  SYS_FAILURE: 'SYS_FAILURE',
  SDS_FAILURE: 'SDS_FAILURE',
  UNKNOWN: 'UNKNOWN'
};
export const DIALOG_SPEAKER_S = {
  CHATBOT: 'CHATBOT',
  USER: 'USER'
};
export interface UtterData {
  // speaker: DIALOG_SPEAKER;
  speaker: any;
  utter_id: number;
  userId: string;
  sessionId: string;
  timestamp: Date;
  sentence: string;
  dialogSeq: number;
  currStep: string;
  sluWeight: number;
  sdsDomain?: string;
  da?: string;
  richContent?: ChatbotRichContent;
  debug?: SdsDebug;
  bqaDebug?: BqaDebugWrap;
  bqaDebugEx?: any;
  contentJson?: string;
  isDialogEnded: number;
  reconfirmMsg: ReconfirmMsg;
  intenJson?: string;
}

// export class SdsUtterRespData {
//   system: string;
//   slu: string;
//   da: string;
//   isDialogEnd: string;
//   slu_weight: string;
//   slu_similar_str: string;
//   debug: string;
//   err_msg: string;
//   rx: string;
//   time: string;
//   deamon_log_file_name: string;
//   cmd: string;
//   engine: string = "_DIALOG_SYSTEM_ENGINE_";
//   servPort: string = "_DIALOG_SYSTEM_SERV_PORT_";
// }
export interface ChatbotRichContent {
  rcId: number;
  contentTypeCd: string;
  title: string;
  content: string;
  imgPath: string;
  linkPath: string;
  createdDtm: Date;
  updatedDtm: Date;
  creatorId: string;
  updatorId: string;
  buttons: Array<ChatbotButton>;
  image: ChatbotRichImage;
  carousels: Array<ChatbotRichCarousel>;
}
export interface ChatbotButton {
  rcId: number;
  btnOrder: number;
  title: string;
  userUtter: string;
  createdDtm: Date;
  updatedDtm: Date;
  creatorId: string;
  updatorId: string;
}
export interface ChatbotRichImage {
  rcId: number;
  imagePath: string;
  linkUrl: string;
  imageText: string;
}
export interface ChatbotRichCarousel {
  rcId: number;
  carouselOrder: number;
  imagePath: string;
  title: string;
  subTitle: string;
  utterTypeCd: string;
  userUtter: string;
}

export interface ReconfirmMsg {
  msg: string;
  richContent: ChatbotRichContent;

}
