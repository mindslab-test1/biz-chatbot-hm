export const chatbotStepInit = [
   'InitGreeting'
];
export const chatbotStepBqa = [
   'InitialSynonym'
  ,'BQA'
  ,'BQARichContent'
];
export const chatbotStepScf = [
  'SimpleClassifier'
];
export const chatbotStepSds = [
   'IntentFinderSds'
  ,'DnnClassifier'
  ,'Sds'
  ,'SdsRichContent'
  ,'Interface'
];
export const chatbotStepInvalid = [
  'invalid'
];
export const chatbotStepGroupNames = {
    init: 'init'
  , bqa: 'bqa'
  , scf: 'scf'
  , sds: 'sds'
  , invalid: 'invalid'
};
const names = chatbotStepGroupNames;
export const chatbotStepGroups = {
    init: {name: names.init, member: chatbotStepInit }
  , bqa: {name: names.bqa, member: chatbotStepBqa }
  , scf: {name: names.scf, member: chatbotStepScf }
  , sds: {name: names.sds, member: chatbotStepSds }
  , invalid: {name: names.invalid, member: chatbotStepInvalid}
};
