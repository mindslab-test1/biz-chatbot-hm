export class	IChatbotConst	{
	static MAP_NAME_SESSION: string = "sessionss";
	static MAP_NAME_DIALOG: string = "dialogs";
	static MAP_NAME_CONFIG: string = "objcache";//general
	static MAP_NAME_BOTCONFIG: string = "botconfig";//bot

	static TOPIC_DIALOG_PREFIX: string = "^dialog$";

	static ERR_CODE_INVALID_SESSION_ID: number = 800000;
	static ERR_MSG_INVALID_SESSION_ID: string = "INVALID SESSION ID";
	static ERR_CODE_INVALID_CHATBOT: number = 800001;
	static ERR_MSG_INVALID_CHATBOT: string = "INVALID CHATBOT";

	static KEY_PRJ_PORT: string = "project_port";
	static KEY_PRJ_PORT_ITF: string = "project_port_itf";


	static API_HTTP_METHOD_GET: string = "GET";
	static API_HTTP_METHOD_POST: string = "POST";

	static API_HTTP_PARAM_TYPE_HEADER: string = "PT0001";
	static API_HTTP_PARAM_TYPE_PARAM: string = "PT0002";

	static RICH_CONTENT_TYPE_BUTTON: string = "RT0001";
	static RICH_CONTENT_TYPE_IMAGE: string = "RT0002";
	static RICH_CONTENT_TYPE_CAROUSEL: string = "RT0003";
	static RICH_CONTENT_TYPE_API: string = "RT9001";

	static MSG_TYPE_GREETING: string = "MT0001";
	static MSG_TYPE_UNKNOWN: string = "MT0002";

	static SDS_TERMINATION_BY_USER: string = "TR0001";
	static SDS_TERMINATION_BY_SYSTEM: string = "TR0002";

	static PREFIX_SYSOUT: string = "__RESCODE_";
}

export enum ACCESS_CHANNEL{
  WEB_BROWSER,
  ANDROID,
  IOS
}

export enum DIALOG_SPEAKER{
  CHATBOT,
  USER
}

//여기서 확장
export enum TALK_RESULT{
  SUCCESS,
  SYS_FAILURE,
  SDS_FAILURE,
  UNKNOWN
}

export enum SDS_STATUS{
  BEFORE_OPEN,
  OPEN,
  CLOSED
}