import { RespDialog } from "./response-dialog";

export interface ChatbotMessage {
  
}

export interface Message {
  type: string;
  msgType: string;
  message: string;
  data?: RespDialog;
}

export const Type = {
  SERVER: 'server',
  CLIENT: 'client'
};

export const MessageType = {
  TEXT: 'text',
  IMAGE: 'image',
  BUTTON: 'button',
  CAROUSEL: 'carousel',
  API: 'api'
};

export const Speaker = {
  USER: 'USER',
  CHATBOT: 'CHATBOT'
};