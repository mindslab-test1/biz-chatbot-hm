export interface Clicks {
  r_object_id: string;
  order_no: number;
  keyword: string;
  user_id: number;
  user_dept_code: string;
  user_div_code: number;
  session_id: number;
  click_date: string;
  key: string;
  doc_count: number;
}


export interface StatClicks {
  _source: Clicks;
  buckets: Clicks;
}


