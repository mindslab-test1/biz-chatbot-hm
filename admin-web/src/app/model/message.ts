export const lang = 'ko';
export const Msg = {
  err:{
    ko:{
      failCreateData: '데이터를 생성하지 못했습니다.',
      failLoadData: '데이터를 불러오지 못했습니다.',
      failUpdateData: '데이터를 갱신하지 못했습니다.',
      failDeleteData: '데이터를 삭제하지 못했습니다.',
      failLogin: '로그인에 실패했습니다.',
      failLogout: '로그아웃에 실패했습니다.',
      chatbotNotFoundAnswer: '답변을 찾을 수 없습니다.',
      chatbotSaveFirst: '먼저 챗봇 설정을 해주십시오.',
      domainFailUpdate: '도메인을 저장하지 못했습니다.',
      domainAlreaySelection: '이미 선택된 도메인입니다.',
      sessionNotOpenedYet:'Session이 열리지 않았습니다.',
      itfCloseFail: 'itf 종료를 실패했습니다.',
      invalidAccess: '잘못된 접근입니다.',
      invalidParams: '입력값이 잘못되었습니다.',
      nowLoading: '아직 조회중입니다.',
      _endline: '마지막 줄'
    },
    en:{
      failCreateData: 'Failed to create data',
      failLoadData: 'Failed to load data',
      failUpdateData: 'Failed to update data',
      failDeleteData: 'Failed to delete data',
      failLogin: 'Failed to login',
      failLogout: 'Failed to logout',
      chatbotNotFoundAnswer: 'Chatbot has Not found answer.',
      chatbotSaveFirst: 'You save chatbot data first.',
      domainFailUpdate: 'Update domains error.',
      sessionNotOpenedYet:'Session not opened yet.',
      itfCloseFail: 'Failed to close itf.',
      invalidAccess: 'Invalid access.',
      invalidParams: 'Invalid params.',
      nowLoading: 'Now loading.',
      _endline: '마지막 줄'
    }
  },
  com:{
    ko:{
      nowLoadingChatbot: '챗봇이 로딩 중입니다.',
      succCreateData: '데이터를 생성했습니다.',
      succUpdateData: '데이터를 갱신했습니다.',
      succDeleteData: '데이터를 삭제했습니다.',
      chatbotSessionClose: 'Session을 종료합니다.',
      confirmLogout: '로그아웃하시겠습니까?',
      confirmUpdate: '수정하시겠습니까?',
      confirmResetPassword: '패스워드를 리셋하시겠습니까?',
      domainSuccUpdate: '도메인을 저장했습니다.',
      tagAdmin: '[ADMIN]:',
      titleDashboard: '대시보드',
      titleDialogLog: '대화 이력',
      titleSuccess: '성공',
      titleError: '오류',
      titleAlert: '알림',
      titleConfirm: '확인',
      _endline: '마지막 줄'
    },
    en:{
      nowLoadingChatbot: 'Chatbot, Now loading.',
      succCreateData: 'Created.',
      succUpdateData: 'Updated.',
      succDeleteData: 'Deleted.',
      chatbotSessionClose: 'Close a chatbot session.',
      confirmLogout: 'Logout?',
      confirmUpdate: 'Update?',
      confirmResetPassword: 'Reset password?',
      domainSuccUpdate: 'Updated domains.',
      tagAdmin: '[ADMIN]: ',
      titleDashboard: 'Dashboard',
      titleDialogLog: 'Dialog log',
      titleSuccess: 'Success',
      titleError: 'Error',
      titleAlert: 'Alert',
      titleConfirm: 'Confirm',
      _endline: '마지막 줄'
    }
  }
};
// Msg.com[lang].
// Msg.err[lang].
// Msg.err[lang].invalidAccess
// Msg.com[lang].nowLoadingChatbot
// Msg.err[lang].failLoadData
// Msg.err[lang].SessionNotOpenedYet