export const common = {
  // defaultPath: '/management/chatbot-management',
  defaultPath: '/management/dashboard',
  defaultPath2: '/management/domains/d-bqa',
  root: '/',
  container: 'management',
  // ttl: 1209600, 2 weeks
  ttl: 172800, // 2 days
  adminDefaultPassword: '1234',
  table: {
    SORT: {
      DESC: 'DESC',
      ASC: 'ASC'
    },
    LIMIT: {
      count: [ 10, 25, 50, 100, 500, 1000 ],
      default: 25
    },
    STATE: {
      LOADING: 'loading',
      DONE: 'done'
    },
    FILTER: {
      ALL: 'ALL',
      NUMBER: 'number',
      STRING: 'string',
      DATE: 'date',
      SELECT: 'select',
      RANGE_DATE: 'range_date'
    }
  },
  access: {
    READONLY: 'READONLY',
    READ_WRITE: 'READ_WRITE',
    DENIED: 'DENIED'
  },
  _: require('lodash'),
  // daName: 'da',

  api: 'http://',

  BACK_API_PATH: {
    send_chat: '/bqa/search'
   ,sc_query: '/admin/api/sc/query'
   ,itf_admin_query: '/admin/api/itf/adminquery'
   ,itf_close: '/admin/api/itf/close'
 },
  FRONT_API_PATH: {
    open_dialog: '/dialog/openDialog',
    send_text: '/dialog/sendText',
    close_dialog: '/dialog/closeDialog'
  },
  SDS_API_PATH: {
    itf_learn: '/admin/sds/itf_learn',
    view_login: '/view/loginmodule.do',
    view_logout: '/view/logout.do'
  },
};

