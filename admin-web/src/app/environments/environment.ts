import { common } from './environment.common';
const _ = require('lodash');
export const environment  = _.merge(common, {
  production: false,
});
