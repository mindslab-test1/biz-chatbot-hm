import { Pipe, PipeTransform } from '@angular/core';
import { CommonUtilService } from '../service';

@Pipe({
  name: 'convertCase'
})
export class ConvertCasePipe implements PipeTransform {

  constructor(private cm: CommonUtilService){}
  transform(value: any, before?: any, expect?: any, beforeDelim?: string, afterDelim?: string): any {
    return this.cm.convertCase(value, before, expect, beforeDelim, afterDelim);
  }

}
