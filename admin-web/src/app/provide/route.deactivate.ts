import { Injectable, OnDestroy } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs'
import { DataService } from '../service/data.service';
import { UserService } from '../service/user.service';
export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable()
export class RouteDeactivate implements CanDeactivate<CanComponentDeactivate>, OnDestroy {
    constructor(
      private ds: DataService,
      private userService: UserService
    ) {
    }
    
    ngOnDestroy() {
      this.ds.clearData();
    }

    canDeactivate(
      component: CanComponentDeactivate,
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot){

        if(this.userService.isAuthUser()){  // 2018-09-10 로그아웃 버그 수정
          if(component['isSave'] != undefined){
            const isSave = component['isSave'];
            if(isSave == false) {
              const _getPath = (route, state)=>{
                if (!route || !state){
                  throw 'invalid';
                }
                return route._routerState.url || state.url;
              };
              const url = _getPath(route, state);
              this.ds.sendData(this.ds.CMD.NOTI_EXIT, {isSave, url});
              return false;
            }
          }
        }
       
        // console.log(
        //     'canDeactive'
        //   , '====\n'
        //   , 'component'
        //   , '====\n'
        //   , component
        //   , '\n\n===='
        //   , 'route'
        //   , '====\n'
        //   , route
        //   , '\n\n===='
        //   , 'state'
        //   , '====\n'
        //   , state
        // );
      return component.canDeactivate ? component.canDeactivate() : true;
    }
}
