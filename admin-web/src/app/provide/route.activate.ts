import { Injectable, Inject, EventEmitter } from '@angular/core';
import { Router, CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService, CommonUtilService, MenuService, CommonModalService, MessageExService, AlertModalEvent } from '../service';
import { environment } from '../environments/environment';
import { UserService } from '../service/user.service';
import { Msg, lang } from '../model/message';

@Injectable()
export class RouteActivate implements CanActivate {
    constructor(
      private router: Router,
      private authService: AuthService,
      private cm: CommonUtilService,
      private menuService: MenuService,
      @Inject(CommonModalService) private commonModalService: CommonModalService,
      private userService: UserService
    ) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      // this.cm.devLog('\nrouteActivate canActivate...', '======================================================='
      //   ,'\nActivatedRouteSnapshot==>', route
      //   ,'\nRouterStateSnapshot ==> ', state
      //   ,'\nrouter ==>' ,this.router
      // );
      this.userService.findUser().subscribe((res) => {

        // this.cm.devLog('auth');

      }, err => {
        this.cm.devLog(MessageExService.parseErrorMessage(err));
        // this.commonModalService.openAlertDialog(Msg.com[lang].titleError, Msg.err[lang].invalidAccess, false, null);
        alert(Msg.err[lang].invalidAccess);
        this.cm.devLog('navigate to root... cause Failed to find user');
        this.router.navigateByUrl(environment.root);
        return false;
      });

      const currUser = this.userService.getCachedUserInfo();
      this.cm.devLog('user Check', currUser);
      if (this.cm.isEmpty(currUser)) {
        this.cm.devLog('navigate to root... cause USER_EMPTY');
        this.router.navigateByUrl(environment.root);
      }

      const {isRoot, currPath} = this.checkCurrPath(route, state);
      if (isRoot) {
        this.cm.devLog('navigate to root... cause IS_ROOT');
        return true;
      }
      this.cm.devLog(`canActivate. currPath: ${currPath}`);
      // this.cm.devLog('====================================================================================\n');


      if (this.checkCurrMenuAuth(currPath)) {
        return true;
      }

      this.cm.devLog('false alert.');

      const msg = `Your account hasn't auth for [${currPath}].`;
      const emitter = new EventEmitter<AlertModalEvent>();
      emitter.subscribe(res => {
        if (res.isOk) {
          return true;
        }
        this.router.navigateByUrl(environment.root);
        return false;
      }, err => {
        this.cm.devLog('err', err);

      });
      this.commonModalService.openAlertDialog(Msg.com[lang].titleError, msg, false, emitter);

      return false;
    }

    checkCurrPath(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

      // router 객체의 타입 체크 오류 회피용 함수
      const _getPath = (route: any, state: any, router: any) => {
        if (!route || !state) {
          throw new Error('invalid');
        }
        return route._routerState.url || state.url || router.location.path() || router.url;
      };
      const currPath = _getPath(route, state, this.router);
      const isRoot = ((currPath === '') ? true : false);
      return {
        isRoot, currPath
      };
    }

    checkCurrMenuAuth(currPath) {
      const {res, menu} = this.menuService.findCurrentMenu(currPath);
      if (!res) {
        return false;
      }
      return this.authService.checkMenuAuthRead(menu);
    }

}
