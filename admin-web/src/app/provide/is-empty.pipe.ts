import { Pipe, PipeTransform } from '@angular/core';
import { CommonUtilService } from '../service';

@Pipe({
  name: 'isEmpty'
})
export class IsEmptyPipe implements PipeTransform {
  constructor(private cm: CommonUtilService){}

  transform(value: any, args?: any): any {
    return this.cm.isEmpty(value);
  }
}
