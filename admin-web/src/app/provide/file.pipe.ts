import { Pipe, PipeTransform } from '@angular/core';
import { FileService } from '../service/file.service';
import { CommonUtilService } from '../service';

@Pipe({
  name: 'file'
})
export class FilePipe implements PipeTransform {
  constructor(
    private cm: CommonUtilService
    , private fileService: FileService
  ) {
  }

  transform(value: string, args?: any): any {
    if(this.cm.isEmpty(args)){
      return '';
    }
    return this.fileService.getDownloadPath(args,value);
  }

}
