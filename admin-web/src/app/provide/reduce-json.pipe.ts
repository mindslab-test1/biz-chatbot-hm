import { Pipe, PipeTransform } from '@angular/core';
import { CommonUtilService } from '../service';

@Pipe({
  name: 'reduceJson'
})
export class ReduceJsonPipe implements PipeTransform {

  constructor(
    private cm:CommonUtilService
  ){
  }

  transform(value: any, args): any {
    const json: Array<Object> = value;
    
    if(this.cm.isEmpty(json) || !json.length) {
      return value;
    }
    if(this.cm.isEmpty(args)) {
      return value;
    }
    const key = args[0];
    if(this.cm.isEmpty(key)) {
      return value;
    }
    const DELIM = args.length < 2 ? ',' : args[1];

    const result = this.cm.getKeys(json, key);

    return result.join(DELIM);
  }

}
