import { Http, Request, RequestOptionsArgs, Response, RequestOptions, ConnectionBackend, Headers, XHRBackend } from '@angular/http';
import { Router} from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import { CookieService } from 'ng2-cookies';

import 'rxjs/add/operator/map';

@Injectable()
export class HttpInterceptor extends Http {

  constructor(backend: ConnectionBackend,
              defaultOptions: RequestOptions,
              private router: Router,
              private cookie: CookieService) {
      super(backend, defaultOptions);
  }

  // request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
  //     return this.intercept( super.request(url, options));
  // }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
      return this.intercept(super.get(url, this.getRequestOptionArgs(options)));
  }

  post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
      if (url.indexOf('upload') !== -1) {
        return this.intercept(super.post(url, body, this.getRequestOptionArgs(options, true)));
      } else {
        return this.intercept(super.post(url, body, this.getRequestOptionArgs(options)));
      }
  }

  put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
      return this.intercept(super.put(url, body, this.getRequestOptionArgs(options)));
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
      return this.intercept(super.delete(url, this.getRequestOptionArgs(options)));
  }

  private getRequestOptionArgs(options?: RequestOptionsArgs, passContextType: boolean = false): RequestOptionsArgs {
      if (options == null) {
          options = new RequestOptions();
      }
      if (options.headers == null) {
          options.headers = new Headers();
      }
      if (!passContextType) {
        options.headers.append('Content-Type', 'application/json');
      }
      return options;
  }
  intercept(observable: Observable<Response>): Observable<any> {
      return observable
        .map((res: Response) => {
            return  res;

        });
  }
}


export function HttpInterceptorFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions, router: Router, cookie: CookieService) {
  return new HttpInterceptor(xhrBackend, requestOptions, router, cookie);
}
