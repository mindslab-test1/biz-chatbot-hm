import { Pipe, PipeTransform } from '@angular/core';
import { DateService } from '../service/date.service';

@Pipe({
  name: 'isDateStr'
})
export class IsDateStrPipe implements PipeTransform {
  constructor(private dt: DateService) {}

  transform(value: string, args?: any): any {
    return this.dt.isDateStr(value);
  }

}
