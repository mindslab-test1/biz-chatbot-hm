import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'emptyValue'
})
export class EmptyValuePipe implements PipeTransform {
  NO_DATA_V: string = '-';
  transform(value: any, args?: any): any {
    return value || this.NO_DATA_V;
  }
}
